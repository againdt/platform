package com.getinsured.hix.platform.tools.encryption.jpa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;

import com.getinsured.hix.platform.tools.encryption.domain.D2CEnrollment;
import com.getinsured.hix.platform.tools.encryption.domain.IssuerPaymentInformation;
import com.getinsured.hix.platform.tools.encryption.domain.Securables;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixSymetricPooledCipher;

public class EncryptionMigrator {
	private static EntityManager manager;
	private GhixJasyptEncrytorUtil oldEncryptor;
	private boolean initialized;
	
	private GhixSymetricPooledCipher ghixSymetricPooledCipherObj;
	private GhixAESCipherPool ghixAESCipherPoolObj;
	private static String options_str = "\n"
			+ "Migrate Enrollment      (D2C).................................1\n"
			+ "Migrate SECURABLES      (CAP).................................2\n"
			+ "Cleanup Bad Enrollments (D2C).................................3\n"
			+ "Generate Encrypted Tag  ......................................4\n"
			+ "Generate Encrypted Property...................................5\n"
			+ "Decrypt a Jasypt Encrypted Property...........................6\n"
			+ "Decrypt a Latest Algo Encrypted Property......................7\n"
			+ "MIGRATE ISSUER PAYMENT INFORMATION........................... 8\n"
			+ "AES Migrate Enrollment      (D2C).............................9\n"
			+ "AES Migrate SECURABLES      (CAP).............................10\n"
			+ "Generate AES Encrypted Tag  ..................................11\n"
			+ "Generate AES Encrypted Property...............................12\n"
			+ "AES MIGRATE ISSUER PAYMENT INFORMATION........................13\n"
			+ "Select your option";

	

	public EncryptionMigrator(EntityManager mgr, boolean aesFlow) {
		manager = mgr;
		ghixSymetricPooledCipherObj = new GhixSymetricPooledCipher();
		ghixAESCipherPoolObj = new GhixAESCipherPool();
	}
	
	public EncryptionMigrator(EntityManager mgr) {
		manager = mgr;
		ghixSymetricPooledCipherObj = new GhixSymetricPooledCipher();
		ghixAESCipherPoolObj = new GhixAESCipherPool();
		try {
				init();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */

	private static String getUserInput(String paramName, String defaultVal)
			throws IOException {
		boolean inputProvided = false;
		String val = null;
		while (!inputProvided) {
			if (paramName != null) {
				System.out.print("GhixEncryptionMigration> "+paramName
						+ "::"
						+ ((defaultVal == null) ? ("   ") : ("[" + defaultVal)
								+ "]   "));
			}
			val = new BufferedReader(new InputStreamReader(System.in))
					.readLine();
			if (val == null || val.length() == 0) {
				if (defaultVal != null) {
					System.out.println("");
					return defaultVal;
				} else {
					continue;
				}
			}
			inputProvided = true;
		}
		if (val.equalsIgnoreCase("\\q")) {
			System.out.println("GhixEncryptionMigration> "+"....Bye");
			System.exit(0);
		}
		return val;
	}

	private static boolean getConfirmation(HashMap<String, String> params)
			throws IOException {
		Set<Entry<String, String>> set = params.entrySet();
		Iterator<Entry<String, String>> cursor = set.iterator();
		Entry<String, String> entry = null;
		while (cursor.hasNext()) {
			entry = cursor.next();
			System.out.println(entry.getKey() + "::" + entry.getValue());
		}
		System.out.print("Please confirm [Y/N]:___");
		String answer = getUserInput(null, "N");
		return answer.equalsIgnoreCase("Y");

	}

	public static void main(String[] args) throws IOException {
		while (true) {
			String option = getUserInput(options_str, "1");
			processOption(option);
		}
		// System.exit(0);
	}

	private static void setMigrationRoutine(String migrateOption) {
		// TODO Auto-generated method stub

	}

	private static void processOption(String option) throws IOException {
		if (option.equalsIgnoreCase("1")) {
			processD2CEnrollment();
		} else if (option.equalsIgnoreCase("2")) {
			processSecurables();
		} else if (option.equalsIgnoreCase("3")) {
			processBadEnrollment();
		} else if (option.equalsIgnoreCase("4")) {
			encryptForGENCTag();
		} else if (option.equalsIgnoreCase("5")) {
			encryptForConfigurationProperty();
		}else if (option.equalsIgnoreCase("6")) {
			decryptAProperty();
		}else if (option.equalsIgnoreCase("7")) {
			decryptAPropertyUsintNewAlgo();
		}else if (option.equalsIgnoreCase("8")) {
			procesIssuerPaymentInfo();
		}else if (option.equalsIgnoreCase("9")) {
			processAESD2CEnrollment();
		}else if (option.equalsIgnoreCase("10")) {
			processAESSecurables();
		}else if (option.equalsIgnoreCase("11")) {
			encryptAESForGENCTag();
		}else if (option.equalsIgnoreCase("12")) {
			encryptAESForConfigurationProperty();
		}else if (option.equalsIgnoreCase("13")) {
			procesAESIssuerPaymentInfo();
		}else {
			System.out.println("Invalid Input Option:" + option);
			System.exit(0);
		}

	}
	
	private static void procesAESIssuerPaymentInfo() throws IOException {
		System.out.println("************************************* Processing Issuer payment Information ********************************************");
		EntityManager emgr = getEntityManager();
		EncryptionMigrator migrator = new EncryptionMigrator(emgr, true);
		try {
			String outFile = migrator.migrateAESIssuerPaymentInfo(false);
			String commit = getUserInput(
					"Please review the dry run data stored in file:" + outFile
							+ " And confirm[Y/N]", "N");
			if (commit.equalsIgnoreCase("Y")) {
				migrator.migrateAESIssuerPaymentInfo(true);
			} else {
				System.out.println("Not making any DB changes...");
			}
			String runTest = getUserInput(
					"Done migrating data, Do you want to run a decryption Test?",
					"Y");
			if (runTest.equalsIgnoreCase("Y")) {
				migrator.testAESIssuerPaymentInfo();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done migrating payment information data");
		
	}
	
	private static void procesIssuerPaymentInfo() throws IOException {
		System.out.println("************************************* Processing Issuer payment Information ********************************************");
		EntityManager emgr = getEntityManager();
		EncryptionMigrator migrator = new EncryptionMigrator(emgr);
		EntityTransaction tx = emgr.getTransaction();
		tx.begin();
		try {
			String outFile = migrator.migrateIssuerPaymentInfo(false);
			String commit = getUserInput(
					"Please review the dry run data stored in file:" + outFile
							+ " And confirm[Y/N]", "N");
			if (commit.equalsIgnoreCase("Y")) {
				migrator.migrateIssuerPaymentInfo(true);
			} else {
				System.out.println("Not making any DB changes...");
			}
			String runTest = getUserInput(
					"Done migrating data, Do you want to run a decryption Test?",
					"Y");
			if (runTest.equalsIgnoreCase("Y")) {
				migrator.testSecurables();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		System.out.println("Done migrating payment information data");
		
	}
	
	private String migrateAESIssuerPaymentInfo(boolean commitChanges) throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String encData = null;
		String decData = null;
		int offset = 0;
		File outputFile = new File("IssuerPaymentInfo_aes" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		
		CriteriaBuilder qb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(IssuerPaymentInformation.class)));
		
		Long count = manager.createQuery(cq).getSingleResult();
		printLog(pw, "num of Issuer Payment Information records:" +count);
		System.out.println("num of Issuer Payment Information records:" + count);
		
		List<IssuerPaymentInformation> resultList = new ArrayList<IssuerPaymentInformation>();
		EntityTransaction tx = manager.getTransaction();
		
		do {
			 resultList = manager.createQuery( "Select a From IssuerPaymentInformation a", IssuerPaymentInformation.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
		
			 for (IssuerPaymentInformation next : resultList) {
				printLog(pw, "************ Issuer Payment Information [ID=" + next.getId()+ "] *******************");
				tx.begin();
				try {
						data = next.getPassword();// Expected to be encrypted
						printLog(pw, "Input Password:" + data);
						if (data != null && !data.startsWith("{")) {
							decData = ghixSymetricPooledCipherObj.decrypt(data);
							printLog(pw, "Decrypted:" + decData);
							if (decData != null) {
								encData = ghixAESCipherPoolObj.encrypt(decData);
								printLog(pw, "Re Encrypted to:" + encData);
								if (commitChanges) {
									next.setPassword(encData);
								}
							}
						}
							
					} catch (Exception e) {
						printLog(pw, "Password column Failed with:" + e.getMessage());
					}
					
				try {
					data = null;decData = null;encData=null;
					data = next.getPasswordSecuredKey();// Expected to be encrypted
					printLog(pw, "Input Password Secured Key:" + data);
					if (data != null && !data.startsWith("{")) {
						decData = ghixSymetricPooledCipherObj.decrypt(data);
						printLog(pw, "Decrypted:" + decData);
						if (decData != null) {
							encData = ghixAESCipherPoolObj.encrypt(decData);
							printLog(pw, "Re Encrypted to:" + encData);
							if (commitChanges) {
								next.setPasswordSecuredKey(encData);
							}
						}
					}
					
				} catch (Exception e) {
					printLog(pw,"Password Key processing Failed with:" + e.getMessage());
				}
				
				if (commitChanges) {
					manager.merge(next);
				}
				try {
					tx.commit();
				} catch (Exception e) {
					printLog(pw, "Failed to Update DB :" + e.getMessage());
				}
			 }
			 
			 offset +=10; 
			 pw.flush();
			 
			}while (resultList.size() > 0);
			
			pw.close();
		return outputFile.getAbsolutePath();
	}
	
	private String testAESIssuerPaymentInfo() throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String encData = null;
		String decData = null;
		int offset = 0;
		File outputFile = new File("IssuerPaymentInfo_aes_Test" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		
		CriteriaBuilder qb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(IssuerPaymentInformation.class)));
		
		Long count = manager.createQuery(cq).getSingleResult();
		printLog(pw, "num of Issuer Payment Information records:" +count);
		System.out.println("num of Issuer Payment Information records:" + count);
		
		List<IssuerPaymentInformation> resultList = new ArrayList<IssuerPaymentInformation>();
		
		do {
			 resultList = manager.createQuery( "Select a From IssuerPaymentInformation a", IssuerPaymentInformation.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
		
			 for (IssuerPaymentInformation next : resultList) {
				printLog(pw, "************ Issuer Payment Information [ID=" + next.getId()+ "] *******************");
				try {
						data = next.getPassword();// Expected to be encrypted
						printLog(pw, "Input:" + data);
						if (data != null && !data.startsWith("{")) {
							decData = ghixAESCipherPoolObj.decrypt(data);
							printLog(pw, "Decrypted Password:" + decData);
						}
					} catch (Exception e) {
						printLog(pw, "Password column Failed with:" + e.getMessage());
						System.out.println("DECRYPTED Password: FAILED");
					}
				try {
						data = null;decData = null;encData=null;
						data = next.getPasswordSecuredKey();// Expected to be encrypted
						printLog(pw, "Input:" + data);
						if (data != null && !data.startsWith("{")) {
							decData = ghixAESCipherPoolObj.decrypt(data);
							printLog(pw, "Decrypted Password Secured Key:" + decData);
						}
							
					} catch (Exception e) {
						printLog(pw,"Password Key processing Failed with:" + e.getMessage());
						System.out.println("DECRYPTED Password Secured Key: FAILED");
					}
				 }
				 offset +=10; 
				 pw.flush();
			}while (resultList.size() > 0);
			pw.close();
		return outputFile.getAbsolutePath();
	}
	
	private String migrateIssuerPaymentInfo(boolean commitChanges) throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String encData = null;
		String decData = null;
		File outputFile = new File("IssuerPaymentInfo_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		List<IssuerPaymentInformation> resultList = manager.createQuery(
				"Select a From IssuerPaymentInformation a", IssuerPaymentInformation.class)
				.getResultList();	
		printLog(pw, "num of Records:" + resultList.size());
		for (IssuerPaymentInformation next : resultList) {
			printLog(pw, "************ Issuer Payment Information [ID=" + next.getId()+ "] *******************");
			try {
						data = next.getPassword();// Expected to be encrypted
						printLog(pw, "Input:" + data);
						if (data != null) {
							decData = this.decryptData(data);
							printLog(pw, "Decrypted:" + decData);
							if (decData != null) {
								encData = ghixSymetricPooledCipherObj.encrypt(decData);
								printLog(pw, "Re Encrypted to:" + encData);
								if (commitChanges) {
									next.setPassword(encData);
								}
							}
						}
						
					} catch (Exception e) {
						printLog(pw, "Password column Failed with:" + e.getMessage());
					}
			printLog(pw, "************ PASSWORD KEY [ID=" + next.getId()+ "]*******************");
					try {
						data = null;decData = null;encData=null;
						data = next.getPasswordSecuredKey();// Expected to be encrypted
						printLog(pw, "Input:" + data);
						if (data != null) {
							decData = this.decryptData(data);
							printLog(pw, "Decrypted:" + decData);
							if (decData != null) {
								encData = ghixSymetricPooledCipherObj.encrypt(decData);
								printLog(pw, "Re Encrypted to:" + encData);
								if (commitChanges) {
									next.setPasswordSecuredKey(encData);
								}
							}
						}
						
					} catch (Exception e) {
						printLog(pw,
								"Password Key processing Failed with:" + e.getMessage());
					}
			pw.flush();
			pw.close();
			if (commitChanges) {
				manager.merge(next);
			}
		}
		return outputFile.getAbsolutePath();
	}
	

	private static void decryptAPropertyUsintNewAlgo()  {
		String encryptionKey = System.getProperty("PASSWORD_KEY");
		GhixSymetricPooledCipher ghixSymetricPooledCipherObj = new GhixSymetricPooledCipher();
		try{
			if (encryptionKey == null) {
				encryptionKey = getUserInput("Encryption Key", null);
				System.setProperty("PASSWORD_KEY", encryptionKey);
			}
		String encryptedStr = getUserInput("Please Enter an encrypted property", null);
		System.out.print(ghixSymetricPooledCipherObj.decrypt(encryptedStr));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void decryptAProperty() throws IOException {
		String encryptionKey = System.getProperty("PASSWORD_KEY");
		if (encryptionKey == null) {
			encryptionKey = getUserInput("Encryption Key", null);
			System.setProperty("PASSWORD_KEY", encryptionKey);
		}
		String encryptedStr = getUserInput("Please Enter an encrypted property", null);
		EncryptionMigrator migrator = new EncryptionMigrator(null);
		System.out.println(migrator.decryptData(encryptedStr));
	}
	
	private static void encryptAESForConfigurationProperty() throws IOException {
		String key = null;
		String val = null;
		GhixAESCipherPool ghixAESCipherPoolObj = new GhixAESCipherPool();
		
		try {

			key = getUserInput("Property Key", null);
			if (key == null || key.length() == 0) {
				return;
			}
			val = getUserInput("Property Value", null);
			System.out
					.println("******************** Encrypted property **********************");
			System.out.println(key + "=ENC("
					+ ghixAESCipherPoolObj.encrypt(val) + ")");
			System.out
					.println("**************************************************************");
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}
	
	private static void encryptForConfigurationProperty() throws IOException {
		String key = null;
		String val = null;
		GhixSymetricPooledCipher ghixSymetricPooledCipherObj = new GhixSymetricPooledCipher();
		
		String encryptionKey = System.getProperty("PASSWORD_KEY");
		if (encryptionKey == null) {
			encryptionKey = getUserInput("Encryption Key", null);
			System.setProperty("PASSWORD_KEY", encryptionKey);
		}
		try {

			key = getUserInput("Property Key", null);
			if (key == null || key.length() == 0) {
				return;
			}
			val = getUserInput("Property Value", null);
			System.out
					.println("******************** Encrypted property **********************");
			System.out.println(key + "=ENC("
					+ ghixSymetricPooledCipherObj.encrypt(val) + ")");
			System.out
					.println("**************************************************************");
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}
	
	private static void encryptAESForGENCTag() throws IOException {
		String val = null;
		GhixAESCipherPool ghixAESCipherPoolObj = new GhixAESCipherPool();
		
		try {
			val = getUserInput("Tag Value", null);
			System.out
					.println("******************** Encrypted Tag ***************************");
			System.out.println(val + "=GENC["
					+ ghixAESCipherPoolObj.encrypt(val) + "]");
			System.out
					.println("**************************************************************");
		} catch (Exception ie) {
			ie.printStackTrace();
		}

	}
	
	private static void encryptForGENCTag() throws IOException {
		String val = null;
		String encryptionKey = System.getProperty("PASSWORD_KEY");
		GhixSymetricPooledCipher ghixSymetricPooledCipherObj = new GhixSymetricPooledCipher();
		
		if (encryptionKey == null) {
			encryptionKey = getUserInput("Encryption Key", null);
			System.setProperty("PASSWORD_KEY", encryptionKey);
		}

		try {
			val = getUserInput("Tag Value", null);
			System.out
					.println("******************** Encrypted Tag ***************************");
			System.out.println(val + "=GENC["
					+ ghixSymetricPooledCipherObj.encrypt(val) + "]");
			System.out
					.println("**************************************************************");
		} catch (Exception ie) {
			ie.printStackTrace();
		}

	}

	private static void processBadEnrollment() throws IOException {
		System.out.println("*********************************** Processing Bad Enrollment ***********************************************");
		System.out.println(" This procedure will read all the Encrypted UI_DATA records from D2C Enrollment Table\n"
						 + " And attempt to Decrypt using the new encryption procedure, In case decryption is not\n "
						 + " successfull, This UI_DATA value field will be set to NULL, PLEASE DO NOT USE THIS OPTION\n"
						 + " IF YOU DON'T KNOW WHAT YOU ARE DOING");
		String confirmation = getUserInput(" Please confirm if you understand the risks and still want to proceed", "N");
		if(confirmation.equalsIgnoreCase("N")){
			return;
		}
		EntityManager emgr = getEntityManager();
		String outFile = null;
		EncryptionMigrator migrator = new EncryptionMigrator(emgr);
		EntityTransaction tx = emgr.getTransaction();
		tx.begin();
		try {
			outFile = migrator.markBadD2CEnrollments();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		System.out.println("Done processing bad enrollment data:" + outFile);

	}

	private static void processSecurables() throws IOException {
		System.out.println("************************************* Processing Table Securables ********************************************");
		EntityManager emgr = getEntityManager();
		EncryptionMigrator migrator = new EncryptionMigrator(emgr);
		EntityTransaction tx = emgr.getTransaction();
		tx.begin();
		try {
			String outFile = migrator.migrateSecurables(false);
			String commit = getUserInput(
					"Please review the dry run data stored in file:" + outFile
							+ " And confirm[Y/N]", "N");
			if (commit.equalsIgnoreCase("Y")) {
				migrator.migrateSecurables(true);
			} else {
				System.out.println("Not making any DB changes...");
			}
			String runTest = getUserInput(
					"Done migrating data, Do you want to run a decryption Test?",
					"Y");
			if (runTest.equalsIgnoreCase("Y")) {
				migrator.testSecurables();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		System.out.println("Done migrating Securement data");
	}
	
	private static void processAESSecurables() throws IOException {
		System.out.println("************************************* Processing Table Securables ********************************************");
		EntityManager emgr = getEntityManager();
		EncryptionMigrator migrator = new EncryptionMigrator(emgr, true);
		try {
			String outFile = migrator.migrateAESSecurables(false);
			String commit = getUserInput(
					"Please review the dry run data stored in file:" + outFile
							+ " And confirm[Y/N]", "N");
			if (commit.equalsIgnoreCase("Y")) {
				migrator.migrateAESSecurables(true);
			} else {
				System.out.println("Not making any DB changes...");
			}
			String runTest = getUserInput(
					"Done migrating data, Do you want to run a decryption Test?",
					"Y");
			if (runTest.equalsIgnoreCase("Y")) {
				migrator.testAESSecurables();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done migrating Securement data");
	}
	
	private String migrateAESSecurables(boolean commitChanges) throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String encData = null;
		String decData = null;
		File outputFile = new File("Securables_aes" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		CriteriaBuilder qb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(Securables.class)));
		
		Long count = manager.createQuery(cq).getSingleResult();
		printLog(pw, "num of Securables records:" +count);
		System.out.println("num of securables records:" + count);
		
		List<Securables> resultList = new ArrayList<Securables>();
		EntityTransaction tx = manager.getTransaction();
		int offset = 0;
		do {
			 resultList = manager.createQuery( "Select a From Securables a", Securables.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			 for (Securables record : resultList) {
				printLog(pw, "********** SECURABLE PASSWORD [ID=" + record.getId() + "]************");
				data = record.getPassword();
				tx.begin();
				try {
					if (data == null) {
						printLog(pw, "No Password found");
					} else {
						printLog(pw, "[Input          ]:" + data);
						decData = ghixSymetricPooledCipherObj.decrypt(data);
						encData = ghixAESCipherPoolObj.encrypt(decData);
						printLog(pw, "[Decrypted      ]:" + decData);
						printLog(pw, "[Re-Encrypted To]:" + encData);
						if (commitChanges) {
							record.setPassword(encData);
						}
					}
				} catch (Exception e) {
					printLog(pw, "Failed with:" + e.getMessage());
				}
				if (commitChanges) {
					manager.merge(record);
				}
				try {
					tx.commit();
				} catch (Exception e) {
					printLog(pw, "Failed to Update DB :" + e.getMessage());
				}
			 }
			 offset +=10; 
			 pw.flush();
		}while (resultList.size() > 0);
		
		pw.close();
		return outputFile.getAbsolutePath();
	}
	
	private void testAESSecurables() throws FileNotFoundException {
		String data = null;
		String decData = null;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		File outputFile = new File("Securables_AES_Test" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		
		List<Securables> resultList = new ArrayList<Securables>();
		int offset = 0;
		do {
			 resultList = manager.createQuery( "Select a From Securables a", Securables.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			for (Securables next : resultList) {
				printLog(pw, "********** Record ID:" + next.getId()	+ " ***************");
				data = next.getPassword();
				try {
					if (data == null) {
						printLog(pw, "Password data is null");
					} else {
						printLog(pw, "DATA:" + data);
						decData = ghixAESCipherPoolObj.decrypt(data);
						printLog(pw, "DECRYPTED Password:" + decData);
						
					}
				} catch (Exception e) {
					printLog(pw, "Decryption Failed :" + e.getMessage());
					System.out.println("DECRYPTED Password: FAILED");
				}
			}
			offset +=10; 
			pw.flush();
		} while (resultList.size() > 0);
		pw.close();
	}
	
	private static void processD2CEnrollment() throws IOException {
		System.out
				.println("************ Processing Table D2C Enrollment....Initiating a dry run, no DB changes will be made **********");
		EntityManager emgr = getEntityManager();
		EncryptionMigrator migrator = new EncryptionMigrator(emgr);
		EntityTransaction tx = emgr.getTransaction();
		tx.begin();
		try {
			String outFile = migrator.migrateD2CEnrollments(false);
			String commit = getUserInput(
					"Please review the dry run data stored in file:" + outFile
							+ " And confirm[Y/N]", "N");
			if (commit.equalsIgnoreCase("Y")) {
				System.out.println("Initiating DB changes.......");
				migrator.migrateD2CEnrollments(true);
			} else {
				System.out.println("Not making DB changes....");
			}
			String runTest = getUserInput(
					"Done migrating data, Do you want to run a decryption Test?",
					"Y");
			if (runTest.equalsIgnoreCase("Y")) {
				migrator.testD2CEnrollments();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		System.out.println("Done migrating D2CEnrollment data");
	}
	
	private static void processAESD2CEnrollment() throws IOException {
		System.out
				.println("************ Processing Table D2C Enrollment....Initiating a dry run, no DB changes will be made **********");
		EntityManager emgr = getEntityManager();
		EncryptionMigrator migrator = new EncryptionMigrator(emgr, true);
		
		try {
			String outFile = migrator.migrateAESD2CEnrollments(false);
			String commit = getUserInput(
					"Please review the dry run data stored in file:" + outFile
							+ " And confirm[Y/N]", "N");
			if (commit.equalsIgnoreCase("Y")) {
				System.out.println("Initiating DB changes.......");
				migrator.migrateAESD2CEnrollments(true);
			} else {
				System.out.println("Not making DB changes....");
			}
			String runTest = getUserInput(
					"Done migrating data, Do you want to run a decryption Test?",
					"Y");
			if (runTest.equalsIgnoreCase("Y")) {
				migrator.testAESD2CEnrollments();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Done migrating D2CEnrollment data");
	}
	
	
	public static EntityManager getEntityManager() throws IOException {
		if (manager != null) {
			return manager;
		}
		System.out
				.println("\n*************************** Encrypted Data Migration Tool *******************************\n");
		System.out
				.println(" This Utility will migrate the data encrypted by builds compiled and deployed prior to Jan 30, 2015");
		System.out
				.println(" Into the new Encryption implementation. As of now there are only 2 models known to be persisting data");
		System.out.println(" \t1) D2C Enrollment\n\t\t a) UI_DATA");
		System.out.println(" \t\t b) PAYMENT_DATA");
		System.out.println(" \t2) Securables\n\t\t a) Password");
		System.out.println(" \t3) Issuer Payment Information\n\t\t a) Password\n\t\t b) Secret Key");
		System.out
				.println(" If you intend to migrate fields other than mentioned above, STOP and contact the platform team\n");
		System.out.println(" Enter \\q at any time to quit\n");

		String proceed = getUserInput(" Do you want to continue?", "N");
		if (proceed.equalsIgnoreCase("N")) {
			System.exit(0);
		}
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		EntityManager manager = null;
		String driver = getUserInput("Driver Class", "oracle.jdbc.OracleDriver");
		parameters.put("javax.persistence.jdbc.driver", driver);

		String dbUrl = getUserInput("Complete DB URL", "");
		parameters.put("javax.persistence.jdbc.url", dbUrl);

		String dbUser = getUserInput("DB User name", "");
		parameters.put("javax.persistence.jdbc.user", dbUser);

		String password = getUserInput("DB Password", "");
		parameters.put("javax.persistence.jdbc.password", password);

		if (!getConfirmation(parameters)) {
			parameters.clear();
			getEntityManager();
		}
		
		proceed = getUserInput(" Please confirm if you have already taken back up of above mentioned tables", "N");
		if(proceed.equalsIgnoreCase("N")){
			System.out.println("You can not continue without taking the back up of current state for the tables \"D2C_Enrollment\" and \"SECURABLES\"");
			System.exit(0);
		}
		
		try {
			EntityManagerFactory factory = Persistence
					.createEntityManagerFactory("encryptionUnit", parameters);
			manager = factory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Failed to create Entity manager, Message:"
					+ e.getMessage());
			e.printStackTrace();
		}
		if (manager == null) {
			System.exit(0);
		}
		return manager;
	}

	public void init() throws IOException {
		if (this.initialized) {
			return;
		}
		String encryptionKey = System.getProperty("PASSWORD_KEY");
		if (encryptionKey == null) {
			encryptionKey = getUserInput("Encryption Key", null);
			System.setProperty("PASSWORD_KEY", encryptionKey);
		}
		EnvironmentStringPBEConfig environmentStringPBEConfig = new EnvironmentStringPBEConfig();
		environmentStringPBEConfig.setAlgorithm("PBEWithMD5AndDES");
		environmentStringPBEConfig.setPasswordEnvName("PASSWORD_KEY");
		environmentStringPBEConfig.setPoolSize(100);
		//environmentStringPBEConfig.setStringOutputType("base64");
		this.oldEncryptor = new GhixJasyptEncrytorUtil(
				environmentStringPBEConfig);
		this.initialized = true;
	}

	private String encryptJaonWithNew(String data) throws Exception {

		if (data != null && data.startsWith("{")) {
			return ghixSymetricPooledCipherObj.encrypt(data);
		}
		return null;
	}

	private String decryptData(String data) {
		if (data == null) {
			return data;
		}
		if (data.startsWith("{")) {
			return data;
		}
		return this.oldEncryptor.decryptStringByJasypt(data);
	}
	
	private String migrateAESD2CEnrollments(boolean commitChanges) throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String paymentData = null;
		String encPaymentData = null;
		String decPaymentData = null;
		String encData = null;
		String decData = null;
		int offset =0;
		File outputFile = new File("D2C_Enrollment_Aes" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		CriteriaBuilder qb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(D2CEnrollment.class)));
		
		Long count = manager.createQuery(cq).getSingleResult();
		printLog(pw, "num of D2C records:" +count);
		System.out.println("num of D2C records:" + count);
		
		List<D2CEnrollment> resultList = new ArrayList<D2CEnrollment>();
		EntityTransaction tx = manager.getTransaction();
		do {
			 resultList = manager.createQuery( "Select a From D2CEnrollment a", D2CEnrollment.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			 for (D2CEnrollment next : resultList) {
					tx.begin();
					printLog(pw, "************ D2C  [ID=" + next.getId()	+ "] *******************");
					try {
						data = next.getUiData();// Expected to be encrypted
						printLog(pw, " UI DATA Input:" + data);
						if (data != null &&  !data.startsWith("{")) {
							decData = ghixSymetricPooledCipherObj.decrypt(data);
							printLog(pw, " UI DATA Decrypted:" + decData);
							if (decData != null) {
								encData = ghixAESCipherPoolObj.encrypt(decData);
								printLog(pw, "[Re Encrypted to ]:" + encData);
								if (commitChanges) {
									next.setUiData(encData);
								}
							}
						}
					} catch (Exception e) {
						printLog(pw, "UI Data Failed with:" + e.getMessage());
					}
					
					try {
						paymentData = next.getPaymentData();
						printLog(pw, "Payment DATA Input:" + paymentData);
						if (paymentData != null) {
							decPaymentData = ghixSymetricPooledCipherObj.decrypt(paymentData);
							printLog(pw, "Payment DATA Decrypted     :" + decPaymentData);
							if (decPaymentData != null) {
								encPaymentData = ghixAESCipherPoolObj.encrypt(decPaymentData);
								printLog(pw, "[Re-Encrypted to]:" + encPaymentData);
								if (commitChanges) {
									next.setPaymentData(encPaymentData);
								}
							}
						}
					} catch (Exception e) {
						printLog(pw,
								"Payment data processing Failed with:" + e.getMessage());
					}
					if (commitChanges) {
						manager.merge(next);
					}
					try {
						tx.commit();
					} catch (Exception e) {
						printLog(pw, "Failed to Update DB :" + e.getMessage());
					}
			 }
			 offset +=10; 
			 pw.flush();
		}while (resultList.size() > 0);
		
		pw.close();
		
		return outputFile.getAbsolutePath();
	}
	
	@SuppressWarnings("unused")
	private String migrateD2CEnrollments(boolean commitChanges)
			throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String paymentData = null;
		String encPaymentData = null;
		String decPaymentData = null;
		String encData = null;
		String decData = null;
		boolean updated = false;
		File outputFile = new File("D2C_Enrollment_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		List<D2CEnrollment> resultList = manager.createQuery(
				"Select a From D2CEnrollment a", D2CEnrollment.class)
				.getResultList();	
		printLog(pw, "num of employess:" + resultList.size());
		for (D2CEnrollment next : resultList) {
			printLog(pw, "************ UI DATA [ID=" + next.getId()
					+ "] *******************");
			updated = false;
			try {
				data = next.getUiData();// Expected to be encrypted
				printLog(pw, "Input:" + data);
				if (data != null) {
					decData = this.decryptData(data);
					printLog(pw, "Decrypted:" + decData);
					if (decData != null) {
						encData = this.encryptJaonWithNew(decData);
						printLog(pw, "Re Encrypted to:" + encData);
						if (commitChanges) {
							next.setUiData(encData);
						}
					}
				}
			} catch (Exception e) {
				printLog(pw, "UI Data Failed with:" + e.getMessage());
			}
			printLog(pw, "************ PAYMENT DATA [ID=" + next.getId()
					+ "]*******************");
			try {
				paymentData = next.getPaymentData();
				printLog(pw, "[Input         ]:" + paymentData);
				if (paymentData != null) {
					decPaymentData = this.decryptPaymentData(paymentData);
					printLog(pw, "[Decrypted     ]:" + decPaymentData);
					if (decPaymentData != null) {
						encPaymentData = this
								.encryptJaonWithNew(decPaymentData);
						printLog(pw, "[Re-Encrypted to]:" + encPaymentData);
						if (commitChanges) {
							next.setPaymentData(encPaymentData);
						}
					}
				}
			} catch (Exception e) {
				printLog(pw,
						"Payment data processing Failed with:" + e.getMessage());
			}
			pw.flush();
			pw.close();
			if (commitChanges) {
				manager.merge(next);
			}
		}
		return outputFile.getAbsolutePath();
	}
	
	private String testAESD2CEnrollments() throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String paymentData = null;
		String decPaymentData = null;
		String decData = null;
		File outputFile = new File("D2C_Enrollment_AES_Test_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		
		List<D2CEnrollment> resultList = new ArrayList<D2CEnrollment>();
		int offset = 0;
		do {
			 resultList = manager.createQuery( "Select a From D2CEnrollment a", D2CEnrollment.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			 for (D2CEnrollment next : resultList) {
				 printLog(pw, "************ D2C  [ID=" + next.getId()	+ "] *******************");
				 System.out.println("************ D2C  [ID=" + next.getId()	+ "] *******************");
				 try {
						data = next.getUiData();// Expected to be encrypted
						printLog(pw, "[Input         ]:" + data);
						if (data != null) {
							decData = ghixAESCipherPoolObj.decrypt(data);
							printLog(pw, "[Decrypted     ]:" + decData);
							printLog(pw, "[UI DATA Status     ]: PASSED");
						}
					} catch (Exception e) {
						printLog(pw, "UI Data Failed with:" + e.getMessage());
						System.out.println("UI DATA ENCRYPTED : FAILED");
					}
					try {
						paymentData = next.getPaymentData();
						printLog(pw, "[Input          ]:" + paymentData);
						if (paymentData != null) {
							decPaymentData = ghixAESCipherPoolObj.decrypt(paymentData);
							printLog(pw, "[Decrypted    ]:" + decPaymentData);
							printLog(pw, "[PAYMENT DATA Status     ]: PASSED");
						}
					} catch (Exception e) {
						printLog(pw,"Payment data processing Failed with:" + e.getMessage());
						System.out.println("PAYMENT DATA ENCRYPTED : FAILED");
					}
			 }
			 offset +=10; 
			 pw.flush();
		} while (resultList.size() > 0);
		
		pw.close();
		return outputFile.getAbsolutePath();
		
	}
	
	private String testD2CEnrollments() throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String paymentData = null;
		String decPaymentData = null;
		String decData = null;
		List<D2CEnrollment> resultList = manager.createQuery(
				"Select a From D2CEnrollment a", D2CEnrollment.class)
				.getResultList();
		File outputFile = new File("D2C_Enrollment_Test_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		for (D2CEnrollment next : resultList) {
			printLog(
					pw,
					"*************** DECRYPTION TEST: UI DATA [ID="
							+ next.getId() + "] ****************");
			try {
				data = next.getUiData();// Expected to be encrypted
				printLog(pw, "[Input         ]:" + data);
				if (data != null) {
					decData = this.decryptDataNew(data);
					printLog(pw, "[Decrypted     ]:" + decData);
				}
			} catch (Exception e) {
				printLog(pw, "UI Data Failed with:" + e.getMessage());
			}
			printLog(pw, "*************** DECRYPTION TEST: PAYMENT DATA [ID="
					+ next.getId() + "] ****************");
			try {
				paymentData = next.getPaymentData();
				printLog(pw, "[Input          ]:" + paymentData);
				if (paymentData != null) {
					decPaymentData = this.decryptDataNew(paymentData);
					printLog(pw, "[Decrypted    ]:" + decPaymentData);
				}
			} catch (Exception e) {
				printLog(pw,
						"Payment data processing Failed with:" + e.getMessage());
			}
		}
		pw.flush();
		pw.close();
		return outputFile.getAbsolutePath();
	}

	private String markBadD2CEnrollments() throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String paymentData = null;
		String decPaymentData = null;
		String decData = null;
		List<D2CEnrollment> resultList = manager.createQuery(
				"Select a From D2CEnrollment a", D2CEnrollment.class)
				.getResultList();
		File outputFile = new File("D2C_Enrollment_Test_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		for (D2CEnrollment next : resultList) {
			boolean updated = false;
			printLog(pw,
					"*************** DECRYPTION : UI DATA [ID=" + next.getId()
							+ "] ****************");
			try {
				data = next.getUiData();// Expected to be encrypted
				printLog(pw, "[Input         ]:" + data);
				if (data != null) {
					decData = this.decryptDataNew(data);
					printLog(pw, "[Decrypted     ]:" + decData);
				}
			} catch (Exception e) {
				updated = true;
				next.setUiData(null);
				printLog(pw,
						"Bad record , setting UIData to null:" + e.getMessage());
			}
			printLog(pw, "*************** DECRYPTION : PAYMENT DATA [ID="
					+ next.getId() + "] ****************");
			try {
				paymentData = next.getPaymentData();
				printLog(pw, "[Input          ]:" + paymentData);
				if (paymentData != null) {
					decPaymentData = this.decryptDataNew(paymentData);
					printLog(pw, "[Decrypted    ]:" + decPaymentData);
				}
			} catch (Exception e) {
				updated = true;
				next.setPaymentData(null);
				printLog(pw,
						"Bad Payment data setting it to null:" + e.getMessage());
			}
			if (updated) {
				manager.merge(next);
			}
		}
		pw.flush();
		pw.close();
		return outputFile.getAbsolutePath();
	}

	private void printLog(PrintWriter pw, String str) {
		pw.println(str);
		pw.flush();
		// System.out.println(str);
	}

	private String decryptDataNew(String data) throws Exception {
		return ghixSymetricPooledCipherObj.decrypt(data);
	}

	private String decryptPaymentData(String paymentData) {
		if (paymentData == null) {
			return paymentData;
		}
		return this.oldEncryptor.decryptStringByJasypt(paymentData);
	}
	


	private String migrateSecurables(boolean commit)
			throws FileNotFoundException {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		String data = null;
		String encData = null;
		String decData = null;
		boolean updated = false;
		File outputFile = new File("Securables_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		List<Securables> resultList = manager.createQuery(
				"Select a From Securables a", Securables.class).getResultList();
		for (Securables record : resultList) {
			printLog(pw, "********** SECURABLE PASSWORD [ID=" + record.getId()
					+ "]************");
			updated = false;
			data = record.getPassword();
			try {
				if (data == null) {
					printLog(pw, "No Password found");
				} else {
					// printLog(pw,"*** Data is encrypted, decrypting now");
					printLog(pw, "[Input          ]:" + data);
					decData = this.oldEncryptor.decryptStringByJasypt(data);
					encData = ghixSymetricPooledCipherObj.encrypt(decData);
					printLog(pw, "[Decrypted      ]:" + decData);
					printLog(pw, "[Re-Encrypted To]:" + encData);
					if (commit) {
						record.setPassword(encData);
						updated = true;
					}
				}
			} catch (Exception e) {
				printLog(pw, "Failed with:" + e.getMessage());
			}
			if (updated) {
				printLog(pw, "Merging the Enrollment record");
				manager.merge(record);
			}
		}
		pw.close();
		return outputFile.getAbsolutePath();
	}

	@SuppressWarnings("unused")
	private void testSecurables() throws FileNotFoundException {
		String data = null;
		String decData = null;
		boolean updated = false;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMYYYY_HH_mm");
		Date dt = new Date(System.currentTimeMillis());
		File outputFile = new File("Securables_Test_" + sdf.format(dt));
		if (outputFile.exists()) {
			outputFile.delete();
		}
		PrintWriter pw = new PrintWriter(outputFile);
		List<Securables> resultList = manager.createQuery(
				"Select a From Securables a", Securables.class).getResultList();
		printLog(pw, "# of Securables record:" + resultList.size());
		for (Securables next : resultList) {
			printLog(pw, "********** Record ID:" + next.getId()
					+ " ***************");
			updated = false;
			data = next.getPassword();
			try {
				if (data == null) {
					printLog(pw, "Password data is null");
				} else {
					printLog(pw, "DATA:" + data);
					data = ghixSymetricPooledCipherObj.decrypt(data);
					printLog(pw, "DECRYPTED Password:" + data);
				}
			} catch (Exception e) {
				printLog(pw, "Decryption Failed :" + e.getMessage());
			}
		}
		pw.close();
	}

}
