package com.getinsured.hix.platform.tools.encryption.jpa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;


import com.getinsured.hix.platform.tools.encryption.domain.D2CEnrollment;
import com.getinsured.hix.platform.tools.encryption.domain.IssuerPaymentInformation;
import com.getinsured.hix.platform.tools.encryption.domain.Securables;
import com.getinsured.hix.platform.util.Encryption;
import com.getinsured.hix.platform.util.GhixSymetricPooledCipher;
import com.getinsured.hix.platform.util.GhixAESCipherPoolNew;
import com.getinsured.hix.platform.util.GhixAESCipherPool;



public class AESEncryptionMigrator {
	private static final String MIGRATE_TO_AES = "MIGRATE_TO_AES";
	private static final String KEY_ROTATION = "KEY_ROTATION";
	
	private EntityManager manager;
	private EntityManager testManager;
	
	private String action;
	
	private Encryption migrateFrom;
	private Encryption migrateTo;
	
	private PrintWriter encryptionLogger;
	private File outputFile;

	public AESEncryptionMigrator() {
		try {
				outputFile = new File("AESEncryptionMigrator.log");
				if (outputFile.exists()) { outputFile.delete(); }
				this.encryptionLogger = new PrintWriter(outputFile);
			} catch (FileNotFoundException e) {
				System.err.println("Error while setting up logger"+ e.getMessage());
				System.exit(0);
			}
		
		this.processAction() .createEntityManager() .doAction();
		this.encryptionLogger.flush();
		this.encryptionLogger.close();
		
		
	} 
	
	private AESEncryptionMigrator processAction() {
		String action = (System.getProperty("ACTION") != null) ? System.getProperty("ACTION").toUpperCase() : "";
		
		switch (action) {
			case MIGRATE_TO_AES:
					this.action = "MIGRATE_TO_AES";
					this.migrateFrom = new GhixSymetricPooledCipher();
					this.migrateTo = new GhixAESCipherPool();
			break;
			case KEY_ROTATION:
				this.action = "KEY_ROTATION";
				 String currentKey = System.getProperty("PASSWORD_KEY");
				 String newKey = System.getProperty("PASSWORD_KEY_NEW");
				if(currentKey == null || newKey == null) {
					System.out.println("Key rotation not possible without currentKey & newKey." );
					System.exit(0);
				}
				
				this.migrateFrom = new GhixAESCipherPool();
				this.migrateTo = new GhixAESCipherPoolNew();
			break;
			default:
				System.out.println("Nothing to do." );
				System.exit(0);
			break;
		}
		
		return this;
	}
	
	private AESEncryptionMigrator createEntityManager() {
		HashMap<String, String> dbParameters = new HashMap<String, String>();
		try {
			dbParameters.put("javax.persistence.jdbc.driver",   "oracle.jdbc.OracleDriver"); 
			dbParameters.put("javax.persistence.jdbc.url",      System.getProperty("dbUrl")); 
			dbParameters.put("javax.persistence.jdbc.user",     System.getProperty("dbUser"));
			dbParameters.put("javax.persistence.jdbc.password", System.getProperty("dbPassword"));
			
			/*dbParameters.put("javax.persistence.jdbc.driver",   "oracle.jdbc.OracleDriver"); 
			dbParameters.put("javax.persistence.jdbc.url",      "jdbc:oracle:thin:@oracle6db.ghixqa.com:1521/ghixdb"); 
			dbParameters.put("javax.persistence.jdbc.user",     "iexdev");
			dbParameters.put("javax.persistence.jdbc.password", "Mzc1YzE2NDM2YTgy");*/
		
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("encryptionUnit", dbParameters);
			this.manager =  factory.createEntityManager();
			this.testManager = factory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Failed to create Entity manager, Message:" + e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
		return this;
	}
	
	private void doAction() {
		switch (this.action) {
			case MIGRATE_TO_AES:
			case KEY_ROTATION:
				  this.doMigration();
			break;
			default:
				System.out.println("Nothing to do." );
				System.exit(0);
			break;			  
		}
		
	}

	private void doMigration() {
		//backup the tables;
		this.doBackup();
		
		boolean migrationD2CDone = this.migrateAESD2CEnrollments();
		if(migrationD2CDone) {
			if( testAESD2CEnrollments() ){
				System.out.println("Verification of D2C Enrollment Data ended ..." );
				encryptionLogger.println( "Verification of D2C Enrollment Data ended ...");
				encryptionLogger.flush();
			}else{
				System.out.println("Migrating D2C Enrollment Data Failed Check log ."+outputFile.getAbsolutePath() );
				encryptionLogger.flush();
				System.exit(0);
			}
		}
		
		boolean migrationSecurablesDone = this.migrateAESSecurables();
		if(migrationSecurablesDone){
			if( testAESSecurables() ){
				System.out.println("Verification of Securables Data ended ..." );
				encryptionLogger.println( "Verification of Securables Data ended ...");
				encryptionLogger.flush();
			}else{
				System.out.println("Migrating Securables Data Failed Check log ."+outputFile.getAbsolutePath() );
				encryptionLogger.flush();
				System.exit(0);
			}
		}
		
		
		boolean migrationIssuerPaymentDone = this.migrateAESIssuerPaymentInfo();
		if(migrationIssuerPaymentDone){
			if( testAESIssuerPaymentInfo() ){
				System.out.println("Verification of Issuer Payment Info Data ended ..." );
				encryptionLogger.println( "Verification of Issuer Payment Info Data ended ...");
				encryptionLogger.flush();
			}else{
				System.out.println("Migrating Issuer Payment Info Data Failed Check log ."+outputFile.getAbsolutePath() );
				encryptionLogger.flush();
				System.exit(0);
			}
		}
	}
	
	private boolean migrateAESD2CEnrollments() {
		int offset =0;
		String data = null;
		String paymentData = null;
		String encPaymentData = null;
		String decPaymentData = null;
		String encData = null;
		String decData = null;
		Long D2C_count = null;
		
		try{
			CriteriaBuilder qb = manager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = qb.createQuery(Long.class);
			cq.select(qb.count(cq.from(D2CEnrollment.class)));
			D2C_count = manager.createQuery(cq).getSingleResult();
		}catch(Exception e){
			System.out.println("Table D2C Enrollment Failed to read :" +e.getMessage());
			encryptionLogger.println("Table D2C Enrollment Failed to read :" +e.getMessage());
			
			System.out.println("Skipping D2C Enrollment migration. :" );
			encryptionLogger.println("Skipping D2C Enrollment migration.");
			return false;
		}
		
		encryptionLogger.println("Migrating D2C Enrollment Data Record count:" +D2C_count);
		System.out.println("Migrating D2C Enrollment Data Record count:" +D2C_count);
		
		List<D2CEnrollment> resultList = new ArrayList<D2CEnrollment>();
		EntityTransaction tx = manager.getTransaction();
		do {
			 resultList = manager.createQuery( "Select a From D2CEnrollment a", D2CEnrollment.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			 for (D2CEnrollment next : resultList) {
					tx.begin();
					try {
						data = next.getUiData();// Expected to be encrypted
						if (data != null &&  ! data.startsWith("{")) {
							decData = migrateFrom.decrypt(data);
							if (decData != null && !decData.isEmpty()) {
								encData = migrateTo.encrypt(decData);
								next.setUiData(encData);
							}
						}
					} catch (Exception e) {
						encryptionLogger.println("************ D2C  [ID=" + next.getId()	+ "] *******************");
						encryptionLogger.println( "UI Data Failed with:" + e.getMessage());
					}
					
					try {
						paymentData = next.getPaymentData();
						if (paymentData != null) {
							decPaymentData = migrateFrom.decrypt(paymentData);
							if (decPaymentData != null && !decPaymentData.isEmpty()) {
								encPaymentData = migrateTo.encrypt(decPaymentData);
								next.setPaymentData(encPaymentData);
							}
						}
					} catch (Exception e) {
						encryptionLogger.println("************ D2C  [ID=" + next.getId()	+ "] *******************");
						encryptionLogger.println( "Payment data processing Failed with:" + e.getMessage());
					}
					
					manager.merge(next);
					try {
						tx.commit();
					} catch (Exception e) {
						encryptionLogger.println("************ D2C  [ID=" + next.getId()	+ "] *******************");
						encryptionLogger.println( "Failed to Update D2CEnrollment :" + e.getMessage());
					}
			 }
			 offset +=10; 
			 encryptionLogger.flush();
		}while (resultList.size() > 0);
		
		encryptionLogger.println("Migrating D2C Enrollment Data  Ended");
		System.out.println("Migrating D2C Enrollment Data  Ended...");
		
		encryptionLogger.flush();
		return true;
	}
	
	private boolean testAESD2CEnrollments()  {
		String data = null;
		String paymentData = null;
		String decData = null;
		List<D2CEnrollment> resultList = new ArrayList<D2CEnrollment>();
		int offset = 0;
		do {
			 resultList = testManager.createQuery( "Select a From D2CEnrollment a", D2CEnrollment.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			 for (D2CEnrollment next : resultList) {
				 try {
						data = next.getUiData();// Expected to be encrypted
						if (data != null) {
							decData = migrateTo.decrypt(data);
							if(!(decData != null && !decData.isEmpty())) {
								 throw new Exception("Decryption failed");
							}
						}
					} catch (Exception e) {
						encryptionLogger.println( "D2CEnrollment ID :" + next.getId());
						encryptionLogger.println( "Migration Failed D2CEnrollment UI data :" + e.getMessage());
						//return false;
					}
					try {
						paymentData = next.getPaymentData();
						if (paymentData != null) {
							decData = migrateTo.decrypt(paymentData);
							if(!(decData != null && !decData.isEmpty())) {
								 throw new Exception("Decryption failed");
							}
						}
					} catch (Exception e) {
						encryptionLogger.println( "D2CEnrollment ID :" + next.getId());
						encryptionLogger.println( "Migration Failed D2CEnrollment payment data :" + e.getMessage());
						//return false;
					}
			 }
			 offset +=10; 
		} while (resultList.size() > 0);
		return true;
	}
	
	private boolean migrateAESSecurables() {
		String data = null;
		String encData = null;
		String decData = null;
		Long Securables_count = null;
		try{
			CriteriaBuilder qb = manager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = qb.createQuery(Long.class);
			cq.select(qb.count(cq.from(Securables.class)));
			Securables_count = manager.createQuery(cq).getSingleResult();
		}catch(Exception e){
			System.out.println("Table Securables Failed to read :" +e.getMessage());
			encryptionLogger.println("Table Securables Failed to read :" +e.getMessage());
			
			System.out.println("Skipping Securables migration. :" );
			encryptionLogger.println("Skipping Securables migration.");
			return false;
		}
		
		encryptionLogger.println("Migrating Securables Data Record count:" +Securables_count);
		System.out.println("Migrating Securables Data Record count:" +Securables_count);
		
		List<Securables> resultList = new ArrayList<Securables>();
		EntityTransaction tx = manager.getTransaction();
		int offset = 0;
		do {
			 resultList = manager.createQuery( "Select a From Securables a", Securables.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			 for (Securables record : resultList) {
				data = record.getPassword();
				tx.begin();
				try {
					if (data != null) {
						decData = migrateFrom.decrypt(data);
						if (decData != null && !decData.isEmpty()) {
							encData = migrateTo.encrypt(decData);
							record.setPassword(encData);
						}
					}
				} catch (Exception e) {
					encryptionLogger.println("************ SECURABLE  [ID=" + record.getId()	+ "] *******************");
					encryptionLogger.println( "Password Encryption Failed with:" + e.getMessage());
				}
				
				manager.merge(record);
				
				try {
					tx.commit();
				} catch (Exception e) {
					encryptionLogger.println("************ SECURABLE  [ID=" + record.getId()	+ "] *******************");
					encryptionLogger.println( "Failed to Update Securables :" + e.getMessage());
				}
			 }
			 offset +=10; 
			 encryptionLogger.flush();
		}while (resultList.size() > 0);
		
		encryptionLogger.println("Migrating Securables Data Ended");
		System.out.println("Migrating Securables Data Ended...");
		encryptionLogger.flush();
		return true;
	}
	
	private boolean testAESSecurables() {
		String data = null;
		String decData = null;
		List<Securables> resultList = new ArrayList<Securables>();
		int offset = 0;
		do {
			 resultList = testManager.createQuery( "Select a From Securables a", Securables.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
			for (Securables next : resultList) {
				data = next.getPassword();
				try {
					if (data != null) {
						decData = migrateTo.decrypt(data);
						if(!(decData != null && !decData.isEmpty())) {
							 throw new Exception("Decryption failed");
						}
					}
				} catch (Exception e) {
					encryptionLogger.println( "Securables ID :" + next.getId());
					encryptionLogger.println( "Migration Failed Securables Password :" + e.getMessage());
					//return false;
				}
			}
			offset +=10; 
		} while (resultList.size() > 0);
		return true;
	}
	
	private boolean migrateAESIssuerPaymentInfo() {
		String data = null;
		String encData = null;
		String decData = null;
		int offset = 0;
		Long IssuerPaymentInformation_count = null;
		
		try{
			CriteriaBuilder qb = manager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = qb.createQuery(Long.class);
			cq.select(qb.count(cq.from(IssuerPaymentInformation.class)));
			IssuerPaymentInformation_count = manager.createQuery(cq).getSingleResult();
		}catch(Exception e){
			System.out.println("Table IssuerPaymentInformation Failed to read :" +e.getMessage());
			encryptionLogger.println("Table IssuerPaymentInformation Failed to read :" +e.getMessage());
			
			System.out.println("Skipping IssuerPaymentInformation migration. :" );
			encryptionLogger.println("Skipping IssuerPaymentInformation migration.");
			return false;
		}
		
		encryptionLogger.println("Migrating Issuer Payment Information Data Record count:" +IssuerPaymentInformation_count);
		System.out.println("Migrating Issuer Payment Information Data Record count:" +IssuerPaymentInformation_count);
		
		List<IssuerPaymentInformation> resultList = new ArrayList<IssuerPaymentInformation>();
		EntityTransaction tx = manager.getTransaction();

		do {
			 resultList = manager.createQuery( "Select a From IssuerPaymentInformation a", IssuerPaymentInformation.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
		
			 for (IssuerPaymentInformation next : resultList) {
				tx.begin();	
				 try {
					 	data = next.getPassword();// Expected to be encrypted	
					 	if (data != null && !data.startsWith("{")) {
							decData = migrateFrom.decrypt(data);
							if (decData != null && !decData.isEmpty()) {
								encData = migrateTo.encrypt(decData);
								next.setPassword(encData);
							}
						}
					} catch (Exception e) {
						encryptionLogger.println("************ Issuer Payment Information  [ID=" + next.getId()	+ "] *******************");
						encryptionLogger.println( "Password Encryption Failed with:" + e.getMessage());
					}
					
				try {
						data = null;decData = null;encData=null;
						data = next.getPasswordSecuredKey();// Expected to be encrypted
						if (data != null && !data.startsWith("{")) {
							decData = migrateFrom.decrypt(data);
							if (decData != null && !decData.isEmpty()) {
								encData = migrateTo.encrypt(decData);
								next.setPasswordSecuredKey(encData);
							}
						}
					} catch (Exception e) {
						encryptionLogger.println("************ Issuer Payment Information  [ID=" + next.getId()	+ "] *******************");
						encryptionLogger.println( "Password Key Encryption Failed with:" + e.getMessage());
					}
				
				manager.merge(next);
				try {
					tx.commit();
				} catch (Exception e) {
					encryptionLogger.println("************  Issuer Payment Information   [ID=" + next.getId()	+ "] *******************");
					encryptionLogger.println( "Failed to Update  Issuer Payment Information  :" + e.getMessage());
				}
			 }
			 offset +=10; 
			 encryptionLogger.flush();
			}while (resultList.size() > 0);
		
		encryptionLogger.println("Migrating Issuer Payment Information Ended");
		System.out.println("Migrating Issuer Payment Information Ended...");
		encryptionLogger.flush();
		return true;
	}
	
	private boolean testAESIssuerPaymentInfo() {
		String data = null;
		String decData = null;
		int offset = 0;
		
		List<IssuerPaymentInformation> resultList = new ArrayList<IssuerPaymentInformation>();
		
		do {
			 resultList = testManager.createQuery( "Select a From IssuerPaymentInformation a", IssuerPaymentInformation.class)
					 			 .setFirstResult(offset) 
					             .setMaxResults(10)
					             .getResultList();
		
			 for (IssuerPaymentInformation next : resultList) {
				try {
						data = next.getPassword();// Expected to be encrypted
						if (data != null && !data.startsWith("{")) {
							 decData = migrateTo.decrypt(data);
							 if(!(decData != null && !decData.isEmpty())) {
								 throw new Exception("Decryption failed");
							 }
						}
					} catch (Exception e) {
						encryptionLogger.println( "Issuer Payment Information ID :" + next.getId());
						encryptionLogger.println( "Migration Failed IssuerPaymentInformation Password :" + e.getMessage());
						//return false;
					}
				try {
						data = next.getPasswordSecuredKey();// Expected to be encrypted
						if (data != null && !data.startsWith("{")) {
							decData = migrateTo.decrypt(data);
							if(!(decData != null && !decData.isEmpty())) {
								 throw new Exception("Decryption failed");
							}
						}
					} catch (Exception e) {
						encryptionLogger.println( "Issuer Payment Information ID :" + next.getId());
						encryptionLogger.println( "Migration Failed IssuerPaymentInformation Password Secured Key :" + e.getMessage());
						//return false;
					}
				 }
				 offset +=10; 
			}while (resultList.size() > 0);
		return true;
	}

	private void doBackup() {
		System.out.println("Taking Backup of tables started..." );
		encryptionLogger.println( "Taking Backup of tables...");
		List<String> backupQueries = new ArrayList<String>();
		backupQueries.add("DROP table D2C_ENROLLMENT_TMP");
		backupQueries.add("create table D2C_ENROLLMENT_TMP as select * from D2C_ENROLLMENT");
		backupQueries.add("DROP table SECURABLES_TMP");
		backupQueries.add("create table SECURABLES_TMP as select * from securables");
		backupQueries.add("DROP table ISSUER_PAYMENT_INFO_TMP");
		backupQueries.add("create table ISSUER_PAYMENT_INFO_TMP as select * from ISSUER_PAYMENT_INFO");
		
		EntityTransaction tx = this.manager.getTransaction();
		for (String query : backupQueries) {
			try {
				tx.begin();
				Query q = this.manager.createNativeQuery(query);
				q.executeUpdate();
				tx.commit();
			} catch (Exception e) {
				tx.rollback();
			}
		}
		System.out.println("Taking Backup of tables Ended..." );
		encryptionLogger.println( "Taking Backup of tables Ended...");
		
		if( testBackupProcess() ){
			System.out.println("Verification of Backup tables Ended..." );
			encryptionLogger.println( "Verification of Backup tables Ended...");
		}else{
			System.out.println("Backup Failed more info in log "+outputFile.getAbsolutePath() );
			encryptionLogger.flush();
			System.exit(0);
		}
		encryptionLogger.flush();
	}
	
	private boolean testBackupProcess() {
		List<String> testBackupTables = new ArrayList<String>();
		testBackupTables.add("D2C_ENROLLMENT");
		testBackupTables.add("SECURABLES");
		testBackupTables.add("ISSUER_PAYMENT_INFO");
		
		for (String table : testBackupTables) {
			String sql = " SELECT DECODE(COUNT(*),0,0,1) FROM " +
					"( " +
					"	( select count(*) from "+table+") " +
					"		minus " +
					"	( select count(*) from "+table+"_TMP)" +
					")";
			try {
				Query q = this.manager.createNativeQuery(sql);
				BigDecimal  count =  (BigDecimal) q.getSingleResult();
				if (count.intValueExact() != 0 ){
					throw new Exception("Count not match");
				}
			} 
			catch (PersistenceException e){
				encryptionLogger.println( table+" doesn't exist No backup needed. ");
				System.out.println(table+" doesn't exist No backup needed.");
				continue;
			}
			catch (Exception e) {
				encryptionLogger.println( table+" backup failed "+e.getMessage());
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException {
		new AESEncryptionMigrator();
		
		System.out.println("Migration Done");
		System.exit(0);
		/*String text = "xlimXGbTmEO1_Pvo4hKuA";
		GhixSymetricPooledCipher migrateFrom = new GhixSymetricPooledCipher();
		GhixAESCipherPool migrateTo = new GhixAESCipherPool();
		
		String decData = migrateFrom.decrypt(text);
		String encData = "";
		if (decData != null && !decData.isEmpty()) {
			 encData = migrateTo.encrypt(decData);
		}
		
		System.out.println("Test String :"+text);
		System.out.println("Decoded :"+decData);
		System.out.println("Encoded :"+encData);*/
	}

}
