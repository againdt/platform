package com.getinsured.hix.platform.util.exception;

public class GIRuntimeException extends RuntimeException{

	public GIRuntimeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GIRuntimeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GIRuntimeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GIRuntimeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
