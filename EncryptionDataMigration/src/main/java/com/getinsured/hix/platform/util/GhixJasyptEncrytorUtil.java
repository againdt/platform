package com.getinsured.hix.platform.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;



public class GhixJasyptEncrytorUtil{
	
	
	private final static Logger logger = Logger.getLogger(GhixJasyptEncrytorUtil.class);
	
	private  static final String OUTPUTTYPE ="base64";
	private static final Matcher ENCRYPTEDVALUEMATCHER = Pattern.compile(".*\\$GENC\\[(\\w+)\\].*",Pattern.DOTALL).matcher("");
	private PooledPBEStringEncryptor pooledPBEStringEncryptor ;
	
	
	/**
	 * Instantiate using this constructor if jasypt encryption needed.Configure the bean like 
	 * {@code  <bean id="ghixJasyptUtil" class="com.getinsured.hix.platform.util.GhixJasyptEncryptorUtil" init-method="customInit">
    	<constructor-arg ><bean class="org.jasypt.encryption.pbe.StandardPBEStringEncryptor"/></constructor-arg></bean>}
	 * @author nayak_b
	 * @since 24-Feb-2014
	 * @param pooledPBEStringEncryptor
	 */
	/*public GhixJasyptEncrytorUtil(StandardPBEStringEncryptor standardPBEStringEncryptor){
		logger.info("GhixJasyptEncrytorUtil initiated with StandardPBEStringEncryptor");
		this.standardPBEStringEncryptor = standardPBEStringEncryptor;
	}*/
	
	public GhixJasyptEncrytorUtil(EnvironmentStringPBEConfig environmentStringPBEConfig){
		logger.info("GhixJasyptEncrytorUtil initiated ");
		int poolSize = 200;
		try{
			poolSize = environmentStringPBEConfig.getPoolSize();
		}catch(NullPointerException npe){
			// COnfiguration needs environment property needs to set, if not, it throws NPE
			logger.info("Failed to read the configuration valid for pool size from provided configiration, setting default to 200");
			poolSize = 200;
		}
		this.pooledPBEStringEncryptor = new PooledPBEStringEncryptor();
		this.pooledPBEStringEncryptor.setPoolSize(poolSize);
		this.pooledPBEStringEncryptor.setAlgorithm(environmentStringPBEConfig.getAlgorithm());
		this.pooledPBEStringEncryptor.setPassword(environmentStringPBEConfig.getPasswordEnvName());
		this.pooledPBEStringEncryptor.setStringOutputType(OUTPUTTYPE);
	}
	
	/**
	 * 
	 *  This method will called while instantiating the @link {@link GhixEncryptorUtil} class. Please provie this a init-method 
	 * for any bean injection. e.g init-method="customInit"
	 * @author nayak_b
	 * @since 24-Feb-2014
	 * @throws Exception if it does not found encryptor object.  
	 * 
	 */
	/*public void customInit() {
		logger.info("Loading configuration for Encryptor from properties file.");
		standardPBEStringEncryptor.setAlgorithm(algorithm);
		standardPBEStringEncryptor.setPassword(password);
		if(!StringUtils.isBlank(outputType)){
			standardPBEStringEncryptor.setStringOutputType(outputType);
		}
	}*/
	
	public GhixJasyptEncrytorUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This will throw exception if the configuration used for encryption was not 
	 * configured to decrypt the string. Caller need to handle the Exception
	 * @author Biswakalyan
	 * @since 24-Feb-2014
	 * @throws Exception
	 * @param encryptedValue
	 * @return string after decrypting the given value. 
	 */
	public String decryptStringByJasypt(String encryptedValue){
		return pooledPBEStringEncryptor.decrypt(encryptedValue);
	}
	
	/**
	 * Use the method  decryptGencStringByJasypt(..) to decrypt the data that
	 * encapsulated between '$GNC[' and ']'.  This method is useful if the data can not be
	 * decrypted at filter level, for example the 'name' attribute of any form element can not be
	 * decrypted at filter lavel.
	 * If the data did not encapsulated by '$GNC[' and ']' then returns null.
	 * Caller need to handle the Exception
	 * @author Venkata Tadepalli
	 * @since  16-Sept-2014
	 * @throws Exception
	 * @param gencEncryptedValue
	 * @return string after decrypting the given value or null if does not match. 
	 */
	public String decryptGencStringByJasypt(String gencEncryptedValue){
		
		String decValue=null;
		if(ENCRYPTEDVALUEMATCHER.reset(gencEncryptedValue).matches()){
			decValue = decryptStringByJasypt(ENCRYPTEDVALUEMATCHER.group(1));
		}
		
		return decValue;
	}
	
	/**
	 * This will throw exception if the configuration used for encryption was not 
	 * configured properly.
	 * @author Biswakalyan
	 * @since 24-Feb-2014
	 * @throws Exception
	 * @param textValue to encrypt.
	 * @return string after encrypting the given value. 
	 */
	public  String encryptStringByJasypt(String textValue){
		return pooledPBEStringEncryptor.encrypt(textValue);
	}
	
	public static void main(String[] args) {
		System.setProperty("PASSWORD_KEY","ghix123");
		EnvironmentStringPBEConfig environmentStringPBEConfig = new EnvironmentStringPBEConfig();
		environmentStringPBEConfig.setAlgorithm("PBEWithMD5AndDES");
		environmentStringPBEConfig.setPasswordEnvName("PASSWORD_KEY");
		environmentStringPBEConfig.setPoolSize(100);
		System.out.println(new GhixJasyptEncrytorUtil(environmentStringPBEConfig).encryptStringByJasypt("employee"));
		
	}

}
