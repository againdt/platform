package com.getinsured.hix.platform.tools.encryption.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="ISSUER_PAYMENT_INFO")
public class IssuerPaymentInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ISSUER_PAYMENT_INFO_SEQ")
	@SequenceGenerator(name = "ISSUER_PAYMENT_INFO_SEQ", sequenceName = "ISSUER_PAYMENT_INFO_SEQ", allocationSize = 1)
	private int id;

	
	@Column(name = "KEY_STORE_FILE_LOC")
	private String keyStoreFileLocation;
	
	@Column(name = "PRIVATE_KEY_NAME")
	private String privateKeyName;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "PASSWORD_SEC_KEY")
	private String passwordSecuredKey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordSecuredKey() {
		return passwordSecuredKey;
	}

	public void setPasswordSecuredKey(String passwordSecuredKey) {
		this.passwordSecuredKey = passwordSecuredKey;
	}
	
	
}
