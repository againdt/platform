package com.getinsured.hix.platform.util;

/**
 * This class provides a convenient and efficient way to encrypt data using DES and TripleDES algorithms
 * 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.EmptyStackException;
import java.util.Stack;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;


public class GhixSymetricPooledCipher implements Encryption{
	private static final String saltBytesStr = "%$dfkht^*jj";
	private static final int KEY_ITERATIONS=1000;
	public static final String FORMAT = "yyyy-MMM-dd HH:mm:ss zzz"; 
	private static Logger logger = LoggerFactory.getLogger(GhixSymetricPooledCipher.class);
	private static final String CIPHER_ALGO = "PBEWithMD5AndDES";
	private static SecretKeySpec secret = null;
	//Encryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeEncryptorPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyEncryptorPool = new Stack<Cipher>();
	
	//Decryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeDecryptionPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyDecryptionPool = new Stack<Cipher>();
	private static SecureRandom random  = new SecureRandom();
	private static final int POOL_SIZE = 100;
	private static boolean poolInitialized = false;
	private static byte[] salt = new byte[8];
	
	public String decrypt(String base64EncodedEncString) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException{
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE);
		}
		boolean onDemand = false;
		int blockSize = -1;
		byte[] cipherTextToken = Base64.decodeBase64(base64EncodedEncString);
		Cipher cipher = null;
		cipher = getDecryptionCipher();
		if(cipher == null){
			onDemand = true;
			cipher = initializeDecryptionCipherOnDemand();
		}
		blockSize = cipher.getBlockSize();
		byte[] decipheredBytes = cipher.doFinal(cipherTextToken);
		byte[] actualBytes = new byte[decipheredBytes.length-blockSize];
		System.arraycopy(decipheredBytes, 8, actualBytes, 0, decipheredBytes.length-blockSize);
		String plaintext = new String(actualBytes, "UTF-8");
		if(!onDemand){
			standbyDecryptionPool.push(cipher); // Return this cipher to the pool
		}
		return plaintext;
	}
	
	
	private static synchronized void initializCipherPools(int poolSize) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchProviderException{
		if(poolInitialized){
			return;
		}
		String password = ( System.getProperty("PASSWORD_KEY") != null) ? System.getProperty("PASSWORD_KEY") : System.getenv("PASSWORD_KEY");
		if(password == null){
			throw new RuntimeException("No password env found, expected \"PASSWORD_KEY\"");
		}
		
		Cipher cipher = null;
		PBEParameterSpec pbeParameterSpec = null;
		byte[] saltBytes = saltBytesStr.getBytes();
		System.arraycopy(saltBytes, 0, salt, 0, 8);
		SecretKeyFactory factory = SecretKeyFactory.getInstance(CIPHER_ALGO);
		KeySpec spec = new PBEKeySpec(password.toCharArray());
		SecretKey tmp = factory.generateSecret(spec);
		pbeParameterSpec = new PBEParameterSpec(salt, KEY_ITERATIONS);
		secret = new SecretKeySpec(tmp.getEncoded(), CIPHER_ALGO);
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance(CIPHER_ALGO);
			cipher.init(Cipher.ENCRYPT_MODE, secret,pbeParameterSpec);
			activeEncryptorPool.add(cipher);
		}
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance(CIPHER_ALGO);
			cipher.init(Cipher.DECRYPT_MODE, secret, pbeParameterSpec);
			activeDecryptionPool.add(cipher);
		}
		poolInitialized = true;
	}
	
	private static Cipher initializeDecryptionCipherOnDemand() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new GIRuntimeException("Decryption Environment: Fatal Error, pools should have been initialized by now");
		}
		logger.warn("Initializing Decryption cipher on demand, consider increasing pool size ["+POOL_SIZE+"]");
		Cipher cipher = null;
		cipher = Cipher.getInstance(CIPHER_ALGO);
		PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, KEY_ITERATIONS);
		cipher.init(Cipher.DECRYPT_MODE, secret, pbeParameterSpec);
		return cipher;
	}
	
	private static synchronized Cipher initializeEncryptionCipherOnDemand() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new GIRuntimeException("Encryption Environment: Fatal Error, pools should have been initialized by now");
		}
		logger.warn("Initializing Encryption cipher on demand, consider increasing pool size ["+POOL_SIZE+"]");
		Cipher cipher = null;
		cipher = Cipher.getInstance(CIPHER_ALGO);
		PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, KEY_ITERATIONS);
		cipher.init(Cipher.ENCRYPT_MODE, secret,pbeParameterSpec);
		return cipher;
	}
	
	private static synchronized Cipher getEncryptionCipher(){
		Cipher cipher = null;
		Stack<Cipher> emptyBank;
		try{
			cipher = activeEncryptorPool.pop();
		}catch(EmptyStackException es){
			logger.debug("Switching to Standby encryption pool [Current Active Pool:"+activeEncryptorPool.size()+"], StandBy Pool:"+standbyEncryptorPool.size());
			emptyBank = activeEncryptorPool;
			activeEncryptorPool = standbyEncryptorPool;
			standbyEncryptorPool = emptyBank;
		}
		return cipher;
	}
	
	private static synchronized Cipher getDecryptionCipher(){
		Stack<Cipher> emptyBank;
		Cipher cipher = null;
		try{
			cipher = activeDecryptionPool.pop();
		}catch(EmptyStackException es){
			logger.debug("Switching to Standby decryption pool exception encountered, StandBy count:"+standbyDecryptionPool.size());
			emptyBank = activeDecryptionPool;
			activeDecryptionPool = standbyDecryptionPool;
			standbyDecryptionPool = emptyBank;
		}
		return cipher;
	}
	
	/**
	 * 
	 * @param password
	 * @param trustedEntityName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchProviderException 
	 */
	public String encrypt(String strToBeEncrypted) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException{
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE);
		}
		boolean onDemand = false;
		Cipher cipher = null;
		cipher = getEncryptionCipher();
		if(cipher == null){
			onDemand = true;
			cipher = initializeEncryptionCipherOnDemand();
		}
		byte[] randomizer = new byte[8];
		random.nextBytes(randomizer);
		byte[] actualBytes = strToBeEncrypted.getBytes("UTF-8");
		byte[] inputBytes = new byte[actualBytes.length+8];
		System.arraycopy(randomizer, 0, inputBytes, 0, randomizer.length);
		System.arraycopy(actualBytes,0 , inputBytes, randomizer.length, actualBytes.length);
		
		// Add a random to ensure uniqueness of the token across the JVM's in a culstered environment
		byte[] ciphertext = cipher.doFinal(inputBytes);
		if(!onDemand){
			standbyEncryptorPool.push(cipher); //Send the cipher environment to standby pool
		}
		return new String(Base64.encodeBase64URLSafe(ciphertext));
	}
	
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException, IOException{
		String testString = "V12pNXqk8NWqnkMg9UlnVmPR2ZzfEAn7YcJXyPSj8TtyNbMMLey9SdQL2h_34bbB_tetlouAf8XCyhfZuIefK4NnUW-IgFXVDCaHzBf2rQk3w6RC-IgPunZ1a0cYZfU3le0sD8Dl3qxUnOvjn4HHxEPnJg4SS2GgJuKCzCYGxUakdbCZVOZejDyrTXoQS4y3KmGtvnbjZMWgKeiM1EO7SpbU3-A5kFfR51BjQ35le6WHnQZQ2FL75wjuzCQ2Kkw8iAiEGER817iMsq_GsgVg4uOj-1b9WYt8grR9kY8De_fjtfOnR7itGaZtEveRfiynNVV_4Ot-zPaW-XtKOst5CNoEHTLPkGwEIMz3T2mhsjMnUoXdY8jPWl6D3mICBYTYj9lcVZ1R61X9r1Ry2l1XCgszRFZfboHGtyLn2zAbIOQdyuP3DD6ewjNKQeuj6in-X9ubQyjidHoq9bff5nHidJRudJxCRl5pl90rhvriZmY4jBeJkQFF5aO0ULrXd77oWn733-JgTdf5Aoq5P8i0NC3W9OTl8RvWms_he2ck99Rua1PWNo993QfrgnP6YlR6GNUDJAsFD6yhZHnGY7amwPhR0peCY9iD-W4YhCtoB7dCyy_GWktU-SIpyno8g07uiC8t1M9k4QErEsn7bm6k9QrH5iikXLxn043g8noXRVHlIUALNxV96_myN-p1vTTQZ3W2tRF46KxgOY2S2Xf6RYnJyARasPEgppb8I7UK0gLCmfuhmghYhRGl_uWalfBQQCRL8nQMqxXEWqellsjEJliIZbS-O-qA-vAWSPOmJsb6n92-i0V6MaWyjjQ6iV5ciyz2vWb1sxPx9lS9OXYJNd_KQsNe7-9JqfjsAj5eZ_u6JRr2uJD4So5KWL0s-MfqDqLVOfuPdjeIHIhif_aJFLrvtUOpKZTIm6NRur55QMb3UGiE3Wxvno6P9lXlNxoAzRPhchUBNP8jr-SCelJy_pEijtLH6h4R72OCXmAcQWBYet6Hq8GLeVw7T1QbdTKG7XODvwBbGm3hIr5aZH25Cg5vOEbJswlC0IrLtPq42D_UHQyMfIAilx2X6vJP0YSWK_uiZIMueT2GLx1r9Jv1B-PfFC4OAZbEcCwimFNaxkgKYIDGUxume4Gu4T3KRa2Z430krAeHdJdpk68tljvNBcEIf4TWGbGLtxAIGF_aDoUREldD8KpvC5to4tWwZPWd1nbHABiBUNdGXvOT7HKW4p4pbaUHi0kyyJTXtlS-Fr44La6a8FlGxUzvaYuEyQNXYyccrCBYioDB85Tc1foYTx_udpcqbDK5JSbB8bu2b220zChub76NXl2kU41-yBgxk0LpcicpaR55rshDWgIxj19tg9DOlR3y73IetMtB57Jf5tB2XZFrYXTPiihIRtuRucy8p_kIADuEOONN5iCaSyOlvwQyzN79GmCX-MqcSof0nFfOP1Xh-urfBsE_iyJytAspZVgVbssNZ_BPIbhD4u1Kp2OYW_D1DzC8IDymOzUcQaG0zitSmR3fQ3YiDEAk-runsEt-_WbHGMaIB9JuirowiJOpDuOY8YPw7YVVBI3wyT1xu-S6XZVBFE8uopccxj8uXjuQo9ovyWxlL9xBn3ju3x-7CU1kjKLF7pXUu7sUGZ1tD-vd4QYYUOENwBrL1uEJJJHIX9GgvJRbcBrvOvkD0wLf5YlB9DoWeQTV_Zc9HVcGF9izCb_jTGoIZHwrsORmRhml1BxT58uq8D62qSCQ3pZPY_yjR-mSTC6SLMdZyhU70p1gme1kuO-bNKTg_TPAis5q5eyhPj5PTRcOuh5DuPrVLXsL1d8h4vwZRfgYRdUmpUKpsIGJKbpvkYdQDIikYVUtnG_PiqCJP8Bpzic6NeofID7D_wIX_YdwH0dLaS9FSX1d9keyKLLC3Kpq959fqQZddIV0tJrRfrWxg_xg9smLoF13zvcJ3Jg2XwCYHY2GPxVb5M-d0-bIAO6__LNWYb_Bnwhsc9ObASlj3WtCcryi_dq2KNV8FSrTtPRKpTnWhNfjKBQzgMi1Gtj_jf6NbmyGMBiZzHoy15Pk9tJncRl0Yc0dhtcZ2ZI11xZSXqDtR9t_2x8BOzS1-sc6R_MR9AZcOeVEI--3Vy1OCUIaNdPYm9hOdkPmqPVfSAIl88MvnWq_KDk--FWpB-gldkR25zUUPD3xBQgPPwynwHv6lYNuWSdbgGtH1mQ4tG_auMbS_tI03VuQI1_xDi8gev45HMcmFw9urlcqwS7YDAmonT3jAy4KBLFZGugasQFeEzSLjio_O7lNOTLozMiieV_hP5yTs8lSHTqJMVMVwL8BqAl29DySGwILm9bTrvLvFlaHigz33xLwfs3pvCN8chPX6z-8XyQ59kr759pa1ijB1xEIxNEBILcMcyZdLZsheZsjiDi_HRGqMYDBglxhBdpTdGBYBhTQ74aUCFontbuv2EplomJ5mTsq6xJ0cuB3rkyT8hUKHNmZhK8jiPbyNrD-ijUW9f-QNWmrnvKoTriigcVSPRCUe2_9Lg4C2VS8i184oI8irtLqfEVIhifDRCIRetba2_vg1kyxWAXPmp3bza0bM8X9aeySEQvwG8BQnMOUhfltN27_x23MnlWQxCK7KiKTa_5B21LgCzKRsltkXWzYG58TsvN2XRagt8umA_nEn1ISG5_TiOwwMjHF6LBqPTHPG78qRpuMq_cS8NoMMHLqDbwHXVnItuttg26PPX-n62qknpu7AloIsnMbUQLWhIS8hVypcK1bcOTzodaaMevEbL2-nbs0alIn-GPL900cPIygdKV48qsQZze1jVrOx9dRhN28AUhMhLFfVNo618G-LklSPcTsQXiQu-TW1tFuTxB-TnANqDkMU-IEGms8-VEVTGFtTWEJUcExHsOFhRmN6O5jkpW4cuC-dUMjuT3JZVz5d24gsu-xqOu8kH4dpyBySZ7w7Q8IjOGcfuesI9XcJnkSLj6LmkQPpBA_w-7eT5nQAcFPF5rWKySMVXaROGm_wqs0BCkFCaMUe30gqqmTI7v1qaPd4SItJ8Kw1IKQPMhjTCcVmmHZNEHL55F5KX8UkIGzk3yHjswRJTBOm6lC9nBz6TkPOYwSi1cXEpR5fjZcO0tZs4qeC1BHfwFqEEdfsd51p2srMUT8ORbf7YDW8gaEsNnqof1PlOc";
		
		Encryption enc = new GhixSymetricPooledCipher();
		
		System.out.println("Decrypted: "+enc.decrypt(testString));
		 
	
		/*for(int i = 0; i < 10; i++){
			String enc = encrypt(testString+i);
			System.out.println("Encrypted:"+enc.toString());
			System.out.println("Decrypted:"+decrypt( enc));
		}*/
		
	}


	private static String read() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("enc.txt"))));
		return br.readLine().trim();
	}
}
