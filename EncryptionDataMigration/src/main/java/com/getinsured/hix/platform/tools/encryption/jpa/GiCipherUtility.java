package com.getinsured.hix.platform.tools.encryption.jpa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.getinsured.hix.platform.util.GhixAESCipherPool;

public class GiCipherUtility {
	
	private static GhixAESCipherPool ghixAESPooledCipherObj;
	private static String options_str = "\n"
			+ "Generate Encrypted Tag  ......................................1\n"
			+ "Generate Encrypted Property...................................2\n"
			+ "Validate Encrypted Property...................................3\n"
			+ "Select your option";

	

	public GiCipherUtility() {
		ghixAESPooledCipherObj = new GhixAESCipherPool();
	}
	
	/**
	 * @param args
	 * @throws IOException
	 */

	private static String getUserInput(String paramName, String defaultVal)
			throws IOException {
		boolean inputProvided = false;
		String val = null;
		while (!inputProvided) {
			if (paramName != null) {
				System.out.print("Ghix Property Encryption> "+paramName
						+ "::"
						+ ((defaultVal == null) ? ("   ") : ("[" + defaultVal)
								+ "]   "));
			}
			val = new BufferedReader(new InputStreamReader(System.in))
					.readLine();
			if (val == null || val.length() == 0) {
				if (defaultVal != null) {
					System.out.println("");
					return defaultVal;
				} else {
					continue;
				}
			}
			inputProvided = true;
		}
		if (val.equalsIgnoreCase("\\q")) {
			System.out.println("GhixEncryptionMigration> "+"....Bye");
			System.exit(0);
		}
		return val;
	}

	public static void main(String[] args) throws IOException {
		while (true) {
			String option = getUserInput(options_str, "1");
			processOption(option);
		}
	}

	private static void processOption(String option) throws IOException {
		if (option.equalsIgnoreCase("1")) {
			encryptAESForGENCTag();
		} else if (option.equalsIgnoreCase("2")) {
			encryptAESForConfigurationProperty();
		} else if (option.equalsIgnoreCase("3")){
			decryptAPropertyUsintNewAlgo();
		} else {
			System.out.println("Invalid Input Option:" + option);
			System.exit(0);
		}

	}
	
	private static void getSecretKey() throws IOException{
		String encryptionKey = getUserInput("Encryption Key", null);
		if (encryptionKey == null) {
			System.out.println("Secret key is mandatory for crypto operations, exiting");
			System.exit(0);
		}
		System.setProperty("PASSWORD_KEY", encryptionKey);
		if(ghixAESPooledCipherObj == null){
			ghixAESPooledCipherObj = new GhixAESCipherPool();
		}
	}

	private static void decryptAPropertyUsintNewAlgo()  {
		try{
			getSecretKey();
			getUserInput("Encryption Key", null);
			String encryptedStr = getUserInput("Please Enter an encrypted property", null);
			System.out.print("Output:"+ghixAESPooledCipherObj.decrypt(encryptedStr)+"\n");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  String decrypt(String encryptedString)  {
		try{
			String key = System.getProperty("PASSWORD_KEY");
			if(key == null){
				throw new RuntimeException("Expected system variable\"PASSWORD_KEY\"");
			}
			return ghixAESPooledCipherObj.decrypt(encryptedString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public  String decrypt(String encryptedString, String password)  {
		try{
			System.setProperty("PASSWORD_KEY", password);
			return ghixAESPooledCipherObj.decrypt(encryptedString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public  String encrypt(String value){
		try{
			String key = System.getProperty("PASSWORD_KEY");
			if(key == null){
				throw new RuntimeException("Expected system variable\"PASSWORD_KEY\"");
			}
			return ghixAESPooledCipherObj.encrypt(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public  String encrypt(String value, String password){
		try{
			System.setProperty("PASSWORD_KEY", password);
			return ghixAESPooledCipherObj.encrypt(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static void encryptAESForConfigurationProperty() throws IOException {
		String key = null;
		String val = null;
		getSecretKey();
		
		try {
			key = getUserInput("Property Key", null);
			if (key == null || key.length() == 0) {
				return;
			}
			val = getUserInput("Property Value", null);
			System.out
					.println("******************** Encrypted property **********************");
			System.out.println("Output:"+key + "=ENC("
					+ ghixAESPooledCipherObj.encrypt(val) + ")");
			System.out
					.println("**************************************************************");
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}
	
	private static void encryptAESForGENCTag() throws IOException {
		String val = null;
		getSecretKey();
		
		try {
			val = getUserInput("Tag Value", null);
			System.out
					.println("******************** Encrypted Tag ***************************");
			System.out.println("Output:"+val + "=GENC["
					+ ghixAESPooledCipherObj.encrypt(val) + "]");
			System.out
					.println("**************************************************************");
		} catch (Exception ie) {
			ie.printStackTrace();
		}

	}
	
	
	public static void welcome() throws IOException {
		
		System.out
				.println("\n*************************** Encrypted Data Migration Tool *******************************\n");
		System.out
				.println(" This Utility can be used to generate the static encrypted property values consumable by GI Platform 2.0.");
		System.out
				.println(" using AES 256 bit algorithm, before you use this tool you need to know the following");
		System.out.println(" \ta) Property value you need to encrypt/decrypt\n\t\t b) Secret Key");
		
		System.out.println(" Enter \\q at any time to quit\n");

		String proceed = getUserInput(" Do you want to continue?", "N");
		if (proceed.equalsIgnoreCase("N")) {
			System.exit(0);
		}
	}
}
