package com.getinsured.hix.platform.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.EmptyStackException;
import java.util.Stack;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GhixAESCipherPoolNew implements Encryption{
	public static final String FORMAT = "yyyy-MMM-dd HH:mm:ss zzz"; 
	private static Logger logger = LoggerFactory.getLogger(GhixAESCipherPoolNew.class);
	private static SecretKeySpec secret = null;
	//Encryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeEncryptorPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyEncryptorPool = new Stack<Cipher>();
	
	//Decryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeDecryptionPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyDecryptionPool = new Stack<Cipher>();
	private static SecureRandom random  = new SecureRandom();
	private static final int POOL_SIZE = 100;
	private static boolean poolInitialized = false;
	
	
	public String decrypt(String encryptedStr) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		String plainText = null;
		long start = System.currentTimeMillis();
		
		String password = ( System.getProperty("PASSWORD_KEY_NEW") != null) ? System.getProperty("PASSWORD_KEY_NEW") : System.getenv("PASSWORD_KEY_NEW");
		if(password == null){
			throw new RuntimeException("No password env found, expected \"PASSWORD_KEY_NEW\"");
		}
		try{
			if(encryptedStr == null){
				throw new RuntimeException("IV and input value for decryption are required for decryption");
			}
			if(!poolInitialized){
				initializCipherPools(POOL_SIZE, password);
			}
			byte[] encData = Base64.decodeBase64(encryptedStr); 
			byte[] iv = new byte[16];
			System.arraycopy(encData, 0, iv, 0, 16);
			byte[] cipherTextToken = new byte[encData.length-16];
			System.arraycopy(encData, 16, cipherTextToken, 0, encData.length-16);
			Cipher cipher = null;
			try{
				cipher = getActiveDecryptorPool().pop();
			}catch(EmptyStackException es){
				logger.warn("Validate token failed to retrieve the cipher environment from the pool, initializing new");
				cipher = getDecryptionCipherEnv(password);
			}
			cipher.init(Cipher.DECRYPT_MODE, secret,new IvParameterSpec(iv));
			plainText = new String(cipher.doFinal(cipherTextToken), "UTF-8");
			standbyDecryptionPool.push(cipher); // Return this cipher to the pool
			int hashIndex = plainText.indexOf("#");
			if(hashIndex != -1){
				return plainText.substring(hashIndex+1);
			}
		}catch(Exception ae){
			throw new RuntimeException("decryption failed with :"+ae.getMessage(), ae);
		}
		logger.info("Decryption took:"+(System.currentTimeMillis()-start)+" ms");
		return plainText;
	}
	
	
	private static synchronized void initializCipherPools(int poolSize, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(poolInitialized){
			return;
		}
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			activeEncryptorPool.add(cipher);
		}
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			activeDecryptionPool.add(cipher);
		}
		poolInitialized = true;
	}
	
	private static Cipher getDecryptionCipherEnv(String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Decryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		return cipher;
	}
	
	private static synchronized Cipher getEncryptionCipherEnv(String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Encryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		return cipher;
	}
	
	private static synchronized Stack<Cipher> getActiveEncryptorPool(){
		Stack<Cipher> emptyBank;
		try{
			activeEncryptorPool.peek();
		}catch(EmptyStackException es){
			System.out.println("Switching Encryption Cipher Banks");
			emptyBank = activeEncryptorPool;
			activeEncryptorPool = standbyEncryptorPool;
			standbyEncryptorPool = emptyBank;
		}
		return activeEncryptorPool;
	}
	
	private static synchronized Stack<Cipher> getActiveDecryptorPool(){
		Stack<Cipher> emptyBank;
		try{
			activeDecryptionPool.peek();
		}catch(EmptyStackException es){
			System.out.println("Switching Decryption Cipher Banks");
			emptyBank = activeDecryptionPool;
			activeDecryptionPool = standbyDecryptionPool;
			standbyDecryptionPool = emptyBank;
		}
		return activeDecryptionPool;
	}
	
	/**
	 * 
	 * @param password
	 * @param trustedEntityName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 */
	public String encrypt(String plainText) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		long start = System.currentTimeMillis();
		String returnStr = null;
		String password = ( System.getProperty("PASSWORD_KEY_NEW") != null) ? System.getProperty("PASSWORD_KEY_NEW") : System.getenv("PASSWORD_KEY_NEW");
		if(password == null){
			throw new RuntimeException("No password env found, expected \"PASSWORD_KEY_NEW\"");
		}
		
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE, password);
		}
		Cipher cipher = null;
		try{
			cipher = getActiveEncryptorPool().pop();
		}catch(EmptyStackException es){
			logger.warn("Generate Token Failed to get cipher env from the pool, initializing a new one");
			cipher = getEncryptionCipherEnv(password);
		}
		String plainTextIn = random.nextDouble()+"#"+plainText;
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal((plainTextIn).getBytes("UTF-8"));
		byte[] enc = new byte[iv.length+ciphertext.length];
		System.arraycopy(iv, 0, enc, 0, iv.length);
		System.arraycopy(ciphertext, 0, enc, iv.length, ciphertext.length);
		standbyEncryptorPool.push(cipher); //Send the cipher environment to standby pool
		returnStr =new String(Base64.encodeBase64URLSafe(enc));
		logger.info("Encryption took:"+(System.currentTimeMillis()-start)+" ms");
		return returnStr;
	}
	
	public static void main(String[] args) throws Exception{
		/*String testString = "{\"family\":{\"dependents\":[],\"specialEnroll\":{\"married\":true},\"representative\":{},\"payment\":{\"initialType\":\"CREDIT_CARD\",\"recurringType\":\"DIRECT_BILL\"},\"childApp\":false,\"primary\":{\"memberNumber\":1,\"dateOfBirth\":\"05/08/1963\",\"isNativeAmerican\":null,\"medicaidChild\":\"N\",\"isPregnant\":\"N\",\"relationshipToPrimary\":\"SELF\",\"gender\":\"Male\",\"age\":51,\"specialEnroll\":{\"married\":{\"selected\":true,\"date\":\"03022015\"}},\"tobacco\":false,\"dob\":\"05081963\",\"planInfo\":{\"planName\":\"Silver Copay Select 1\",\"planId\":568105,\"planCost\":\"525.3\",\"applicantnum\":1,\"effectivedate\":1427860800000,\"premium\":525.3,\"deductible\":\"5000\",\"totalCost\":525.3},\"bestNumber\":\"home\",\"bestContactTime\":\"Afternoon\",\"languages\":\"English\",\"fname\":\"Willi\",\"lname\":\"Becher\",\"homePhone\":\"2615550000\",\"email\":\"willibecher@uhc.pa\",\"address\":{\"state\":\"PA\",\"zip\":\"19101\",\"county\":\"PHILADELPHIA\",\"street\":\"General Delivery\",\"city\":\"Philadelphia\"},\"signaturedate\":1426272931850,\"today\":1426272931850,\"maritalStatus\":\"Single\",\"diffmailing\":false,\"noncitizen\":false,\"pcp\":{\"fname\":\"Viktor\",\"lname\":\"Frankenstein\",\"phone\":\"8005559999\",\"address\":{\"street\":\"1 Castle Way\",\"city\":\"Oberbayern\",\"zip\":\"19101\"}},\"ssn\":\"654565456\",\"esign\":{\"fname\":\"Willi\",\"lname\":\"Becher\"}},\"hsa\":{\"depositAmt\":0},\"effectivedate\":1427860800000,\"networkType\":\"PPO\",\"enrollmentId\":5},\"addtlInfo\":{\"numChildren\":0,\"hasSpouse\":false,\"anyTobacco\":false,\"anySelected\":{\"married\":true},\"selectedDoctor\":false,\"openEnrollment\":false,\"planDisplayDate\":\"04/01/2015\",\"appId\":\"696190261739\",\"hsaEligible\":false,\"hsaMax\":279.16,\"allCitizens\":true,\"anyMedicare\":false},\"payer\":{\"payerCheck\":\"other\",\"fname\":\"Willi\",\"lname\":\"Becher\",\"addrStreet\":\"7101 s central ave\",\"addrCity\":\"los angeles\",\"addrState\":\"CA\",\"addrZip\":\"90001\",\"email\":\"willibecher@uhc.pa\"},\"lastSection\":\"14-E-Signature\"}";
		
		String enc = encrypt(testString);
		System.out.println("Encrypted:"+enc.toString());
		System.out.println("Decrypted:"+decrypt( "4uX5e0KlTADPzzZ2XbnCmRYGnxekd7CUS6J_s0duSMUzqeQOk_HZODyaSpnh1edFPICCGfYyD4wpTrAdCBvimHPtFqUSK0GInac_fw_VK0ojGHJO7XXpoQsuOzSgb2L2wzOdTaMAMNn1VFeF-7--6H2R9B-VNbQxrVJ7mK0EPdHAcKKImY4C8msz44Q1-8pXj7pNtWjEwrJ7rnjZH5lgsfwC4WbEslw8rHatnzQRD-vkuqCPhxYHgPzmwdl4b-Bs27SC3PjOAMG5MQNgI8m2kGqPGmhKoaSwzp6KDXhcdW2SPO5HEG_w4FRm_sqYLBRrE-PV9V64Q3E5qZa6z7Vugpd2jG01LlxF-sfSPokqk1fLDuDJPCvj8mDXuffnavvBN1koV7I72eR0NsDsUDO0F8f2yKHeJFnsT-sXIO02Eny641FCfw21RohWwIvLdO5UdqOrfiEOd76MKpo-zaIFQ_jAH9CM1Ntp2zaMb0QdrneV3nYFAPEHd6Oc6X5D96XLRCG5kBv-68O_HWFhQBaoZHm5zygO4W_i6bT46jFfFlO7gOhQ5MWfssjYcpL9FyZ1jz1u_WYw-12bMeS4seSzxy5tcazF7RASspAu8gM0IvVQ5EBqhV8pT4b2bdfEmeXUrA4pbnRmRW6iY1599uuw1u-k-0GXnJsmbtjIQjBvb7mWVYCY2ns6OHwXQSCpFZB47ieqFjPL18mBrD4OSWtTkjpUSYzPfAxjD1Ku6H6QJ6LNzofw4P3_ErmP0UXawkWoGKIe0QZbfOy0RnMWpWK3rCe07nACxr7m43y8NGd1UYekVQr2dmtG7IE8JwpMd9X0moUiu0bp4RAcvab1m4-p8kYunEX19r9uLBHMYqCijFjWBLOqtp7qarrX0AgZapS4G5odz5AfOveRRQTFtox2wZA3PyG3tfEcsIsWfqG4DySK0eHzFFXIvhWIc5t4-GPAANvcxNM2M91Zp6Sk25LUs7cgZkCyTlqnDjpx5NWNEowf5UCc3Lra1KdbLw0FLKBATJ5V4CGUVDoyPjWWQO4A1kLE0KdcdXBMYJ2HjebdheppxskUquAYnpxYSJYkls7zFmvHkww0tSEbNrxFminjuRQcdsSNyZKhf6SjR7qnNE6RtKZq9DsBUncrNrTRFkwEAXxGgtSK2joq9MAr1ahYgxR1sujhseWc9kExpkaHFKhl92lDxD2uTxOwHOaee1gBZeYTjP1cqByDDTFviYnFmrabNe3t-5X9ZFei77ZYpEFW7z2oJZaBcl3OQiclJIUllamqcPdxhmHitNojYLkpvO1VUQyOryzostlneBChKsIu7gdVcxRn17TRAQdnh71bIr58RQHhjZGpFZv8lTDs-nEUOonMijTpF1pK945q7PHBiuhBuzbHXqlF1kYFAuCR4yNxRV-bdhDqUEOgs9YEuFgzLVWzzTOpMz6P5ABvvmQf2JcNNdKmuCxmU14h_BYRX1tyj4v5s5iXosVq1oTRB6PpfrQDK4Wt3YkOkUhvy9IoGZdk7rOC6yuVVXCmX8Q9n_mtgjSOU5iF6bmIZgyuac6x4exgmq5sPOjZE2rZP_bTGKRLo2l8F50mbbhMoxqhUKtIVGD_dBnvRICp5b7N4RuINZj3yd1qqka9sHZswC5xnrd3ihG6y6USxZvY_VSynb4NgoKpt2VODbByERWfMyzHbx3G35fDkJpeNehG48QyXuIzK9WEBAvTU4bMm_UV840ZXSLJW8Gmdo4wJd-Pu9WoTPWWv28ytNKfIoLuqqbhxWD1IikYNLcwJ-rL9z-noKDem-H3-YkIjzZzfWtueO6D9APbmRoYBJdE7K7nLunz2cNsQFo1L6J8wV4TrazA-5HtNJ6ag40_0xhnxzsZeXdhwfgcqN-Z9I8rZDzIh-S2DUPkAjkTJBPfHpIYDxqQJaBXn-UjD0TX3LofcBg8awa6O0vs_Pohi6jqYJ1P1yawaqtDPf7DI4kaXIdnjv3gtlH6flg_MSUdgd6jQRiB1P86xRfFKgyU4QL9xhTVvX4YsFmeoxX14bp7rPDqvi2Bbc2P6MXdSSSZNpLSTjzuVoy-lh7jmWERfEp1qTQFi77rNQHkfYKZag7Yi7DIA7D1raKnLNcC6q4t9n2cTaPPZ9_dtQFvAwBmt2vFhv6NHMVw7DpEQIMcFda9Zdom8GGXoShHssOwcJ79HzOCSkHm2dSjk6TZHpfyBEDMfv7V3ESYR4IPj6S_5lof3bPSGs5DAySaZjpCSy2ARCBjMbT9OCIwhnlRQi5_ic4qkCgxQgmoSLMEQKvU49tF5zhVctPqtdpkmII0Ml6Xh2DzdpTIxEQKFznKOXJJAk-3urvdtA_G450RrFjWzLgvKp83FMQTF3yN7ECzE1i_izQE-479yHSOuzBh4FluLNneSHvDaA3bgJV4TPNIcT9zuA7AyllvKFbqyBBheB6DOzh6dfdZcw"));
		*/
		/*String testString = "20-";
		
		for(int i = 0; i < 10; i++){
			String enc = encrypt(testString+i);
			System.out.println("Encrypted:"+enc.toString());
			System.out.println("Decrypted:"+decrypt( enc));
		}*/
	}
}