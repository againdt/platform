package com.getinsured.hix.platform.tools.encryption.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: D2CEnrollment
 *
 */
@Entity
@Table(name="SECURABLES")
public class Securables implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3562535532104680492L;


	public Securables(){
		super();
	}
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SECURABLES_SEQ")
	@SequenceGenerator(name = "SECURABLES_SEQ", sequenceName = "SECURABLES_SEQ", allocationSize = 1)
	private Long id;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	@Column(name="PASSWORD")
	private String password;


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
   
}
