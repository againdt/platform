package com.getinsured.hix.platform.tools.encryption.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: D2CEnrollment
 *
 */
@Entity
@Table(name="D2C_ENROLLMENT")
public class D2CEnrollment implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3562535532104680492L;


	public D2CEnrollment(){
		super();
	}
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "D2C_ENROLLMENT_SEQ")
	@SequenceGenerator(name = "D2C_ENROLLMENT_SEQ", sequenceName = "D2C_ENROLLMENT_SEQ", allocationSize = 1)
	private Long id;
	
	
	@Column(name="UI_DATA")
	private String uiData;
	
		
	@Column(name="DATA")
	private String data;
	
	@Column(name="PAYMENT_DATA")
	private String paymentData;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUiData() {
		return uiData;
	}
	public void setUiData(String uiData) {
		this.uiData = uiData;
	}

	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	public String getPaymentData() {
		return paymentData;
	}
	public void setPaymentData(String paymentData) {
		this.paymentData = paymentData;
	}
   
}
