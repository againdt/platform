To Create jar on any server:
1-Go to /var/lib/jenkins/jobs/Build/workspace/platform/ZipcodeLoaderUtility/
2-Execute command  
  a.export JAVA_HOME=/usr/lib/jvm/jdk-8u60-linux-x64/jdk1.8.0_77
  b./usr/local/apache-maven-3.3.9/bin/mvn clean compile assembly:single
3-You will see new jar file “ZipcodeLoaderUtility-0.0.1-SNAPSHOT-jar-with-dependencies.jar” in /var/lib/jenkins/jobs/Build/workspace/platform/ZipcodeLoaderUtility/ path
4-Rename this jar with “ZipcodeLoaderUtility.jar”
5-Create new directory “externaljars” in /opt/web/ghixhome/ghix-setup/
6-Copy ZipcodeLoaderUtility.jar to externaljars

To upload Out of territory zipcode on couchbase:
1- Go to /opt/web/ghixhome/ghix-setup/externaljars
2- Keep CSV file on same location
3- Execute following command with csv name
   a.java -DGHIX_HOME=/opt/web/ghixhome -jar ZipcodeLoaderUtility.jar "zipcode.csv" 
4- Check the status in OOTZipcodeUploader.log file