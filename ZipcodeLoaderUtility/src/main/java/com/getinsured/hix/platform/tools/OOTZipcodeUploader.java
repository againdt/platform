package com.getinsured.hix.platform.tools;

import java.util.List;
import java.util.Properties;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;


public class OOTZipcodeUploader{
	    private Cluster cluster;
	    private Bucket bucket;
	    private static final String [] FILE_HEADER_MAPPING = {"zipcode"};
	    private static final String zipcode = "zipcode";
	    
	    private PrintWriter zipcodeLogger;
	    private File outputFile;
	    
	    public OOTZipcodeUploader(String fileName, String bucketName, String bucketPassword, String nodes){
	    	try {
				outputFile = new File("OOTZipcodeUploader.log");
				if (outputFile.exists()) { outputFile.delete(); }
				this.zipcodeLogger = new PrintWriter(outputFile);
			} catch (FileNotFoundException e) {
				System.err.println("Error while setting up logger"+ e.getMessage());
				System.exit(-1);
			}
	    this.uploadOotZipcode(fileName, bucketName, bucketPassword, nodes);
		this.zipcodeLogger.flush();
		this.zipcodeLogger.close();
		
	    }
	    
		private void uploadOotZipcode(String fileName, String bucketName, String bucketPassword, String nodes){
			try{
				this.cluster = CouchbaseCluster.create(nodes);
		        // get a Bucket reference from the cluster to the configured bucket
		        this.bucket = this.cluster.openBucket(bucketName, bucketPassword);
		        if (!this.bucket.isClosed()) {
		        	zipcodeLogger.println("Open CouchBase Bucket: " + bucketName + " successful.");
		        } else {
		        	zipcodeLogger.println("Open CouchBase Bucket: " + bucketName + " failed.");
		        	zipcodeLogger.flush();
		            System.exit(-1);
		        }
			}catch(Exception e){
				zipcodeLogger.println("Error while opening couchbase Bucket:"+ e.getMessage()+"");
				e.printStackTrace();
				zipcodeLogger.flush();
				System.exit(-1);
			}
			
			FileReader fileReader = null;
	        CSVParser csvFileParser = null;
	        //Create the CSVFormat object with the header mapping
	        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);
			 try {
				 String deleteStatement = "delete from " + bucketName +" where id like 'ZipcodeNotInTerritory::%' "
				 		+ "and metaData.category = 'CAP' "
				 		+ "and metaData.subCategory = 'ZIPCODE_OUT_OF_TERRITORY'";
				 N1qlQueryResult deleteqr = this.bucket.query(N1qlQuery.simple(deleteStatement));
				 if(!deleteqr.status().equals("success")){
					 zipcodeLogger.println("Unable to delete exisiting data from couch");
					 zipcodeLogger.flush();
					 System.exit(-1);
				 }
				 
 	            //initialize FileReader object
	             fileReader = new FileReader(fileName);
	             
	             //initialize CSVParser object
	             csvFileParser = new CSVParser(fileReader, csvFileFormat);
 	             
	             //Get a list of CSV file records
   	             List csvRecords = csvFileParser.getRecords(); 
 
   	             //Read the CSV file records starting from the second record to skip the header
   	            zipcodeLogger.println("Number of records =>" + csvRecords.size()+"");
 	            for (int i = 1; i < csvRecords.size(); i++) {
 	            	String statement = "";
 	            	zipcodeLogger.flush();
 	                CSVRecord record = (CSVRecord) csvRecords.get(i);
 	                String zip = record.get(zipcode);
 	                String id = "ZipcodeNotInTerritory::" + record.get(zipcode); 
 	                //print counter after every 100 records
 	                //first delete all records of old data
 	                statement = "UPSERT INTO " 
 	                       + bucketName + " (KEY, VALUE) VALUES (\"" + id + "\",{"
 	                       + "\"id\":\""+ id +"\","
 	            		   + "\"ZipCode\":\""+zip+"\","
 	            		   + "\"metaData\":{" 
 	               		   + "\"category\":\"CAP\","
 	                       + "\"subCategory\":\"ZIPCODE_OUT_OF_TERRITORY\","
 	               		   + "\"createdBy\":\"migrationadmin\","
 	               		   + "\"modifiedBy\":\"migrationadmin\","
 	               		   + "\"creationDate\":STR_TO_MILLIS(NOW_LOCAL()),"
 	               		   + "\"modifiedDate\":STR_TO_MILLIS(NOW_LOCAL())"
 	               		   +"}})";
	                N1qlQueryResult qr = this.bucket.query(N1qlQuery.simple(statement));
	                if(!qr.status().equals("success")){
	                	zipcodeLogger.println("Unable to upload zipcode " + record.get(zipcode) +" on couch%n");
	                	continue;
	                }else{
	                	zipcodeLogger.println("Upsert completed for =>"+ record.get(zipcode)+"");
	                }
	                if(i % 1000 == 0 ){
	                	zipcodeLogger.println("Records completed =>" + i +"");
	                }
 	            }
 	        } 
 	        catch (Exception e) {
 	        	    zipcodeLogger.println("Error in CsvFileReader !!!");
	 	            e.printStackTrace();
	 	            zipcodeLogger.flush();
	 	            System.exit(-1);
 	        } finally {
 	            try {
 	                fileReader.close();
 	                csvFileParser.close();
 	            } catch (IOException e) {
 	            	zipcodeLogger.println("Error while closing fileReader/csvFileParser !!!");
 	                e.printStackTrace();
 	                zipcodeLogger.flush();
 	                System.exit(-1);
 	            }
 	        }
		}
		
		 public static void main(String[] args) {
	            // load the properties file
			    if(args.length == 0){
			    	System.out.println("Please pass CSV file name!");
			 		System.exit(-1);
			    }
			 	String filename = System.getProperty("GHIX_HOME") + "/ghix-setup/externaljars/"+ args[0];
			 	File f = new File(filename);
   			    if(f.exists()){
					  System.out.println("File existed");
				}else{
					  System.out.println("File not found!");
					  System.exit(-1);
				}
			 	if(filename.isEmpty()){
			 		System.out.println("CSV File name is empty, please pass correct arguments");
			 		System.exit(-1);
			 	}
	            try {
	            	 Properties prop = new Properties();
	            	 if(System.getProperty("GHIX_HOME")== null || System.getProperty("GHIX_HOME").isEmpty()){
	            		System.out.println("Please set GHIX_HOME path");
	 			 		System.exit(-1);
	            	 }
	    			 String configfile = System.getProperty("GHIX_HOME") + "/ghix-setup/conf/configuration.properties";
	                 InputStream configProp = new FileInputStream(configfile);
                	 prop.load(configProp);
	                 String bucketName = prop.getProperty("couchbase.nonbinary.bucketname");
	                 String bucketPassword = prop.getProperty("couchbase.password");
	                 String nodes = prop.getProperty("couchbase.nodes");
	                 if(bucketName.isEmpty() || bucketPassword.isEmpty() || nodes.isEmpty()){
	 			 		System.out.println("Bucket properties are empty!");
	 			 		System.exit(-1);
	 			 	}
	                new OOTZipcodeUploader(filename ,bucketName, bucketPassword, nodes);
	                System.out.println("OOT zipcodes upload on couchbase completed !");
 			 		System.exit(-1);
				} catch (IOException e) {
					 System.out.println("Error while Reading configuration file !!!");
					 e.printStackTrace();
					 System.exit(-1);
				}
			 
		 }
}