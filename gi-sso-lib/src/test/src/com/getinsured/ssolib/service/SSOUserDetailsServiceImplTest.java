package com.getinsured.ssolib.service;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.impl.AssertionImpl;
import org.opensaml.saml2.core.impl.AuthnStatementImpl;
import org.opensaml.saml2.core.impl.IssuerImpl;
import org.opensaml.saml2.core.impl.SubjectImpl;
import org.springframework.security.saml.SAMLCredential;

import com.getinsured.ssolib.user.SSOUser;
import com.getinsured.ssolib.util.SAMLTestHelper;

import static org.mockito.Mockito.when;

/**
 * Tests for {@link SSOUserDetailsServiceImpl}.
 *
 * @author Yevgen Golubenko
 * @since 4/6/17
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class SSOUserDetailsServiceImplTest
{
  private SSOUserDetailsServiceImpl userDetailsService;

  @Mock
  private NameID nameID;

  @Mock
  private SAMLCredential credential;

  @Mock
  private AssertionImpl assertion;

  @Mock
  AuthnStatementImpl authnStatement;

  @Mock
  private SubjectImpl subject;

  @Mock
  private IssuerImpl issuer;

  @Before
  public void setUp()
  {
    MockitoAnnotations.initMocks(this);
    userDetailsService = new SSOUserDetailsServiceImpl();

    nameID = ((SAMLObjectBuilder<NameID>) SAMLTestHelper.getBuilderFactory().getBuilder(NameID.DEFAULT_ELEMENT_NAME)).buildObject();
    //assertion = ((SAMLObjectBuilder<Assertion>) SAMLTestHelper.getBuilderFactory().getBuilder(Assertion.DEFAULT_ELEMENT_NAME)).buildObject();
    credential  = sampleSAMLCredential();
  }

  @Test
  public void testLoadUserBySAML() throws Exception
  {
    when(subject.getNameID()).thenReturn(nameID);
    when(issuer.getValue()).thenReturn("sso.ghixqa.com");

    when(authnStatement.getAuthnInstant()).thenReturn(new DateTime());
    when(assertion.getAuthnStatements()).thenReturn(Arrays.asList(authnStatement));

    when(assertion.getSubject()).thenReturn(subject);
    when(assertion.getIssuer()).thenReturn(issuer);
    when(assertion.getIssueInstant()).thenReturn(new DateTime());

    SSOUser user = (SSOUser) userDetailsService.loadUserBySAML(credential);
    Assert.assertNotNull("Could not load user by SAMLCredential", user);
  }

  private SAMLCredential sampleSAMLCredential()
  {
    nameID.setValue("testName");
    assertion.setID("testID");
    SAMLCredential credential = null;
    String localEntityID = "MS-STARR";
    String remoteEntityID = "sso.ghixqa.com";
    String relayState = null;
    List<Attribute> attributes = null;
    Serializable additionalData = null;
    credential = new SAMLCredential(nameID, assertion, "sso.ghixqa.com", "MS-STARR");

    return credential;
  }
}