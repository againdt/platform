package com.getinsured.authorization;

import java.io.Serializable;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.getinsured.ssolib.config.SSOConfig;

/**
 * Custom permission evaluator.
 * <p>
 *   Version updates for ssolib:
 * </p>
 * <ul>
 * 	<li>1.1 Allows usage of <code>@PreAuthorize(&quot;hasPermission('MY_PERM')&quot;)</code>.</li>
 *  <li>1.2 Allows listing permissions to be allowed when sso.enabled == false and no custom permission evaluator
 *  is implemented on the application.</li>
 * </ul>
 */
@Component
@EnableConfigurationProperties({SSOConfig.class})
public class GiPermissionEvaluator implements PermissionEvaluator {

	private static final Logger log = LoggerFactory.getLogger(GiPermissionEvaluator.class);

	@Autowired
	private SSOConfig ssoConfig;

	@PostConstruct
	public void postConstruct()
	{
		if(log.isInfoEnabled())
		{
			log.info("<GiPermissionEvaluator> initialized with sso config: {}", ssoConfig);
		}
	}

	@Override
	public boolean hasPermission(Authentication subject, Object arg1, Object permissionName) {
		Collection<? extends GrantedAuthority> authorities = subject.getAuthorities();
		for(GrantedAuthority authority:authorities){
			if(authority.getAuthority().equalsIgnoreCase((String)permissionName)){
				if(log.isDebugEnabled())
				{
					log.debug("hasPermission passed for permission: {}",permissionName);
				}
				return true;
			}
		}

		// If sso.enabled == false, and application uses hasPermissions but doesn't implement
		// them, allow checking ssoConfig.passForPermissionsWithoutSso to see allowed access list.
		if(ssoConfig != null && !ssoConfig.getEnabled())
		{
			String[] allowedPermissions = ssoConfig.getPassForPermissionsWithoutSso();

			if(allowedPermissions != null && allowedPermissions.length > 0)
			{
				for(String perm : allowedPermissions)
				{
					if(perm.equals(permissionName))
					{
						if(log.isDebugEnabled())
						{
							log.debug("sso: disabled, but allowing permissions because of configuration: {}", permissionName);
						}

						return true;
					}
				}
			}
		}

		if(log.isDebugEnabled())
		{
			log.debug("hasPermission DID NOT pass for permission: {}", permissionName);
		}

		return false;
	}

	@Override
	public boolean hasPermission(Authentication subject, Serializable targetObject, String objClassName, Object permissionName) {
		try
		{
			Class<?> clz = Class.forName(objClassName);
			Object obj = clz.cast(targetObject);
			return this.hasPermission(subject, obj, permissionName);
		}
		catch (ClassNotFoundException ce)
		{
			if (log.isErrorEnabled())
			{
				log.error("Failed to execute hasPermission", ce);
			}

			return false;
		}
	}
}
