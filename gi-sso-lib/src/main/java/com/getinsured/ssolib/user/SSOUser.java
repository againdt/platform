package com.getinsured.ssolib.user;

import java.io.Serializable;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.xml.schema.impl.XSStringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Holds logged in user information.
 *
 * @author Yevgen Golubenko
 * @since 4/5/17
 */
public class SSOUser implements Principal, Serializable, UserDetails
{
  private static final Logger log = LoggerFactory.getLogger(SSOUser.class);

  private final String name;
  private final String authenticationResponseIssuingEntityName;
  private final String authenticationAssertionIssuingEntityName;
  private final String authenticationResponseID;
  private final String authenticationAssertionID;
  private final DateTime authenticationResponseIssueInstant;
  private final DateTime authenticationAssertionIssueInstant;
  private final DateTime authenticationIssueInstant;
  private final HashMap<String, String> userAttributesMap;

  private Set<GrantedAuthority> authorities;

  public SSOUser(String name,
                 String authenticationResponseIssuingEntityName,
                 String authenticationAssertionIssuingEntityName,
                 String authenticationResponseID, String authenticationAssertionID,
                 DateTime authenticationResponseIssueInstant,
                 DateTime authenticationAssertionIssueInstant,
                 DateTime authenticationIssueInstant,
                 Collection<? extends GrantedAuthority> authorities,
                 List<Attribute> userAttributes)
  {
    super();
    this.name = name;
    this.authenticationResponseIssuingEntityName = authenticationResponseIssuingEntityName;
    this.authenticationAssertionIssuingEntityName = authenticationAssertionIssuingEntityName;
    this.authenticationResponseID = authenticationResponseID;
    this.authenticationAssertionID = authenticationAssertionID;
    this.authenticationResponseIssueInstant = authenticationResponseIssueInstant;
    this.authenticationAssertionIssueInstant = authenticationAssertionIssueInstant;
    this.authenticationIssueInstant = authenticationIssueInstant;
    this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));

    if (userAttributes.size() > 0)
    {
      this.userAttributesMap = new HashMap<>(userAttributes.size());

      userAttributes.forEach(attribute ->
      {

        String vals = attribute.getAttributeValues()
            .stream()
            .map(v -> ((XSStringImpl) v).getValue())
            .collect(Collectors.joining(","));

        this.userAttributesMap.put(attribute.getName(), vals);
      });
    } else
    {
      this.userAttributesMap = new HashMap<>();
    }

    log.info("<SSOUser> Constructed SSOUser: {}", this.toString());
  }

  private static SortedSet<GrantedAuthority> sortAuthorities(
      Collection<? extends GrantedAuthority> authorities)
  {
    SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<>(new AuthorityComparator());

    sortedAuthorities.addAll(authorities);

    return sortedAuthorities;
  }

  public String getAuthenticationResponseIssuingEntityName()
  {
    return authenticationResponseIssuingEntityName;
  }

  public String getAuthenticationAssertionIssuingEntityName()
  {
    return authenticationAssertionIssuingEntityName;
  }

  public String getAuthenticationResponseID()
  {
    return authenticationResponseID;
  }

  public String getAuthenticationAssertionID()
  {
    return authenticationAssertionID;
  }

  public DateTime getAuthenticationResponseIssueInstant()
  {
    return authenticationResponseIssueInstant;
  }

  public DateTime getAuthenticationAssertionIssueInstant()
  {
    return authenticationAssertionIssueInstant;
  }

  public DateTime getAuthenticationIssueInstant()
  {
    return authenticationIssueInstant;
  }

  public HashMap<String, String> getUserAttributesMap()
  {
    return userAttributesMap;
  }

  public void setAuthorities(final Set<GrantedAuthority> authorities)
  {
    this.authorities = authorities;
  }

  /**
   * Returns the name of this principal.
   *
   * @return the name of this principal.
   */
  @Override
  public String getName()
  {
    return name;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities()
  {
    return authorities;
  }

  @Override
  public String getPassword()
  {
    return null;
  }

  @Override
  public String getUsername()
  {
    String username = this.getUserAttributesMap().get("http://wso2.org/claims/username");

    if (username != null)
    {
      return username;
    }

    return name;
  }

  @Override
  public boolean isAccountNonExpired()
  {
    return false;
  }

  @Override
  public boolean isAccountNonLocked()
  {
    return false;
  }

  @Override
  public boolean isCredentialsNonExpired()
  {
    return false;
  }

  @Override
  public boolean isEnabled()
  {
    return false;
  }

  private static class AuthorityComparator implements
      Comparator<GrantedAuthority>, Serializable
  {
    private static final long serialVersionUID = 2490388631339334388L;

    public int compare(GrantedAuthority g1, GrantedAuthority g2)
    {

      if (g2.getAuthority() == null)
      {
        return -1;
      }

      if (g1.getAuthority() == null)
      {
        return 1;
      }

      return g1.getAuthority().compareTo(g2.getAuthority());
    }
  }


  @Override
  public boolean equals(Object obj)
  {

    if (obj == null)
    {
      return false;
    }

    if (obj == this)
    {
      return true;
    }

    if (obj.getClass() != getClass())
    {
      return false;
    }

    SSOUser rhs = (SSOUser) obj;
    return new EqualsBuilder().append(name, rhs.name).isEquals();
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder(517, 43).append(name).toHashCode();

  }

  @Override
  public String toString()
  {
    return "SSOUser{" +
        "name='" + name + '\'' +
        ", authenticationResponseIssuingEntityName='" + authenticationResponseIssuingEntityName + '\'' +
        ", authenticationAssertionIssuingEntityName='" + authenticationAssertionIssuingEntityName + '\'' +
        ", authenticationResponseID='" + authenticationResponseID + '\'' +
        ", authenticationAssertionID='" + authenticationAssertionID + '\'' +
        ", authenticationResponseIssueInstant=" + authenticationResponseIssueInstant +
        ", authenticationAssertionIssueInstant=" + authenticationAssertionIssueInstant +
        ", authenticationIssueInstant=" + authenticationIssueInstant +
        ", userAttributesMap=" + userAttributesMap +
        ", authorities=" + authorities +
        '}';
  }
}
