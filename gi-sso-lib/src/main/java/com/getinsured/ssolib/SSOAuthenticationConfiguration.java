package com.getinsured.ssolib;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.velocity.app.VelocityEngine;
import org.opensaml.saml2.metadata.provider.FilesystemMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLBootstrap;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.SAMLLogoutFilter;
import org.springframework.security.saml.SAMLLogoutProcessingFilter;
import org.springframework.security.saml.SAMLProcessingFilter;
import org.springframework.security.saml.context.SAMLContextProviderImpl;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.security.saml.key.KeyManager;
import org.springframework.security.saml.log.SAMLDefaultLogger;
import org.springframework.security.saml.metadata.CachingMetadataManager;
import org.springframework.security.saml.metadata.ExtendedMetadata;
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate;
import org.springframework.security.saml.metadata.MetadataDisplayFilter;
import org.springframework.security.saml.parser.ParserPoolHolder;
import org.springframework.security.saml.processor.HTTPArtifactBinding;
import org.springframework.security.saml.processor.HTTPPAOS11Binding;
import org.springframework.security.saml.processor.HTTPPostBinding;
import org.springframework.security.saml.processor.HTTPRedirectDeflateBinding;
import org.springframework.security.saml.processor.HTTPSOAP11Binding;
import org.springframework.security.saml.processor.SAMLBinding;
import org.springframework.security.saml.processor.SAMLProcessorImpl;
import org.springframework.security.saml.storage.EmptyStorageFactory;
import org.springframework.security.saml.util.VelocityFactory;
import org.springframework.security.saml.websso.*;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import com.getinsured.ssolib.config.MethodPermissionConfiguration;
import com.getinsured.ssolib.config.SSOConfig;
import com.getinsured.ssolib.service.SSOUserDetailsServiceImpl;

/**
 * Configures application to authenticate against GetInsured WSO2 server.
 *
 * <p>
 *   There is confluence page <a href="https://confluence.getinsured.com/pages/viewpage.action?pageId=31919218">Microservice Integration with WSO2/SSO</a>
 *   that describes in some details how to integrate microservice with this library.
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 4/5/17
 */
@Configuration
@EnableWebSecurity
@EnableConfigurationProperties({SSOConfig.class})
@ComponentScan(basePackages = {"com.getinsured.ssolib.config", "com.getinsured.ssolib.user", "com.getinsured.ssolib.service", "com.getinsured.ssolib.controller"})
@ConditionalOnExpression("${sso.enabled:false}")
@Order(Ordered.LOWEST_PRECEDENCE - 1000)
@Import({MethodPermissionConfiguration.class})
public class SSOAuthenticationConfiguration extends WebSecurityConfigurerAdapter
{
  private static final Logger log = LoggerFactory.getLogger(SSOAuthenticationConfiguration.class);

  @Autowired
  private SSOUserDetailsServiceImpl ssoUserDetailsService;

  @Autowired
  private SSOConfig ssoConfig;

  @PostConstruct
  private void postConstruct()
  {
    Assert.notNull(this.ssoUserDetailsService, "Could not @Autowire SSOUserDetailsServiceImpl");
    Assert.notNull(this.ssoConfig, "Could not @Autowire SSOConfig");
    log.info("<SSOAuthenticationConfiguration> initialized: sso-lib version: 1.4");
    this.ssoConfig.validate();
    log.info("<SSOAuthenticationConfiguration> SSO Configuration: {}", this.ssoConfig.toString());
  }

  /**
   * Defines the web based security configuration.
   *
   * @param  http It allows configuring web based security for specific http requests.
   * @throws Exception if any errors occurred during configuration.
   */
  @Override
  protected void configure(HttpSecurity http) throws Exception
  {
    final HttpSessionSecurityContextRepository repo = new HttpSessionSecurityContextRepository();
    repo.setDisableUrlRewriting(true);

    http.authenticationProvider(samlAuthenticationProvider());
    http.httpBasic().authenticationEntryPoint(samlEntryPoint());
    http.securityContext().securityContextRepository(repo);
    http.exceptionHandling().accessDeniedPage(ssoConfig.getLogin().getAccessDeniedUrl());

    String[] permitAllUrls = ssoConfig.getPermitAllUrls();

    if(permitAllUrls == null || permitAllUrls.length == 0)
    {
      /*
        - /actuator/**
        - /login
        - /logout
        - /js/**
        - /css/**
        - /bower_components/**
        - /resources/**
        - /health
        - /info
        - /mappings
        - /beans
        - /ssolib/logout
       */
      permitAllUrls = new String[] {
          "/resources/**",
          "/scripts/**",
          "/bower_components/**",
          "/assets/**",
          "/actuator/**",
          "/login",
          "/logout",
          "/info",
          "/health",
          "/beans",
          "/mappings",
          SSOConfig.Login.DEFAULT_LOGIN_LOGOUT_URL
      };

      if(log.isInfoEnabled())
      {
        log.info("permitAllUrls not set, using default list: {}", Arrays.asList(permitAllUrls));
      }
    }

    http.authorizeRequests()
        .antMatchers(permitAllUrls).permitAll()//.anyRequest().authenticated()
        .antMatchers("/**", "/**/**").fullyAuthenticated();
    http.logout().deleteCookies(ssoConfig.getLogout().getRemoveCookies())
        .invalidateHttpSession(true).logoutSuccessUrl(ssoConfig.getLogout().getLogoutUrl());
    http.addFilterBefore(samlFilter(), BasicAuthenticationFilter.class);
    http.csrf().disable();
  }

  /**
   * Sets a custom authentication provider.
   *
   * @param   auth SecurityBuilder used to create an AuthenticationManager.
   * @throws Exception if any errors occurred during configuration.
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception
  {
    auth.authenticationProvider(samlAuthenticationProvider());
  }

  // Initialization of the velocity engine
  @Bean
  public VelocityEngine velocityEngine()
  {
    return VelocityFactory.getEngine();
  }

  // XML parser pool needed for OpenSAML parsing
  @Bean(initMethod = "initialize")
  public StaticBasicParserPool parserPool()
  {
    return new StaticBasicParserPool();
  }

  @Bean
  public PasswordEncoder passwordEncoder()
  {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public ParserPoolHolder parserPoolHolder()
  {
    return new ParserPoolHolder();
  }

  // Bindings, encoders and decoders used for creating and parsing messages
  @Bean
  public MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager()
  {
    return new MultiThreadedHttpConnectionManager();
  }

  @Bean
  public HttpClient httpClient()
  {
    return new HttpClient(multiThreadedHttpConnectionManager());
  }

  // SAML Authentication Provider responsible for validating of received SAML
  // messages
  @Bean
  public SAMLAuthenticationProvider samlAuthenticationProvider()
  {
    SAMLAuthenticationProvider samlAuthenticationProvider = new SAMLAuthenticationProvider();
    samlAuthenticationProvider.setUserDetails(ssoUserDetailsService);
    samlAuthenticationProvider.setForcePrincipalAsString(false);
    return samlAuthenticationProvider;
  }

  // Provider of default SAML Context
  @Bean
  public SAMLContextProviderImpl contextProvider()
  {
    SAMLContextProviderImpl samlContextProvider = new SAMLContextProviderImpl();

    // http://docs.spring.io/spring-security-saml/docs/1.0.0.RELEASE/reference/html/chapter-troubleshooting.html
    // some issues when we are http://localhost and wso2 is https://sso.host message id do not match
    samlContextProvider.setStorageFactory(new EmptyStorageFactory());
    return samlContextProvider;
  }

  // Initialization of OpenSAML library
  @Bean
  public static SAMLBootstrap sAMLBootstrap()
  {
    return new SAMLBootstrap();
  }

  // Logger for SAML messages and events
  @Bean
  public SAMLDefaultLogger samlLogger()
  {
    SAMLDefaultLogger logger = new SAMLDefaultLogger();
    logger.setLogMessages(true);
    return logger;
  }

  // SAML 2.0 WebSSO Assertion Consumer
  @Bean
  public WebSSOProfileConsumer webSSOprofileConsumer()
  {
    WebSSOProfileConsumerImpl webSSOProfileConsumer = new WebSSOProfileConsumerImpl();
    webSSOProfileConsumer.setResponseSkew(ssoConfig.getResponseTimeSkewSeconds());
    // webSSOProfileConsumer.setMaxAuthenticationAge(); // 7200 ms
    return webSSOProfileConsumer;
  }

  // SAML 2.0 Holder-of-Key WebSSO Assertion Consumer
  @Bean
  public WebSSOProfileConsumerHoKImpl hokWebSSOprofileConsumer()
  {
    return new WebSSOProfileConsumerHoKImpl();
  }

  // SAML 2.0 Web SSO profile
  @Bean
  public WebSSOProfile webSSOprofile()
  {
    return new WebSSOProfileImpl();
  }

  // SAML 2.0 Holder-of-Key Web SSO profile
  @Bean
  public WebSSOProfileConsumerHoKImpl hokWebSSOProfile()
  {
    return new WebSSOProfileConsumerHoKImpl();
  }

  // SAML 2.0 ECP profile
  @Bean
  public WebSSOProfileECPImpl ecpprofile()
  {
    return new WebSSOProfileECPImpl();
  }

  @Bean
  public SingleLogoutProfile logoutprofile()
  {
    return new SingleLogoutProfileImpl();
  }

  // Central storage of cryptographic keys
  @Bean
  public KeyManager keyManager()
  {
    DefaultResourceLoader loader = new DefaultResourceLoader();
    Resource storeFile = loader.getResource(ssoConfig.getKeystore().getFilePath());
    String storePass = ssoConfig.getKeystore().getPassword();
    Map<String, String> passwords = new HashMap<>();
    passwords.put(ssoConfig.getKeystore().getCertificateName(), ssoConfig.getKeystore().getCertificatePassword());

    String defaultKey = ssoConfig.getKeystore().getEncryptionKey();

    return new JKSKeyManager(storeFile, storePass, passwords, defaultKey);
  }

  @Bean
  public WebSSOProfileOptions defaultWebSSOProfileOptions()
  {
    WebSSOProfileOptions webSSOProfileOptions = new WebSSOProfileOptions();
    webSSOProfileOptions.setIncludeScoping(false);
    return webSSOProfileOptions;
  }

  // Entry point to initialize authentication, default values taken from
  // properties file
  @Bean
  public SAMLEntryPoint samlEntryPoint()
  {
    SAMLEntryPoint samlEntryPoint = new SAMLEntryPoint();
    samlEntryPoint.setDefaultProfileOptions(defaultWebSSOProfileOptions());
    return samlEntryPoint;
  }

  // Setup advanced info about metadata
  @Bean
  @Scope("prototype")
  public ExtendedMetadata extendedMetadata()
  {
    ExtendedMetadata extendedMetadata = new ExtendedMetadata();
    extendedMetadata.setIdpDiscoveryEnabled(false);
    extendedMetadata.setSignMetadata(false);
    return extendedMetadata;
  }


  @Bean
  public FilesystemMetadataProvider idpProviderMetadataFile() throws MetadataProviderException
  {
    ClassPathResource classPathResource = new ClassPathResource(ssoConfig.getIdentityProvider().getFilePath());

    File idpFile = null;

    if (classPathResource.exists())
    {
      try
      {
        idpFile = new File(classPathResource.getURL().getFile());

        log.info("<SSOAuthenticationConfiguration> (ClassPathResource) IDP configuration: {}", idpFile.getAbsolutePath());
      }
      catch (IOException e)
      {
        log.error("IOException while reading identity provider configuration file supplied", e);
        log.error("Unable to read identity provider configuration file: {}", ssoConfig.getIdentityProvider().getFilePath());
      }
    } else
    {
      idpFile = new File(ssoConfig.getIdentityProvider().getFilePath());

      if (idpFile.exists())
      {
        log.info("<SSOAuthenticationConfiguration> (File Resource) IDP configuration: {}", idpFile.getAbsolutePath());
      } else
      {
        log.error("Unable to read SP configuration file via ClassPathResource or File, initialization will fail now.");
      }
    }

    if (idpFile != null && idpFile.canRead())
    {
      log.info("<SSOAuthenticationConfiguration> IDP configuration: {}", idpFile.getAbsolutePath());
      FilesystemMetadataProvider idpMetadata = new FilesystemMetadataProvider(idpFile);
      idpMetadata.setParserPool(parserPool());

      return idpMetadata;
    }

    throw new MetadataProviderException("Unable to load IDP file with specified path:" + ssoConfig.getIdentityProvider().getFilePath());
  }

  @Bean
  public FilesystemMetadataProvider SPProviderMetadataFile() throws MetadataProviderException
  {
    ClassPathResource classPathResource = new ClassPathResource(ssoConfig.getServiceProvider().getFilePath());

    File spFile = null;

    if (classPathResource.exists())
    {
      try
      {
        spFile = new File(classPathResource.getURL().getFile());

        log.info("<SSOAuthenticationConfiguration> (ClassPathResource) SP configuration: {}", spFile.getAbsolutePath());
      }
      catch (IOException e)
      {
        log.error("IOException while reading service provider configuration file supplied", e);
        log.error("Unable to read service provider configuration file: {}", ssoConfig.getServiceProvider().getFilePath());
      }
    } else
    {
      spFile = new File(ssoConfig.getServiceProvider().getFilePath());

      if (spFile.exists())
      {
        log.info("<SSOAuthenticationConfiguration> (File Resource) SP configuration: {}", spFile.getAbsolutePath());

        FilesystemMetadataProvider spMetadata = new FilesystemMetadataProvider(spFile);
        spMetadata.setParserPool(parserPool());
        return spMetadata;
      } else
      {
        log.error("Unable to read SP configuration file via ClassPathResource or File, initialization will fail now.");
      }
    }


    if (spFile != null && spFile.canRead())
    {
      log.info("<SSOAuthenticationConfiguration> SP configuration: {}", spFile.getAbsolutePath());
      FilesystemMetadataProvider spMetadata = new FilesystemMetadataProvider(spFile);
      spMetadata.setParserPool(parserPool());

      return spMetadata;
    }

    throw new MetadataProviderException("Unable to load SP file with specified path:" + ssoConfig.getServiceProvider().getFilePath());
  }

  @Bean
  public ExtendedMetadataDelegate idpExtendedMetadataProvider()
      throws MetadataProviderException
  {
    ExtendedMetadataDelegate extendedMetadataDelegate =
        new ExtendedMetadataDelegate(idpProviderMetadataFile(), extendedMetadata());
    extendedMetadataDelegate.setMetadataTrustCheck(true);
    extendedMetadataDelegate.setMetadataRequireSignature(false);
    return extendedMetadataDelegate;
  }

  @Bean
  public ExtendedMetadataDelegate spMetadataProvider()
      throws MetadataProviderException
  {
    ExtendedMetadata extMetadata = extendedMetadata();
    extMetadata.setAlias(ssoConfig.getServiceProvider().getAlias());
    extMetadata.setLocal(true);
    extMetadata.setSecurityProfile("metaiop");
    extMetadata.setSslSecurityProfile("metaiop");
    extMetadata.setSslHostnameVerification("default");
    extMetadata.setSignMetadata(false);
    extMetadata.setSigningKey(ssoConfig.getKeystore().getSigningKey());
    extMetadata.setEncryptionKey(ssoConfig.getKeystore().getEncryptionKey());
    extMetadata.setIdpDiscoveryEnabled(false);
    extMetadata.setRequireArtifactResolveSigned(false);
    extMetadata.setRequireLogoutRequestSigned(false);
    extMetadata.setRequireLogoutResponseSigned(false);
    ExtendedMetadataDelegate extendedMetadataDelegate = new ExtendedMetadataDelegate(SPProviderMetadataFile(), extMetadata);
    extendedMetadataDelegate.setMetadataTrustCheck(true);
    extendedMetadataDelegate.setMetadataRequireSignature(false);

    return extendedMetadataDelegate;
  }

  // IDP Metadata configuration - paths to metadata of IDPs in circle of trust
  // is here
  // Do no forget to call iniitalize method on providers
  @Bean
  @Qualifier("metadata")
  public CachingMetadataManager metadata() throws MetadataProviderException
  {
    List<MetadataProvider> providers = new ArrayList<>();
    providers.add(idpExtendedMetadataProvider());
    providers.add(spMetadataProvider());
    return new CachingMetadataManager(providers);
  }

  // The filter is waiting for connections on URL suffixed with filterSuffix
  // and presents SP metadata there
  @Bean
  public MetadataDisplayFilter metadataDisplayFilter()
  {
    return new MetadataDisplayFilter();
  }

  // Handler deciding where to redirect user after successful login
  @Bean
  public SavedRequestAwareAuthenticationSuccessHandler successRedirectHandler()
  {
    SavedRequestAwareAuthenticationSuccessHandler successRedirectHandler =
        new SavedRequestAwareAuthenticationSuccessHandler();
    successRedirectHandler.setDefaultTargetUrl(ssoConfig.getLogin().getSuccessUrl());
    return successRedirectHandler;
  }

  // Handler deciding where to redirect user after failed login
  @Bean
  public SimpleUrlAuthenticationFailureHandler authenticationFailureHandler()
  {
    SimpleUrlAuthenticationFailureHandler failureHandler =
        new SimpleUrlAuthenticationFailureHandler();
    failureHandler.setUseForward(true);
    failureHandler.setDefaultFailureUrl(ssoConfig.getLogin().getFailureUrl());
    return failureHandler;
  }

  // Processing filter for WebSSO profile messages
  @Bean
  public SAMLProcessingFilter samlWebSSOProcessingFilter() throws Exception
  {
    SAMLProcessingFilter samlWebSSOProcessingFilter = new SAMLProcessingFilter();
    samlWebSSOProcessingFilter.setAuthenticationManager(authenticationManager());
    samlWebSSOProcessingFilter.setAuthenticationSuccessHandler(successRedirectHandler());
    samlWebSSOProcessingFilter.setAuthenticationFailureHandler(authenticationFailureHandler());
    return samlWebSSOProcessingFilter;
  }

  // Handler for successful logout
  @Bean
  public SimpleUrlLogoutSuccessHandler successLogoutHandler()
  {
    SimpleUrlLogoutSuccessHandler successLogoutHandler = new SimpleUrlLogoutSuccessHandler();

//    if(ssoConfig.getLogout().isCreateLogoutController())
//    {
//      successLogoutHandler.setDefaultTargetUrl("/ssolib/logout");
//    }
//    else
//    {
      successLogoutHandler.setDefaultTargetUrl("/");
//    }

    return successLogoutHandler;
  }

  // Logout handler terminating local session
  @Bean
  public SecurityContextLogoutHandler logoutHandler()
  {
    SecurityContextLogoutHandler logoutHandler =
        new SecurityContextLogoutHandler();
    logoutHandler.setInvalidateHttpSession(true);
    logoutHandler.setClearAuthentication(true);
    return logoutHandler;
  }

  // Filter processing incoming logout messages
  // First argument determines URL user will be redirected to after successful
  // global logout
  @Bean
  public SAMLLogoutProcessingFilter samlLogoutProcessingFilter()
  {
    return new SAMLLogoutProcessingFilter(successLogoutHandler(),
        logoutHandler());
  }

  // Overrides default logout processing filter with the one processing SAML
  // messages
  @Bean
  public SAMLLogoutFilter samlLogoutFilter()
  {
    return new SAMLLogoutFilter(successLogoutHandler(),
        new LogoutHandler[]{logoutHandler()},
        new LogoutHandler[]{logoutHandler()});
  }

  // Bindings
  private ArtifactResolutionProfile artifactResolutionProfile()
  {
    final ArtifactResolutionProfileImpl artifactResolutionProfile =
        new ArtifactResolutionProfileImpl(httpClient());
    artifactResolutionProfile.setProcessor(new SAMLProcessorImpl(soapBinding()));
    return artifactResolutionProfile;
  }

  @Bean
  public HTTPArtifactBinding artifactBinding(ParserPool parserPool, VelocityEngine velocityEngine)
  {
    return new HTTPArtifactBinding(parserPool, velocityEngine, artifactResolutionProfile());
  }

  @Bean
  public HTTPSOAP11Binding soapBinding()
  {
    return new HTTPSOAP11Binding(parserPool());
  }

  @Bean
  public HTTPPostBinding httpPostBinding()
  {
    return new HTTPPostBinding(parserPool(), velocityEngine());
  }

  @Bean
  public HTTPRedirectDeflateBinding httpRedirectDeflateBinding()
  {
    return new HTTPRedirectDeflateBinding(parserPool());
  }

  @Bean
  public HTTPSOAP11Binding httpSOAP11Binding()
  {
    return new HTTPSOAP11Binding(parserPool());
  }

  @Bean
  public HTTPPAOS11Binding httpPAOS11Binding()
  {
    return new HTTPPAOS11Binding(parserPool());
  }

  // Processor
  @Bean
  public SAMLProcessorImpl processor()
  {
    Collection<SAMLBinding> bindings = new ArrayList<>();
    bindings.add(httpRedirectDeflateBinding());
    bindings.add(httpPostBinding());
    bindings.add(artifactBinding(parserPool(), velocityEngine()));
    bindings.add(httpSOAP11Binding());
    bindings.add(httpPAOS11Binding());
    return new SAMLProcessorImpl(bindings);
  }

  /**
   * Define the security filter chain in order to support SSO Auth by using SAML 2.0
   *
   * @return Filter chain proxy
   * @throws Exception if any exceptions occurrs
   */
  @Bean
  public FilterChainProxy samlFilter() throws Exception
  {
    List<SecurityFilterChain> chains = new ArrayList<>();

    chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/login/**"), samlEntryPoint()));
    chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/logout/**"), samlLogoutFilter()));
    chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/metadata/**"), metadataDisplayFilter()));
    chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SSO/**"), samlWebSSOProcessingFilter()));
    chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SingleLogout/**"), samlLogoutProcessingFilter()));
    return new FilterChainProxy(chains);
  }

  /**
   * Returns the authentication manager currently used by Spring.
   * It represents a bean definition with the aim allow wiring from
   * other classes performing the Inversion of Control (IoC).
   *
   * @throws Exception if any exceptions occurrs
   */
  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception
  {
    return super.authenticationManagerBean();
  }

}
