package com.getinsured.ssolib.service;

import java.util.HashMap;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.getinsured.ssolib.user.SSOUser;

/**
 * Contains utility methods to get currently logged in user information.
 *
 * @author Yevgen Golubenko
 * @since 9/7/17
 */
public class CurrentUserService
{
  /**
   * Get information about currently logged in user.
   *
   * @return {@link SSOUser} object or null if user not logged in.
   */
  public SSOUser getLoggedInUser()
  {
    SecurityContext secContext = SecurityContextHolder.getContext();

    if(secContext != null)
    {
      Authentication auth = secContext.getAuthentication();

      if(auth != null && auth.isAuthenticated())
      {
        SSOUser u = null;

        if(auth.getPrincipal() != null)
        {
          u = (SSOUser) auth.getPrincipal();

          return u;
        }
      }
    }

    return null;
  }

  /**
   * Returns username of the logged in user.
   *
   * @return username, or null.
   */
  public String getLoggedInUsername()
  {
    SSOUser u = getLoggedInUser();

    if(u == null)
    {
      return null;
    }

    return u.getName();
  }

  /**
   * Returns user id of logged in user.
   * <p>
   *   userid returned, is equivalent to SCIM user id on wso2 server
   *   as well as {@code extn_app_userid} in GI {@code users} table.
   * </p>
   * @return user id or null.
   */
  public String getLoggedInUserId()
  {
    SSOUser u = getLoggedInUser();

    if(u == null)
    {
      return null;
    }

    HashMap<String, String> userAttributes = u.getUserAttributesMap();
    return userAttributes.get("http://wso2.org/claims/userid");
  }

  /**
   * Returns tenant id of logged in user.
   * @return tenant id or null.
   */
  public String getLoggedInTenantId()
  {
    SSOUser u = getLoggedInUser();

    if(u == null)
    {
      return null;
    }

    HashMap<String, String> userAttributes = u.getUserAttributesMap();
    return userAttributes.get("http://wso2.org/claims/giTenantId");
  }

  /**
   * Returns first name of logged in user.
   * @return first name or null.
   */
  public String getLoggedInFirstName()
  {
    SSOUser u = getLoggedInUser();

    if(u == null)
    {
      return null;
    }

    HashMap<String, String> userAttributes = u.getUserAttributesMap();
    return userAttributes.get("http://wso2.org/claims/givenname");
  }

  /**
   * Retruns last name of logged in user.
   * @return last name or null.
   */
  public String getLoggedInLastName()
  {
    SSOUser u = getLoggedInUser();

    if(u == null)
    {
      return null;
    }

    HashMap<String, String> userAttributes = u.getUserAttributesMap();
    return userAttributes.get("http://wso2.org/claims/lastname");
  }
}
