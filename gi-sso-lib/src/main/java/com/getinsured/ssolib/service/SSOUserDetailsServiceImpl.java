package com.getinsured.ssolib.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;

import com.getinsured.ssolib.config.SSOConfig;
import com.getinsured.ssolib.user.SSOUser;

/**
 *
 * @author Yevgen Golubenko
 * @since 4/5/17
 */
@Service
public class SSOUserDetailsServiceImpl implements SAMLUserDetailsService
{
  private static final Logger log = LoggerFactory.getLogger(SSOUserDetailsServiceImpl.class);
  private CloseableHttpClient httpClient = HttpClients.createDefault();
  private static final String ID_ATTR_NAME = "http://wso2.org/claims/userid";
  @Autowired
  private SSOConfig ssoConfig;
  /**
   * The method is supposed to identify local account of user referenced by data in the SAML assertion
   * and return UserDetails object describing the user. In case the user has no local account, implementation
   * may decide to create one or just populate UserDetails object with data from assertion.
   * <p>
   * Returned object should correctly implement the getAuthorities method as it will be used to populate
   * entitlements inside the Authentication object.
   *
   * @param credential data populated from SAML message used to validate the user
   *
   * @return a fully populated user record (never <code>null</code>)
   *
   * @throws UsernameNotFoundException if the user details object can't be populated
   */
  @Override
  public Object loadUserBySAML(final SAMLCredential credential) throws UsernameNotFoundException
  {
	List<Attribute> attributes = credential.getAttributes();
    Assertion assertion = credential.getAuthenticationAssertion();
    //List<AttributeStatement> attributeStatements = assertion.getAttributeStatements();
    Set<GrantedAuthority> authorities = null;
    try
    {
      authorities = this.extractAuthorities(attributes);
    }
    catch (Exception e)
    {
      log.error("Unable to extract Authorities from SAML attribute statements", e);
    }

    AuthnStatement statement = assertion.getAuthnStatements().get(0);
    
    SSOUser user = new SSOUser(assertion.getSubject().getNameID().getValue(),
        null, assertion.getIssuer().getValue(), null,
        assertion.getID(), null, assertion.getIssueInstant(),
        statement.getAuthnInstant(), authorities, attributes
    );

    if (log.isInfoEnabled())
    {
      log.info("Received User Object from WSO2 authentication: " + user);
    }

    return user;
  }

  private Set<GrantedAuthority> extractAuthorities(
      List<Attribute> attributes) throws Exception
  {
    Set<GrantedAuthority> authorities = new HashSet<>();
    log.info("Initializing permissions");
    String externalAppUserId = null;
    if(attributes == null)
    {
    		return authorities;
    }
    String attrName = null;
    for (Attribute attribute : attributes)
    {
    	  attrName = attribute.getName();
	  if(attrName.equals(ID_ATTR_NAME))
	  {
	    	for (XMLObject xmlObj : attribute.getAttributeValues())
	    	{
	    		if (xmlObj instanceof XSString)
	    		{
	    			externalAppUserId = ((XSString) xmlObj).getValue();
	    			getUserPermissions(externalAppUserId, authorities);
	    		}
	    	}
	    	break;
	  }
    }
    return authorities;
  }
  
  private void getUserPermissions(String id, Set<GrantedAuthority> authorities){
	  HttpEntity entity = null;
	  CloseableHttpResponse response = null;
	  HttpGet serverHandle = new HttpGet(ssoConfig.getAuthorizationProvider().getAuthProviderUrl()+"/"+id);
	  log.info("Connecting to authorization server with URL:"+serverHandle.getURI());
	  try {
		response = this.httpClient.execute(serverHandle);
		int code = response.getStatusLine().getStatusCode();
		if(code == HttpStatus.SC_OK)
		{
			entity = response.getEntity();
			InputStreamReader bis = new InputStreamReader(entity.getContent());
			JSONParser parser = new JSONParser();
			JSONObject responseObj = (JSONObject) parser.parse(bis);
			JSONArray permissionArray = (JSONArray) responseObj.get("permissions");
			int permissionCount = permissionArray.size();
			log.info("Received ["+permissionCount+"] permissions for id:"+responseObj.get("id"));
			String permission = null;
			for(int i = 0; i < permissionCount; i++ )
			{
				permission = (String) permissionArray.get(i);
				if(log.isDebugEnabled())
				{
					log.debug("Added permission "+permission+" as granted authority");
				}
				authorities.add(new SimpleGrantedAuthority(permission));
			}
			EntityUtils.consumeQuietly(entity);
		}else{
			log.error("Error response received from server while fetching permissions for user:"+id+" \nErrorMessage:"+this.retrieveErrorResponse(response));
		}
	} 
	catch (IOException | ParseException e) 
	{
		log.error("Error encountered while fetching permission for user id:"+id,e);
	}
	finally
	{
		serverHandle.releaseConnection();
		try 
		{
			if(response != null){
				response.close();
			}
		} catch (IOException e) {
			//Ignored
		}
	}
  }
  
  private String retrieveErrorResponse(CloseableHttpResponse response){
	  HttpEntity entity = response.getEntity();
	  String errorMsg = "";
	  try
	  {
		BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
		StringBuilder builder= new StringBuilder();
		String str = null;
		while((str = br.readLine()) != null){
			builder.append(str);
		}
		str = builder.toString();
		log.info("Received Error response:"+str);
		JSONParser parser = new JSONParser();
		JSONObject responseObj = (JSONObject) parser.parse(str);
		errorMsg = (String)responseObj.get("error_message");
		EntityUtils.consumeQuietly(entity);
	  } 
	  catch (IOException | ParseException e) 
	  {
		log.warn("Exception encountered while fetching the error response",e);
	  }
	  return errorMsg;
  }
  
}
