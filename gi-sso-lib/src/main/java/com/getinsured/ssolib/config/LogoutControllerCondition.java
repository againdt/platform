package com.getinsured.ssolib.config;

import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Condition to check if we need to create LogoutController in the lib.
 *
 * @author Yevgen Golubenko
 * @since 8/28/17
 */
public class LogoutControllerCondition extends SpringBootCondition
{
  @Override
  public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata)
  {
    boolean createLogoutController = createLogoutControllerConfiguration(context, "sso.logout.");
    return new ConditionOutcome(createLogoutController, "LogoutController created by the ssolib");
  }

  private boolean createLogoutControllerConfiguration(ConditionContext context, String prefix) {
    RelaxedPropertyResolver resolver = new RelaxedPropertyResolver(
        context.getEnvironment(), prefix);
    return resolver.getProperty("create-logout-controller", Boolean.class, true);
  }
}
