package com.getinsured.ssolib.config;

import java.util.Arrays;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * SSO Configuration.
 *
 * @author Yevgen Golubenko
 * @since 4/5/17
 */
@ConfigurationProperties(prefix = "sso")
@Component("ssoConfig")
public class SSOConfig
{
  private Boolean enabled = Boolean.FALSE;
  private String[] passForPermissionsWithoutSso;
  private String[] permitAllUrls;
  private Keystore keystore = new Keystore();
  private ServiceProvider serviceProvider = new ServiceProvider();
  private IdentityProvider identityProvider = new IdentityProvider();
  private Login login = new Login();
  private Logout logout = new Logout();
  private AuthorizationProvider authorizationProvider;// = new AuthorizationProvider();
  private Integer responseTimeSkewSeconds = 60;

  /**
   * Configuration for Java Keystore.
   */
  public static class Keystore
  {
    private String filePath = "classpath:/saml/samlKeystore.jks";

    private String password = "nalle123";
    private String certificateName = "gi_hosted_sp";
    private String certificatePassword = "nalle123";
    private String signingKey = "gi_hosted_sp";
    private String encryptionKey = "gi_hosted_sp";

    /**
     * Returns path to a javakeystore.jks file to be used.
     *
     * @return path to a java keystore.jks file to use.
     */
    public String getFilePath()
    {
      return filePath;
    }

    /**
     * Sets path of the javakeystore.jks file to use.
     *
     * @param filePath full path to a java keystore.jks file.
     */
    public void setFilePath(final String filePath)
    {
      this.filePath = filePath;
    }

    /**
     * Returns password to be used to access given java keystore file.
     *
     * @return password to be used to access java keystore file.
     */
    public String getPassword()
    {
      return password;
    }

    /**
     * Sets password to use to access protected java keystore.jsk file.
     *
     * @param password password for given java keystore.jks file.
     */
    public void setPassword(final String password)
    {
      this.password = password;
    }

    /**
     * Returns name of the certificate within given java keystore.jks file to be used
     * for encryption/decryption.
     *
     * @return name of the certificate.
     */
    public String getCertificateName()
    {
      return certificateName;
    }

    /**
     * Sets the name of the certificate to use within given java keystore.jks file.
     *
     * @param certificateName name of the certificate to use.
     */
    public void setCertificateName(final String certificateName)
    {
      this.certificateName = certificateName;
    }

    /**
     * Returns password for given certificate.
     *
     * @return password for given certifiate.
     */
    public String getCertificatePassword()
    {
      return certificatePassword;
    }

    /**
     * Sets certifiate password to be used to access given certificate within given java keystore.jks file.
     *
     * @param certificatePassword certifiate password to use.
     */
    public void setCertificatePassword(final String certificatePassword)
    {
      this.certificatePassword = certificatePassword;
    }

    /**
     * Name of the key to use to sign SAML payloads.
     *
     * @return name of signining key.
     */
    public String getSigningKey()
    {
      return signingKey;
    }

    /**
     * Sets signing key to use to sign SAML payloads.
     *
     * @param signingKey signing key to use.
     */
    public void setSigningKey(final String signingKey)
    {
      this.signingKey = signingKey;
    }

    /**
     * Sets encryption key to use to sign SAML payloads.
     *
     * @return name of the encryption key to use.
     */
    public String getEncryptionKey()
    {
      return encryptionKey;
    }

    /**
     * Sets encryption key to use to sign SAML payloads.
     *
     * @param encryptionKey encryption key to use to sign SAML payloads.
     */
    public void setEncryptionKey(final String encryptionKey)
    {
      this.encryptionKey = encryptionKey;
    }

    @Override
    public String toString()
    {
      return "Keystore{" +
          "filePath='" + filePath + '\'' +
          ", password='" + (password != null ? "******" : null) + '\'' +
          ", certificateName='" + certificateName + '\'' +
          ", certificatePassword='" + (certificatePassword != null ? "******" : null) + '\'' +
          ", signingKey='" + signingKey + '\'' +
          ", encryptionKey='" + encryptionKey + '\'' +
          '}';
    }
  }

  /**
   * Service provider configuration.
   */
  public static class ServiceProvider
  {
    private String filePath;  // "ms-starr_sp.xml"
    private String alias; // MS-STARR

    /**
     * Returns service provider XML configuration file path.
     *
     * @return file path of Service Provider XML configuration.
     */
    public String getFilePath()
    {
      return filePath;
    }

    /**
     * Sets Service Provider XML configuration file path.
     *
     * @param filePath Service Provider's XML configuration file path.
     */
    public void setFilePath(final String filePath)
    {
      this.filePath = filePath;
    }

    /**
     * Returns Service Provider alias.
     *
     * @return Service Provider alias name.
     */
    public String getAlias()
    {
      return alias;
    }

    /**
     * Sets Service Provider alias (must be configured on WSO2).
     *
     * @param alias Service Provider alias to set.
     */
    public void setAlias(final String alias)
    {
      this.alias = alias;
    }

    @Override
    public String toString()
    {
      return "ServiceProvider{" +
          "filePath='" + filePath + '\'' +
          ", alias='" + alias + '\'' +
          '}';
    }
  }

  /**
   * Identity Provider configuration.
   */
  public static class IdentityProvider
  {
    private String filePath = "/idp/sso.ghixqa.com.xml"; // idp.

    /**
     * Returns Identity Provider's XML configuration file path.
     * @return Identity Provider XML configuration file path.
     */
    public String getFilePath()
    {
      return filePath;
    }

    /**
     * Sets Identity Provider's XML configuration file path.
     * @param filePath full path to Identity Provider's XML configuration.
     */
    public void setFilePath(final String filePath)
    {
      this.filePath = filePath;
    }

    @Override
    public String toString()
    {
      return "IdentityProvider{" +
          "filePath='" + filePath + '\'' +
          '}';
    }
  }

  /**
   * Authorization Configuration.
   */
  public static class AuthorizationProvider
  {
    private String authProviderUrl;// "http://localhost:8081/gi-identity/user/permissions";

    /**
     * Return Auth Provider URL to be used to get permissions for logged in user.
     *
     * @return auth provider URL to be used to retrieve permissions for user.
     */
    public String getAuthProviderUrl()
    {
      return authProviderUrl;
    }

    /**
     * Sets Auth Provider URL to be used to get permissions for logged in user.
     *
     * @param url sets Auth Provider URL to be used to get permissions for logged in user.
     */
    public void setAuthProviderUrl(final String url)
    {
      this.authProviderUrl = url;
    }

    @Override
    public String toString()
    {
      final StringBuffer sb = new StringBuffer("AuthorizationProvider{");
      sb.append("authProviderURL='").append(authProviderUrl).append('\'');
      sb.append('}');
      return sb.toString();
    }
  }

  /**
   * Login Configuration.
   */
  public static class Login
  {
    public static final String DEFAULT_LOGIN_LOGOUT_URL = "/ssolib/logout";
    private String successUrl;
    private String failureUrl = DEFAULT_LOGIN_LOGOUT_URL;
    private String accessDeniedUrl = "/account/user/denied";

    /**
     * Returns URL to be used after successfull login.
     *
     * @return URL to be used after successfull login.
     */
    public String getSuccessUrl()
    {
      return successUrl;
    }

    /**
     * Sets URL to be used after successfull login.
     * @param successUrl URL to be used after successfull login.
     */
    public void setSuccessUrl(final String successUrl)
    {
      this.successUrl = successUrl;
    }

    /**
     * Returns URL to be used after failed login.
     * @return URL to be used after failed login.
     */
    public String getFailureUrl()
    {
      return failureUrl;
    }

    /**
     * Sets URL to be used after failed login.
     * @param failureUrl URL to be used after failed login.
     */
    public void setFailureUrl(final String failureUrl)
    {
      this.failureUrl = failureUrl;
    }

    /**
     * Returns URL to be used after access to some resource was denied.
     *
     * @return URL to be used after access to some resource was denied.
     */
    public String getAccessDeniedUrl()
    {
      return accessDeniedUrl;
    }

    /**
     * Sets URL to be used after access to some resource was denied.
     * @param accessDeniedUrl URL to be used after access to some resource was denied.
     */
    public void setAccessDeniedUrl(final String accessDeniedUrl)
    {
      this.accessDeniedUrl = accessDeniedUrl;
    }

    @Override
    public String toString()
    {
      return "Login{" +
          "successUrl='" + successUrl + '\'' +
          ", failureUrl='" + failureUrl + '\'' +
          ", accessDeniedUrl='" + accessDeniedUrl + '\'' +
          '}';
    }
  }

  /**
   * Logout Configuration.
   */
  public static class Logout
  {
    private String logoutUrl = "/saml/logout";
    private String[] removeCookies = new String[]{"JSESSIONID", "SPRING_SECURITY_REMEMBER_ME_COOKIE", "SESSION"};
    private boolean createLogoutController = false;

    /**
     * Returns SAML logout URL, default is {@code /saml/logout}
     *
     * @return SAML logout URL to be used to initiate a logout process.
     */
    public String getLogoutUrl()
    {
      return logoutUrl;
    }

    /**
     * SAML logout URL to be used to initiate a logout process.
     *
     * @param logoutUrl SAML logout URL to be used to initiate a logout process.
     */
    public void setLogoutUrl(final String logoutUrl)
    {
      this.logoutUrl = logoutUrl;
    }

    /**
     * Returns list of cookies to remove upon successfull logout process. Default values are:
     * <ul>
     *   <li>JSESSIONID</li>
     *   <li>SPRING_SECURITY_REMEMBER_ME_COOKIE</li>
     *   <li>SESSION</li>
     * </ul>
     *
     * @return list of cookies to be removed after logout is successfull.
     */
    public String[] getRemoveCookies()
    {
      return removeCookies;
    }

    /**
     * Sets names of the cookies to remove after logout is successfull.
     *
     * @param removeCookies cookies to remove after logout is successfull.
     */
    public void setRemoveCookies(final String[] removeCookies)
    {
      this.removeCookies = removeCookies;
    }

    /**
     * Returns true if logout controller doesn't exist in application and should be created by the library.
     *
     * @return boolean value indicating if logout controller doesn't exist in application and should be created by the library.
     */
    public boolean isCreateLogoutController()
    {
      return createLogoutController;
    }

    /**
     * Sets boolean value to tell library if logout controller needs to be created by the library.
     *
     * @param createLogoutController boolean value to tell library if logout controller needs to be created by the library.
     */
    public void setCreateLogoutController(boolean createLogoutController)
    {
      this.createLogoutController = createLogoutController;
    }

    @Override
    public String toString()
    {
      final StringBuffer sb = new StringBuffer("Logout{");
      sb.append("logoutUrl='").append(logoutUrl).append('\'');
      sb.append(", removeCookies=").append(removeCookies == null ? "null" : Arrays.asList(removeCookies).toString());
      sb.append(", createLogoutController=").append(createLogoutController);
      sb.append('}');
      return sb.toString();
    }
  }

  /**
   * Returns boolean value indicating if SSO auth is enabled or not.
   *
   * @return boolean value indicating if SSO auth is enabled or not.
   */
  public Boolean getEnabled()
  {
    return enabled;
  }

  /**
   * Sets SSO auth to be enabled or disabled.
   *
   * @param enabled boolean value that idicates if SSO auth to be enabled or disabled.
   */
  public void setEnabled(final Boolean enabled)
  {
    this.enabled = enabled;
  }

  /**
   * Returns {@link Keystore} configuration.
   *
   * @return {@link Keystore} configuration.
   */
  public Keystore getKeystore()
  {
    return keystore;
  }

  /**
   * Sets {@link Keystore} configuration.
   *
   * @param keystore {@link Keystore} configuration to use.
   */
  public void setKeystore(final Keystore keystore)
  {
    this.keystore = keystore;
  }

  /**
   * Returns {@link ServiceProvider} configuration.
   * @return {@link ServiceProvider} configuration.
   */
  public ServiceProvider getServiceProvider()
  {
    return serviceProvider;
  }

  /**
   * Sets {@link ServiceProvider} configuration.
   * @param serviceProvider {@link ServiceProvider} configuration to use.
   */
  public void setServiceProvider(final ServiceProvider serviceProvider)
  {
    this.serviceProvider = serviceProvider;
  }

  /**
   * Returns {@link IdentityProvider} configuration.
   * @return {@link IdentityProvider} configuration.
   */
  public IdentityProvider getIdentityProvider()
  {
    return identityProvider;
  }

  /**
   * Sets {@link IdentityProvider} configuration.
   * @param identityProvider {@link IdentityProvider} configuration to use.
   */
  public void setIdentityProvider(final IdentityProvider identityProvider)
  {
    this.identityProvider = identityProvider;
  }

  /**
   * Returns {@link AuthorizationProvider} configuration.
   * @return {@link AuthorizationProvider} configuration.
   */
  public AuthorizationProvider getAuthorizationProvider(){
	  return this.authorizationProvider;
  }

  /**
   * Sets {@link AuthorizationProvider} configuration.
   * @param provider {@link AuthorizationProvider} configuration to use.
   */
  public void setAuthorizationProvider(final AuthorizationProvider provider){
	  this.authorizationProvider = provider;
  }

  /**
   * Retunrs {@link Login} configuration.
   *
   * @return {@link Login} configuration.
   */
  public Login getLogin()
  {
    return login;
  }

  /**
   * Sets {@link Login} configuration.
   *
   * @param login {@link Login} configuration to use.
   */
  public void setLogin(final Login login)
  {
    this.login = login;
  }

  /**
   * Returns {@link Logout} configuration.
   *
   * @return {@link Logout} configuration.
   */
  public Logout getLogout()
  {
    return logout;
  }

  /**
   * Sets {@link Logout} configuration.
   * @param logout {@link Logout} configuration to use.
   */
  public void setLogout(final Logout logout)
  {
    this.logout = logout;
  }

  /**
   * Returns list of permissions to ignore and pass-through when SSO is disabled and
   * underlaying component does not correctly implement permission checking.
   * <p>
   *   In other words, xyz-component has controller with
   *   <code>
   *     <pre>
   *     @RequestMapping("/somepage")
   *     @PreAuthorize("hasPermission('ABC')")
   *     public String somePage() { ...  }
   *     </pre>
   *   </code>
   *   In this case, if SSO is enabled, then when logged-in user tries to access it, permissions will
   *   be checked by the library and permission list will be available to the application, so that if {@code ABC}
   *   permission exists, it will work.
   *   <br/><br/>
   *   Now, if SSO is disabled, and application does not have logic/code/means to figure out logged-in user's
   *   permissions, permission list will be empty, so method will not be accessible by logged in user. This
   *   is scenario with ms-starr module, which uses it's own  username/password as 'admin/somepass' and allows
   *   'admin' to access everything not taking into account <code>@PreAuthorize()</code> settings.
   * </p>
   *
   * @return list of permissions to pass-through if sso is disabled.
   */
  public String[] getPassForPermissionsWithoutSso()
  {
    return passForPermissionsWithoutSso;
  }

  /**
   * Sets list of permissions to pass-through if sso is disabled.
   *
   * @see #getPassForPermissionsWithoutSso()
   * @param passForPermissionsWithoutSso list of permissions to pass through if sso is disabled.
   */
  public void setPassForPermissionsWithoutSso(final String[] passForPermissionsWithoutSso)
  {
    this.passForPermissionsWithoutSso = passForPermissionsWithoutSso;
  }

  /**
   * Returns list of URLs to add to permitAll() list.
   *
   * @return list of URLs to add to permitAll() list.
   */
  public String[] getPermitAllUrls()
  {
    return permitAllUrls;
  }

  /**
   * Sets list of URL's to add to permitAll() list.
   *
   * @param permitAllUrls list of URL's to add to permitAll() list.
   */
  public void setPermitAllUrls(String[] permitAllUrls)
  {
    this.permitAllUrls = permitAllUrls;
  }


  /**
   * Validates that all required properties are present and throws exception if
   * some of the properties are not present.
   *
   * @return true if validation passed, false otherwise
   */
  public boolean validate()
  {
    StringBuilder sb = new StringBuilder();

    // Keystore
    if (this.getKeystore() == null)
    {
      sb.append("sso.keystore - section not present [").append(this.getKeystore()).append("]\n");
    } else
    {
      if (this.getKeystore().getFilePath() == null)
      {
        sb.append("sso.keystore.filePath - property not present [").append(this.getKeystore().getFilePath()).append("]\n");
      }

      if (this.getKeystore().getCertificateName() == null)
      {
        sb.append("sso.keystore.certificateName - property not present [").append(this.getKeystore().getCertificateName()).append("]\n");
      }

      if (this.getKeystore().getSigningKey() == null)
      {
        sb.append("sso.keystore.signingKey - property not present [").append(this.getKeystore().getSigningKey()).append("]\n");
      }

      if (this.getKeystore().getEncryptionKey() == null)
      {
        sb.append("sso.keystore.encryptionKey - property not present [").append(this.getKeystore().getEncryptionKey()).append("]\n");
      }
    }

    // Service Provider
    if (this.getServiceProvider() == null)
    {
      sb.append("sso.serviceProvider - section not present [").append(this.getServiceProvider()).append("]\n");
    } else
    {
      if (this.getServiceProvider().getFilePath() == null)
      {
        sb.append("sso.serviceProvider.filePath - property not present [").append(this.getServiceProvider().getFilePath()).append("]\n");
      }

      // TODO: Get alias automatically from XML just like I do in platform
      if (this.getServiceProvider().getAlias() == null)
      {
        sb.append("sso.serviceProvider.alias - property not present [").append(this.getServiceProvider().getAlias()).append("]\n");
      }
    }

    // Identity Provider
    if (this.getIdentityProvider() == null)
    {
      sb.append("sso.identityProvider - section not present [").append(this.getIdentityProvider()).append("]\n");
    } else
    {
      if (this.getIdentityProvider().getFilePath() == null)
      {
        sb.append("sso.identityProvider.filePath - property not present [").append(this.getIdentityProvider().getFilePath()).append("]\n");
      }
    }

    // Authorization Provider
    if (this.getAuthorizationProvider() == null)
    {
      sb.append("sso.authenticationProvider - section not present [").append(this.getAuthorizationProvider()).append("]\n");
    } else
    {
      if (this.getAuthorizationProvider().getAuthProviderUrl() == null)
      {
        sb.append("sso.authenticationProvider.authProviderURL - property not present [").append(this.getAuthorizationProvider().getAuthProviderUrl()).append("]\n");
      }
    }

    // Login
    if (this.getLogin() == null)
    {
      sb.append("sso.login - section not present [").append(this.getLogin()).append("]\n");
    } else
    {
      if (this.getLogin().getSuccessUrl() == null)
      {
        sb.append("sso.login.successUrl - property not present [").append(this.getLogin().getSuccessUrl()).append("]\n");
      }
    }

    if (sb.length() > 0)
    {
      throw new RuntimeException(sb.toString());
    }

    return true;
  }

  /**
   * Returns max response time skew in seconds.
   *
   * @return maximum response time skew in seconds.
   * @since 1.5
   */
  public Integer getResponseTimeSkewSeconds()
  {
    return responseTimeSkewSeconds;
  }

  /**
   * Sets max allowed response time skew in seconds.
   *
   * @param responseTimeSkewSeconds seconds.
   * @since 1.5
   */
  public void setResponseTimeSkewSeconds(Integer responseTimeSkewSeconds)
  {
    this.responseTimeSkewSeconds = responseTimeSkewSeconds;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer("SSOConfig{");
    sb.append("enabled=").append(enabled);
    sb.append(", passForPermissionsWithoutSso=").append(passForPermissionsWithoutSso == null ? "null" : Arrays.asList(passForPermissionsWithoutSso).toString());
    sb.append(", permitAllUrls=").append(permitAllUrls == null ? "null" : Arrays.asList(permitAllUrls).toString());
    sb.append(", keystore=").append(keystore);
    sb.append(", serviceProvider=").append(serviceProvider);
    sb.append(", identityProvider=").append(identityProvider);
    sb.append(", login=").append(login);
    sb.append(", logout=").append(logout);
    sb.append(", authorizationProvider=").append(authorizationProvider);
    sb.append('}');
    return sb.toString();
  }
}
