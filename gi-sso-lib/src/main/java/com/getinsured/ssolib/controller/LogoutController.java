package com.getinsured.ssolib.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.common.SAMLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.getinsured.ssolib.config.LogoutControllerCondition;
import com.getinsured.ssolib.config.SSOConfig;

/**
 * Logout Controller when logout controller is not available in application.
 *
 * @author Yevgen Golubenko
 * @since 8/28/17
 */
@Controller
@Conditional(LogoutControllerCondition.class)
public class LogoutController
{
  private static final Logger log = LoggerFactory.getLogger(LogoutController.class);
  private final SSOConfig config;

  @Autowired
  public LogoutController(@Qualifier("ssoConfig") final SSOConfig config)
  {
    this.config = config;

    if(log.isInfoEnabled())
    {
      log.info("Creating LogoutController as specified by the configuration: {}", config.getLogout().isCreateLogoutController());
    }
  }

  @RequestMapping(value = SSOConfig.Login.DEFAULT_LOGIN_LOGOUT_URL)
  public String logout(HttpServletRequest request)
  {
    if(log.isInfoEnabled())
    {
      log.info("Redirecting to {} after logging out.", config.getLogin().getSuccessUrl());
    }

    return "redirect:" + config.getLogin().getSuccessUrl();
  }
//
//  @ExceptionHandler(SAMLException.class)
//  public String logoutSSO(HttpServletRequest request, HttpServletResponse response, Exception ex)
//  {
//    if(log.isErrorEnabled())
//    {
//      log.error("Exception occurrect while processing SAML", ex);
//    }
//
//    return "redirect:" + config.getLogin().getSuccessUrl();
//  }
}
