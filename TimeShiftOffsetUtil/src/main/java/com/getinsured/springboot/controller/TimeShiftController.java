package com.getinsured.springboot.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.getinsured.springboot.service.UserOffsetService;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.springboot.model.UserOffset;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
public class TimeShiftController {

	@Autowired
	private UserOffsetService userOffsetService;
	private static ObjectMapper mapperObj = new ObjectMapper();

	@GetMapping("/timeshift/{username:.+}")
	@ResponseBody
	public ResponseEntity<String> retrieveOffsetForUser(@PathVariable String username) {
		
		System.out.println("Retrieve OffsetForUser username = " + username );
		UserOffset userOffset = userOffsetService.getUserOffset(username);
		if( userOffset != null) {
			try {
				String jsonStr = mapperObj.writer().writeValueAsString(userOffset);
				System.out.println(jsonStr);
				return new ResponseEntity<String>(jsonStr, HttpStatus.OK);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ResponseEntity<String>("", HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/timeshift/add/{username}/{timeshiftinmillis}")
	@ResponseBody
	public ResponseEntity<String> addOffsetForUser(@PathVariable String username , @PathVariable String timeshiftinmillis) {
		System.out.println("addOffsetForUser username = " + username + " timeshiftinmillis = " + timeshiftinmillis);
		UserOffset userOffset = new UserOffset(username, true, Long.valueOf(timeshiftinmillis));
		if(userOffsetService.addUserOffset(userOffset)) {
			return new ResponseEntity<String>("User timeshift info added", HttpStatus.OK);
		}
		return new ResponseEntity<String>("User timeshift info could not be added", HttpStatus.BAD_REQUEST);
	}
}
