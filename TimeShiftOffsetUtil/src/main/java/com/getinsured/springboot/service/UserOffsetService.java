package com.getinsured.springboot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.getinsured.springboot.model.UserOffset;

@Component
public class UserOffsetService {

	private static List<UserOffset> userOffsets = new ArrayList<>();
   
	static {
		//Initialize Data
		
		UserOffset user1 = new UserOffset("user1", true,100, 5961600000L);
		UserOffset user2 = new UserOffset("user2", true,101, -5961600000L);
		UserOffset user3 = new UserOffset("user3", false,102, -1);

		userOffsets.add(user1);
		userOffsets.add(user2);
		userOffsets.add(user3);
	}

	public List<UserOffset> retrieveAllStudents() {
		return userOffsets;
	}

	public UserOffset getUserOffset(String userName) {
		for (UserOffset userOffset : userOffsets) {
			if (userOffset.getUsername().equals(userName)) {
				return userOffset;
			}
		}
		return null;
	}

	public boolean addUserOffset(UserOffset userOffset) {
		if(userOffset != null) {
			return userOffsets.add(userOffset);
		}
		return false;
	}
}