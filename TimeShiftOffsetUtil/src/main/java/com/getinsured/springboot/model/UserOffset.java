package com.getinsured.springboot.model;

import java.util.List;
import java.util.Random; 

public class UserOffset {
	private String username;
	private boolean timeShifterEnabled;
	private int userId;
	private long timeshifterOffsetMillis;
	
	public UserOffset(String username, boolean timeShifterEnabled, int userId,
			long timeshifterOffsetMillis) {
		this.username = username;
		this.timeShifterEnabled = timeShifterEnabled;
		this.userId = userId; 
		this.timeshifterOffsetMillis = timeshifterOffsetMillis;
	}
	
	public UserOffset(String username, boolean timeShifterEnabled, long timeshifterOffsetMillis) {
		this.username = username;
		this.timeShifterEnabled = timeShifterEnabled;
		Random rand = new Random(); 
		this.userId =  rand.nextInt(1000);
		this.timeshifterOffsetMillis = timeshifterOffsetMillis;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isTimeShifterEnabled() {
		return timeShifterEnabled;
	}

	public void setTimeShifterEnabled(boolean timeShifterEnabled) {
		this.timeShifterEnabled = timeShifterEnabled;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public long getTimeshifterOffsetMillis() {
		return timeshifterOffsetMillis;
	}

	public void setTimeshifterOffsetMillis(long timeShifterOffsetMillis) {
		this.timeshifterOffsetMillis = timeshifterOffsetMillis;
	}
	

	@Override
	public String toString() {
		return String.format(
				"UserOffset [username=%s, timeShifterEnabled=%s, userId=%s, timeshifterOffsetMillis=%s]", username,timeShifterEnabled, userId, timeshifterOffsetMillis);
	}
}