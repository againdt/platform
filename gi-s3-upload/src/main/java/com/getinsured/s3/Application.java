package com.getinsured.s3;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.s3.service.S3Service;

@SpringBootApplication
public class Application {

	private static final Logger LOG = LogManager.getLogger(Application.class.getName());

	@Value("${localFolder}")
	private String localFolder;

	@Value("${branch}")
	private String branch;

	@Value("${env}")
	private String env;

	@Value("${tenantCode}")
	private String tenantCode;

	@Value("${aws.accessKey}")
	private String accessKey;

	@Value("${aws.secretKey}")
	private String secretKey;

	@Value("${aws.regionName}")
	private String regionName;

	@Value("${aws.s3.bucketName}")
	private String bucketName;

	@Value("${aws.s3.baseFolder}")
	private String baseFolder;

	@Value("${spring.datasource.url}")
	private String datasource;

	@Value("${spring.datasource.username}")
	private String dbUsername;

	@Autowired
	private S3Service s3Service;

	@Autowired private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) throws IOException {

		SpringApplication app = new SpringApplication(Application.class);
		app.setWebEnvironment(false);
		ConfigurableApplicationContext ctx = app.run(args);
		Application mainObj = ctx.getBean(Application.class);

		if (args == null || args.length == 0){
			LOG.error("Mode not specified.");
			throw new RuntimeException("Mode not specified.");
		}

		String mode = null;
		if (args.length >= 1){
			mode = args[0];
		}

		mainObj.displayConfiguration(mode);

		if ("noticetmpl".equalsIgnoreCase(mode)){
			mainObj.uploadToS3();
		} else {
			LOG.error("Unknown Mode passed!");
			throw new RuntimeException("Unknown Mode passed!");
		}

	}

	private void uploadToS3() {

		int count = jdbcTemplate.update("update notice_types set external_id = null ");
		LOG.info("DB refreshed.. count - " + count);

		Instant start = Instant.now();
		long noOfFileUploaded = s3Service.upload();
		Instant end = Instant.now();
		LOG.info("\nNo of templates uploaded - " + noOfFileUploaded + "\nTime Taken - " + Duration.between(start, end));
	}

	private void displayConfiguration(String mode) {
		LOG.info("=========================================================================");
		LOG.info("---------------------- S3 Notice Upload Job Parameters ---------------------");
		LOG.info("=========================================================================");

		LOG.info("Reading files from local folder - " + localFolder);
		LOG.info("Branch - " + branch);
		LOG.info("Tenant Code - " + tenantCode);
		LOG.info("Env - " + env);
		LOG.info("Access Key - " + accessKey);
		LOG.info("Secret Key - " + "********");
		LOG.info("Region Name - " + regionName);
		LOG.info("S3 Bucket Name - " + bucketName);
		LOG.info("S3 Base Folder Name - " + baseFolder);
		LOG.info("DB connected - " + datasource);
		LOG.info("DB username - " + dbUsername);

		LOG.info("Mode - " + mode);

		LOG.info("=========================================================================");

	}
}
