package com.getinsured.s3.service;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Service
public class S3Service {

	private static final String FORWARD_SLASH = "/";

	@Value("${localFolder}")
	private String localFolder;

	@Value("${branch}")
	private String branch;

	@Value("${env}")
	private String env;

	@Value("${tenantCode}")
	private String tenantCode;

	@Value("${aws.accessKey}")
	private String accessKey;

	@Value("${aws.secretKey}")
	private String secretKey;

	@Value("${aws.regionName}")
	private String regionName;

	@Value("${aws.s3.bucketName}")
	private String bucketName;

	@Value("${aws.s3.baseFolder}")
	private String baseFolder;


	public long upload() {

		Regions region = Regions.fromName(regionName);

		AmazonS3 s3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
				.withRegion(region)
				.build();

		File file = new File(localFolder);
		File[] files = file.listFiles();

		Stream.of(files).filter(f -> f.isFile()).parallel().forEach((f) -> {
			upload(f, f.getName(), s3);});

		return Stream.of(files).filter(f -> f.isFile()).count();
	}

	private static final String TEMPLATE = "TMPL";
	private static final String PLATFORM = "PT";

	private void upload(File file, String name, AmazonS3 s3) {
		String preKey = baseFolder + FORWARD_SLASH + tenantCode + FORWARD_SLASH + "notificationTemplates" + FORWARD_SLASH;

		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType("text/html");
		metadata.setContentLength(file.length());
		metadata.setLastModified(new Date());
		metadata.setContentEncoding(StandardCharsets.UTF_8.name());

		metadata.addUserMetadata("branch", branch);
		metadata.addUserMetadata("category", PLATFORM);
		metadata.addUserMetadata("createdBy", "uploadjob");
		metadata.addUserMetadata("creationDate", String.valueOf(new Date().getTime()));
		metadata.addUserMetadata("modifiedBy", "uploadjob");
		metadata.addUserMetadata("modifiedDate", String.valueOf(new Date().getTime()));
		metadata.addUserMetadata("description", name);
		metadata.addUserMetadata("docType", "text/html");
		metadata.addUserMetadata("env", env);
		metadata.addUserMetadata("fileName", name);
		metadata.addUserMetadata("mimeType", "text/html");
		metadata.addUserMetadata("relativePath", preKey);
		metadata.addUserMetadata("subCategory", TEMPLATE);
		metadata.addUserMetadata("tenantCode", tenantCode);

		PutObjectRequest request = new PutObjectRequest(bucketName, preKey + name, file).withMetadata(metadata);

		s3.putObject(request);

	}

}
