package gi.mail.service.notices;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.getinsured.hix.model.QueryData;


public interface QueryDataService {

	public String generateDynamicQuery(String tagQueryName, String subQueryName, String jsonTagValues);

	String mergeQueries(String tagQueryName, String subQuery);

	QueryData findByName(String queryName);

	String generateDynamicQueryWithData(String tagQueryName, String subQueryName, Map<String, String> eventDataList) throws JsonParseException, JsonMappingException, IOException;
}
