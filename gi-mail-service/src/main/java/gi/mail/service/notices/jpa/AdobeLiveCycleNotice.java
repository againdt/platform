package gi.mail.service.notices.jpa;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformRequest.AdobeLiveCycleRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

/**
 * Adobe LiveCycle Notice implementation to generate PDF and upload to ECM (Oracle - provided by Accenture).
 * Accenture SOAP Web Service is invoked which generates PDF and uploads to ECM (Oracle - provided by Accenture).
 *
 * @author EkramAli Kazi
 *
 */
@Component
class AdobeLiveCycleNotice extends AbstractNotice {

	private static final Logger LOGGER = Logger.getLogger(AdobeLiveCycleNotice.class);


	@Autowired private RestTemplate restTemplate;

	@Override
	public String createPDFDocument(NoticeType noticeType, Map<String, Object> tokens, String ecmFilePath, String ecmFileName) throws NoticeServiceException {

		// validate ExternalId is present in noticeType
		if (StringUtils.isEmpty(noticeType.getExternalId())){
			//LOGGER.error("NoticeType.ExternalId cannot be blank. Please check configuration for NoticeType - " + noticeType.getNotificationName());
			throw new NoticeServiceException("NoticeType.ExternalId cannot be blank. Please check configuration for NoticeType - " + noticeType.getNotificationName());
		}

		// extract extension
		String extension = FilenameUtils.getExtension(ecmFileName);
		if (StringUtils.isEmpty(extension) || !"PDF".equalsIgnoreCase(extension)){
			LOGGER.error("Invalid ecmFileName. Should have pdf extension - " + ecmFileName);
			throw new NoticeServiceException("Invalid ecmFileName. Should have pdf extension - " + ecmFileName);
		}

		//form Map<String, String> tokens from  Map<String, Object> tokens for AHBX version 1
		HashMap<String, String> tokenParameter = new HashMap<String, String>();

		for (@SuppressWarnings("rawtypes") Map.Entry entry : tokens.entrySet()) {
			tokenParameter.put(entry.getKey().toString(), entry.getValue().toString());
		}

		/**
		 * form platformRequest for adobeLiveCycleRequest
		 */
		PlatformRequest platformRequest = new PlatformRequest();
		AdobeLiveCycleRequest adobeLiveCycleRequest = platformRequest.new AdobeLiveCycleRequest();
		adobeLiveCycleRequest.setFormat(extension.toUpperCase());
		adobeLiveCycleRequest.setTemplateName(noticeType.getExternalId());
		adobeLiveCycleRequest.setTokens(tokenParameter);
		platformRequest.setAdobeLiveCycleRequest(adobeLiveCycleRequest);

		/**
		 * Rest WS call to GHIX-AHBX module
		 */
		String ahbxResponse = restTemplate.postForObject(AHBXEndPoints.PLATFORM_ADOBELIVECYCLE, platformRequest, String.class);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamGenericObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX Notices Service call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			LOGGER.warn("AHBX Notices Service call failed - " +  platformResponse.getErrCode() + platformResponse.getErrMsg());
			throw new NoticeServiceException("AHBX Notices Service call failed - " + platformResponse.getErrMsg());
		}

		return platformResponse.getAhbxEcmId();
	}

}
