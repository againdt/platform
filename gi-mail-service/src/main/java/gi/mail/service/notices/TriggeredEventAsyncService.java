package gi.mail.service.notices;

import com.getinsured.hix.model.TriggeredEvent;

public interface TriggeredEventAsyncService {
	
	public TriggeredEvent save(TriggeredEvent triggeredEvent);
	
}
