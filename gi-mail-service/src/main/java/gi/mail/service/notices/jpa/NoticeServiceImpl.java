package gi.mail.service.notices.jpa;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.util.exception.GIException;

import gi.mail.service.notices.NoticeService;
import gi.mail.service.notification.NoticeTmplHelper;

/**
 * Implementation for NoticeService interface to generate PDFs based on either FLYING SAUCER or ADOBE LiveCycle Strategy.
 * 
 * @author EkramAli Kazi
 *
 */
@Service
public class NoticeServiceImpl implements NoticeService {

	@Value("#{configProp['notice_engine_type']}")
	private String noticeEngineType;

	@Autowired private DefaultNotice defaultNotice;
	@Autowired private AdobeLiveCycleNotice adobeLiveCycleNotice;
	@Autowired private NoticeTypeRepository noticeTypeRepository;

	private INoticeStrategy iNoticeStrategy;

	@Autowired private ContentManagementService ecmService;

	private static final Logger LOGGER = LoggerFactory.getLogger(NoticeServiceImpl.class);

	@PostConstruct
	public void createDocumentContextInitilizer() {
		if ("DEFAULT".equals(noticeEngineType)) {
			iNoticeStrategy = defaultNotice;
		}else if ("ADOBE_LIVECYCLE".equals(noticeEngineType)) {
			iNoticeStrategy = adobeLiveCycleNotice;
		}else{
			LOGGER.error("Unknow notice_engine_type set in configuration.properties.");
			throw new IllegalArgumentException("Unknow notice_engine_type set in configuration.properties.");
		}
	}



	@Override
	public Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, AccountUser user) throws NoticeServiceException{

		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);

		if (user == null || user.getId() == 0){
			throw new IllegalArgumentException("Account User cannot be blank");
		}

		return iNoticeStrategy.createNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, user, null, null);

	}
	
	@Override
	public Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, 
			String ecmRelativePath, String ecmFileName, AccountUser user,Location location, GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException{

		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);
		
		if (user == null || user.getId() == 0){
			throw new IllegalArgumentException("Account User cannot be blank");
		}
		
		return iNoticeStrategy.createNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, user, location, communicationPref);
	}
	
	@Override
	public Notice createNoticeWOInbox(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, AccountUser user) throws NoticeServiceException{

		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);

		if (user == null || user.getId() == 0){
			throw new IllegalArgumentException("Account User cannot be blank");
		}

		return iNoticeStrategy.createNoticeWOInbox(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, user);

	}

	@Override
	public byte[] getNotificationPDFContent(Notice notice) throws ContentManagementServiceException{

		if (notice == null || StringUtils.isEmpty(notice.getEcmId())){
			throw new IllegalArgumentException("Notice ECM ID cannot be blank");
		}

		return ecmService.getContentDataById(notice.getEcmId());

	}

	@Override
	public Notice createModuleNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, String moduleName, long moduleId, List<String> sendToEmailList, String fromFullName, String toFullName) throws NoticeServiceException {
		
		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);
		if(!(moduleId>0 && moduleName !=null && moduleName.trim().length()>0)){
			throw new IllegalArgumentException("Module User information cannot be blank");				
		}
		return iNoticeStrategy.createModuleNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, moduleName, moduleId, sendToEmailList, fromFullName, toFullName, null, null);
	}
	
	@Override
	public Notice createModuleNotice(String noticeTemplateName,GhixLanguage language, Map<String, Object> noticeDataMap,
			String ecmRelativePath, String ecmFileName, String moduleName,long moduleId, List<String> sendToEmailList, String fromFullName,
			String toFullName, Location location,GhixNoticeCommunicationMethod communicationPref)throws NoticeServiceException {
		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);
		
		return iNoticeStrategy.createModuleNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, moduleName, moduleId, sendToEmailList, fromFullName, toFullName, location, communicationPref);
	}
	
	private boolean validateRequest(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName){
		
		if (StringUtils.isEmpty(noticeTemplateName)){
			throw new IllegalArgumentException("Notice template name cannot be blank");
		}

		if (language == null || StringUtils.isEmpty(language.getLanguage())){
			throw new IllegalArgumentException("Notice template language cannot be blank");
		}

		if (StringUtils.isEmpty(ecmRelativePath)){
			throw new IllegalArgumentException("ECM path cannot be blank");
		}

		if (StringUtils.isEmpty(ecmFileName)){
			throw new IllegalArgumentException("ECM file name cannot be blank");
		}
		
		return true;
	}



	@Autowired private NoticeTmplHelper noticeTmplHelper;
	
	@Override
	public String getEcmContentId(NoticeType noticeType) throws ContentManagementServiceException {
		String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
				+ noticeType.getTemplateLocation();
		
		Content content = noticeTmplHelper.readContentMetaByPath(ecmTemplateFolderPath);;
		String ecmContentId = content.getContentId();
		noticeType.setExternalId(ecmContentId);
		noticeType.setUpdated(new Date());
		
		// save the content id to DB
		NoticeType updatedNotice = noticeTypeRepository.save(noticeType);
		return updatedNotice.getExternalId();
		
	}

	
	@Override
	public byte[] createPreviewContent(String  templateContent, Map<String, Object> tokens) throws NoticeServiceException{
		return defaultNotice.getPreviewContent(templateContent, tokens);
	}


	@Override
	public void postToInbox(AccountUser userObj, List<String> sendToEmailList, String moduleName, long moduleId,
			Notice notice, String docName, String fromFullName, String toFullName)
					throws ContentManagementServiceException, GIException {
		defaultNotice.postToInbox(userObj, sendToEmailList, moduleName, moduleId, notice, docName, fromFullName, toFullName);
	}
}
