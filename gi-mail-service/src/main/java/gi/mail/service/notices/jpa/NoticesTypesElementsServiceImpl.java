package gi.mail.service.notices.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.NoticesTypesElements;
import com.getinsured.hix.platform.repository.INoticesTypesElementsRepository;

import gi.mail.service.notices.NoticesTypesElementsService;

@Service("NoticesTypesElementsService")
public class NoticesTypesElementsServiceImpl implements NoticesTypesElementsService {

	@Autowired INoticesTypesElementsRepository iNoticesTypesElementsRepository;
	@Override
	public List<NoticesTypesElements> getAllElementsByNoticeType(
			NoticeType noticeType) {
		return iNoticesTypesElementsRepository.findByNoticeType(noticeType);
	}
	

}
