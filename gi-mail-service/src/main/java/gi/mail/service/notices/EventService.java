package gi.mail.service.notices;

import com.getinsured.hix.model.Event;

public interface EventService {
	public Event findByEventName(String eventName);
}
