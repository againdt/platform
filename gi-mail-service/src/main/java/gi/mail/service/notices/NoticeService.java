package gi.mail.service.notices;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.util.exception.GIException;

import gi.mail.service.notices.jpa.NoticeServiceException;

/**
 * Interface to define operations for Notification Engine to generate PDFs.
 * 
 * @author EkramAli Kazi
 *
 */
public interface NoticeService {

	/**
	 * @deprecated
	 * Generates PDF document by invoking
	 * 		1. FLYING SAUCER open source APIs
	 * OR 	2. ADOBE LiveCycle web service
	 * 
	 * Persist generated doc information in database.
	 * 
	 * @param noticeTemplateName; notice template name stored in notices_type table
	 * @param language; notice template for a particular language stored in notices_type table
	 * @param noticeDataMap; the data which client needs to map on the html template
	 * @param ecmRelativePath; ecm (Alfresco) path where client wishes to store the generated PDF
	 * @param ecmFileName; generated PDF file name
	 * @param user; to update user information in notices table
	 * 
	 * @return notice object to the client
	 * @throws NoticeServiceException; if noticeTemplateName NOT FOUND or some processing exception with FLYING SAUCER or ADOBE LiveCycle.
	 */
	@Deprecated
	Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, AccountUser user) throws NoticeServiceException;
	
	/**
	 * Generates PDF document by invoking
	 * 		1. FLYING SAUCER open source APIs
	 * OR 	2. ADOBE LiveCycle web service
	 * 
	 * Persist generated doc information in database. Fields which are not mentioned about null value are mandatory fields.
	 * This method always a valid user object.
	 * 
	 * @param noticeTemplateName; notice template name stored in notices_type table
	 * @param language; notice template for a particular language stored in notices_type table
	 * @param noticeDataMap; the data which client needs to map on the html template
	 * @param ecmRelativePath; ecm (Alfresco) path where client wishes to store the generated PDF
	 * @param ecmFileName; generated PDF file name
	 * @param user; to update user information in notices table. 
	 * @param location; to support address template, address will populate on PDF. If null will not send paper notice.
	 * @param communicationPref ; to decide for print paper preference.if null will check with notice type or user preference   
	 * 
	 * @return notice object to the client
	 * @throws NoticeServiceException; if noticeTemplateName NOT FOUND or some processing exception with FLYING SAUCER or ADOBE LiveCycle.
	 * 
	 */
	Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath,
			String ecmFileName, AccountUser user, Location location, GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException;
	/**
	 * Get Notification PDF content from Alfresco.
	 * 
	 * @param notice; not null and notice.ecmId cannot be null
	 * @return content in byte[]
	 * 
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	byte[] getNotificationPDFContent(Notice notice) throws ContentManagementServiceException;

	/**
	 * @deprecated
	 * Generates PDF document by invoking
	 * 		1. FLYING SAUCER open source APIs
	 * OR 	2. ADOBE LiveCycle web service
	 * 
	 * Persist generated doc information in database.
	 * 
	 * @param noticeTemplateName; notice template name stored in notices_type table
	 * @param language; notice template for a particular language stored in notices_type table
	 * @param noticeDataMap; the data which client needs to map on the html template
	 * @param ecmRelativePath; ecm (Alfresco) path where client wishes to store the generated PDF
	 * @param ecmFileName; generated PDF file name
	 * @param moduleName; to update moduleName information in inbox_msg table
	 * @param moduleId; to update moduleId information in inbox_msg table
	 * @param sendToEmailList; to send an email about notification
	 * 
	 * @return notice object to the client
	 * @throws NoticeServiceException; if noticeTemplateName NOT FOUND or some processing exception with FLYING SAUCER or ADOBE LiveCycle.
	 */
	@Deprecated
	Notice createModuleNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath,
			String ecmFileName, String moduleName, long moduleId, List<String> sendToEmailList, String fromFullName, String toFullName) throws NoticeServiceException;
	
	/**
	 * Generates PDF document by invoking
	 * 		1. FLYING SAUCER open source APIs
	 * OR 	2. ADOBE LiveCycle web service
	 * 
	 * Persist generated doc information in database.Fields which are not mentioned about null value are mandatory fields.
	 * This method does not depend on User or module detail. moduleName, moduleId, sendToEmailList, location, communicationPref
	 * can be null. 
	 * 
	 * @param noticeTemplateName; notice template name stored in notices_type table
	 * @param language; notice template for a particular language stored in notices_type table
	 * @param noticeDataMap; the data which client needs to map on the html template
	 * @param ecmRelativePath; ecm (Alfresco) path where client wishes to store the generated PDF
	 * @param ecmFileName; generated PDF file name
	 * @param moduleName; to update moduleName information in inbox_msg table. If null will not map the inboxMsg to user or module
	 * @param moduleId; to update moduleId information in inbox_msg table. If null will not map the inboxMsg to user or module
	 * @param sendToEmailList; to send an email about notification. If null will not send any email
	 * @param fromFullName
	 * @param toFullName
	 * @param location; to support address template, address will populate on PDF. If null will not send paper notice.
	 * @param communicationPref ; to decide for print paper preference.if null will check with notice type or user preference  
	 * 
	 * @return notice object to the client
	 * @throws NoticeServiceException; if noticeTemplateName NOT FOUND or some processing exception with FLYING SAUCER or ADOBE LiveCycle.
	 * 
	 * 
	 */
	Notice createModuleNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath,
			String ecmFileName, String moduleName, long moduleId, List<String> sendToEmailList, String fromFullName, String toFullName,
			Location location, GhixNoticeCommunicationMethod communicationPref)throws NoticeServiceException;
	/**
	 * Generated PDF document without sending email using secure inbox.
	 * @param noticeTemplateName
	 * @param language
	 * @param noticeDataMap
	 * @param ecmRelativePath
	 * @param ecmFileName
	 * @param user
	 * @return
	 * @throws NoticeServiceException
	 * JIRA ID: HIX-15363
	 */
	Notice createNoticeWOInbox(String noticeTemplateName,
			GhixLanguage language, Map<String, Object> noticeDataMap,
			String ecmRelativePath, String ecmFileName, AccountUser user)
			throws NoticeServiceException;
	
	String getEcmContentId(NoticeType noticeType) throws ContentManagementServiceException;

	

	byte[] createPreviewContent(String updatedTemplate,
			Map<String, Object> tokens) throws NoticeServiceException;
	
	/**
	 * Method to Create secure Inbox Message 
	 * @param userObj Object<AccountUser>
	 * @param sendToEmailList List<String>
	 * @param moduleName String
	 * @param moduleId long
	 * @param notice Notice
	 * @param docName String
	 * @param fromFullName String
	 * @param toFullName String
	 * @throws ContentManagementServiceException
	 * @throws GIException
	 */
	void postToInbox(final AccountUser userObj, final List<String> sendToEmailList, final String moduleName,
			final long moduleId, final Notice notice, final String docName, final String fromFullName,
			final String toFullName) throws ContentManagementServiceException, GIException;
	
}
