package gi.mail.service.notices.jpa;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import gi.mail.service.notices.TemplateTokens;
import gi.mail.service.notification.NoticeTmplHelper;
/**
 * Default Notice implementation based on Flying Saucer to generate PDF and upload to ECM (Alfresco).
 *
 * @author EkramAli Kazi
 *
 */
@Component
class DefaultNotice extends AbstractNotice {


	private static final String TENDIGITCONSTANT = "0123456789";
	@Autowired private ContentManagementService ecmService;
	@Autowired private NoticeTmplHelper noticeTmplHelper;
	@Autowired private  ApplicationContext appContext;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultNotice.class);
	
	public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	
	public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";

	public byte[] getPreviewContent(String templateContent, Map<String, Object> tokens) throws NoticeServiceException{

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		byte[] pdfBytes=null;

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			getAllAdditionalTokens(tokens);
			
			tmpl.process(tokens, sw);
			
			pdfBytes = generatePdf(sw.toString());
			
		} catch (Exception e) {
			LOGGER.warn("Exception found in getPreviewContent() template review- ", e);
			throw new NoticeServiceException();
		}
		return pdfBytes;
	}
	
	
	/**
	 * This method only called for preview not for actual population of data.
	 * @param tokens
	 * @return
	 * @throws NoticeServiceException 
	 */
	private Map<String, Object> getAllAdditionalTokens(Map<String, Object> tokens) throws NoticeServiceException {
		
		Map<String,String> templateTokens = new HashMap<String, String>();			
		templateTokens.put(TemplateTokens.HOST,GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
		LOGGER.debug("Exchange full Name " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		
		templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		templateTokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		
		//the hard coded value added. This method only called for preview not for actual population of data.
		templateTokens.put(TemplateTokens.NOTICE_UNIQUE_ID, TENDIGITCONSTANT);
		
		tokens.put(TemplateTokens.HEADER_CONTENT, this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_HEADER_LOCATION));
		tokens.put(TemplateTokens.FOOTER_CONTENT, this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_FOOTER_LOCATION));
		return tokens;
	}
	
	private Object getTemplateContentWithTokensReplaced(Map<String, String> tokens, String location) throws NoticeServiceException {
		
		String templateContent = readTemplateContent(location);
		
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
		}
		return sw.toString();

	}
	


	@Override
	public String createPDFDocument(NoticeType noticeType, Map<String, Object> tokens, String ecmFilePath, String ecmFileName) throws NoticeServiceException {

		if (StringUtils.isEmpty(noticeType.getTemplateLocation())){
			throw new NoticeServiceException("Template Location not found for notice name - " + noticeType.getNotificationName() + " and language - " + noticeType.getLanguage());
		}
		
		String templateContent = readTemplateContent(noticeType.getTemplateLocation());

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.warn("Exception found in notice template - ", e);
		}

		byte[] pdfBytes;
		try {
			pdfBytes = generatePdf(sw.toString());
		} catch (Exception e) {
			LOGGER.error("Error invoking flying saucer api - ", e);
			throw new NoticeServiceException("Error invoking flying saucer api - " + e.getMessage(), e);
		}

		try {
			return ecmService.createContent(ecmFilePath, ecmFileName, pdfBytes, ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.NOTICE, null);
		} catch (ContentManagementServiceException e) {
			throw new NoticeServiceException(e);
		}

	}
	
	private String readTemplateContent(String path) throws NoticeServiceException {
		if (StringUtils.isEmpty(path)){
			throw new NoticeServiceException("Template Location not found for notice name - " + path);
		}
		
		InputStream inputStreamUrl = null;
		try {
			if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE).equalsIgnoreCase("N")){
				inputStreamUrl = getTemplateFromResources(path);
			}
			else{
				String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
						+ path;
				
				inputStreamUrl = getTemplateFromECM(ecmTemplateFolderPath);
			}
			return IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (ContentManagementServiceException cmse) {
			LOGGER.error("Error reading template"  , cmse);
			throw new NoticeServiceException("Error reading template from  - " + path, cmse);
		} catch (IOException ioe) {
			LOGGER.error("Error reading template ", ioe);
			throw new NoticeServiceException("Error reading template from  - " + path , ioe);
		}
		finally
		{
			IOUtils.closeQuietly(inputStreamUrl);
		}
		
	}
	
	private byte[] generatePdf(String incomingData) throws IOException, NoticeServiceException {

		/** Replace resources path (images, css, etc) with the full actual host name */
		String finalData = new String(StringUtils.replace(formData(incomingData), "{host}", GhixPlatformEndPoints.GHIXWEB_SERVICE_URL));


		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try{
			/** If resources (images, css, etc) are not found,
			 * then ITextRenderer logs "<strong>java.io.IOException: Stream closed</strong>" exception without throwing it
			 * So we can't catch such exception thrown by 3rd party jar
			 *
			 * Also, stack trace gets printed in the PDF on Alfresco
			 *
			 */
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(finalData);
			renderer.layout();
			renderer.createPDF(os);
		}catch(Exception e){
			throw new NoticeServiceException(e);
		}
		finally{
			os.close();
		}

		return os.toByteArray();
	}

	private String formData(String data) throws IOException {

		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType("omit");
		tidy.setXHTML(true);
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.parse(new StringReader(data), writer);
		writer.close();
		return writer.toString();
	}
	
	private InputStream getTemplateFromECM(String path) throws ContentManagementServiceException {
		return new ByteArrayInputStream(
				noticeTmplHelper.readBytesByPath(path));
	}
	
	private InputStream getTemplateFromResources(String path) throws IOException {
		Resource resource = 
		           appContext.getResource("classpath:"+ path);
		return resource.getInputStream();
	}
}
