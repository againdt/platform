package gi.mail.service.notices.jpa;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;

/**
 * Interface to define strategies for Notification PDF generation.
 * 
 * @author EkramAli Kazi
 *
 */
interface INoticeStrategy {


	/**
	 * Generates PDF document by invoking either
	 * 		1. FLYING SAUCER open source APIs
	 * OR 	2. ADOBE LiveCycle web service (Accenture Web Service for California State)
	 * 
	 * Persist generated doc information in db.
	 * @param location 
	 * @param communicationPref 
	 * 
	 * @param noticeTemplateName; notice template name stored in notices_type table
	 * @param language; notice template for a particular language stored in notices_type table
	 * @param noticeDataMap; the data which client needs to map on the html template
	 * @param ecmRelativePath; ecm (Alfresco) path where client wishes to store the generated PDF
	 * @param ecmFileName; generated PDF file name
	 * @param user; AccountUser entity for which PDF is being generated
	 * 
	 * @return notice object to be persisted by client
	 * @throws NoticeServiceException; if noticeTemplateName NOT FOUND or some processing exception with FLYING SAUCER or ADOBE LiveCycle or uploading document on Alfresco.
	 */
	Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> tokens,
			String ecmFilePath, String ecmFileName, AccountUser user, Location location, GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException;

	/**
	 * Generates PDF document by invoking either
	 * 		1. FLYING SAUCER open source APIs
	 * OR 	2. ADOBE LiveCycle web service (Accenture Web Service for California State)
	 * 
	 * Persist generated doc information in db.
	 * @param location 
	 * @param communicationPref 
	 * 
	 * @param noticeTemplateName; notice template name stored in notices_type table
	 * @param language; notice template for a particular language stored in notices_type table
	 * @param noticeDataMap; the data which client needs to map on the html template
	 * @param ecmRelativePath; ecm (Alfresco) path where client wishes to store the generated PDF
	 * @param ecmFileName; generated PDF file name
	 * @param user; AccountUser entity for which PDF is being generated
	 * 
	 * @return notice object to be persisted by client
	 * @throws NoticeServiceException; if noticeTemplateName NOT FOUND or some processing exception with FLYING SAUCER or ADOBE LiveCycle or uploading document on Alfresco.
	 */	
	Notice createModuleNotice(String noticeTemplateName, GhixLanguage language,
			Map<String, Object> tokens, String ecmFilePath,
			String ecmFileName, String moduleName, long moduleId,
			List<String> sendToEmailList, String fromFullName, String toFullName, Location location, GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException;

	Notice createNoticeWOInbox(String noticeTemplateName,
			GhixLanguage language, Map<String, Object> tokens,
			String ecmFilePath, String ecmFileName, AccountUser user)
			throws NoticeServiceException;

}
