package gi.mail.service.notices.jpa;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.TriggeredEvent;
import com.getinsured.hix.platform.repository.ITriggeredEventRepository;
import com.getinsured.hix.platform.util.GhixUtils.EMAIL_STATS;
import com.google.gson.Gson;

import gi.mail.service.notices.TriggeredEventAsyncService;
import gi.mail.service.notify.EmailNotificationService;

@Service("TriggeredEventAsyncService")
public class TriggeredAsyncEventServiceImpl implements TriggeredEventAsyncService {

	@Autowired EmailNotificationService emailNotificationService;
	@Autowired ITriggeredEventRepository triggeredEventRepository;
	
	@Override
	@Async
	public TriggeredEvent save(TriggeredEvent triggeredEvent) {
		
		Map<String, String> employeeData = new HashMap<String, String>();
		//employeeData = gson.fromJson(triggeredEvent.getJsonMergeTags(), employeeData.getClass());
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		Notice noticeObj = new Notice();
        try {
            //convert JSON string to Map
        	employeeData = gson.fromJson(triggeredEvent.getJsonMergeTags(), HashMap.class);
            System.out.println(employeeData);
            
            Map<String, String> emaildata = new HashMap<String, String>();
    		emaildata.put("To", employeeData.get("To"));
    		
    		noticeObj = emailNotificationService.generateEmail(emaildata, employeeData, triggeredEvent.getEvent().getNoticeType().getEmailClass(), null);
    		EnumMap<EMAIL_STATS, String> notificationTrackingAttributes = new EnumMap<EMAIL_STATS, String>(EMAIL_STATS.class);
			notificationTrackingAttributes.put(EMAIL_STATS.FLOW_ID, String.valueOf(employeeData.get("flowId")));
			notificationTrackingAttributes.put(EMAIL_STATS.AFFILIATE_ID, String.valueOf(employeeData.get("affiliateId")));
			notificationTrackingAttributes.put(EMAIL_STATS.TENANT_ID, String.valueOf(employeeData.get("tenantId")));
			  
			emailNotificationService.sendEmail(noticeObj,notificationTrackingAttributes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        triggeredEvent.setNotice(noticeObj);
		return triggeredEventRepository.saveAndFlush(triggeredEvent);
	}

}
