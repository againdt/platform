package gi.mail.service.notices;


public interface TriggeredEventService {
	
	String createTriggeredEvent(String eventName, String jsonEventData);
}
