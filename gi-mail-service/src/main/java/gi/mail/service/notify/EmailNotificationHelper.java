package gi.mail.service.notify;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import gi.mail.service.notices.TemplateTokens;
import gi.mail.service.notify.NotificationDto;
import gi.mail.service.notify.NotificationTypeNotFound;

@Component("emailNotificationHelperNew")
public class EmailNotificationHelper {

	private static final String EXCEPTION = "Exception ";
	private static final String NOTICE_CAN_NOT_CREATED_WITHOUT_UNIQUE_ID_NOTICE_ID_NOTICE_SEQ_ID_IS_NON_INTEGER = "Notice can not created without Unique ID.notice id (noticeSeqId is non-Integer)- ";
	private static final String WELCOME_EMAIL = "welcomeEmail";
	private static final String USER_ID = "UserId";
	private static final String KEY_NAME = "KeyName";
	private static final String KEY_ID = "KeyId";
	private static final String ATTACHMENT = "Attachment";
	private static final String TO = "To";
	private static final String FROM = "From";
	private static final String SUBJECT = "Subject";
	private static final String BCC = "Bcc";
	private static final String CC = "Cc";

	private static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	private static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
	private static final String ADDRESS_TEMPLATE_HEADER = "notificationTemplate/addressTemplateHeader.html";
	private static final String ADDRESS_TEMPLATE_HEADER_SPANISH="notificationTemplate/addressTemplateHeaderSpanish.html";
	public static final String EMAIL_FOOTERESP_LOCATION = "notificationTemplate/emailTemplateFooterEsp.html";

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailNotificationHelper.class);
	private static final String EMPTY = "";

	@Autowired private GhixDBSequenceUtil ghixDBSequenceUtil;
	@Autowired private NoticeRepository noticeRepo;
	@Autowired private ApplicationContext appContext;
	@Autowired private UserRepository userRepository;
	@Autowired private NoticeTemplateFactory templateFactory;

	private Map<String, String> populateStaticTokens() {
		Map<String,String> templateTokens = new HashMap<String, String>();
		templateTokens.put(TemplateTokens.HOST,GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
		templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		templateTokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

		String noticeSeqId = generateNoticeSeqId();
		templateTokens.put(TemplateTokens.NOTICE_UNIQUE_ID, StringUtils.leftPad(noticeSeqId, GhixPlatformConstants.TEN, GhixPlatformConstants.ZERO));

		return templateTokens;
	}


	private String generateNoticeSeqId() {
		return ghixDBSequenceUtil.getNextSequenceFromDB(Notice.NOTICESEQUENCE.notices_seq.toString());
	}


	public Map<String, String> populateFinalTokens(NoticeType noticeType, Map<String, String> messageTokens, NotificationDto notificationDto) {

		Map<String, String> templateTokens = populateStaticTokens();
		
		if (messageTokens != null && !messageTokens.isEmpty()){
			templateTokens.putAll(messageTokens); // populate messageTokens for PHIX custom address
		}
		templateTokens.put(TemplateTokens.PRIVACY_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));
		templateTokens.put(TemplateTokens.PRIVACY_URL_ESP, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL_ESP));
		templateTokens.put(TemplateTokens.FOOTER_YEAR, Integer.toString(Calendar.getInstance().get(Calendar.YEAR) ));
		
		Map<String,String> tokens = new HashMap<String, String>();
		tokens.put(TemplateTokens.HEADER_CONTENT, getTemplateContentWithTokensReplaced(noticeType, templateTokens, EMAIL_HEADER_LOCATION));
		tokens.put(TemplateTokens.FOOTER_CONTENT, getTemplateContentWithTokensReplaced(noticeType, templateTokens, EMAIL_FOOTER_LOCATION));
		tokens.put(TemplateTokens.SPAINISH_FOOTER_CONTENT, getTemplateContentWithTokensReplaced(noticeType, templateTokens, EMAIL_FOOTER_LOCATION));

		if (null != notificationDto){
			Location locationObj = notificationDto.getLocation();
			String userFullName = notificationDto.getUserFullName();

			Map<String, String> addressTokens = populateAddressTokens(userFullName, locationObj);
			tokens.put(TemplateTokens.ADDRESS_CONTENT, getTemplateContentWithTokensReplaced(noticeType, addressTokens, ADDRESS_TEMPLATE_HEADER));
			tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, getTemplateContentWithTokensReplaced(noticeType, addressTokens, ADDRESS_TEMPLATE_HEADER_SPANISH));
		}

		tokens.putAll(templateTokens);
		return tokens;
	}


	private Map<String, String> populateAddressTokens(String userFullName, Location locationObj) {
		Map<String, String> replaceableObj = new HashMap<String, String>();
		replaceableObj.put(TemplateTokens.USER_FULL_NAME, (null==userFullName)?EMPTY:userFullName);
		replaceableObj.put(TemplateTokens.ADDRESS_LINE_1, (null== locationObj.getAddress1())? EMPTY : locationObj.getAddress1());
		replaceableObj.put(TemplateTokens.ADDRESS_LINE_2, (null== locationObj.getAddress2())? EMPTY : locationObj.getAddress2());
		replaceableObj.put(TemplateTokens.CITY_NAME, (null== locationObj.getCity())?EMPTY:locationObj.getCity());
		replaceableObj.put(TemplateTokens.STATE_CODE, (null== locationObj.getState())?EMPTY:locationObj.getState());
		replaceableObj.put(TemplateTokens.PIN_CODE, (null == locationObj.getZip())?EMPTY:locationObj.getZip());
		return replaceableObj;
	}


	private String getTemplateContentWithTokensReplaced(NoticeType noticeType, Map<String, String> tokens, String location ){
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try {
			tmpl = templateFactory.getTemplate(location);
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.error(EXCEPTION, e);
		}
		return sw.toString();
	}
	
	/**
	 * May return NULL if notice doesn't exist
	 * @param name
	 * @return
	 */
	public NoticeType getNoticeType(String name){
		return this.templateFactory.getNoticeType(name);
	}

	
	public Notice createEmail(NoticeType noticeType, Map<String, String> emailData, Map<String, String> tokens, String clsName) throws NotificationTypeNotFound{

		// 1. prepare notice object
		Notice noticeObj = new Notice();
		String noticeSeqId = tokens.get(TemplateTokens.NOTICE_UNIQUE_ID);

		if (!StringUtils.isNumeric(noticeSeqId)){
			String exceptionReason = NOTICE_CAN_NOT_CREATED_WITHOUT_UNIQUE_ID_NOTICE_ID_NOTICE_SEQ_ID_IS_NON_INTEGER + noticeSeqId;
			LOGGER.error(exceptionReason);
			throw new GIRuntimeException(exceptionReason);
		}

		noticeObj.setId(Integer.parseInt(noticeSeqId));
		// 2. populate notice object
		String cc = StringUtils.isEmpty(emailData.get(CC))  ? StringUtils.EMPTY : emailData.get(CC);
		noticeObj.setCcAddress(cc);
		String bcc = StringUtils.isEmpty(emailData.get(BCC))  ? StringUtils.EMPTY : emailData.get(BCC);
		noticeObj.setBccAddress(bcc);
		String subject = StringUtils.isEmpty(emailData.get(SUBJECT))  ? noticeType.getEmailSubject() : emailData.get(SUBJECT);
		noticeObj.setSubject(subject);
		String from = StringUtils.isEmpty(emailData.get(FROM))  ? noticeType.getEmailFrom() : emailData.get(FROM);
		noticeObj.setFromAddress(from);
		String to = StringUtils.isEmpty(emailData.get(TO))  ? noticeType.getEmailTo() : emailData.get(TO);
		noticeObj.setToAddress(to);
		String attachment = StringUtils.isEmpty(emailData.get(ATTACHMENT))  ? null : emailData.get(ATTACHMENT);
		noticeObj.setAttachment(attachment);
		Integer keyId =  emailData.get(KEY_ID) != null ? Integer.parseInt(emailData.get(KEY_ID)) : 0;
		noticeObj.setKeyId(keyId);
		noticeObj.setKeyName( ( emailData.get(KEY_NAME) != null) ? emailData.get(KEY_NAME) : null);

		boolean checkUserId=true;
		if(StringUtils.isEmpty(emailData.get(USER_ID))){
			checkUserId = false;
		}
		if(checkUserId){
			AccountUser userObj = userRepository.findById(Integer.parseInt(emailData.get(USER_ID)));
			noticeObj.setUser(userObj);
		}
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		
		try {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Retrieving template for class:"+clsName);
			}
			tmpl = templateFactory.getTemplate(noticeType.getTemplateLocation());
			tmpl.process(tokens, sw);
		} catch (TemplateException | TemplateNotFoundException | IOException e) {
			LOGGER.error("Error encountered while processing template data:"+e.getMessage());
			throw new NotificationTypeNotFound(e);
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Done processing temlate with data");
		}
		noticeObj.setEmailBody(sw.toString());
		noticeObj.setNoticeType(noticeType);
		noticeObj = noticeRepo.save(noticeObj);
		noticeRepo.flush();
		return noticeObj;


	}

	public Notice updateNotice(Notice noticeObj) {
		return noticeRepo.update(noticeObj);
	}

}
