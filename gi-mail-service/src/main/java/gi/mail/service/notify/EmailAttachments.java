package gi.mail.service.notify;

public class EmailAttachments {

	private byte[] attachmentBytes;
	private String attachmentName;
	private String contentType;

	/**
	 * @return the attachmentBytes
	 */
	public byte[] getAttachmentBytes() {
		return attachmentBytes.clone();
	}

	/**
	 * @param attachmentBytes
	 *            the attachmentBytes to set
	 */
	public void setAttachmentBytes(byte[] attachmentBytesGiven) {

		//this.attachmentBytes = attachmentBytesGiven;

		if (null == attachmentBytesGiven) {
			this.attachmentBytes = null;
			return;
		}

		System.arraycopy(attachmentBytesGiven, 0, this.attachmentBytes, 0,
				attachmentBytesGiven.length);
	}

	/**
	 * @return the attachmentName
	 */
	public String getAttachmentName() {
		return attachmentName;
	}

	/**
	 * @param attachmentName
	 *            the attachmentName to set
	 */
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	/**
	 * @return the attachmentContentType
	 */
	public String getContentType() {
		return this.contentType;
	}

	/**
	 * @param attachmentContentType
	 *            the attachmentContentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
