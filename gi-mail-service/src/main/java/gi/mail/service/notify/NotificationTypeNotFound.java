package gi.mail.service.notify;

public class NotificationTypeNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotificationTypeNotFound() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotificationTypeNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NotificationTypeNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotificationTypeNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotificationTypeNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
