package gi.mail.service.notify;

import com.getinsured.hix.model.ExitFlowConfigurationDTO;
import com.getinsured.hix.model.SelfServiceRegistrationConfigurationDTO;
import com.getinsured.hix.platform.enums.YesNoEnum;


public interface ConfigurationService {
 	String logoUrl(Long flowId, Long affiliateId, Long tenantId);
	String baseUrl(Long affiliateId, Long tenantId);
	String fromEmailAddress(Long affiliateId, Long tenantId);
	String postalAddress(Long affiliateId, Long tenantId);
	String customerCareNum(Long flowId, Long affiliateId, Long tenantId);
	String medicareUrl(Long flowId, Long affiliateId, Long tenantId);
	ExitFlowConfigurationDTO getExitOfferConfigurations(Integer flowId, Long affiliateId, Long tenantId);
	String getPrivacyTextForConsumer(Integer flowId);
	String getDisclaimerTextForEmailFooter(Long flowId, Long affiliateId,
			Long tenantId);
	SelfServiceRegistrationConfigurationDTO getSelfServiceRegistrationConfiguration(Long affiliateId, Long tenantId);
	YesNoEnum getShowPufPlansConfiguration(Long affiliateId, Long tenantId);
}
