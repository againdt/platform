package gi.mail.service.notify;

import com.getinsured.hix.model.Notice;

import gi.mail.service.notify.NotificationException;

public interface NotificationService {
	
	public void dispatch(Notice notice) throws NotificationException;
}
