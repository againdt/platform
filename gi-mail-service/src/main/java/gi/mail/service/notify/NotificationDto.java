package gi.mail.service.notify;

import org.apache.commons.lang.StringUtils;

import com.getinsured.hix.model.Location;
/**
 * POJO class to capture target audience address and print notice details.
 * 
 * @author Ekram
 *
 */
public class NotificationDto {
	
	/** indicates whether generated email notice would have associated PDF,
	 * if set to true,  ecmFileName and ecmRelativePath are required */
	private boolean isPrintable;
	
	/** user full name is required if NotificationDto is not null */
	private String userFullName;
	
	/** location is required if NotificationDto is not null */
	private Location location;
	
	/** ecmFileName is required if isPrintable is true */
	private String ecmFileName;
	
	/** ecmRelativePath is required if isPrintable is true */
	private String ecmRelativePath;

	public boolean isPrintable() {
		return isPrintable;
	}

	public void setPrintable(boolean isPrintable) {
		this.isPrintable = isPrintable;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getEcmFileName() {
		return ecmFileName;
	}

	public void setEcmFileName(String ecmFileName) {
		this.ecmFileName = ecmFileName;
	}

	public String getEcmRelativePath() {
		return ecmRelativePath;
	}

	public void setEcmRelativePath(String ecmRelativePath) {
		this.ecmRelativePath = ecmRelativePath;
	}
	
	public boolean validateAddressDetails() {
		boolean isValid = true;
		if (validateUserFullName() || !validateLocation()){
			isValid = false;
		} 
		return isValid;
	}

	private boolean validateUserFullName() {
		return StringUtils.isEmpty(userFullName);
	}

	private boolean validateLocation() {
		boolean isValid = true;
		if (location == null || StringUtils.isEmpty(location.getAddress1()) || StringUtils.isEmpty(location.getCity()) 
				|| StringUtils.isEmpty(location.getState()) || StringUtils.isEmpty(location.getZip())){
			isValid  = false;
		}
		return isValid;
	}
	
	public boolean validateEcmPreData() {
		boolean isValid = true;
		if (isPrintable && (StringUtils.isEmpty(ecmRelativePath) || StringUtils.isEmpty(ecmFileName))){
			isValid = false;
		}
		return isValid;
	}
	
}
