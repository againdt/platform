package gi.mail.service.notify;

import java.util.List;

import com.getinsured.hix.model.NoticeType;

public class Notification {

	private List<String> toRecipients;
	private List<String> bCCRecipients;
	private String fromAddress;
	private String toAddress;
	private String emailBody;
	private String attachment;
	private String subject;
	private NoticeType noticeType;

	public NoticeType getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(NoticeType noticeType) {
		this.noticeType = noticeType;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public List<String> getToRecipients() {
		return toRecipients;
	}

	public void setToRecipients(List<String> toRecipients) {
		this.toRecipients = toRecipients;
	}

	public List<String> getbCCRecipients() {
		return bCCRecipients;
	}

	public void setbCCRecipients(List<String> bCCRecipients) {
		this.bCCRecipients = bCCRecipients;
	}

}
