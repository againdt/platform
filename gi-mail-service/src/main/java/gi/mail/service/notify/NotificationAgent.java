package gi.mail.service.notify;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import gi.mail.service.notices.TemplateTokens;
import gi.mail.service.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

public abstract class NotificationAgent {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NotificationAgent.class);

	protected EmailService emailService = null;
	protected NoticeTypeRepository noticeTypeRepo = null;
	protected NoticeRepository noticeRepo = null;
	protected UserRepository userRepository = null;
	protected ApplicationContext appContext = null;
	protected ContentManagementService ecmService = null;
	protected GhixDBSequenceUtil ghixDBSequenceUtil = null;
	protected NoticeTemplateFactory templateFactory = null;

	
	public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
	public static final String EMAIL_FOOTERESP_LOCATION = "notificationTemplate/emailTemplateFooterEsp.html";
	
	public static final String ADDRESS_TEMPLATE_HEADER = "notificationTemplate/addressTemplateHeader.html";
	private static final String ADDRESS_TEMPLATE_HEADER_SPANISH="notificationTemplate/addressTemplateHeaderSpanish.html";

	
	private static final String EMPTY = "";
	
	
	public Notice generateEmail(String clsName, Location location, Map<String, String> emailData, Map<String,String> tokens)  throws NotificationTypeNotFound
	{
		LOGGER.info("Generating email...............");
		String noticeSeqId = null;
		//Required tokens info for header/footer, therefore, combining tokens and templateTokens.
		if(StringUtils.isBlank(tokens.get("baseUrl"))) {
			tokens.put(TemplateTokens.HOST, GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
		} else {
			tokens.put(TemplateTokens.HOST, tokens.get("baseUrl"));
		}
		LOGGER.debug("Exchange full Name " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		tokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		tokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		tokens.put(TemplateTokens.APPSERVER_URL,GhixPlatformEndPoints.APPSERVER_URL);
		
		tokens.put(TemplateTokens.PRIVACY_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));
		tokens.put(TemplateTokens.PRIVACY_URL_ESP, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL_ESP));
		tokens.put(TemplateTokens.FOOTER_YEAR, Integer.toString(Calendar.getInstance().get(Calendar.YEAR) )  );
		
		String privacyStatement = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_STATEMENT);
		if(privacyStatement != null){
			tokens.put(TemplateTokens.PRIVACY_STATEMENT,privacyStatement);
		}
		String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
		if(contactInformation != null){
			tokens.put(TemplateTokens.CONTACT_INFORMATION,contactInformation);
		}
		tokens.put("userName", tokens.get("name"));


		try {
			noticeSeqId = ghixDBSequenceUtil.getNextSequenceFromDB(Notice.NOTICESEQUENCE.notices_seq.toString());
			tokens.put(TemplateTokens.NOTICE_UNIQUE_ID, StringUtils.leftPad(noticeSeqId, GhixPlatformConstants.TEN, GhixPlatformConstants.ZERO));
			
			tokens.put(TemplateTokens.HEADER_CONTENT,this.getTemplateContentWithTokensReplaced(tokens,EMAIL_HEADER_LOCATION));
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Done creating email header");
			}
			tokens.put(TemplateTokens.FOOTER_CONTENT,this.getTemplateContentWithTokensReplaced(tokens, EMAIL_FOOTER_LOCATION));
			tokens.put(TemplateTokens.SPAINISH_FOOTER_CONTENT,this.getTemplateContentWithTokensReplaced(tokens, EMAIL_FOOTERESP_LOCATION));
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Done creating email footer");
			}
			if(location != null ){
				tokens.put(TemplateTokens.ADDRESS_CONTENT, populatreAddressHeaderTemplate(ADDRESS_TEMPLATE_HEADER,tokens.get("name"),location));
				tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, populatreAddressHeaderTemplate(ADDRESS_TEMPLATE_HEADER_SPANISH,tokens.get("name"),location));
			}else{
				//set empty
				tokens.put(TemplateTokens.ADDRESS_CONTENT, EMPTY);
				tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, EMPTY);
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Done populating templates with values");
			}
		}catch (Exception e) {
			throw new NotificationTypeNotFound("Error reading template from Resource - "+e.getMessage(), e);
		}
		return this.createEmail(emailData,tokens, clsName, noticeSeqId);
	}
	
	private Notice createEmail(Map<String, String> emailData, Map<String,String> tokens, String clsName, String noticeSeqId) throws NotificationTypeNotFound{
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Creating email........");
		}
		NoticeType noticeType = this.templateFactory.getNoticeType(clsName);
		if(noticeType == null){
			throw new NotificationTypeNotFound("No notofications found for name:"+clsName);
		}
		Notice noticeObj = new Notice();
		try {
			noticeObj.setId(Integer.parseInt(noticeSeqId));
		} catch (Exception e) {
			throw new GIRuntimeException("Notice created without Unique ID");
		}

	//	noticeObj.setToAddress(emailData.get("To"));
		String cc = StringUtils.isEmpty(emailData.get("Cc"))  ? "" : emailData.get("Cc");
		noticeObj.setCcAddress(cc);
		String bcc = StringUtils.isEmpty(emailData.get("Bcc"))  ? "" : emailData.get("Bcc");
		noticeObj.setBccAddress(bcc);
		String subject = StringUtils.isEmpty(emailData.get("Subject"))  ? noticeType.getEmailSubject() : emailData.get("Subject");
		noticeObj.setSubject(subject);
		String from = StringUtils.isEmpty(emailData.get("From"))  ? noticeType.getEmailFrom() : emailData.get("From");
		noticeObj.setFromAddress(from);
		String to = emailData.get("To");
		if(StringUtils.isEmpty(to)){
			to = noticeType.getEmailTo();
			LOGGER.warn("No Receipient address found in the email data , using receipient field data from Notie type:"+to);
		}
		noticeObj.setToAddress(to);
		String attachment = StringUtils.isEmpty(emailData.get("Attachment"))  ? null : emailData.get("Attachment");
		noticeObj.setAttachment(attachment);
		Integer keyId =  emailData.get("KeyId") != null ? Integer.parseInt(emailData.get("KeyId")) : 0;
		noticeObj.setKeyId(keyId);
		noticeObj.setKeyName( ( emailData.get("KeyName") != null) ? emailData.get("KeyName") : null);

		boolean checkUserId=true;
		if(StringUtils.isEmpty(emailData.get("UserId"))){
			checkUserId = false;
		}
		if(checkUserId){
			AccountUser userObj = userRepository.findById(Integer.parseInt(emailData.get("UserId")));
			noticeObj.setUser(userObj);
		}
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Retrieving template for class:"+clsName);
			}
			tmpl = templateFactory.getTemplate(noticeType.getTemplateLocation());
			tmpl.process(tokens, sw);
		} catch (TemplateException | TemplateNotFoundException | IOException e) {
			LOGGER.error("Error encountered while processing template data:"+e.getMessage());
			throw new NotificationTypeNotFound(e);
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Done processing temlate with data");
		}
		noticeObj.setEmailBody(sw.toString());
		noticeObj.setNoticeType(noticeType);
		noticeObj = noticeRepo.save(noticeObj);
		noticeRepo.flush();
		return noticeObj;
	}

	public Notice sendEmail(Notice noticeObj, Map<GhixUtils.EMAIL_STATS, String> emailStatData){
		try{
			
			this.emailService.dispatch(noticeObj,emailStatData);
			
			noticeObj.setSentDate(new Date ());
			noticeObj.setStatus(STATUS.EMAIL_SENT);
		}catch (Exception e) {
			LOGGER.error("Email sending failed "+e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}
		LOGGER.debug("Done sending email......updating the repository");
		noticeObj = noticeRepo.update(noticeObj);
		return noticeObj;
	}
	
	public Notice sendEmail(Notice noticeObj){
		try{
			
			this.emailService.dispatch(noticeObj);
			noticeObj.setSentDate(new Date ());
			noticeObj.setStatus(STATUS.EMAIL_SENT);
		}catch (Exception e) {
			LOGGER.error("Email sending failed "+e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}
		LOGGER.debug("Done sending email......updating the repository");
		noticeObj = noticeRepo.update(noticeObj);
		return noticeObj;
	}
	
	public String getTemplateContentWithTokensReplaced(Map<String, String> tokens, String location) throws TemplateNotFoundException, IOException, TemplateException {
		Template tmpl = null;
		StringWriter sw = new StringWriter();
		tmpl = templateFactory.getTemplate(location);
		tmpl.process(tokens, sw);
		String content = sw.toString();
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(" Generated content for "+location+" Content: "+content);
		}
		return sw.toString();

	}


	/**
	 *
	 * @param noticeObj
	 * @return
	 * @throws Exception
	 */
	public Notification generateNotification(Notice noticeObj)throws Exception
	{
		NoticeType noticeType = noticeTypeRepo.findByEmailClass(getClass().getSimpleName());
		if (noticeType == null)
		{
			throw new NotificationTypeNotFound(this.getClass().getName());
		}
		if(noticeObj == null)
		{
			throw new Exception("Notice Object is null");
		}
		Notification notificationObj = new Notification();
		String toRecipientsAddress = noticeObj.getToAddress();
		if(StringUtils.isNotEmpty(toRecipientsAddress))
		{
			List<String> emailRecipientDetail = new ArrayList<String>();
			if(toRecipientsAddress.contains(";"))
			{
				String splitBuff[] =  toRecipientsAddress.split(";");
				if(splitBuff != null)
				{
					for(String splitEmailAddress: splitBuff)
					{
						emailRecipientDetail.add(splitEmailAddress);
					}
				}
				notificationObj.setToRecipients(emailRecipientDetail);
			}
			else
			{
				emailRecipientDetail.add(toRecipientsAddress);
				notificationObj.setToRecipients(emailRecipientDetail);
			}
		}
		String toBccAddress = noticeObj.getBccAddress();
		if(StringUtils.isNotEmpty(toBccAddress))
		{
			List<String> emailRecipientDetail = new ArrayList<String>();
			if(toBccAddress.contains(";"))
			{
				String splitBuff[] =  toBccAddress.split(";");
				if(splitBuff != null)
				{
					for(String splitEmailAddress: splitBuff)
					{
						emailRecipientDetail.add(splitEmailAddress);
					}
				}
				notificationObj.setbCCRecipients(emailRecipientDetail);
			}
			else
			{
				emailRecipientDetail.add(toRecipientsAddress);
				notificationObj.setbCCRecipients(emailRecipientDetail);
			}
		}
		notificationObj.setAttachment(noticeObj.getAttachment());
		notificationObj.setEmailBody(noticeObj.getEmailBody());
		notificationObj.setFromAddress(noticeObj.getFromAddress());
		notificationObj.setNoticeType(noticeType);
		notificationObj.setSubject(noticeObj.getSubject());

		return notificationObj;
	}


	/**
	 * @Since 23rd April 2014
	 * @param noticeObj
	 * @return
	 */
	public Notice sendEmailToMultipleRecipient(Notification notificationObj, Notice noticeObj)
	{
		try{
			this.emailService.dispatchToAll(notificationObj);
			noticeObj.setSentDate(new Date ());
			noticeObj.setStatus(STATUS.EMAIL_SENT);
		}catch (Exception e) {
			LOGGER.error("Email sending failed "+e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}
		//noticeObj = noticeRepo.save(noticeObj);
		noticeObj = noticeRepo.update(noticeObj);
		//noticeRepo.flush();
		return noticeObj;
	}

	private String populatreAddressHeaderTemplate(String templateName, String userFullName,Location location) throws NoticeServiceException
	{
		Map<String, Object> replaceableObj = new HashMap<String, Object> ();
		//place empty if no detail available
		replaceableObj.put(TemplateTokens.USER_FULL_NAME, (null==userFullName)?EMPTY:userFullName);
		//replaceableObj.put(TemplateTokens.USER_LAST_NAME, (null==user.getLastName())?EMPTY:user.getLastName());
		replaceableObj.put(TemplateTokens.ADDRESS_LINE_1, (null== location.getAddress1())?EMPTY:location.getAddress1());
		replaceableObj.put(TemplateTokens.ADDRESS_LINE_2, (null== location.getAddress2())?EMPTY:location.getAddress2());
		replaceableObj.put(TemplateTokens.CITY_NAME, (null== location.getCity())?EMPTY:location.getCity());
		replaceableObj.put(TemplateTokens.STATE_CODE, (null== location.getState())?EMPTY:location.getState());
		replaceableObj.put(TemplateTokens.PIN_CODE, (null == location.getZip())?EMPTY:location.getZip());

		StringWriter sw = new StringWriter();

		try {
			Template tmpl = templateFactory.getTemplate(templateName);
			tmpl.process(replaceableObj, sw);

		} catch (Exception e) {
			throw new NoticeServiceException(e);
		}
		finally{
			IOUtils.closeQuietly(sw);
		}
		return sw.toString();
	}
	
	public abstract Map<String, String> getTokens(Map<String, Object> notificationContext);

	public abstract Map<String, String> getEmailData(Map<String, Object> notificationContext);
}
