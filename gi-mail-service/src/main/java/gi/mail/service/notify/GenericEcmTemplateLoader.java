package gi.mail.service.notify;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

import freemarker.cache.TemplateLoader;
import gi.mail.service.notification.NoticeTmplHelper;

@Component
public class GenericEcmTemplateLoader implements TemplateLoader {
	Logger LOGGER = LoggerFactory.getLogger(GenericEcmTemplateLoader.class);
	
	@Autowired private NoticeTmplHelper noticeTmplHelper;
	
	private static Map<String, TemplateSourceWrapper> contentIdmap = Collections.synchronizedMap(new HashMap<String, TemplateSourceWrapper>());

	@Override
	public void closeTemplateSource(Object templateSource) throws IOException {
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Closing source:"+templateSource.getClass().getName());
		}
		TemplateSourceWrapper notice = (TemplateSourceWrapper)templateSource;
		Reader reader = notice.getSourceReader();
		if(reader != null){
			reader.close();
		}
		
	}
	
	@Override
	public Object findTemplateSource(String path) throws IOException {
		if("N".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))){
			LOGGER.info("ECM Not enabled, falling back on local resources for path:"+path);
			return null;
		}
		LOGGER.debug("Trying to find the template for "+path+" Using ECM");
		int fileExtnIndex = path.lastIndexOf(".");
		if(fileExtnIndex == -1){
			LOGGER.info("No file extension found for path:"+path+" Not attempting to fetch, probably a GHIX Email template, this loader does not support contents without a valid extension");
			return null;
		}
		String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH);
		ecmTemplateFolderPath += path;
		
		TemplateSourceWrapper tmp = contentIdmap.get(ecmTemplateFolderPath);
		if(tmp != null){
			LOGGER.info("Resource found:"+path);
			return tmp;
		}
		
		try {
			tmp = getEcmContent(ecmTemplateFolderPath);
		} catch (ContentManagementServiceException e) {
			throw new IOException(e.getMessage(), e);
		}
		
		return tmp;
	}

	/**
	 * Ideally this should be called every time when the template is accessed but that will add another overhead of ECM call
	 * May be a time delay, refresh after every 1 hr or so?
	 */
	@Override
	public long getLastModified(Object templateSource) {
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper)templateSource;
		if(wrapper.shouldRefreshTheSource()){
			contentIdmap.remove(wrapper.getPath());
			return System.currentTimeMillis();
		}
		Content content = (Content)wrapper.getTemplateSource();
		return content.getModifiedDate().getTime();
	}

	@Override
	public Reader getReader(Object templateSource, String encoding) throws IOException {
		Exception ex = null;
		InputStreamReader reader = null;
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper)templateSource;
		if(wrapper.isExpired()){
			LOGGER.info("Template found expired while reading, initializing again");
			wrapper = (TemplateSourceWrapper) findTemplateSource(wrapper.getPath());
		}
		Content ecmContent  = (Content)wrapper.getTemplateSource();
		
		try {
			reader = new InputStreamReader(getTemplateStreamFromECM(ecmContent));
			wrapper.setSourceReader(reader);
		} catch (ContentManagementServiceException | NotificationTypeNotFound e) {
			ex = e;
		}
		if(ex != null){
			throw new IOException(ex.getMessage(), ex);
		}
		LOGGER.debug("Template reader provided");
		return reader;
	}
	
	private InputStream getTemplateStreamFromECM(Content content) throws ContentManagementServiceException,NotificationTypeNotFound {
		return new ByteArrayInputStream(noticeTmplHelper.readBytesByEcmId(content.getContentId()));
	}
	
	private TemplateSourceWrapper getEcmContent(String templatePath) throws ContentManagementServiceException {
		TemplateSourceWrapper content = null;
		Content ecmContent = null;
		LOGGER.debug("Extracting content Id from ECM for path "+templatePath);
		try{
			ecmContent = noticeTmplHelper.readContentMetaByPath(templatePath);;
			LOGGER.info("Received content from source:"+Content.CONTENT_SOURCES[ecmContent.getContentSource()]);
		}catch(ContentManagementServiceException cme){
			LOGGER.warn("Path:"+templatePath+" does not exist");
			return null;
		}
		if(LOGGER.isDebugEnabled() && ecmContent != null){
			LOGGER.debug("Retrieved content Id ["+ecmContent.getContentId()+"] ECM, Caching locally");
		}
		if(ecmContent == null){
			LOGGER.error("No content found for path:"+templatePath);
			return null;
		}
		content = new TemplateSourceWrapper(ecmContent);
		content.setPath(templatePath);
		contentIdmap.put(templatePath, content);
		return content;
	}

}
