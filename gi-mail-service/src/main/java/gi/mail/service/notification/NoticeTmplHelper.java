package gi.mail.service.notification;

import java.nio.charset.StandardCharsets;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.couchbase.helper.BucketMetaHelper;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.factory.ECMFactory;

/**
 * Component to talk to ECM to fetch notice template meta info and bytes.
 * 
 */
@Component
public class NoticeTmplHelper {
	
	private static final String NO_BYTE_OR_CONTENT_INFORMATION_FOUND = "No byte or content information found!";

	private static final String PHIX = "PHIX";
	
	@Autowired private ContentManagementService ecmService;
	
	@Value("#{configProp['couchbase.nonbinary.bucketname']}")
	private String nonbinaryBucketName;
	
	@Value("#{configProp['ecm.type']}")
	private String ecmType;

	public static final String COUCHBASE_TMPL_QUERY = 
			"select * from %s where metaData.tenantCode = '%s' and metaData.branch = '%s' and name = '%s' ";
	
	/* should we have this check during server startup - fast fail check */
	@PostConstruct
	public void doPost(){
		if (ECMFactory.ECM.COUCH.toString().equals(ecmType)){
			//EKRAM: fix me
			//if (!StringUtils.equalsIgnoreCase(PHIX, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))){
			//	
			//}
		}
	}

	public String formQuery(String templatePath){
		
		if (ECMFactory.ECM.COUCH.toString().equals(ecmType)){
			String templateNames[] = templatePath.split("/");
			return String.format(COUCHBASE_TMPL_QUERY, nonbinaryBucketName, BucketMetaHelper.getTenantCode(), BucketMetaHelper.getBuildArtifactVersion(), templateNames[templateNames.length - 1]);
		} 
		
		return templatePath;
	}
	
	public byte[] readBytesByPath(String path) throws ContentManagementServiceException{
		return ecmService.getContentDataByPath(formQuery(path));
	}
	
	public byte[] readBytesByEcmId(String ecmId) throws ContentManagementServiceException{
		if (ECMFactory.ECM.COUCH.toString().equals(ecmType)){
			Content content = readContentMetaByEcmId(ecmId);
			
			if (content == null || StringUtils.isBlank(content.getContent())){
				throw new ContentManagementServiceException(NO_BYTE_OR_CONTENT_INFORMATION_FOUND);
			}
			
			return content.getContent().getBytes(StandardCharsets.UTF_8);
		}
		return ecmService.getContentDataById(ecmId);
	}
	
	public Content readContentMetaByPath(String path) throws ContentManagementServiceException{
		return ecmService.getContentByPath(formQuery(path), true);
	}
	
	public Content readContentMetaByEcmId(String ecmId) throws ContentManagementServiceException{
		return ecmService.getContentById(ecmId);
	}

}
