package gi.mail.service.notification;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import gi.mail.service.notices.TemplateTokens;
import gi.mail.service.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

@Component
public class NotificationAgent {

	private static final String N = "N";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NotificationAgent.class);

	@Autowired private EmailService emailService;
	@Autowired private NoticeTypeRepository noticeTypeRepo;
	@Autowired private NoticeRepository noticeRepo;
	@Autowired private UserRepository userRepository;
	@Autowired private ApplicationContext appContext;
	@Autowired private GhixDBSequenceUtil ghixDBSequenceUtil;
	
	private Location location;
	private Map<String,String> tokens;
	private NoticeType noticeType;

	public Map<String, Object> notificationData;
	public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
	public static final String ADDRESS_TEMPLATE_HEADER = "notificationTemplate/addressTemplateHeader.html";
	private static final String ADDRESS_TEMPLATE_HEADER_SPANISH="notificationTemplate/addressTemplateHeaderSpanish.html";
	public static final String EMAIL_FOOTERESP_LOCATION = "notificationTemplate/emailTemplateFooterEsp.html";

	private static final String EMPTY = "";
	private static final String NOTICE_TEMPLATE = "noticeTemplate";

	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	public void setTokens(Map<String,String> tokens)
	{
		this.tokens = tokens;
	}
	public Map<String,String> getTokens(){
		return tokens;
	}

	private void setNoticeType() throws NotificationTypeNotFound
	{
		this.noticeType = noticeTypeRepo.findByEmailClass(getClass().getSimpleName());
		if (noticeType == null)
		{
			NotificationTypeNotFound notFound = new NotificationTypeNotFound();
			LOGGER.warn("Error while getting template from database "+notFound.getMessage());
			throw notFound;
		}
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Notice generateEmail()  throws NotificationTypeNotFound
	{
		setNoticeType();
		Map<String, String> emailData = this.getSingleData();
		String noticeSeqId = null;
		/**
		 * @author Sunil D
		 * Reading the header and footer content from the common templates, replace the tokens and add it to the main template.
		 *
		 */
		//Required tokens info for header/footer, therefore, combining tokens and templateTokens.
		if(StringUtils.isBlank(tokens.get("baseUrl"))) {
			tokens.put(TemplateTokens.HOST, GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
		} else {
			tokens.put(TemplateTokens.HOST, tokens.get("baseUrl"));
		}
		LOGGER.debug("Exchange full Name " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		tokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		tokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		tokens.put(TemplateTokens.APPSERVER_URL,GhixPlatformEndPoints.APPSERVER_URL);
		tokens.put(TemplateTokens.FOOTER_YEAR, Integer.toString(Calendar.getInstance().get(Calendar.YEAR) )  );
		
		String privacyStatement = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_STATEMENT);
		if(privacyStatement != null){
			tokens.put(TemplateTokens.PRIVACY_STATEMENT,privacyStatement);
		}
		String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
		if(contactInformation != null){
			tokens.put(TemplateTokens.CONTACT_INFORMATION,contactInformation);
		}
		tokens.put("userName", tokens.get("name"));

		InputStream addressInputstream = null;
		InputStream addressInputstreamSpanish = null;

		try {
			noticeSeqId = ghixDBSequenceUtil.getNextSequenceFromDB(Notice.NOTICESEQUENCE.notices_seq.toString());
			tokens.put(TemplateTokens.NOTICE_UNIQUE_ID, StringUtils.leftPad(noticeSeqId, GhixPlatformConstants.TEN, GhixPlatformConstants.ZERO));
			tokens.put(TemplateTokens.HEADER_CONTENT,this.getTemplateContentWithTokensReplaced(tokens, EMAIL_HEADER_LOCATION));
			
			tokens.put(TemplateTokens.PRIVACY_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));
			tokens.put(TemplateTokens.PRIVACY_URL_ESP, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL_ESP));
			
			tokens.put(TemplateTokens.FOOTER_CONTENT,this.getTemplateContentWithTokensReplaced(tokens, EMAIL_FOOTER_LOCATION));
			tokens.put(TemplateTokens.SPAINISH_FOOTER_CONTENT,this.getTemplateContentWithTokensReplaced(tokens, EMAIL_FOOTERESP_LOCATION));
			//For a quick solution added for address template. But we need to refactor the notice/email API for common operation
			if(getLocation()!= null ){
	
				if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE).equalsIgnoreCase(N)){
					addressInputstream = getTemplateFromResources(ADDRESS_TEMPLATE_HEADER);
					addressInputstreamSpanish = getTemplateFromResources(ADDRESS_TEMPLATE_HEADER_SPANISH);
				}
				else{
					addressInputstream = getTemplateFromECM(ADDRESS_TEMPLATE_HEADER);
					addressInputstreamSpanish = getTemplateFromECM(ADDRESS_TEMPLATE_HEADER_SPANISH);
				}

				tokens.put(TemplateTokens.ADDRESS_CONTENT, populatreAddressHeaderTemplate(addressInputstream,tokens.get("name"),getLocation()));
				tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, populatreAddressHeaderTemplate(addressInputstreamSpanish,tokens.get("name"),getLocation()));
			}else{
				//set empty
				tokens.put(TemplateTokens.ADDRESS_CONTENT, EMPTY);
				tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, EMPTY);
			}
		}catch (IOException  | ContentManagementServiceException | NoticeServiceException e) {
			LOGGER.error("Error reading template from Resource - " + e);
		}finally{
			IOUtils.closeQuietly(addressInputstream);
			IOUtils.closeQuietly(addressInputstreamSpanish);
		}
		//After generating the notice just make the location as null. We don't care about communication pref.
		setLocation(null);
		return this.createEmail(emailData,noticeSeqId);
	}

	//dummy method will be invoked from child class
	public Map<String, String> getSingleData(){
		return null;
	}


	private Notice createEmail(Map<String, String> emailData, String noticeSeqId) throws NotificationTypeNotFound{

		Notice noticeObj = new Notice();
		try {
			noticeObj.setId(Integer.parseInt(noticeSeqId));
		} catch (Exception e) {
			LOGGER.error("Notice can not created without Unique ID.notice id-"+noticeSeqId +e);
			throw new GIRuntimeException("Notice created without Unique ID");
		}

	//	noticeObj.setToAddress(emailData.get("To"));
		String cc = StringUtils.isEmpty(emailData.get("Cc"))  ? "" : emailData.get("Cc");
		noticeObj.setCcAddress(cc);
		String bcc = StringUtils.isEmpty(emailData.get("Bcc"))  ? "" : emailData.get("Bcc");
		noticeObj.setBccAddress(bcc);
		String subject = StringUtils.isEmpty(emailData.get("Subject"))  ? this.getNoticeType().getEmailSubject() : emailData.get("Subject");
		noticeObj.setSubject(subject);
		String from = StringUtils.isEmpty(emailData.get("From"))  ? this.getNoticeType().getEmailFrom() : emailData.get("From");
		noticeObj.setFromAddress(from);
		String to = StringUtils.isEmpty(emailData.get("To"))  ? this.getNoticeType().getEmailTo() : emailData.get("To");
		noticeObj.setToAddress(to);
		String attachment = StringUtils.isEmpty(emailData.get("Attachment"))  ? null : emailData.get("Attachment");
		noticeObj.setAttachment(attachment);
		Integer keyId =  emailData.get("KeyId") != null ? Integer.parseInt(emailData.get("KeyId")) : 0;
		noticeObj.setKeyId(keyId);
		noticeObj.setKeyName( ( emailData.get("KeyName") != null) ? emailData.get("KeyName") : null);

		boolean checkUserId=true;
		if(StringUtils.isEmpty(emailData.get("UserId"))){
			checkUserId = false;
		}
		if(checkUserId){
			AccountUser userObj = userRepository.findById(Integer.parseInt(emailData.get("UserId")));
			noticeObj.setUser(userObj);
		}
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		String templateContent;
		InputStream inputStreamUrl = null;
		try {
			// As per HIX-27201 - To get templates from ECM - By Darshan Hardas
//			Resource resource = appContext.getResource("classpath:"+noticeType.getTemplateLocation());
//			inputStreamUrl = resource.getInputStream();
			//inputStreamUrl= new URL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL + noticeType.getTemplateLocation()).openStream();
			if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE).equalsIgnoreCase(N)){
				inputStreamUrl = getTemplateFromResources(noticeType.getTemplateLocation());
			}
			else{
				inputStreamUrl = getTemplateFromECM(noticeType.getTemplateLocation());
			}


			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (ContentManagementServiceException cmse) {
			LOGGER.error("Error reading template ", cmse);
			throw new NotificationTypeNotFound("Error reading template from  - " + noticeType.getTemplateLocation() , cmse);
		} catch (IOException ioe) {
			LOGGER.error("Error reading template ", ioe);
			throw new NotificationTypeNotFound("Error reading template from  - " + noticeType.getTemplateLocation() , ioe);
		}
		finally
		{
			IOUtils.closeQuietly(inputStreamUrl);
		}


		Template tmpl = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
		}
		noticeObj.setEmailBody(sw.toString());
		noticeObj.setNoticeType(this.getNoticeType());
		noticeObj = noticeRepo.save(noticeObj);
		noticeRepo.flush();

		return noticeObj;

	}

	@Autowired private NoticeTmplHelper noticeTmplHelper;
	
	private InputStream getTemplateFromECM(String location) throws ContentManagementServiceException,NotificationTypeNotFound {
		if(null==noticeType){
			setNoticeType();
		}
		String ecmContentId = noticeType.getExternalId();
		if (noticeType.getTemplateLocation().equalsIgnoreCase(location) && ecmContentId==null) {
			ecmContentId = getEcmContentId(noticeType);
			return new ByteArrayInputStream(noticeTmplHelper.readBytesByEcmId(ecmContentId));
		}
		else{
			String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
			 	+ location;
			
			return new ByteArrayInputStream(noticeTmplHelper.readBytesByPath(ecmTemplateFolderPath));
			
		}

	}

	private String getEcmContentId(NoticeType noticeType) throws ContentManagementServiceException {
		String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
				+ noticeType.getTemplateLocation();

		Content content = noticeTmplHelper.readContentMetaByPath(ecmTemplateFolderPath);

		String ecmContentId = content.getContentId();
		noticeType.setExternalId(ecmContentId);
		noticeType.setUpdated(new Date());
		
		// save the content id to DB
		NoticeType updatedNotice = noticeTypeRepo.save(noticeType);
		return updatedNotice.getExternalId();

	}

	private InputStream getTemplateFromResources(String string) throws  IOException {
		Resource resource = appContext.getResource("classpath:"+string);
		InputStream inputStreamUrl = resource.getInputStream();
	//	inputStreamUrl= new URL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL + noticeType.getTemplateLocation()).openStream();
		return inputStreamUrl;
	}

	public Notice sendEmail(Notice noticeObj){
		try{
			this.emailService.dispatch(noticeObj);
			noticeObj.setSentDate(new Date ());
			noticeObj.setStatus(STATUS.EMAIL_SENT);
		}catch (Exception e) {
			LOGGER.error("Email sending failed "+e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}
		//noticeObj = noticeRepo.save(noticeObj);
		noticeObj = noticeRepo.update(noticeObj);
		//noticeRepo.flush();
		return noticeObj;
	}

	public NoticeType getNoticeType(){
		return this.noticeType;
	}

	public String getTemplateContentWithTokensReplaced(Map<String, String> tokens, String location ){
		InputStream inputStreamUrl = null;
		String templateContent = "";
		try {
			if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE).equalsIgnoreCase(N)){
				inputStreamUrl = getTemplateFromResources(location);
			}
			else{
				inputStreamUrl = getTemplateFromECM(location);
			}

			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (Exception e) {
			LOGGER.error("Error reading content from  - " + location , e);
		}
		finally{
			IOUtils.closeQuietly(inputStreamUrl);
		}
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
		}
		return sw.toString();

	}


	/**
	 *
	 * @param noticeObj
	 * @return
	 * @throws Exception
	 */
	public Notification generateNotification(Notice noticeObj)throws Exception
	{
		if(noticeObj == null)
		{
			throw new Exception("Notice Object is null");
		}
		Notification notificationObj = new Notification();
		String toRecipientsAddress = noticeObj.getToAddress();
		if(StringUtils.isNotEmpty(toRecipientsAddress))
		{
			List<String> emailRecipientDetail = new ArrayList<String>();
			if(toRecipientsAddress.contains(";"))
			{
				String splitBuff[] =  toRecipientsAddress.split(";");
				if(splitBuff != null)
				{
					for(String splitEmailAddress: splitBuff)
					{
						emailRecipientDetail.add(splitEmailAddress);
					}
				}
				notificationObj.setToRecipients(emailRecipientDetail);
			}
			else
			{
				emailRecipientDetail.add(toRecipientsAddress);
				notificationObj.setToRecipients(emailRecipientDetail);
			}
		}
		String toBccAddress = noticeObj.getBccAddress();
		if(StringUtils.isNotEmpty(toBccAddress))
		{
			List<String> emailRecipientDetail = new ArrayList<String>();
			if(toBccAddress.contains(";"))
			{
				String splitBuff[] =  toBccAddress.split(";");
				if(splitBuff != null)
				{
					for(String splitEmailAddress: splitBuff)
					{
						emailRecipientDetail.add(splitEmailAddress);
					}
				}
				notificationObj.setbCCRecipients(emailRecipientDetail);
			}
			else
			{
				emailRecipientDetail.add(toRecipientsAddress);
				notificationObj.setbCCRecipients(emailRecipientDetail);
			}
		}
		notificationObj.setAttachment(noticeObj.getAttachment());
		notificationObj.setEmailBody(noticeObj.getEmailBody());
		notificationObj.setFromAddress(noticeObj.getFromAddress());
		notificationObj.setNoticeType(noticeType);
		notificationObj.setSubject(noticeObj.getSubject());

		return notificationObj;
	}


	/**
	 * @Since 23rd April 2014
	 * @param noticeObj
	 * @return
	 */
	public Notice sendEmailToMultipleRecipient(Notification notificationObj, Notice noticeObj)
	{
		try{
			this.emailService.dispatchToAll(notificationObj);
			noticeObj.setSentDate(new Date ());
			noticeObj.setStatus(STATUS.EMAIL_SENT);
		}catch (Exception e) {
			LOGGER.error("Email sending failed "+e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}
		//noticeObj = noticeRepo.save(noticeObj);
		noticeObj = noticeRepo.update(noticeObj);
		//noticeRepo.flush();
		return noticeObj;
	}

	private String populatreAddressHeaderTemplate(InputStream inputStreamTemplate,String userFullName,Location location) throws NoticeServiceException
	{
		Map<String, Object> replaceableObj = new HashMap<String, Object> ();
		//place empty if no detail available
		replaceableObj.put(TemplateTokens.USER_FULL_NAME, (null==userFullName)?EMPTY:userFullName);
		//replaceableObj.put(TemplateTokens.USER_LAST_NAME, (null==user.getLastName())?EMPTY:user.getLastName());
		replaceableObj.put(TemplateTokens.ADDRESS_LINE_1, (null== location.getAddress1())?EMPTY:location.getAddress1());
		replaceableObj.put(TemplateTokens.ADDRESS_LINE_2, (null== location.getAddress2())?EMPTY:location.getAddress2());
		replaceableObj.put(TemplateTokens.CITY_NAME, (null== location.getCity())?EMPTY:location.getCity());
		replaceableObj.put(TemplateTokens.STATE_CODE, (null== location.getState())?EMPTY:location.getState());
		replaceableObj.put(TemplateTokens.PIN_CODE, (null == location.getZip())?EMPTY:location.getZip());

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		try {
			stringLoader.putTemplate(NOTICE_TEMPLATE, IOUtils.toString(inputStreamTemplate, "UTF-8"));
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate(NOTICE_TEMPLATE);
			tmpl.process(replaceableObj, sw);

		} catch (Exception e) {
			throw new NoticeServiceException(e);
		}
		finally{
			IOUtils.closeQuietly(sw);
		}
		return sw.toString();
	}
}
