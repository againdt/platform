package gi.mail.service.notification;

import com.getinsured.hix.model.CampaignDTO;
import com.getinsured.hix.model.EmxFlowConfigurationDTO;
import com.getinsured.hix.model.ExitFlowConfigurationDTO;
import com.getinsured.hix.model.PartnerConfigReponseDto;
import com.getinsured.hix.model.SelfServiceRegistrationConfigurationDTO;



public interface ConfigurationService {
 	String logoUrl(Long flowId, Long affiliateId, Long tenantId);
	String baseUrl(Long flowId, Long affiliateId, Long tenantId);
	String fromEmailAddress(Long affiliateId, Long tenantId);
	String postalAddress(Long affiliateId, Long tenantId);
	String customerCareNum(Long flowId, Long affiliateId, Long tenantId);
	String medicareUrl(Long flowId, Long affiliateId, Long tenantId);
	ExitFlowConfigurationDTO getExitOfferConfigurations(Integer flowId, Long affiliateId, Long tenantId);
	ExitFlowConfigurationDTO getIncompleteEnrollmentExitOfferConfigurations(Integer flowId, Long affiliateId, Long tenantId);
	EmxFlowConfigurationDTO getEmxConfigurations(Integer flowId);
	String getPrivacyTextForConsumer(Integer flowId);
	String getCustomerAgreementText(Long affiliateId, Long tenantId);
	
	/**
	 * @since 09th September 2015
	 * Jira Id: https://jira.getinsured.com/browse/HIX-75490
	 * @param affiliateId Long
	 * @param tenantId Long
	 * @return String Tenant/Super Affiliate name
	 */
	String getCompanyName(Long flowId, Long affiliateId, Long tenantId);
	CampaignDTO getCampaignConfigurations(Integer flowId);
	String callCenterHours(Long affiliateId, Long tenantId);
	String getDisclaimerTextForEmailFooter(Long flowId, Long affiliateId,
			Long tenantId);
	
	PartnerConfigReponseDto getPartnerConfig(String tenantId,String affiliateId, String flowId);
	SelfServiceRegistrationConfigurationDTO getSelfServiceRegistrationConfiguration(Long affiliateId, Long tenantId);
}
