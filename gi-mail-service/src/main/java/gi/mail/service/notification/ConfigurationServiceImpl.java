package gi.mail.service.notification;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.enums.AffiliateAncillaryStatus;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.CampaignDTO;
import com.getinsured.hix.model.EmxFlowConfigurationDTO;
import com.getinsured.hix.model.ExitFlowConfigurationDTO;
import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.model.PartnerConfigReponseDto;
import com.getinsured.hix.model.ProductConfigurationDTO;
import com.getinsured.hix.model.SelfServiceRegistrationConfigurationDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.UIConfigurationDTO;
import com.getinsured.hix.platform.repository.AffiliateFlowRepository;
import com.getinsured.hix.platform.repository.AffiliateRepository;
import com.getinsured.hix.platform.repository.GIAppConfigRepository;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixUtils;

@Component
public class ConfigurationServiceImpl implements ConfigurationService {
	
	private static final String HTTPS = "https://";

	private static final String GLOBAL_EXCHANGE_PHONE = "global.ExchangePhone";

	private static final Logger LOGGER  = LoggerFactory.getLogger(ConfigurationServiceImpl.class);
	
	@Autowired
	private AffiliateFlowRepository affiliateFlowRepository;
	
	@Autowired
	private AffiliateRepository affiliateRepository;
	
	@Autowired
	private TenantService tenantService;
	
	@Autowired
	private GIAppConfigRepository gIAppConfigRepository;

	@Override
	public String logoUrl(Long flowId, Long affiliateId, Long tenantId) {
		String returnLogoUrl = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && StringUtils.isNotBlank(affiliateFlow.getLogoURL())) {
				returnLogoUrl = affiliateFlow.getLogoURL();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnLogoUrl)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && 
			   affiliate.getAffiliateConfig() != null && 
			   affiliate.getAffiliateConfig().getUiConfiguration() != null &&
			   affiliate.getAffiliateConfig().getUiConfiguration().getBrandingConfiguration() != null &&
			   StringUtils.isNotBlank(affiliate.getAffiliateConfig().getUiConfiguration().getBrandingConfiguration().getLogoUrl())) {
				returnLogoUrl = affiliate.getAffiliateConfig().getUiConfiguration().getBrandingConfiguration().getLogoUrl();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnLogoUrl)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && 
				tenant.getConfiguration() != null && 
				tenant.getConfiguration().getUiConfiguration() != null &&
				tenant.getConfiguration().getUiConfiguration().getBrandingConfiguration() != null &&
				StringUtils.isNotBlank(tenant.getConfiguration().getUiConfiguration().getBrandingConfiguration().getLogoUrl())) {
				returnLogoUrl = tenant.getConfiguration().getUiConfiguration().getBrandingConfiguration().getLogoUrl();
			}
		}
		return returnLogoUrl;
	}

	@Override
	public String baseUrl(Long flowId, Long affiliateId, Long tenantId) {
		String returnBaseUrl = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && StringUtils.isNotBlank(affiliateFlow.getUrl())) {
				returnBaseUrl = affiliateFlow.getUrl();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnBaseUrl)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && StringUtils.isNotBlank(affiliate.getUrl())) {
				returnBaseUrl = affiliate.getUrl();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnBaseUrl)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && StringUtils.isNotBlank(tenant.getUrl())) {
				returnBaseUrl = tenant.getUrl();
			}
		}
		if(StringUtils.isNotBlank(returnBaseUrl)) {
			returnBaseUrl = HTTPS + returnBaseUrl;
		}
		return returnBaseUrl;
	}

	@Override
	public String fromEmailAddress(Long affiliateId, Long tenantId) {
		String returnFromEmailAddress = null;
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && 
			   affiliate.getAffiliateConfig() != null && 
			   affiliate.getAffiliateConfig().getEmailConfiguration() != null &&
			   StringUtils.isNotBlank(affiliate.getAffiliateConfig().getEmailConfiguration().getFromAddress())) {
				returnFromEmailAddress = affiliate.getAffiliateConfig().getEmailConfiguration().getFromAddress();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnFromEmailAddress)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && 
				tenant.getConfiguration() != null && 
				tenant.getConfiguration().getEmailConfiguration() != null &&
				StringUtils.isNotBlank(tenant.getConfiguration().getEmailConfiguration().getFromAddress())) {
				returnFromEmailAddress = tenant.getConfiguration().getEmailConfiguration().getFromAddress();
			}
		}
		return returnFromEmailAddress;
	}

	@Override
	public String postalAddress(Long affiliateId, Long tenantId) {
		String returnPostalAddress = null;
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && 
			   affiliate.getAffiliateConfig() != null && 
			   affiliate.getAffiliateConfig().getEmailConfiguration() != null &&
			   StringUtils.isNotBlank(affiliate.getAffiliateConfig().getEmailConfiguration().getFooterAddress())) {
				returnPostalAddress = affiliate.getAffiliateConfig().getEmailConfiguration().getFooterAddress();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnPostalAddress)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && 
				tenant.getConfiguration() != null && 
				tenant.getConfiguration().getEmailConfiguration() != null &&
				StringUtils.isNotBlank(tenant.getConfiguration().getEmailConfiguration().getFooterAddress())) {
				returnPostalAddress = tenant.getConfiguration().getEmailConfiguration().getFooterAddress();
			}
		}
		return returnPostalAddress;
	}

	@Override
	public String customerCareNum(Long flowId, Long affiliateId, Long tenantId) {
		String returnCustomerCareNum = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && affiliateFlow.getIvrNumber() != null) {
				returnCustomerCareNum = affiliateFlow.getIvrNumber().toString();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnCustomerCareNum)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && StringUtils.isNotBlank(affiliate.getPhone())) {
				returnCustomerCareNum = affiliate.getPhone();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnCustomerCareNum)) {
			GIAppProperties exchangePhone = gIAppConfigRepository.findByPropertyKeyAndTenantId(GLOBAL_EXCHANGE_PHONE, tenantId);
			if(exchangePhone != null) {
				returnCustomerCareNum = exchangePhone.getPropertyValue();
			}
		}
		if(StringUtils.isNotBlank(returnCustomerCareNum)) {
			returnCustomerCareNum = returnCustomerCareNum.replaceAll("\\D+","").replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		}
		return returnCustomerCareNum;
	}
	
	@Override
	public String medicareUrl(Long flowId, Long affiliateId, Long tenantId) {
  		String redirectMedicareUrl = null;
		AffiliateFlow affiliateFlow = null;
		Affiliate affiliate = null;
		TenantDTO tenant = null;
		
		if(flowId != null) {
			affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
		}
		
		if(affiliateId != null) {
			affiliate = affiliateRepository.findOne(affiliateId);
		}
		
		if(tenantId != null) {
			tenant = tenantService.getTenant(tenantId);
		}else{
			tenant = tenantService.getTenant("GINS");
		}

		if(tenant != null && tenant.getConfiguration() != null){
			if(isMedicareProductSelected(tenant.getConfiguration().getProductConfigurations())){
				if(StringUtils.isNotBlank(tenant.getConfiguration().getProductUrl("MDR"))){
					redirectMedicareUrl = tenant.getConfiguration().getProductUrl("MDR");
				}
				if(affiliate != null && affiliate.getAffiliateConfig() != null){
					if(isMedicareProductSelected(affiliate.getAffiliateConfig().getProductConfigurations())){
						if(StringUtils.isNotBlank(affiliate.getAffiliateConfig().getProductUrl("MDR"))){
							redirectMedicareUrl = affiliate.getAffiliateConfig().getProductUrl("MDR");
						}
						if(affiliateFlow != null && affiliateFlow.getAffiliateFlowConfig() != null) {
							if(AffiliateAncillaryStatus.ON.equals(affiliateFlow.getAffiliateMedicare())){
								if(StringUtils.isNotBlank(affiliateFlow.getMedicareRedirectionUrl())){
									redirectMedicareUrl = affiliateFlow.getMedicareRedirectionUrl();
								}
							}else{
								redirectMedicareUrl = null;
							}
						}
					}else{
						redirectMedicareUrl = null;
					}
				}
			}else{
				redirectMedicareUrl = null;
			}
		}
		
		return redirectMedicareUrl;
	}

	private boolean isMedicareProductSelected(List<ProductConfigurationDTO> productConfigurations) {
		if(productConfigurations != null && productConfigurations.size() > 0){
			for(ProductConfigurationDTO productConfigurationDTO: productConfigurations){
				if(productConfigurationDTO != null && productConfigurationDTO.getCode() != null && productConfigurationDTO.getCode().equalsIgnoreCase("MDR")){
					if(productConfigurationDTO != null && productConfigurationDTO.getSelected()){
						return true;
					}else{
						return false;
					}
				}
			}
		}
		return false;
	}

	@Override
	public ExitFlowConfigurationDTO getExitOfferConfigurations(Integer flowId, Long affiliateId, Long tenantId){
		
		ExitFlowConfigurationDTO exitFlowConfigurationDTO = null;
		/**
		 * First try to pull the exitFlowConfigurationDTO for the flowId
		 */
		if (flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId);
			if (affiliateFlow != null) {
				exitFlowConfigurationDTO = affiliateFlow.getAffiliateFlowConfig().getExitFlowConfigurations();
				if(null!=exitFlowConfigurationDTO && exitFlowConfigurationDTO.isEmpty()){
					exitFlowConfigurationDTO = null;
				}
			}
		}  
		/**
		 * If exitFlowConfigurationDTO is still null , try to pull it for the affiliate
		 */
		if (affiliateId != null && exitFlowConfigurationDTO == null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if (affiliate != null && affiliate.getAffiliateConfig() != null) {
				exitFlowConfigurationDTO = affiliate.getAffiliateConfig().getExitFlowConfigurations();
				if(null!=exitFlowConfigurationDTO && exitFlowConfigurationDTO.isEmpty()){
					exitFlowConfigurationDTO = null;
				}
			}
		}  
		/**
		 * If exitFlowConfigurationDTO is still null , try to pull it for the tenant
		 */
		if (tenantId != null && exitFlowConfigurationDTO == null) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if (tenant != null && tenant.getConfiguration() != null) {
				exitFlowConfigurationDTO = tenant.getConfiguration().getExitFlowConfigurations();
			}
		}
		return exitFlowConfigurationDTO;
	}

	@Override
	public EmxFlowConfigurationDTO getEmxConfigurations(Integer flowId){
		
		EmxFlowConfigurationDTO emxFlowConfigurationDTO = null;
		
		if (flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId);
			if (affiliateFlow != null) {
				emxFlowConfigurationDTO = affiliateFlow.getAffiliateFlowConfig().getEmxFlowConfigurations();
			}
		}  
		
		return emxFlowConfigurationDTO;
	}

	/**
	 * The method is used to show the privacy text to the consumer on signup page
	 * @param affiliateFlowId
	 * @return
	 */
	@Override
	public String getPrivacyTextForConsumer(Integer affiliateFlowId) {
		String privacyText = null;
		
		if(affiliateFlowId == null) {
			return privacyText;
		}
		
		try {
			AffiliateFlow flow = affiliateFlowRepository.findOne(affiliateFlowId);
			UIConfigurationDTO uiConfig = flow.getAffiliateFlowConfig().getUiConfiguration();
			privacyText = (uiConfig != null) ? uiConfig.getPrivacyNotice() : privacyText;
		} catch (Exception e) {
			LOGGER.error("Invalid affiliate flow id");
		}
		
		return privacyText;
	}

	/**
	 * This method fetchs the Tenant name or Affiliate Company Name
	 * @since 09th September 2015
	 * Jira Id: https://jira.getinsured.com/browse/HIX-75490
	 * @param affiliateId Long
	 * @param tenantId Long
	 * @return String Tenant/Super Affiliate name
	 */
	@Override
	public String getCompanyName(Long flowId, Long affiliateId, Long tenantId) {
		String custCompanyName = null;
		try
		{
			if(flowId != null && StringUtils.isBlank(custCompanyName))
			{
				AffiliateFlow flow = affiliateFlowRepository.findOne(flowId.intValue());
				if(flow != null && StringUtils.isNotEmpty(flow.getFlowName()))
				{
					custCompanyName = flow.getFlowName();
				}
			}
			if(affiliateId != null && StringUtils.isBlank(custCompanyName))
			{
				Affiliate affiliate = affiliateRepository.findOne(affiliateId);
				if(affiliate != null && StringUtils.isNotEmpty(affiliate.getCompanyName()))
				{
					custCompanyName = affiliate.getCompanyName();
				}
			}
			if(tenantId != null && StringUtils.isBlank(custCompanyName))
			{
				TenantDTO tenant = tenantService.getTenant(tenantId);
				if(tenant != null && StringUtils.isNotEmpty(tenant.getName()))
				{
					custCompanyName = tenant.getName();
				}
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Unable to Fetch customer company Name: "+ex);
		}
		return custCompanyName;
	}
	
	@Override
	public CampaignDTO getCampaignConfigurations(Integer flowId) {
		if (flowId != null) {
			AffiliateFlow flow = affiliateFlowRepository.findOne(flowId);
			if (flow != null && flow.getAffiliateFlowConfig() != null) {
				return flow.getAffiliateFlowConfig().getCampaignConfigurations();
			}
		}

		return null;
	}
	
	@Override
	public String callCenterHours(Long affiliateId, Long tenantId) {
		String callCenterHours = null;
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig() != null && affiliate.getAffiliateConfig().getSupport() != null){
				callCenterHours = affiliate.getAffiliateConfig().getSupport().getCallCenterHours();
			}
		}
		if(tenantId == null){
			tenantId = tenantService.getTenant("GINS").getId();
		}
		if(tenantId != null && StringUtils.isBlank(callCenterHours)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant !=null && tenant.getConfiguration() != null && tenant.getConfiguration().getSupport() != null){
				callCenterHours = tenant.getConfiguration().getSupport().getCallCenterHours();
			}
		}
		if(StringUtils.isBlank(callCenterHours)){
			callCenterHours = TenantTokenHelper.DEFAULT_CALL_CENTER_HOURS;
	    }
		return callCenterHours;
	}
	@Override
	public String getDisclaimerTextForEmailFooter(Long flowId, Long affiliateId, Long tenantId) {
		String returnDisclaimerContent = StringUtils.EMPTY;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && affiliateFlow.getAffiliateFlowConfig() != null && 
					affiliateFlow.getAffiliateFlowConfig().getEmailConfiguration() != null && 
					StringUtils.isNotBlank(affiliateFlow.getAffiliateFlowConfig().getEmailConfiguration().getDisclaimerContent())) {
				returnDisclaimerContent = affiliateFlow.getAffiliateFlowConfig().getEmailConfiguration().getDisclaimerContent();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnDisclaimerContent)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig() != null && 
					affiliate.getAffiliateConfig().getEmailConfiguration() != null && 
					StringUtils.isNotBlank(affiliate.getAffiliateConfig().getEmailConfiguration().getDisclaimerContent())) {
				returnDisclaimerContent = affiliate.getAffiliateConfig().getEmailConfiguration().getDisclaimerContent();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnDisclaimerContent)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && tenant.getConfiguration() != null && 
					tenant.getConfiguration().getEmailConfiguration() != null && 
					StringUtils.isNotBlank(tenant.getConfiguration().getEmailConfiguration().getDisclaimerContent())) {
				returnDisclaimerContent = tenant.getConfiguration().getEmailConfiguration().getDisclaimerContent();
			}
		}
		return GhixUtils.parseForHtmlContent(returnDisclaimerContent);
	}
	
	/**
	 * Get the partner config based on the flow id;
	 */
	@Override
	public PartnerConfigReponseDto getPartnerConfig(String tenantId,String affiliateId, String flowId) {
		PartnerConfigReponseDto responseDto = new PartnerConfigReponseDto();
		
		if(!StringUtils.isEmpty(flowId) && StringUtils.isNumeric(flowId)) {
			try {
				AffiliateFlow flow = affiliateFlowRepository.findOne(Integer.parseInt(flowId));
				if (flow != null) {
					responseDto.setIvrNumber(String.valueOf(flow.getIvrNumber()));
					responseDto.setLogoUrl(flow.getLogoURL());

					// Get the logo redirect and campaign
					if (flow.getAffiliateFlowConfig() != null) {
						
						// get the logo redirect
						if(	flow.getAffiliateFlowConfig().getUiConfiguration() !=null &&
							flow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration() !=null) {
							responseDto.setLogoRedirectUrl(flow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration().getLogoRedirectUrl());
						}
						
						// get the Campaign id
						if(flow.getAffiliateFlowConfig().getCampaignConfigurations() !=null) {
							responseDto.setCampaignId(flow.getAffiliateFlowConfig().getCampaignConfigurations().getCampaignId());
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("Unable to get the flow details");
			}
		}
		
		return responseDto;
	}

	@Override
	public String getCustomerAgreementText(Long affiliateId, Long tenantId) 
	{
		String custAgrmtText = null;
		
		try {
			if(affiliateId != null && StringUtils.isBlank(custAgrmtText)) {
				Affiliate affiliate = affiliateRepository.findOne(affiliateId);
				if(affiliate != null && affiliate.getAffiliateConfig() != null && affiliate.getAffiliateConfig().getUiConfiguration() != null &&
					StringUtils.isNotBlank(affiliate.getAffiliateConfig().getUiConfiguration().getCustomerAgreement())) {
					custAgrmtText = affiliate.getAffiliateConfig().getUiConfiguration().getCustomerAgreement();
				}
			}	
			if(tenantId != null && StringUtils.isBlank(custAgrmtText)) {
				TenantDTO tenant = tenantService.getTenant(tenantId);
				if(tenant != null && tenant.getConfiguration() != null && tenant.getConfiguration().getUiConfiguration() != null &&
					StringUtils.isNotBlank(tenant.getConfiguration().getUiConfiguration().getCustomerAgreement())) {
					custAgrmtText = tenant.getConfiguration().getUiConfiguration().getCustomerAgreement();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Invalid tenant Id or affiliate id");
		}		
		return custAgrmtText;
	}

	@Override
	public SelfServiceRegistrationConfigurationDTO getSelfServiceRegistrationConfiguration(Long affiliateId, Long tenantId) {
		SelfServiceRegistrationConfigurationDTO returnSelfServiceRegistrationConfiguration = new SelfServiceRegistrationConfigurationDTO();
		returnSelfServiceRegistrationConfiguration.setBypassD2CRegistration(SelfServiceRegistrationConfigurationDTO.BypassTypeConfig.OFF);
		returnSelfServiceRegistrationConfiguration.setBypassFFMRegistration(SelfServiceRegistrationConfigurationDTO.BypassTypeConfig.OFF);
		
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig() != null && affiliate.getAffiliateConfig().getSelfServiceRegistrationConfiguration() != null) {
				returnSelfServiceRegistrationConfiguration = affiliate.getAffiliateConfig().getSelfServiceRegistrationConfiguration();
			}
		} else if(tenantId != null) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && tenant.getConfiguration() != null && tenant.getConfiguration().getSelfServiceRegistrationConfiguration() != null) {
				returnSelfServiceRegistrationConfiguration = tenant.getConfiguration().getSelfServiceRegistrationConfiguration();
			}
		}
		return returnSelfServiceRegistrationConfiguration;
	}

	@Override
	public ExitFlowConfigurationDTO getIncompleteEnrollmentExitOfferConfigurations(Integer flowId, Long affiliateId,
			Long tenantId) {
		ExitFlowConfigurationDTO exitFlowConfigurationDTO = null;
		/**
		 * First try to pull the exitFlowConfigurationDTO for the flowId
		 */
		if (flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId);
			if (affiliateFlow != null) {
				exitFlowConfigurationDTO = affiliateFlow.getAffiliateFlowConfig().getIncompleteExitFlowConfigurations();
				if(null!=exitFlowConfigurationDTO && exitFlowConfigurationDTO.isEmpty()){
					exitFlowConfigurationDTO = null;
				}
			}
		}  
		/**
		 * If exitFlowConfigurationDTO is still null , try to pull it for the affiliate
		 */
		if (affiliateId != null && exitFlowConfigurationDTO == null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if (affiliate != null && affiliate.getAffiliateConfig() != null) {
				exitFlowConfigurationDTO = affiliate.getAffiliateConfig().getIncompleteExitFlowConfigurations();
				if(null!=exitFlowConfigurationDTO && exitFlowConfigurationDTO.isEmpty()){
					exitFlowConfigurationDTO = null;
				}
			}
		}  
		/**
		 * If exitFlowConfigurationDTO is still null , try to pull it for the tenant
		 */
		if (tenantId != null && exitFlowConfigurationDTO == null) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if (tenant != null && tenant.getConfiguration() != null) {
				exitFlowConfigurationDTO = tenant.getConfiguration().getIncompleteExitFlowConfigurations();
			}
		}
		return exitFlowConfigurationDTO;	
	}

}
