package gi.mail.service.notification;

import com.getinsured.hix.model.Notice;

import com.getinsured.hix.platform.notification.exception.NotificationException;

public interface NotificationService {
	public void dispatch(Notice notice) throws NotificationException;
}
