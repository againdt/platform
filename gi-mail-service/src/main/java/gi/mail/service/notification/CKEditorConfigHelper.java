package gi.mail.service.notification;
 
import java.util.HashMap;
import java.util.Map;

import com.ckeditor.CKEditorConfig;
 
public final class CKEditorConfigHelper {
	
	private CKEditorConfigHelper() {
		
	}
 
 
	public static CKEditorConfig createConfig() {
		CKEditorConfig config = new CKEditorConfig();
		config.addConfigValue("width","800");
		
		return config;
	}
	
	public static Map<String, String> templateConfig(){
		Map<String, String> templateAttributes = new HashMap<String, String>();
		templateAttributes.put("id", "template");
		
		return templateAttributes;
	}
	
}