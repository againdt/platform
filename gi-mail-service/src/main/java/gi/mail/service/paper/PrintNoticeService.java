package gi.mail.service.paper;

import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeDTO;
import com.getinsured.hix.platform.util.exception.GIException;

public interface PrintNoticeService {
	
	List<Notice> paperNoticeToPrintQueue(Date startDate,Date endDate);
	
	/**
	 * 
	 * @param noticeDTO
	 * @throws GIException
	 */
	void paperNoticeToPrintQueue(NoticeDTO noticeDTO) throws GIException;

}
