package gi.mail.service.paper.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeDTO;
import com.getinsured.hix.model.Notice.IsPrinted;
import com.getinsured.hix.model.Notice.PrintableFlag;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;

import gi.mail.service.paper.PrintManagementService;
import gi.mail.service.paper.PrintNoticeService;

@Service("printNoticeService")
public class PrintNoticeServiceImpl implements PrintNoticeService {

	@Autowired private PrintManagementService printManagementService;
	@Autowired private NoticeRepository noticeRepository;
	@Autowired private ContentManagementService ecmService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrintNoticeServiceImpl.class);
	private static final String DDMMMYYYY = "ddMMMyyyy";
	
	@Override
	public List<Notice> paperNoticeToPrintQueue(Date startDate,Date endDate) {
		
		List<Notice> printNotice = new ArrayList<>();
		String folderName;
		String fileName = null;
		int totalCount = 0;
		int skipped = 0;
		List<Notice> noticeToPrint = noticeRepository.getNoticeByPrintableAndCreated(startDate,endDate, PrintableFlag.Y);
		int allNoticeCount = noticeToPrint.size();
		if(noticeToPrint != null){
			for (Notice notice : noticeToPrint) {
				try {
					/* This try catch written purposefully. If error occurred for only for one notice it should not block other notices */
					if(StringUtils.isBlank(notice.getEcmId())){
						LOGGER.error("ECM Id found null for notice(notice id- "+notice.getId()+"). Skip this notice for print service.");
					}else{
						Content content = ecmService.getContentById(notice.getEcmId());
						folderName = DateUtil.dateToString(notice.getCreated(), DDMMMYYYY);
						fileName = content.getOriginalFileName();
						byte[] data = ecmService.getContentDataById(notice.getEcmId());
						if(data.length == 0){
							skipped++;
							LOGGER.error("No data received from ECM, Original File Name: "+fileName + " for notice id: "+notice.getId()+" Total processed till now "+totalCount+"/"+allNoticeCount+" Skipped till now :"+skipped);
							continue;
						}
						boolean status = printManagementService.postPrintMail(data,
								fileName,folderName);
						if(status){
							LOGGER.info("Notice uploaded to folder ");
							totalCount++;
						}else{
							LOGGER.error("Failed uploading notice(notice id- "+notice.getId()+") into print service folder.");
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error occured. Unable to upload the notice (notice id- "+notice.getId()+" with error:"+e.getMessage()+") Total processed till now "+totalCount+"/"+allNoticeCount+" Skipped till now :"+skipped, e);
					skipped++;
				}
			}
		}
		
		return printNotice;
	}

	@Override
	public void paperNoticeToPrintQueue(NoticeDTO noticeDTO) throws GIException {
		if(noticeDTO == null){
			throw new GIException("Received Null or Empty NoticeDTO");
		}
		
		if(StringUtils.isEmpty(noticeDTO.getEcmDocId())){
			throw new GIException("Received Null or Empty EcmDocId in NoticeDTO");
		}
		String folderName;
		String fileName = null;
		try{
			Content content = ecmService.getContentById(noticeDTO.getEcmDocId());
			folderName = DateUtil.dateToString(noticeDTO.getCreationTimeStamp(), DDMMMYYYY);
			fileName = content.getOriginalFileName();
			byte[] data = ecmService.getContentDataById(noticeDTO.getEcmDocId());
			if(data.length == 0){
				throw new GIException("No data received from ECM, Original File Name: "+fileName + " for notice id: "+noticeDTO.getNoticeId());
			}
			boolean status = printManagementService.postPrintMail(data,
					fileName,folderName);
			if(status){
				/*noticeRepository.updateIsPrintedFlag(IsPrinted.Y, noticeDTO.getNoticeId());*/
				LOGGER.info("Notice uploaded to folder ");	
			}else{
				throw new GIException("Failed uploading notice(notice id- "+noticeDTO.getNoticeId()+") into print service folder.");
			}
		}
		catch(Exception ex){
			throw new GIException(ex);
		}
	}
}
