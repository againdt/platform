package gi.mail.service.paper;


public interface PrintManagementService {


	/**
	 * This method will upload the file with specific configuration.
	 * @param contentDataById
	 * @param title
	 * @return staus true | false
	 */
	boolean postPrintMail(byte[] contentDataById, String title,String floderName);

}
