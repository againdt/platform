package com.getinsured.identity.ws.test;



import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import com.getinsured.identity.ws.client.UserServiceClient;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class UserServiceTest {
	
	static {
	    //for localhost testing only
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){

	        public boolean verify(String hostname,
	                javax.net.ssl.SSLSession sslSession) {
	            if (hostname.equals("localhost")) {
	                return true;
	            }
	            return false;
	        }
	    });
	}
	
	@Autowired
	  private UserServiceClient userServiceClient;

	  @Test
	  public void testCreateUser() {
	    int userId = userServiceClient.createUser();

	    assertThat(userId).isEqualTo(2);
	  }
	  

	  @Test
	  public void testUpdateUser() {
	   try {
		   String response = userServiceClient.updateUser();
		   assertThat(response).isEqualTo("User Updated Successfully");
	   }catch(Exception e) {
		   System.out.println("Exception occured"+ e.getMessage());
	   }
	    
	  } 
}



