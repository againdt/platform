package com.getinsured.identity.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import org.hibernate.annotations.TypeDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The persistent class for the users database table.
 *
 */
@Entity
@Table(name="users")
public class AccountUser implements Serializable,Cloneable{
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountUser.class);
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountUser_Seq")
	@SequenceGenerator(name = "AccountUser_Seq", sequenceName = "users_seq", allocationSize = 1)
	private int id;

	
	@Column(name="username",unique = true,nullable = false)
	private String userName;

	@Column(name="email")
	private String email;

	@Column(name="phone", length=10)
	private String phone;

	@Column(name="recovery",unique = true,nullable = false)
	private String recovery;

	@Column(name="communication_pref")
	private String communicationPref;

	@Column(name="confirmed")
	private int confirmed;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date created;

	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;


	@Column(name="pass_recovery_token" ,length=4000)
	private String passwordRecoveryToken;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pass_recovery_token_expiration")
	private Date passwordRecoveryTokenExpiration;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="lastLogin")
	private Date lastLogin;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="startDate")
	private Date startDate;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="endDate")
	private Date endDate;


	@Column(name="ISSUPERUSER")
	private Integer isSuperUser;

	@Column(name = "first_name")
	private String firstName;

	public String getRecovery() {
		return recovery;
	}

	public void setRecovery(String recovery) {
		this.recovery = recovery;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityQuestion1() {
		return securityQuestion1;
	}

	public void setSecurityQuestion1(String securityQuestion1) {
		this.securityQuestion1 = securityQuestion1;
	}

	public String getSecurityAnswer1() {
		return securityAnswer1;
	}

	public void setSecurityAnswer1(String securityAnswer1) {
		this.securityAnswer1 = securityAnswer1;
	}

	public String getSecurityQuestion2() {
		return securityQuestion2;
	}

	public void setSecurityQuestion2(String securityQuestion2) {
		this.securityQuestion2 = securityQuestion2;
	}

	public String getSecurityAnswer2() {
		return securityAnswer2;
	}

	public void setSecurityAnswer2(String securityAnswer2) {
		this.securityAnswer2 = securityAnswer2;
	}

	@Column(name = "last_name")
	private String lastName;
	
	@Column(name="password" ,length=2000)
	private String password;
	
	@Column(name="security_question_1")
	private String securityQuestion1;

	
	@Column(name="security_answer_1")
	private String securityAnswer1;

	@Column(name="security_question_2")
	private String securityQuestion2;

	@Column(name="security_answer_2")
	private String securityAnswer2;
	
	/*
	 * HIX-42019 : Persists extn_app_userid
	 */
	@Column(name="EXTN_APP_USERID",unique = true)
	private String extnAppUserId;

	/*@Transient ModuleUser activeModule stores users active module details.
	 * 1. When the user has logged in then activeModule will be the ModuleUser record that
	 *    had created while user's post-registration process
	 * 2. When broker is switching from broker role to employer role then activeModule will be
	 *    the ModuleUser record that matches;
	 *       a. moduleId=<switch to module id>
	 *       b. moduleName=<switch to module name> (its EMPLOYER_MODULE)
	 *       c. userId=loggedin user id
	 */

	@Column(name = "TENANT_ID")
	private Long tenantId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	    
	@Column(name="status")
	//@Transient
	private String status;

	@Column(name = "user_npn")
	private String userNPN;

	public AccountUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail(){

		return this.email;
	}

	public void setEmail(String email){
		if(email != null){
			this.email = email.trim().toLowerCase();
		}
	}


	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getIsSuperUser() {
		return isSuperUser;
	}

	public void setIsSuperUser(Integer isSuperUser) {
		this.isSuperUser = isSuperUser;
	}
	
	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountUser other = (AccountUser) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}


}
