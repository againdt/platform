package com.getinsured.identity.ws.client;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.identity.provision.CreateUserRequest;
import com.getinsured.identity.provision.CreateUserResponse;
import com.getinsured.identity.provision.CredentialsType;
import com.getinsured.identity.provision.NameType;
import com.getinsured.identity.provision.ObjectFactory;
import com.getinsured.identity.provision.PasswordType;
import com.getinsured.identity.provision.PhoneType;
import com.getinsured.identity.provision.TaxIdentifierType;
import com.getinsured.identity.provision.UpdateAttributeType;
import com.getinsured.identity.provision.UpdateUserRequest;
import com.getinsured.identity.provision.UpdateUserResponse;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

@Component
public class UserServiceClient {

	@Autowired
	private WebServiceTemplate webServiceTemplate;

	@SuppressWarnings("unchecked")
	public int createUser() {
		try {
			ObjectFactory factory = new ObjectFactory();
			CreateUserRequest request = factory.createCreateUserRequest();

			request.setEmail("vishakatestuser1@yopmail.com");
			request.setUsername("vishakatestuser1@yopmail.com");
			
			TaxIdentifierType tiType = factory.createTaxIdentifierType();
			tiType.setIdentifier("6546546540");
			request.setSsn(tiType);
			
			CredentialsType credType = factory.createCredentialsType();
			PasswordType pwdType = factory.createPasswordType();
			pwdType.setPassword("ghix123#");
			credType.setPassword(pwdType);
			request.setCredentials(credType);

			GregorianCalendar c = new GregorianCalendar();
			c.setTime(new Date());
			XMLGregorianCalendar date2;
			date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    		request.setDateOfBirth(date2);
			
			NameType nameType = factory.createNameType();
			nameType.setFirstname("Vishaka");
			nameType.setLastname("Tharani");
			nameType.setMiddlename("S");
			request.setName(nameType);
			
			PhoneType phoneType = factory.createPhoneType();
			phoneType.setMobilePhone("9999999999");
			request.setPhone(phoneType);
						
			CreateUserResponse response =
					(CreateUserResponse) webServiceTemplate.marshalSendAndReceive(request);
			return response.getUserId();
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return -1;
	}

	@SuppressWarnings("unchecked")
	public String updateUser() {

		ObjectFactory factory = new ObjectFactory();
		UpdateUserRequest request = factory.createUpdateUserRequest();
		request.setId(22081L);
		List<UpdateAttributeType> updateAtttributes = new ArrayList<UpdateAttributeType>();
		
		UpdateAttributeType uAttrFName = factory.createUpdateAttributeType();
		uAttrFName.setAttributeName("firstName");
		uAttrFName.setAttributeValue("Vishaka");
		updateAtttributes.add(uAttrFName);
		
		UpdateAttributeType uAttrLName = factory.createUpdateAttributeType();
		uAttrLName.setAttributeName("lastName");
		uAttrLName.setAttributeValue("Tharani");
		updateAtttributes.add(uAttrLName);
		
		UpdateAttributeType uAttrEmail = factory.createUpdateAttributeType();
		uAttrEmail.setAttributeName("email");
		uAttrEmail.setAttributeValue("vishaka.tharani@yopmail.com");
		updateAtttributes.add(uAttrEmail);
		
		UpdateAttributeType uAttrUserName = factory.createUpdateAttributeType();
		uAttrUserName.setAttributeName("userName");
		uAttrUserName.setAttributeValue("vishaka.tharani@yopmail.com");
		updateAtttributes.add(uAttrUserName);
	
		request.setUpdateAtttribute(updateAtttributes);

		UpdateUserResponse response =
		(UpdateUserResponse) webServiceTemplate.marshalSendAndReceive(request);

		return response.getOut();
	}
}
