package com.getinsured.identity.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.identity.model.AccountUser;
import com.getinsured.identity.provision.CreateUserRequest;
import com.getinsured.identity.provision.CreateUserResponse;
import com.getinsured.identity.provision.UpdateAttributeType;
import com.getinsured.identity.provision.UpdateUserRequest;
import com.getinsured.identity.provision.UpdateUserResponse;
import com.getinsured.identity.repository.IUserRepository;



@Endpoint
public class UserServiceEndpoint {
	
	private final String TARGET_NAMESPACE = "http://mn1dev.ghixqa.com/provision";
	
	@Autowired IUserRepository userRepo;
		
	@PayloadRoot(localPart = "CreateUserRequest", namespace = TARGET_NAMESPACE)
	@WebMethod(operationName = "CreateUser", action = "http://mn1dev.ghixqa.com/provision/createuser")
	@WebResult(name = "CreateUserResponse", targetNamespace = TARGET_NAMESPACE, partName = "parameters")
	public @ResponsePayload CreateUserResponse createUser(@RequestPayload CreateUserRequest createUserRequest) {
		CreateUserResponse response = null;
		
	
			if(createUserRequest != null) {
				AccountUser user = new AccountUser();
				if(createUserRequest.getName() != null) {
					user.setFirstName(createUserRequest.getName().getFirstname());
					user.setLastName(createUserRequest.getName().getLastname());
				}
				
				user.setEmail(createUserRequest.getEmail());
				user.setUserName(createUserRequest.getUsername());
				
				if(createUserRequest.getCredentials() != null && createUserRequest.getCredentials().getPassword() != null) {
					user.setPassword(createUserRequest.getCredentials().getPassword().getPassword());
				}
				
				user.setRecovery("test");
				
				if(createUserRequest.getCredentials() != null) {
					int count = createUserRequest.getCredentials().getSecurityQuestion().size();
		
					if(count > 0) {
						user.setSecurityQuestion1(createUserRequest.getCredentials().getSecurityQuestion().get(0).getQuestion());
						user.setSecurityAnswer1(createUserRequest.getCredentials().getSecurityQuestion().get(0).getAnswer());
					}
		
					if(count > 1) {
						user.setSecurityQuestion2(createUserRequest.getCredentials().getSecurityQuestion().get(1).getQuestion());
						user.setSecurityAnswer2(createUserRequest.getCredentials().getSecurityQuestion().get(1).getAnswer());
					}
				}
			/*	userService = (UserServiceImpl) UserServiceImpl.getApplicationContext().getBean("userService");
	
				if(userService != null) {
					user = userService.createUser(user);
					response = new CreateUserResponse();
					response.setRemoteId("1");
					response.setUserId(user.getId());
				}*/
				
				AccountUser dbUser = userRepo.save(user);
				response = new CreateUserResponse();
				response.setRemoteId("1");
				response.setUserId(dbUser.getId());
			}
			
		return response;
	}

	@PayloadRoot(localPart = "UpdateUserRequest", namespace = TARGET_NAMESPACE)
	@WebMethod(operationName = "UpdateUser", action = "http://mn1dev.ghixqa.com/provision/updateuser")
	@WebResult(name = "UpdateUserResponse", targetNamespace = TARGET_NAMESPACE, partName = "parameters")
	public @ResponsePayload UpdateUserResponse updateUser(@RequestPayload UpdateUserRequest updateUserRequest) {
		UpdateUserResponse response = null;
		if(updateUserRequest != null) {
			Long userId = updateUserRequest.getId();

			if(userId > 0) {
					AccountUser user = userRepo.findOne(userId.intValue());
					List<UpdateAttributeType>  attributes  = updateUserRequest.getUpdateAtttribute();
					for (UpdateAttributeType attribute : attributes) {
						switch(attribute.getAttributeName()) {
						case "firstName":
							user.setFirstName(attribute.getAttributeValue());
							break;
						case "lastName":
							user.setLastName(attribute.getAttributeValue());
							break;
						case "email":
							user.setEmail(attribute.getAttributeValue());
							user.setUserName(attribute.getAttributeValue());
							break;
						case "userName":
							user.setEmail(attribute.getAttributeValue());
							user.setUserName(attribute.getAttributeValue());
							break;
						case "securityQuestion1":
							user.setSecurityQuestion1(attribute.getAttributeValue());
							break;
						case "securityAnswer1":
							user.setSecurityAnswer1(attribute.getAttributeValue());
							break;
						case "securityQuestion2":
							user.setSecurityQuestion2(attribute.getAttributeValue());
							break;
						case "securityAnswer2":
							user.setSecurityAnswer2(attribute.getAttributeValue());
							break;

						}
				}
				userRepo.save(user)	;
				response = new UpdateUserResponse();
				response.setOut("User Updated Successfully");
		}
		}
		return response;
	}
}