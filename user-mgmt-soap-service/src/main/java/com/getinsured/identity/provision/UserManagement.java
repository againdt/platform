
package com.getinsured.identity.provision;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "UserManagement", targetNamespace = "http://mn1dev.ghixqa.com/provision")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface UserManagement {


    /**
     * 
     * @param parameters
     * @return
     *     returns com.getinsured.identity.provision.CreateUserResponse
     * @throws CreateUserFault_Exception
     */
    @WebMethod(operationName = "CreateUser", action = "http://mn1dev.ghixqa.com/provision/createuser")
    @WebResult(name = "CreateUserResponse", targetNamespace = "http://mn1dev.ghixqa.com/provision", partName = "parameters")
    public CreateUserResponse createUser(
        @WebParam(name = "CreateUserRequest", targetNamespace = "http://mn1dev.ghixqa.com/provision", partName = "parameters")
        CreateUserRequest parameters)
        throws CreateUserFault_Exception
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.getinsured.identity.provision.UpdateUserResponse
     * @throws UpdateUserFault_Exception
     */
    @WebMethod(operationName = "UpdateUser", action = "http://mn1dev.ghixqa.com/provision/updateuser")
    @WebResult(name = "UpdateUserResponse", targetNamespace = "http://mn1dev.ghixqa.com/provision", partName = "parameters")
    public UpdateUserResponse updateUser(
        @WebParam(name = "UpdateUserRequest", targetNamespace = "http://mn1dev.ghixqa.com/provision", partName = "parameters")
        UpdateUserRequest parameters)
        throws UpdateUserFault_Exception
    ;

}
