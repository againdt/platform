
package com.getinsured.identity.provision;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "UpdateUserFault", targetNamespace = "http://mn1dev.ghixqa.com/provision")
public class UpdateUserFault_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private UpdateUserFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public UpdateUserFault_Exception(String message, UpdateUserFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public UpdateUserFault_Exception(String message, UpdateUserFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: com.getinsured.identity.provision.UpdateUserFault
     */
    public UpdateUserFault getFaultInfo() {
        return faultInfo;
    }

}
