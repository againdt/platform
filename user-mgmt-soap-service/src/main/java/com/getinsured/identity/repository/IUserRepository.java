package com.getinsured.identity.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.identity.model.AccountUser;

public interface IUserRepository extends JpaRepository<AccountUser, Integer> {
}
