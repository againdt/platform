package com.getinsured.hix.platform.featureflag.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FeatureFlagService implements FeatureFlag {

	enum FeatureProvider { LAUNCHDARKLY };

	@Value("#{configProp['featureflag.type']}")
	private String featureFlagType;

	@Autowired private LaunchDarkly launchDarkly;
	@Autowired private NoFeatureFlag noFeatureFlag;

	private FeatureFlagStrategy featureFlagStrategy;

	@PostConstruct
	public void createDocumentContextInitilizer() {
		if (FeatureProvider.LAUNCHDARKLY.name().equals(featureFlagType)) {
			featureFlagStrategy = launchDarkly;
		}else{
			featureFlagStrategy = noFeatureFlag;
		}
	}

	@Override
	public boolean isEnabled(Object user, Object feature, boolean defaultValue) {
		return featureFlagStrategy.isEnabled(user, feature, defaultValue);
	}

	@Override
	public String readStringValue(Object user, Object feature, String defaultValue) {
		return featureFlagStrategy.readStringValue(user, feature, defaultValue);
	}
}
