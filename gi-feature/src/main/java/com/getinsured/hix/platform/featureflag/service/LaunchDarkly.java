package com.getinsured.hix.platform.featureflag.service;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.featureflag.service.FeatureFlagService.FeatureProvider;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.launchdarkly.client.LDClient;
import com.launchdarkly.client.LDUser;

@Component
class LaunchDarkly implements FeatureFlagStrategy {

	private static final String FLOW_ID = "flowId";

	private static final String AFFILIATE_ID = "affiliateId";

	private static final String LAUNCH_DARKLY_NOT_CLEAN_UP_PROPERLY = "LaunchDarkly not clean up properly";

	private static final String ERROR_LAUNCH_DARKLY_NOT_INITIALIZED = "Error: LaunchDarkly not initialized";

	private static final String ERROR_LAUNCH_DARKLY_NOT_AVAILABLE = "Error: LaunchDarkly not available";

	private static final Logger LOGGER = Logger.getLogger(LaunchDarkly.class);

	private static final String ANONYMOUS_USER = "anonymousUser";
	private static final String TENANT_CODE = "tenantCode";
	private static final String USERNAME = "userName";
	private static final String IP_ADDRESS = "ipAddress";
	private static final String REQUEST_URL = "requestUrl";

	@Value("#{configProp['featureflag.sdk.key']}")
	private String sdkKey;

	@Value("#{configProp['featureflag.type']}")
	private String featureFlagType;

	private LDClient client;

	@PostConstruct
	public void createSession() {
		if (FeatureProvider.LAUNCHDARKLY.name().equals(featureFlagType)){
			try {
				client = new LDClient(sdkKey);
				
				if (client == null) {
					LOGGER.fatal(ERROR_LAUNCH_DARKLY_NOT_AVAILABLE);
				} else if (!client.initialized()){
					LOGGER.fatal(ERROR_LAUNCH_DARKLY_NOT_INITIALIZED);
				}
			} catch (Exception e) {
				LOGGER.fatal(ERROR_LAUNCH_DARKLY_NOT_AVAILABLE, e);
			}
		}

	}

	@PreDestroy
	public void preDestroy() {
		if (client != null){
			client.flush();
			try {
				client.close();
			} catch (IOException ioe) {
				LOGGER.error(LAUNCH_DARKLY_NOT_CLEAN_UP_PROPERLY, ioe);
			}
		}

	}

	@Override
	public boolean isEnabled(Object user, Object feature, boolean defaultValue) {
		
		if (!isServiceActive()){
			createSession();
		}

		if (isServiceActive()) {
			String tenantCode = getTenantCode();
			LDUser ldUser = buildLaunchDarklyUser(user, tenantCode);
	
			return client.boolVariation((String) feature, ldUser, defaultValue);
		} else {
			return defaultValue;
		}
	}

	private boolean isServiceActive() {
		return client != null && client.initialized();
	}

	@Override
	public String readStringValue(Object user, Object feature, String defaultValue) {
		
		if (!isServiceActive()){
			createSession();
		}
		
		if (isServiceActive()) {
			String tenantCode = getTenantCode();
			LDUser ldUser = buildLaunchDarklyUser(user, tenantCode);
			return client.stringVariation((String) feature, ldUser, defaultValue);
			
		} else {
			return defaultValue;
		}
		
	}

	private String getTenantCode() {
		TenantDTO tenant = TenantContextHolder.getTenant();
		return tenant != null ? tenant.getCode() : "";
	}

	private LDUser buildLaunchDarklyUser(Object principal, String tenantCode ) {
		
		LDUser ldUser;
		if (principal != null && principal instanceof AccountUser){
			AccountUser user = (AccountUser) principal;
			ldUser = new LDUser.Builder(user.getEmail())
					.email(user.getEmail())
					.firstName(user.getFirstName())
					.lastName(user.getLastName())
					.custom(TENANT_CODE, tenantCode)
					.custom(AFFILIATE_ID, getAffiliateId())
					.custom(FLOW_ID, getFlowId())
					.custom(IP_ADDRESS, TenantContextHolder.getIPAddress())
					.custom(REQUEST_URL, TenantContextHolder.getRequesturl())
					.custom(USERNAME, user.getUserName())
					.build();
		} else {
			ldUser = new LDUser.Builder(ANONYMOUS_USER)
					.anonymous(true)
					.custom(TENANT_CODE, tenantCode)
					.custom(AFFILIATE_ID, getAffiliateId())
					.custom(FLOW_ID, getFlowId())
					.custom(IP_ADDRESS, TenantContextHolder.getIPAddress())
					.custom(REQUEST_URL, TenantContextHolder.getRequesturl())
					.build();
		}
		return ldUser;
	}

	private int getFlowId() {
		return TenantContextHolder.getFlowId() != null ? TenantContextHolder.getFlowId() : 0;
	}

	private long getAffiliateId() {
		return TenantContextHolder.getAffiliateId() != null ? TenantContextHolder.getAffiliateId() : 0;
	}

}
