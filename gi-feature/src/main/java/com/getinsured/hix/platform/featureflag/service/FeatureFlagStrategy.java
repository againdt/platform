package com.getinsured.hix.platform.featureflag.service;

interface FeatureFlagStrategy {
	
	boolean isEnabled(Object user,  Object feature, boolean defaultValue);

	String readStringValue(Object user, Object feature, String defaultValue);

}
