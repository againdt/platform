package com.getinsured.hix.platform.featureflag.taglib;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.support.SecurityWebApplicationContextUtils;

import com.getinsured.hix.platform.featureflag.FeatureEvaluator;

public class FeatureTag extends TagSupport {
	
	private static final String MISSING_SECURITY_CONTEXT = "SecurityContextHolder did not return a non-null Authentication object, so skipping tag body";
	private static final String MISSING_DEFAULT_VALUE = "defaultValue resolved to null, so including tag body";
	private static final String MISSING_RETURN_TYPE = "returnType resolved to null, so including tag body";
	private static final String MULTIPLE_BEAN_FOUND = "Found incorrect number of %s instances in application context - you must have only have one!";

	private static final long serialVersionUID = 1L;

	protected static final Log LOGGER = LogFactory.getLog(FeatureTag.class);
	
	private ApplicationContext applicationContext;
    private FeatureEvaluator featureEvaluator;
    private String feature = StringUtils.EMPTY;
    private String var;
    private TYPE returnType;
    private String defaultValue;
    
    public enum TYPE {BOOLEAN, STRING};

    
    @Override
	public int doStartTag() throws JspException {
        if ((null == feature) || "".equals(feature)) {
            return skipBody();
        }

        initializeIfRequired();
        
        if (returnType == null){
        	if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(MISSING_RETURN_TYPE);
            }

            // Of course they have access to a null object!
            return evalBody();
        }
        
        if (defaultValue == null){
        	if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(MISSING_DEFAULT_VALUE);
            }

            // Of course they have access to a null object!
            return evalBody();
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(MISSING_SECURITY_CONTEXT);
            }

            return skipBody();
        }
        
        String value = null;
        if (returnType == TYPE.BOOLEAN){
        	boolean defaultValueTmp = Boolean.parseBoolean(defaultValue);
        	Boolean boolValue = featureEvaluator.isFeatureEnabled(authentication, feature, defaultValueTmp);
        	value = Boolean.toString(boolValue);
        } else if (returnType == TYPE.STRING){
        	value = featureEvaluator.readStringValue(authentication, feature, defaultValue);
        }

        if (value == null){
        	return skipBody();
        }

        return evalBody(value);
    }
    
    private int skipBody() {
        return Tag.EVAL_BODY_INCLUDE;
    }
    
    private void initializeIfRequired() throws JspException {
        if (applicationContext != null) {
            return;
        }
        this.applicationContext = getContext(pageContext);
        this.featureEvaluator = getBeanOfType(FeatureEvaluator.class);
    }
    
    protected ApplicationContext getContext(PageContext pageContext) {
        ServletContext servletContext = pageContext.getServletContext();
        return SecurityWebApplicationContextUtils.findRequiredWebApplicationContext(servletContext);
    }
    
    private <T> T getBeanOfType(Class<T> type) throws JspException {
        Map<String, T> map = applicationContext.getBeansOfType(type);

        for (ApplicationContext context = applicationContext.getParent();
            context != null; context = context.getParent()) {
            map.putAll(context.getBeansOfType(type));
        }

        if (map.size() == 0) {
            return null;
        } else if (map.size() == 1) {
            return map.values().iterator().next();
        }

        throw new JspException(String.format(MULTIPLE_BEAN_FOUND, type.getSimpleName()));
    }
    
    private int evalBody(String value) {
        if (var != null) {
        	if (returnType == TYPE.BOOLEAN){
        		Boolean finalValue = Boolean.parseBoolean(value);
        		pageContext.setAttribute(var, finalValue, PageContext.PAGE_SCOPE);
        	} else if (returnType == TYPE.STRING){
        		pageContext.setAttribute(var, value, PageContext.PAGE_SCOPE);
        	}
        }
        return Tag.EVAL_BODY_INCLUDE;
    }
    
    private int evalBody() {
        return Tag.EVAL_BODY_INCLUDE;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public void setVar(String var) {
        this.var = var;
    }

	public void setReturnType(TYPE returnType) {
		this.returnType = returnType;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}
