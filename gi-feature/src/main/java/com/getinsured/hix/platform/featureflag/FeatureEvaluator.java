package com.getinsured.hix.platform.featureflag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.featureflag.service.FeatureFlag;

@Component
public class FeatureEvaluator {

	@Autowired private FeatureFlag featureFlag;

	public boolean isFeatureEnabled(Authentication authentication, Object feature, boolean defaultValue) {

		Object principal = authentication.getPrincipal();
		if (principal != null){
			return featureFlag.isEnabled(principal, feature, defaultValue);
		}

		return defaultValue;
	}


	public String readStringValue(Authentication authentication, Object feature, String defaultValue) {

		Object principal = authentication.getPrincipal();
		if (principal != null){
			return featureFlag.readStringValue(principal, feature, defaultValue);
		}

		return defaultValue;
	}


}
