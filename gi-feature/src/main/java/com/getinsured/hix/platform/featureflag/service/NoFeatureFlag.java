package com.getinsured.hix.platform.featureflag.service;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
class NoFeatureFlag implements FeatureFlagStrategy{
	
	private static final String FEATURE_FLAG_DISABLED = "Feature Flag is disabled, setting NoFeatureFlag as default context";
	
	private static final Logger LOGGER = Logger.getLogger(NoFeatureFlag.class);
	
	@Value("#{configProp['featureflag.type']}")
	private String featureFlagType;

	@PostConstruct
	public void createSession() {
		if (StringUtils.isBlank(featureFlagType)){
			LOGGER.warn(FEATURE_FLAG_DISABLED);
		}
	}
	
	@Override
	public boolean isEnabled(Object user, Object feature, boolean defaultValue) {
		return defaultValue;
	}

	@Override
	public String readStringValue(Object user, Object feature, String defaultValue) {
		return defaultValue;
	}

}
