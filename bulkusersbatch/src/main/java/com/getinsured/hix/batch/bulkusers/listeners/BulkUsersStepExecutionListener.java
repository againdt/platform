package com.getinsured.hix.batch.bulkusers.listeners;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.getinsured.hix.batch.bulkusers.common.MergeCsvSplitFiles;

public class BulkUsersStepExecutionListener implements StepExecutionListener{

	private static final Logger logger = Logger.getLogger(BulkUsersStepExecutionListener.class);
	
	
	private MergeCsvSplitFiles mergeCsvSplitFiles;
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		try {
			mergeCsvSplitFiles.mergeFiles();
		} catch (IOException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
	        String exceptionAsString = sw.toString();
	        logger.error("afertStep() -> Exception Details : "+exceptionAsString);
		}
		logger.info("After Step Execution -> [ Total No. Of All CSV Files Records Count : "+stepExecution.getWriteCount()+" ]");
		System.out.println("After Step Execution -> [ Total No. Of All CSV Files Records Count : "+stepExecution.getWriteCount()+" ]");
		return null;
	}

	public MergeCsvSplitFiles getMergeCsvSplitFiles() {
		return mergeCsvSplitFiles;
	}

	public void setMergeCsvSplitFiles(MergeCsvSplitFiles mergeCsvSplitFiles) {
		this.mergeCsvSplitFiles = mergeCsvSplitFiles;
	}
	
}
