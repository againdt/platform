package com.getinsured.hix.batch.bulkusers.step;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.hix.batch.bulkusers.common.CsvUserStatusWriteIntoFile;
import com.getinsured.hix.batch.bulkusers.config.RestTemplateConfig;
import com.getinsured.identity.provision.BulkUsersResponse;
import com.getinsured.identity.provision.CreateUserRequest;
import com.getinsured.identity.provision.CsvUserStatus;

public class BulkUsersWriter extends CsvUserStatusWriteIntoFile implements ItemWriter<CreateUserRequest> {

	private static final Logger logger = Logger.getLogger(BulkUsersWriter.class);

	private String threadName;
	private RestTemplateConfig restTemplateConfig;
	private String msUsersRestAPIUrl;

	public BulkUsersWriter() {
	}

	@Override
	public void write(List<? extends CreateUserRequest> users) throws Exception {
		boolean statusFlag = false;
		int totalNoOfUsers = (users != null) ? users.size() : 0;
		BulkUsersResponse bulkUsersResponse = null;
		logger.info("Calling ms-usermanagement restAPI. [Total No. Of Users Processing :" + totalNoOfUsers
				+ ". File Name : " + this.inputFileName);
		if (users != null) {
			try {
				
				logger.info("Calling ms-usermanagement create users rest service.");
				
				String restReqFileName = this.inputFileName.substring(this.inputFileName.indexOf("bulkuserdata_split"), this.inputFileName.length());
				ResponseEntity<BulkUsersResponse> responseEntity = restTemplateConfig.restTemplate()
						.postForEntity(msUsersRestAPIUrl+restReqFileName, users, BulkUsersResponse.class);
				
				HttpStatus statusCode = responseEntity.getStatusCode();
				//System.out.println("HttpStatus code : "+statusCode.getReasonPhrase());
				bulkUsersResponse = responseEntity.getBody();
				logger.info("write() START -> Received response. Writing ms-usermanagement service response data into CSV file.");
				statusFlag = writeIntoCsvFile(bulkUsersResponse.getLstCsvUserStatus());
				logger.info(
						"write() END -> Writing ms-usermanagement service response data into CSV file. statusFlag : "
								+ statusFlag);

			} catch (Exception e) {

				List<String> lstRemoteIds = null;
				String exceptionAsString = CommonUtil.convertExceptionToString(e);
				String msg = "";
				List<CsvUserStatus> lstUserStatus = new ArrayList<CsvUserStatus>();
				logger.info("write() -> Exception occured while calling ms-usermanagement create users rest service. ");

				lstRemoteIds = users.stream().map(x -> x.getRemoteId()).collect(Collectors.toList());
				if(e.getMessage() != null)
				{
					msg = e.getMessage().replaceAll("\"", "").replaceAll(",", ";");
					msg = "\"" + msg + "\"";
				}
				for (int i = 0; i < users.size(); i++) {
					lstUserStatus.add(new CsvUserStatus(users.get(i).getRemoteId(), "Failed", "ServerError", msg));
				}

				logger.info("write() START -> Writing CsvUserStatus exception details into CSV file. ");
				statusFlag = writeIntoCsvFile(lstUserStatus);
				logger.info("write() END -> Completed writing CsvUserStatus exception details into CSV file. statusFlag : "+statusFlag);
				logger.error("write() --> Total No. Of Users : " + ((users != null) ? users.size() : 0)
						+ ". \nFailed RemoteIds are : "	+ ((lstRemoteIds != null) ? lstRemoteIds : "RemoteIds are not found.")
						+ "\nException Details : " + exceptionAsString);

			}

		} else {
			logger.info("No data found to call ms-usermanagement restAPI. [Total No. Of Users Processing :"
					+ totalNoOfUsers + ". File Name : " + this.inputFileName + "]");
		}

	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	public String getMsUsersRestAPIUrl() {
		return msUsersRestAPIUrl;
	}

	public void setMsUsersRestAPIUrl(String msUsersRestAPIUrl) {
		this.msUsersRestAPIUrl = msUsersRestAPIUrl;
	}

	public RestTemplateConfig getRestTemplateConfig() {
		return restTemplateConfig;
	}

	public void setRestTemplateConfig(RestTemplateConfig restTemplateConfig) {
		this.restTemplateConfig = restTemplateConfig;
	}
	
}