package com.getinsured.hix.batch.bulkusers.step;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.getinsured.identity.provision.CreateUserRequest;
import com.getinsured.identity.provision.CredentialsType;
import com.getinsured.identity.provision.NameType;
import com.getinsured.identity.provision.PhoneType;
import com.getinsured.identity.provision.RoleType;
import com.getinsured.identity.provision.UserRequest;

public class BulkUsersProcessor implements ItemProcessor<UserRequest, CreateUserRequest> {

	private static final Logger logger = Logger.getLogger(BulkUsersProcessor.class);

	private String threadName;

	@Override
	public CreateUserRequest process(UserRequest user) throws Exception {

		CreateUserRequest newReq = new CreateUserRequest();

		mapCreateUserRequest(user, newReq);
		
		return newReq;
	}

	public void mapCreateUserRequest(UserRequest request, CreateUserRequest user) {

		NameType nameType = new NameType();
		PhoneType phoneType = new PhoneType();
		CredentialsType credType = new CredentialsType();

		nameType.setFirstname(request.getFirst_name());
		nameType.setLastname(request.getLast_name());

		phoneType.setMobilePhone(request.getPhone_number());

		credType.setManaged(true);
		fillRole(request, credType.getRole());

		user.setCredentials(credType);
		user.setEmail(request.getUser_email());
		user.setName(nameType);
		user.setPhone(phoneType);
		user.setRemoteId(request.getRemoteId());
		user.setUsername(request.getUser_name());

	}

	public void fillRole(UserRequest req, List<RoleType> lstRole) {
		if (req.getRole() != null && req.getRole().length() > 0) {
			String arryRole[] = req.getRole().split(":");
			for (String str : arryRole) {
				lstRole.add(new RoleType(str.substring(0, str.indexOf(",")),
						Boolean.valueOf(str.substring(str.indexOf(",") + 1, str.length()))));
			}

		}

	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

}