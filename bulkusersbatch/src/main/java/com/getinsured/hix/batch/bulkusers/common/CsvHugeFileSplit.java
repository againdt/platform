package com.getinsured.hix.batch.bulkusers.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class CsvHugeFileSplit {

	private static final Logger logger = Logger.getLogger(CsvHugeFileSplit.class);

	private String bulkUsersCsvFilePath;
	private String csvSplitFilesPath;
	private String csvHeader;
	private int noOfRecordsInEachFile;

	public void splitIntoCsvFiles() throws IOException {
		
		long totalRecordsCount = CommonUtil.rowsCount(bulkUsersCsvFilePath);
		int totalNoOfFiles = calculateNoOfFiles(totalRecordsCount, noOfRecordsInEachFile);
		
		logger.info("Total No. Of Records : " + (totalRecordsCount - 1));
		logger.info("Total No Of Split CSV Files : " + totalNoOfFiles);
		logger.info("Total No. Of Records Contains In Each File : " + noOfRecordsInEachFile);
		
		BufferedWriter writer[] = createFiles(totalNoOfFiles, csvSplitFilesPath);

		BufferedReader readerHugeData = new BufferedReader(
				new InputStreamReader(new FileInputStream(bulkUsersCsvFilePath)));

		String thisLine = "";
		int count = -1;
		int fileCount = 0;

		while ((thisLine = readerHugeData.readLine()) != null) {
			if (count < noOfRecordsInEachFile) {
				count++;
			} else {
				fileCount = fileCount + 1;
				count = 1;
				writer[fileCount].write(csvHeader + "\n");
			}
			writer[fileCount].write(thisLine + "\n");
			writer[fileCount].flush();
		}
		logger.info("CSV File Splitted Into Small CSV Files.");
	}

	public BufferedWriter[] createFiles(int noOfFiles, String csvSplitFilesPath) throws IOException {

		BufferedWriter writeArray[] = new BufferedWriter[noOfFiles];

		for (int i = 0; i < noOfFiles; i++) {
			writeArray[i] = new BufferedWriter(new FileWriter(new File(csvSplitFilesPath + i + ".csv")));
		}

		return writeArray;
	}

	public int calculateNoOfFiles(long noOfRecords, int eachFileNoOfRecords) {

		int totalNoOfFiles = (int) (noOfRecords / eachFileNoOfRecords);
		if (noOfRecords % eachFileNoOfRecords > 0) {
			totalNoOfFiles = totalNoOfFiles + 1;
		}

		return totalNoOfFiles;
	}

	public String getBulkUsersCsvFilePath() {
		return bulkUsersCsvFilePath;
	}

	public void setBulkUsersCsvFilePath(String bulkUsersCsvFilePath) {
		this.bulkUsersCsvFilePath = bulkUsersCsvFilePath;
	}

	public String getCsvSplitFilesPath() {
		return csvSplitFilesPath;
	}

	public void setCsvSplitFilesPath(String csvSplitFilesPath) {
		this.csvSplitFilesPath = csvSplitFilesPath;
	}

	public String getCsvHeader() {
		return csvHeader;
	}

	public void setCsvHeader(String csvHeader) {
		this.csvHeader = csvHeader;
	}

	public int getNoOfRecordsInEachFile() {
		return noOfRecordsInEachFile;
	}

	public void setNoOfRecordsInEachFile(int noOfRecordsInEachFile) {
		this.noOfRecordsInEachFile = noOfRecordsInEachFile;
	}

}
