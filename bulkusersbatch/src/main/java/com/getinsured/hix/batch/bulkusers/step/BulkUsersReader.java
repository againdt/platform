package com.getinsured.hix.batch.bulkusers.step;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.getinsured.identity.provision.UserRequest;

public class BulkUsersReader{

	private static final Logger logger = Logger.getLogger(BulkUsersReader.class);

	private String inputFileName;

	private ExecutionContext executionContext;

	public BulkUsersReader() {
	}

	public BulkUsersReader(String inputFileName) {
		this.inputFileName = inputFileName;

	}

	public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	public static FlatFileItemReader<UserRequest> reader(String path)
			throws UnexpectedInputException, ParseException, Exception {
		logger.info("Reading Data From File : " + path);

		ResourceLoader resourceLoader = new DefaultResourceLoader();
		FlatFileItemReader<UserRequest> reader = new FlatFileItemReader<UserRequest>();

		Resource resource = resourceLoader.getResource("file:" + path);
		reader.setResource(resource);

		reader.setLineMapper(new DefaultLineMapper<UserRequest>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "first_name", "last_name", "user_name", "user_email", "phone_number",
								"role", "remoteId" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<UserRequest>() {
					{
						setTargetType(UserRequest.class);
					}
				});
			}
		});
		reader.setLinesToSkip(1);

		return reader;
	}

	public ExecutionContext getExecutionContext() {
		return executionContext;
	}

	public void setExecutionContext(ExecutionContext executionContext) {
		this.executionContext = executionContext;
	}

}