package com.getinsured.hix.batch.bulkusers.listeners;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemProcessListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.identity.provision.CreateUserRequest;
import com.getinsured.identity.provision.UserRequest;

public class BulkUsersProcessorListener<T, S> implements ItemProcessListener<UserRequest, CreateUserRequest> {

	private static final Logger logger = Logger.getLogger(BulkUsersProcessorListener.class);

	@Override
	public void beforeProcess(UserRequest item) {
	}

	@Override
	public void afterProcess(UserRequest item, CreateUserRequest result) {
	}

	@Override
	public void onProcessError(UserRequest user, Exception e) {
        String exceptionAsString = CommonUtil.convertExceptionToString(e);
		logger.error("onProcessError() -> ["+user+"] ExceptionDetails : "+exceptionAsString);
	}

}
