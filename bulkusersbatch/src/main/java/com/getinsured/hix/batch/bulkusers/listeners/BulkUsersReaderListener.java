package com.getinsured.hix.batch.bulkusers.listeners;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemReadListener;

import com.getinsured.identity.provision.UserRequest;

public class BulkUsersReaderListener implements ItemReadListener<UserRequest>{

	private static final Logger logger = Logger.getLogger(BulkUsersReaderListener.class);
	
	@Override
	public void beforeRead() {
	}

	@Override
	public void afterRead(UserRequest item) {
	}

	@Override
	public void onReadError(Exception exception) {
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
		logger.error("onReadError() -->  "+exceptionAsString);	
	}

}
