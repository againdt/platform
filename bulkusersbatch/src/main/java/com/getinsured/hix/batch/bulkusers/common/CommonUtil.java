package com.getinsured.hix.batch.bulkusers.common;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;

public class CommonUtil {
	
	private static final Logger logger = Logger.getLogger(CommonUtil.class);
	
	public static long rowsCount(String fileLocation) {
		Path path = Paths.get(fileLocation);
		long lineCount = 0;
		try {
			lineCount = Files.lines(path).count();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lineCount;
	}
	
	public static String dateToString() {

		Date curDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy_hh_mm_ss");
		String dateToStr = format.format(curDate);

		return dateToStr;
	}

	public static ArrayList<String> getListOfFileNames(String directoryName, String fileNameExpr) throws IOException {
		File directory = new File(directoryName);
		Collection<File> files = FileUtils.listFiles(directory,
				new WildcardFileFilter(fileNameExpr), null);
		ArrayList<String> fileNames = (ArrayList<String>) files.stream().map(temp -> temp.getAbsolutePath())
				.collect(Collectors.toList());
		
		return fileNames;

	}
	
	public static String convertExceptionToString(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
        
		return sw.toString();
	}
	
}
