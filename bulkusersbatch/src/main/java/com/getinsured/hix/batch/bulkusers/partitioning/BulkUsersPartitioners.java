package com.getinsured.hix.batch.bulkusers.partitioning;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.hix.batch.bulkusers.common.CsvHugeFileSplit;

public class BulkUsersPartitioners implements Partitioner {

	private static final Logger logger = Logger.getLogger(BulkUsersPartitioners.class);

	private CsvHugeFileSplit csvHugeFileSplit;
	private String csvSplitFilesDirectory;
	private String csvJobsOutputFileNameExpr;
	private String csvSplitFileNameExpr;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		StringWriter exceptionMsgWriter = new StringWriter();
		String exceptionAsString = "";
		
		Map<String, ExecutionContext> partitionData = new HashMap<String, ExecutionContext>();
		try {
			
			logger.info("Calling CsvHugeFileSplit.splitIntoCsvFiles() method.");
			
			csvHugeFileSplit.splitIntoCsvFiles();

			ArrayList<String> fileNames = null;

			fileNames = CommonUtil.getListOfFileNames(csvSplitFilesDirectory, csvSplitFileNameExpr);

			if (fileNames != null && fileNames.size() > 0) {
				for (int i = 0; i < fileNames.size(); i++) {
					
					ExecutionContext executionContext = new ExecutionContext();

					executionContext.putString("inputFileName", fileNames.get(i));
					String jobsOutputFileName = csvJobsOutputFileNameExpr + i + " " + CommonUtil.dateToString();
					executionContext.put("outputFileName", jobsOutputFileName);

					executionContext.putString("threadName", "[Thread - " + i + "]");
					partitionData.put("partition: " + i, executionContext);

					logger.info("Split CSV File Name : " + fileNames.get(i));
				}
			}

		} catch (IOException e) {			
	        exceptionAsString = CommonUtil.convertExceptionToString(e);
			logger.error("Exception Details : "+exceptionAsString);	
		}catch (Exception e) {
	        exceptionAsString = CommonUtil.convertExceptionToString(e);
			logger.error("Exception Details : "+exceptionAsString);	
		}
		
		return partitionData;
	}

	public CsvHugeFileSplit getCsvHugeFileSplit() {
		return csvHugeFileSplit;
	}

	public void setCsvHugeFileSplit(CsvHugeFileSplit csvHugeFileSplit) {
		this.csvHugeFileSplit = csvHugeFileSplit;
	}

	public String getCsvSplitFilesDirectory() {
		return csvSplitFilesDirectory;
	}

	public void setCsvSplitFilesDirectory(String csvSplitFilesDirectory) {
		this.csvSplitFilesDirectory = csvSplitFilesDirectory;
	}

	public String getCsvJobsOutputFileNameExpr() {
		return csvJobsOutputFileNameExpr;
	}

	public void setCsvJobsOutputFileNameExpr(String csvJobsOutputFileNameExpr) {
		this.csvJobsOutputFileNameExpr = csvJobsOutputFileNameExpr;
	}

	public String getCsvSplitFileNameExpr() {
		return csvSplitFileNameExpr;
	}

	public void setCsvSplitFileNameExpr(String csvSplitFileNameExpr) {
		this.csvSplitFileNameExpr = csvSplitFileNameExpr;
	}

}
