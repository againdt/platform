package com.getinsured.hix.batch.bulkusers.listeners;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemWriteListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.identity.provision.CreateUserRequest;

public class BulkUsersWriterListener implements ItemWriteListener<CreateUserRequest> {

	private static final Logger logger = Logger.getLogger(BulkUsersWriterListener.class);

	public BulkUsersWriterListener() {
	}

	@Override
	public void beforeWrite(List<? extends CreateUserRequest> items) {
	}

	@Override
	public void afterWrite(List<? extends CreateUserRequest> items) {
	}

	@Override
	public void onWriteError(Exception exception, List<? extends CreateUserRequest> items) {
		List<String> lstRemoteIds = null;
		
		String exceptionAsString = CommonUtil.convertExceptionToString(exception);

		try {
			if(items != null) {
				lstRemoteIds = items.stream().map(x -> x.getRemoteId()).collect(Collectors.toList());
			}
		} catch (Exception e) {
			logger.error("onWriterError() lstRemoteIds exception : "+CommonUtil.convertExceptionToString(e));			
		}

		logger.error("onWriteError() --> Total No. Of Users : " + ((items != null)? items.size() : 0) + ". \nFailed RemoteIds are : "
				+ ((lstRemoteIds != null) ? lstRemoteIds : "RemoteIds are not found.") + "\nException Details : "
				+ exceptionAsString);
	}

}
