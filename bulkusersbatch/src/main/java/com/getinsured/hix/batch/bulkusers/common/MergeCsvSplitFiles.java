package com.getinsured.hix.batch.bulkusers.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class MergeCsvSplitFiles {

	private static final Logger logger = Logger.getLogger(MergeCsvSplitFiles.class);

	private String csvUserStatusHeader;
	private String consolidatedCsvFile;
	private String csvJobsOutputFileDirectory;
	private String csvJobsOutputFileNameExpr;

	public void mergeFiles() throws IOException {

		String thisLine = "";
		
		ArrayList<String> files = CommonUtil.getListOfFileNames(csvJobsOutputFileDirectory, csvJobsOutputFileNameExpr+ "*.csv");
		logger.info("Started Merging CSV Jobs Output Files.");

		String consolidatedFileName = consolidatedCsvFile + CommonUtil.dateToString() + ".csv";

		logger.info("Final Consolidated CSV File Name  : "+consolidatedFileName);
		
		BufferedWriter consolidatedCsvWriter = new BufferedWriter(new FileWriter(new File(consolidatedFileName)));
		consolidatedCsvWriter.write(csvUserStatusHeader + "\n");

		if (files != null && files.size() > 0) {
			logger.info("Total no. of CSV jobs files are " + files.size());
			for (int i = 0; i < files.size(); i++) {
				BufferedReader readerHugeData = new BufferedReader(
						new InputStreamReader(new FileInputStream(files.get(i))));
				int count = 0;
				logger.info("Reading data from CSV jobs output files and consolidated filename is " + files.get(i));
				 
				while ((thisLine = readerHugeData.readLine()) != null) {
					if (count != 0) {
						consolidatedCsvWriter.write(thisLine + "\n");
					}
					count++;
				}
				consolidatedCsvWriter.flush();
				count = 0;
			}
		} else {
			logger.info("No files are found to merge csv jobs output files.");
		}
		logger.info("Completed merging CSV jobs output files");
		
	}

	public String getCsvUserStatusHeader() {
		return csvUserStatusHeader;
	}

	public void setCsvUserStatusHeader(String csvUserStatusHeader) {
		this.csvUserStatusHeader = csvUserStatusHeader;
	}

	public String getConsolidatedCsvFile() {
		return consolidatedCsvFile;
	}

	public void setConsolidatedCsvFile(String consolidatedCsvFile) {
		this.consolidatedCsvFile = consolidatedCsvFile;
	}

	public String getCsvJobsOutputFileDirectory() {
		return csvJobsOutputFileDirectory;
	}

	public void setCsvJobsOutputFileDirectory(String csvJobsOutputFileDirectory) {
		this.csvJobsOutputFileDirectory = csvJobsOutputFileDirectory;
	}

	public String getCsvJobsOutputFileNameExpr() {
		return csvJobsOutputFileNameExpr;
	}

	public void setCsvJobsOutputFileNameExpr(String csvJobsOutputFileNameExpr) {
		this.csvJobsOutputFileNameExpr = csvJobsOutputFileNameExpr;
	}

}
