package com.getinsured.identity.provision;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(value = { "remoteId", "status", "exceptionType", "errorMessage", "fileName"})
public class CsvUserStatus {

	private String remoteId;
	private String exceptionType;
	private String errorMessage;
	private String status;
	private String fileName;

	public CsvUserStatus() {}
	
	public CsvUserStatus(String remoteId, String status, String exceptionType, String errorMessage) {
		this.remoteId = remoteId;
		this.status = status;
		this.exceptionType = exceptionType;
		this.errorMessage = errorMessage;
		this.fileName = "";
	}

	public String getRemoteId() {
		return remoteId;
	}

	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "CsvUserStatus [remoteId=" + remoteId + ", exceptionType=" + exceptionType + ", errorMessage="
				+ errorMessage + ", status=" + status + ", fileName=" + fileName + "]";
	}
	
}
