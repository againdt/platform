package com.getinsured.identity.provision;

import java.util.List;

public class BulkUsersResponse {
	
	private String code;
	private List<CsvUserStatus> lstCsvUserStatus;
	private List<String> lstSuccessRemoteIds;
	private List<String> lstFailureRemoteIds;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<CsvUserStatus> getLstCsvUserStatus() {
		return lstCsvUserStatus;
	}
	public void setLstCsvUserStatus(List<CsvUserStatus> lstCsvUserStatus) {
		this.lstCsvUserStatus = lstCsvUserStatus;
	}
	public List<String> getLstSuccessRemoteIds() {
		return lstSuccessRemoteIds;
	}
	public void setLstSuccessRemoteIds(List<String> lstSuccessRemoteIds) {
		this.lstSuccessRemoteIds = lstSuccessRemoteIds;
	}
	public List<String> getLstFailureRemoteIds() {
		return lstFailureRemoteIds;
	}
	public void setLstFailureRemoteIds(List<String> lstFailureRemoteIds) {
		this.lstFailureRemoteIds = lstFailureRemoteIds;
	}
	
	public BulkUsersResponse() {}
}
