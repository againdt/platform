package com.getinsured.identity.provision;

import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(value = { "first_name", "last_name", "user_name", "user_email", "phone_number", "role",
		"remoteId"})
public class UserRequest {

	String first_name;
	String last_name;
	String user_name;
	String user_email;
	String phone_number;
	String role;
	String remoteId;
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRemoteId() {
		return remoteId;
	}
	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}
	@Override
	public String toString() {
		return "UserRequest [first_name=" + first_name + ", last_name=" + last_name + ", user_name=" + user_name
				+ ", user_email=" + user_email + ", phone_number=" + phone_number + ", role=" + role + ", remoteId="
				+ remoteId + "]";
	}
	
}
