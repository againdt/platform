package com.getinsured.iex.platform.couchbase.converters;

import java.io.IOException;
import java.util.Date;

import com.getinsured.iex.platform.couchbase.helper.DocumentHelper;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class CouchbaseAdapterFactory implements TypeAdapterFactory {
	@Override
	public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> type) {
		Class<? super T> rawType = type.getRawType();
		if (rawType == byte[].class) {
			return new ByteTypeAdapter<T>();
		} else if (rawType == Date.class) {
			return new DateTypeAdapter<T>();
		}
		return null;
	}

	public class ByteTypeAdapter<T> extends TypeAdapter<T> {
		public void write(JsonWriter out, T value) throws IOException {
			if (value == null) {
				out.nullValue();
				return;
			}
			byte[] source = (byte[]) value;
			out.value(DocumentHelper.encodeBase64String(source));
		}

		@SuppressWarnings("unchecked")
		public T read(JsonReader in) throws IOException {
			final String s = in.nextString();
			if (s == null) {
				return null;
			} else {
				return (T) DocumentHelper.decodeBase64(s);
			}
		}
	}

	public class DateTypeAdapter<T> extends TypeAdapter<T> {
		public void write(JsonWriter out, T value) throws IOException {
			if (value == null) {
				out.nullValue();
				return;
			}
			Date source = (Date) value;
			out.value(source.getTime());
		}

		@SuppressWarnings("unchecked")
		public T read(JsonReader in) throws IOException {
			final long l = in.nextLong();
			return (T) new Date(l);
		}
	}
}
