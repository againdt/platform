package com.getinsured.iex.platform.couchbase.converters;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component("iex_gsonConverter")
public class IEXGsonConverter implements JsonConverter {
	private final Gson gson = 
		new GsonBuilder()
			.registerTypeAdapterFactory(new CouchbaseAdapterFactory())
			.excludeFieldsWithoutExposeAnnotation()
			.create();

	public <T> T fromJson(String source, Class<T> type) {
		return gson.fromJson(source, type);
	}

	public <T> String toJson(T source) {
		return gson.toJson(source);
	}
}