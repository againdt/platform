package com.getinsured.iex.platform.couchbase.converters;
public interface JsonConverter {
	<T> T fromJson(String source, Class<T> type);

	<T> String toJson(T source);
}