package com.getinsured.iex.platform.actuator.config;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;

public class WSO2HealthIndicator extends AbstractHealthIndicator{
	String hubURL;
	
	public WSO2HealthIndicator(String hubURL) {
		this.hubURL = hubURL;
	}
	
	@Override
	protected void doHealthCheck(Builder builder) throws Exception {
		HubConnectivityClient hubClient = new HubConnectivityClient();
		boolean wso2Connected = hubClient.checkWSO2Availability(hubURL);
		
		if(wso2Connected){
			builder.up().build();
		}else{
			builder.down();
		}
	}
}
