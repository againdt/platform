package com.getinsured.iex.platform.actuator.config;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.EndpointAutoConfiguration;
import org.springframework.boot.actuate.endpoint.BeansEndpoint;
import org.springframework.boot.actuate.endpoint.EnvironmentEndpoint;
import org.springframework.boot.actuate.endpoint.HealthEndpoint;
import org.springframework.boot.actuate.endpoint.InfoEndpoint;
import org.springframework.boot.actuate.endpoint.RequestMappingEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter;
import org.springframework.boot.actuate.endpoint.mvc.HealthMvcEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.MvcEndpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpStatus;

/**
 * ActuatorConfig Enables the Spring Actuator endpoints with out Spring Boot's
 * EnableAutoConfiguration. And sets the context to "/actuator" Note : This
 * doesn't uses the application.properties, where all the endpoints are
 * defaulted with the EndpointAutoConfiguration
 *
 * @author Suresh Kancherla
 * @since 1.0
 */
@Configuration
@Import({EndpointAutoConfiguration.class}) // Boot strap with default Config's for the end points.
@ConditionalOnBean(name = "iexHttpClient")
public class HubActuatorConfig
{

  private static final String ERROR_READING_PROPERTIES = "Error reading properties";

  private static final Logger LOGGER = Logger.getLogger(HubActuatorConfig.class);

  @Value("classpath:/build-number.properties")
  private Resource buildInfo;

  @Value("classpath:/depends-on.properties")
  private Resource dependsOnInfo;

  @Autowired
  @Qualifier("configProp")
  Properties properties;

  @Bean
  @Autowired
  // Define the HandlerMapping similar to RequestHandlerMapping to expose the endpoint
  public EndpointHandlerMapping endpointHandlerMapping(Collection<? extends MvcEndpoint> endpoints)
  {
    EndpointHandlerMapping endpointHandlerMapping = new EndpointHandlerMapping(endpoints);
    endpointHandlerMapping.setPrefix("/actuator");
    return endpointHandlerMapping;
  }


  @Bean
  public HubHealthIndicator hubHealthIndicator()
  {
    String hubURL = properties.getProperty("ghixHubIntegrationURL");
    return new HubHealthIndicator(hubURL);
  }

  @Bean
  public WSO2HealthIndicator wso2HealthIndicator()
  {
    String hubURL = properties.getProperty("ghixIdentitySvcUrl");
    return new WSO2HealthIndicator(hubURL);
  }

  @Bean
  @Autowired
  // define the HealthPoint endpoint
  public HealthMvcEndpoint healthMvcEndpoint(HealthEndpoint delegate)
  {
    Map<String, HttpStatus> statusMapping = new HashMap<>();
    statusMapping.put("UP", HttpStatus.OK);
		
		/*boolean hubConnected = HubConnectivityClient.checkHubConnectivity();
		
		if(hubConnected){
			statusMapping.put("HubConnectivity", HttpStatus.OK);
		}else{
			statusMapping.put("HubConnectivity", HttpStatus.SERVICE_UNAVAILABLE);
		}*/

    HealthMvcEndpoint healthMvcEndpoint = new HealthMvcEndpoint(delegate, false);
    healthMvcEndpoint.setStatusMapping(statusMapping);
    return healthMvcEndpoint;
  }

  /*
   * Custom Implementation for injecting the Build Info to /Info End point
   *
   * Note :Currently InfoEndpoint is by default configured to display the
   * git.properties contents or any entries in the properties file entries
   * which starts with 'info.*', With this implemnetation this functionality
   * will be overridden to display only the build-number.properties contents
   * to /info
   */
  @Bean
  @Autowired
  public EndpointMvcAdapter infoMvcEndPoint(InfoEndpoint delegate)
  {
    LinkedHashMap<String, Object> info = new LinkedHashMap<String, Object>();
    // Put the contents of build-numer.properties to /info if build-number.properties file Exists in classpath
    if (this.buildInfo.exists())
    {
      Properties properties;
      try
      {
        properties = PropertiesLoaderUtils.loadProperties(buildInfo);
        for (final String name : properties.stringPropertyNames())
          info.put(name, properties.getProperty(name));

      } catch (IOException e)
      {
        LOGGER.error(ERROR_READING_PROPERTIES);
      }
    }
    InfoEndpoint infoEndpoint = new InfoEndpoint(info);
    return new EndpointMvcAdapter(infoEndpoint);
  }

  @Bean
  @Autowired
  // define the beans endpoint
  public EndpointMvcAdapter beansEndPoint(BeansEndpoint delegate)
  {
    return new EndpointMvcAdapter(delegate);
  }

  @Bean
  @Autowired
  // define the env endpoint
  public EndpointMvcAdapter environmentEndPoint(EnvironmentEndpoint delegate)
  {
    return new EndpointMvcAdapter(delegate);
  }

  @Bean
  @Autowired
  // define the mappings endpoint
  public EndpointMvcAdapter requestMappingEndPoint(RequestMappingEndpoint delegate)
  {
    return new EndpointMvcAdapter(delegate);
  }
}
