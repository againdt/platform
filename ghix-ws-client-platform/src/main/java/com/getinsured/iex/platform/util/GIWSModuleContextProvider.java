package com.getinsured.iex.platform.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class GIWSModuleContextProvider {
	private static Logger logger = LoggerFactory.getLogger(GIWSModuleContextProvider.class);
	private static ApplicationContext moduleContext = null;
	
	public void setModuleContext(ApplicationContext context){
		moduleContext = context;
	}
	
	public static ApplicationContext getModuleContext(){
		return moduleContext;
	}
	
	public static Object getBean(Class<?> clz){
		if(moduleContext == null){
			logger.error("Modue context not available, can not find the bean for "+clz.getName());
			return null;
		}
		Map<String, ?> beans = moduleContext.getBeansOfType(clz);
		if(beans == null || beans.size() == 0){
			return null;
		}
		if(beans.size() > 1){
			throw new RuntimeException("Multiple ocurrences found for type:"+clz.getName()+" Names:"+beans.keySet());
		}
		// We have exactle one item
		return beans.entrySet().iterator().next().getValue();
	}
	
	public static List<?> getAllBeans(Class<?> clzName){
		if(moduleContext == null){
			logger.error("Modue context not available, can not find the bean for "+clzName.getName());
			return null;
		}
		Map<String, ?> beans = moduleContext.getBeansOfType(clzName);
		if(beans == null || beans.size() == 0){
			return null;
		}
		if(beans.size() > 1){
			throw new RuntimeException("Multiple ocurrences found for type:"+clzName.getName()+" Names:"+beans.keySet());
		}
		Iterator<?> cursor = beans.entrySet().iterator();
		List<Object> list = new ArrayList<>();
		while(cursor.hasNext()){
			@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String, Object>) cursor.next();
			list.add(entry.getValue());
		}
		return list;
	}
	
	public static Object getBeanByTypeAndName(Class<?> clzName, String name){
		if(moduleContext == null){
			logger.error("Modue context not available, can not find the bean for "+clzName.getName()+" for Id:"+name);
			return null;
		}
		Map<String, ?> beans = moduleContext.getBeansOfType(clzName);
		if(beans == null || beans.size() == 0){
			return null;
		}
		Object bean = null;
		Iterator<?> cursor = beans.entrySet().iterator();
		while(cursor.hasNext()){
			@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String, Object>) cursor.next();
			if(entry.getKey().equals(name)){
				bean = entry.getValue();
			}
		}
		return bean;
	}
}
