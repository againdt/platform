package com.getinsured.iex.platform.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class GIWSModuleAwareClassLoader {
	private static Set<ClassLoader> availableLoaders = new HashSet<>();
	private static List<String> moduleList = new ArrayList<String>();
	
	static{
		availableLoaders.add(GIWSModuleAwareClassLoader.class.getClassLoader());
		moduleList.add("WS-Client-Platform");
	}
	
	public void registerClassLoader(ClassLoader loader, String moduleName){
		if(availableLoaders.add(loader)){
			moduleList.add(moduleName);
		}
	}
	
	public static synchronized Class<?> forName(String clsName) throws ClassNotFoundException{
		Iterator<ClassLoader> cursor = availableLoaders.iterator();
		Class<?> cls = null;
		while(cursor.hasNext()){
			try{
				cls = (Class<?>) Class.forName(clsName,true,cursor.next());
			}catch(ClassNotFoundException ignored){}
		}
		if(cls == null){
			throw new ClassNotFoundException(clsName+ " Not found with any of the registered modules "+moduleList);
		}
		return cls;
	}
}
