package com.getinsured.iex.platform.actuator.config;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;

public class HubHealthIndicator extends AbstractHealthIndicator{
	String hubURL;
	
	public HubHealthIndicator(String hubURL) {
		this.hubURL = hubURL;
	}
	
	@Override
	protected void doHealthCheck(Builder builder) throws Exception {
		HubConnectivityClient hubClient = new HubConnectivityClient();
		boolean hubConnected = hubClient.checkHubConnectivity(hubURL);
		
		if(hubConnected){
			builder.up().build();
		}else{
			builder.down();
		}
	}
}
