package com.getinsured.iex.platform.actuator.config;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.security.HubSecureHttpClient;

public class HubConnectivityClient {
	private static final Logger LOGGER = Logger.getLogger(HubConnectivityClient.class);
	private CloseableHttpClient httpClient;
	
	public boolean checkHubConnectivity(String hubURL)  {
		HttpEntity requestEntity = null;
		HttpPost httpost = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		boolean bRet = false;
		
		LOGGER.info("HubConnectivityClient : URL : " + hubURL + "checkHubConnectivity");
		
		String payload = "{}";
		try {
			URL url = new URL(hubURL + "checkHubConnectivity");
			if (this.httpClient == null) {
				httpClient = HubSecureHttpClient.getBasicAuthHttpClient();
			}
			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(payload).build();
			httpost = new HttpPost(url.toURI());
			
			httpost.setEntity(requestEntity);
			httpResponse = httpClient.execute(httpost);
			responseEntity = httpResponse.getEntity();
			BufferedInputStream bis = new BufferedInputStream(
					responseEntity.getContent());
			byte[] buf = new byte[bis.available()];
			bis.read(buf);
			//LOGGER.info(operation + httpResponse.getStatusLine());
			String response = new String(buf);
			LOGGER.info("Check hub connectivity response:" + response);
			if(response.indexOf("HS000000") >=0 && response.indexOf("Success") >=0){
				bRet = true;
			}
			
		} catch (IOException | HubServiceException | URISyntaxException e) {
			LOGGER.error("Error in response [" + e.getMessage()+ "]", e);
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		return bRet;
	}
	
	public boolean checkWSO2Availability(String requestUrl)  {
		HttpGet httpget = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		boolean bRet = false;
		
		try {
			URL url = new URL(requestUrl + "checkAvailability");
			LOGGER.info("checkWSO2Availability : requestUrl : " + requestUrl + " url : " +  url.toString() );
			if (this.httpClient == null) {
				httpClient = HubSecureHttpClient.getBasicAuthHttpClient();
			}
			httpget = new HttpGet(url.toURI());
			
			httpResponse = httpClient.execute(httpget);
			responseEntity = httpResponse.getEntity();
			BufferedInputStream bis = new BufferedInputStream(
					responseEntity.getContent());
			byte[] buf = new byte[bis.available()];
			bis.read(buf);
			//LOGGER.info(operation + httpResponse.getStatusLine());
			String response = new String(buf);
			LOGGER.info("Check WSO2 connectivity response:" + response);
			if(response.indexOf("EXCEPTION") == -1){
				bRet = true;
			}
			
		} catch (IOException | HubServiceException | URISyntaxException e) {
			LOGGER.error("Error in response [" + e.getMessage()+ "]", e);
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		return bRet;
	}

}