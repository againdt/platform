package com.getinsured.iex.hub.platform.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.getinsured.iex.hub.platform.AbstractTransformer;
import com.getinsured.iex.hub.platform.ObjectTransformationException;

public class SSACDateTransformer extends AbstractTransformer{

	@Override
	public Object transform(Object obj) throws ObjectTransformationException {
		String date = null;
		String sourceFormat = this.getContextValue("sourceFormat");
		String targetFormat = this.getContextValue("targetFormat");
		if(targetFormat == null){
			throw new ObjectTransformationException("No target format found, can not transform the object");
		}
		try{
			date = (String)obj;
			if(date == null || date.trim().length() == 0){
				return null;
			}
		}catch(ClassCastException e){
			throw new ObjectTransformationException("Transformation failed for "+obj.getClass().getName(),e);
		}
		
		if(date.contains("00:00:00")){
			sourceFormat = sourceFormat.substring(0, 12); //Ignoring the time part from date
			date = date.substring(0, 12); 
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
		sdf.setLenient(false);
		Date dt = null;
		try {
			dt = sdf.parse(date);
			if (!date.equals(sdf.format(dt))){
			  throw new ParseException("Value of date " + date + " does not match source format " + sourceFormat +" or is an invalid date",0);
			}
		} catch (ParseException e) {
			throw new ObjectTransformationException("Failed to transform the date, input ["+date+"] is not in correct format expected ["+sourceFormat+"]",e);
		}
		SimpleDateFormat target = new SimpleDateFormat(targetFormat);
		return target.format(dt);
	}
}
