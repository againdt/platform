package com.getinsured.iex.hub.platform.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.utils.GhixHubConstants;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public final class HubSecureHttpClient {
	
	private HubSecureHttpClient(){
		
	}
	private static CloseableHttpClient httpClient = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(HubSecureHttpClient.class);
	private static final int MAX_CONNECTIONS = 20000;
	private static final int MAX_CONNECTIONS_PER_ROUTE = 1000;
	private static final int MAX_EXECUTIONS = 5;
	
	private static String location = null;
	private static String file = null;
	
	private static HashMap<String, String> aliasMapping = new HashMap<>();
	private static String defaultAlias = null;
	
	private static RequestConfig requestConfig = null;
	private static KeyStore trustStore;
	private static boolean shouldContinue = true;
	private static final long KEY_STORE_REFRESH_INTERVAL = 24*60*60*1000;
	private static String httpBasicAuthUser = GhixHubConstants.HTTP_BASIC_AUTH_USER;
	private static String httpBasicAuthPass = GhixHubConstants.HTTP_BASIC_AUTH_PASS;
	private static String httpBasicAuthHost = GhixHubConstants.HTTP_BASIC_AUTH_HOST;
	private static int httpBasicAuthPort = GhixHubConstants.HTTP_BASIC_AUTH_PORT;
	private static boolean refreshStarted = false;
	private static boolean initiazing;
	private static String password;
	private static CloseableHttpClient basicAuthHttpClient;
	/**
	 * Connection Request Timeout: Time in millis to get the connecion from the connection manager
	 * Connect Timeout: Time in millis to establish the connection
	 * Socket timeout: Max Idle Time in millis allowed between 2 consecutive data packets, or socket will be timeout
	 */
	static{
		trustStore = loadKeyStore();
		requestConfig = RequestConfig.custom().setConnectionRequestTimeout(1000).setConnectTimeout(3000).setSocketTimeout(300000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	}
	
	/**
	 * Module should call this in their @Pre_Destroy if they have obtained the reference to this Client.
	 */
	public void stopRefresh(){
		LOGGER.info("Stopping keystore refresh thread");
		shouldContinue = false;
	}
	
	private static String getCertificateDetails(X509Certificate cert){
		return "Subject:"+cert.getSubjectX500Principal().getName()+"Issues By:"+cert.getIssuerX500Principal().getName()+" Valid through:"+cert.getNotAfter();
	}
	
	public static synchronized CloseableHttpClient getBasicAuthHttpClient()
			throws HubServiceException {
		if(basicAuthHttpClient != null){
			return basicAuthHttpClient;
		}
		if(trustStore == null){
			LOGGER.error("No trust store available, from file:["+file+"] located at ["+location+"], check earlier errors");
			throw new RuntimeException("Failed to initialize secure HttpClient");
		}
		loadAliasMapping();
		LOGGER.info("Initializing Secure HTTP Client using KeyStore:"+GhixHubConstants.HUB_KEY_STORE_FILE+" located at: "+GhixHubConstants.HUB_KEY_STORE_LOCATION);
		loadKeyStore(GhixHubConstants.HUB_KEY_STORE_LOCATION,GhixHubConstants.HUB_KEY_STORE_FILE,GhixHubConstants.HUB_KEYSTORE_PASS);
		PlainConnectionSocketFactory psf = PlainConnectionSocketFactory
				.getSocketFactory();
		
		Registry<ConnectionSocketFactory> registry = null;
		try {
			registry = RegistryBuilder
					.<ConnectionSocketFactory> create().register("https", getSSLConnectionSocketFactory(trustStore,GhixHubConstants.HUB_KEYSTORE_PASS, GhixHubConstants.HUB_KEY_STORE_LOCATION))
					.register("http", psf).build();
		} catch (KeyManagementException | UnrecoverableKeyException
				| NoSuchAlgorithmException | KeyStoreException e) {
			throw new RuntimeException("Failed to initialize secure HttpClient");
		}
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(
				registry);

		cm.setMaxTotal(MAX_CONNECTIONS);
		cm.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
		HttpHost target = new HttpHost(httpBasicAuthHost, httpBasicAuthPort, "https");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(target.getHostName(), target.getPort()),
                new UsernamePasswordCredentials(httpBasicAuthUser, httpBasicAuthPass));
		HttpClientBuilder builder = HttpClients.custom()
				.setConnectionManager(cm)
				.setDefaultCredentialsProvider(credsProvider)
				.setRetryHandler(new GiHttpRequestRetryHandler());
		builder.addInterceptorFirst(new RemoveSoapHeaderInterceptor());
		builder.addInterceptorFirst(new GIHttpResponseInterceptor());
		
		location = GhixHubConstants.HUB_KEY_STORE_LOCATION;
		file = GhixHubConstants.HUB_KEY_STORE_FILE;
		password=GhixHubConstants.HUB_KEYSTORE_PASS;
		refreshKeyStore();
		basicAuthHttpClient= builder.build();
		return basicAuthHttpClient;
		
	}
	
	private static boolean refreshRequired(){
		File keyStoreDir = new File(location);
		File refreshFile = new File(keyStoreDir,"hub_refresh.txt");
		if(refreshFile.exists() && refreshFile.isFile()){
			refreshFile.delete();
			return true;
		}
		return false;
	}
	
	public static void refreshKeyStore(){
		if(refreshStarted){
			return;
		}
		refreshStarted = true;
		Thread refreshThread = new Thread(new Runnable(){
			@Override
			public void run() {
				while(shouldContinue){
					try {
						if(!refreshRequired()){
							Thread.sleep(KEY_STORE_REFRESH_INTERVAL);
						}
						KeyStore store = loadKeyStore();
						if(store != null){
						Enumeration<String> aliases = store.aliases();
						String alias = null;
						X509Certificate tmp = null;
						X509Certificate existing = null;
						while (aliases.hasMoreElements()) {
							alias = aliases.nextElement();
								LOGGER.info("Checking store alias:" + alias);
							tmp = (X509Certificate) trustStore.getCertificate(alias);
							existing = (X509Certificate) store.getCertificate(alias);
							if(existing == null){
									if(LOGGER.isInfoEnabled()){
								LOGGER.info("Found new certificate for "+alias+"::"+getCertificateDetails(tmp));
									}
								trustStore.setCertificateEntry(alias, tmp);
							}else{
								// We have an existing certificate with this alias
								if(!tmp.equals(existing)){
									// The one we have is not the same as this one
										if(LOGGER.isInfoEnabled()){
									LOGGER.info("******* Overriding the existing certificate ***********");
									LOGGER.info("Existing Certificate:"+getCertificateDetails(existing));
									LOGGER.info("Now replaced with "+getCertificateDetails(tmp));
									LOGGER.info("********************************************************");
										}
									trustStore.setCertificateEntry(alias, tmp);
								}
							}
						}			
						}
					} catch (KeyStoreException e) {
						LOGGER.error("Unexpected Exception reading key store", e);
					}catch (InterruptedException e) {
						// Ignore;
					}
				}
				LOGGER.info("Received signal, Exiting keystore refresh");
				refreshStarted = false;
			}
		}, "HUB Keystore Refresh");
		refreshThread.start();
	}
	
	private static void loadAliasMapping(){
		String ghixHome = System.getProperty("GHIX_HOME");
		if(ghixHome == null){
			throw new RuntimeException("Expected GHIX_HOME property to be available to load the alias mapping, found none");
		}
		String aliasMappingFile = ghixHome+File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+"sslConf.xml";
		LOGGER.info("Loading alias mapping from file:"+aliasMappingFile);
		Document doc = null;
		InputStream is = null;
		try {
			DocumentBuilderFactory factory = PlatformServiceUtil.getDocumentBuilderFactoryInstance();
			is = new FileInputStream(aliasMappingFile);
			doc = factory.newDocumentBuilder().parse(is);
			buildAliasMap(doc);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			LOGGER.error("Failed to load the attribute map", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
	}
	
	private static void buildAliasMap(Document doc) {
		NodeList aliasMaps = doc.getElementsByTagName("aliasMap");
		Node aliasMapNode = null;
		String alias = null;
		String server = null;
		String tmpVal = null;
		Node tmp = null;
		NamedNodeMap attributesList = null;
		int len = aliasMaps.getLength();
		for(int i = 0; i < len; i++){
			aliasMapNode = aliasMaps.item(i);
			attributesList = aliasMapNode.getAttributes();
			tmp = attributesList.getNamedItem("alias");
			if(tmp != null){
				alias = tmp.getNodeValue();
			}else{
				LOGGER.error("No alias found for alias entry, skipping");
				continue;
			}
			tmp = attributesList.getNamedItem("server");
			if(tmp != null){
				server = tmp.getNodeValue();
			}else{
				LOGGER.error("No server entry found for alias:"+alias+", skipping");
				continue;
			}
			tmp = attributesList.getNamedItem("default");
			if(tmp != null){
				tmpVal = tmp.getNodeValue();
				if(Boolean.valueOf(tmpVal)){// This alias is set to default
					if(defaultAlias != null && !alias.equals(defaultAlias)){
						throw new RuntimeException("Alias:"+defaultAlias+" is already set to be a default alias, \""+alias+"\" can not be set to be default alias, please provide only one");
					}
					defaultAlias  = alias;
					LOGGER.info("Marking \""+alias+"\" entry as default alias");
				}
			}
			
			if(alias.length() > 0 && server.length() > 0){
				aliasMapping.put(server, alias);
				LOGGER.info("Added alias:"+alias+" for server:"+server);
			}else{
				LOGGER.error("Invalid alias mapping entry:[alias="+alias+" -> server="+server+"], Ignoring");
			}
		}
	}

	private static SSLConnectionSocketFactory getSSLConnectionSocketFactory(final KeyStore trustStore, String password, final String location) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException{
		SSLConnectionSocketFactory sslsf = null;
		SSLContext sslcontext = SSLContexts
				.custom()
				.loadTrustMaterial(trustStore, new TrustStrategy() {

					public boolean isTrusted(X509Certificate[] chain,
							String authType) throws CertificateException {
						StringBuffer certsReceived = new StringBuffer();
						String connectionServerName = null;
						for (int i = 0; i < chain.length; i++) {
							connectionServerName = chain[i].getIssuerX500Principal().getName();
							certsReceived.append("["+connectionServerName+"] ,");
							if (isCertificateTrusted(trustStore, chain[i])) {
								LOGGER.info("Allowing connection from: " + connectionServerName);
								return true;
							}
						}
						LOGGER.warn("Not allowing SSL Connection from "
								+ connectionServerName
								+ " make sure you have imported the server certificate in the keystore ["
								+ location + "] used, received certificates "+certsReceived);
						return false;
					}
				})
				.loadKeyMaterial(trustStore, password.toCharArray(),new PrivateKeyStrategy() {
						@Override
						public String chooseAlias(Map<String, PrivateKeyDetails> aliases,
								Socket socket) {
							return chooseAliasFromKeyStore(aliases,socket);
						}
					})
				.build();
				sslsf = new SSLConnectionSocketFactory(
						sslcontext, split("Protocols",System.getProperty("https.protocols")),
			            split("Supported Ciphers",System.getProperty("https.cipherSuites")),
						new DefaultHostnameVerifier());
		return sslsf;
	}
	
	private static String[] split(String property,String propertyVal) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Evaluating property:"+property+" For value:"+propertyVal);
		}
		if(propertyVal != null && propertyVal.length() > 0){
			return propertyVal.split(" *, *");
		}
		return null;
	}
	
	private static String chooseAliasFromKeyStore(Map<String, PrivateKeyDetails> aliases,
			Socket socket) {
		String server = socket.getInetAddress().getHostName();
		String alias = aliasMapping.get(server);
		if(alias == null){
			if(defaultAlias == null){
				throw new RuntimeException("No alias found for server:"+server+", and No default alias provided, Expected one of the registered servers in sslConf.xml::"+aliasMapping);
			}
			LOGGER.info("No explicit alias provided for server:"+server+" using default alias:"+defaultAlias);
			return defaultAlias;
		}
		LOGGER.info("Using key entry with alias:"+alias+" for server "+server);
		return alias;
	}
	
	private static boolean isCertificateTrusted(KeyStore store,
			X509Certificate certificate) {
		try {
			Enumeration<String> aliases = store.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				if(LOGGER.isTraceEnabled()){
					LOGGER.debug("Checking store alias:" + alias);
				}
				if (store.getCertificate(alias).equals(certificate)) {
					return true;
				}
			}
		} catch (KeyStoreException e) {
			LOGGER.error("Unexpected Exception reading key store", e);
			return false;
		}
		return true;
	}

	private static File getKeyStoreFile(String location,String file, String password){
		File keyStoreDir = new File(location);
		File keyStore = new File(keyStoreDir,file);
		if(!keyStore.exists() || !keyStore.isFile()){
			throw new RuntimeException("Key store ["+keyStore.getAbsolutePath()+"] does not exist Or its not a file");
		}
		return keyStore;
	}
	
	/**
	 * Checks if configured aliases have a coressponding private key available in the supplied keystore
	 * @param trustStore
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws UnrecoverableEntryException
	 */
	private static void validateKeyStore(KeyStore trustStore) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException{
		Collection<String> requiredAliases = aliasMapping.values();
		Enumeration<String> aliases = trustStore.aliases();
		String tmp = null;
		while(aliases.hasMoreElements()){
			tmp = aliases.nextElement();
			if(requiredAliases.contains(tmp)){
				
				//Try loading the key
				KeyStore.ProtectionParameter protParam =
			        new KeyStore.PasswordProtection(GhixHubConstants.HUB_KEYSTORE_PASS.toCharArray());
				// get private key
				KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry)
					trustStore.getEntry(tmp, protParam);
				PrivateKey aliasPrivateKey = pkEntry.getPrivateKey();
				if(aliasPrivateKey == null){
					LOGGER.error("Alias:"+tmp+" is expected to have a coressponding private key entry, ....... false");
					throw new RuntimeException("No private key entry found for alias:"+tmp);
				}
				LOGGER.info("Alias:"+tmp+" is expected to have a coressponding private key entry, ....... true");
			}
		}
		// finally check if Keystore has all the aliases we have configured
		for(String requiredOne:requiredAliases){
			if(!trustStore.containsAlias(requiredOne)){
				LOGGER.info("Alias:"+requiredOne+" is expected to have a coressponding private key entry, ....... false, Ignoring");
			}
		}
		
	}
	
	private static KeyStore loadKeyStore(){
		KeyStore trustStore = null;
		InputStream instream = null;
		try {
			File keyStore = getKeyStoreFile(GhixHubConstants.HUB_KEY_STORE_LOCATION, GhixHubConstants.HUB_KEY_STORE_FILE,GhixHubConstants.HUB_KEYSTORE_PASS);
			if(keyStore != null){
				instream = new FileInputStream(keyStore);
			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(instream, GhixHubConstants.HUB_KEYSTORE_PASS.toCharArray());
			LOGGER.info("Done creating trust store...");
			validateKeyStore(trustStore);
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to initialize the http client", e);
		} finally {
			try {
				if (instream != null) {
					instream.close();
				}
			} catch (IOException e) {
				LOGGER.error(e.getMessage(),e);
			}
		}
		return trustStore;
	}
	
	private static KeyStore loadKeyStore(String keyStoreLocation, String keyStoreFile, String keyStorePass) {
		KeyStore trustStore = null;
		InputStream instream = null;
		try {
			File keyStore = getKeyStoreFile(keyStoreLocation, keyStoreFile,keyStorePass);
			if(keyStore != null){
				instream = new FileInputStream(keyStore);
			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(instream, GhixHubConstants.HUB_KEYSTORE_PASS.toCharArray());
			LOGGER.info("Done creating trust store...");
			validateKeyStore(trustStore);
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to initialize the http client", e);
		} finally {
			try {
				if (instream != null) {
					instream.close();
				}
			} catch (IOException e) {
				LOGGER.error(e.getMessage(),e);
			}
		}
		return trustStore;
	}
	
	public static synchronized CloseableHttpClient getHttpClient() {
		while(initiazing){
			try {
				Thread.sleep(1000);;
			} catch (InterruptedException e) {
				//ignored
			}
		}
		if(httpClient != null){
			LOGGER.info("Returning existing Http Client, not initializing it again, earlier initiazed from file:["+file+"] located at ["+location+"]");
			return httpClient;
		}
		if(trustStore == null){
			LOGGER.error("No trust store available, from file:["+file+"] located at ["+location+"], check earlier errors");
			throw new RuntimeException("Failed to initialize secure HttpClient");
		}
		initiazing = true;
		loadAliasMapping();
		LOGGER.info("Initializing Secure HTTP Client using KeyStore:"+GhixHubConstants.HUB_KEY_STORE_FILE+" located at: "+GhixHubConstants.HUB_KEY_STORE_LOCATION);
		loadKeyStore(GhixHubConstants.HUB_KEY_STORE_LOCATION,GhixHubConstants.HUB_KEY_STORE_FILE,GhixHubConstants.HUB_KEYSTORE_PASS);
		PlainConnectionSocketFactory psf = PlainConnectionSocketFactory
				.getSocketFactory();
		
		Registry<ConnectionSocketFactory> registry = null;
		try {
			registry = RegistryBuilder
					.<ConnectionSocketFactory> create().register("https", getSSLConnectionSocketFactory(trustStore,GhixHubConstants.HUB_KEYSTORE_PASS, GhixHubConstants.HUB_KEY_STORE_LOCATION))
					.register("http", psf).build();
		} catch (KeyManagementException | UnrecoverableKeyException
				| NoSuchAlgorithmException | KeyStoreException e) {
			throw new RuntimeException("Failed to initialize secure HttpClient");
		}
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(
				registry);

		cm.setMaxTotal(MAX_CONNECTIONS);
		cm.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
		HttpClientBuilder builder = HttpClients.custom()
				.setConnectionManager(cm)
				.setDefaultRequestConfig(requestConfig)
				.setRetryHandler(new GiHttpRequestRetryHandler())
				.addInterceptorFirst(new RemoveSoapHeaderInterceptor())
				.addInterceptorFirst(new GIHttpResponseInterceptor());
		httpClient= builder.build();
		location = GhixHubConstants.HUB_KEY_STORE_LOCATION;
		file = GhixHubConstants.HUB_KEY_STORE_FILE;
		password=GhixHubConstants.HUB_KEYSTORE_PASS;
		initiazing = false;
		refreshKeyStore();
		return httpClient;
	}
	
	public static synchronized CloseableHttpClient getHttpClient(String keyStoreLocation, String keyStoreFile, String keyStorePass) {
		while(initiazing){
			try {
				Thread.sleep(1000);;
			} catch (InterruptedException e) {
				//ignored
			}
		}
		if(httpClient != null){
			LOGGER.info("Returning existing Http Client, not initializing it again, earlier initiazed from file:["+file+"] located at ["+location+"]");
			return httpClient;
		}
		if(trustStore == null){
			LOGGER.error("No trust store available, from file:["+file+"] located at ["+location+"], check earlier errors");
			throw new RuntimeException("Failed to initialize secure HttpClient");
		}
		initiazing = true;
		loadAliasMapping();
		LOGGER.info("Initializing Secure HTTP Client using KeyStore:"+keyStoreFile+" located at: "+keyStoreLocation);
		loadKeyStore(keyStoreLocation,keyStoreFile,keyStorePass);
		PlainConnectionSocketFactory psf = PlainConnectionSocketFactory
				.getSocketFactory();
		
		Registry<ConnectionSocketFactory> registry = null;
		try {
			registry = RegistryBuilder
					.<ConnectionSocketFactory> create().register("https", getSSLConnectionSocketFactory(trustStore, keyStorePass,keyStoreLocation))
					.register("http", psf).build();
		} catch (KeyManagementException | UnrecoverableKeyException
				| NoSuchAlgorithmException | KeyStoreException e) {
			throw new RuntimeException("Failed to initialize secure HttpClient");
		}
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(
				registry);

		cm.setMaxTotal(MAX_CONNECTIONS);
		cm.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
		HttpClientBuilder builder = HttpClients.custom()
				.setConnectionManager(cm)
				.setDefaultRequestConfig(requestConfig)
				.setRetryHandler(new GiHttpRequestRetryHandler())
				.addInterceptorFirst(new RemoveSoapHeaderInterceptor())
				.addInterceptorFirst(new GIHttpResponseInterceptor());
		httpClient= builder.build();
		location = keyStoreLocation;
		file = keyStoreFile;
		password=keyStorePass;
		initiazing = false;
		return httpClient;
	}
	
	private static class GiHttpRequestRetryHandler implements HttpRequestRetryHandler {
		public boolean retryRequest(IOException exception, int executionCount,
				HttpContext context) {
			HttpHost targetHost;
			HttpClientContext clientContext = HttpClientContext.adapt(context);
			targetHost = clientContext.getTargetHost();
			String exceptionMessage = "";
			if(exception != null){
				exceptionMessage = exception.getMessage();
			}
			boolean shouldRetry = false;
			if (executionCount >= MAX_EXECUTIONS) {
				LOGGER.error("Maximum retries reached, not able to connect to ["+targetHost.getHostName()+"], No retrying");
				return shouldRetry;
			}
			if (exception instanceof HttpHostConnectException) {
				LOGGER.error("Connect Failed with:"+exceptionMessage+", aborting request to ["+targetHost.getHostName()+"]");
				shouldRetry = false;
			}
			if (exception instanceof InterruptedIOException) {
				LOGGER.error("Connection Timeout with:"+exceptionMessage+", aborting request to ["+targetHost.getHostName()+"]");
				shouldRetry = true;
			}
			if (exception instanceof UnknownHostException) {
				LOGGER.error("Request failed with:"+exceptionMessage+", aborting connect to ["+targetHost.getHostName()+"]");
				shouldRetry = false;
			}
			if (exception instanceof ConnectTimeoutException) {
				LOGGER.error("Connection refused by the server or server is busy ["+targetHost.getHostName()+"] failed with:"+exceptionMessage+",");
				shouldRetry = true;
			}
			if (exception instanceof org.apache.http.NoHttpResponseException) {
	            LOGGER.warn("No response from server on " + executionCount + " call");
	            return true;
	        }
			if (exception instanceof SSLException) {
				LOGGER.error("SSL handshake failed, aborting connection to server ["+targetHost.getHostName()+"] Error:["+exception.getMessage()+"]");
				shouldRetry = false;
			}
			HttpRequest request = clientContext.getRequest();
			boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
			if (idempotent && shouldRetry) {
				// Retry if the request is considered idempotent and there is a valid reason to retry
				shouldRetry = true;
			}else{
				shouldRetry = false;
			}
			LOGGER.error("Retrying connection to "+targetHost.getHostName()+" attempt # ["+executionCount+"]");
			return shouldRetry;
		}
	}
	
	/**
	 * Following methods used for unit testing purposes only.
	 * @param args
	 * @throws Exception
	 */
	
	
	
	public static String getGETData(final String url) throws Exception {

		HttpGet httpGet = null;
		String httpResponse = null;
		Exception ex = null;
		try {
			httpGet = new HttpGet(url);
			httpGet.addHeader("Host", "localhost:4443");
			httpResponse = getHttpClient().execute(httpGet,getResponseHandler());
		} catch (IOException e) {
			if(httpGet != null){
				httpGet.abort();
				httpGet.releaseConnection();
			}
			ex = e;
		} 
		if(ex != null){
			throw ex;
		}
		return httpResponse;
	}
	
	public static ResponseHandler<String> getResponseHandler(){
		
		ResponseHandler<String> handler = new ResponseHandler<String>() {

            public String handleResponse(
                    final HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                	//LOGGER.error("Request Failed with status "+response.getStatusLine());
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }

        };
        return handler;
	}
}
