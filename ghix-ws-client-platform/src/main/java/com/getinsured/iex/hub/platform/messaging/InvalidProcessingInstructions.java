package com.getinsured.iex.hub.platform.messaging;

public class InvalidProcessingInstructions extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidProcessingInstructions() {
	}

	public InvalidProcessingInstructions(String message) {
		super(message);
	}

	public InvalidProcessingInstructions(Throwable cause) {
		super(cause);
	}

	public InvalidProcessingInstructions(String message, Throwable cause) {
		super(message, cause);
	}
}
