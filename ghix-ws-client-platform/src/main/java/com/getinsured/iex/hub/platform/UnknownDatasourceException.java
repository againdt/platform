package com.getinsured.iex.hub.platform;

public class UnknownDatasourceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnknownDatasourceException() {
		super();
	}

	public UnknownDatasourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownDatasourceException(String message) {
		super(message);
	}

	public UnknownDatasourceException(Throwable cause) {
		super(cause);
	}

}
