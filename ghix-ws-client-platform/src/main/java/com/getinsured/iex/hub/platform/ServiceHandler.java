package com.getinsured.iex.hub.platform;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.ws.client.core.WebServiceMessageCallback;

public interface ServiceHandler {
	public static final String RESPONSE_HANDLER = "RESPONSE_HANDLER";
	public static final String CASE_NUMBER = "CASE_NUMBER";
	public static final String CASE_POC_FULL_NAME = "CasePOCFullName";
	public static final String CASE_POC_PHONE_NUMBER = "CasePOCPhoneNumber";
	public static final String SERVICE_ERROR = "SERVICE_ERROR";
	
	Object getRequestpayLoad() throws HubServiceException, HubMappingException;
	
	String handleResponse(Object responseObject) throws HubServiceException;
	
	String handleError(Map<String, Object> context) throws HubServiceException;

	void setGlobalTransformer(Transformer transformer);

	void setParameterMap(
			List<ParameterMap> parameterMapList);

	@SuppressWarnings("rawtypes")
	void setNamespaceSet(Set<Class> namespaceSet);

	void handleInputParameter(String name, Object value) throws HubServiceException, HubMappingException;
	
	Object getPayLoadFromJSONInput(String name, String jsonStr) throws HubServiceException, HubMappingException;
	
	void setContext(Map<String, Object> context);
	
	Map<String, Object> getContext();
	
	/*
	 * Map to hold specific key-value pairs.
	 * This context will be used during post processing of
	 * saving hub Response 
	 */
	Map<String, Object> getResponseContext();
	
	void setJsonInput(String jsonInput);

	String getJsonInput();

	void setServiceSchedule(List<Schedule> schedule);

	boolean isServiceAvailable() throws ServiceUnAvailableException;
	
	String getResponseCode();

	void setResponseCode(String responseCode);
	
	RequestStatus getRequestStatus();
	boolean isFtiAware() ;
	
	void setFtiAware(boolean ftiAware);
	
	boolean isPiiAware();
	
	void setPiiAware(boolean piiFlag);
	
	List<Schedule> getServiceDownTimeSchedule();

	WebServiceMessageCallback getMessageCallback();
	String getServiceIdentifier();

	Marshaller getServiceMarshaller();

	Unmarshaller getServiceUnMarshaller();

}
