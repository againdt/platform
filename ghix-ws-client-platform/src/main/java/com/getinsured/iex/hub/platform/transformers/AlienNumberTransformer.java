package com.getinsured.iex.hub.platform.transformers;

import com.getinsured.iex.hub.platform.AbstractTransformer;
import com.getinsured.iex.hub.platform.ObjectTransformationException;

public class AlienNumberTransformer extends AbstractTransformer{

	@Override
	public Object transform(Object obj) throws ObjectTransformationException {
		Integer alienNumber = null;
		String targetFormat = this.getContextValue("targetFormat");
		if(targetFormat == null){
			throw new ObjectTransformationException("No target format found, can not transform the object");
		}
		try{
			alienNumber = Integer.valueOf((String)obj);
			if(alienNumber == null ){
				return null;
			}
			
		}catch(ClassCastException|NumberFormatException e){
			throw new ObjectTransformationException("Transformation failed for "+obj.getClass().getName(),e);
		}
		
		return String.format(targetFormat, alienNumber);
	}
	
	public static void main(String[] args) throws ObjectTransformationException{
		AlienNumberTransformer ssnconv = new AlienNumberTransformer();
		ssnconv.setContextValue("targetFormat", "%09d");
		String x = (String) ssnconv.transform("78945");
		System.out.println(x);
	}
}
