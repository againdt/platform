package com.getinsured.iex.hub.platform.messaging;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public abstract class PostProcessor{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PostProcessor.class);
	
	public static final int INVOKE_RULES = 0;
	public static final int PERSIST_RESPONSE = 1;
	public static final String INPUT_KEY = "input";
	public static final String RESPONSE_KEY = "response";
	public static final String RULE_NAME_KEY = "RULE_NAME";
	private static final String POST_PROCESS_EXCHANGE = "post_process";
	public static final String PERSISTENCE_ROUTING_KEY = "persist";
	public static final String RULES_ROUTING_KEY = "rules";
	
	//Rules
	public static final String DEATH_VERIFICATION_RULE = "death";
	public static final String SSN_VERIFICATION_RULE = "ssn";
	public static final String INCARCERATION_VERIFICATION_RULE = "incarceration";
	
	
	//@Autowired
	private RabbitTemplate mqTarget;
	
	protected void initiatePostProcessing(int processIdentifier, Object context) throws InvalidProcessingInstructions{
		
		LOGGER.debug("Process Identifier " + processIdentifier);
		
		switch(processIdentifier){
			case INVOKE_RULES:
				processRulesEngineInvocation(context);
				break;
			
			case PERSIST_RESPONSE:
				processResponsePersistence(context);
				break;
			
			default:
				throw new InvalidProcessingInstructions("Process Identifier ["+processIdentifier+"] not recognized, Expecting "+INVOKE_RULES+" for Rules Engine and "+PERSIST_RESPONSE+" for persisting the response");
			}
		
		}

	private void processResponsePersistence(Object context) throws InvalidProcessingInstructions {
		this.mqTarget.convertAndSend(POST_PROCESS_EXCHANGE,PERSISTENCE_ROUTING_KEY,context);
	}

	@SuppressWarnings("unchecked")
	private void processRulesEngineInvocation(Object context) throws InvalidProcessingInstructions {
		try{
			Map<String, Object> ctx = (HashMap<String, Object>)context;
			if(ctx.get(INPUT_KEY) == null){
				throw new InvalidProcessingInstructions("\"input\" Parameter is mandatory for rules engine processing");
			}
			if(ctx.get(RESPONSE_KEY) == null){
				throw new InvalidProcessingInstructions("\"response\" Parameter is mandatory for Rules engine processing");
			}
			if(ctx.get(RULE_NAME_KEY) == null){
				throw new InvalidProcessingInstructions("Rule name is mandatory for processing the rule");
			}
		}catch(ClassCastException ce){
			throw new InvalidProcessingInstructions("Can not initiate post processing, expecting a context map",ce);
		}
		this.mqTarget.convertAndSend(POST_PROCESS_EXCHANGE,RULES_ROUTING_KEY,context);
	}
}