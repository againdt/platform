package com.getinsured.iex.hub.platform;

public class InputDataValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InputDataValidationException() {
	}

	public InputDataValidationException(String message) {
		super(message);
	}

	public InputDataValidationException(Throwable cause) {
		super(cause);
	}

	public InputDataValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}
