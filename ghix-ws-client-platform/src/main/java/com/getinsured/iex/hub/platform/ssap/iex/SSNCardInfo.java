package com.getinsured.iex.hub.platform.ssap.iex;

import org.json.simple.JSONObject;

public class SSNCardInfo {
	private String socialSecurityNumber = null;
	private boolean nameSameOnSSNCardIndicator;
	private String firstNameOnSSNCard;
	private String middleNameOnSSNCard;
	private String lastNameOnSSNCard;
	
	public SSNCardInfo(JSONObject ssnInfo){
		Object tmp = null;
		if(ssnInfo != null){
			 this.socialSecurityNumber = (String)ssnInfo.get("socialSecurityNumber");
			 tmp = ssnInfo.get("nameSameOnSSNCardIndicator");
			 if(tmp != null){
				 this.nameSameOnSSNCardIndicator = (Boolean)tmp;
			 }
			 
			 if(!this.nameSameOnSSNCardIndicator){
				 tmp = ssnInfo.get("firstNameOnSSNCard");
				 if(tmp != null){
					 this.firstNameOnSSNCard = (String)tmp;
				 }
				 tmp = ssnInfo.get("middleNameOnSSNCard");
				 if(tmp != null){
					 this.middleNameOnSSNCard = (String)tmp;
				 }
				 tmp = ssnInfo.get("lastNameOnSSNCard");
				 if(tmp != null){
					 this.lastNameOnSSNCard = (String)tmp;
				 }
			 }
		}
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public boolean isNameSameOnSSNCardIndicator() {
		return nameSameOnSSNCardIndicator;
	}

	public String getFirstNameOnSSNCard() {
		return firstNameOnSSNCard;
	}

	public String getMiddleNameOnSSNCard() {
		return middleNameOnSSNCard;
	}

	public String getLastNameOnSSNCard() {
		return lastNameOnSSNCard;
	}
}
