package com.getinsured.iex.hub.platform.ssap.iex;

import org.json.simple.JSONObject;

/**
 * SSAP model class for Citizenship Info
 * 
 * @author Nikhil Talreja
 * @since 15-Apr-2014
 *
 */
public class CitizenshipInfo {
	
	private Boolean citizenshipAsAttestedIndicator;
	
	public CitizenshipInfo(JSONObject citizenshipInfo){
		
		Object tmp = null;
		if(citizenshipInfo != null){
			tmp = citizenshipInfo.get("citizenshipAsAttestedIndicator");
			 if(tmp instanceof Boolean){
				 this.citizenshipAsAttestedIndicator = (Boolean)tmp;
			 }
		}
		
	}
	
	public Boolean isCitizenshipAsAttestedIndicator() {
		return citizenshipAsAttestedIndicator;
	}
	
}
