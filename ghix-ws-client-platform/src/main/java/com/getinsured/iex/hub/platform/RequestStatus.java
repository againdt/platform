package com.getinsured.iex.hub.platform;

public enum RequestStatus {
	
	PENDING("PENDING"), 														// Request Execution is in process, awaiting response
	RETRY("RETRY"),																// Request failed and should be retried 
	RETRY_BATCH("RETRY BATCH"),													// Request Should be tried in batch mode
	NETWORK_ERROR("NETWORK ERROR"),									// Request failed because of a network error
	SERVICE_TEMPORARILY_NOT_AVAILABLE("SERVICE TEMP DOWN"), 		// Execution not attempted as service is not available
	SERVICE_NOT_AVAILABLE("SERVICE UNAVAILABLE"), 							    // Execution failed as service is not available 
	SUCCESS("SUCCESS"), FAILED("FAILED"), SYSTEM_ERROR("SYETEM_ERROR"), SERVICE_INVOCATION_ERROR("SERVICE INVOKE ERROR"),			// Failed or Success
	PENDING_CLOSE("PENDING_CLOSE"), //Status to indicate that this case needs to be closed,
	
	/*Step 2 and 3 Request life cycle*/
	ADDL_VERIFY_FAILED("ADDL_VERIFY_FAILED"), 
	ADDL_VERIFY_INITIATED("ADDL_VERIFY_INITIATED"),
	ADDL_VERIFY_ACKNOLEDGED("ADDL_VERIFY_ACK"),
	ADDL_VERIFY_RESPONSE_AVAILABLE("ADDL_VERIFY_RESP_AVAILABLE"),
	ADDL_VERIFY_RESPONSE_RETRIEVED("ADDL_VERIFY_RESP_RETRIEVED"),
	THIRD_VERIFY_REQUIRED("THIRD_VERIFY_REQUIRED"),
	THIRD_VERIFY_INITIATED("THIRD_VERIFY_INITIATED"),
	THIRD_VERIFY_FAILED("THIRD_VERIFY_FAILED"),
	THIRD_VERIFY_ACKNOLEDGED("THIRD_VERIFY_ACK"),
	THIRD_VERIFY_RESPONSE_AVAILABLE("THIRD_VERIFY_RESP_AVAILABLE"),
	THIRD_VERIFY_RESPONSE_RETRIEVED("THIRD_VERIFY_RESP_RETRIEVED"),
	PENDING_ADDL_VERIFY_CLOSE("PENDING_ADDL_VERIFY_CLOSE"), //Status to indicate that ADDL_VERIFY is pending to be closed,
	PENDING_THIRD_VERIFY_CLOSE("PENDING_THIRD_VERIFY_CLOSE"), //Status to indicate that THIRD_VERIFY is pending to be closed
	CASE_CLOSED("CASE_CLOSED"), //Status to indicate that this case has been closed
	HUB_ERROR_RESPONSE("HUB ERROR RESPONSE"), DB_NOT_AVAILABLE("DB NOT AVAILABLE"), UNDER_MAINT("UNDER MAINTENANCE"),
	SERVICE_NOT_REACHABLE("SERVICE_NOT_REACHABLE"), 
	RESUBMIT_SAVIS("RESUBMIT_SAVIS"), 
	RE_VERIFY("RE-VERIFY"), 
	INIT_VERIFY_COMPLETE("INIT_VERIFY_COMPLETE"), 
	REVERIFY_COMPLETE("RE-VERIFY_COMPLETE"), 
	RESUBMIT_COMPLETE("RE-SUBMIT_COMPLETE"), ADDL_VERIFY_RESPONSE_RETRIEVED_FAILED("ADDL_RESPONSE_RETRIEVE_FAILED"), CASE_CLOSED_FAILED("CLOSE_CASE_FAILED"); //Used when close case service fails for record more than 3 times
	
	private String statusCode;

	RequestStatus(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
