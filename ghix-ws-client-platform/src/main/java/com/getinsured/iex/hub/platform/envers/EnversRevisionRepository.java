package com.getinsured.iex.hub.platform.envers;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.history.Revision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.history.RevisionRepository;



/**
 * Convenience interface to allow pulling in {@link JpaRepository} and {@link RevisionRepository} functionality in one
 * go.
 */
@NoRepositoryBean
public interface EnversRevisionRepository<T, ID extends Serializable, N extends Number & Comparable<N>> extends
		RevisionRepository<T, ID, N>, JpaRepository<T, ID> {
	
	Revision<N, T> findRevisionByDate(ID id, Date date, String dateFieldName);
	
	Revision<N, T> findRevisionByEvent(ID id, Integer intValue, String intFieldName);
	
	Integer findLastRaceRevision(Integer enrolleeID, Integer revisionID);
	
	List<Object> findRacesCreatedForEnrollee(Integer enrolleeID, Integer revisionID);
	
	Integer findLastRelationRevision(Integer sourceID, Integer targetID, Integer revisionID);
	
	Revision<N, T> findLastSubscriberChangeRevision(ID id, Integer revValue);
	
	Revision<N, T> findLastLocationRevision(ID id, Integer revValue);
}
