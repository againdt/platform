package com.getinsured.iex.hub.platform;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Validator {
	
	private int maxLen = -1;
	private int minLen = -1;
	private String regEx = null;
	private boolean isRequired = false;
	private boolean isNumeric = false;
	private String[] allowedValues = null;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Validator.class);
	
	public void addContextParameter(String name, Object value) {

		if (name != null && value != null) {
			
			String val = (String) value;
			setParams(name, val);
			
		}
	}
	
	private void setParams(String name, String val){
		
		switch (name) {

			case "isRequired":
				this.isRequired = val.equalsIgnoreCase("Y");
				break;
			case "isNumeric":
				this.isNumeric = val.equalsIgnoreCase("Y");
				break;
			case "regex":
				this.regEx = val;
				break;
			case "minLength":
				setMinLength(val);
				break;
			case "maxLength":
				setMaxLength(val);
				break;
			case "allowedValues": this.allowedValues = val.split(",");break;
			default: break;	
			
		}
		
	}
	
	private void setMinLength(String val){
		
		try {
			this.minLen = Integer.parseInt(val);
		} catch (NumberFormatException ne) {
			LOGGER.warn("Invalid value for minLength :" + val);
		}
		
	}
	
	private void setMaxLength(String val){
		
		try {
			this.maxLen = Integer.parseInt(val);
		} catch (NumberFormatException ne) {
			LOGGER.warn("Invalid value for maxLength :" + val);
		}
		
	}
	
	public void checkForAllowedvalues(String name, Object value) throws InputDataValidationException{
		String tmp;
		if(this.checkForNullOrEmpty(name, value)){
			LOGGER.info("Skipping Allowed Values validation, for "+name+" since it is null or empty");
			return;
		}
		if(this.allowedValues != null && this.allowedValues.length > 0){
			
			if(!(value instanceof String)){
				throw new InputDataValidationException("Value provided for "+name+" is not compatible type for allowed values type validation");
			}
			tmp = (String)value;
			if(Arrays.asList(this.allowedValues).contains(tmp)){
				return;
			}
			
			//If this line is reached, validation has failed
			throw new InputDataValidationException("Value provided for "+name+" expected to be one of "+Arrays.toString(this.allowedValues));
			
		}
	}
	
	public void checkForRegularExpression(String name, Object value) throws InputDataValidationException{
		String tmp;
		if(this.checkForNullOrEmpty(name, value)){
			LOGGER.info("Skipping Regex validation, for "+name+" as it is null or empty");
			return;
		}
		if(this.regEx != null){
			// This has to be a String
			if(!(value instanceof String)){
				throw new InputDataValidationException("Regular Expression validation can not be applied for "+value.getClass().getName());
			}
			tmp = (String)value;
			Pattern p = Pattern.compile(this.regEx);
			Matcher m = p.matcher(tmp);
			if(!m.matches()){
				throw new InputDataValidationException(" Value provided for "+name+" doesn't match the expression "+this.regEx);
			}
		}
	}
	
	public void checkForMinAndMaxLen(String name, Object value) throws InputDataValidationException{
		String tmp;
		if(this.checkForNullOrEmpty(name, value)){
			LOGGER.info("Skipping Length validation, for "+name+" since it is null or empty");
			return;
		}
		
		//Applicable for String values only
		if(!(value instanceof String)){
			throw new InputDataValidationException(name+" should be a String value for length validation");
		}
		
		tmp = (String)value;
		
		checkLength(tmp,name);
	}
	
	private void checkLength(String tmp, String name) throws InputDataValidationException{
		
		if(this.minLen > 0 && (tmp.length() < this.minLen)){
			throw new InputDataValidationException(name+" should be a within the length range of ["+this.minLen+"-"+this.maxLen+"]");
		}
		if(this.maxLen > 0 && (tmp.length() > this.maxLen)){
			throw new InputDataValidationException(name+" should be within the length range of ["+this.minLen+"-"+this.maxLen+"]");
		}
	}
	
	protected boolean checkForNullOrEmpty(String name, Object value) throws InputDataValidationException{
		
		if(value == null){
			if(this.isRequired){
				throw new InputDataValidationException(name+" is a required field");
			}
			return true;
		}
		else if(!(value instanceof String)){
			return true;
		}
		else {
			return checkForEmpty(value, name);
		}
		
	}
	
	private boolean checkForEmpty(Object value, String name) throws InputDataValidationException{
		
		if(value instanceof String){
			String tmp = ((String)value).trim();
			if(this.isRequired && tmp.length() == 0){
				throw new InputDataValidationException(name+" is a required field, received empty");
			}
			else{
				if(tmp.isEmpty()){
					checkLength(tmp, name);
					return true;
				}
				
			}
		}
		return false;
		
	}
	
	public int getMaxLen() {
		return maxLen;
	}

	public int getMinLen() {
		return minLen;
	}

	public String getRegEx() {
		return regEx;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public boolean isNumeric() {
		return isNumeric;
	}

	public String[] getAllowedValues() {
		
		if (allowedValues == null) {
            return new String[0];
		}
		else{
			return (String[])allowedValues.clone();
		}
		
	}

	protected abstract void validate(String name, Object value)  throws InputDataValidationException;
}
