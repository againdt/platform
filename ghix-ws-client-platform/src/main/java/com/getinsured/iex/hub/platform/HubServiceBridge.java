package com.getinsured.iex.hub.platform;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.getinsured.iex.hub.platform.utils.GhixHubConstants;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.platform.util.GIWSModuleAwareClassLoader;



public final class HubServiceBridge {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HubServiceBridge.class);
	private static final String SERVICE_DEFINITION_DIR = GhixHubConstants.HUB_SERVICE_DEFINITION_DIR.trim();
	
	private static Map<String, HandlerEnvironment> serviceHandlers  = new HashMap<String, HandlerEnvironment>();
	private String serviceName;
	private ServiceHandler servicehandler = null;
	private Map<String, Object> context = null;
	private static List<ClassLoader> availableClassLoaders = null;
	
	private static final String RAW_TYPES = "rawtypes";
	
	static {
		availableClassLoaders = new ArrayList<ClassLoader>();
		availableClassLoaders.add(HubServiceBridge.class.getClassLoader());
	}
	
	private HubServiceBridge(){
	}
	
	public static void registerClassLoader(ClassLoader moduleClassLoader){
		if(moduleClassLoader != null && !availableClassLoaders.contains(moduleClassLoader)){
			availableClassLoaders.add(moduleClassLoader);
		}
	}
	public void processInputParameter(String name, Object value) throws HubMappingException{
		try{
			if(value == null){
				LOGGER.warn("No value provided for "+name+" skipping processing");
				return;
			}
			if(!(value instanceof String)){
				throw new HubMappingException(name+" does not support type "+value.getClass().getName());
			}
			this.servicehandler.handleInputParameter(name, (String)value);
		}
		catch(Exception e){
			throw new HubMappingException(e.getMessage(), e);
		}
	}
	
	public String handleResponse(Object responseObject) throws HubServiceException{
		return this.servicehandler.handleResponse(responseObject);
	}
	
	public void addContextParameter(String name, Object value){
		if(this.context == null){
			this.context = new HashMap<String, Object>();
			this.servicehandler.setContext(context);
		}
		this.context.put(name, value);
	}
	
	private static InputStream validateServiceDefintionDirectory(String serviceName) throws  BridgeException{
		String errMsg = "None"; 
		try{
			if(SERVICE_DEFINITION_DIR == null){
				errMsg = "Service definition Directory["+SERVICE_DEFINITION_DIR+"] not provided";
				LOGGER.error(errMsg);
				throw new BridgeException(errMsg);
			}
			File dir = new File(SERVICE_DEFINITION_DIR.trim());
			if(!dir.exists() || !dir.isDirectory()){
				errMsg = "Directory location ["+SERVICE_DEFINITION_DIR+"] not found";
				LOGGER.error(errMsg);
				throw new IOException(errMsg);
			}
			return new FileInputStream(new File(dir,serviceName+".xml"));

		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			throw new BridgeException("Failed to create the service bridge", e);
		}
		
	}
	
	@SuppressWarnings(RAW_TYPES)
	public static HubServiceBridge getHubServiceBridge(String serviceName) throws  BridgeException{
		
		HandlerEnvironment handlerEnv = serviceHandlers.get(serviceName);
		if(handlerEnv != null && handlerEnv.isStale()){
			handlerEnv = null;
		}
		HubServiceBridge bridge = new HubServiceBridge();
		InputStream metadaStream = null;
		try{
			if(handlerEnv != null){
				bridge.setServiceHandler(handlerEnv.getServiceHandler());
			}
			else{
				metadaStream = validateServiceDefintionDirectory(serviceName);
				DocumentBuilderFactory factory = PlatformServiceUtil.getDocumentBuilderFactoryInstance();
				factory.setNamespaceAware(true);
				DocumentBuilder docBuilder = factory.newDocumentBuilder();
				Document doc = docBuilder.parse(metadaStream);
				String rootName = doc.getDocumentElement().getNodeName();
				LOGGER.info("Processing :"+rootName+" for "+serviceName);
				bridge.setServiceName(serviceName);
				Class<ServiceHandler> handlerClass = loadServiceHandler(doc);
				String refreshFrequency = loadRefreshTime(doc);
				boolean ftiAware = loadFtiFlag(doc);
				boolean piiFlag = loadPiiFlag(doc);
				Transformer globalTransformer = loadGlobalTransformer(doc);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("PII Flag:"+piiFlag+" FTI Flag:"+ftiAware);
				}
				Set<Class> namespaceSet = new HashSet<Class>();
				List<ParameterMap> parameterMapList = new ArrayList<ParameterMap>();
				loadParametersMapAndNamespace(doc, namespaceSet, parameterMapList);
				List<Schedule> schedule = loadServiceSchedule(doc);
				LOGGER.info("Loaded:"+namespaceSet.size()+" Namespaces and "+parameterMapList.size()+" mappings");
				handlerEnv = new HandlerEnvironment();
				handlerEnv.setRefreshTime(refreshFrequency);
				handlerEnv.setHandlerClass(handlerClass);
				handlerEnv.setNamespaceSet(namespaceSet);
				handlerEnv.setParameterMapList(parameterMapList);
				handlerEnv.setFtiAware(ftiAware);
				handlerEnv.setPiiFlag(piiFlag);
				handlerEnv.setScheduleInfo(schedule);
				handlerEnv.setGlobalTransformer(globalTransformer);
				serviceHandlers.put(serviceName, handlerEnv);
				ServiceHandler handler = handlerEnv.getServiceHandler();
				bridge.setServiceHandler(handler);
				
			}
		}catch(Exception e){
			throw new BridgeException("Failed to create the service bridge", e);
		}finally{
			IOUtils.closeQuietly(metadaStream);
		}
		return bridge;
	}

	/**
	 * <schedule_list>
	 * 	<schedule>
	 * 		<down_time_start>Mar 23, 2014 13:10:00</down_time_start>
	 * 		<down_time_end>Mar 23, 2014 15:00:00</down_time_end>
	 * 	</schedule>
	 * 	<schedule>
	 * 		<down_time_start>Mar 24, 2014 13:10:00</down_time_start>
	 * 		<down_time_end>Mar 24, 2014 15:10:00</down_time_end>
	 * 	</schedule>
	 * </schedule_list>
	 * @param doc
	 * @return
	 * @throws HubMappingException
	 */
	private static List<Schedule> loadServiceSchedule(Document doc) throws ParseException, HubMappingException {
		List<Schedule> scheduleList = new ArrayList<Schedule>();
		
			NodeList tmpNodeList = doc.getElementsByTagName("schedule_list");
			int len = tmpNodeList.getLength();
			if(len > 1){
				throw new HubMappingException("Only 1 \"schedule_list\" element expected found:"+len);
			}
			Node tmp = null;
			Node scheduleNode = null;
			for(int i = 0; i < len; i ++){
				tmp = tmpNodeList.item(i);
			}
			if(tmp != null){
				tmpNodeList = tmp.getChildNodes();
				len = tmpNodeList.getLength();
				for(int i = 0; i < len; i++){
					scheduleNode = tmpNodeList.item(i);
					if(scheduleNode.getNodeType() != Node.ELEMENT_NODE){
						continue;
					}
					if(scheduleNode.getNodeName().equalsIgnoreCase("schedule")){
						scheduleList.add(getSchedule(scheduleNode));
					}
				}
			}
		
		return scheduleList;
	}

	private static Schedule getSchedule(Node scheduleNode) throws ParseException, HubMappingException {
		Schedule sch = new Schedule();
		NodeList scheduleParamList = scheduleNode.getChildNodes();
		Node scheduleAttr = null;
		String attrName;
		int len = scheduleParamList.getLength();
		for(int i = 0; i < len; i++){
			scheduleAttr = scheduleParamList.item(i);
			if(scheduleAttr.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			attrName = scheduleAttr.getNodeName();
			if(attrName.equalsIgnoreCase("down_time_start")){
				sch.setServiceDownStartTime(scheduleAttr.getTextContent());
			}else if(attrName.equalsIgnoreCase("down_time_end")){
				sch.setServiceDownEndTime(scheduleAttr.getTextContent());
			}else{
				throw new HubMappingException("Schedule Arrtibute:\""+attrName+"\" not supported");
			}
		}
		LOGGER.info("Loaded Schedule:"+sch);
		return sch;
	}

	private static Class<ServiceHandler> loadServiceHandler(Document doc) throws HubMappingException {
		NodeList handlerNodes = doc.getElementsByTagName("handler");
		Node handler = null;
		Node tmp;
		for(int i = 0; i < handlerNodes.getLength();i++){
			tmp = handlerNodes.item(0);
			if (tmp.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			handler = tmp;
		}
		if(handler == null){
			throw new HubMappingException("No handler defined");
		}
		NamedNodeMap x = handler.getAttributes();
		Node handlerClass = x.getNamedItem("namespace");
		if(handlerClass == null){
			throw new HubMappingException("No handler class defined");
		}
		return getHandlerClass(handlerClass.getNodeValue());
	}
	
	private static boolean loadFtiFlag(Document doc){
		NodeList handlerNodes = doc.getElementsByTagName("fti_aware");
		Node ftiFlag = null;
		Node tmp;
		for(int i = 0; i < handlerNodes.getLength();i++){
			tmp = handlerNodes.item(0);
			if (tmp.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			ftiFlag = tmp;
		}
		if(ftiFlag != null){
			return ftiFlag.getTextContent().equalsIgnoreCase("true");
		}
		return false;
	}
	
	private static boolean loadPiiFlag(Document doc){
		NodeList handlerNodes = doc.getElementsByTagName("pii_aware");
		Node piiFlag = null;
		Node tmp;
		for(int i = 0; i < handlerNodes.getLength();i++){
			tmp = handlerNodes.item(0);
			if (tmp.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			piiFlag = tmp;
		}
		if(piiFlag != null){
			return piiFlag.getTextContent().equalsIgnoreCase("true");
		}
		return false;
	}

	private static Transformer loadGlobalTransformer(Document doc){
		NodeList handlerNodes = doc.getElementsByTagName("global_transformer");
		Node global = null;
		Node tmp;
		for(int i = 0; i < handlerNodes.getLength();i++){
			tmp = handlerNodes.item(0);
			if (tmp.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			global = tmp;
		}
		if(global != null){
			String clsName =  global.getTextContent();
			try {
				return (Transformer) GIWSModuleAwareClassLoader.forName(clsName).newInstance();
			}catch(Exception e) {
				LOGGER.error("Error loading the global transformer for class {}",clsName);
				throw new RuntimeException("Failed to load the declared global transformer ", e);
			}
		}
		return null;
	}
	
	private static String loadRefreshTime(Document doc){
		NodeList handlerNodes = doc.getElementsByTagName("refresh_frequency");
		Node refreshFrequency = null;
		Node tmp;
		for(int i = 0; i < handlerNodes.getLength();i++){
			tmp = handlerNodes.item(0);
			if (tmp.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			refreshFrequency = tmp;
		}
		if(refreshFrequency != null){
			return refreshFrequency.getTextContent();
		}
		return null;
	}
	
	public static Class<?> getClass(String clsName) throws HubMappingException{
		Class<?> cls = null;
		ClassNotFoundException clex = null;
		for(ClassLoader cl: availableClassLoaders){
			try{
				cls = (Class<?>) Class.forName(clsName,true,cl);
			}catch(ClassNotFoundException e){
				clex = e;
			}
		}
		if(cls != null){
			return cls;
		}
		throw new HubMappingException("Failed to load the service handler "+clsName,clex);
	}

	@SuppressWarnings("unchecked")
	private static Class<ServiceHandler> getHandlerClass(String hClassName) throws HubMappingException {
		Class<ServiceHandler> cls = null;
		ClassNotFoundException clex = null;
		for(ClassLoader cl: availableClassLoaders){
			try{
				cls = (Class<ServiceHandler>) Class.forName(hClassName,true,cl);
			}catch(ClassNotFoundException e){
				clex = e;
			}
		}
		if(cls != null){
			return cls;
		}
		throw new HubMappingException("Failed to load the service handler "+hClassName,clex);
	}
	
	@SuppressWarnings(RAW_TYPES)
	private static void loadParametersMapAndNamespace(Document doc, Set<Class> namespaceSet, List<ParameterMap> parameterMapList) throws HubMappingException, BridgeException {
		NodeList maps = doc.getElementsByTagName("map");
		Node mapNode = null;
		ParameterMap tmpMap = null;
		int len = maps.getLength();
		for(int i = 0; i < len; i ++){
			mapNode = maps.item(i);
			tmpMap = extractParameterMap(mapNode);
			namespaceSet.add(tmpMap.getTargetNamespace());
			parameterMapList.add(tmpMap);
			tmpMap = null;
		}
	}
	
	private static ParameterMap extractParameterMap(Node mapNode) throws HubMappingException, BridgeException {
		NodeList paramList = mapNode.getChildNodes();
		int len = paramList.getLength();
		Node tmpParamNode = null;
		ParameterMap tmpParameterMap = new ParameterMap();
		Parameter param = null;
		for(int i = 0; i < len; i++){
			tmpParamNode = paramList.item(i);
			if(tmpParamNode.getNodeType() != Node.ELEMENT_NODE){
				continue;
			}
			// We have a parameter node
			param = extractParameter(tmpParamNode);
			tmpParameterMap.setParameter(param);
		}
		return tmpParameterMap;
	}

	private static Parameter extractParameter(Node tmpParamNode) throws HubMappingException {
		Parameter tmpParam = null;
		String paramName = null;
		String isTarget = null;
		String attrName = null;
		String attrVal = null;
		String type = null;
		
		String namespace = tmpParamNode.getNamespaceURI();
		NamedNodeMap attrsList = tmpParamNode.getAttributes();
		Node tmpAttr = null;
		int attrLen  = attrsList.getLength();
		for(int i = 0; i < attrLen; i++){
			tmpAttr = attrsList.item(i);
			attrName = tmpAttr.getNodeName();
			attrVal = tmpAttr.getNodeValue();
			if(attrName.equals("name")){
				paramName = attrVal;
			}else if(attrName.equals("isTarget")){
				isTarget = attrVal; 
			}else if(attrName.equals("type")){
				type = attrVal;
			}else{
				LOGGER.warn("Unkbown param attribute ["+attrName+"="+attrVal+"] found..ignoring");
			}
		}
		if(paramName == null){
			throw new HubMappingException("Invalid parameter definition, name not found");
		}
		tmpParam = new Parameter(namespace, paramName,type);
		tmpParam.setTarget(Boolean.parseBoolean(isTarget));
		tmpParam.setType(type);
		//Lets get the validator
		Validator validator = extractValidatorFromParameter(tmpParamNode);
		Transformer transformer = extractTransformarFromParameter(tmpParamNode);
		if(validator != null){
			//
			LOGGER.info("Found validator for:"+paramName+" Validator:"+validator.getClass().getName());
			tmpParam.setValidator(validator);
		}
		if(transformer != null){
			LOGGER.info("Found transformer for:"+paramName+" Transformer:"+transformer.getClass().getName());
			tmpParam.setTransformer(transformer);
		}
		return tmpParam;
	}
	
	
	private static Transformer extractTransformarFromParameter(Node tmpParamNode) throws HubMappingException {
		NodeList childs = tmpParamNode.getChildNodes();
		int len = childs.getLength();
		String transformarClassName = null;
		for(int i = 0;  i < len; i++){
			if(childs.item(0).getNodeType() != Node.ELEMENT_NODE &&
					!childs.item(i).getNodeName().equalsIgnoreCase("transformer")){
				continue;
			}
			transformarClassName = childs.item(i).getTextContent();
			NamedNodeMap attributes = childs.item(i).getAttributes();
			
			if(transformarClassName != null && transformarClassName.length() > 0){
				return extractTransformer(transformarClassName, attributes);
			}
		}
		return null;
	}
	
	@SuppressWarnings(RAW_TYPES)
	private static Transformer extractTransformer(String transformarClassName, NamedNodeMap attributes) throws HubMappingException{
		
		try{
			Class cls = Class.forName(transformarClassName);
			Transformer transformer = (Transformer) cls.newInstance();

			//Adding transformer attributes to context				
			for(int j =0; j<attributes.getLength();j++){
				Node attribute = attributes.item(j);
				transformer.setContextValue(attribute.getNodeName(), attribute.getNodeValue());
			}
			return transformer;
			//.newInstance();
		}catch(Exception e){
			throw new HubMappingException("Failed to find the vaidator class ["+transformarClassName+"]", e);
		}
	}
	
	private static Validator extractValidatorFromParameter(Node tmpParamNode) throws HubMappingException {
		NodeList childs = tmpParamNode.getChildNodes();
		int len = childs.getLength();
		String validatorClassName = null;
		for(int i = 0;  i < len; i++){
			if(childs.item(0).getNodeType() != Node.ELEMENT_NODE &&
					!childs.item(i).getNodeName().equalsIgnoreCase("validator")){
				continue;
			}
			validatorClassName = childs.item(i).getTextContent();
			NamedNodeMap attributes = childs.item(i).getAttributes();
			
			if(validatorClassName != null && validatorClassName.length() > 0){
				return extractValidator(validatorClassName, attributes);
			}
		}
		return null;
	}
	
	@SuppressWarnings(RAW_TYPES)
	private static Validator extractValidator(String validatorClassName, NamedNodeMap attributes) throws HubMappingException{
		
		try{
			Class cls  = GIWSModuleAwareClassLoader.forName(validatorClassName);
	 	 	 	 		                       
			Validator validator = (Validator) cls.newInstance();

			//Adding validator attributes to context				
			for(int j =0; j<attributes.getLength();j++){
				Node attribute = attributes.item(j);
				validator.addContextParameter(attribute.getNodeName(), attribute.getNodeValue());
			}
			
			return validator;
			//.newInstance();
		}catch(Exception e){
			throw new HubMappingException("Failed to find the vaidator class ["+validatorClassName+"]", e);
		}
		
	}
	
	public void setServiceHandler(ServiceHandler handler) {
		this.servicehandler = handler;
		
	}
	
	public ServiceHandler getServiceHandler() {
		return servicehandler; 
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
}

class HandlerEnvironment{
	
	private static final String RAW_TYPES = "rawtypes";
	
	@SuppressWarnings(RAW_TYPES)
	private Set<Class> namespaceSet = null;
	private List<ParameterMap> parameterMapList = null;
	private Class<ServiceHandler> handlerClass = null;
	private static final int HOURS_IN_DAY = 24;
	private static final int MINUTES_IN_HOUR = 60;
	private static final int SECS_IN_MIN = 60;
	private static final int MILLIS_IN_SEC = 1000;
	
	private static final int MILLIS_IN_DAY = HOURS_IN_DAY * MINUTES_IN_HOUR * SECS_IN_MIN * MILLIS_IN_SEC;
	
	private int refreshTime = MILLIS_IN_DAY; //1 day
	private long dateCreated = 0;

	private static final int DD = 0;
	private static final int HH = 1;
	private static final int MM = 2;
	private Logger logger = LoggerFactory.getLogger(HandlerEnvironment.class);
	
	private boolean ftiAware;

	private List<Schedule> schedule;

	private boolean piiFlag;

	private Transformer transformer;
	

	public boolean isFtiAware() {
		return this.ftiAware;
	}
	
	public void setPiiFlag(boolean pFlag) {
		this.piiFlag = pFlag;
	}
	
	public boolean getPiiFlag(){
		return this.piiFlag;
	}

	public Transformer getGlobalTransformer() {
		return transformer;
	}

	public void setGlobalTransformer(Transformer globalTransformer) {
		this.transformer = globalTransformer;
	}

	public void setScheduleInfo(List<Schedule> schedule) {
		this.schedule = schedule;
	}
	
	public List<Schedule> getScheduleInfo() {
		return this.schedule;
	}

	public HandlerEnvironment(){
		this.dateCreated = System.currentTimeMillis();
	}
	
	@SuppressWarnings(RAW_TYPES)
	public Set<Class> getNamespaceSet() {
		return namespaceSet;
	}
	
	@SuppressWarnings(RAW_TYPES)
	public void setNamespaceSet(Set<Class> namespaceSet) {
		this.namespaceSet = namespaceSet;
	}
	public List<ParameterMap> getParameterMapList() {
		return parameterMapList;
	}
	public void setParameterMapList(List<ParameterMap> parameterMapList) {
		this.parameterMapList = parameterMapList;
	}
	public Class<ServiceHandler> getHandlerClass() {
		return handlerClass;
	}
	public void setHandlerClass(Class<ServiceHandler> handlerClass) {
		this.handlerClass = handlerClass;
	}
	
	private void setupRefreshTime(String timeStr, int mode){
		
		int refreshMin = 0;
		int refreshDays = 0;
		int refreshHours = 0;
		boolean useDefaultRefreshTime = false;
		
		try{
			switch(mode){
			case MM:
				refreshMin = Integer.parseInt(timeStr);
				break;
			
			case DD:
				refreshDays = Integer.parseInt(timeStr);
				break;
			
			case HH:
				refreshHours = Integer.parseInt(timeStr);
				break;
			
			default:
				break;
			
			}
		}catch(NumberFormatException ne){
			useDefaultRefreshTime = true;
		}
		if(useDefaultRefreshTime){
			this.refreshTime = MILLIS_IN_DAY;
		}else{
			this.refreshTime = (refreshDays*HOURS_IN_DAY*MINUTES_IN_HOUR*SECS_IN_MIN*MILLIS_IN_SEC)+
								(refreshHours*MINUTES_IN_HOUR*SECS_IN_MIN*MILLIS_IN_SEC)+
								(refreshMin*SECS_IN_MIN*MILLIS_IN_SEC);
		}
		
	}
	
	
	private static final int SEGMENT_LEN = 3;
	
	//Format DD:HH:MM
	public void setRefreshTime(String refreshTime){
		if(refreshTime != null && refreshTime.length() > 0){
			String[] timeSegments = refreshTime.split(":");
			if(timeSegments.length <= SEGMENT_LEN){
				int i = 0;
				for(String str: timeSegments){
					this.setupRefreshTime(str,i);
					i++;
				}
			}
		}
	}
	
	public void setFtiAware(boolean ftiAware) {
		this.ftiAware = ftiAware;
	}
	
	public ServiceHandler getServiceHandler() throws HubMappingException{
		ServiceHandler handler = null;
		try{
			handler =  this.handlerClass.newInstance();
			handler.setNamespaceSet(namespaceSet);
			handler.setParameterMap(parameterMapList);
			handler.setFtiAware(this.ftiAware);
			handler.setPiiAware(this.piiFlag);
			handler.setServiceSchedule(this.schedule);
			handler.setGlobalTransformer(this.transformer);
		}catch(Exception e){
			throw new HubMappingException("Failed to create service handler for class:"+this.handlerClass.getName(),e);
		}
		return handler;
	}
	
	public boolean isStale(){
		long now  = System.currentTimeMillis();
		if((this.dateCreated +this.refreshTime) > now){
			return false;
		}
		logger.info("Stale Environment:"+this.handlerClass.getName());
		return true;
	}
}


