/**
 * 
 */
package com.getinsured.iex.hub.platform.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ValidationException;

/**
 * Utility class for the VLP service operations
 * 
 * @author Nikhil Talreja
 * @since 14-Jan-2014
 *
 */

public final class PlatformServiceUtil {
	
	private static final String XML_ENTITY_PROP_PARAM = "http://xml.org/sax/features/external-parameter-entities";
	private static final String XML_ENTITY_PROP_GENERAL = "http://xml.org/sax/features/external-general-entities";
	private static final String XML_ENTITY_PROP_SECURE = "http://javax.xml.XMLConstants/feature/secure-processing";
	private static Logger logger = LoggerFactory.getLogger(PlatformServiceUtil.class);
	
	private PlatformServiceUtil(){
		
	}
	
	//private static final Logger logger = LoggerFactory.getLogger(PlatformServiceUtil.class);
	
	/**
	 * Checks if the date is in the required format
	 * Creates an XML Gregorian Calendar Object type from the date string
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 * @param date - Date as string
	 * @param dateFormat - Date format of the string
	 * @return XMLGregorianCalendar form of the date
	 * 
	 * @throws ParseException - If the beginning of the specified string cannot be parsed as the required format
	 * @throws DatatypeConfigurationException -  If the implementation is not available or cannot be instantiated.
	 */
	public static XMLGregorianCalendar stringToXMLDate(String date, String dateFormat) throws ParseException, DatatypeConfigurationException{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		Date parsedDate = sdf.parse(date);
		GregorianCalendar gDate = new GregorianCalendar();
		gDate.setTime(parsedDate);
		return  DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
	}
	
	/**
	 * Checks if the date is in the required format
	 * Converts XML Gregorian Calendar Object type to required date string type
	 * 
	 * @author - Nikhil Talreja
	 * @since - 10-Feb-2014
	 *
	 * @param date - XMLGregorianCalendar Date
	 * @param dateFormat - Date format of the string
	 * @return String form of the date
	 * 
	 * @throws ParseException - If the beginning of the specified string cannot be parsed as the required format
	 * @throws DatatypeConfigurationException -  If the implementation is not available or cannot be instantiated.
	 */
	public static String xmlDateToString(XMLGregorianCalendar date, String dateFormat) throws ParseException, DatatypeConfigurationException{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		return sdf.format(date.toGregorianCalendar().getTime());
	}
	
	/**
	 * Do not change this method, de serialization will be done by platform based on this implementation
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String exceptionToJson(Throwable e){
		if(e == null){
			return null;
		}
		HubServiceException he = null;
		JSONObject obj  = new JSONObject();
		JSONObject error = new JSONObject();
		JSONArray errors = null;
		if(e instanceof HubServiceException){
			he = (HubServiceException)e;
			error.put("ERROR_CODE".intern(), he.getErrorCode());
			if(he.getErrorCode().equalsIgnoreCase(RequestStatus.UNDER_MAINT.getStatusCode())){
				error.put("SERVICE_UNDER_MAINT".intern(), he.getServiceDownTimeSchedule());
			}
		}else if(e instanceof ValidationException){
			errors = ((ValidationException)e).getValidationErrorMessages();
		}
		error.put("EXCEPTION_TYPE".intern(), e.getClass().getName());
		if(errors != null){
			error.put("VALIDATION_ERRORS".intern(), errors);
		}
		
		String message = e.getMessage();
		if(message != null)
		{
			message = message.replaceAll("\"","");
		}
		error.put("EXCEPTION_MESSAGE".intern(), message);
		Exception tmpExp = (Exception) e.getCause();
		if(tmpExp != null){
			error.put("EXCEPTION_CAUSE".intern(), exceptionToJson(e.getCause()));
		}
		obj.put("EXCEPTION".intern(), error);
		
		String response = obj.toJSONString();
		response = response.replaceAll("\\\\", "");
		response = response.replaceAll("(\"\\{)", "\\{");
		response = response.replaceAll("(\\}\")", "\\}");
		return response;
	}
	
	/**
	 * @author Biswakesh This method is used create an instance of
	 *         DocumentBuilderFactory and prevent external entity expansion and
	 *         injection attacks
	 * @return Object containing DocumentBuilderFactory instance
	 * @throws ParserConfigurationException
	 */
	public static DocumentBuilderFactory getDocumentBuilderFactoryInstance()
			throws ParserConfigurationException {
		DocumentBuilderFactory factory = null;

		factory = DocumentBuilderFactory.newInstance();
		// HIX-67478 - XML External Entity Expansion Injection for SHARED-PLATFORM fix
		factory.setFeature(XML_ENTITY_PROP_SECURE, true);
		// HIX-67479 - XML External Entity Injection for SHARED-PLATFORM fix
		factory.setFeature(XML_ENTITY_PROP_GENERAL, false);
		factory.setFeature(XML_ENTITY_PROP_PARAM, false);

		return factory;
	}
	
	public static String getHash(String input){
		MessageDigest digest;
		if(input == null || input.length() == 0){
			logger.warn("No input provided for calcuating the hash, nothing to hash");
			return null;
		}
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(hash);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error while calculating SHA 256 Hash".intern(),e);
		}
		return null;
	}
	
	public static ResponseEntity<String> getResponseEntity(String responseStr, HttpStatus status, HashMap<String, String> headers, MediaType type){
		HttpHeaders x = new HttpHeaders();
	    x.setContentType(type);
	    if(headers != null && headers.size() > 0){
	    	 Iterator<Entry<String, String>> cursor = headers.entrySet().iterator();
	    	 Entry<String, String> tmp = null;
	    	 while(cursor.hasNext()){
	    		 tmp = cursor.next();
	    		 x.add(tmp.getKey(), tmp.getValue());
	    	 }
	    	
	    }
		return new ResponseEntity<String>(responseStr, x, status);
	}
	
	/**
	 * 
	 * @param e
	 * @param maxLines
	 * @return
	 */
	public static String shortenedStackTrace(Exception e, int maxLines) {
		if (e == null) {
			return "";
		}
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		String[] lines = writer.toString().split("\n");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
			sb.append(lines[i]).append("\n");
		}
		return sb.toString();
	}

	
}
