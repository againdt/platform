package com.getinsured.iex.hub.platform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceMessageCallback;

public abstract class AbstractServiceHandler implements ServiceHandler {
	
	protected WebServiceMessageCallback callBackHandler = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractServiceHandler.class);
	
	private Map<String, Object> namespaceObjects = new HashMap<String, Object>();
	private List<ParameterMap> parameterMap;
	private List<Schedule> schedule = null;
	
	private String responseCode = null;
	private String serviceName = null;
	private boolean ftiAware = false;
	protected RequestStatus requestStatus = RequestStatus.FAILED;
	
	private static final String RAW_TYPES = "rawtypes";
	
	/*
	 * This field will be used by services to determine the identifier
	 * to be used while interacting with the HUB
	 *
	 * E.g. for VLP service, this field will hold the DHS ID type to be used
	 * for verification
	 *
	 * author - Nikhil Talreja
	 * since - 13-Feb-2014
	 */
	private Object objectIdentifier;


	private String jsonInput;

	private Map<String, Object> context = null;

	private Map<String, Object> responseContext = null;

	@SuppressWarnings(RAW_TYPES)
	private Set<Class> namespaceSet;
	private boolean piiAware;
	protected Transformer transformer;

	public void setParameterMap(List<ParameterMap> parameterMapList) {
		this.parameterMap = parameterMapList;
	}

	public Map<String, Object> getNamespaceObjects() {
		
		if(this.namespaceObjects == null){
			this.namespaceObjects = new HashMap<String, Object>();
		}
		
		return namespaceObjects;
	}

	public void setNamespaceObjects(Map<String, Object> namespaceObjects) {
		this.namespaceObjects = namespaceObjects;
	}

	@SuppressWarnings(RAW_TYPES)
	public void setNamespaceSet(Set<Class> namespaceSet) {
		this.namespaceSet = namespaceSet;

	}

	public Object getObjectIdentifier() {
		return objectIdentifier;
	}

	public void setObjectIdentifier(Object objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	@SuppressWarnings(RAW_TYPES)
	public void handleInputParameter(String name, Object value) throws HubServiceException, HubMappingException{
	Class namespace = null;
	for(ParameterMap map: parameterMap){
		if(!map.acceptInput(name)){
			continue;
		}
		Parameter p = map.getTarget();
		namespace = p.getNamespace();
		if(!this.namespaceSet.contains(namespace)){
			throw new HubServiceException("Namespace does not exists "+namespace+" for parameter:"+name);
		}
		/*
		 * This duplicate call to validator is being commented out
		 * transformer and validator are being called inside the
		 * setValue() method
		 * 
		 * Besides this, calling the transformer without calling
		 * the validator will cause exceptions
		 * 
		 * --Nikhil Talreja
		 */
		/*try{
			Validator v = p.getValidator();
			if(v!= null){
				v.validate(name, value);
			}
		}catch(InputDataValidationException ie){
			throw new HubServiceException(ie);
		}*/
		Object obj = getNamespaceObjects().get(namespace.getName());
		if(obj == null){
			obj = getNamespaceInstance(namespace);
			getNamespaceObjects().put(namespace.getName(),obj );
		}
		
		p.setValue(obj, value);
	}

	}

	public boolean setRequestParameterFromJson(Object targetObj, JSONObject jsonObj, String jsonKey, String requestKey, String namespace) throws  HubMappingException
	{
		boolean ret = false;
		if(jsonObj == null)
		{
			LOGGER.error("Invalid JSON Object passed in setRequestParameterFromJson");
			throw new HubMappingException("JSON object for accessing key: " + requestKey + " is null");
		}

		Parameter requestParameter = null;

		if (jsonObj.get(jsonKey) != null) {
			requestParameter = this.getTargetParameter(requestKey, namespace);

			if (requestParameter == null) {
				LOGGER.error("Invalid " + requestKey + " :");
				throw new HubMappingException(
						"Failed to find parameter mapping for " + requestKey + " for namespace:"
								+ namespace);
			}
			requestParameter.setValue(targetObj, jsonObj.get(jsonKey));
			ret = true;
		}
		return ret;
	}

	@SuppressWarnings(RAW_TYPES)
	private Object getNamespaceInstance(Class clz) throws HubMappingException {
		try{
			return clz.newInstance();
		}catch(Exception e){
			throw new HubMappingException("Failed to initilize the namespace"+clz.getCanonicalName(),e);
		}
	}

	/**
	 * This method will be use full when you have input available in JSON format, implementation can process the entire JSON
	 * provided to extract what they want or use the parameter @param jsonSectionKey to extract a particular section they are interested in
	 */
	public Object getPayLoadFromJSONInput(String jsonSectionKey, String jsonStr) throws HubServiceException, HubMappingException{
		throw new HubServiceException("Operation not supported, Individual services need to provide implementation for this operation");
	}

	protected Parameter getTargetParameter(String input, String className){
		List<ParameterMap> x = this.parameterMap;
		Iterator<ParameterMap> cursor = x.iterator();
		ParameterMap pm;
		Parameter target;
		while (cursor.hasNext()) {
			pm = cursor.next();
			if(pm.acceptInput(input)){
				target = pm.getTarget();
				if(target.getNamespace().getName().equals(className)){
					return target;
				}
			}

		}
		return null;
	}

	protected List<Parameter> getAllTargetParameters(String input){
		List<Parameter> returnList = new ArrayList<Parameter>();
		List<ParameterMap> x = this.parameterMap;
		Iterator<ParameterMap> cursor = x.iterator();
		ParameterMap pm;
		while (cursor.hasNext()) {
			pm = cursor.next();
			if(pm.acceptInput(input)){
				returnList.add(pm.getTarget());
			}
		}
		return returnList;
	}

	public String getJsonInput() {
		return jsonInput;
	}

	public void setJsonInput(String jsonInput) {
		if(jsonInput == null){
			LOGGER.warn("**** No data provided while initializing the required JSON input ****");
		}else{
			this.jsonInput = jsonInput;
			LOGGER.info("Received JSON input:"+this.jsonInput.length()+" Characters");
		}
	}

	@Override
	public String handleError(Map<String, Object> context){
		throw new HubServiceException("operation not supported, Individual services need to provide implementation for this operation");
	}

	public void setContext(Map<String, Object> context){
		this.context = context;
	}

	@Override
	public Map<String, Object> getResponseContext(){
		if(this.responseContext == null){
			this.responseContext = new HashMap<String, Object>();
		}
		return this.responseContext;
	}

	public Map<String, Object> getContext(){
		if(this.context == null){
			this.context = new HashMap<String, Object>();
		}
		return this.context;
	}

	public boolean isServiceAvailable() throws ServiceUnAvailableException{
		if(this.schedule == null || this.schedule.isEmpty()){
			LOGGER.warn("No schedule information available, assuming service is available");
			return true;
		}
		Iterator<Schedule> cursor = this.schedule.iterator();
		Schedule sch;
		while(cursor.hasNext()){
			sch = cursor.next();
			sch.isServiceAvailable();
		}
		return true;
	}

	public void setServiceSchedule(List<Schedule> schedule){
		this.schedule = schedule;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	protected void extractSSAPData(Object ssapdata, Object wrapper) throws HubMappingException{
		Parameter source;
		Parameter target;
		for(ParameterMap map: this.parameterMap){
			source = map.getSource();
			target = map.getTarget();
			@SuppressWarnings(RAW_TYPES)
			Class sourceNamespace =source.getNamespace();
			if(sourceNamespace == null){
				LOGGER.warn("No namespace found for "+source.getName()+" not sure how to handle, skipping");
				continue;
			}
			if(sourceNamespace.getName().equals(ssapdata.getClass().getName()) &&
					target.getNamespace().getName().equals(wrapper.getClass().getName())){
				target.setValue(wrapper,source.getValue(ssapdata));
			}
		}
	}
	@SuppressWarnings("unchecked")
	protected void listJSONKeys(){
		JSONParser parser  = new JSONParser();
		JSONObject obj = null;
		try {
			obj = (JSONObject) parser.parse(jsonInput);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage(),e);
		}
		Set<Entry<String, Object>> eSet = obj.entrySet();
		Iterator<Entry<String, Object>> i = eSet.iterator();
		while(i.hasNext()){
			LOGGER.info("Key:"+i.next().getKey());
		}
	}
	
	public boolean isFtiAware() {
		return ftiAware;
	}

	public void setFtiAware(boolean ftiAware) {
		this.ftiAware = ftiAware;
	}
	
	public boolean isPiiAware(){
		return this.piiAware;
	}
	
	public void setPiiAware(boolean isPii){
		this.piiAware = isPii;
	}

	public void setGlobalTransformer(Transformer transformer) {
		this.transformer = transformer;
	}

	public abstract RequestStatus getRequestStatus();
	
	public List<Schedule> getServiceDownTimeSchedule(){
		return this.schedule;
	}
	
	public WebServiceMessageCallback getMessageCallback(){
		return this.callBackHandler;
	}
	
	protected Jaxb2Marshaller getMarshaller(String contextPath, Object namespaceMapper) {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(contextPath);
		Map<String, Object> props = new HashMap<>();
		props.put("com.sun.xml.bind.namespacePrefixMapper", namespaceMapper);
		marshaller.setMarshallerProperties(props);
		return marshaller;
	}
	
	protected Jaxb2Marshaller getMarshaller(String[] contextPaths, Object namespaceMapper) {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPaths(contextPaths);
		Map<String, Object> props = new HashMap<>();
		props.put("com.sun.xml.bind.namespacePrefixMapper", namespaceMapper);
		marshaller.setMarshallerProperties(props);
		return marshaller;
	}
}
