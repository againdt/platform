package com.getinsured.iex.hub.platform;


public interface Transformer {
	
	Object transform(Object obj) throws ObjectTransformationException;
	String getContextValue(String key);
	void setContextValue(String key, String value);

}
