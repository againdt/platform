package com.getinsured.iex.hub.platform.security;

import java.io.IOException;
import java.net.URI;
import java.util.Base64;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.transport.WebServiceConnection;
import org.springframework.ws.transport.http.HttpComponentsConnection;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;
import org.springframework.ws.transport.http.HttpTransportConstants;

public class TenantAwareMessageSender extends HttpComponentsMessageSender {
	private Logger logger = LoggerFactory.getLogger(TenantAwareMessageSender.class);
	private String credentialsStr = null;
	private String tenantId;
	
	public TenantAwareMessageSender(String targetTenantId, String userName, String password){
		super();
		this.tenantId = targetTenantId;
		this.credentialsStr = "Basic "+Base64.getEncoder().encodeToString((userName+":"+password).getBytes());
	}
	
	public TenantAwareMessageSender(){
		super();
	}
	
	@Override
	public WebServiceConnection createConnection(URI uri) throws IOException {
		logger.info("Creating WS connection for target server with URI:"+uri.getRawQuery());
		HttpPost httpPost = new HttpPost(uri);
		if (isAcceptGzipEncoding()) {
			httpPost.addHeader(HttpTransportConstants.HEADER_ACCEPT_ENCODING,
					HttpTransportConstants.CONTENT_ENCODING_GZIP);
		}
		if(credentialsStr != null){
			httpPost.addHeader("Authorization".intern(),credentialsStr);
			if(logger.isDebugEnabled()){
				logger.debug("Added basic auth header for tenant:"+this.tenantId);
			}
		}
		HttpContext httpContext = createContext(uri);
		return new GhixHttpComponentConnection(getHttpClient(), httpPost, httpContext);
	}
	
	private class GhixHttpComponentConnection extends HttpComponentsConnection{
		protected GhixHttpComponentConnection(HttpClient httpClient, HttpPost httpPost, HttpContext httpContext) {
			super(httpClient, httpPost, httpContext);
		}
	}

}
