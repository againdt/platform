package com.getinsured.iex.hub.platform.ssap.iex;

import org.json.simple.JSONObject;

/**
 * SSAP model class for Incarceration Info
 * 
 * @author Nikhil Talreja
 * @since 15-Apr-2014
 *
 */
public class IncarcerationInfo {
	
	private Boolean incarcerationAsAttestedIndicator;
	
	public IncarcerationInfo(JSONObject citizenshipInfo){
		
		Object tmp = null;
		if(citizenshipInfo != null){
			tmp = citizenshipInfo.get("incarcerationAsAttestedIndicator");
			 if(tmp instanceof Boolean){
				 this.incarcerationAsAttestedIndicator = (Boolean)tmp;
			 }
		}
		
	}
	
	public Boolean isIncarcerationAsAttestedIndicator() {
		return incarcerationAsAttestedIndicator;
	}
	
}
