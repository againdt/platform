package com.getinsured.iex.hub.platform.security;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;



@Component("hubGhixSecretKey")
@Scope("singleton")
public class GhixSecretKey {
	public static String ENRYPTION_KEY_FILE;
	private Logger logger = LoggerFactory.getLogger(GhixSecretKey.class);
	private static String encryptionKey;
	
	public GhixSecretKey() {
		ENRYPTION_KEY_FILE = System.getProperty("GHIX_HOME")+ File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+"secretKey.txt";
		File secretKeyFile = new File(ENRYPTION_KEY_FILE);
		if(!secretKeyFile.exists() || secretKeyFile.isDirectory()){
			logger.error("Failed to find encryption key file : " + ENRYPTION_KEY_FILE + " does not exist or is not a file");
			ENRYPTION_KEY_FILE = "NONE".intern();
		}
		initSecureProperties(ENRYPTION_KEY_FILE);
	}

	private void initSecureProperties(String secPropertiesFile) {
		Properties props = new Properties();
		if(secPropertiesFile.equalsIgnoreCase("NONE")){
			logger.error("No secret key provided, expected availability of \"" + secPropertiesFile + "\" property, found none, will use default");
		}else{
		try {
			props.load( new FileInputStream(secPropertiesFile));
		} catch (Exception e) {
				logger.error("Error loading the secure properties from file "+ secPropertiesFile+" failed with exception "+e.getMessage(),e);
			} 
		} 
		if(props.size() == 0){
			logger.info("No secure properties available from file:"+secPropertiesFile);
		}else{
		encryptionKey = props.getProperty("ENCRYPTION_KEY");
		}
		if(encryptionKey == null){
			logger.error("Encryption key is a mandatory property required to run the application, using default, change as soon as possible");
			encryptionKey = "ghix123";
		}
	}

	public static String getEncryptionKey() {
		return encryptionKey;
	}

	}
