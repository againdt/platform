package com.getinsured.iex.hub.platform.repository.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.hub.platform.models.GIHUBResponse;

@Repository
public interface HubResponseRepository extends JpaRepository<GIHUBResponse, Long> {

}
