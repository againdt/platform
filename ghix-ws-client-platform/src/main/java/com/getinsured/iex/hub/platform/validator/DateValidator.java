package com.getinsured.iex.hub.platform.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
/**
 * Date validator for date fields
 * 
 * @author Vishaka Tharani
 * @since 11-Feb-2014
 *
 */
public class DateValidator extends Validator {
	private static final Logger LOGGER = LoggerFactory.getLogger(DateValidator.class);
	private boolean futureDateAllowed = true;
	private String dateFormat;

	public void addContextParameter(String name, Object value){
		super.addContextParameter(name, value);
		String tmp;
		if(name != null && value != null){
			if(name.equalsIgnoreCase("futureDateAllowed")){
				tmp = (String)value;
				this.futureDateAllowed = !tmp.equalsIgnoreCase("N");
			}
			if(name.equalsIgnoreCase("dateFormat")){
				tmp = (String)value;
				this.dateFormat = tmp;
			}
		}
	}
	/** Validates a field using following rules
	 *  1. isRequired - If "Y", the value is mandatory
	 *  2. minLength - Minimum length of field
	 *  3. maxLength - Maximum length of field
	 *  4. dateFormat - To match a specific format of date
	 *  
	 *  @param name - Name of the field
	 *  @param value - Value for the parameter
	 *  
	 *  @throws InputDataValidationException - If a validation fails
	 *  
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		if(this.checkForNullOrEmpty(name, value)){
			LOGGER.info("Skipping Length validation, for "+name+" is null or empty");
			return;
		}
		
		if(!(value instanceof String)){
			throw new InputDataValidationException("Incompatible value for date validation of "+name);
		}
		
		this.checkForMinAndMaxLen(name, value);
		
		if(this.dateFormat == null){
			throw new InputDataValidationException("Can not validate the date without the format");
		}
		
		validateDate(name,value);
	}
	
	private void validateDate(String name, Object value) throws InputDataValidationException {
		
		SimpleDateFormat sdf = new SimpleDateFormat(this.dateFormat);
		sdf.setLenient(false);
		try{
			Date providedDate = sdf.parse((String)value);
			Date current = new Date(System.currentTimeMillis());
			if(providedDate.after(current) && !this.futureDateAllowed){
				throw new InputDataValidationException(name+" provided with future date");
			}
		}catch(ParseException pe){
				throw new InputDataValidationException("failed to validate the parameter "+name+" invalid format ["+this.dateFormat+"] provided",pe);
		}
	}
}
