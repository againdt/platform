package com.getinsured.iex.hub.platform;

import java.util.Set;

public interface ConfigurationChangeListener {
	public Set<String> getSubscribedConfigurationList();
	public void handleConfigurationChange(String name, String currentProperty);
}
