package com.getinsured.iex.hub.platform.ssap.iex;

public class MalformedSSAPApplicationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MalformedSSAPApplicationException() {
	}

	public MalformedSSAPApplicationException(String message) {
		super(message);
	}

	public MalformedSSAPApplicationException(Throwable cause) {
		super(cause);
	}

	public MalformedSSAPApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public MalformedSSAPApplicationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
