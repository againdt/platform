package com.getinsured.iex.hub.platform.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;

/**
 * Generic validator for hub request fields
 * 
 * @author Nikhil Talreja
 * @since 11-Feb-2014
 *
 */
public class FieldValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FieldValidator.class);
	
	private void checkIfNumeric(String name, Object value) throws InputDataValidationException{
		if(isNumeric()){
			String tmp;
			if(value instanceof String){
				tmp = (String)value;
				char[] chars = tmp.toCharArray();
				for(char c: chars){
					checkForDigit(c,name);
				}
			}else if(!(value instanceof Long) || !(value instanceof Integer)){
				throw new InputDataValidationException(name+" is a numeric field");
			}
		}
	}
	
	private void checkForDigit(char c, String name) throws InputDataValidationException{
		if(!Character.isDigit(c)){
			throw new InputDataValidationException(name+" is a numeric field");
		}
	}
	
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		LOGGER.info("Validating field " + name);
		
		if(this.checkForNullOrEmpty(name, value)){
			LOGGER.info("Skipping validation, for "+name);
			return;
		}
		
		this.checkIfNumeric(name,value);
		this.checkForMinAndMaxLen(name, value);
		this.checkForRegularExpression(name, value);
		this.checkForAllowedvalues(name, value);
	}
}
