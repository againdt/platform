package com.getinsured.iex.hub.platform.security;

/**
 * This class provides a convenient and efficient way to encrypt data using DES and TripleDES algorithms
 * 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.EmptyStackException;
import java.util.Stack;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class GhixSymetricPooledCipher {
	private static final String saltBytesStr = "%$dfkht^*jj";
	private static final int KEY_ITERATIONS=1000;
	public static final String FORMAT = "yyyy-MMM-dd HH:mm:ss zzz"; 
	private static Logger logger = LoggerFactory.getLogger(GhixSymetricPooledCipher.class);
	private static final String CIPHER_ALGO = "PBEWithMD5AndDES";
	private static SecretKeySpec secret = null;
	//Encryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeEncryptorPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyEncryptorPool = new Stack<Cipher>();
	
	//Decryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeDecryptionPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyDecryptionPool = new Stack<Cipher>();
	private static SecureRandom random  = new SecureRandom();
	private static final int POOL_SIZE = 500;
	private static boolean poolInitialized = false;
	private static byte[] salt = new byte[8];
	
	public static String decrypt(String base64EncodedEncString) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException{
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE);
		}
		boolean onDemand = false;
		int blockSize = -1;
		byte[] cipherTextToken = Base64.decodeBase64(base64EncodedEncString);
		Cipher cipher = null;
		cipher = getDecryptionCipher();
		if(cipher == null){
			onDemand = true;
			cipher = initializeDecryptionCipherOnDemand();
		}
		blockSize = cipher.getBlockSize();
		byte[] decipheredBytes = cipher.doFinal(cipherTextToken);
		byte[] actualBytes = new byte[decipheredBytes.length-blockSize];
		System.arraycopy(decipheredBytes, 8, actualBytes, 0, decipheredBytes.length-blockSize);
		String plaintext = new String(actualBytes, "UTF-8");
		if(!onDemand){
			standbyDecryptionPool.push(cipher); // Return this cipher to the pool
		}
		return plaintext;
	}
	
	
	private static synchronized void initializCipherPools(int poolSize) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchProviderException{
		if(poolInitialized){
			return;
		}
		String password = System.getProperty("PASSWORD_KEY");
		if(password == null){
			throw new RuntimeException("No password env found, expected \"PASSWORD_KEY\"");
		}
		
		Cipher cipher = null;
		PBEParameterSpec pbeParameterSpec = null;
		byte[] saltBytes = saltBytesStr.getBytes();
		System.arraycopy(saltBytes, 0, salt, 0, 8);
		SecretKeyFactory factory = SecretKeyFactory.getInstance(CIPHER_ALGO);
		KeySpec spec = new PBEKeySpec(password.toCharArray());
		SecretKey tmp = factory.generateSecret(spec);
		pbeParameterSpec = new PBEParameterSpec(salt, KEY_ITERATIONS);
		secret = new SecretKeySpec(tmp.getEncoded(), CIPHER_ALGO);
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance(CIPHER_ALGO);
			cipher.init(Cipher.ENCRYPT_MODE, secret,pbeParameterSpec);
			activeEncryptorPool.add(cipher);
		}
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance(CIPHER_ALGO);
			cipher.init(Cipher.DECRYPT_MODE, secret, pbeParameterSpec);
			activeDecryptionPool.add(cipher);
		}
		poolInitialized = true;
	}
	
	private static Cipher initializeDecryptionCipherOnDemand() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Decryption Environment: Fatal Error, pools should have been initialized by now");
		}
		logger.warn("Initializing Decryption cipher on demand, consider increasing pool size ["+POOL_SIZE+"]");
		Cipher cipher = null;
		cipher = Cipher.getInstance(CIPHER_ALGO);
		PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, KEY_ITERATIONS);
		cipher.init(Cipher.DECRYPT_MODE, secret, pbeParameterSpec);
		return cipher;
	}
	
	private static synchronized Cipher initializeEncryptionCipherOnDemand() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Encryption Environment: Fatal Error, pools should have been initialized by now");
		}
		logger.warn("Initializing Encryption cipher on demand, consider increasing pool size ["+POOL_SIZE+"]");
		Cipher cipher = null;
		cipher = Cipher.getInstance(CIPHER_ALGO);
		PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, KEY_ITERATIONS);
		cipher.init(Cipher.ENCRYPT_MODE, secret,pbeParameterSpec);
		return cipher;
	}
	
	private static synchronized Cipher getEncryptionCipher(){
		Cipher cipher = null;
		Stack<Cipher> emptyBank;
		try{
			cipher = activeEncryptorPool.pop();
		}catch(EmptyStackException es){
			logger.debug("Switching to Standby encryption pool [Current Active Pool:"+activeEncryptorPool.size()+"], StandBy Pool:"+standbyEncryptorPool.size());
			emptyBank = activeEncryptorPool;
			activeEncryptorPool = standbyEncryptorPool;
			standbyEncryptorPool = emptyBank;
		}
		return cipher;
	}
	
	private static synchronized Cipher getDecryptionCipher(){
		Stack<Cipher> emptyBank;
		Cipher cipher = null;
		try{
			cipher = activeDecryptionPool.pop();
		}catch(EmptyStackException es){
			logger.debug("Switching to Standby decryption pool exception encountered, StandBy count:"+standbyDecryptionPool.size());
			emptyBank = activeDecryptionPool;
			activeDecryptionPool = standbyDecryptionPool;
			standbyDecryptionPool = emptyBank;
		}
		return cipher;
	}
	
	/**
	 * 
	 * @param password
	 * @param trustedEntityName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchProviderException 
	 */
	public static String encrypt(String strToBeEncrypted) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException{
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE);
		}
		boolean onDemand = false;
		Cipher cipher = null;
		cipher = getEncryptionCipher();
		if(cipher == null){
			onDemand = true;
			cipher = initializeEncryptionCipherOnDemand();
		}
		byte[] randomizer = new byte[8];
		random.nextBytes(randomizer);
		byte[] actualBytes = strToBeEncrypted.getBytes("UTF-8");
		byte[] inputBytes = new byte[actualBytes.length+8];
		System.arraycopy(randomizer, 0, inputBytes, 0, randomizer.length);
		System.arraycopy(actualBytes,0 , inputBytes, randomizer.length, actualBytes.length);
		
		// Add a random to ensure uniqueness of the token across the JVM's in a culstered environment
		byte[] ciphertext = cipher.doFinal(inputBytes);
		if(!onDemand){
			standbyEncryptorPool.push(cipher); //Send the cipher environment to standby pool
		}
		return new String(Base64.encodeBase64URLSafe(ciphertext));
	}
	
	public static void main(String[] args) throws Exception{
		String data = null;
		//data = encrypt("[jhadjhajhasjhasjhasjhasjhas]");
		//save(data);
		data = read();
		System.out.println("Read:"+data);
		System.out.println("Decrypted to:"+decrypt(data));
	}
	
	private static String read() throws IOException {
		String data = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("enc.txt"))));
		data = br.readLine().trim();
		br.close();
		return data;
	}
	
	private static void save(String val) throws FileNotFoundException{
		PrintWriter pws = new PrintWriter(new File("enc.txt"));
		pws.print(val);
		pws.close();
		System.out.println("Saved:"+val);
	}
}
