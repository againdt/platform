package com.getinsured.iex.hub.platform.security;

import java.io.IOException;

import org.apache.http.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GIHttpResponseInterceptor implements HttpResponseInterceptor {
	private static Logger logger = LoggerFactory.getLogger(GIHttpResponseInterceptor.class);

	@Override
	public void process(HttpResponse response, HttpContext context)
			throws HttpException, IOException {

		if(!logger.isDebugEnabled()) {
			return;
		}
		int responseCode = response.getStatusLine().getStatusCode();
		if(responseCode != 200) {
			StringBuilder sb = new StringBuilder();
			sb.append("******************** ERROR RESPONSE HEADERS *****************************\n");
			Header[] headers = response.getAllHeaders();
			HttpClientContext clientContext = HttpClientContext.adapt(context);
			HttpRequest request = clientContext.getRequest();
			sb.append("Request:"+request.getRequestLine().getUri());
			for(int i = 0; i < headers.length; i++){


				sb.append("Response Code:"+response.getStatusLine().getStatusCode());
				sb.append("Reason Phrase:"+response.getStatusLine().getReasonPhrase());
				sb.append(headers[i].getName()+"="+headers[i].getValue()+"\n");
				sb.append("*************************************************************************\n");
			}
			logger.error(sb.toString());
		}

	}

}
