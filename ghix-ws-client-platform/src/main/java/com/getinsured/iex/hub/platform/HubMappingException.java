package com.getinsured.iex.hub.platform;

public class HubMappingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HubMappingException() {
		super();
	}

	public HubMappingException(String message, Throwable cause) {
		super(message, cause);
	}

	public HubMappingException(String message) {
		super(message);
	}

	public HubMappingException(Throwable cause) {
		super(cause);
	}

}
