package com.getinsured.iex.hub.platform.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.EmptyStackException;
import java.util.Stack;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GhixAESCipherPool{
	public static final String FORMAT = "yyyy-MMM-dd HH:mm:ss zzz"; 
	private static Logger logger = LoggerFactory.getLogger(GhixAESCipherPool.class);
	private static SecretKeySpec secret = null;
	//Encryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeEncryptorPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyEncryptorPool = new Stack<Cipher>();
	
	//Decryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeDecryptionPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyDecryptionPool = new Stack<Cipher>();
	private static SecureRandom random  = new SecureRandom();
	private static final int POOL_SIZE = 100;
	private static boolean poolInitialized = false;
	
	
	public static String decrypt(String encryptedStr) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		String plainText = null;
		long start = System.currentTimeMillis();
		String password = GhixSecretKey.getEncryptionKey();
		try{
			if(encryptedStr == null){
				throw new RuntimeException("IV and input value for decryption are required for decryption");
			}
			if(!poolInitialized){
				initializCipherPools(POOL_SIZE, password);
			}
			byte[] encData = Base64.decodeBase64(encryptedStr); 
			byte[] iv = new byte[16];
			System.arraycopy(encData, 0, iv, 0, 16);
			byte[] cipherTextToken = new byte[encData.length-16];
			System.arraycopy(encData, 16, cipherTextToken, 0, encData.length-16);
			Cipher cipher = null;
			try{
				cipher = getActiveDecryptorPool().pop();
			}catch(EmptyStackException es){
				logger.warn("Validate token failed to retrieve the cipher environment from the pool, initializing new");
				cipher = getDecryptionCipherEnv(password);
			}
			cipher.init(Cipher.DECRYPT_MODE, secret,new IvParameterSpec(iv));
			plainText = new String(cipher.doFinal(cipherTextToken), "UTF-8");
			standbyDecryptionPool.push(cipher); // Return this cipher to the pool
			int hashIndex = plainText.indexOf("#");
			if(hashIndex != -1){
				return plainText.substring(hashIndex+1);
			}
		}catch(IndexOutOfBoundsException | ArrayStoreException ae){
			throw new RuntimeException("decryption failed with :"+ae.getMessage(), ae);
		}
		logger.info("Decryption took:"+(System.currentTimeMillis()-start)+" ms");
		return plainText;
	}
	
	
	private static synchronized void initializCipherPools(int poolSize, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(poolInitialized){
			return;
		}
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			activeEncryptorPool.add(cipher);
		}
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			activeDecryptionPool.add(cipher);
		}
		poolInitialized = true;
	}
	
	private static Cipher getDecryptionCipherEnv(String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Decryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		return cipher;
	}
	
	private static synchronized Cipher getEncryptionCipherEnv(String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Encryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		return cipher;
	}
	
	private static synchronized Stack<Cipher> getActiveEncryptorPool(){
		Stack<Cipher> emptyBank;
		try{
			activeEncryptorPool.peek();
		}catch(EmptyStackException es){
			if(logger.isDebugEnabled()) {
				logger.debug("Switching Encryption Cipher Banks");
			}
			emptyBank = activeEncryptorPool;
			activeEncryptorPool = standbyEncryptorPool;
			standbyEncryptorPool = emptyBank;
		}
		return activeEncryptorPool;
	}
	
	private static synchronized Stack<Cipher> getActiveDecryptorPool(){
		Stack<Cipher> emptyBank;
		try{
			activeDecryptionPool.peek();
		}catch(EmptyStackException es){
			if(logger.isDebugEnabled()) {
				logger.debug("Switching Decryption Cipher Banks");
			}
			emptyBank = activeDecryptionPool;
			activeDecryptionPool = standbyDecryptionPool;
			standbyDecryptionPool = emptyBank;
		}
		return activeDecryptionPool;
	}
	
	/**
	 * 
	 * @param password
	 * @param trustedEntityName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 */
	public static String encrypt(String plainText) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		long start = System.currentTimeMillis();
		String returnStr = null;
		String password = GhixSecretKey.getEncryptionKey();
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE, password);
		}
		Cipher cipher = null;
		try{
			cipher = getActiveEncryptorPool().pop();
		}catch(EmptyStackException es){
			logger.warn("Generate Token Failed to get cipher env from the pool, initializing a new one");
			cipher = getEncryptionCipherEnv(password);
		}
		String plainTextIn = random.nextDouble()+"#"+plainText;
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal((plainTextIn).getBytes("UTF-8"));
		byte[] enc = new byte[iv.length+ciphertext.length];
		System.arraycopy(iv, 0, enc, 0, iv.length);
		System.arraycopy(ciphertext, 0, enc, iv.length, ciphertext.length);
		standbyEncryptorPool.push(cipher); //Send the cipher environment to standby pool
		returnStr =new String(Base64.encodeBase64URLSafe(enc));
		logger.info("Encryption took:"+(System.currentTimeMillis()-start)+" ms");
		return returnStr;
	}
	
	public static void main(String[] args) throws Exception{
		String testString = "{\"family\":{\"dependents\":[],\"specialEnroll\":{\"married\":true},\"representative\":{},\"payment\":{\"initialType\":\"CREDIT_CARD\",\"recurringType\":\"DIRECT_BILL\"},\"childApp\":false,\"primary\":{\"memberNumber\":1,\"dateOfBirth\":\"05/08/1963\",\"isNativeAmerican\":null,\"medicaidChild\":\"N\",\"isPregnant\":\"N\",\"relationshipToPrimary\":\"SELF\",\"gender\":\"Male\",\"age\":51,\"specialEnroll\":{\"married\":{\"selected\":true,\"date\":\"03022015\"}},\"tobacco\":false,\"dob\":\"05081963\",\"planInfo\":{\"planName\":\"Silver Copay Select 1\",\"planId\":568105,\"planCost\":\"525.3\",\"applicantnum\":1,\"effectivedate\":1427860800000,\"premium\":525.3,\"deductible\":\"5000\",\"totalCost\":525.3},\"bestNumber\":\"home\",\"bestContactTime\":\"Afternoon\",\"languages\":\"English\",\"fname\":\"Willi\",\"lname\":\"Becher\",\"homePhone\":\"2615550000\",\"email\":\"willibecher@uhc.pa\",\"address\":{\"state\":\"PA\",\"zip\":\"19101\",\"county\":\"PHILADELPHIA\",\"street\":\"General Delivery\",\"city\":\"Philadelphia\"},\"signaturedate\":1426272931850,\"today\":1426272931850,\"maritalStatus\":\"Single\",\"diffmailing\":false,\"noncitizen\":false,\"pcp\":{\"fname\":\"Viktor\",\"lname\":\"Frankenstein\",\"phone\":\"8005559999\",\"address\":{\"street\":\"1 Castle Way\",\"city\":\"Oberbayern\",\"zip\":\"19101\"}},\"ssn\":\"654565456\",\"esign\":{\"fname\":\"Willi\",\"lname\":\"Becher\"}},\"hsa\":{\"depositAmt\":0},\"effectivedate\":1427860800000,\"networkType\":\"PPO\",\"enrollmentId\":5},\"addtlInfo\":{\"numChildren\":0,\"hasSpouse\":false,\"anyTobacco\":false,\"anySelected\":{\"married\":true},\"selectedDoctor\":false,\"openEnrollment\":false,\"planDisplayDate\":\"04/01/2015\",\"appId\":\"696190261739\",\"hsaEligible\":false,\"hsaMax\":279.16,\"allCitizens\":true,\"anyMedicare\":false},\"payer\":{\"payerCheck\":\"other\",\"fname\":\"Willi\",\"lname\":\"Becher\",\"addrStreet\":\"7101 s central ave\",\"addrCity\":\"los angeles\",\"addrState\":\"CA\",\"addrZip\":\"90001\",\"email\":\"willibecher@uhc.pa\"},\"lastSection\":\"14-E-Signature\"}";
		
		String enc = encrypt(testString);
		System.out.println("Encrypted:"+enc.toString());
		System.out.println("Decrypted:"+decrypt( enc));
		
		//String dec = decrypt("e6rUHPy_TvDg4ctzQKDwbKAE9UBSd99UuBo-J8tRY3blQU9hb9PLUL8LbySyt3tn24clgoJchmuLYJDGaFrfcqDcO-T2jUGe_A45GyayYbMgNIexZqYca2sq-V6fnD6jFPtGirQtz44hhTLLslyEY4fSBqwbETVtD9bb_CN7Srgmyn6h5fjncKhBc0ib_02Y2rJ25bJFdMoXctOSYHG1HQ7ZLah6_SHwa-ziERo8cRvTCtIm53xxHCMZ3Sd-9Lcea1QsfZDyniFE2s0NiL4d0rCmLvMWPSGnCjvnPHWVoWy2DIAGXK8_VUOazynF8Pd6iDhkLsEf6M0YZ4QcwD5QdODxBIxnVNmb0id0KmRy47wD0oa_u52nUPB7whcM6xVNkzpaov54f2lAzZNt0gl9iJhXqVdm9KivDlBrvVSPtFnhuVAVjhHDK6fZTpt24gRSj_5MaAjbVQcmPwz_bAJeFVGjqICrhzi0EdYNKx4-HpGAYrWijmhBSr1ENJAMAadoCoS0yJVORX00hCZnwehxSJ4ViHduLiL00LduWyCQtlPaLcm3qqDoh11TCUNqtBltQNzgNVEUQdakzFb1V400knXeFlY7ln5vl7ra2i39hkYHN7giecJo2SbbQbqqfb7OcXd_tJQaFwMtNCNsGk-4bIimIThrdy2ihj3P6JqwitqGFhAdErcz_Rz0XjTdHTV1fkE-dlsEQKcIs4uvIZNtBmGVLKIV-_0XU8CGbJEJbqkBdcqGrzSN0pocG05st_PIGOWB7QmxEZjcR0TjgeA5ScxOiW5XbvyROnYav2P-xMQJ0zX9T1sY0AqTLBdpvS3FT46dYCEsAhQm3wkklQEFZxHRVc46eSrM8hHORdIp_3gk29ZwkUBx6GiJk1UG-oV3-xStf12uLGzzqo8Te8anHzQYHS-Vm8cM9CG_G7TtskJO8fMhcsc6p8b24TzWEOoZdYHDRvgkNp0nVb461QG6sG-IqR0OMqxyQ1HHMmpyxsUjgF6FqkiaaKstcH3HQtDkcAtNYlfUxCEzQs_QSR7Ccv5UN_UodrUGPYh07hcOsRzBTB8EGA3AJZd-u_ZVQQM1-THXcaJdJ3Wtr-98I7qqPIR8ZnSpnRRcY6quMxgSQoWoGPpKD0ByQLPLCUR-Zw_HMAdaUM6OzVtZ_yYxQhUxlALkBAiQELUvbpHRKpH7MCbTcnJGzLEXy8EFxw_sDU-Ele4Jg2bJfLhZRmW5CGdNO_UO2c-SAMAPKQVce23DrhzH0ju7O2HVffw9DdCaCd0dC8mPAuv-DaLpEszdiiBOtL6ei9POC9CI_HBIpzl1mwj2xIVmbzZeQ0IFCfnkQNvtdTBiSeL-Oam4soJb5Rt-3TpNtW4ViZNa5zAs1EOkUE48J6Up6bWSgDw35XEHhnBhrj5KK324PlsBOEnr0_GO-hSxLElWcSaDuIfAykZzd9hmcuHCIG2vl6gmkTbSaRee9OqTbA942G463678WpVK1F9piNwL7gmuE37qXeoGvKzN7ypkA3m0urdfxRWPvsfYGyk2bTar2vv-v4-ia6dze8uYJumyj-WSjBrW_GJQfMQBk5eV6E_7F8z4lkO10dPvgPIh5XKkl3-P2lK-72mTWik5Z3DBXbOdFT3VRe9-atOnDmxJgMRMxRkFP7PBTQkI31A5Cd8d09IDsSfQsgvvVIVrsIcH48nzHOvG0VhuLf1lUJYo1G7-6xBwjir7N7NpEMMi5t838EfcyooiNluwr6FG_rLYUgaIEgCyfG1n2smzjcKA3c8uTKCp5tSrGFSI_S-gCF0_CmBrTxleVJlzYcglFDP15pSBEiWXjG9SLu-tEcxizUd2SleQ4RkGjyEpMAgXRV6zJf1aOaXc1af37_WgrG0zFuBgLpipIgaal9W_Zs5rB1CLrxRFk6DmCqNqiBVCQk7rCp_GM2EmOJmei8pHGJHbycxeXdk28jlZPGmvBMTiXWFUaxLm7Zpukw8NF_DArcz-2Cnhj-9YsbEtgvs-SBPnr57r9irpAd3cDb7sGCWT56SUXKhrA15StyD-mSOo1c02drACkHDzwTbdVxLH6aOGzuB0MAqn7gHdlPU3P0N3GOBU6Zxsfr68PY_lHKoEMt-FKvU-3c0R3p0ofNu9HGOo2BYVlh2M6jE5dQoewXbhHEnPJ84pOtBDfNodSrcO9znh6VuJx355ll0wCyNVtFKlfxqAzfW8lnPw9FV8Gmv4TzXiIC5WIeTDh2xycskggA0Yp14STP6Bi-wPbI_AGtICzY8NY1Aco3SWFRrzcSFlMs4d670GqL8jh36LTMcq2H3uJ075pyrC5DE-XfrIqvnU031bnodeJZNs9aSUN8F5ZWiHrcrmKgK5NemFyo3bTjvROF1phnS2CQ-g8QZX0bUoUnt9");
		//System.out.println(dec);
		/*for(int i = 0; i < 10; i++){
			String enc = encrypt(testString+i);
			System.out.println("Encrypted:"+enc.toString());
			System.out.println("Decrypted:"+decrypt( enc));
		}*/
	}
}