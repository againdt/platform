/**
 * Util class used across GHIX application.
 * @author venkata_tadepalli
 * @since 12/08/2012
 */
package com.getinsured.iex.hub.platform.utils;

import java.util.Date;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.ISO8601DateConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentCollectionConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentMapConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentSortedMapConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentSortedSetConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernateProxyConverter;
import com.thoughtworks.xstream.hibernate.mapper.HibernateMapper;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;


public class GhixUtils {
	private static XStream xStreamHibernateXmlObject;
	private static XStream xStreamHibernateJsonObject;
	private static XStream xStreamGenericObject;
	private static XStream xStreamStaxObject;
	public static enum EMAIL_STATS {AFFILIATE_ID,STATE_CODE,EXCHANGE_URL,FLOW_ID,TENANT_ID }

	protected static final String JSON_DATE_FORMAT = "MMM dd, yyyy hh:mm:ss a";

			

    /**
     * XStream's Hibernate package to serialize hibernate objects to xml
     * 
     * @author Pratap panda
   	 * @since 01/03/2013
     * @return
     * @throws GIException
     */
    public static XStream xStreamHibernateXmlMashaling(){
    	if(xStreamHibernateXmlObject != null){
    		return xStreamHibernateXmlObject;
    	}
    	xStreamHibernateXmlObject = new XStream() {
			  protected MapperWrapper wrapMapper(final MapperWrapper next) {
			    return new HibernateMapper(next);
			  }
		};
		xStreamHibernateXmlObject.registerConverter(new HibernateProxyConverter());
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentCollectionConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentMapConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentSortedMapConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentSortedSetConverter(xStreamHibernateXmlObject.getMapper()));
		
		return xStreamHibernateXmlObject;
    }
    
    /**
     * XStream's Hibernate package to serialize hibernate objects to JSON
     * 
     * @author Pratap panda
   	 * @since 01/03/2013
     * @return
     * @throws GIException
     */
    public static synchronized XStream xStreamHibernateJSONMashaling(){
    	if(xStreamHibernateJsonObject != null){
    		return xStreamHibernateJsonObject;
    	}
    	xStreamHibernateJsonObject = new XStream(new JettisonMappedXmlDriver()) {
			  protected MapperWrapper wrapMapper(final MapperWrapper next) {
			    return new HibernateMapper(next);
			  }
		};
		xStreamHibernateJsonObject.registerConverter(new HibernateProxyConverter());
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentCollectionConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentMapConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentSortedMapConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentSortedSetConverter(xStreamHibernateJsonObject.getMapper()));
		
		return xStreamHibernateJsonObject;
    }
    
    public synchronized static XStream getXStreamGenericObject(){
    	if(xStreamGenericObject != null){
    		return xStreamGenericObject;
    	}
    	xStreamGenericObject = new XStream();
		return xStreamGenericObject;
    }
    
    public static ISO8601DateConverter DATE_CONVERTER = new ISO8601DateConverter() { 
    	@Override 
    	public boolean canConvert(Class type) { 
    		return Date.class.isAssignableFrom(type); 
    	} 
    }; 
    public synchronized static XStream getXStreamStaxObject(){
    	if(xStreamStaxObject != null){
    		return xStreamStaxObject;
    	}
    	xStreamStaxObject = new XStream(new StaxDriver());
    	
    	xStreamStaxObject.addDefaultImplementation(java.sql.Date.class,java.util.Date.class); 
    	xStreamStaxObject.addDefaultImplementation(java.sql.Timestamp.class,java.util.Date.class); 
    	xStreamStaxObject.addDefaultImplementation(java.sql.Time.class,java.util.Date.class); 
    	xStreamStaxObject.registerConverter(DATE_CONVERTER); 
    	
		return xStreamStaxObject;
    }
    
    
}