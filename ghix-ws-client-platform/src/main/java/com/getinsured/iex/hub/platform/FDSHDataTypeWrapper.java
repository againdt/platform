package com.getinsured.iex.hub.platform;

public interface FDSHDataTypeWrapper {
	Object getSystemRepresentation();
}
