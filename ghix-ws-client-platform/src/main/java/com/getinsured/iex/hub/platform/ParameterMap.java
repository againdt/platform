package com.getinsured.iex.hub.platform;


public class ParameterMap {
	
	private Parameter source;
	private Parameter target;
	
	public void setParameter(Parameter param) throws HubMappingException{
		if(param.isTarget()){
			this.setTarget(param);
		}else{
			this.setSource(param);
		}
	}
	
	public Parameter getSource() {
		return source;
	}
	
	public void setSource(Parameter source) throws HubMappingException {
		if(this.source != null){
			throw new HubMappingException("Invalid Map,Source with name ["+this.source.getName()+"] already available");
		}
		if(source.isTarget()){
			throw new HubMappingException("Invalid,Parameter ["+source.getName()+"] is of type:\"target\" expecting type \"source\"");
		}
		this.source = source;
	}
	
	public Parameter getTarget() {
		return target;
	}
	
	public void setTarget(Parameter target) throws HubMappingException {
		if(this.target != null){
			throw new HubMappingException("Invalid Map,Source with name ["+this.target.getName()+"] already available");
		}
		if(!target.isTarget()){
			throw new HubMappingException("Invalid,Parameter is of type:source expecting type \"target\"");
		}
		this.target = target;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ParameterMap other = (ParameterMap) obj;
		if (source == null) {
			if (other.source != null) {
				return false;
			}
		} else if (!source.equals(other.source)) {
			return false;
		}
		if (target == null) {
			if (other.target != null) {
				return false;
			}
		} else if (!target.equals(other.target)) {
			return false;
		}
		return true;
	}

	public boolean acceptInput(String name) {
		if(this.source.getName().equals(name)){
			return true;
		}
		return false;
	}

	@SuppressWarnings("rawtypes")
	public Class getTargetNamespace() {
		return this.target.getNamespace();
	}
	
}
