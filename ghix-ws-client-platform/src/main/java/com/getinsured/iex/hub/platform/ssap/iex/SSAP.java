package com.getinsured.iex.hub.platform.ssap.iex;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SSAP {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SSAP.class);

	private Date applicationDate = null;
	private long applicationId = 0l;
	private boolean financialAssistanceIndicator = false;
	private List<SSAPTaxHouseHold> taxHouseHolds = null;
	
	
	public SSAP(String ssapJSON) throws ParseException{
		JSONParser parser = new JSONParser();
		JSONObject ssap = (JSONObject) parser.parse(ssapJSON);
		
		Object tmp = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
		tmp = ssap.get("applicationDate");
		if(tmp == null){
			LOGGER.warn("No application Date provided with input SSAP data");
		}else{
			try{
				this.applicationDate = sdf.parse((String)tmp);
			}catch(java.text.ParseException pe){
				LOGGER.warn("Invalid/Unknown Date format received with SSAP");
			}
		}
		tmp = ssap.get("applyingForFinancialAssistanceIndicator");
		if(tmp != null){
			this.financialAssistanceIndicator = (Boolean)tmp;
		}
		tmp = ssap.get("ssapApplicationId");
		if(tmp != null){
			this.applicationId = Long.parseLong((String)tmp);
		}
		
		tmp = ssap.get("taxHousehold");
		if(tmp != null){
			JSONArray households = (JSONArray) tmp;
			if(!households.isEmpty()){
				this.taxHouseHolds = new ArrayList<SSAPTaxHouseHold>();
				for(Object obj: households){
					this.taxHouseHolds.add(new SSAPTaxHouseHold((JSONObject)obj));
				}
			}
		}
	}

	public Iterator<SSAPTaxHouseHold> houseHoldIterator(){
		if(this.taxHouseHolds == null || this.taxHouseHolds.isEmpty()){
			return null;
		}
		return (Iterator<SSAPTaxHouseHold>)this.taxHouseHolds.iterator();
	}
	
	public Date getApplicationDate() {
		
		if(applicationDate != null){
			return new Date(applicationDate.getTime());
		}
		
		return null;
	}


	public boolean isFinancialAssistanceIndicator() {
		return financialAssistanceIndicator;
	}

	public long getApplicationId() {
		return applicationId;
	}
}
