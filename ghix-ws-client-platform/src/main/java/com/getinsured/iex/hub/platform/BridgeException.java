package com.getinsured.iex.hub.platform;

public class BridgeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BridgeException() {
	}

	public BridgeException(String message) {
		super(message);
	}

	public BridgeException(Throwable cause) {
		super(cause);
	}

	public BridgeException(String message, Throwable cause) {
		super(message, cause);
	}

}
