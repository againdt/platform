package com.getinsured.iex.hub.platform;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.stereotype.Component;

@Component("DataSourceRegistry")
public class DatasourceRegistry {

	private Map<String, DataSource> dataSourceMap = null;
	
	private String ftiDs = null;
	private String defaultDs = null;
	
	public void setDataSourceMap(Map<String, DataSource> dsMap){
		this.dataSourceMap = dsMap;
	}
	
	public Map<String, DataSource> getDataSourceMap(){
		return this.dataSourceMap;
	}
	
	public DatasourceRegistry() {
		
	}
	
	public DataSource getDataSource(String name){
		return this.dataSourceMap.get(name);
	}

	public String getFtiDs() {
		return ftiDs;
	}

	public void setFtiDs(String ftiDs) {
		this.ftiDs = ftiDs;
	}

	public String getDefaultDs() {
		return defaultDs;
	}

	public void setDefaultDs(String defaultDs) {
		this.defaultDs = defaultDs;
	}

	public DataSource getDefaultDatasource() {
		if(this.dataSourceMap == null || this.dataSourceMap.isEmpty()){
			return null;
		}
		return this.dataSourceMap.get(defaultDs);
	}
	
	public DataSource getFtidataSource(){
		if(this.dataSourceMap == null || this.dataSourceMap.isEmpty()){
			return null;
		}
		return this.dataSourceMap.get(ftiDs);
	}

}
