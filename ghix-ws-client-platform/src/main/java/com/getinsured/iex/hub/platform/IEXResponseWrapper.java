package com.getinsured.iex.hub.platform;

import java.util.HashMap;
import java.util.Map;

public class IEXResponseWrapper {
	
	private Map<String, Object> context;
	
	public IEXResponseWrapper(){
		
	}
	
	public Map<String, Object> getContext(){
		if(this.context == null){
			this.context = new HashMap<String, Object>();
		}
		return this.context;
	}
	
	public void addContextParameter(String name, String value){
		if(this.context == null){
			context = new HashMap<String, Object>();
		}
		this.context.put(name, value);
	}
	
	public Object getContextParameter(String paramName){
		if(this.context != null){
			return this.context.get(paramName);
		}
		return null;
	}

}
