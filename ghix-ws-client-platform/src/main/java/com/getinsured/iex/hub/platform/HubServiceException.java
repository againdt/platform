package com.getinsured.iex.hub.platform;

import java.util.ArrayList;
import java.util.List;

public class HubServiceException extends RuntimeException {

	/**
	 * 
	 */
	private String errorCode = "NONE";
	private List<Schedule> serviceDownTimeSchedule;
	private static final long serialVersionUID = 1L;

	public HubServiceException() {
		super();
	}

	public HubServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public HubServiceException(String message) {
		super(message);
	}

	public HubServiceException(Throwable cause) {
		super(cause);
	}
	
	public String getErrorCode(){
		return this.errorCode;
	}
	
	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}

	public void setServiceDownTimeSchedule(
			List<Schedule> serviceDownTimeSchedule) {
		this.serviceDownTimeSchedule = serviceDownTimeSchedule;
		
	}
	
	// Never return a null object, Empty is OK
	public List<Schedule> getServiceDownTimeSchedule(){
		if(this.serviceDownTimeSchedule == null){
			this.serviceDownTimeSchedule = new ArrayList<Schedule>();
		}
		return this.serviceDownTimeSchedule;
	}

}
