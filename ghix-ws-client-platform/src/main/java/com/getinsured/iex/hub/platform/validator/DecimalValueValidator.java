package com.getinsured.iex.hub.platform.validator;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;

/**
 * Generic validator for hub request fields
 * 
 * @author Vishaka Tharani
 * @since 5-Mar-2014
 *
 */
public class DecimalValueValidator extends Validator {
	private static Logger logger = LoggerFactory.getLogger(DecimalValueValidator.class);
	private double minValue = -1.0d;
	private double maxValue = -1.0d;
	private int decimalDigits = -1;
	
	
	public void addContextParameter(String name, Object value){
		super.addContextParameter(name, value);
		String tmp = null;
		if(name != null && value != null){
			if(name.equalsIgnoreCase("minValue")){
				tmp = (String)value;
				this.minValue = Double.parseDouble(tmp);
			}else if(name.equalsIgnoreCase("maxValue")){
				tmp = (String)value;
				this.maxValue = Double.parseDouble(tmp);
			}else if(name.equalsIgnoreCase("decimalDigits")){
				tmp = (String)value;
				this.decimalDigits = Integer.parseInt(tmp);
			}
		}
	}
	
	public void validate(String name, Object value)	throws InputDataValidationException {
		
		if(this.checkForNullOrEmpty(name, value)){
			logger.info("Skipping Decimal Number validation, for "+name+" since it is null or empty");
			return;
		}
		try{
			//Check for required validation
			BigDecimal fieldValue = null;
			
			/**
			 * This null check is redundant since its
			 * already checked in checkForNullOrEmpty() method
			 */
			
			//if(value != null){
				fieldValue = BigDecimal.valueOf(Double.parseDouble(value.toString()));
				validateNumber(name, fieldValue);
			//}
		}
		catch(ClassCastException e){
			throw new InputDataValidationException("Invalid value for attribute ",e);
		}
		catch(NumberFormatException e){
			throw new InputDataValidationException(name + " should be a number",e);
		}
		
	}
	
	private void validateNumber(String name, BigDecimal fieldValue)	throws InputDataValidationException {
		
		if(decimalDigits != -1 && fieldValue.scale() > decimalDigits){
			throw new InputDataValidationException("Value of " + name + " should have maximum " + decimalDigits + " decimal digits");
		}
		
		if((minValue != -1.0 && fieldValue.doubleValue() < minValue) || (maxValue != -1.0 && fieldValue.doubleValue()  > maxValue)){
			throw new InputDataValidationException("Value of " + name + " should be between "+ minValue +" and "
					+ maxValue );
		}
	}
}
