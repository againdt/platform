package com.getinsured.iex.hub.platform.transformers;

import com.getinsured.iex.hub.platform.AbstractTransformer;
import com.getinsured.iex.hub.platform.ObjectTransformationException;

public class StringToBooleanTransformer extends AbstractTransformer {

	@Override
	public Object transform(Object obj) throws ObjectTransformationException {
		if(obj instanceof Boolean){
			return obj;
		}else if(obj instanceof Integer){
			Integer i = (Integer)obj;
			return i == 1;
		}else if(!(obj instanceof String)){
			throw new ObjectTransformationException("Can not convert "+obj.getClass().getName()+" to boolean");
		}
		
		String tmp = (String)obj;
		if(tmp.equalsIgnoreCase("Y") || tmp.equalsIgnoreCase("YES") || tmp.equalsIgnoreCase("1") || tmp.equalsIgnoreCase("true")){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public static void main(String[] args) throws ObjectTransformationException{
		StringToBooleanTransformer trnsformer = new StringToBooleanTransformer();
		System.out.println("Expected true:"+trnsformer.transform(Boolean.TRUE));
		System.out.println("Expected true:"+trnsformer.transform("Y"));
		System.out.println("Expected False:"+trnsformer.transform("N"));
		System.out.println("Expected False:"+trnsformer.transform(Integer.valueOf(5)));
		System.out.println("Expected true:"+trnsformer.transform(Integer.valueOf(1)));
		System.out.println("Expected False:"+trnsformer.transform(Integer.valueOf(0)));
		System.out.println("Expected true:"+trnsformer.transform("YES"));
		System.out.println("Expected False:"+trnsformer.transform("No"));
	}
}
