package com.getinsured.iex.hub.platform.ssap.iex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SSAPTaxHouseHold{
	private boolean applyingForCashBenefitsIndicator = false;

	private List<HouseholdMember> householdMembers = null;
	
	public SSAPTaxHouseHold(JSONObject taxHouseHold) {
		if(taxHouseHold != null){
			this.extractHouseholdMembers(taxHouseHold);
		}
	}
	
	private void extractHouseholdMembers(JSONObject taxHouseholdItem) {
		Object tmp = taxHouseholdItem.get("applyingForCashBenefitsIndicator");
		if( tmp != null)
		{
			this.applyingForCashBenefitsIndicator = (Boolean)tmp;
		}
		
		JSONArray members = (JSONArray) taxHouseholdItem.get("householdMember");
		if(!members.isEmpty()){
			this.householdMembers = new ArrayList<HouseholdMember>();
			for(Object obj: members){
				this.householdMembers.add(new HouseholdMember((JSONObject)obj));
			}
		}
		
	}

	public Iterator<HouseholdMember> iterator(){
		if(this.householdMembers == null || this.householdMembers.isEmpty()){
			return null;
		}
		return (Iterator<HouseholdMember>)this.householdMembers.iterator();
	}

	public boolean isApplyingForCashBenefitsIndicator() {
		return applyingForCashBenefitsIndicator;
	}
	
}
