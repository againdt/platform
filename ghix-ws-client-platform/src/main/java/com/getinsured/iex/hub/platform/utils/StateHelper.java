package com.getinsured.iex.hub.platform.utils;

import java.util.ArrayList;
import java.util.List;

public class StateHelper {

	/**
	 * Objects are returned of type State
	 */
	public class State {
		private String code;
		private String name;

		protected State(String code, String name) {
			this.code = code;
			this.name = name;
		}
		
		public State(String code){
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			State other = (State) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (code == null) {
				if (other.code != null) {
					return false;
				}
			} else if (!code.equals(other.code)) {
				return false;
			}
			return true;
		}
		
		private StateHelper getOuterType() {
			return StateHelper.this;
		}
	}

	private List<State> states = null;
	
	private static final int TOTAL_STATES = 51;
	
	public StateHelper() {
		states = new ArrayList<State>(TOTAL_STATES);
		states.add(new State("AL", "Alabama"));
		states.add(new State("AK", "Alaska"));
		states.add(new State("AZ", "Arizona"));
		states.add(new State("AR", "Arkansas"));
		states.add(new State("CA", "California"));
		states.add(new State("CO", "Colorado"));
		states.add(new State("CT", "Connecticut"));
		states.add(new State("DE", "Delaware"));
		states.add(new State("DC", "Dist of Columbia"));
		states.add(new State("FL", "Florida"));
		states.add(new State("GA", "Georgia"));
		states.add(new State("HI", "Hawaii"));
		states.add(new State("ID", "Idaho"));
		states.add(new State("IL", "Illinois"));
		states.add(new State("IN", "Indiana"));
		states.add(new State("IA", "Iowa"));
		states.add(new State("KS", "Kansas"));
		states.add(new State("KY", "Kentucky"));
		states.add(new State("LA", "Louisiana"));
		states.add(new State("ME", "Maine"));
		states.add(new State("MD", "Maryland"));
		states.add(new State("MA", "Massachusetts"));
		states.add(new State("MI", "Michigan"));
		states.add(new State("MN", "Minnesota"));
		states.add(new State("MS", "Mississippi"));
		states.add(new State("MO", "Missouri"));
		states.add(new State("MT", "Montana"));
		states.add(new State("NE", "Nebraska"));
		states.add(new State("NV", "Nevada"));
		states.add(new State("NH", "New Hampshire"));
		states.add(new State("NJ", "New Jersey"));
		states.add(new State("NM", "New Mexico"));
		states.add(new State("NY", "New York"));
		states.add(new State("NC", "North Carolina"));
		states.add(new State("ND", "North Dakota"));
		states.add(new State("OH", "Ohio"));
		states.add(new State("OK", "Oklahoma"));
		states.add(new State("OR", "Oregon"));
		states.add(new State("PA", "Pennsylvania"));
		states.add(new State("RI", "Rhode Island"));
		states.add(new State("SC", "South Carolina"));
		states.add(new State("SD", "South Dakota"));
		states.add(new State("TN", "Tennessee"));
		states.add(new State("TX", "Texas"));
		states.add(new State("UT", "Utah"));
		states.add(new State("VT", "Vermont"));
		states.add(new State("VA", "Virginia"));
		states.add(new State("WA", "Washington"));
		states.add(new State("WV", "West Virginia"));
		states.add(new State("WI", "Wisconsin"));
		states.add(new State("WY", "Wyoming"));
	}

	public List<State> getAllStates() {
		return states;
	}
	
	public String getStateName(String code){
		State st = new State(code);
		int ind = this.states.indexOf(st);
		if( ind >= 0) {
			return this.states.get(ind).getName();
		}
		return null;
	}
}