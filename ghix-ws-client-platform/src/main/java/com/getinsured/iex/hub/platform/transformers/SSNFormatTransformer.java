package com.getinsured.iex.hub.platform.transformers;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.AbstractTransformer;
import com.getinsured.iex.hub.platform.ObjectTransformationException;

public class SSNFormatTransformer extends AbstractTransformer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractServiceHandler.class);
	
	@Override
	public Object transform(Object obj) throws ObjectTransformationException {
		
		if(obj == null){
			return null;
		}
		
		if(!(obj instanceof String)){
			throw new ObjectTransformationException("Invalid source for transformation, expected String");
		}
		
		String sourceSSN = (String)obj;
		
		//Check for source format
		String sourceFormats = this.getContextValue("sourceFormats");
		if(sourceFormats != null){
			String [] formats = sourceFormats.split(",");
			boolean matched = false;
			for(String format : formats){
				Pattern p = Pattern.compile(format);
				Matcher m = p.matcher(sourceSSN);
				if(m.matches()){
					matched = true;
					break;
				}
			}
			
			if(!matched){
				throw new ObjectTransformationException("SSN value is not in one of the expected formats " + Arrays.toString(formats));
			}
		}
		
		return transformSSN (sourceSSN);
	}
	
	private String transformSSN(String sourceSSN) throws ObjectTransformationException{
		
		String targetFormat = this.getContextValue("targetFormat");
		if(targetFormat == null){
			throw new ObjectTransformationException("Can not transform, target format not provided");
		}
		
		StringBuilder builder;
		
		if(targetFormat.equals("\\d{9}")){
			
			builder = new StringBuilder();
			for(int i = 0; i  < sourceSSN.length(); i++){
				char ch = sourceSSN.charAt(i);
				if(Character.isDigit(ch)){
					builder.append(ch);
				}
			}
			Pattern p = Pattern.compile(targetFormat);
			Matcher m = p.matcher(builder);
			if(m.matches()){
				return builder.toString();
			}
			throw new ObjectTransformationException("Failed to transform the input SSN [XXXXXXXX Masked] for format:"+targetFormat);
		}
		return null;
	}
	
	public static void main(String[] args) throws ObjectTransformationException{
		SSNFormatTransformer ssnconv = new SSNFormatTransformer();
		ssnconv.setContextValue("sourceFormats", "[0-9]{3}-[0-9]{2}-[0-9]{4},\\d{9}");
		ssnconv.setContextValue("targetFormat", "\\d{9}");
		//String x = (String) ssnconv.transform("");
		//LOGGER.debug(x);
	}

}
