package com.getinsured.iex.hub.platform.repository.fti;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;

import org.springframework.stereotype.Repository;

import com.getinsured.iex.hub.platform.models.GIHUBResponse;

@Repository
public interface HubFtiResponseRepository extends JpaRepository<GIHUBResponse, Long> {

}
