package com.getinsured.iex.hub.platform;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractTransformer implements Transformer {
	
	private Map<String, String> context;
	
	public void setContext(Map<String, String> context){
		this.context = context;
	}
	
	public String getContextValue(String key){
		if(this.context == null){
			return null;
		}
		return this.context.get(key);
	}
	
	public void setContextValue(String key, String value){
		if(this.context == null){
			this.context = new HashMap<String, String>();
		}
		this.context.put(key, value);
	}

}
