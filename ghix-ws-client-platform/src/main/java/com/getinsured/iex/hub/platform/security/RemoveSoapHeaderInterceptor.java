package com.getinsured.iex.hub.platform.security;

import java.io.IOException;
import java.util.Arrays;

import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Removes {@code Transfer-Encoding} and {@code Content-Length} headers
 * from the request if present.
 */
@Component
public class RemoveSoapHeaderInterceptor implements HttpRequestInterceptor {
	private Logger logger = LoggerFactory.getLogger(getClass());

    public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
    	Header[] x = request.getAllHeaders();

    	if(logger.isDebugEnabled())
        {
          Arrays.stream(x).forEach(h -> logger.debug("Received Headers: {} Value: {} ",
              h.getName(), h.getValue()));
    	}

        if (request instanceof HttpEntityEnclosingRequest) {
            if (request.containsHeader(HTTP.TRANSFER_ENCODING)) {
                request.removeHeaders(HTTP.TRANSFER_ENCODING);
            }
            if (request.containsHeader(HTTP.CONTENT_LEN)) {
                request.removeHeaders(HTTP.CONTENT_LEN);
            }
        }
    }
}
