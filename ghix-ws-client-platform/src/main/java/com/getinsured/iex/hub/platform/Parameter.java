package com.getinsured.iex.hub.platform;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class Parameter {
	
	private static final String RAW_TYPES = "rawtypes";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Parameter.class);
	@SuppressWarnings(RAW_TYPES)
	private Class namespace;
	private String name;
	private Validator validator;
	private Method getter;
	private Method setter;
	private String type;
	private boolean isTarget = false;
	private Transformer transformer;
	private boolean isRequired = false;
	private String suffix = null;
	
	public Parameter(String namespace, String name, String type) throws HubMappingException{
		
		@SuppressWarnings(RAW_TYPES)
		Class cls = null;
		this.type = type;
		if(namespace != null){
			try {
				cls = HubServiceBridge.getClass(namespace);
				this.suffix = Character.toUpperCase(name.charAt(0))+name.substring(1);
			} catch (Exception e) {
				throw new HubMappingException("failed to load the class for namespace:"+namespace,e);
			}
		}
		this.namespace = cls;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Object getValue(Object target) throws HubMappingException {
		Exception ex = null;
		Object obj  = null;
		Object[] param = {};
		try{
			if(this.getter == null){
				this.getter = this.getReadAccessor(this.namespace,suffix);
			}
			
			obj = this.getter.invoke(target, param);
		}catch(IllegalAccessException ile){
			ex = ile;
		}catch(IllegalArgumentException iae){
			ex = iae;
		}catch(InvocationTargetException ite){
			ex = ite;
		}
		if(ex != null){
			throw new HubMappingException("Failed to get the value for parameter "+this.name+" with namespace "+this.namespace.getCanonicalName(),ex);
		}
		return obj;
	}
	
	public void setValue(Object target, Object value) throws HubMappingException{
		String tmp = null;
		try{
			
			value = transformAndValidate(value);
			
			if(this.setter == null){
				this.setter = this.getWriteAccessor(this.namespace,this.suffix);
			}
			if(this.type.equalsIgnoreCase("java.lang.Boolean") && value instanceof String){

				tmp = (String)value;
				if(!tmp.equalsIgnoreCase("true") && !tmp.equalsIgnoreCase("false")){
					throw new HubMappingException("Expected \"true/false\" for boolean type["+this.name+" in namespace:"+this.namespace+"] found ");
				}
				value = Boolean.parseBoolean((String)value);

			}
			this.setter.invoke(target, value);
		}catch(Exception e){
			
			String className = "";
			
			if(value != null)
			{
				className = "[Class Type:" + value.getClass().getName() + "]";
			}
			throw new HubMappingException("Failed to set value "+ className +" for parameter "+this.name+" with namespace "+this.namespace.getCanonicalName(),e);
		}
	}
	
	private Object transformAndValidate(Object value) throws ObjectTransformationException, InputDataValidationException{
		
		if(this.transformer != null && value != null){
			value = this.transformer.transform(value);
		}
		if(this.validator != null){
			this.validator.validate(name, value);
		}
		
		return value;
		
	}
	
	public Validator getValidator() {
		return validator;
	}
	
	public void setValidator(Validator validator) {
		this.validator = validator;
		if(this.validator != null){
			this.isRequired = validator.isRequired();
		}
	}
		
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@SuppressWarnings({ "unchecked", RAW_TYPES })
	private Method getReadAccessor(Class cls, String getter) throws HubMappingException {
		String tmp = getter;
		if(this.type.equalsIgnoreCase("java.lang.Boolean")){
			tmp = "is"+getter;
		}else{
			tmp = "get"+getter;
		}
		LOGGER.debug("looking for a read accessor:"+tmp+" from "+cls.getName());
		Class[] args = null;
		try{
			return cls.getMethod(tmp, args);
		}catch(Exception e){
			throw new HubMappingException("Invalid Map,failed to find read accessor method ["+tmp+"]", e);
		}
	}
	
	@SuppressWarnings({ "unchecked", RAW_TYPES })
	private Method getWriteAccessor(Class cls, String setter) throws HubMappingException{
		Class arg = null;
		if(this.type.equalsIgnoreCase("java.lang.Boolean")){
			arg = Boolean.TYPE;
		}
		else if(!this.type.equalsIgnoreCase("java.lang.String")){
			try {
				arg = Class.forName(this.type);
			} catch (ClassNotFoundException e) {
				throw new HubMappingException("Invalid type specified " + this.type,e);
			}
		}
		else{
			arg = java.lang.String.class;
		}
		Class[] args = {arg};
		try{
			return cls.getMethod("set"+setter, args);
		}catch(Exception e){
			throw new HubMappingException("Invalid Map,failed to find write accessor method ["+"set"+setter+"]", e);
		}
	}
	
	public boolean isTarget() {
		return isTarget;
	}
	
	public void setTarget(boolean isTarget) throws HubMappingException {
		this.isTarget = isTarget;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Parameter other = (Parameter) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (namespace == null) {
			if (other.namespace != null) {
				return false;
			}
		} else if (!namespace.equals(other.namespace)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	@SuppressWarnings(RAW_TYPES)
	public Class getNamespace() {
		return this.namespace;
	}
	
	public String toString(){
		return "Namespace:"+this.namespace+" Name:"+this.name+" Type:"+this.type;
	}

	public void setTransformer(Transformer transformer) {
		this.transformer = transformer;
	}
	
	public Transformer getTransformer() {
		return transformer;
	}
	
	public boolean isRequired(){
		return this.isRequired;
	}
	
}
