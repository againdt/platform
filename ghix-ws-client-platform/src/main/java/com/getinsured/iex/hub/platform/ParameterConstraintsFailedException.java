package com.getinsured.iex.hub.platform;

public class ParameterConstraintsFailedException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ParameterConstraintsFailedException(){
		super();
	}
	public ParameterConstraintsFailedException(String message){
		super(message);
	}
	

}
