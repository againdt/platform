package com.getinsured.iex.hub.platform;

public class ObjectTransformationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ObjectTransformationException() {
		super();
	}

	public ObjectTransformationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectTransformationException(String message) {
		super(message);
	}

	public ObjectTransformationException(Throwable cause) {
		super(cause);
	}
	

}
