package com.getinsured.iex.hub.platform.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.platform.utils.StateHelper;

/**
 * State validator for hub request fields
 * 
 * @author Vishaka Tharani
 * @since 11-Feb-2014
 *
 */
public class StateValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StateValidator.class);
	
	
	/** Validates a field using following rules
	 *  1. isRequired - If "Y", the value is mandatory
	 *  2. minLength - Minimum length of field
	 *  3. maxLength - Maximum length of field
	 *  
	 *  @param name - Name of the field
	 *  @param value - Value for the parameter
	 *  
	 *  @throws InputDataValidationException - If a validation fails
	 *  
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		if(this.checkForNullOrEmpty(name, value)){
			LOGGER.info("Skipping Length validation, for "+name+" is null or empty");
			return;
		}
		this.checkForMinAndMaxLen(name, value);
		if(!(value instanceof String)){
			throw new InputDataValidationException("Incompatible value ["+value.getClass().getName()+"] for STATE validation expected String");
		}
		StateHelper stateHelper = new StateHelper();
		//State matching
		if(stateHelper.getStateName((String)value) == null){
			throw new InputDataValidationException("Invalid state code: ");
		}
	}
}
