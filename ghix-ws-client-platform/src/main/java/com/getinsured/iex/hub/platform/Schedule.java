package com.getinsured.iex.hub.platform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class Schedule implements JSONAware {
	
	public static final String SERVICE_SCHEDULE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss z";
	private SimpleDateFormat sdf = new SimpleDateFormat(SERVICE_SCHEDULE_FORMAT);
	private Date serviceDownStartTime;
	private Date serviceDownEndTime;
	private long lastExecutionTime;
	private int maxRetryCount;
	private int delayBetweenExecution;
	
	public boolean isServiceAvailable() throws ServiceUnAvailableException{
		if(this.serviceDownEndTime == null || this.serviceDownStartTime == null){
			return true;
		}
		long timeNow = System.currentTimeMillis();
		Date dt = new Date(timeNow);
		if(dt.after(this.serviceDownStartTime) && dt.before(this.serviceDownEndTime)){
			throw new ServiceUnAvailableException("Scheduled Down Time "+this);
		}
		return true;
	}
	
	public boolean isServiceAvailable(long time){
		if(this.serviceDownEndTime == null || this.serviceDownStartTime == null){
			return true;
		}
		Date dt = new Date(time);
		if(dt.after(this.serviceDownStartTime) && dt.before(this.serviceDownEndTime)){
			return false;
		}
		return true;
	}
	
	public Date getServiceDownStartTime() {
		return new Date(serviceDownStartTime.getTime());
	}

	public void setServiceDownStartTime(String serviceDownStartTime) throws ParseException {
		Date date = sdf.parse(serviceDownStartTime);
		this.serviceDownStartTime =  new Date(date.getTime());
	}

	public Date getServiceDownEndTime() {
		return new Date(serviceDownEndTime.getTime());
	}

	public void setServiceDownEndTime(String serviceDownEndTime) throws ParseException {
		Date date = sdf.parse(serviceDownEndTime);
		this.serviceDownEndTime = new Date(date.getTime());
	}

	public long getLastExecutionTime() {
		return lastExecutionTime;
	}

	public void setLastExecutionTime(long lastExecutionTime) {
		this.lastExecutionTime = lastExecutionTime;
	}

	public int getMaxRetryCount() {
		return maxRetryCount;
	}

	public void setMaxRetryCount(int maxRetryCount) {
		this.maxRetryCount = maxRetryCount;
	}

	public int getDelayBetweenExecution() {
		return delayBetweenExecution;
	}

	public void setDelayBetweenExecution(int delayBetweenExecution) {
		this.delayBetweenExecution = delayBetweenExecution;
	}
	
	public String toString(){
		String sch = "[START:"+serviceDownStartTime+"], [END:"+serviceDownEndTime+"]";
		if(serviceDownStartTime != null && serviceDownEndTime != null){
			sch = "[START:"+sdf.format(serviceDownStartTime)+"], [END:"+this.sdf.format(serviceDownEndTime)+"]"; 
		}
		return sch;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("MAINT_START_TIME", this.sdf.format(serviceDownStartTime));
		obj.put("MAINT_END_TIME", this.sdf.format(serviceDownEndTime));
		return obj.toJSONString();
	}
}
