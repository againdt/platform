package com.getinsured.iex.hub.platform;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.WebServiceTransportException;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessageCreationException;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.messaging.PostProcessor;
import com.getinsured.iex.hub.platform.models.GIHUBResponse;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.repository.base.GIWSPayloadRepository;
import com.getinsured.iex.hub.platform.repository.base.HubResponseRepository;
import com.getinsured.iex.hub.platform.repository.fti.HubFtiResponseRepository;
import com.getinsured.iex.hub.platform.security.GhixAESCipherPool;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class MessageProcessor extends PostProcessor {

	@Autowired 
	private GIWSPayloadRepository giWSPayloadRepository;

	@Autowired
	private HubResponseRepository giHubResponseRepository;

	@Autowired
	private HubFtiResponseRepository giHubFtiResponseRepository;

	private Logger logger = LoggerFactory.getLogger(MessageProcessor.class);

	public static final int MAX_RETRY_COUNT = 3;
	public static final String INITIAL_STATUS = "PENDING";
	public static final String APPLICATION_ID_KEY = "applicationId";
	public static final String APP_ID_KEY = "APPID";
	public static final String CLIENT_IP_KEY = "clientIp";
	public static final String PAYLOAD_KEY = "payload";
	private static final String DEFAULT_IP="0.0.0.0";
	
	protected Object receivePayload(Message message) {
		JsonMessageConverter conv = new JsonMessageConverter();
		return conv.fromMessage(message);
	}

	protected GIWSPayload persistRequest(GIWSPayload payload) {
		return this.giWSPayloadRepository.saveAndFlush(payload);
	}

	/**
	 * @param payloadRecord
	 * @param handler
	 * @param serviceTemplate
	 * @return
	 * @throws HubServiceException
	 */
	protected ResponseEntity<String> executeRequest(GIWSPayload payloadRecord,
			ServiceHandler handler, WebServiceTemplate serviceTemplate) // throws
																		// HubServiceException
	{
		ResponseEntity<String> responseEntity = null;
		long requestId =-1l;
		long responseId = -1l;
		HttpStatus requestStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		long timeTaken = -1;
		String responseStr = null;
		boolean responseAvailable = false;
		HubServiceException he = null;
		String uri = null;
		uri = serviceTemplate.getDefaultUri();
		payloadRecord.setEndpointUrl(uri);
		payloadRecord.setStatus(RequestStatus.PENDING.getStatusCode());
		// Create a record with Pending Status
		String pl = handler.getJsonInput();
		try {
			if(handler.isPiiAware() || handler.isFtiAware()){
				//logger.debug("Using Encryption Key:"+GhixHubConstants.PASSWORD_KEY);
				String encPl = GhixAESCipherPool.encrypt( pl);
				payloadRecord.setRequestPayload(encPl);
			}else{
				payloadRecord.setRequestPayload(pl);
			}
			payloadRecord = this.updateRequest(payloadRecord, handler);
			requestId = payloadRecord.getId();
			logger.info("Done with persisting request with ID:"
					+ requestId);
		} catch (HubServiceException |InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException re) {
			responseEntity = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(re), HttpStatus.NOT_FOUND);
			logger.error("Exception:"+re.getMessage()+" encountered while encrypting the payload");
			return responseEntity;
		}
		//Reaching here means we have request log available in the database
		long start = 0;
		try {
			handler.isServiceAvailable();
			start = System.currentTimeMillis();
			Object requestPayload = handler.getRequestpayLoad();
			if (requestPayload == null) {
				he = new HubServiceException("Failed to create the payload");
				he.setErrorCode(RequestStatus.SYSTEM_ERROR.getStatusCode());
				throw he;
			}
			
			payloadRecord.setCorrelationId((String)handler.getContext().get("organizationCode"));
			
			WebServiceMessageCallback callBack = handler.getMessageCallback();
			Object tmp = null;
			if(callBack != null){
				tmp = serviceTemplate.marshalSendAndReceive(requestPayload,callBack);
			}else{
				tmp = serviceTemplate.marshalSendAndReceive(requestPayload);
			}
			timeTaken = System.currentTimeMillis() - start;
			responseStr = handler.handleResponse(tmp);
			responseAvailable = true;
			RequestStatus rs = handler.getRequestStatus();
			if(rs != null){
				payloadRecord.setStatus(rs.getStatusCode());
			}
		} catch (SoapMessageCreationException ie) {
			he = new HubServiceException(ie.getMessage(), ie);
			payloadRecord.setStatus(RequestStatus.SYSTEM_ERROR.getStatusCode());
		} catch (WebServiceTransportException ie) {
			he = new HubServiceException(ie.getMessage(), ie);
			he.setErrorCode(RequestStatus.NETWORK_ERROR.getStatusCode());
			payloadRecord
					.setStatus(RequestStatus.NETWORK_ERROR.getStatusCode());
		} catch (WebServiceIOException we) {
			he = new HubServiceException(we.getMessage(), we);
			he.setErrorCode(RequestStatus.RETRY.getStatusCode());
			payloadRecord.setStatus(RequestStatus.RETRY.getStatusCode());
			requestStatus = HttpStatus.NOT_FOUND;
		} catch (SoapFaultClientException se) {
			he = new HubServiceException(se.getFaultCode() + ":"
					+ se.getFaultStringOrReason(), se);
			he.setErrorCode(RequestStatus.SERVICE_INVOCATION_ERROR.getStatusCode());
			payloadRecord.setStatus(RequestStatus.SERVICE_INVOCATION_ERROR
					.getStatusCode());
		} catch (HubServiceException we) {
			he = we;
			he.setErrorCode(RequestStatus.FAILED.getStatusCode());
			payloadRecord.setStatus(RequestStatus.FAILED.getStatusCode());
		} catch (HubMappingException e) {
			he = new HubServiceException("System error:" + e.getMessage(), e);
			he.setErrorCode(RequestStatus.SYSTEM_ERROR.getStatusCode());
			payloadRecord.setStatus(RequestStatus.SYSTEM_ERROR.getStatusCode());
		} catch (ServiceUnAvailableException e) {
			payloadRecord.setStatus(RequestStatus.UNDER_MAINT.getStatusCode());
			he = new HubServiceException(e.getMessage(), e);
			he.setErrorCode(RequestStatus.UNDER_MAINT.getStatusCode());
			he.setServiceDownTimeSchedule(handler.getServiceDownTimeSchedule());
			requestStatus = HttpStatus.SERVICE_UNAVAILABLE;
		}
		if (he != null) {
			logger.error("Error encountered "+he.getMessage()+" Attempting to update the request log",he);
			responseStr = PlatformServiceUtil.exceptionToJson(he);
			payloadRecord.setExceptionMessage(responseStr);
			
		}
		// Now try to update the request status
		try {
			this.updateRequest(payloadRecord, handler);
		} catch (HubServiceException re) {
			logger.error("failed to update the request log",re);
			he = re;
			responseStr = PlatformServiceUtil.exceptionToJson(he);
		}
		// If we have exception at this stage, no point going forward
		if(he != null){
			return makeResponse(responseStr, HttpStatus.INTERNAL_SERVER_ERROR,requestId, responseId);
		}
		
		// Reaching here means Request has been success so far, now update the response only if
		// 1) Request has been executed without any error 
		// 2) Request Log has been updated without any error
		// This check looks redundant ....
		if (responseAvailable) {
			try {
				responseId = this.updateResponse(payloadRecord, timeTaken, responseStr, handler, serviceTemplate);
				if(responseId > 0){
					requestStatus = HttpStatus.OK;
				}
			} catch (HubServiceException e) {
				he = e;
			}
		}
		// Even if Service excuted successfully, if we fail to save the response, we'll fail the entire transaction
		if (he != null) {
			responseStr = PlatformServiceUtil.exceptionToJson(he);
			logger.error(responseStr, he);
		}
		return makeResponse(responseStr, requestStatus,requestId, responseId);
	}
	
	
	
	private ResponseEntity<String> makeResponse(String responseStr,
			HttpStatus requestStatus, long requestId, long responseId) {
		 HttpHeaders x = new HttpHeaders();
	     x.setContentType(MediaType.valueOf("application/json"));
		 x.add("X-RequestId", Long.toString(requestId));
		 x.add("X-ResponseId", Long.toString(responseId));
		 ResponseEntity<String> response = new ResponseEntity<String>(responseStr, x, requestStatus);
		 return response;
				
	}
	
	
	
	private long updateResponse(GIWSPayload payloadRecord, long timeTaken, String responseStr, ServiceHandler handler, WebServiceTemplate serviceTemplate) throws HubServiceException{
		HubServiceException he = null;
		long responseId = -1;
		
		try {
			GIHUBResponse hubResponse = new GIHUBResponse();
			try {
				if(handler.isPiiAware() || handler.isFtiAware()){
					responseStr = GhixAESCipherPool.encrypt( responseStr);
				}
			} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException re) {
				throw new HubServiceException(re);
			}
			hubResponse.setEndPointURL(serviceTemplate.getDefaultUri());

			hubResponse.setGiWspayloadId(payloadRecord.getId());
			hubResponse.setClientNodeIp(DEFAULT_IP);// Not yet implemented
			hubResponse.setSsapApplicationId(payloadRecord.getSsapApplicantId());
			hubResponse.setStatus(handler.getRequestStatus().getStatusCode());
			hubResponse.setElapsedTime(timeTaken);
			hubResponse.setHubResponsePayload(responseStr);
			hubResponse.setHubResponseCode(handler.getResponseCode());
			if(handler.getResponseContext().get("HUB_CASE_NUMBER") != null){
				hubResponse.setHubCaseNumber(""+handler.getResponseContext().get("HUB_CASE_NUMBER"));
			}
			if(handler.isFtiAware()){
				hubResponse = this.giHubFtiResponseRepository.saveAndFlush(hubResponse);
			}else{
				hubResponse = this.giHubResponseRepository.saveAndFlush(hubResponse);
			}
			responseId = hubResponse.getId();
		} catch (RuntimeException e) {
			he = new HubServiceException(
					"Service executed successfully but Failed to save the response log, failing the entire transaction. Check datbase connectivity and try again.",
					e);
			logger.error(he.getMessage());
			throw he;
		}
		return responseId;
	}
	
	private GIWSPayload updateRequest(GIWSPayload request, ServiceHandler handler) throws HubServiceException{
		HubServiceException he = null;

		try {
			if(request.getSsapApplicationId() == null || request.getSsapApplicationId() > 0) {
				logger.debug("Saving Hub Response");
				request = this.giWSPayloadRepository.saveAndFlush(request);
			}
		} catch (RuntimeException e) {
			he = new HubServiceException(
					"Service executed successfully but Failed to save the response log",
					e);
			he.setErrorCode(RequestStatus.DB_NOT_AVAILABLE.getStatusCode());
			logger.error(he.getMessage());
			throw he;
		}
		return request;
	}

	public void setGiWSPayloadRepository(GIWSPayloadRepository giWSPayloadRepository) {
		this.giWSPayloadRepository = giWSPayloadRepository;
	}

	public void setGiHubResponseRepository(HubResponseRepository giHubResponseRepository) {
		this.giHubResponseRepository = giHubResponseRepository;
	}

	public void setGiHubFtiResponseRepository(HubFtiResponseRepository giHubFtiResponseRepository) {
		this.giHubFtiResponseRepository = giHubFtiResponseRepository;
	}
	
	
}
