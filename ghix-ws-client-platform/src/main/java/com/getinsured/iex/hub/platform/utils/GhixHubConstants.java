/**
 * 
 */
package com.getinsured.iex.hub.platform.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.iex.hub.platform.security.GhixAESCipherPool;

@Component("ghixHubConstants")
@DependsOn({"hubGhixSecretKey","configProp"})
public final class GhixHubConstants {
	public static final String ENC_START_TAG = "ENC(";
	public static final String ENC_END_TAG = ")";
	public static final String SYS_PROP_START_TAG = "${";
	public static final String SYS_PROP_END_TAG = "}";
	public static boolean VLP_2_3_Enabled = false;

	public static String HTTP_BASIC_AUTH_USER = null;

	public static String HTTP_BASIC_AUTH_PASS = null;

	public static String HUB_KEY_STORE_LOCATION;

	public static String HUB_KEY_STORE_FILE;
	
	public static String HUB_SERVICE_DEFINITION_DIR;
	
	public static String HUB_KEYSTORE_PASS;

	public static String HTTP_BASIC_AUTH_HOST;

	public static int HTTP_BASIC_AUTH_PORT;
	
	//public static String PASSWORD_KEY = System.getProperty("PASSWORD_KEY");

	private GhixHubConstants() {
	}

	/****************************************************************************************/
	/********************* HUB Configuration ************************************************/
	/****************************************************************************************/
	@Value("#{configProp['fdh.vlp2And3Enabled'] != null ? configProp['fdh.vlp2And3Enabled'] : 'true'}")
	public void setVLp2And3Enabled(String vlp2And3Enabled){
		VLP_2_3_Enabled = vlp2And3Enabled.trim().equalsIgnoreCase("true");
	}
	
	@Value("#{configProp['hubKeyStoreDir'] != null ? configProp['hubKeyStoreDir'] : 'NOT_AVAILABLE'}")
	public void setHubKeyStoreLocation(String hubKeyStoreLocation){
		HUB_KEY_STORE_LOCATION = convertPropertyValue(hubKeyStoreLocation);
	}
	
	@Value("#{configProp['hubKeyStoreFile'] != null ? configProp['hubKeyStoreFile'] : 'NOT_AVAILABLE'}")
	public void setHubKeyStoreFile(String hubKeyStoreFile){
		HUB_KEY_STORE_FILE = convertPropertyValue(hubKeyStoreFile);
	}
	
	@Value("#{configProp['hubServiceDefinitionDir'] != null ? configProp['hubServiceDefinitionDir'] : 'NOT_AVAILABLE'} ")
	public void setServiceDefinitionDirectory(String serviceDefDir){
		HUB_SERVICE_DEFINITION_DIR = convertPropertyValue(serviceDefDir);
	}
	
	@Value("#{configProp['hubKeyStorePass'] != null ? configProp['hubKeyStorePass'] : 'NOT_AVAILABLE'}")
	public void setHubKeyStorePassword(String pass){
		HUB_KEYSTORE_PASS = pass;	
	}
	
	@Value("#{configProp['httpBasicAuthUser'] != null ? configProp['httpBasicAuthUser'] : 'opadmin@ghix.com'}")
	public void setHttpBasicAuthUserName(String userName){
		HTTP_BASIC_AUTH_USER = userName;
	}
	
	@Value("#{configProp['httpBasicAuthPass'] != null ? configProp['httpBasicAuthPass'] : 'notadmin123#'}")
	public void setHttpbasicAuthUserPass(String pass){
		HTTP_BASIC_AUTH_PASS = pass;
	}
	
	@Value("#{configProp['httpBasicAuthHost'] != null ? configProp['httpBasicAuthHost'] : 'localhost'}")
	public void setHttpBasicAuthHost(String host){
		HTTP_BASIC_AUTH_HOST = host;
	}
	
	@Value("#{configProp['httpBasicAuthPort'] != null ? configProp['httpBasicAuthPort'] : '9443'}")
	public void setHttpbasicAuthUserPort(int port){
		HTTP_BASIC_AUTH_PORT = port;
	}
	private static  String convertPropertyValue(final String originalValue) {
		//System.out.println("Converting value:"+originalValue);
		if(originalValue == null){
			return null;
		}
		if(originalValue.startsWith(ENC_START_TAG) && originalValue.endsWith(ENC_END_TAG)){
			try {
				return GhixAESCipherPool.decrypt(getInnerEncryptedValue(originalValue));
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| InvalidKeySpecException | NoSuchPaddingException
					| InvalidAlgorithmParameterException
					| UnsupportedEncodingException | IllegalBlockSizeException
					| BadPaddingException e) {
				throw new RuntimeException("Failed to decrypt the property ["+originalValue+"]"+e.getMessage(),e);
			}
		}
		if(originalValue.startsWith(SYS_PROP_START_TAG)){
			try {
				//System.out.println("Evaluating the property:"+originalValue);
				return getSystemProperty(originalValue);
			} catch (RuntimeException e) {
				throw new RuntimeException("Failed to decrypt the property ["+originalValue+"]"+e.getMessage(),e);
			}
		}
		return originalValue;
	}


	private static String getSystemProperty(String originalValue) {
		StringBuilder sb = new StringBuilder();
		String tmp = null;
		int endTag = originalValue.indexOf(SYS_PROP_END_TAG);
		if(endTag == -1){
			//System.out.println("No End tag found for "+originalValue);
			return originalValue;
		}
		String sysProperty = originalValue.substring(SYS_PROP_START_TAG.length(),endTag);
	//	System.out.println("Looking for System property:"+sysProperty+" in System properties");
		
		String propValue = System.getProperty(sysProperty);
		if(propValue == null){
			// Try the environment
			//System.out.println("Looking for System property:"+sysProperty+" in Environment");
			propValue = System.getenv(sysProperty);
		}
	//	System.out.println("Retrieved property ["+sysProperty+"]:"+propValue);
		sb.append(propValue);
		sb.append(originalValue.substring(endTag+1));
		tmp = sb.toString();
	//	System.out.println("Resolved:"+tmp);
		return tmp;
	}
	private static String getInnerEncryptedValue(String value) {
		return value.substring(ENC_START_TAG.length(),
				(value.length() - ENC_END_TAG.length()));
	}
}
