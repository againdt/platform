package com.getinsured.iex.hub.platform.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="HUB_RESPONSES")
public class GIHUBResponse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HUB_RESPONSES_SEQ")
	@SequenceGenerator(name = "HUB_RESPONSES_SEQ", sequenceName = "HUB_RESPONSES_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private long id;
	
	@Column(name = "GI_WS_PAYLOAD_ID")
	private long giWspayloadId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
    private Date createdTimestamp;
	
	@Column(name = "HUB_CASE_NUMBER") 
	private String hubCaseNumber;
	
	@Column(name = "HUB_SESSION_IDENTIFIER")
	private String hubSessionId;
	
	@Column(name = "HUB_RESPONSE_PAYLOAD")
	private String hubResponsePayload;
	
	@Column(name = "HUB_RESPONSE_CODE")
	private String hubResponseCode;
	
	@Column(name = "ELAPSE_TIME")
	private long elapsedTime;
	
	@Column(name = "SSAP_APPLICANT_ID")
	private Long ssapApplicationId;
	
	@Column(name = "ENDPOINT_URL")
	private String endPointURL;
	
	@Column(name = "CLIENT_NODE_IP")
	private String clientNodeIp;
	
	@Column(name = "RETRY_COUNT")
	private long retryCount;
	
	@Column(name = "STATUS")
	private String status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getGiWspayloadId() {
		return giWspayloadId;
	}

	public void setGiWspayloadId(long giWspayloadId) {
		this.giWspayloadId = giWspayloadId;
	}

	public Date getCreatedTimestamp() {
		
		if(createdTimestamp != null){
			return new Date(createdTimestamp.getTime());
		}
		
		return null;
		
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		
		if(createdTimestamp != null){
			this.createdTimestamp = new Date(createdTimestamp.getTime());
		}
		
	}

	public String getHubCaseNumber() {
		return hubCaseNumber;
	}

	public void setHubCaseNumber(String hubCaseNumber) {
		this.hubCaseNumber = hubCaseNumber;
	}

	public String getHubSessionId() {
		return hubSessionId;
	}

	public void setHubSessionId(String hubSessionId) {
		this.hubSessionId = hubSessionId;
	}

	public String getHubResponsePayload() {
		return hubResponsePayload;
	}

	public void setHubResponsePayload(String hubResponsePayload) {
		this.hubResponsePayload = hubResponsePayload;
	}

	public String getHubResponseCode() {
		return hubResponseCode;
	}

	public void setHubResponseCode(String hubResponseCode) {
		this.hubResponseCode = hubResponseCode;
	}

	public long getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getEndPointURL() {
		return endPointURL;
	}

	public void setEndPointURL(String endPointURL) {
		this.endPointURL = endPointURL;
	}

	public String getClientNodeIp() {
		return clientNodeIp;
	}

	public void setClientNodeIp(String clientNodeIp) {
		this.clientNodeIp = clientNodeIp;
	}

	public long getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(long retryCount) {
		this.retryCount = retryCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	// To AutoUpdate created and updated dates while persisting object
	@PrePersist
	public void prePersist()
	{
		this.setCreatedTimestamp(new Date());
	}
}
