package com.getinsured.iex.hub.platform;

public class ServiceUnAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServiceUnAvailableException() {
	}

	public ServiceUnAvailableException(String message) {
		super(message);
	}

	public ServiceUnAvailableException(Throwable cause) {
		super(cause);
	}

	public ServiceUnAvailableException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceUnAvailableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
