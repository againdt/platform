package com.getinsured.iex.hub.platform.repository.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.hub.platform.models.GIWSPayload;

@Repository
public interface GIWSPayloadRepository extends JpaRepository<GIWSPayload, Long> {
	
	@Query("select u from GIWSPayload u where u.ssapApplicationId = ?1 AND u.endpointUrl= ?2")
	GIWSPayload findByApplicationIdAndEndpointURL(long appId, String url);

}
