package com.getinsured.iex.hub.platform.messaging;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GIJson {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GIJson.class);
	
	private JSONObject jsonObject;
	private GIJson(JSONObject obj){
		this.jsonObject = obj;
	}
	
	public static GIJson getGIJson(String jsonStr) throws ParseException{
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(jsonStr);
		return new GIJson(json);
	}
	
	/**
	 * Doesn't work if there are multiple keys, will return the first one only
	 * This method should return the list of all matching keys for a given path
	 * @param path
	 * @param key
	 * @param c 
	 * @return
	 */
	public Object getValueForKey(String path, String key){
		JSONObject targetObj = this.jsonObject;
		String tmpStr = null;
		Object obj;
		String[] pathElements = path.split("#");
		int len = pathElements.length;
		for(int i = 0; i < len; i++){
			tmpStr = pathElements[i];//i = 0 means root element in a given JSON
			obj = targetObj.get(tmpStr);
			if(obj instanceof JSONObject){
				targetObj = (JSONObject)obj;
			}else if(obj instanceof JSONArray){
				LOGGER.debug(tmpStr+" is an array, finding target JSONObject");
				JSONArray arrayObj = (JSONArray)obj;
				if((i+1) < len){
					targetObj = findTargetFromJSONArray(pathElements[i+1],arrayObj);
				}else{
					//we are at the end of path element
					targetObj = findTargetFromJSONArray(key, arrayObj);
				}
			}
		}
		if(targetObj == null){
			return null;
		}
		return targetObj.get(key);
	}
	
	private JSONObject findTargetFromJSONArray(String nextPathElement, JSONArray array) {
		int len = array.size();
		JSONObject tmpObj = null;
		Object tmp;
		for(int i = 0; i < len; i++){
			tmp= array.get(i);
			if(tmp instanceof JSONObject){
				tmpObj = (JSONObject)tmp;
				if(tmpObj.get(nextPathElement) != null){
					break;
				}
			}else if(tmp instanceof JSONArray){
				return findTargetFromJSONArray(nextPathElement, (JSONArray)tmp);
			}else{
				LOGGER.debug("We have :"+tmp.getClass().getName());
				tmpObj = (JSONObject)tmp;
			}
		}
		return tmpObj;
	}
	
	public static void main(String[] args) throws ParseException{
		String json = "" +
				"{" +
				"\"householdMember\":" +
					"[" +
						"{" +
						"\"personId\":\"1\"," +
						"\"name\":" +
						"	{" +
						"	\"firstName\":\"George\"," +
						"	\"middleName\":\"B\"," +
						"	\"lastName\":\"Prince\"" +
						"	}," +
						"\"dateOfBirth\":\"1950-07-13\"," +
						"\"socialSecurityCard\":" +
						"	{" +
						"	\"socialSecurityNumber\":\"221212503\"" +
						"	}," +
						"\"citizenshipImmigrationStatus\":" +
						"	{" +
						"	\"citizenshipAsAttestedIndicator\":\"true\"" +
						"	}," +
						"\"incarcerationStatus\":" +
						"	{" +
						"	\"incarcerationAsAttestedIndicator\":\"true\"" +
						"	}" +
						"}" +
					"]" +
				"}";
		GIJson jsonObj = GIJson.getGIJson(json);
		Criteria c = new Criteria();
		c.setCriteria("personId", "1");
		LOGGER.debug(""+jsonObj.getValueForKey("householdMember#socialSecurityCard", "socialSecurityNumber"));
	}
}

class Criteria{
	private Map<String, String> matches;
		
		public Criteria(){
			
		}
		
		public void setCriteria(String key, String value){
			if(this.matches == null){
				this.matches = new HashMap<String, String>();
			}
			this.matches.put(key, value);
		}
		
		public boolean match(String key, String value){
			String val = this.matches.get(key);
			if(val == null){
				return false;
			}
			return value.equalsIgnoreCase(val);
		}
}
