package com.getinsured.iex.hub.platform.ssap.iex;

import org.json.simple.JSONObject;

public class HouseholdMember {
	private long personId = -1;
	private String personGivenName;
	private String personMiddleName;
	private String personSurName;
	private String personNameSuffixText;
	private String dateOfBirth = null;
	private String gender = null;
	private boolean applyingForCoverageIndicator = false;
	private HouseholdMemberAddress homeAddress = null;
	private SSNCardInfo ssnInfo = null;
	private CitizenshipInfo citizenshipInfo = null;
	private IncarcerationInfo incarcerationInfo = null;
	private String insurancePolicyEffectiveDate;
	private String insurancePolicyExpirationDate;
	private String taxFilerCategoryCode;
	
	
	public HouseholdMember(JSONObject member){
		Object tmp = null;
		if(member != null){
			tmp = member.get("personId");
			if(tmp != null){
				this.personId = (Long) tmp;
			}
			tmp = member.get("name");
			if(tmp != null){
				this.setPersonName((JSONObject)tmp);
			}
			tmp = member.get("dateOfBirth");
			if(tmp != null){
				this.dateOfBirth = (String)tmp;
			}
			tmp = member.get("applyingForCoverageIndicator");
			if(tmp != null){
				this.applyingForCoverageIndicator = (Boolean)tmp;
			}
			
			tmp = member.get("householdContact");
			if(tmp != null){
				JSONObject householdContact = (JSONObject)tmp;
				this.homeAddress = new HouseholdMemberAddress((JSONObject)householdContact.get("homeAddress"));
			}
			tmp = member.get("socialSecurityCard");
			if(tmp != null){
				this.ssnInfo = new SSNCardInfo((JSONObject)tmp);
			}
			tmp = member.get("citizenshipImmigrationStatus");
			if(tmp != null){
				this.citizenshipInfo = new CitizenshipInfo((JSONObject)tmp);
			}
			tmp = member.get("incarcerationStatus");
			if(tmp != null){
				this.incarcerationInfo = new IncarcerationInfo((JSONObject)tmp);
			}
			tmp = member.get("gender");
			if(tmp != null){
				this.gender = (String)tmp;
			}
			setInsurance(member);
			tmp = member.get("taxFilerCategoryCode");
			if(tmp != null){
				this.taxFilerCategoryCode = (String)tmp;
			}
		}
		
	}
	
	private void setInsurance(JSONObject member){
		
		Object tmp = member.get("insurance");
		if(tmp != null){
			JSONObject insuranceObj = (JSONObject)tmp;
			this.insurancePolicyEffectiveDate = (String)insuranceObj.get("insurancePolicyEffectiveDate");
			this.insurancePolicyExpirationDate = (String)insuranceObj.get("insurancePolicyExpirationDate");
		}
	}
	
	private void setPersonName(JSONObject name) {
		this.personGivenName = (String) name.get("firstName");
		this.personMiddleName = (String) name.get("middleName");
		this.personSurName = (String) name.get("lastName");
		this.personNameSuffixText = (String) name.get("suffix");
	}
	public long getPersonId() {
		return personId;
	}
	public String getPersonGivenName() {
		return personGivenName;
	}
	public String getPersonMiddleName() {
		return personMiddleName;
	}
	public String getPersonSurName() {
		return personSurName;
	}
	
	public String getPersonNameSuffixText() {
		return personNameSuffixText;
	}
	public String getDateOfBirth(){
		return this.dateOfBirth;
	}
	
	public boolean getApplyingForCoverageIndicator(){
		return this.applyingForCoverageIndicator;
	}

	public HouseholdMemberAddress getHomeAddress() {
		if(this.homeAddress == null){
			this.homeAddress = new HouseholdMemberAddress(null);
		}
		return this.homeAddress;
	}
	
	public SSNCardInfo getSsnInfo(){
		if(this.ssnInfo == null){
			this.ssnInfo = new SSNCardInfo(null);
		}
			
		return this.ssnInfo;
	}
	
	public CitizenshipInfo getCitizenshipInfo(){
		if(this.citizenshipInfo == null){
			this.citizenshipInfo = new CitizenshipInfo(null);
		}
			
		return this.citizenshipInfo;
	}
	
	public IncarcerationInfo getIncarcerationInfo(){
		if(this.incarcerationInfo == null){
			this.incarcerationInfo = new IncarcerationInfo(null);
		}
			
		return this.incarcerationInfo;
	}

	public String getGender() {
		return gender;
	}

	public String getInsurancePolicyEffectiveDate() {
		return insurancePolicyEffectiveDate;
	}

	public String getInsurancePolicyExpirationDate() {
		return insurancePolicyExpirationDate;
	}

	public String getTaxFilerCategoryCode() {
		return taxFilerCategoryCode;
	}
	
}
