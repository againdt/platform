package com.getinsured.iex.hub.platform.ssap.iex;

import org.json.simple.JSONObject;

public class HouseholdMemberAddress {
	
	private String streetAddress1;
	private String streetAddress2;
	private String city;
	private String state;
	private String postalCode;
	private String county;

	public HouseholdMemberAddress(JSONObject address){
		Object tmp;
		if(address != null){
			//Mandatory
			this.streetAddress1 = (String)address.get("streetAddress1");
			//Optional
			tmp = address.get("streetAddress2");
			if(tmp != null){
				this.streetAddress2 = (String)tmp;
			}
			tmp = address.get("city");
			if(tmp != null){
				this.city = (String)tmp;
			}
			tmp = address.get("state");
			if(tmp != null){
				this.state = (String)tmp;
			}
			tmp = address.get("postalCode");
			if(tmp != null){
				this.postalCode = tmp.toString();
			}
			tmp = address.get("county");
			if(tmp != null){
				this.county = (String)tmp;
			}
		}
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCounty() {
		return county;
	}
}
