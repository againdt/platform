package com.getinsured.iex.hub.platform.datasource;


import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.getinsured.iex.hub.platform.UnknownDatasourceException;

public class RoutingHubDataSource extends AbstractRoutingDataSource{
	
	private static final String DEFAULT_DATASOURCE_KEY = "ghixDS";
	
	private String dataSourceKey = DEFAULT_DATASOURCE_KEY;
	
	public void resetDataSource()
	{
		dataSourceKey = DEFAULT_DATASOURCE_KEY;
	}
	public String getDataSourceKey() {
		return dataSourceKey;
	}
	public void setDataSourceKey(String dataSourceKey) throws UnknownDatasourceException {
		String currentDataSourceKey = this.dataSourceKey;
		
		this.dataSourceKey = dataSourceKey;
		try{
			determineTargetDataSource();
		}catch(IllegalStateException e){
			this.dataSourceKey = currentDataSourceKey;
			throw new UnknownDatasourceException("Datasource mapping not found for the lookup key provided in the service definition file", e);
		}
	}
	
	@Override
	protected Object determineCurrentLookupKey() {
		return dataSourceKey;
	}
}