
package com.getinsured.iex.hub.platform.envers;

import org.hibernate.envers.DefaultRevisionEntity;
import org.springframework.data.repository.history.support.RevisionEntityInformation;

class DefaultRevisionEntityInformation implements RevisionEntityInformation {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.repository.history.support.RevisionEntityInformation#getRevisionNumberType()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getRevisionNumberType() {
		return Integer.class;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.repository.history.support.RevisionEntityInformation#isDefaultRevisionEntity()
	 */
	public boolean isDefaultRevisionEntity() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.repository.history.support.RevisionEntityInformation#getRevisionEntityClass()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getRevisionEntityClass() {
		return DefaultRevisionEntity.class;
	}
}
