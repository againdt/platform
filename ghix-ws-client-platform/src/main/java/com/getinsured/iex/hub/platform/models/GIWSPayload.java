package com.getinsured.iex.hub.platform.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="GI_WS_PAYLOAD")
public class GIWSPayload {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GI_WS_PAYLOAD_SEQ")
	@SequenceGenerator(name = "GI_WS_PAYLOAD_SEQ", sequenceName = "GI_WS_PAYLOAD_SEQ", allocationSize = 1)
	@Column(name = "GI_WS_PAYLOAD_ID")
	private long id;
	
	
	@Column(name="REQUEST_PAYLOAD")
    private String requestPayload;
	
	
	@Column(name="RESPONSE_PAYLOAD")
    private String responsePayload;
	

	@Column(name="RESPONSE_CODE")
    private Integer responseCode;
	
	
	@Column(name="STATUS")
    private String status;
	
	@Column(name = "CORRELATION_ID")
	private String correlationId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_TIMESTAMP")
    private Date createdTimestamp;

	@Column(name = "ENDPOINT_URL")
	private String endpointUrl;

	@Column(name = "ENDPOINT_FUNCTION")
	private String endpointFunction;
	
	@Column(name = "ENDPOINT_OPERATION_NAME")
	private String endpointOperationName;
	
	@Column(name="EXCEPTION_MESSAGE")
    private String exceptionMessage;
	
	@Column(name="CREATED_USER_ID")
	private long createdUserId;
	
	@Column(name="ACCESS_IP")
	private String accessIp;
	
	@Column(name="CLIENT_NODE_IP")
	private String clientNodeIp;
	
	@Column(name="RETRY_COUNT")
	private long retryCount;
	
	@Column(name="SSAP_APPLICANT_ID")
	private Long ssapApplicantId;

	
	@Column(name="SSAP_APPLICATION_ID")
	private Long ssapApplicationId;
	
	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getRequestPayload() {
		return requestPayload;
	}



	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}



	public String getResponsePayload() {
		return responsePayload;
	}


	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}



	public Integer getResponseCode() {
		return responseCode;
	}


	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}


	public String getCorrelationId() {
		return correlationId;
	}


	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}



	public Date getCreatedTimestamp() {
		
		if(createdTimestamp != null){
			return new Date(createdTimestamp.getTime());
		}
		
		return null;
	}


	public void setCreatedTimestamp(Date createdTimestamp) {
		
		if(createdTimestamp != null){
			this.createdTimestamp =  new Date(createdTimestamp.getTime());
		}
		
	}



	public String getEndpointUrl() {
		return endpointUrl;
	}


	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}


	public String getEndpointFunction() {
		return endpointFunction;
	}


	public void setEndpointFunction(String endpointFunction) {
		this.endpointFunction = endpointFunction;
	}


	public String getEndpointOperationName() {
		return endpointOperationName;
	}



	public void setEndpointOperationName(String endpointOperationName) {
		this.endpointOperationName = endpointOperationName;
	}


	public String getExceptionMessage() {
		return exceptionMessage;
	}


	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}



	public long getCreatedUserId() {
		return createdUserId;
	}


	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}
	
	
	public String getAccessIp() {
		return accessIp;
	}



	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}



	public String getClientNodeIp() {
		return clientNodeIp;
	}



	public void setClientNodeIp(String clientNodeIp) {
		this.clientNodeIp = clientNodeIp;
	}



	public long getRetryCount() {
		return retryCount;
	}



	public void setRetryCount(long retryCount) {
		this.retryCount = retryCount;
	}



	public Long getSsapApplicantId() {
		return ssapApplicantId;
	}



	public void setSsapApplicantId(Long ssapApplicantId) {
		this.ssapApplicantId = ssapApplicantId;
	}



	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}



	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}



	// To AutoUpdate created and updated dates while persisting object
	@PrePersist
	public void prePersist()
	{
		this.setCreatedTimestamp(new Date());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void preUpdate()
	{
		this.setCreatedTimestamp(new Date());
	}


}
