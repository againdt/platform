package com.hub.platform.test;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.validator.FieldValidator;

public class FieldValidatorTest {

	@BeforeClass
	public static void init() {
	   PropertyConfigurator.configure("src/main/resources/log4j.properties");
	}
	
	@Test
	public void testSSN() throws InputDataValidationException{
		
		String value = "789635221";
		FieldValidator ssnValidator = new FieldValidator();
		ssnValidator.addContextParameter("isRequired", "Y");
		ssnValidator.addContextParameter("minLength", "9");
		ssnValidator.addContextParameter("maxLength", "9");
		ssnValidator.addContextParameter("regex", "^([1-57-8][0-9]{2}|0([1-9][0-9]|[0-9][1-9])|6([0-57-9][0-9]|[0-9][0-57-9]))([1-9][0-9]|[0-9][1-9])([1-9]\\d{3}|\\d[1-9]\\d{2}|\\d{2}[1-9]\\d|\\d{3}[1-9])$");
		ssnValidator.addContextParameter("isNumeric","Y");
		ssnValidator.validate("personSSNIdentification", value);
	}
	
}
