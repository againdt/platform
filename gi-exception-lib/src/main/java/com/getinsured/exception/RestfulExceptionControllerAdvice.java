package com.getinsured.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Controller Advice to be used in RESTful Controllers on the Service Layer.
 *
 * @author Yevgen Golubenko
 * @since 10/20/17
 */
@ControllerAdvice
public class RestfulExceptionControllerAdvice
{
  private static final Logger log = LoggerFactory.getLogger(RestfulExceptionControllerAdvice.class);

  /**
   * Global RESTful Exception handler.
   *
   * @param ex Exception.
   * @param request HTTP Servlet Request.
   * @return {@link ResponseEntity} object.
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Map<String, Object>> exceptionHandler(Exception ex, HttpServletRequest request)
  {
    ResponseEntity<Map<String, Object>> response = null;

    if(!(ex instanceof RestfulException))
    {
      RestfulException restfulException = new RestfulException(HttpStatus.INTERNAL_SERVER_ERROR, ex);
      restfulException.setStackTrace(ex.getStackTrace());
      response = getResponseEntity(restfulException, request);
    }
    else
    {
      response = getResponseEntity((RestfulException) ex, request);
    }

    log.error("Exception occurred in SVC layer: {}", response);
    return response;
  }

  private ResponseEntity<Map<String, Object>> getResponseEntity(RestfulException ex, HttpServletRequest request)
  {
    Map<String, Object> exceptionDetails = new HashMap<>(6);

    exceptionDetails.put("url", request.getRequestURI());
    exceptionDetails.put("parameters", request.getParameterMap());
    exceptionDetails.put("message", ex.getMessage());
    exceptionDetails.put("details", ex.getDetails());

    List<String> stackTrace = new ArrayList<>();

    for(StackTraceElement trace : ex.getStackTrace())
    {
      stackTrace.add(trace.toString());
    }

    exceptionDetails.put("stackTrace", stackTrace);
    exceptionDetails.put("httpStatus", ex.getHttpStatus().value());
    exceptionDetails.put("tag", ex.getTag());

    return new ResponseEntity<>(exceptionDetails, ex.getHttpStatus());
  }
}
