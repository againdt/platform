package com.getinsured.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;

/**
 * RESTful Runtime Exception.
 *
 * <p>
 *  Throw it at the service layer
 *  so that approporiate JSON response with Exception Details, HTTP Error Code
 *  and so forth could be thrown from the RESTful Service Layer.
 * </p>
 *
 * <p>
 *   It's later will be consumed by the WEB layer and JSP will be rendered with
 *   the details of this exception.
 * </p>
 * <p>
 *   Detault HTTP error code will be 500 ({@link HttpStatus#INTERNAL_SERVER_ERROR}).
 * </p>
 * <p>
 *  You can also add {@code tag} to the exception before throwing it, so that it can be
 *  categorized and searched upon given {@code tag} later.
 * </p>
 * @author Yevgen Golubenko
 * @since 10/20/17
 */
public class RestfulException extends RuntimeException
{
  private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  private Map<String, Object> details;
  private String tag;

  /**
   * Throws RESTful exception with given exception details.
   * <p>
   *   This probably easiest constructor to use, because it takes care
   *   of figuring out what exception happened with HTTP Response Code
   *   if it knows what whent wrong.
   * </p>
   * <p>
   *   Default HTTP Response will be 500, defined by {@link HttpStatus#INTERNAL_SERVER_ERROR}.
   * </p>
   * @param ex original exception that you want to wrap into RESTful Exception.
   */
  public RestfulException(Throwable ex)
  {
    super(ex);
    this.populateExtraDetails(ex);
  }

  /**
   * If you want message to be displayed to the end user, use this.
   * <p>
   *   Default HTTP Response will be 500, defined by {@link HttpStatus#INTERNAL_SERVER_ERROR}.
   * </p>
   * @param message Custom message to be displayed to the <b>end user</b>.
   * @param ex original exception.
   */
  public RestfulException(String message, Throwable ex)
  {
    super(message, ex);
    this.populateExtraDetails(ex);
  }

  /**
   * Takes message and tags this exception.
   * <p>
   *   Default HTTP Response will be 500, defined by {@link HttpStatus#INTERNAL_SERVER_ERROR}.
   * </p>
   * @param message Custom message to be displayed to the <b>end user</b>.
   * @param tag Tag for this exception.
   * @param cause original exception.
   */
  public RestfulException(String message, String tag, Throwable cause)
  {
    super(message, cause);
    this.setTag(tag);
  }

  /**
   * Constructs w/o any custom message.
   * <p>
   *   Consider use other constructors, because this defines purpose
   *   of this framework because it doesn't provide any message to the developer!
   * </p>
   * @param httpStatus http status code.
   * @param ex original exception.
   */
  public RestfulException(HttpStatus httpStatus, Throwable ex)
  {
    super(ex);
    this.setHttpStatus(httpStatus);
    this.populateExtraDetails(ex);
  }

  /**
   * Constructs exception with message and HTTP status code.
   *
   * @param message message.
   * @param httpStatus HTTP status code.
   */
  public RestfulException(String message, HttpStatus httpStatus)
  {
    super(message);
    this.setHttpStatus(httpStatus);
  }

  /**
   * Constructs exception with message, HTTP code, Tag and original Exception.
   *
   * @param message message.
   * @param httpStatus HTTP status code.
   * @param tag Tag.
   * @param cause Original cause.
   */
  public RestfulException(String message, HttpStatus httpStatus, String tag, Throwable cause)
  {
    super(message, cause);
    this.setHttpStatus(httpStatus);
    this.setTag(tag);
  }

  /**
   * Constructs with message, HTTP Status code and original exception.
   *
   * @param message Custom message.
   * @param httpStatus http status code.
   * @param cause Original Exception
   */
  public RestfulException(String message, HttpStatus httpStatus, Throwable cause)
  {
    super(message, cause);
    this.setHttpStatus(httpStatus);
    this.populateExtraDetails(cause);
  }

  /**
   * Constructs with message, HTTP Status code and original exception.
   *
   * @param message Custom message.
   * @param httpStatus http status code.
   * @param cause Original Exception.
   * @param enableSuppression whether or not suppression is enabled
   *                          or disabled
   * @param writableStackTrace whether or not the stack trace should
   *                           be writable
   */
  public RestfulException(String message, HttpStatus httpStatus, Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace)
  {
    super(message, cause, enableSuppression, writableStackTrace);
    this.setHttpStatus(httpStatus);
    this.populateExtraDetails(cause);
  }

  private void populateExtraDetails(Throwable throwable)
  {
    // If exception is of transactional type, usually extra details available
    Map<String, Object> details = new HashMap<>();

    if(throwable instanceof TransactionSystemException)
    {
      TransactionSystemException tse = (TransactionSystemException) throwable;
      Throwable originalException = tse.getMostSpecificCause();

      // TODO: Add more conditions for known exceptions if it's useful
      if (originalException instanceof ConstraintViolationException)
      {
        ConstraintViolationException cve = (ConstraintViolationException) originalException;
        List<String> violations = new ArrayList<>();

        for (ConstraintViolation cv : cve.getConstraintViolations())
        {
          violations.add(cv.getPropertyPath() + " - " + cv.getMessage());
        }

        details.put("constraint-violations", violations);
        this.setHttpStatus(HttpStatus.UNPROCESSABLE_ENTITY);
      } else
      {
        details.put("message", originalException.getMessage());
      }
    }
    else if(throwable instanceof DataIntegrityViolationException)
    {
      details.put("integrity-violation", ((DataIntegrityViolationException) throwable).getMostSpecificCause().getMessage());
      this.setHttpStatus(HttpStatus.NOT_ACCEPTABLE);
    }

    this.setDetails(details);
  }

  /**
   * Returns HTTP Status code for this Exception.
   *
   * @return {@link HttpStatus} code.
   */
  public HttpStatus getHttpStatus()
  {
    return httpStatus;
  }

  /**
   * Sets HTTP Status code for this Exception.
   *
   * @param httpStatus {@link HttpStatus} code to set.
   */
  public void setHttpStatus(final HttpStatus httpStatus)
  {
    this.httpStatus = httpStatus;
  }

  /**
   * Returns any extra detail messages about the exception.
   *
   * @return May be {@code null} if no extra details available.
   */
  public Map<String, Object> getDetails()
  {
    return details;
  }

  /**
   * Sets any extra detail message.
   *
   * @param details extra detail information.
   */
  public void setDetails(final Map<String, Object> details)
  {
    this.details = details;
  }

  /**
   * Returns custom tag assigned to this exception.
   *
   * @return custom tag.
   */
  public String getTag()
  {
    return tag;
  }

  /**
   * Sets the tag of this exception.
   * <p>
   *   If you want to categorize certain exceptions, so that they can be searched
   *   on, set it via this method. <br/><br/>
   *   Later you may search to find out how many of exceptions with {@code XYZ} tag
   *   occurred between today and week ago, etc.
   * </p>
   *
   * @param tag tag for this exception.
   */
  public void setTag(String tag)
  {
    this.tag = tag;
  }
}
