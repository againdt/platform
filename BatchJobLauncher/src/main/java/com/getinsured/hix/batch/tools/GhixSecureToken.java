package com.getinsured.hix.batch.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GhixSecureToken {
	
	private String base64EncodedIv = null;
	private String base64EncodedToken = null;
	private static final Logger logger = LoggerFactory.getLogger(GhixSecureToken.class);
	
	public GhixSecureToken(){
		
	}
	
	public GhixSecureToken(String tokenIv, String tokenData) {
		this.base64EncodedIv = tokenIv;
		this.base64EncodedToken = tokenData;
	}
	
	
	public GhixSecureToken(String value) {
		String[] ivAndData = value.split("&");
		if(ivAndData.length != 2){
			logger.error("Invalid token, invalid format, IV and data should be seperated by '&'");
		}
		String base64iv = ivAndData[0];
		String base64Token = ivAndData[1];
		int equalIndex = base64iv.indexOf("=");
		if(equalIndex != -1){
			this.base64EncodedIv = base64iv.substring(equalIndex+1);
		}
		equalIndex = base64Token.indexOf("=");
		if(equalIndex != -1){
			this.base64EncodedToken = base64Token.substring(equalIndex+1);
		}
	}
	
	public String getBase64EncodedIv() {
		return base64EncodedIv;
	}
	public void setBase64EncodedIv(String base64EncodedIv) {
		this.base64EncodedIv = base64EncodedIv;
	}
	public String getBase64EncodedToken() {
		return base64EncodedToken;
	}
	public void setBase64EncodedToken(String base64EncodedToken) {
		this.base64EncodedToken = base64EncodedToken;
	}
	
	public String toString(){
		return "tokenIv="+this.base64EncodedIv+"&tokenData="+this.base64EncodedToken;
	}
}
