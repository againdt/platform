package com.getinsured.hix.batch.tools;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Stack;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SAMLTokenValidator {
	public static final String FORMAT = "yyyy-MMM-dd HH:mm:ss zzz"; 
	private static Logger logger = LoggerFactory.getLogger(SAMLTokenValidator.class);
	private static SecretKeySpec secret = null;
	private static final int CLOCK_OFFSET_ALLOWANCE = 30000; //milliseconds
	//Encryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<TokenEnv> activeEncryptorPool = new Stack<TokenEnv>();
	private static Stack<TokenEnv> standbyEncryptorPool = new Stack<TokenEnv>();
	
	//Decryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<TokenEnv> activeDecryptionPool = new Stack<TokenEnv>();
	private static Stack<TokenEnv> standbyDecryptionPool = new Stack<TokenEnv>();
	private static SecureRandom random  = new SecureRandom();
	private static final int POOL_SIZE = 200;
	private static boolean poolInitialized = false;
	private static long emptyEncPoolEncountered = 0;
	private static long emptyDecPoolEncountered = 0;
	

	private static class TokenEnv{
		private Cipher cipher = null;
		private SimpleDateFormat dateForMatter;
		TokenEnv(Cipher cipher){
			this.cipher = cipher;
			this.dateForMatter = new SimpleDateFormat(FORMAT);
			this.dateForMatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		}
		
	}
	
	/**
	 * Only called when DEBUG is enabled
	 */
	private static synchronized void updateEncPoolEmptyCount(){
		emptyEncPoolEncountered++;
		logger.debug("Enc pool empty count:"+emptyEncPoolEncountered);
	}
	
	/**
	 * Only called when DEBUG is enabled
	 */
	private static synchronized void updateDecPoolEmptyCount(){
		emptyDecPoolEncountered++;
		logger.debug("Dec pool empty count:"+emptyDecPoolEncountered);
	}
	
	public static boolean validateToken(String password,GhixSecureToken token, int maxValidity, HashSet<String> trustedParties) throws InvalidTokenException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE, password);
		}
		String ivStr = token.getBase64EncodedIv();
		if(ivStr == null){
			throw new InvalidTokenException("No IV found, invalid token");
		}
		logger.debug("IV Str received:"+ivStr);
		String tokenStr = token.getBase64EncodedToken();
		if(tokenStr == null){
			throw new InvalidTokenException("No token found");
		}
		logger.debug("Token Str received:"+tokenStr);
		
		byte[] iv = Base64.decodeBase64(ivStr);
		byte[] cipherTextToken = Base64.decodeBase64(tokenStr);
		Cipher cipher = null;
		TokenEnv cipherEnv = null;
		SimpleDateFormat sdf = null;
		try{
			cipherEnv = getActiveDecryptorPool().pop();
		}catch(EmptyStackException es){
			logger.warn("Validate token failed to retrieve the cipher environment from the pool, initializing new");
			cipherEnv = getDecryptionCipherEnv();
			if(logger.isDebugEnabled()){
				updateDecPoolEmptyCount();
			}
		}
		cipher = cipherEnv.cipher;
		sdf = cipherEnv.dateForMatter;
		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
		
		String plaintext = new String(cipher.doFinal(cipherTextToken), "UTF-8");
		standbyDecryptionPool.push(cipherEnv); // Return this cipher to the pool
		
		String[] aliasColonTime = plaintext.split("#");
		if(aliasColonTime.length != 3){
			throw new InvalidTokenException("Invalid token format received expected <alias>:<time originated in millis> found:"+aliasColonTime);
		}
		if(trustedParties != null && !trustedParties.contains(aliasColonTime[0])){
			throw new InvalidTokenException("Invalid alias token received, expected one of "+trustedParties+" found:"+aliasColonTime[0]);
		}
		String timeStr = aliasColonTime[1];
		Date tokenInitTime = null;
		try {
			tokenInitTime = sdf.parse(timeStr.trim());
		} catch (ParseException e) {
			throw new InvalidTokenException("["+e.getClass().getName()+":"+e.getMessage()+"] Invalid timestamp ["+timeStr+"] received with token["+plaintext+"]");
		}
		long timeNow = System.currentTimeMillis();
		Date allowedRemoteInitTime = new Date(timeNow- maxValidity);
		Date now = new Date(timeNow +CLOCK_OFFSET_ALLOWANCE);
		if(!tokenInitTime.equals(now) && (tokenInitTime.after(allowedRemoteInitTime) && tokenInitTime.before(now)) ){
			if(logger.isDebugEnabled()) {
				logger.debug("Validated secure token from "+aliasColonTime[0]+" Valid till:"+timeStr+ " Current Time:"+sdf.format(now));
			}
			return true;
		}
		throw new InvalidTokenException("Received expired or invalid token from Entity ["+aliasColonTime[0]+"], Current time (with a variance of "+CLOCK_OFFSET_ALLOWANCE+" milliseconds):["+sdf.format(now)+"] has passed token validity time ["+timeStr+"] allowed validity: "+maxValidity/1000+" seconds");
		
	}
	
	
	private static synchronized void initializCipherPools(int poolSize, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(poolInitialized){
			return;
		}
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			activeEncryptorPool.add(new TokenEnv(cipher));
		}
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			activeDecryptionPool.add(new TokenEnv(cipher));
		}
		poolInitialized = true;
	}
	
	private static TokenEnv getDecryptionCipherEnv() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Decryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		return new TokenEnv(cipher);
	}
	
	private static synchronized TokenEnv getEncryptionCipherEnv() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Encryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		return new TokenEnv(cipher);
	}
	
	private static synchronized Stack<TokenEnv> getActiveEncryptorPool(){
		Stack<TokenEnv> emptyBank;
		try{
			activeEncryptorPool.peek();
		}catch(EmptyStackException es){
						
			if(logger.isDebugEnabled()) {
				logger.debug("Switching Encryption Cipher Banks");
			}
			emptyBank = activeEncryptorPool;
			activeEncryptorPool = standbyEncryptorPool;
			standbyEncryptorPool = emptyBank;
		}
		return activeEncryptorPool;
	}
	
	private static synchronized Stack<TokenEnv> getActiveDecryptorPool(){
		Stack<TokenEnv> emptyBank;
		try{
			activeDecryptionPool.peek();
		}catch(EmptyStackException es){
			if(logger.isDebugEnabled()) {
				logger.debug("Switching Decryption Cipher Banks");
			}
			emptyBank = activeDecryptionPool;
			activeDecryptionPool = standbyDecryptionPool;
			standbyDecryptionPool = emptyBank;
		}
		return activeDecryptionPool;
	}
	
	/**
	 * 
	 * @param password
	 * @param trustedEntityName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 */
	public static GhixSecureToken generateToken(String password,String trustedEntityName) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE, password);
		}
		//Random double has been added to insure token uniqueness.
		
		//System.out.println("Input Token data:"+tokenDataIn);
		Cipher cipher = null;
		TokenEnv cipherEnv = null;
		SimpleDateFormat sdf = null;
		try{
			cipherEnv = getActiveEncryptorPool().pop();
		}catch(EmptyStackException es){
			logger.warn("Generate Token Failed to get cipher env from the pool, initializing a new one");
			cipherEnv = getEncryptionCipherEnv();
			if(logger.isDebugEnabled()){
				updateEncPoolEmptyCount();
			}
		}
		cipher = cipherEnv.cipher;
		sdf = cipherEnv.dateForMatter;
		// Add a random to ensure uniqueness of the token across the JVM's in a culstered environment
		String tokenDataIn = trustedEntityName+"#"+sdf.format(new Date(System.currentTimeMillis()))+"#"+random.nextDouble();
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal((tokenDataIn).getBytes("UTF-8"));
		standbyEncryptorPool.push(cipherEnv); //Send the cipher environment to standby pool
		GhixSecureToken token = new GhixSecureToken();
		token.setBase64EncodedToken(new String(Base64.encodeBase64URLSafe(ciphertext)));
		token.setBase64EncodedIv(new String(Base64.encodeBase64URLSafe(iv)));
		if(logger.isDebugEnabled()) {
			logger.debug("BASE 64 TOKEN: "+token.toString());
		}
		
		return token;
	}
	
	public static GhixModuleAccessToken generateModuleAccessToken(String emailId, String originatingModule, Cipher cipher) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		// Add a random to ensure uniqueness of the token across the JVM's in a culstered environment
		String tokenDataIn = random.nextDouble()+"#"+emailId+"#"+originatingModule+"#"+sdf.format(new Date(System.currentTimeMillis()));
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal((tokenDataIn).getBytes("UTF-8"));
		GhixSecureToken token = new GhixSecureToken();
		token.setBase64EncodedToken(new String(Base64.encodeBase64URLSafe(ciphertext)));
		token.setBase64EncodedIv(new String(Base64.encodeBase64URLSafe(iv)));
		if(logger.isDebugEnabled()) {
			logger.debug("BASE 64 TOKEN: "+token.toString());
		}
		
		return new GhixModuleAccessToken(token);
	}
	
	public static GhixModuleAccessToken generateModuleAccessToken(int userId, String originatingModule, Cipher cipher) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		// Add a random to ensure uniqueness of the token across the JVM's in a culstered environment
		String tokenDataIn = random.nextDouble()+"#"+userId+"#"+originatingModule+"#"+sdf.format(new Date(System.currentTimeMillis()));
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal((tokenDataIn).getBytes("UTF-8"));
		GhixSecureToken token = new GhixSecureToken();
		token.setBase64EncodedToken(new String(Base64.encodeBase64URLSafe(ciphertext)));
		token.setBase64EncodedIv(new String(Base64.encodeBase64URLSafe(iv)));
		if(logger.isDebugEnabled()) {
			logger.debug("BASE 64 TOKEN: "+token.toString());
		}
		
		return new GhixModuleAccessToken(token);
	}
	
}
