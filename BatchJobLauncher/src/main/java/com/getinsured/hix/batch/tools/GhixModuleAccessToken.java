package com.getinsured.hix.batch.tools;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GhixModuleAccessToken {
	
	private GhixSecureToken token;
	private static final Logger logger = LoggerFactory.getLogger(GhixModuleAccessToken.class);

	public GhixModuleAccessToken(GhixSecureToken token){
		this.token = token;
	}
	
	
	private static Cipher getEncryptionCipher(String password) throws Exception{
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		return cipher;
	}
	
	private static Cipher getDecryptionCipher(String password, byte[] iv) throws Exception{
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secret,new IvParameterSpec(iv));
		return cipher;
	}
	
	public static GhixModuleAccessToken getModuleAccessToken(String keyPassword) {
		int clientId = 11;
		GhixModuleAccessToken token = null;
		try {
			Cipher cipher = getEncryptionCipher(keyPassword);
			token = SAMLTokenValidator.generateModuleAccessToken(clientId, "ghix-web",cipher);
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to generate the module access token:"+e.getMessage());
		}
		return token;
	}
	
	public static GhixModuleAccessToken getModuleAccessToken(String userName, String keyPassword) {
			GhixModuleAccessToken token = null;
			try {
				Cipher cipher = getEncryptionCipher(keyPassword);
				token = SAMLTokenValidator.generateModuleAccessToken(userName, "ghix-web",cipher);
				
			} catch (Exception e) {
				throw new RuntimeException("Failed to generate the module access token:"+e.getMessage());
			}
			return token;
	}
	
	public String toString(){
		return "tokenIv="+this.token.getBase64EncodedIv()+"&tokenData="+this.token.getBase64EncodedToken();
	}
}
