package com.getinsured.hix.batch.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.util.Properties;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class BatchJobLauncher {
	private static CloseableHttpClient httpclient = null;
	private static String serverURL = null;
	static Properties prop = new Properties();
	
	public enum BATCH_STATUS{
		 COMPLETED, STARTING, STARTED, STOPPING, STOPPED, FAILED, ABANDONED, UNKNOWN, NOT_STARTED
	};
	
	public static void main(String[] args) throws IOException {
		Logger.getRootLogger().setLevel(Level.OFF);

		/*
		 Usage:
		  BatchLauncher.jar <action>    	--start  
		                    <keypassword> 	--ghix1234
		  					<username>  	--exadmin@ghix.com 
		  					<serverURL> 	--https://caupgrade4qa.ghixqa.com 
							<jobName> 		--enrollmentXMLIndividualJob
		  					<Job params> 	-- runId=2 Start_Date=20161220011900 End_Date=20161220012100
		 */

		String action = args[0];

		if(action != null){
			String keyPassword = args[1];
			String userName = args[2]; //getUserInput(user_name_str, null);
			serverURL = args[3]; //  getUserInput(server_url_str, null);

			BatchJobLauncher jobLauncher = new BatchJobLauncher();
			jobLauncher.loadProperties();
			// getUserInput(key_str, null);

			System.out.println("Authenticating user :  " + userName);

			while(!jobLauncher.initHttpClient()){
				System.out.println("Invalid keystore password");
				System.exit(1);
			}
			
			if(!jobLauncher.authenticateUser(userName, keyPassword)){
				System.out.println("Authentication failed, either the provided username does not exist or it does not have right to access Batch admin. Exiting.");
				System.exit(1);
			}

			System.out.println(userName + " authenticated.");

			if(action.compareToIgnoreCase("start") == 0){

				String jobName = args[4]; // getUserInput(job_name_str, null);
				String jobParams = "";
				for(int i=5; i < args.length; i++){
					jobParams += args[i] +"," ;// getUserInput(job_param_str, null);
				}
				if(jobParams.endsWith(",")) {
					jobParams = jobParams.substring(0,jobParams.length()-1);
				}

				if(jobName != null && !jobName.isEmpty()){
					BATCH_STATUS exitCode = jobLauncher.launchJob(jobName, jobParams);
					if( exitCode != BATCH_STATUS.COMPLETED){	
						System.out.println("Error in launching the job");
					}
					System.exit(exitCode.ordinal());
				}
			}else if(action.compareToIgnoreCase("stop") == 0){

				String jobExecutionId = args[4]; //getUserInput(user_name_str, null);
				if(!jobLauncher.stopJobExecution(jobExecutionId)){	
					System.out.println("Could not stop the job, it may not be running.");
				}
			}else if(action.compareToIgnoreCase("status") == 0){
				String exitStatus = null;
				String jobExecutionId = args[4]; //getUserInput(user_name_str, null);

				JSONObject objStatus = jobLauncher.getJobExecutionStatus(jobExecutionId);
				String jobStatus = null;
				if(objStatus != null) {
					jobStatus = (String) objStatus.get("Status");
				}

				System.out.println("Status: " + jobStatus);
			}
		}
	}


	private boolean authenticateUser(String userName, String keyPassword){
		boolean bAuthenticated = false;

		StringBuffer stAuthUrl = new StringBuffer();
		GhixModuleAccessToken ghixModuleAccessToken = GhixModuleAccessToken.getModuleAccessToken(userName, keyPassword);
		stAuthUrl = new StringBuffer(serverURL).append("/ghix-batch/account/user/login?").append(ghixModuleAccessToken.toString());

		HttpGet httpget;

		try {
			//	System.out.println("authURL: " + stAuthUrl);
			httpget = new HttpGet (stAuthUrl.toString());
			HttpResponse response  = httpclient.execute(httpget);
			//	System.out.println("response = " + response);
			if(response != null){
				String output;
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				while ((output = br.readLine()) != null) {
					if(output.indexOf("Spring Batch Admin") >= 0){
						bAuthenticated = true;
						break;
					}
				}
				br.close();
			}
		} catch (IOException  e1) {
			e1.printStackTrace();
			System.out.println("Exception caught in authenticateUser : " + e1.getMessage());
		} 		

		return bAuthenticated;
	}

	public static StringBuilder getBatchJobList() {
		StringBuilder stUri = new StringBuilder();

		if(serverURL == null){
			stUri.append("http://localhost:8080/ghix-batch/listBatchJobs");
		}else{
			stUri.append(serverURL + "/ghix-batch/listBatchJobs");
		}
		HttpResponse response = doGetRequest(stUri.toString());
		StringBuilder stJobNames = new StringBuilder();
		BufferedReader br;
		try {
			String output;
			br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((output = br.readLine()) != null) {
				stJobNames.append(output);
			}
			br.close();

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return stJobNames;
	}

	public JSONObject getJobExecutionStatus(String jobExecutionId) {
		StringBuilder stUri = new StringBuilder();
		JSONObject obj = null;
		if(serverURL == null){
			stUri.append("http://localhost:8080/ghix-batch/getJobStatus?jobExecutionId="+jobExecutionId);
		}else{
			stUri.append(serverURL + "/ghix-batch/getJobStatus?jobExecutionId="+jobExecutionId);
		}

		StringEntity params = null;
		try {
			params = new StringEntity("{\"jobExecutionId\":\"" + jobExecutionId + "\"} ");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		HttpResponse response =  doGetRequest(stUri.toString());
		//System.out.println("response = " + response);

		if(response != null && response.getStatusLine().getStatusCode() == 200){
			try {
				HttpEntity responseEntity = response.getEntity();

				JSONParser parser = new JSONParser();
				obj = (JSONObject) parser.parse(new BufferedReader(
						new InputStreamReader(responseEntity.getContent())));


			}catch (Exception e) {
				e.printStackTrace();
			}

		}
		return obj; 
	}


	public boolean stopJobExecution(String jobExecutionId) {
		boolean status = false;

		StringBuilder stUri = new StringBuilder();

		if(serverURL == null){
			stUri.append("http://localhost:8080/ghix-batch/stopJobExecution/");
		}else{
			stUri.append(serverURL + "/ghix-batch/stopJobExecution/");
		}

		StringEntity params = null;
		try {
			params = new StringEntity("{\"jobExecutionId\":\"" + jobExecutionId + "\"} ");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		HttpResponse response =  doPostRequest(stUri.toString(), params);
		//	System.out.println(response);
		if(response != null && response.getStatusLine().getStatusCode() == 200){
			BufferedReader br;
			try {
				br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String output;
				while ((output = br.readLine()) != null) {
					if(output.indexOf("Batch job stopped successfully") >= 0){
						status = true;
						System.out.println("Job stopped successfully.");
						break;
					}else{
						System.out.println(output);
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}

		}
		return status;
	}
	public BATCH_STATUS launchJob(String jobName, String jobParams) {
		BATCH_STATUS retCode = BATCH_STATUS.NOT_STARTED;

		StringBuilder stUri = new StringBuilder();

		if(serverURL == null){
			stUri.append("http://localhost:8080/ghix-batch/launchJob/");
		}else{
			stUri.append(serverURL + "/ghix-batch/launchJob/");
		}

		StringEntity params = null;
		try {
			params = new StringEntity("{\"jobName\":\"" + jobName + "\", \"jobParams\":\"" + jobParams + "\"} ");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} 
		HttpResponse response =  doPostRequest(stUri.toString(), params);

		if(response != null && response.getStatusLine().getStatusCode() == 200){
			HttpEntity responseEntity = response.getEntity();

			BufferedReader br;
			try {
				br = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(new BufferedReader(
						new InputStreamReader(responseEntity.getContent())));

				if(((String)obj.get("Status")).equalsIgnoreCase("Job launched successfully")) {
					Long jobExecutionId = (Long) obj.get("executionId");
					String exitCode = null;
					JSONObject objStatus = getJobExecutionStatus(jobExecutionId.toString());
					String jobStatus = null;
					if(objStatus != null) {
						jobStatus = (String) objStatus.get("Status");
						exitCode = (String) objStatus.get("exitCode");
					}

					while(objStatus != null && !(jobStatus.equalsIgnoreCase("COMPLETED") || jobStatus.equalsIgnoreCase("STOPPED") || jobStatus.equalsIgnoreCase("FAILED")
							 || jobStatus.equalsIgnoreCase("ABANDONED"))) {

						jobStatus = (String) objStatus.get("Status");
						exitCode = (String) objStatus.get("exitCode");
						if(jobStatus != null) {
							if(jobStatus.equalsIgnoreCase("Failed to get job status")) {
								retCode = BATCH_STATUS.NOT_STARTED;
								break;
							}

							Thread.sleep(30000); // wait for 30 secs before checking the job status again
							objStatus = getJobExecutionStatus(jobExecutionId.toString());
							if(exitCode.compareToIgnoreCase("ÜNKNOWN") == 0){
								System.out.println("Current Job Status:" + jobStatus + ", Exit code = " + exitCode + " (may be in-progress)");
							}else{
								System.out.println("Current Job Status:" + jobStatus + ", Exit code = " + exitCode);
							}
						}
					}
					
					if(jobStatus.equalsIgnoreCase("COMPLETED")) {
						retCode = BATCH_STATUS.COMPLETED;
					}else if( jobStatus.equalsIgnoreCase("STOPPED")) {
						retCode = BATCH_STATUS.STOPPED;
					}else if(jobStatus.equalsIgnoreCase("FAILED")) {
						retCode = BATCH_STATUS.FAILED;
					}else if (jobStatus.equalsIgnoreCase("ABANDONED")){
						retCode = BATCH_STATUS.ABANDONED;
					}
					

				}



			}catch (Exception e) {
				e.printStackTrace();
			}

		}
		return retCode;
	}

	private void loadProperties(){
		InputStream input = null;
		try {
			input = getClass().getClassLoader().getResourceAsStream("config.properties");

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private boolean initHttpClient(){
		boolean initSuccess = false;
		String key = prop.getProperty("keystorepassword");
		String keyStorePath = prop.getProperty("keystorepath");
		String keyStoreFile = prop.getProperty("keyStorefile");
		
		String ghixHome = System.getProperty("GHIX_HOME");
		
		if(keyStorePath == null){
			File currentDirectory = new File(new File(".").getAbsolutePath());
			try {
				keyStorePath = currentDirectory.getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(key == null || keyStorePath == null || keyStoreFile == null){
			System.out.println("Keystore not available as per configuration");
		}

		try{
			if(ghixHome != null){
				keyStorePath = keyStorePath.replace("${GHIX_HOME}", ghixHome);
			}
			if(httpclient == null){
				InputStream instream = null;

				KeyStore trustStore = null;
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

				instream = new FileInputStream(getKeyStoreFile(keyStorePath, keyStoreFile));
				
				trustStore.load(instream, key.toCharArray());
				//	System.out.println("Done creating trust store...");

				SSLContext sslContext = SSLContexts.custom()
						.useTLS()
						.loadTrustMaterial(trustStore)
						.build();
				SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
				httpclient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
				initSuccess = true;
			}
		}catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Exception caught in initHttpClient");
		}
		return initSuccess;
	}
	private static File getKeyStoreFile(String location,String file){
		//		System.out.println("Keystore Location:"+location);
		File keyStoreDir = new File(location);
		File keyStore = new File(keyStoreDir,file);
		if(!keyStore.exists() || !keyStore.isFile()){
			System.out.println("Key store ["+keyStore.getAbsolutePath()+"] does not exist Or its not a file");
			keyStore = null;
		}
		return keyStore;
	}
	private static HttpResponse doGetRequest(String uri){
		HttpGet request = new HttpGet(uri);
		HttpResponse response = null;
		try {
			response = httpclient.execute(request);
		    // Printing the response
		//	System.out.println("Response :  " + response);
			/*			System.out.println("error code :- "
					+ response.getStatusLine().getStatusCode()); */

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private static HttpResponse doPostRequest(String uri, StringEntity params){
		HttpPost request = new HttpPost(uri);

		request.setEntity(params);
		HttpResponse response = null;
		try {
			response = httpclient.execute(request);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

}

