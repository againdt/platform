package com.getinsured.hix.account;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.security.KeyStore;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateWSO2ForExistingUsers {
	private static String server_url_str = "Server URL:\t\t\t";
	private static String db_url_str = "Database URL:\t\t\t";
	private static String userName_Str = "DB User:\t\t\t";
	private static String password_Str = "DB password:\t\t\t";

	private static CloseableHttpClient httpclient = null;
	private static String serverURL = null;
	private static String username = null;
	private static String password = null;
	private static String dbUrl = null;
	private static Connection con = null;

	private static Logger LOGGER = LoggerFactory.getLogger(UpdateWSO2ForExistingUsers.class);
	private static final File updatedUserRecordsFile = new File("UpdatedUserRecords.txt");
	private static HashSet<Integer> userRecords = new HashSet<Integer>();
	
	private static final long TWO_DAYS= 2*24*60*60*1000L;
	private static final long SIXTY_DAYS = 60*24*60*60*1000L;
	private static final long SEVEN_DAYS = 7*24*60*60*1000L;
	private static final long START_TIME= (System.currentTimeMillis() - SIXTY_DAYS)+SEVEN_DAYS;
	private static long CURRENT_TIMESTAMP = START_TIME;
	
	static {
		try {
			if (!updatedUserRecordsFile.exists()) {
				updatedUserRecordsFile.createNewFile();
				LOGGER.info("Created local store for updated records");
			}
			Integer userId;
			DataInputStream dis = new DataInputStream(new FileInputStream(updatedUserRecordsFile));
			try {
				CURRENT_TIMESTAMP = dis.readLong();
				while (true) {
					userId = dis.readInt();
					userRecords.add(userId);
				}
			} catch (EOFException eof) {
				dis.close();
				LOGGER.info("Loaded previously updated records["+userRecords.size()+" 0 indicates this is the first time we are going to update the records from this directory]");
			}
		} catch (IOException e) {
			LOGGER.error("Error encountered while loading the list of already updated records",e);
		}
	}

	private static class UserInfo {
		private int id;
		private String email;

		UserInfo(int id, String email) {
			this.id = id;
			this.email = email;
		}

		int getId() {
			return this.id;
		}

		String getEmail() {
			return this.email;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserInfo other = (UserInfo) obj;
			if (email == null) {
				if (other.email != null)
					return false;
			} else if (!email.equalsIgnoreCase(other.email))
				return false;
			if (id != other.id)
				return false;
			return true;
		}
		
		@Override
		public String toString(){
			return this.id +"-"+this.email;
		}

	}

	private static void persistSession() {
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		try {
			fos = new FileOutputStream(updatedUserRecordsFile);
			dos = new DataOutputStream(fos);
			dos.writeLong(CURRENT_TIMESTAMP);
			Iterator<Integer> cursor = userRecords.iterator();
			while (cursor.hasNext()) {
				dos.writeInt(cursor.next());
				dos.flush();
			}
		} catch (IOException e) {
			LOGGER.error("Error encountered while persisting the session for updated records", e);
			LOGGER.info("Dumping the session data on the console");
			Iterator<Integer> cursor = userRecords.iterator();
			while (cursor.hasNext()) {
				LOGGER.info(cursor.next().toString());
			}
		} finally {
			try {
				dos.close();
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static String getUserInput(String paramName, String defaultVal) throws IOException {
		boolean inputProvided = false;
		String val = null;
		while (!inputProvided) {
			if (paramName != null) {
				System.out.print(paramName + "::" + ((defaultVal == null) ? ("   ") : ("[" + defaultVal) + "]   "));
			}
			val = new BufferedReader(new InputStreamReader(System.in)).readLine();
			if (val == null || val.length() == 0) {
				if (defaultVal != null) {
					System.out.println("");
					return defaultVal;
				} else {
					continue;
				}
			}
			inputProvided = true;
			// System.out.println(paramName + " = " + val);
		}
		if (val.equalsIgnoreCase("\\q")) {
			System.out.println("Exiting WSO2 User Update utility.......Bye");
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.exit(0);
		}
		return val;
	}

	public static void main(String[] args) throws IOException {

		serverURL = getUserInput(server_url_str, null);
		dbUrl = getUserInput(db_url_str, null);
		username = getUserInput(userName_Str, null);
		password = getUserInput(password_Str, null);
		String keyStorePassword = getUserInput("Keystore password\t\t", null);

		Set<UserInfo> users = getPrivilegedUsersInfo();

		initHttpClient(keyStorePassword);

		// update time stamps for all privileged users on WSO2
		updateUsers(users);
		
	}

	/**
	 * Fetch all the users in one go, otherwise there are chances that connection times out while updating
	 * the users on WSO2
	 * @return
	 */
	private static Set<UserInfo> getPrivilegedUsersInfo() {
		ResultSet rs = null;
		Set<UserInfo> userSet = null;
		try {
			// step1 load the driver class
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// step2 create the connection object
			con = DriverManager.getConnection(dbUrl, username, password);

			// step3 create the statement object
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			// step4 execute query
			rs = stmt.executeQuery("select id, username from users where id in (select distinct(user_id) from user_roles where role_id in (select id from roles where PWD_CONSTRAINTS_ENABLED='Y'))");
			int size = 0;
			if(rs.last()){
				size = rs.getRow();
				rs.beforeFirst();
			}
			LOGGER.info("Attemting to read "+size+" user entries from the DB");
			int read = 0;
			userSet = new HashSet<UserInfo>(size);
			while (rs.next()) {
				read++;
				int userId = rs.getInt(1);
				if(userRecords.contains(userId)){
					LOGGER.info("User Id"+userId+" with Email:"+rs.getString(2)+" Already processed, skipping");
					continue;
				}
				userSet.add(new UserInfo(userId, rs.getString(2)));
			}
			LOGGER.info("Read "+read+" Entried from the DB, Processing :"+userSet.size()+" Skipped rmaining, if any");

		} catch (Exception e) {
			LOGGER.error("Failed to retrieve the user set", e);
		} finally{
			try {
				if(con != null){
					con.close();
				}
			} catch (SQLException e) {
				LOGGER.error("Exception in closing db connection: " + e.getMessage(),e);
			}
		}
		return userSet;
	}

	@SuppressWarnings("unchecked")
	private static boolean isUserExistingonWSO2(String email) {
		boolean userExist = false;
		StringBuilder stUrl = new StringBuilder();
		if (serverURL == null) {
			stUrl.append("http://localhost:8080/ghix-identity/isExistingUser");
		} else {
			stUrl.append(serverURL + "/ghix-identity/isExistingUser");
		}
		String msg = "Checking if user " + email + " Exists on WSO2";
		try {
			JSONObject request = new JSONObject();
			request.put("clientIp", InetAddress.getLocalHost().getHostAddress());
			JSONObject payload = new JSONObject();
			payload.put("userName", email);
			request.put("payload", payload);
			HttpPost httppost = new HttpPost(stUrl.toString());
			StringEntity params = new StringEntity(request.toString());
			httppost.addHeader("content-type", "application/json");
			httppost.setEntity(params);

			HttpResponse postResponse = null;
			postResponse = httpclient.execute(httppost);

			if (postResponse != null) {
				BufferedReader br;
				try {
					String output;
					br = new BufferedReader(new InputStreamReader(postResponse.getEntity().getContent()));
					output = br.readLine();
					br.close();
					if (Boolean.valueOf(output) == true) {
						userExist = true;
					}

				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Failed to check if user exists on WSO2", e);
		}
		msg += "......." + userExist;
		LOGGER.info(msg);
		return userExist;
	}
	
	
	public static long getPassworTimestamp(){
		if(userRecords.size()%50 ==0){
			CURRENT_TIMESTAMP += TWO_DAYS;
			return CURRENT_TIMESTAMP;
		}
		return CURRENT_TIMESTAMP;
	}
	
	private static String getDateString(long timestamp){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		return sdf.format(new Date(timestamp));
	}
	
	@SuppressWarnings("unchecked")
	private static boolean updatePasswordTimeStamp(String email, HttpPost httppost){
		long passwordTimeStamp = getPassworTimestamp();
		try{
			JSONObject request = new JSONObject();
			request.put("clientIp", InetAddress.getLocalHost().getHostAddress());
			JSONObject payload = new JSONObject();
			payload.put("userName", email);
			payload.put("claimURI", "http://wso2.org/claims/passwordTimestamp");
			payload.put("claimValue",String.valueOf(passwordTimeStamp));
			request.put("payload", payload);
			StringEntity params = new StringEntity(request.toString());
			httppost.addHeader("content-type", "application/json");
			httppost.setEntity(params);
			HttpResponse postResponse = null;
			postResponse = httpclient.execute(httppost);
			if (postResponse != null) {
				try {
					new InputStreamReader(postResponse.getEntity().getContent()).close();
				} catch (IOException e) {
					LOGGER.error("Failed to update the password timestamp for user :"+email,e);
					return false;
				}
			}
		}catch(Exception e){
			LOGGER.error("failed to update the password timestamp for user "+email,e);
			return false;
		}
		LOGGER.info("User:"+email+"'s password will expire on "+getDateString(passwordTimeStamp+SIXTY_DAYS));
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private static boolean updateAccountTypeCode(String email, HttpPost httppost){
		JSONObject payload2 = new JSONObject();
		try{
		payload2.put("userName", email);
		payload2.put("claimURI", "http://wso2.org/claims/accountTypeCode");
		payload2.put("claimValue", "1");
		JSONObject request2 = new JSONObject();
		request2.put("clientIp", InetAddress.getLocalHost().getHostAddress());
		request2.put("payload", payload2);

		StringEntity params2 = new StringEntity(request2.toString());
		httppost.addHeader("content-type", "application/json");
		httppost.setEntity(params2);

		CloseableHttpResponse postResponse = httpclient.execute(httppost);

		if (postResponse != null) {
			try {
				new InputStreamReader(postResponse.getEntity().getContent()).close();

			} catch (IOException e) {
				LOGGER.error("Failed to read the response for update account type code",e);
				return false;
			}
		}
		}catch(Exception e){
			LOGGER.error("failed to update the Account Type code for user "+email,e);
			return false;
		}
		return true;
	}

	private static void updateUsers(Set<UserInfo> userSet) {
		if (userSet == null || userSet.size() == 0) {
			LOGGER.info("Empty result set, nothing to update");
			return;
		}
		Iterator<UserInfo> rs = userSet.iterator();
		StringBuilder stUrl = new StringBuilder();
		if (serverURL == null) {
			stUrl.append("http://localhost:8080/ghix-identity/updateUser");
		} else {
			stUrl.append(serverURL + "/ghix-identity/updateUser");
		}
		LOGGER.info("Using server URL:" + stUrl);
		UserInfo user = null;
		
		while (rs.hasNext()) {
			boolean updated = false;
			user = rs.next();
			String email = user.getEmail();
			boolean userExists = isUserExistingonWSO2(email);
			if (userExists) {
				updated = updatePasswordTimeStamp(email,new HttpPost(stUrl.toString()));
				if(!updated){
					LOGGER.error("Failed to update he timestamp, not attempting to update the account type code for "+email);
					continue;
				}
				updated = updateAccountTypeCode(email,new HttpPost(stUrl.toString()));
				if(!updated){
					LOGGER.error("Updated password timestamp for user:"+email+" But failed to update the account Type code, requires manual update on WSO2");
					continue;
				}
				userRecords.add(user.getId());
			}
		}
		persistSession();
	}

	private static boolean initHttpClient(String key) {
		boolean initSuccess = false;
		try {
			if (httpclient == null) {
				InputStream instream = null;

				KeyStore trustStore = null;
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

				String ghix_home = System.getenv("GHIX_HOME");
				LOGGER.info("Using GHIX_HOME="+ghix_home);

				if (ghix_home == null) {
					ghix_home = "/home/tomcat/ghixhome";
				}
				instream = new FileInputStream(getKeyStoreFile(ghix_home + "/ghix-setup/conf", "platformKeyStore.jks"));

				trustStore.load(instream, key.toCharArray());
				LOGGER.info("Done creating trust store...");

				SSLContext sslContext = SSLContexts.custom().useTLS().loadTrustMaterial(trustStore).build();
				SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
				httpclient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
				initSuccess = true;
			}
		} catch (Exception e) {
			LOGGER.error("Exception while initializing the http client",e);
		}
		return initSuccess;
	}

	private static File getKeyStoreFile(String location, String file) {
		LOGGER.info("Keystore Location:" + location);
		File keyStoreDir = new File(location);
		File keyStore = new File(keyStoreDir, file);
		if (!keyStore.exists() || !keyStore.isFile()) {
			LOGGER.info("Key store [" + keyStore.getAbsolutePath() + "] does not exist Or its not a file");
			keyStore = null;
		}
		return keyStore;
	}
}
