package com.getinsured.wso2;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.user.api.Claim;
import org.wso2.carbon.user.api.RealmConfiguration;
import org.wso2.carbon.user.core.UserRealm;
import org.wso2.carbon.user.core.UserStoreException;
import org.wso2.carbon.user.core.claim.ClaimManager;
import org.wso2.carbon.user.core.jdbc.JDBCUserStoreManager;
import org.wso2.carbon.user.core.profile.ProfileConfigurationManager;
import org.wso2.carbon.user.core.util.DatabaseUtil;
import org.wso2.carbon.utils.Secret;

import com.getinsured.wso2.springcore_4_2_1.BCryptPasswordEncoder;

public class GhixUserAuth extends JDBCUserStoreManager {
	private static int PASSWORD_STRENGTH = 10;
	private static Log log = LogFactory.getLog(GhixUserAuth.class);
	private BCryptPasswordEncoder passwordEncoder = null;

	public GhixUserAuth(RealmConfiguration realmConfig, Map<String, Object> properties, ClaimManager claimManager,
			ProfileConfigurationManager profileManager, UserRealm realm, Integer tenantId) throws UserStoreException {
		super(realmConfig, properties, claimManager, profileManager, realm, tenantId);
		log.info("GhixUserAuth, A GetInsured user authentication mechanism initialized...");
	}

	public GhixUserAuth() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean doAuthenticate(String userName, Object credential) throws UserStoreException {
		boolean isAuthenticated = false;
		Connection dbConnection = null;
		ResultSet rs = null;
		PreparedStatement prepStmt = null;
		log.info("Initiating authentication for user:" + userName);
		if (userName != null && credential != null) {
			try {
				Secret secret = (Secret) credential;
				String candidatePassword = new String(secret.getChars());
				log.info("Received password:" + candidatePassword);
				
				String sql = null;
				dbConnection = this.getDBConnection();
				dbConnection.setAutoCommit(false);
				// get the SQL statement used to select user details
				sql = this.realmConfig.getUserStoreProperty("SelectUserSQL");
				if (log.isDebugEnabled()) {
					log.debug(sql);
				}

				prepStmt = dbConnection.prepareStatement(sql);
				prepStmt.setString(1, userName);
				// check whether tenant id is used
				if (sql.contains("UM_TENANT_ID")) {
					log.info("Using tenant Id:" + this.tenantId);
					prepStmt.setInt(2, this.tenantId);
				}

				rs = prepStmt.executeQuery();
				String giTenant = null;
				if (rs.next()) {
					String encodedPassword = rs.getString("um_user_password");
					String userSalt = rs.getString("um_salt_value");
					log.info("Retrieved user cradentials:" + encodedPassword + " and UUID:" + userSalt
							+ " with tenant Id:" + giTenant);
					candidatePassword = candidatePassword + userSalt;
					isAuthenticated = this.getEncoder().matches(candidatePassword, encodedPassword);
				}
				log.info(userName + " is authenticated? " + isAuthenticated);
			} catch (SQLException exp) {
				log.error("Error occurred while retrieving user authentication info.", exp);
				throw new UserStoreException("Authentication Failure");
			} catch (org.wso2.carbon.user.api.UserStoreException e) {
				log.error("Error occurred while retrieving user authentication info.", e);
				throw new UserStoreException("Authentication Failure " + e.getMessage());
			}finally{
				if(dbConnection != null){
					DatabaseUtil.closeAllConnections(dbConnection, rs, prepStmt);
				}
			}
		}
		return isAuthenticated;
	}

	private BCryptPasswordEncoder getEncoder() {
		SecureRandom salt = null;
		if (this.passwordEncoder == null) {
			try {
				salt = SecureRandom.getInstance("SHA1PRNG");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				log.error("Error initializig the Secure Random Instance", e);
				return null;
			}
			this.passwordEncoder = new BCryptPasswordEncoder(PASSWORD_STRENGTH, salt);
		}
		return this.passwordEncoder;
	}

	public boolean validatePassword(String password, String encPassword, String saltValue) {
		return this.getEncoder().matches(password + saltValue, encPassword);
	}

	
	
	protected String preparePassword(Object obj, String saltValue) throws UserStoreException {
		char[] chars = null;
		if(obj instanceof Secret){
			Secret secret = (Secret)obj;
			chars = secret.getChars();
		}else if(obj instanceof String){
			return preparePassword((String)obj,saltValue);
		}
		StringBuilder builder = new StringBuilder();
		builder.append(chars);
		builder.append(saltValue);
		builder.trimToSize();
		String hashedPassword = this.getEncoder().encode(builder.toString());
		log.debug("Prepared hashedPassword = " + hashedPassword);
		return hashedPassword;
	}
	
	protected String preparePassword(String password, String saltValue) throws UserStoreException {
		StringBuilder builder = new StringBuilder();
		builder.append(password.toCharArray());
		builder.append(saltValue);
		builder.trimToSize();
		String hashedPassword = this.getEncoder().encode(builder.toString());
		log.debug("Prepared hashedPassword = " + hashedPassword);
		return hashedPassword;
	}
	
	public static void main(String[] args) throws UserStoreException {
		String password = "ghix123#";
		String uuid = "2f398f71-5b6a-46ae-8032-e915391f141a";
		GhixUserAuth auth = new GhixUserAuth();
		String encPass = auth.preparePassword(password, uuid);
		System.out.println("Encrypted Password:" + encPass);
		boolean valid = auth.validatePassword(password, encPass, uuid);
		System.out.println("Valid:" + valid);
	}
}
