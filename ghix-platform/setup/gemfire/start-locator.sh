#!/bin/bash
#
# Bash script to start gemfire locator.
# Author yevgen.golubenko@getinsured.com
#
# Remove old log files if any...
rm gfsh-*.log 2> /dev/null
# 
# Start main execution
CWD=`pwd`
IP=`ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2	p'`
HOSTNAME=`hostname`
GF_PROPS_FILE=`pwd`/gemfire.properties
GF_PROPS_TEMP=`pwd`/gemfire.props.template
LOCATOR_NAME=`grep -E "^name" ${GF_PROPS_TEMP} | sed -e 's/name=//g'`
PIDFILE="${GEMFIRE}/locators/${LOCATOR_NAME}/vf.gf.locator.pid"
echo " "
echo "----------------------------------------------------"
echo "This script starts Gemfire P2P Locator Server"
echo "See README.md for the proper installation procedure."
echo "----------------------------------------------------"
echo " "
echo "Using IP address: ${IP}"
echo "Using   Hostname: ${HOSTNAME}"
echo "Using     GF Dir: ${GEMFIRE}"
echo "Using GF Locator: ${LOCATOR_NAME}"
echo "Using      props: ${GF_PROPS_FILE}"
echo "Using props temp: ${GF_PROPS_TEMP}"
echo "Using   PID File: ${GEMFIRE}/locators/${LOCATOR_NAME}/${PIDFILE}"
echo " "

if [ -z "$GEMFIRE" ]; then 
	echo "Need to set GEMFIRE to point to gemfire installation directory!"
	exit 1
fi

if [ -z "$LOCATOR_NAME" ]; then
	echo "Could not determinate locator name, make sure ${GF_PROPS_TEMP} is in current directory!"
	exit 1
fi 

if [ -e "$PIDFILE" ]; then
	echo "Gemfire locator[${LOCATOR_NAME}] is already running, stop it first!"
	#echo "Stop locator cmd: gfsh> stop locator --dir=${LOCATOR_NAME}"
	echo To stop locator use: gfsh stop locator --pid=`cat $PIDFILE`
	echo "Would you like me to try to stop the Gemfire server?"
	select yn in "yes" "no"; do
		case $yn in
		    yes ) gfsh stop locator --pid=`cat $PIDFILE`; break;;
		esac
	done
fi

if [ ! -d "$GEMFIRE/locators" ]; then
	echo "Directory $GEMFIRE/locators does not exists, creating directory for locator";
	mkdir "$GEMFIRE/locators";
	echo "Directory created: $GEMFIRE/locators";
fi

echo "Copying properties template to live properties";
cp -v $GF_PROPS_TEMP $GF_PROPS_FILE
echo "Configuring live properties with correct parameters for current host";
sed -i -e "s/__HOSTNAME_FOR_CLIENTS__/${HOSTNAME}/g" $GF_PROPS_FILE
sed -i -e "s/__JMX_BIND_ADDRESS__/${IP}/g" $GF_PROPS_FILE
sed -i -e "s/__BIND_ADDRESS__/${IP}/g" $GF_PROPS_FILE

prompt_locator_address () {
	echo "Provide address of the remote locator, e.g. 10.1.10.11, host.domain.com:" 
    while true; do
        read -p "$1" remotehost
        case $remotehost in
            "" ) echo "Please provide valid locator address! => ${remotehost}";;
            * ) locator_address=${remotehost};break;;
        esac
    done
}


echo "Is this instance going to act as locator for the clients?"
echo "1 = Yes, I'm going to be a locator"
echo "0 = No, I'm going to be just an instance"
select yn in 1 0; do
    case $yn in
        1 ) sed -i -e "s/^locators/#locators/g" $GF_PROPS_FILE; break;;
        * ) 
			prompt_locator_address;
			if [ ! -z "$locator_address" -a "$locator_address" != " " ] ; then
				sed -i -e "s/__LOCATORS__/${locator_address}/g" $GF_PROPS_FILE;
			else
				echo "Modify $GF_PROPS_FILE by name and replace __LOCATORS__ string with correct host[port] for remote ";
			fi
			break
		;;
    esac
done

echo "Configuration finished, please examine:"
echo "${GF_PROPS_FILE} to make sure it looks valid"
echo "press CTRL+C to exit and fix by hand."
echo "----- $GF_PROPS_FILE -----"
cat $GF_PROPS_FILE
echo "-----      END       -----"
echo "Execute:"
echo "iptables -A INPUT -s xxx.xxx.xxx.xxx -j ACCEPT"
echo "replace xxx.xxx.xxx.xxx with IP address of each member that will join current locator"
echo "Press enter to start Gemfire locator: $LOCATOR_NAME";
read

cd $GEMFIRE/locators
gfsh start locator --name=$LOCATOR_NAME --properties-file=$GF_PROPS_FILE
cd $CWD
echo "Finished."
cp show-status.gfsh.template show-status.gfsh
sed -i -e "s/__LOCATOR_NAME__/${LOCATOR_NAME}/g" show-status.gfsh
gfsh run --file=show-status.gfsh
echo "Log file available at: ${GEMFIRE}/locators/${LOCATOR_NAME}/${LOCATOR_NAME}.log"





