Jar Name    : ghix-platform-2.0-BUILD-SNAPSHOT.jar
Current Commit : 
	By 		: Venkata Tadepalli 
Last Commit : 
	By 		: kenil 
	commit# : 84ac6aa 

Release Notes:
==============
Current commit :     
	HIX-30255 : Create seperate platform jar that includes platform models
commit# : 84ac6aa
	HIX-31770 Password Not Getting Expired After 60 Days.
commit# : 495591656caa685a1b9f8464a2e061cc10b62d44
	HIX-32239 - Add Assister role related changes to send activation email
	Author Bimal Sahay