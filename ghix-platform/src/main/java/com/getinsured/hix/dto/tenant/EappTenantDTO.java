package com.getinsured.hix.dto.tenant;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/**
 * 
 * @author root
 *
 */
public class EappTenantDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String brokerName;
	private String brokerTIN;
	private EappWritingAgentDTO writingAgent;
	/**
	 * <state, licenseNumber>
	 */
	private Map<String, String> stateLicenses; 
	private Map<String, Map<String, EappCarrierStateSpecificDTO>> carriers;
	private Map<String, Map<String, IssuerBrandSpecificDTO>> issuerBrand;
	
	
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getBrokerTIN() {
		return brokerTIN;
	}
	public void setBrokerTIN(String brokerTIN) {
		this.brokerTIN = brokerTIN;
	}
	public EappWritingAgentDTO getWritingAgent() {
		return writingAgent;
	}
	public void setWritingAgent(EappWritingAgentDTO writingAgent) {
		this.writingAgent = writingAgent;
	}
	
	public Map<String, String> getStateLicenses() {
		return stateLicenses;
	}
	public void setStateLicenses(Map<String, String> stateLicenses) {
		this.stateLicenses = stateLicenses;
	}
	public Map<String, Map<String, EappCarrierStateSpecificDTO>> getCarriers() {
		return carriers;
	}
	public void setCarriers(Map<String, Map<String, EappCarrierStateSpecificDTO>> carriers) {
		this.carriers = carriers;
	}
	
	public static EappTenantDTO sampleInstance(){
		EappTenantDTO dto = new EappTenantDTO();
		dto.setBrokerName("Vimo, Inc. DBA GetInsured");
		dto.setBrokerTIN("870745172");
		dto.setWritingAgent(EappWritingAgentDTO.sampleInstance());
		
		/**
		 * license numbers
		 */
		Map<String, String> licenseNumber = new HashMap<String, String>();
		licenseNumber.put("UT", "347308");
		licenseNumber.put("LA", "513521");
		licenseNumber.put("FL", "P241259");
		licenseNumber.put("NC", "0001131360");
		dto.setStateLicenses(licenseNumber);
		
		/**
		 * carrier specific details
		 */
		
		Map<String, Map<String, EappCarrierStateSpecificDTO>> carrierDTO = new HashMap<String, Map<String, EappCarrierStateSpecificDTO>>();
		
		/**
		 * Anthem
		 */
		Map<String, EappCarrierStateSpecificDTO> anthemStateDTO = new HashMap<String, EappCarrierStateSpecificDTO>();
		anthemStateDTO.put("US", new EappCarrierStateSpecificDTO("DHJQJNRKSZ", "DHNNMJJSTY"));
		
		carrierDTO.put("anthem", anthemStateDTO);
		
		/**
		 * Anthem
		 */
		Map<String, EappCarrierStateSpecificDTO> uhcStateDTO = new HashMap<String, EappCarrierStateSpecificDTO>();
		uhcStateDTO.put("US", new EappCarrierStateSpecificDTO("AA3016325", null));
		
		carrierDTO.put("uhc", uhcStateDTO);
		
		/**
		 * Humana
		 */
		Map<String, EappCarrierStateSpecificDTO> humanaStateDTO = new HashMap<String, EappCarrierStateSpecificDTO>();
		humanaStateDTO.put("US", new EappCarrierStateSpecificDTO("1591417", "1403377"));
		carrierDTO.put("humana", humanaStateDTO);
		
		/**
		 * HCSC
		 */
		Map<String, EappCarrierStateSpecificDTO> hcscStateDTO = new HashMap<String, EappCarrierStateSpecificDTO>();
		hcscStateDTO.put("NM", new EappCarrierStateSpecificDTO("38513000", null));
		hcscStateDTO.put("OK", new EappCarrierStateSpecificDTO("38656000", null));
		hcscStateDTO.put("TX", new EappCarrierStateSpecificDTO("64006000", null));
		hcscStateDTO.put("IL", new EappCarrierStateSpecificDTO("64007000", null));
		
		carrierDTO.put("hcsc", hcscStateDTO);
		
		dto.setCarriers(carrierDTO);
		
		return dto;
	}
	
	
	public static void main(String args[]){
		EappTenantDTO dto = EappTenantDTO.sampleInstance();
		Gson gson = new Gson();
		System.out.println(gson.toJson(dto));
	}
	/**
	 * @return the issuerBrand
	 */
	public Map<String, Map<String, IssuerBrandSpecificDTO>> getIssuerBrand() {
		return issuerBrand;
	}
	/**
	 * @param issuerBrand the issuerBrand to set
	 */
	public void setIssuerBrand(Map<String, Map<String, IssuerBrandSpecificDTO>> issuerBrand) {
		this.issuerBrand = issuerBrand;
	}
	
	
	

}
