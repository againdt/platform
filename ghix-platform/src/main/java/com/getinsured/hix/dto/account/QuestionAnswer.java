package com.getinsured.hix.dto.account;

import org.apache.commons.lang3.StringUtils;




/**
 * The Class for Security Question Answer .
 * Ref: HIX-47504 , HIX-47515
 * 
 * @author Venkata Tadepalli
 * @since  Aug 27, 2014
 */

public class QuestionAnswer  {
	private static final long serialVersionUID = 1L;
	
	private int idx;
	private String question;
	private String answer;
	
	
	public QuestionAnswer() {
		
	}
	

	public QuestionAnswer(int idx, String question, String answer) {
		this.idx = idx;
		this.question = question;
		this.answer = answer;
	}


	/**
	 * @return the idx
	 */
	public int getIdx() {
		return idx;
	}


	/**
	 * @param idx the idx to set
	 */
	public void setIdx(int idx) {
		this.idx = idx;
	}


	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}


	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}


	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}


	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result + idx;
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		return result;
	}


	/**
	 * Checks if the the this obj is equals with the input questionAnswerDto obj.
	 * returns true if idx, question and answer equals with qstnAnsDto and return false.
	 * note: 'answer' can be compared with ignore case (since user enters the data)
	 *       'question' should not compared with ignore case (since question should match with database)
	 * @param qstnAnsDto the QuestionAnswerDto to compare
	 * @return true 
	 */
	@Override
	public boolean equals(Object obj) {
		
		boolean retValue = false;
		
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof QuestionAnswer)) {
			return false;
		}
		
		QuestionAnswer other = (QuestionAnswer) obj;
		
		if (idx == other.idx &&
				StringUtils.equalsIgnoreCase(answer,other.answer) &&
				StringUtils.equals(question,other.question)) {
			retValue = true;
		}
		
		return retValue;
	}
}