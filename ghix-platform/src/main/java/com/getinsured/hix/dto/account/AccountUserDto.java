package com.getinsured.hix.dto.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityQuestionsHelper;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;


/**
 * The DTO Class for AccountUser model.
 * Ref: HIX-47504 , HIX-47515
 * 
 * @author Venkata Tadepalli
 * @since  Aug 27, 2014
 */

public class AccountUserDto  {
	private static final long serialVersionUID = 1L;

	private int id;
	
	private int secQueRetryCount;
	
	List<QuestionAnswer> userSecQstnAnsList= new ArrayList<QuestionAnswer>();
	
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the secQueRetryCount
	 */
	public int getSecQueRetryCount() {
		return secQueRetryCount;
	}

	/**
	 * @param secQueRetryCount the secQueRetryCount to set
	 */
	public void setSecQueRetryCount(int secQueRetryCount) {
		this.secQueRetryCount = secQueRetryCount;
	}

	/**
	 * @return the userSecQstnAnsList
	 */
	public List<QuestionAnswer> getUserSecQstnAnsList() {
		return userSecQstnAnsList;
	}

	/**
	 * @param userSecQstnAnsList the userSecQstnAnsList to set
	 */
	public void setUserSecQstnAnsList(List<QuestionAnswer> userSecQstnAnsList) {
		this.userSecQstnAnsList = userSecQstnAnsList;
	}

	public boolean questionAnswersEqualWith(List<QuestionAnswer> qstnAnsList){
		boolean isEqual=true;
			
		if(this.userSecQstnAnsList.size() != qstnAnsList.size()){
			return false;
		}
		
		isEqual=this.userSecQstnAnsList.containsAll(qstnAnsList);
		
		return isEqual;
		
	}
	
	public static List<QuestionAnswer> getUserSecurityQuestionAnswers(AccountUser user){
		
		AccountUserDto accountUserDto = new AccountUserDto();
		List<QuestionAnswer> questionAnswerList = new ArrayList<QuestionAnswer>();
		
		//START : HIX-464680
		int noOfSecQtns = 2; //default no of questions 2
		String strNoOfSecQtns = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.NO_OF_SECURITY_QUESTIONS.getValue());
		
		if(StringUtils.isNotBlank(strNoOfSecQtns)){
			int tempNoOfSecQtns = Integer.parseInt(strNoOfSecQtns);
			
			if(tempNoOfSecQtns>0 && tempNoOfSecQtns<4){ //The no of questions should be between 1 -3
				noOfSecQtns = tempNoOfSecQtns;
			}
			
		}
		
		for(int secQtnIdx=1;secQtnIdx<=noOfSecQtns;secQtnIdx++){
			String currQstn = user.getSecurityQuestion(secQtnIdx);
			String currAns  = user.getSecurityAnswer(secQtnIdx);
			if(StringUtils.isNotBlank(currQstn) && StringUtils.isNotBlank(currAns)){
				QuestionAnswer currQuestionAnswer= new QuestionAnswer(secQtnIdx, currQstn,currAns);
				questionAnswerList.add(currQuestionAnswer);
			}
			
		}
		
		return questionAnswerList;
		
	}
	

	public static boolean validateSecurityQuestionAnswers(List<QuestionAnswer> questionAnswerList){
		
		boolean isValid=true;
		SecurityQuestionsHelper securityQuestionsHelper = new SecurityQuestionsHelper();
		
		for(QuestionAnswer currQuestionAnswer : questionAnswerList){
			int idx = currQuestionAnswer.getIdx();
			String currQstn = currQuestionAnswer.getQuestion();
			String currAns = currQuestionAnswer.getAnswer();
			  if(StringUtils.isBlank(currQstn) || StringUtils.isBlank(currAns)){
				  isValid=false;
			  }
			  if(!securityQuestionsHelper.isValidQuestion(idx, currQstn)){
				  isValid=false;
			  }
			  if(!isValid){
				  break;
			  }
		  }
		
		
		if(!isValid){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		  }
		return isValid;
		
	}
	
	public static boolean isValidUserStatus(String userStatus){
		
		boolean isValid=false;
		
		AccountUser.user_status[] checkEditUserStatus=AccountUser.user_status.values();
		
		for(AccountUser.user_status currUserStatus: checkEditUserStatus){
			String tempUserStatus = currUserStatus.value();
			if(StringUtils.equalsIgnoreCase(tempUserStatus, userStatus)){
				isValid=true;
				break;
			}
		}
		
		return isValid;
		
	}
}