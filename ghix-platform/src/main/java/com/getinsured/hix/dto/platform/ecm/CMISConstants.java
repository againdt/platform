package com.getinsured.hix.dto.platform.ecm;

public final class CMISConstants {
	public static final String CONNECTION_TYPE = "ecm.connectionType";
	public static final String DOCUMENT_TYPE = "cmis:document";
	public static final String FILE_SEPARATOR = "ecm.file.separator";
	public static final String FOLDER_TYPE = "cmis:folder";
	public static final String HOST = "ecm.host";
	public static final String LOCALE_COUNTRY = "ecm.localeCountry";
	public static final String LOCALE_LANGUAGE = "ecm.localeLanguage";
	public static final String LOCALE_VARIANT = "ecm.localeVariant";
	public static final String METADATA = "cmiscustom:docprop_string";
	public static final String PASSWORD = "ecm.password";
	public static final String PORT = "ecm.port";
	public static final String RELATIONSHIP_TYPE = "R:cmiscustom:assoc";
	public static final String REPOSITORY_ID = "ecm.repositoryId";
	public static final String USERNAME = "ecm.userName";
	public static final String ATOM_PUB_URL = "ecm.atomPubURL";
	public static final String TITLED = "P:cm:titled";
	public static final String TAGGABLE = "P:cm:taggable";
	public static final Integer MAP_SIZE = 4;
	public static final Integer ZERO = 0;
	public static final String CHARSET_UTF_8 = "; charset=UTF-8";
	public static final String CONTENT_SIZE = "contentSize";
	
	private CMISConstants(){}
}
