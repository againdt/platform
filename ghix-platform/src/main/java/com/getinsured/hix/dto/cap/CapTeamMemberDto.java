package com.getinsured.hix.dto.cap;

public class CapTeamMemberDto {

	private Integer userId;
	private Integer teamId;
	private String userName;
	private String teamName;
	private String startDate;
	private String endDate;
	private String maxDate; // MAX of start date and end date;
	private String teamLeadName;
	private Integer teamLeadId;
	private String teamLeadStartDate;
	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public String getTeamLeadName() {
		return teamLeadName;
	}

	public void setTeamLeadName(String teamLeadName) {
		this.teamLeadName = teamLeadName;
	}

	public Integer getTeamLeadId() {
		return teamLeadId;
	}

	public void setTeamLeadId(Integer teamLeadId) {
		this.teamLeadId = teamLeadId;
	}

	public String getTeamLeadStartDate() {
		return teamLeadStartDate;
	}

	public void setTeamLeadStartDate(String teamLeadStartDate) {
		this.teamLeadStartDate = teamLeadStartDate;
	}


}
