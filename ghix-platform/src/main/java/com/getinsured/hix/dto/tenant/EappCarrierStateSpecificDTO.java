package com.getinsured.hix.dto.tenant;

import java.io.Serializable;

/**
 * For every tenant there can be multiple carriers
 * ie Humana, Aetna, Kaiser
 * 
 * These national level carriers represent all the states where they run business
 * Each of the state may override some of the attributes. eg. they may have a different brokerId
 * for each state. Some however may choose the same for all (captured at the parent level of this class) - containing class
 * 
 * UPDATE: everything here is either writingAgentId or agencyId. They might be called differently by different carriers.
 * But more or less they mean the same.
 * @author root
 *
 * This is a placeholder for other attributes that might come in future..
 */
public class EappCarrierStateSpecificDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String writingAgentId;
	private String brokerId;
	
	public String getWritingAgentId() {
		return writingAgentId;
	}

	public void setWritingAgentId(String writingAgentId) {
		this.writingAgentId = writingAgentId;
	}

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public EappCarrierStateSpecificDTO(){
		
	}
	
	public EappCarrierStateSpecificDTO(String writingAgentId, String agencyId){
		this.writingAgentId = writingAgentId;
		this.brokerId = agencyId;
	}
	
	
}
