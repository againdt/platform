package com.getinsured.hix.dto.tenant;

import java.io.Serializable;

public class IssuerBrandSpecificDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String applicationURL;
	
	/**
	 * @return the applicationURL
	 */
	public String getApplicationURL() {
		return applicationURL;
	}
	/**
	 * @param applicationURL the applicationURL to set
	 */
	public void setApplicationURL(String applicationURL) {
		this.applicationURL = applicationURL;
	}
	
	public IssuerBrandSpecificDTO(String applicationURL){
		this.applicationURL = applicationURL;
	}
	

	public IssuerBrandSpecificDTO(){
		
	}
}
