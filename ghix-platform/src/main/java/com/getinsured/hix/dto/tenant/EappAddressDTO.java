package com.getinsured.hix.dto.tenant;

import java.io.Serializable;

public class EappAddressDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private String county;
	
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public static EappAddressDTO sampleInstance(){
		EappAddressDTO dto = new EappAddressDTO();
		dto.setAddress1("2110 Newmarket Parkway");
		dto.setAddress2("Suite 200");
		dto.setCity("Marietta");
		dto.setState("GA");
		dto.setZipCode("30067");
		dto.setCounty("Cobb");
		return dto;
	}
	

}
