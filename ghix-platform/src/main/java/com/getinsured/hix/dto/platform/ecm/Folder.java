/**
 *
 */
package com.getinsured.hix.dto.platform.ecm;




/**
 * @author Ekram Ali Kazi
 *
 */
public class Folder extends Content {

	

	private String parentId;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	@Override
	public String toString() {
		return "Folder [parentId=" + parentId + "]";
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

}
