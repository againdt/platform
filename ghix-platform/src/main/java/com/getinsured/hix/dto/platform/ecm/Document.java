package com.getinsured.hix.dto.platform.ecm;




public class Document extends Content {

	// instance variables

	private String author = null;

	private String snippet = null;

	private String language = null;

	private long size = 0;

	private int pages = 0;

	private DocumentStream documentStream = null;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public DocumentStream getDocumentStream() {
		return documentStream;
	}

	public void setDocumentStream(DocumentStream documentStream) {
		this.documentStream = documentStream;
	}

	@Override
	public String toString() {
		return "Document [author=" + author + ", snippet=" + snippet
				+ ", language=" + language + ", size=" + size + ", pages="
				+ pages + ", documentStream=" + documentStream + "]";
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

}
