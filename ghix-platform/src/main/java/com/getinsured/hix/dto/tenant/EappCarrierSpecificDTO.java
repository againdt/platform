package com.getinsured.hix.dto.tenant;

import java.io.Serializable;
import java.util.Map;

public class EappCarrierSpecificDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * SAN's (whatever is the full form)
	 * are assigned by carrier to a writingAgent/Agency combo
	 * 
	 * So they are dependent on carrier
	 */
	private Map<String, EappCarrierStateSpecificDTO> config;
	/**
	 * Agency tin can be encrypted by carrier algorithm
	 * and hence it has to stay here instead of being at global level
	 */

	public static EappCarrierSpecificDTO sampleInstance(){
		EappCarrierSpecificDTO dto = new EappCarrierSpecificDTO();
		
		return dto;
	}
	public Map<String, EappCarrierStateSpecificDTO> getConfig() {
		return config;
	}
	public void setConfig(Map<String, EappCarrierStateSpecificDTO> config) {
		this.config = config;
	}
	
	
}
