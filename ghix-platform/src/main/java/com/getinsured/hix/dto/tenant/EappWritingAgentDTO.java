package com.getinsured.hix.dto.tenant;

import java.io.Serializable;

public class EappWritingAgentDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private String npn;
	private String emailAddress;
	private String phoneNumber;
	private String faxNumber;
	private EappAddressDTO address;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNpn() {
		return npn;
	}
	public void setNpn(String npn) {
		this.npn = npn;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public EappAddressDTO getAddress() {
		return address;
	}
	public void setAddress(EappAddressDTO address) {
		this.address = address;
	}
	
	public static EappWritingAgentDTO sampleInstance(){
		EappWritingAgentDTO dto = new EappWritingAgentDTO();
		dto.setPhoneNumber("8009723755");
		dto.setFaxNumber("7702346335");
		dto.setEmailAddress("customersupport@getinsured.com");
		dto.setFirstName("Michael");
		dto.setLastName("Daugherty");
		dto.setNpn("1131360");
		dto.setAddress(EappAddressDTO.sampleInstance());
		
		return dto;
	}
	

}
