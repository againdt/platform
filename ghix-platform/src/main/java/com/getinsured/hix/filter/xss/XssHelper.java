package com.getinsured.hix.filter.xss;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;

import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;





public final class XssHelper {

	private static GhixLogger LOGGER = GhixLogFactory.getLogger(XssHelper.class);

	// it doesn't make sense to instantiate this class
    private XssHelper() {}

    public static final String INVALID_INPUT = "Invalid input found on the page";
    public static final String INVALID_INPUT_LOG = "Possible XSS Attack : Invalid input found on the page - ";

    @Deprecated
	public static boolean checkXSS(String value) {
		boolean isXSS = false;
		if (value.matches("(.*?)<script>(.*?)<script>(.*?)") || value.matches("(.*?)<script>(.*?)</script>(.*?)") ||
				value.matches("(?i)<script.*?>.*?<script.*?>") || value.matches("(?i)<script.*?>.*?</script.*?>") ||
				value.matches("(?i)<.*?javascript:.*?>.*?</.*?>") ||
				value.matches("(?i)<.*?\\s+on.*?>.*?</.*?>")	  ||
				value.matches("eval\\((.*)\\)") ||
				value.matches("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']") ||
				value.matches("(.*?)<script>(.*?)") ||
				value.matches("(.*?)</script>(.*?)") ||
				value.matches("(.*?)<script(.*?)>(.*?)") ||
				value.matches("(.*?)</script(.*?)>(.*?)")
			){
			isXSS = true;
		}
		return isXSS;
	}

    private static Pattern[] patterns = new Pattern[]{
    	//Patterns from CA1.0
		Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
		//Pattern.compile("alert(.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
    	//confirm(...)
       // Pattern.compile("confirm\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
       // Pattern.compile("confirm(.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        //prompt(...)
       // Pattern.compile("prompt\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
       // Pattern.compile("prompt.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        
        Pattern.compile("console.log*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        //Pattern.compile("console.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("window.location\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("window.location*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("document.location*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("document.location\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("window.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        
        // Script fragments
        Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
        // src='...'
        Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        // lonely script tags
        Pattern.compile("</script>", Pattern.CASE_INSENSITIVE),
        Pattern.compile("window\\.(.*?)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
       // Pattern.compile("console\\.([a-z]{3,}?)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("document\\.([a-z]{5,}?)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        // eval(...)
        Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        // expression(...)
        Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        // javascript:...
        Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE),
        // vbscript:...
        Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE),

        // onerror:...
        Pattern.compile("onerror=", Pattern.CASE_INSENSITIVE),
        // onload(...)=...
        Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //alert(...)
        Pattern.compile("alert(\\((.*?)\\)|;|:)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //Pattern.compile("alert.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //confirm(...)
        Pattern.compile("confirm(\\((.*?)\\)|;|:)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //Pattern.compile("confirm.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        //prompt(...)
        Pattern.compile("prompt(\\((.*?)\\)|;|:)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //Pattern.compile("prompt.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //benchmark(...)
        Pattern.compile("benchmark\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //write(...)
        Pattern.compile("write\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        
        Pattern.compile("onEvent\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        //writeln(...)
        Pattern.compile("writeln\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        //document.write(...)
        Pattern.compile("document.write\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("console.log\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

        //document.writeln(...)
        Pattern.compile("document.writeln\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
       // Pattern.compile("((\\W|^)(select)(\\W|$))(.*?)((\\W|^)from(\\W|$))", Pattern.CASE_INSENSITIVE| Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("((\\W|^)(delete from)(\\W|$))", Pattern.CASE_INSENSITIVE| Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("((\\W|^)(insert into)(\\W|$))(.*?)((\\W|^)values(\\W|$))", Pattern.CASE_INSENSITIVE| Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("((\\W|^)(update)(\\W|$))(.*?)((\\W|^)set(\\W|$))", Pattern.CASE_INSENSITIVE| Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("((\\W|^)(drop table)(\\W|$))", Pattern.CASE_INSENSITIVE| Pattern.MULTILINE | Pattern.DOTALL)
    };

	@Deprecated
	public static boolean checkXSSAttack(String value) {

		if (StringUtils.isEmpty(value)){
			return true;
		}
		value = value.toLowerCase();
		if (value.matches("(.*?)<script>(.*?)<script>(.*?)") || value.matches("(.*?)<script>(.*?)</script>(.*?)") ||
				value.matches("(?i)<script.*?>.*?<script.*?>") || value.matches("(?i)<script.*?>.*?</script.*?>") ||
				value.matches("(?i)<.*?javascript:.*?>.*?</.*?>") ||
				value.matches("(?i)<.*?\\s+on.*?>.*?</.*?>")	  ||
				value.matches("eval\\((.*)\\)") ||
				value.matches("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']") ||
				value.matches("(.*?)<script>(.*?)") ||
				value.matches("(.*?)</script>(.*?)") ||
				value.matches("(.*?)<script(.*?)>(.*?)") ||
				value.matches("(.*?)</script(.*?)>(.*?)")
			){
			LOGGER.warn(XssHelper.INVALID_INPUT_LOG + value);
			throw new GhixXssRuntimeException(XssHelper.INVALID_INPUT);
		}
		return true;
	}

	/**
     * Strips any potential XSS threats out of the value
     * @param inputValue
     * @return
     */
	public static String stripXSS( String inputValue )
    {
		String value=inputValue;
        if( value != null )
        {
            // Use the ESAPI library to avoid encoded attacks.
        	value = ESAPI.encoder().canonicalize( value );

            // Avoid null characters

            value = value.replaceAll("\0", "");

            // Clean out HTML
            value = value.replaceAll("\r\n|\n", "nl2br");
            value = Jsoup.clean( value, Whitelist.none());
            value = value.replaceAll("nl2br", "\n");


            /*if (StringUtils.isEmpty(value)){
            	value = " ";
            }*/
            // Remove all sections that match a pattern
            for (Pattern scriptPattern : patterns){
                value = scriptPattern.matcher(value).replaceAll("");
            }

        }
        return value;
    }

	public static boolean stripXSS(StringBuilder valueHolder, String inputValue) {
		String value = inputValue;
		boolean possibleAttack = false;
		String cleanedValue;

		if (value != null) {
			// Use the ESAPI library to avoid encoded attacks.
			try {
				value = ESAPI.encoder().canonicalize(value);
			} catch (IntrusionException ie) {
				possibleAttack = true;
			}

			// Avoid null characters

			value = value.replaceAll("\0", "");

			// Clean out HTML
			value = value.replaceAll("\r\n|\n", "nl2br");
			cleanedValue = Jsoup.clean(value, Whitelist.none());
			if (!cleanedValue.equalsIgnoreCase(value)) {
				possibleAttack = true;
			}
			value = cleanedValue.replaceAll("nl2br", "\n");

			/*
			 * if (StringUtils.isEmpty(value)){ value = " "; }
			 */
			// Remove all sections that match a pattern
			for (Pattern scriptPattern : patterns) {
				cleanedValue = scriptPattern.matcher(value).replaceAll("");
				if (!value.equalsIgnoreCase(cleanedValue)) {
					possibleAttack = true;
				}
				value = cleanedValue;
			}

		}
		valueHolder.setLength(0);
		valueHolder.append(value);
		valueHolder.trimToSize();
		return possibleAttack;
	}
}
