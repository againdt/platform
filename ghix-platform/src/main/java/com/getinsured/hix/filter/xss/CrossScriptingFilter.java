package com.getinsured.hix.filter.xss;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.tokens.CSRFToken;
import com.getinsured.hix.platform.security.tokens.CSRFTokenTag;
import com.getinsured.hix.platform.util.GhixHttpSecurityConfig;


/**
 * Filter that validates CSRF token.
 */
public class CrossScriptingFilter implements Filter
{
  private static final boolean CSRF_CHECK_ENABLED = true;
  private static final String CSRF_HEADER_NAME = "X-CSRF-TOKEN";
  private static Logger log = LoggerFactory.getLogger(CrossScriptingFilter.class);
  private FilterConfig filterConfig;
  private GhixHttpSecurityConfig secConfig = new GhixHttpSecurityConfig();
  /**
   * {@inheritDoc}
   */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {
    this.filterConfig = filterConfig;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void destroy()
  {
    this.filterConfig = null;
  }
  
  private void logSessionInfo(ServletRequest req) {
	  HttpServletRequest request = (HttpServletRequest) req;
	  String uri = request.getRequestURI();
	  if(uri.endsWith("actuator/info".intern())) {
		  return;
	  }
	  HttpSession session = request.getSession(false);
	  String id = "Not available".intern();
	  if(session != null) {
		  id = session.getId();
		  SecurityContextImpl sci = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
		  if (sci != null) {
			  Authentication auth = sci.getAuthentication();
			  if(auth != null){
				  UserDetails cud = (UserDetails) auth.getPrincipal();
				  log.info("DEBUG : Incoming URL {}, Referer {}, User Context {}, session Id {}",request.getRequestURL(), request.getHeader("Referer".intern()), cud.getUsername(),id);
		  	  }else {
		  		  log.info("DEBUG : Incoming URL {}, Referer {}, Auth Context null and Session Id {}",request.getRequestURL(), request.getHeader("Referer".intern()), id);
		  	  }
		  }else {
			  log.info("DEBUG : Incoming URL {}, Referer {}, Security Context null and Session Id {}",request.getRequestURL(), request.getHeader("Referer".intern()), id);
		  }
	  }else {
		  log.info("DEBUG : Incoming URL {}, Referer {}, Session Context null",request.getRequestURL(), request.getHeader("Referer".intern()), id);
	  }
  }
  

  /**
   * {@inheritDoc}
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
  {
    final HttpServletRequest httpReq = (HttpServletRequest) request;
    final String url = httpReq.getRequestURL().toString();
    final String queryStr = httpReq.getQueryString();
    final String requestMethod = httpReq.getMethod().toUpperCase();
    boolean shouldContinue = true;
    boolean newTokenInitialized = false;
    if(log.isInfoEnabled()) {
    	logSessionInfo(request);
    }
    // Whole purpose is to warn if something is wrong, so dont use cpu cycles
    // if warn is not enabled anyways.
    if(log.isWarnEnabled())
    {
    if (queryStr != null && queryStr.contains(CSRFTokenTag.CSRF_PARAMETER_NAME))
    {
      log.warn("Invalid CSRF token placement for URL:[{}], CSRF token should never be part of the query string: [{}]", url, queryStr);
    }
    }

    String csrfTokenParameter = (String) request.getParameter(CSRFTokenTag.CSRF_PARAMETER_NAME);

    if (requestMethod.equals(RequestMethod.GET.name()) && csrfTokenParameter != null)
    {
      throw new ServletException("CSRF token found in a GET Request, denying access to URL: " + url +
          ", please don't pass CSRF token in when performing GET requests.");
    }

    if (httpReq.getSession().getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME) == null)
    {
      httpReq.getSession().setAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME, CSRFToken.getNewToken());
      newTokenInitialized = true;
    }

    //*****************************************************************************/
    // Reverting the following condition check will make all SAML redirects fail
    // Please talk to Abhai Chaudhary if you have questions.
    //
    // By passing CSRF for all SAML related re-directs till we figure a better way
    // on how to handle thirdparty redirects
    //*****************************************************************************/
    if (!url.contains("saml") && !url.contains("webhook/emailstat/sendgrid") && !url.contains("postFfm/postReturnUrl") && !url.contains("directenrollment/payment") && !url.contains("private/ede"))
    {
      
      if (requestMethod.equals(RequestMethod.POST.name()) && CSRF_CHECK_ENABLED)
      {
        /*
         * Try to get CSRF token from HTTP Header
         */
        if (csrfTokenParameter == null)
        {
          /*
           * Check if token is available in request Header.
           *
           * UI Angular library uses X-CSRF-TOKEN header name by default, we use CSRFTokenTag.CSRF_PARAMETER_NAME
           * which is 'csrftoken' at this time, I've talked to UI team, they are going to modify library to
           * conform to our custom standard - Yevgen.
           */
          csrfTokenParameter = httpReq.getHeader(CSRFTokenTag.CSRF_PARAMETER_NAME);

          if(csrfTokenParameter == null || csrfTokenParameter.trim().isEmpty())
          {
            // Lets try standard header
            csrfTokenParameter = httpReq.getHeader(CSRF_HEADER_NAME);
          }
        }

        final String csrfTokenSession = (String) httpReq.getSession().getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME);

        boolean isMultipart = ServletFileUpload.isMultipartContent((HttpServletRequest) request);

        if (!isMultipart)
        {
          // If we have just initialized the csrf token in the session, there is no point in matching with the request
          // This could happen if current session is already invalidated and user is using a stale page
          // We'll let this go from here and allow the authentication filter to check for the session validity
          if (newTokenInitialized)
          {
            if (csrfTokenParameter != null)
            {
              if(log.isErrorEnabled())
              {
              log.error("CSRF token in the session just initialized but received one from the request [{}], probably a stale page, on another browser tab, not allowing POST URL: {}",
                  csrfTokenParameter, url);
              }
              if (isAjaxCall((HttpServletRequest) request))
              {
                  HttpServletResponse resp = (HttpServletResponse) response;
                  resp.sendError(HttpStatus.SC_UNAUTHORIZED);
              }else {
	              HttpServletResponse httpResp = (HttpServletResponse)response;
	              httpResp.sendRedirect(httpReq.getContextPath() + "/error/pageExpired");
	          }
              shouldContinue = false;
            }
          } else
          {
            if (!csrfTokenSession.equalsIgnoreCase(csrfTokenParameter))
            {
              if(log.isErrorEnabled())
              {
              log.info("Possible CSRF Attack. CSRF token mismatch for URL: [{}], Received: [{}], Expected: [{}]; Session token initialized now: [{}]",
                  url, csrfTokenParameter, csrfTokenSession, newTokenInitialized);
              }

              logAttackDetails((HttpServletRequest) request);
              
              if (isAjaxCall((HttpServletRequest) request))
              {
                  HttpServletResponse resp = (HttpServletResponse) response;
                  resp.sendError(HttpStatus.SC_UNAUTHORIZED);
              }else {
	              HttpServletResponse httpResp = (HttpServletResponse)response;
	              httpResp.sendRedirect(httpReq.getContextPath() + "/error/pageExpired");
              }
              shouldContinue = false;
            }	
          }
        } else
        {
          if(log.isWarnEnabled())
          {
          log.warn("CSRF token match check ignored for Multipart POST URL: [{}] Content Type: {}", url, request.getContentType());
        }
      }
      }else {
    	  // This implementation should move out from this filter
    	    if(this.secConfig.isUrlProtected(url)){
			Authentication authentication = null;
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			HttpSession session = httpRequest.getSession(false);

			SecurityContextImpl sci = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");

			if (sci != null) {
				authentication = sci.getAuthentication();
			}
			if (null == authentication) {
				if (log.isDebugEnabled()) {
					log.debug(
							"setActiveRoleForLandingPage: authentication received null from session trying security context");
				}

				authentication = SecurityContextHolder.getContext().getAuthentication();
			}
			if (log.isDebugEnabled()) {
				log.debug("Aauthentication:  {} ", authentication);
			}

			if (authentication == null) {
				session = httpReq.getSession(false);
				String username = (String) session.getAttribute("USER_NAME");
				if (isAjaxCall((HttpServletRequest) request) && username == null) {
					HttpServletResponse resp = (HttpServletResponse) response;
					resp.sendError(HttpStatus.SC_UNAUTHORIZED);
					shouldContinue = false;
				}

			}
    	    }
		}
      
    } else
    {
      if(log.isWarnEnabled())
      {
      log.warn("********************* WARNING ***********************");
      log.warn("  CSRF check by passed for URL:" + url);
      log.warn("*****************************************************");
    }
    }
    
    


    /*
     * NOTE: Also uncomment code at the bottom for this to work if you are
     * thinking about using this again.
     *
     * if submitting form is multipart, then do custom filtering
     * and throw ServletException("Invalid input found on the page") if malicious input found.
     *
        boolean isMultipart = ServletFileUpload.isMultipartContent((HttpServletRequest) request);
		if (isMultipart){
			multiPartFiltering(request);
		}
     */
    if(shouldContinue) {
    	/** if not multipart then process as usual and clear the malicious input and DONOT throw exception. */
    	chain.doFilter(new RequestWrapper((HttpServletRequest) request, filterConfig.getServletContext()), response);
    }
  }
  
  private boolean isAjaxCall(HttpServletRequest request) {
  	String val = request.getHeader("X-Requested-With");
  	if(val == null) {
  		val = request.getHeader("x-requested-with");
  	}
  	boolean status = (val != null && val.contains("XMLHttpRequest"));
  	log.info("Received X-Requested-With {} is Ajax {}",val, status);
  	if(log.isInfoEnabled() && status) {
  		log.info("Ajax Request: {} Referred from {} ", request.getRequestURL(),request.getHeader("Referer".intern()));
  	}
  	return status;
  }

  /**
   * Logs details of the request that we consider to be an attacker.
   *
   * @param request HTTP Servlet Request.
   */
  private void logAttackDetails(HttpServletRequest request)
  {
    // Don't do any of this stuff if at the end error level log is
    // not enabled anyways...
    if(log.isErrorEnabled())
    {
    String userName = "Anonymous";
    String ipAddress = null;

    Authentication authentication = null;

    try
    {
      ipAddress = request.getHeader("x-forwarded-for");

      if (null == ipAddress || ipAddress.isEmpty())
      {
        ipAddress = request.getRemoteAddr();
      }

      authentication = SecurityContextHolder.getContext().getAuthentication();

      if (null != authentication)
      {
        Object principal = authentication.getPrincipal();

        if (principal instanceof String)
        {
          userName = (String) principal;
          } else if (principal instanceof AccountUser)
        {
          AccountUser accountUser = (AccountUser) principal;
          userName = accountUser.getEmail();
        }
      }
      } catch (Exception ex)
    {
      log.error("FAILED TO LOG CSRF ATTACK DETAILS", ex);
      } finally
    {
      log.error("CSRF ATTACK DETAILS: USER: {} IP ADDR: {}", userName, ipAddress);
    }
  }
  }

//
//  if ever used, you can uncomment when needed.
//
//  private static DiskFileItemFactory factory = new DiskFileItemFactory();
//  @SuppressWarnings("unused")
//  private void multiPartFiltering(ServletRequest request) throws ServletException
//  {
//    try
//    {
//      ServletFileUpload upload = new ServletFileUpload(factory);
//      // Parse the request to get file items.
//      @SuppressWarnings("unchecked")
//      List<FileItem> formItems = upload.parseRequest((HttpServletRequest) request);
//
//      if (formItems != null && formItems.size() > 0)
//      {
//        for (FileItem item : formItems)
//        {    // iterates over form's fields
//          // processes only fields that are form fields
//          if (item.isFormField())
//          {
//            if (!StringUtils.isEmpty(item.getString()))
//            {
//              String value = item.getString().toLowerCase();
//
//              if (value.matches("(.*?)<script>(.*?)<script>(.*?)") || value.matches("(.*?)<script>(.*?)</script>(.*?)") ||
//                  value.matches("(?i)<script.*?>.*?<script.*?>") || value.matches("(?i)<script.*?>.*?</script.*?>") ||
//                  value.matches("(?i)<.*?javascript:.*?>.*?</.*?>") ||
//                  value.matches("(?i)<.*?\\s+on.*?>.*?</.*?>") ||
//                  value.matches("eval\\((.*)\\)") ||
//                  value.matches("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']") ||
//                  value.matches("(.*?)<script>(.*?)") ||
//                  value.matches("(.*?)</script>(.*?)")
//                  )
//              {
//
//                //LOGGER.warn("Possible XSS Attack : Invalid input found on the page - " + item.getFieldName() + " - " + item.getString());
//                throw new ServletException("Invalid input found on the page");
//              }
//            }
//          }
//        }
//      }
//    } catch (FileUploadException e)
//    {
//      throw new ServletException(e);
//    }
//  }
}