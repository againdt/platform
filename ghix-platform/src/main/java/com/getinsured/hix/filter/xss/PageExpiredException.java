package com.getinsured.hix.filter.xss;

public class PageExpiredException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PageExpiredException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PageExpiredException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PageExpiredException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PageExpiredException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PageExpiredException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
