package com.getinsured.hix.filter.xss;

/**
 * Thrown to indicate that input data is a malicious one.
 *
 */
public class GhixXssRuntimeException extends RuntimeException {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1053471180051791761L;

	public GhixXssRuntimeException() {
		super();
	}

	public GhixXssRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public GhixXssRuntimeException(String message) {
		super(message);
	}

	public GhixXssRuntimeException(Throwable cause) {
		super(cause);
	}

}
