package com.getinsured.hix.filter.xss;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.log4j.Logger;
import org.owasp.esapi.AccessReferenceMap;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.RandomAccessReferenceMap;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

public final class RequestWrapper extends HttpServletRequestWrapper {
	
	private static final Logger LOGGER = Logger.getLogger(RequestWrapper.class);

	public RequestWrapper(HttpServletRequest servletRequest, ServletContext servletContext) {
		super(servletRequest);
		applicationContext = RequestContextUtils.getWebApplicationContext(servletRequest,servletContext);
		ghixJasyptEncrytorUtil = applicationContext.getBean("ghixJasyptEncrytorUtil",GhixJasyptEncrytorUtil.class);
		this.request = servletRequest; 
	}
	
	
	private ApplicationContext applicationContext;
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	private HttpServletRequest request;
	
	@Override
	public String[] getParameterValues(String parameter) {
		String[] values = super.getParameterValues(parameter);
		ServletRequest request = getRequest();
		AccessReferenceMap<String> map = (RandomAccessReferenceMap)((HttpServletRequest)request).getSession().getAttribute("accessReferenceMap");
		String value;
		if (values == null) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++) {
			value = values[i];
			if(value.startsWith(GhixJasyptEncrytorUtil.GENC_TAG_START)){
				value = ghixJasyptEncrytorUtil.decryptGencStringByJasypt(value);
			}
			value = processIndirectReferences(parameter, values, request, map,
					value, i);
			
			encodedValues[i] = cleanXSS(value);
		}
		
		request.setAttribute(parameter, encodedValues);
		return encodedValues;
	}

	private String processIndirectReferences(String parameter, String[] values,
			ServletRequest request, AccessReferenceMap<String> map, String value, int i) {
		if(null != map) {
			String indVal = null;
			try {
				indVal = request.getParameter(parameter);
				value = (String)map.getDirectReference(indVal);
			} catch (AccessControlException e) {
				value = values[i];
			} catch(Exception ex) {
				value = values[i];
			} finally {
				if(null != map) {
					try {
						map.removeDirectReference(value);
					} catch (Exception e) {
						value = values[i];
					}
				}
			}
		}
		return value;
	}

	@Override
	public String getParameter(String parameter) {
		String value = super.getParameter(parameter);
		if (value == null) {
			return null;
		}
		if(value.startsWith(GhixJasyptEncrytorUtil.GENC_TAG_START)){
			value = ghixJasyptEncrytorUtil.decryptGencStringByJasypt(value);
		}
		String encodedValue = cleanXSS(value);
		ServletRequest request = getRequest();
		request.setAttribute(parameter, encodedValue);
		return encodedValue;
	}

	@Override
	public String getHeader(String name) {
		String value = super.getHeader(name);
		if (value == null){
			return null;
		}
		return cleanXSS(value);
	}

	private String cleanXSS(String value) {
		if (value == null){
			return null;
		}
		/*HIX-30979 The text edited in source of any notification through opsadmin >> Notices>> Manage Notifications is not getting saved*/
		if(!   ( this.getRequestURI().equals("/hix/admin/editTemplate")||
				 this.getRequestURI().equals("/hix/admin/prepareTemplateReview") ||
				 this.getRequestURI().equals("/hix/selfservice/affiliate/saveCompleteAffiliate")
				)
		  )
		{
			StringBuilder inputHolder = new StringBuilder();
			final boolean possibleAttack = XssHelper.stripXSS(inputHolder, value);
			
			if(possibleAttack) {
				logAttackDetails();
			}
			return inputHolder.toString();
		}
		else{
			return value;
		}
		
	}
	
	/**
	 * 
	 * @param request
	 */
	private void logAttackDetails() {
		String ipAddress = null;
		Object principal = null;
		int userId = -1;
		Authentication authentication = null;
		
		AccountUser accountUser = null;
		
		try {
			ipAddress = request.getHeader("x-forwarded-for");
			if(null == ipAddress || ipAddress.isEmpty()) {
				ipAddress = request.getRemoteAddr();
			}
			authentication = SecurityContextHolder.getContext().getAuthentication();
			if(null != authentication) {
				principal = authentication.getPrincipal();
				if(principal instanceof String) {
				} else if(principal instanceof AccountUser) {
					accountUser = (AccountUser) principal;
					userId = accountUser.getId();
				} 
			}
		} catch (Exception ex) {
			LOGGER.warn("FAILED TO LOG CSRF ATTACK DETAILS",ex);
		} finally {
			LOGGER.warn("POSSIBLE XSS ATTACK DETAILS: User Id: "+userId+" IP ADDR: "+ipAddress);
		}
		
	}

}