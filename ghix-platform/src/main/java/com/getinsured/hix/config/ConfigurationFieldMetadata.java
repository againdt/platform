package com.getinsured.hix.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.platform.util.GhixHome;

/**
 * Replacement for {@link ConfigFieldsMetaData}.
 *
 * Created by golubenko_y on 10/13/16.
 */
public class ConfigurationFieldMetadata extends HashMap<String, FieldMetadata>
{
  private static final long serialVersionUID = 1518885543160523382L;
  private static final Logger log = LoggerFactory.getLogger(ConfigurationFieldMetadata.class);
  private static final String METADATA_XML_FILE = "ConfigurationMetaData.xml";
  private static XMLInputFactory xmlInputFactory = null;

  static
  {
    xmlInputFactory = XMLInputFactory.newFactory();

    if (xmlInputFactory.isPropertySupported("javax.xml.stream.isValidating"))
    {
      xmlInputFactory.setProperty("javax.xml.stream.isValidating", Boolean.FALSE);
    }
  }

  private static XMLInputFactory getXmlInputFactory()
  {
    return xmlInputFactory.newInstance();
  }

  private ConfigurationFieldMetadata() {

  }

  /**
   * Reads XML file {@link #METADATA_XML_FILE} and applies validator with constrains.
   *
   * @return {@link ConfigurationFieldMetadata}.
   */
  public static ConfigurationFieldMetadata getMetadataMap() throws FileNotFoundException, XMLStreamException, InvalidOperationException
  {
    ConfigurationFieldMetadata cfm = new ConfigurationFieldMetadata();
    return cfm.readConfigurationXml(new File(GhixHome.getBaseConfigurationDir().getAbsolutePath() + File.separatorChar + METADATA_XML_FILE));
  }

  private ConfigurationFieldMetadata readConfigurationXml(final File xmlFile) throws FileNotFoundException, XMLStreamException, InvalidOperationException
  {
    if(xmlFile == null || !xmlFile.isFile() || !xmlFile.exists() || !xmlFile.canRead())
    {
      if(log.isTraceEnabled())
      {
        if(xmlFile != null)
        {
          log.trace("No validation will be performed for GIAppConfigServiceImpl::validateProperty(). Unable to read given XML file: {}. Regular file: {}; Exists: {}; Readable: {}",
              xmlFile.getAbsolutePath(), xmlFile.isFile(), xmlFile.exists(), xmlFile.canRead());
        }
        else
        {
          log.trace("Unable to read given XML file. Null file given.");
        }
      }
    }
    else
    {
      if(log.isTraceEnabled())
      {
        log.trace("Reading EntityID attribute from given XML file: {}", xmlFile.getAbsolutePath());
      }

      final XMLInputFactory factory = getXmlInputFactory();

      XMLStreamReader xmlStreamReader = null;
      InputStream fis = null;

      try
      {
        fis = new FileInputStream(xmlFile);

        xmlStreamReader = factory.createXMLStreamReader(fis);

        return parseXml(xmlStreamReader);
      }
      catch(FileNotFoundException | XMLStreamException | InvalidOperationException fnfe)
      {
        if(log.isErrorEnabled())
        {
          log.error("Could not create FileInputStream from file: {}", xmlFile.getAbsolutePath(), fnfe);
        }

        throw fnfe;
      }
      finally
      {
        try
        {
          if(xmlStreamReader != null)
          {
            xmlStreamReader.close();
          }

          if(fis != null)
          {
            fis.close();
          }
        }
        catch(Exception e) {
          /* Ignore */
        }
      }
    }

    return null;
  }

  private ConfigurationFieldMetadata parseXml(XMLStreamReader xmlStreamReader) throws XMLStreamException, InvalidOperationException
  {
    ConfigurationFieldMetadata cmd = new ConfigurationFieldMetadata();

    List<FieldMetadata> l = new ArrayList<>();

    while(xmlStreamReader.hasNext())
    {
      xmlStreamReader.next();

      if (xmlStreamReader.isStartElement())
      {
        final String tagName = xmlStreamReader.getLocalName();

        if ("field".equals(tagName))
        {
          FieldMetadata fmd = new FieldMetadata();

          for (int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
          {
            final String attributeName = xmlStreamReader.getAttributeName(i).getLocalPart();

            if ("name".equals(attributeName))
            {
              fmd.setName(xmlStreamReader.getAttributeValue(i));
            }
            else if("isMandatory".equals(attributeName))
            {
              fmd.setMandatory(Boolean.valueOf(xmlStreamReader.getAttributeValue(i)));
            }
            else if("hasMultipleValues".equals(attributeName))
            {
              fmd.setHasMultipleValues(Boolean.valueOf(xmlStreamReader.getAttributeValue(i)));
            }
            else if("scope".equals(attributeName))
            {
              fmd.setScope(xmlStreamReader.getAttributeValue(i));
            }
            else if("id".equals(attributeName))
            {
              fmd.setIdentifier(xmlStreamReader.getAttributeValue(i));
            }
            else if("validationRequired".equals(attributeName))
            {
              fmd.setValidationRequired(Boolean.valueOf(xmlStreamReader.getAttributeValue(i)));
            }
            else if("profiles".equals(attributeName))
            {
              fmd.setProfiles(xmlStreamReader.getAttributeValue(i));
            }
            else if("type".equals(attributeName))
            {
              fmd.setType(xmlStreamReader.getAttributeValue(i));
            }
            else
            {
              if(log.isDebugEnabled())
              {
                log.debug("Ignoring attribute [{}]", attributeName);
              }
            }
          }

          // Do we have <processor /> tag as child of <field/>?
          if(xmlStreamReader.nextTag() == XMLStreamConstants.START_ELEMENT)
          {
            ConfigDataFieldProcessor processor = null;

            if("processor".equals(xmlStreamReader.getName().getLocalPart()))
            {
              if(xmlStreamReader.getAttributeCount() > 0)
              {
                for (int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
                {
                  final String attributeName = xmlStreamReader.getAttributeName(i).getLocalPart();

                  if ("name".equals(attributeName))
                  {
                    try
                    {
                      processor = (ConfigDataFieldProcessor) Class.forName(xmlStreamReader.getAttributeValue(i)).newInstance();
                    }
                    catch (ClassNotFoundException | InstantiationException | IllegalAccessException e)
                    {
                      log.error("Exception while loading/instantiating class: " + xmlStreamReader.getAttributeValue(i) + " defined in: " + METADATA_XML_FILE, e);
                      throw new InvalidOperationException("Exception while loading/instantiating class: " + xmlStreamReader.getAttributeValue(i) + " defined in: " + METADATA_XML_FILE, e);
                    }

                    break;
                  }
                }

                for (int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
                {
                  final String attributeName = xmlStreamReader.getAttributeName(i).getLocalPart();

                  if ("index".equals(attributeName) && processor != null)
                  {
                    processor.setIndex(Integer.valueOf(xmlStreamReader.getAttributeValue(i)));
                    break;
                  }
                }
              }

              // Do we have <context /> tag as child of <processor />?
              if(xmlStreamReader.nextTag() == XMLStreamConstants.START_ELEMENT)
              {
                if("context".equals(xmlStreamReader.getName().getLocalPart()))
                {
                  if(log.isTraceEnabled())
                  {
                    log.trace("<context/> tag found in <processor />");
                  }

                  ValidationContext vc = new ValidationContext();

                  if(xmlStreamReader.getAttributeCount() > 0)
                  {
                    for(int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
                    {
                      vc.addContextInfo(xmlStreamReader.getAttributeName(i).getLocalPart(), xmlStreamReader.getAttributeValue(i));
                    }
                  }

                  // Do we have <constraint /> tag as child of <context/>?
                  if(xmlStreamReader.nextTag() == XMLStreamConstants.START_ELEMENT && "constraint".equals(xmlStreamReader.getName().getLocalPart()))
                  {
                    if(log.isTraceEnabled())
                    {
                      log.trace("<constraint /> tag found in <context/>");
                    }

                    if(xmlStreamReader.getAttributeCount() > 0)
                    {
                      FieldConstraints fc = new FieldConstraints();

                      for(int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
                      {
                        if("index".equals(xmlStreamReader.getAttributeValue(i)))
                        {
                          fc.setIndex(Integer.valueOf(xmlStreamReader.getAttributeValue(i)));
                        }
                        else
                        {
                          try
                          {
                            fc.addConstraintAttribute(xmlStreamReader.getAttributeName(i).getLocalPart(), xmlStreamReader.getAttributeValue(i));
                          }
                          catch (InvalidOperationException e)
                          {
                            e.printStackTrace();
                          }
                        }
                      }

                      vc.addFieldConstraint(fc);
                    }
                  }

                  processor.setValidationContext(vc);
                }
              }

              fmd.addProcessor(processor);
            }
          }

          l.add(fmd);
        }
      }
    }

    l.forEach(f -> {
      cmd.put(f.getName(), f);
    });

    return cmd;
  }
}
