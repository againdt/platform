package com.getinsured.hix.config;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigDataField {

	private FieldMetadata fieldMetaData = null;
	private String name = null;
	private Object value = null;
	private ConfigValidationException validationException = null;
	private static Logger logger = LoggerFactory.getLogger(ConfigDataField.class);
	private boolean id = false;
	private ArrayList<String> processedValue;
	private String multiValueFieldName;
	private boolean multiValue;
	private int lineNo;
	private int column;

	public ConfigDataField() {
		// TODO Auto-generated constructor stub
	}

	public FieldMetadata getFieldMetaData() {
		return fieldMetaData;
	}

	public void setFieldMetaData(FieldMetadata fieldMetaData)
			throws InvalidOperationException {
		this.name = fieldMetaData.getName();
		this.fieldMetaData = fieldMetaData;
	}

	public Object getValue() {
		return value;
	}

	/*
	 * This methods assumes that value is always quoted and an empty value is
	 * provided as "" I have to hard code it here but this is the easiest thing
	 * to do at this point of time Ideally this information should come from the
	 * configuration file
	 */
	public void setValue(String val,  boolean validationRequired)
			throws InvalidOperationException {
		Object tmpObj = null;
		ValidationContext tmpContext = null;
		ConfigDataFieldProcessor processor = null;
		if(!validationRequired){
			this.value = val;
			return;
		}
		
		if(val == null){
			if (this.fieldMetaData.isMandatory()) {
				this.validationException = new ConfigValidationException(
						this.name + " is Mandatory, can not be null");
				logger.error("Encountered ValidationException "
						+ this.validationException.getMessage()
						+ " Metadata:" + this.fieldMetaData.toString());
			}
			return;
		}

		try {
			if (this.fieldMetaData == null) {
				throw new InvalidOperationException(
						"Invalid ProviderDataField, No metadata found");
			}
			if(logger.isDebugEnabled()){
				logger.debug("Removing single quotes from the value ["
					+ val
					+ "] provided, may not be true for all the files we process");
			}
			if (val.equals("\\N")) {
				logger.debug("Unexpected value:\\N found for " + this.name
						+ ", defaulting to agreed empty string \"\"");
				val = "\"\"";
			}
//			if (val.charAt(0) != '"') {
//				throw new ProviderValidationException(
//						"Value ["
//								+ val
//								+ "] for field:\""
//								+ this.name
//								+ "\" not in standard format of enclosed double quotes on line#"
//								+ this.lineNo
//								+ ". Possible delimiter issue in previous field");
//			}
			val = val.replaceAll("\"", "");
			if(logger.isDebugEnabled()){
				logger.debug("Setting value:" + val);
			}
			if (val.length() == 0) {
				if(logger.isDebugEnabled()){
					logger.debug("Empty value, checking if \"" + this.name
						+ "\" is mandatory");
				}
				if (this.fieldMetaData.isMandatory()) {
					this.validationException = new ConfigValidationException(
							this.name + " is Mandatory, can not be empty");
					
						logger.error("Encountered ValidationException "
							+ this.validationException.getMessage()
							+ " Metadata:" + this.fieldMetaData.toString());
					return;
					
				}
			} else {// Value is not empty and we need to process it as per the
					// constraints
				int processors = this.fieldMetaData.getProcessorCount();
				if(logger.isTraceEnabled()){
					logger.trace("Found " + processors + " processors");
				}
				tmpObj = val;
				for (int i = 0; i < processors; i++) {
					processor = this.fieldMetaData.getProcessor(i);
					if(logger.isTraceEnabled()){
						logger.trace("Processsor [" + i + "] " + this.name
							+ " for " + processor.getClass().getName()
								+ " Value to be processed:" + val);
					}
					tmpObj =  processor.process(tmpObj);
					tmpContext = processor.getValidationContext();
					if(tmpContext == null){
						logger.warn("No context available from processor:"+processor.getClass().getName()+" Should not have happened!!");
					}
					extractProcessedFields(processor.getValidationContext());
				}
			}
		} catch (ConfigValidationException pe) {
			this.validationException = pe;
			//logger.error("Encountered ValidationException " + pe.getMessage()+ ", Metadata:" + this.fieldMetaData.toString());
			return;
		}
		this.value = tmpObj;
		if(logger.isDebugEnabled()){
			logger.debug("Set value:"+this.name+" Value:"+this.value);
		}
		if (this.fieldMetaData.getIdentifier() != null) {
			if(logger.isTraceEnabled()){
				logger.trace("Defining " + this.name
					+ " as identity field for this record");
			}
			this.id = true;
		}
	}

	@SuppressWarnings("unchecked")
	private void extractProcessedFields(ValidationContext validationContext)
			throws InvalidOperationException, ConfigValidationException {
		if (this.fieldMetaData.isHasMultipleValues()) {
			this.multiValue = true;
			this.multiValueFieldName = (String) validationContext
					.getContextField("output_field");
			Object obj = validationContext.getContextField(multiValueFieldName);
			if (obj == null) {
				if(this.fieldMetaData.isMandatory()){
					logger.error(this.name
						+ " is a Multi value field and processor didn't return any looked for "
						+ multiValueFieldName);
					throw new ConfigValidationException(
						"Multi value processor did not return the processed value");
				}
			}else{
				try {
					this.setProcessedValue((ArrayList<String>) obj);
				} catch (ClassCastException ce) {
					throw new InvalidOperationException(
						"Processde value is expected in form of an ArrayList");
				}
			}
		}

	}

	public boolean isAnIdentityField() {
		return this.id;
	}

	public ConfigValidationException getValidationException() {
		return validationException;
	}

	public void setValidationException(
			ConfigValidationException validationException) {
		this.validationException = validationException;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getProcessedValue() {
		return processedValue;
	}

	public void setProcessedValue(ArrayList<String> processedValue) {
		this.processedValue = processedValue;
	}

	public boolean isMultiValue() {
		return multiValue;
	}

	public String getMultiValueFieldName() {
		return multiValueFieldName;
	}

	public void setMultiValueFieldName(String multiValueFieldName) {
		this.multiValueFieldName = multiValueFieldName;
	}

	public void setRecordInfo(int lineNo, int column) {
		this.lineNo = lineNo;
		this.column = column;
	}

	public String getRecordInfo() {
		return "[Line No:" + this.lineNo + " Column:" + this.column + "] Name:\""
				+ this.name+"\"";
	}

	public boolean isValidProfile(String profile)  {
		String profiles = fieldMetaData.getProfiles();
		boolean valid = false;
		if(profiles != null){
			String[] allowedProfiles = profiles.split(",");
		
			for(String s: allowedProfiles){
				if(s.compareToIgnoreCase(profile) == 0){
					valid = true;
					break;
				}
			}
			if(!valid){
				this.validationException = new ConfigValidationException(" Validation failed for field:"+ name +" as it is not applicable to profile:"+ profile);
			}
		}else{
			valid = true;
		}
		return valid;
	}
}
