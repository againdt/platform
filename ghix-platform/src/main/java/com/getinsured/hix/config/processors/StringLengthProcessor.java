package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;


public class StringLengthProcessor implements ConfigDataFieldProcessor{

	private ValidationContext context;
	private int index;
	public StringLengthProcessor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object process(Object obj) throws ConfigValidationException {
		String s;
		int len;
		int sLen;
		String maxObj = null;
		String minObj = null;
		try{
			s = (String)obj;
			sLen = s.length();
		}catch (ClassCastException ce){
			throw new ConfigValidationException("Invalid input for length validation", ce);
		}
		String length = (String)context.getNamedConstraintField("length");
		//First check if exact length is expected
		if(length != null){
			len = Integer.valueOf(length);
			if(sLen != len){
				throw new ConfigValidationException("Value length validation failed, Expected:"+len+" Found:"+s.length());
			}
		}else{
		// Now check if range validation is expected
			maxObj = context.getNamedConstraintField("max");
			minObj = context.getNamedConstraintField("min");
			try{
				if(maxObj != null){
					int max = Integer.valueOf(maxObj);
					if(sLen > max){
						throw new ConfigValidationException ("validation failed for maximum length value [ max:"+max+"] found: "+sLen);
					}
				}
				if(minObj != null){
					int min = Integer.valueOf(minObj);
					if(sLen < min){
						throw new ConfigValidationException ("validation failed for minimum length value [min:"+min+"] found: "+sLen);
					}
				}
			}catch(NumberFormatException ne){
				throw new ConfigValidationException("Illegal validation context", ne);
			}
		}
		return s;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
			this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
