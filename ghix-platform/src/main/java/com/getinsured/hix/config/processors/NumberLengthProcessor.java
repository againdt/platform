package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;

public class NumberLengthProcessor implements ConfigDataFieldProcessor {

	private ValidationContext context = null;
	private int index;
	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException {
		String s = null;
		String maxObj = null;
		String minObj = null;
		String length = context.getNamedConstraintField("length");
		try{
			s = objToBeValidated.toString();
			
			for(char c: s.toCharArray()){
				if(!Character.isDigit(c)){
					throw new ConfigValidationException("Input ["+s+"] not in number format");
				}
			}
			//First check if exact length is expected
			int sLen = s.length();
			if(length != null){
				int len = Integer.valueOf(length);
				if(sLen != len){
					throw new ConfigValidationException("Value length validation failed, Expected:"+len+" Found:"+s.length());
				}
			}else{
			// Now check if range validation is expected
				maxObj = context.getNamedConstraintField("max");
				minObj = context.getNamedConstraintField("min");
				try{
					if(maxObj != null){
						int max = Integer.valueOf(maxObj);
						if(sLen > max){
							throw new ConfigValidationException ("validation failed for maximum value [ max length:"+max+"] found: "+sLen);
						}
					}
					if(minObj != null){
						int min = Integer.valueOf(minObj);
						if(sLen < min){
							throw new ConfigValidationException ("validation failed for minimum value [min length:"+min+"] found: "+sLen);
						}
					}
				}catch(NumberFormatException ne){
					throw new ConfigValidationException("Illegal validation context", ne);
				}
			}
		}catch(ClassCastException ce){
			throw new ConfigValidationException("Invalid validation context provided expecting a number for \"length\"");
		}
		return s;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
		
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}
}
