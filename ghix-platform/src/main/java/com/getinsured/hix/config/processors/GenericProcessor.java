package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;

public class GenericProcessor implements ConfigDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException {
				return objToBeValidated;
		// TODO Auto-generated method stub

	}

}
