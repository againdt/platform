package com.getinsured.hix.config.model;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.config.ConfigDataField;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ConfigurationFieldMetadata;
import com.getinsured.hix.config.FieldMetadata;
import com.getinsured.hix.config.InvalidOperationException;

public class ConfigPageData {
	private static Logger LOGGER = LoggerFactory.getLogger(ConfigPageData.class);
	private static ConfigurationFieldMetadata metaDataMap;
	private static boolean configFileNotFound=false;
	
	
	@SuppressWarnings("unchecked")
	public static boolean validateConfigProperty(String keyName, String value) throws InvalidOperationException, ConfigValidationException {
		boolean bValid = false;
		if(metaDataMap == null){
			if(!configFileNotFound){
				try {
					metaDataMap = ConfigurationFieldMetadata.getMetadataMap();
				}catch (FileNotFoundException e) {
					LOGGER.warn("Configuration metadata file not found.", e);
					configFileNotFound = true;
					bValid = true; // Not breaking the server startup due to absence of metadata file
					return bValid;
				} catch (XMLStreamException e) {
					LOGGER.error("Configuration metadata information could not be loaded.", e);
					return bValid;
				}
			}else{
				bValid = true; // Not breaking the server startup due to absence of metadata file in subsequent calls to this function
				return bValid;
			}
		}

//		LOGGER.debug(" Processing: Field:" + keyName + " Value:" + value);

		ConfigDataField field = new ConfigDataField();

		// /opt/web/ghixhome/ghix-setup doesn't contain ConfigurationMetadata.xml
		FieldMetadata md = null;

		if(metaDataMap != null)
		{
			md = metaDataMap.get(keyName);
		}

		if (md == null) {
			//LOGGER.info("Unknown input field ["+ keyName + "=" + value + "]");
			bValid = true; // Needs to be changed to false if meta data for every field is to be made mandatory
		}else{

			field.setFieldMetaData(md);
			field.setValue(value, md.getValidationRequired());

			if (field.isMultiValue()) {
				String name = field.getMultiValueFieldName();
				ArrayList<String> multiVals = field.getProcessedValue();
				if (multiVals == null) {
					LOGGER.debug(field.getName()
							+ " is a multi field without any fields, expected multi fields with name:"
							+ name);
				} else {
					LOGGER.debug(field.getName() + " is a multi field with:"
							+ multiVals.size() + " fields, each with name:" + name);
				}
			}

			ConfigValidationException ce = field.getValidationException();
			if(ce!= null){
				throw ce;
			}
			bValid = true;
			
		}
		return bValid;
	}
}
