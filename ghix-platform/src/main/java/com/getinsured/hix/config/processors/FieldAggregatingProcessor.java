package com.getinsured.hix.config.processors;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.config.ConfigDataField;
import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.ValidationContext;


public class FieldAggregatingProcessor implements ConfigDataFieldProcessor {

	private int index;
	private ValidationContext validationContext;
	private static Logger logger = LoggerFactory.getLogger(FieldAggregatingProcessor.class);

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.validationContext = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.validationContext;
	}

	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException, InvalidOperationException {
		String fieldSet = this.validationContext.getNamedConstraintField("field_set");
		@SuppressWarnings("unchecked")
		ArrayList<ConfigDataField> allFields = (ArrayList<ConfigDataField>) this.validationContext.getContextField("record_fields");
		String[] fields = null;
		ArrayList<String> outputList = null;
		String tmp = "";
		String tmpVal = null;
		Object obj = null;
		//Check the type of aggregation required, List OR String concatenation
		String aggregationType = this.validationContext.getNamedConstraintField("aggregate_as");
		String includeFieldName = this.validationContext.getNamedConstraintField("include_field_name");
		int type = Integer.valueOf(aggregationType);
		if(fieldSet != null){
			fields = fieldSet.split(",");
			
			for(String field: fields){
				tmpVal = this.getFieldValue(allFields, field,includeFieldName);
				switch(type){
					case 0:
						//String
						if(tmpVal != null && tmpVal.length() > 0){
							tmp += tmpVal+",";
						}
						break;
					case 1:
						if(outputList == null){
							outputList = new ArrayList<String>();
						}
						if(tmpVal != null && tmpVal.length() > 0){
							outputList.add(tmpVal);
						}
						break;
					default:
						tmp = "Aggregation Type ["+aggregationType+"] not supported for "+field;
						logger.error(tmp);
						throw new InvalidOperationException(tmp);
				}
			}
			logger.debug("Aggregation completed with value "+tmp);
			switch(type){
				case 0:
					tmp = tmp.substring(0,tmp.length()-1);
					obj = tmp;
					break;
				case 1:
					obj = outputList;
					break;
				default:
			}
			String outputField = (String) this.validationContext.getContextField("output_field");
			if(outputField == null){
				outputField = "address";
			}
			this.validationContext.addContextInfo("output_field", outputField);
			this.validationContext.addContextInfo(outputField, obj);
		}
		return obj;
	}
	
	private String getFieldValue(ArrayList<ConfigDataField> fieldList, String name, String incFieldName) throws InvalidOperationException{
		String val;
		boolean includeFieldNames = false;
		if(incFieldName != null){
			includeFieldNames = Boolean.valueOf(incFieldName);
		}
		for(ConfigDataField pd: fieldList){
			if(pd.getName().equalsIgnoreCase(name)){
				val = (String)pd.getValue();
				if(includeFieldNames){
					return pd.getName()+":"+val;
				}
				return val;
			}
		}
		logger.debug("Failed Lookig up field with name:"+name);
		throw new InvalidOperationException("Failed to lookup field with name:"+name);
	}

}
