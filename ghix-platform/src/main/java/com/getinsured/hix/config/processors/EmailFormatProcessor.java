package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailFormatProcessor implements ConfigDataFieldProcessor{

	private ValidationContext context;
	private int index;

	private Pattern pattern;
	private Matcher matcher;
 
	private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
 
	public EmailFormatProcessor() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}
	
	@Override
	public Object process(Object objToBeValidated) throws ConfigValidationException {
		String email = (String)objToBeValidated;
		matcher = pattern.matcher(email);
		if(!matcher.matches()){
			throw new ConfigValidationException("Input ["+ email +"] not in email format");
		}
		return objToBeValidated;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}
	
	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}
	
	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
