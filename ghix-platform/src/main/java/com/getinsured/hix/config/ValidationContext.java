package com.getinsured.hix.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationContext {
	private Logger logger = LoggerFactory.getLogger(ValidationContext.class);
	private List<FieldConstraints> constraints = null;
	
	
	public HashMap<String, Object> context;
	
	public void addContextInfo(String key, Object ctx){
		if(this.context == null){
			this.context = new HashMap<String, Object>();
		}
		this.context.put(key, ctx);
	}
	
	public Object getContextField(String name){
		if(this.context == null){
			logger.warn("Context does not have any information, are you sure it was initialized?");
			return null;
		}
		return this.context.get(name); 
	}
	
	public void addFieldConstraint(FieldConstraints fc){
		if(this.constraints == null){
			this.constraints = new ArrayList<FieldConstraints>();
		}
		this.constraints.add(fc.getIndex(), fc);
	}
	
	public int getConstraintsCount(){
		return constraints.size();
	}
	
	public FieldConstraints getFieldConstraint(int index){
		if(this.constraints == null || index >= constraints.size()){
			return null;
		}
		return this.constraints.get(index);
	}
	
	public String toString(){
		String tmp= "";
		if(this.constraints != null){
			tmp = "Constraints: "+this.constraints.toString();
		}
		if(this.context != null){
			tmp += "Context:"+this.context.toString();
		}
		return tmp;
	}

	public String getNamedConstraintField(String field) {
		String tmp = null;
		if(this.constraints == null){
			logger.warn("No context available for this field, returning null for "+field);
			return null;
		}
		for (FieldConstraints fc : this.constraints){
			tmp = fc.getConstraintAttribute(field);
			if(tmp != null){
				return tmp;
			}
		}
		logger.debug("No constraint found with name:"+field);
		return null;
	}

}
