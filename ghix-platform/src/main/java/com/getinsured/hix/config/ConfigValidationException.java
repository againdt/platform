package com.getinsured.hix.config;

public class ConfigValidationException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ConfigValidationException() {
		// TODO Auto-generated constructor stub
	}

	public ConfigValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConfigValidationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigValidationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/***
	 * Commenting out as this constructor is available on JDK 1.7 onwards only
	public ProviderValidationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	*/

}
