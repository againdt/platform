package com.getinsured.hix.config.processors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;

public class DateFieldProcessor implements ConfigDataFieldProcessor{

	private ValidationContext context;
	private int index;

	private boolean futureDateAllowed = true;
	private String dateFormat;

	@Override
	public Object process(Object objToBeValidated) throws ConfigValidationException {
		dateFormat = (String) this.context.getNamedConstraintField("dateFormat");
		validateDate(objToBeValidated);
		return objToBeValidated;
	}
	
	private void validateDate( Object value) throws ConfigValidationException {
		
		SimpleDateFormat sdf = new SimpleDateFormat(this.dateFormat);
		sdf.setLenient(false);
		try{
			Date providedDate = sdf.parse((String)value);
			Date current = new TSDate(System.currentTimeMillis());
			if(providedDate.after(current) && !this.futureDateAllowed){
				throw new ConfigValidationException(value+" provided with future date");
			}
		}catch(ParseException pe){
				throw new ConfigValidationException("failed to validate the parameter "+value+" invalid format ["+this.dateFormat+"] provided",pe);
		}
	}
	
	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}
	
	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}
	
	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
