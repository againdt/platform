package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.ValidationContext;

public class KnownValueSetProcessor implements ConfigDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException, InvalidOperationException {
		String valueSet = context.getNamedConstraintField("value_set");
		if(valueSet == null){
			throw new InvalidOperationException(" Value set required for this this processor, please check you metadata file");
		}
		String[] values = valueSet.split(",");
		boolean valid = false;
		for(String s: values){
			if(s.equals(objToBeValidated)){
				valid = true;
				break;
			}
		}
		if(!valid){
			throw new ConfigValidationException(" Validation failed for value:"+objToBeValidated+" One of the following was expected:"+valueSet);
		}
		return objToBeValidated;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
