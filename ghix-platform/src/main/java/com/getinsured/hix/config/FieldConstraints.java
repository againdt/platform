package com.getinsured.hix.config;

import java.util.HashMap;

public class FieldConstraints {
	private int index;
	
	private HashMap<String, String> attrs = new HashMap<String, String>(); 
	
	public int getIndex() {
		return this.index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
	public void addConstraintAttribute(String name, String value) throws InvalidOperationException{
		if(this.attrs.containsKey(name)){
			throw new InvalidOperationException("Constraint ["+name+"]already available");
		}
		this.attrs.put(name, value);
	}
	
	public String getConstraintAttribute(String name){
		
		return this.attrs.get(name);
	}
	
	public String toString(){
		if(this.attrs != null){
			return this.attrs.toString();
		}
		return "Empty field constraints";
	}

}
