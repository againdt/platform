package com.getinsured.hix.config.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;

public class CharacterFilterProcessor implements ConfigDataFieldProcessor {
	int index;
	private ValidationContext context;
	private Logger logger = LoggerFactory.getLogger(CharacterFilterProcessor.class);
	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;

	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException, InvalidOperationException {
		String s = (String)objToBeValidated;
		String characterFilter = (String) this.context.getNamedConstraintField("character_filter");
		if(characterFilter != null){
			logger.debug("Filtering \""+s+"\" for "+characterFilter);
			StringBuilder sb = new StringBuilder();
			for(char c: s.toCharArray()){
				if(characterFilter.indexOf(c) != -1){
					continue;
				}
				sb.append(c);
			}
			s = sb.toString();
		}
		logger.debug("Filtered output:"+s);
		return s;
	}


}
