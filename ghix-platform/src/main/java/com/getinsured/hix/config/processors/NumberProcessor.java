package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;


public class NumberProcessor implements ConfigDataFieldProcessor {

	private ValidationContext context = null;
	private int index;
	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException {
		String s = objToBeValidated.toString();
		try{
			int value = Integer.valueOf(s);
			
			String minValue = (String) this.context.getNamedConstraintField("min_value");
			int min = 0;
			if(minValue != null){
				min = Integer.valueOf(minValue);
				if(value < min){
					throw new ConfigValidationException("Min Value validation failed, Expected:"+min+" Found:"+value);
				}
			}
			
			String maxValue = (String) this.context.getNamedConstraintField("max_value");
			int max = 0;
			if(maxValue != null){
				max = Integer.valueOf(maxValue);
				if(value > max){
					throw new ConfigValidationException("Max Value validation failed, Expected:"+max+" Found:"+value);
				}
			}
			
			for(char c: s.toCharArray()){
				if(!Character.isDigit(c)){
					throw new ConfigValidationException("Input ["+s+"] not in number format");
				}
			}
		}catch(Exception e){
			throw new ConfigValidationException("Number validation failed for ["+s+"]");
		}
		return s;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
		
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

	
}
