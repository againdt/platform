package com.getinsured.hix.config.processors;

import com.getinsured.hix.config.ConfigDataFieldProcessor;
import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.ValidationContext;

public class PhoneNumberProcessor implements ConfigDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public Object process(Object objToBeValidated)
			throws ConfigValidationException {
		String phone = objToBeValidated.toString();
		
		if(phone != null){
			 phone=phone.replaceAll("[\\D]","");
			 if(phone.length() < 9 || phone.length() > 11){
				 throw new ConfigValidationException("Input ["+ phone +"] not in valid phone number format");
			 }
		}
	 	return objToBeValidated;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
