package com.getinsured.hix.config;

public interface ConfigDataFieldProcessor{
	
	public void setIndex(int idx);
	public int getIndex(); 
	public void setValidationContext(ValidationContext validationContext);
	public ValidationContext getValidationContext();
	public Object process(Object objToBeValidated) throws ConfigValidationException, InvalidOperationException;
}
