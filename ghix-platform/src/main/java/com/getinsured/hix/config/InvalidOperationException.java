package com.getinsured.hix.config;

public class InvalidOperationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidOperationException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidOperationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidOperationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidOperationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
/**
	public InvalidOperationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	*/

}
