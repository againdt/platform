package com.getinsured.hix.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FieldMetadata {

	private String name;
	private String type;
	private boolean mandatory;
	private boolean hasMultipleValues;
	private String scope = null;
	private String profiles;
	private List<ConfigDataFieldProcessor> processors;
	
	private static final Logger logger = LoggerFactory.getLogger(FieldMetadata.class);
	private String identifier;
	private boolean validationRequired = true;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public boolean isHasMultipleValues() {
		return hasMultipleValues;
	}
	public void setHasMultipleValues(boolean hasMultipleValues) {
		this.hasMultipleValues = hasMultipleValues;
	}
	
	public int getProcessorCount(){
		return this.processors.size();
	}
	public ConfigDataFieldProcessor getProcessor(int index) {
		if(this.processors == null || index >= this.processors.size()){
			return null;
		}
		return this.processors.get(index);
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	public void addProcessor(ConfigDataFieldProcessor processor) {
		if(this.processors == null){
			this.processors = new ArrayList<ConfigDataFieldProcessor>();
		}

		if(logger.isDebugEnabled())
		{
			logger.debug("Adding processor for field: {} at index: {}", this.name, processor.getIndex());
		}

		this.processors.add(processor.getIndex(), processor);
	}
	
/*	public boolean fieldInScope(int readerScope) throws InvalidOperationException{
		if(this.scope == null){
			return true;
		}
		switch(readerScope){
		case ProviderFieldsMetadata.FACILITY:
			if(this.scope.equalsIgnoreCase("facility")){
				return true;
			}
			break;
		case ProviderFieldsMetadata.INDIVIDUAL:
			if(this.scope.equalsIgnoreCase("individual")){
				return true;
			}
			break;
			default:
				throw new InvalidOperationException("Invalid reader scope ["+readerScope+"] expected 0 or 1");
		}
		return false;
	}
*/		
	public String toString(){
		return "Name:"+this.name+",Type:"+this.type+",Mandatory:"+this.mandatory +",MultipleValues:"+this.hasMultipleValues;
	}
	public void setIdentifier(String tmpVal) {
		this.identifier = tmpVal;
	}
	
	public String getIdentifier(){
		return this.identifier;
	}
	
	public void setValidationRequired(boolean validationRequired) {
		this.validationRequired = validationRequired;
	}
	public boolean getValidationRequired() {
		return this.validationRequired;
	}
	public String getProfiles() {
		return profiles;
	}
	public void setProfiles(String profiles) {
		this.profiles = profiles;
	}
}
