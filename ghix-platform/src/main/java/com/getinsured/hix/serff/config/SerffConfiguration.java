package com.getinsured.hix.serff.config;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.PropertiesEnumMarker;

/**
 * Class is used to provide SERFF configuration through Enum from GI_APP_CONFIG table.
 * 
 * @author Bhavin Parmar
 */
public class SerffConfiguration {
	
	private static final Logger LOGGER = Logger.getLogger(SerffConfiguration.class);
	
	public enum SerffConfigurationEnum implements PropertiesEnumMarker {
		
		PV_BOOLEAN_VALUE("serff.PlanValidatorOptions.BooleanValue"),
		PV_MARKET_COVERAGE_TYPE("serff.PlanValidatorOptions.MarketCoverageType"),
		PV_PLAN("serff.PlanValidatorOptions.Plan"),
		PV_PLAN_TYPE("serff.PlanValidatorOptions.PlanType"),
		PV_COVERAGE_LEVEL("serff.PlanValidatorOptions.CoverageLevel"),
		PV_COVERAGE_LEVEL_DENTAL("serff.PlanValidatorOptions.CoverageLevelDental"),
		PV_PLAN_HEALTH("serff.PlanValidatorOptions.PlanHealth"),
		PV_DISEASE_MANAGEMENT("serff.PlanValidatorOptions.DiseaseManagement"),
		PV_CHILD_OFFERING("serff.PlanValidatorOptions.ChildOffering"),
		PV_RATES("serff.PlanValidatorOptions.Rates"),
		PV_BENEFIT_COVERAGE("serff.PlanValidatorOptions.BenefitCoverage"),
		PV_LIMIT_UNIT_CATOGERY1("serff.PlanValidatorOptions.LimitUnitCatogery1"),
		PV_LIMIT_UNIT_CATOGERY2("serff.PlanValidatorOptions.LimitUnitCatogery2"),
		PV_LIST_VALUES("serff.PlanValidatorOptions.ListValues"),
		PV_COPAY_COINSURANCE("serff.PlanValidatorOptions.CopayCoinsurance"),
		PV_COPAY_COINSURANCE_AFTER("serff.PlanValidatorOptions.CopayCoinsuranceAfter"),
		PV_COPAY_COINSURANCE_BEFORE("serff.PlanValidatorOptions.CopayCoinsuranceBefore"),
		PV_COPAY_PER_DAY("serff.PlanValidatorOptions.CopayPerDay"),
		PV_COPAY_PER_STAY("serff.PlanValidatorOptions.CopayPerStay"),
		PV_COINSURANCE_AFTER("serff.PlanValidatorOptions.CoinsuranceAfter"),
		PV_OTHER_COPAY_STRINGS("serff.PlanValidatorOptions.OtherCopayStrings"),
		PV_CATASTROPHIC_PLAN("serff.PlanValidatorOptions.CatastrophicPlan"),
		RV_RATE_AREA_PREFIX("serff.RatesValidatorOptions.RateAreaPrefix"),
		RV_RATE_AREA_MAX_NUMBER("serff.RatesValidatorOptions.RateAreaMaxNumber"),
		RV_RATE_TOBACCO("serff.RatesValidatorOptions.RateTobacco"),
		RV_RATE_FAMILY_OPTION("serff.RatesValidatorOptions.RateFamilyOption"),
		INV_MARKET_TYPE("serff.IssuerNCQAValidatorOptions.MarketType"),
		INV_ACCREDITATION_STATUS("serff.IssuerNCQAValidatorOptions.AccreditationStatus"),
		IUV_MARKET_TYPE("serff.IssuerURACValidatorOptions.MarketType"),
		IUV_ACCREDITATION_STATUS("serff.IssuerURACValidatorOptions.AccreditationStatus"),
		DV_NETWORK_COST_TYPE("serff.DrugValidatorOptions.NetworkCostType"),
		DV_IN_NETWORK_COST_TYPE("serff.DrugValidatorOptions.InNetworkCostType"),
		DV_COST_SHARING_TYPE("serff.DrugValidatorOptions.CostSharingType"),
		DV_COPAYMENT_COST_SHARING_TYPE("serff.DrugValidatorOptions.CopaymentCostSharingType"),
		DV_COINSURANCE_COST_SHARING_TYPE("serff.DrugValidatorOptions.CoinsuranceCostSharingType"),
		DV_COPAYMENT_DATA("serff.DrugValidatorOptions.CopayData"),
		DV_COINSURANCE_DATA("serff.DrugValidatorOptions.CoinsuranceData"),
		USE_BENEFIT_DISPLAY_RULES_FROM_DB("serff.UseDisplayRulesFromDB"),
		STATE_DENTAL_MAX_CHILD_AGE_LIMIT("serff.state.dental.maxChildAgeLimit");

		private final String value;

		public String getValue() {
			return this.value;
		}

		SerffConfigurationEnum(String value) {
			this.value = value;
		}
	}

	/**
	 * Method is used to check whether value is available in Property Value List or not.
	 * [Example: Property of BOOLEAN_VALUE is => "yes,no" then List.get(0)=yes, List.get(1)=no]
	 * @return True/False.
	 */
	public static boolean hasPropertyInList(String value, SerffConfigurationEnum propertyEnum) {

		if (StringUtils.isBlank(value) || null == propertyEnum) {
			return Boolean.FALSE;
		}
		LOGGER.debug("hasPropertyInList(): Property:" + propertyEnum.name() + " => " + propertyEnum.getValue());

		final String propertyValue = DynamicPropertiesUtil.getPropertyValue(propertyEnum);
		LOGGER.debug("hasPropertyInList(): Value: [" + value + "] => Property Value[" + propertyValue + "]");

		if (StringUtils.isBlank(propertyValue)) {
			return Boolean.FALSE;
		}

		List<String> list = Arrays.asList(propertyValue.split(","));
		return list.contains(value.toLowerCase());
	}

	/**
	 * Method is used to check whether Property Value is in contain of Value or not.
	 * [Example: Property of PV_COPAY_PER_DAY is => "copay per day,copay per stay" then Value = "18 Copay per Day"]
	 * @return True/False.
	 */
	public static boolean containsPropertyValue(String value, SerffConfigurationEnum propertyEnum) {

		if (StringUtils.isBlank(value) || null == propertyEnum) {
			return Boolean.FALSE;
		}
		LOGGER.debug("hasPropertyInList(): Property:" + propertyEnum.name() + " => " + propertyEnum.getValue());

		final String propertyValue = DynamicPropertiesUtil.getPropertyValue(propertyEnum);
		LOGGER.debug("containsPropertyValue(): Value: [" + value + "] => Property Value[" + propertyValue + "]");

		if (StringUtils.isBlank(propertyValue)) {
			return Boolean.FALSE;
		}

		List<String> list = Arrays.asList(propertyValue.split(","));
		for (String propValue : list) {

			if (StringUtils.isNotBlank(propValue) && value.toLowerCase().indexOf(propValue) > -1) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Method is used to check whether value is match with Property Value or not.
	 * [Example: Property of PV_CATASTROPHIC_PLAN is => "catastrophic" then Value = "Catastrophic"]
	 * @return True/False.
	 */
	public static boolean hasPropertyValue(String value, SerffConfigurationEnum propertyEnum) {

		if (StringUtils.isBlank(value) || null == propertyEnum) {
			return Boolean.FALSE;
		}
		LOGGER.debug("hasPropertyInList(): Property:" + propertyEnum.name() + " => " + propertyEnum.getValue());

		final String propertyValue = DynamicPropertiesUtil.getPropertyValue(propertyEnum);
		LOGGER.debug("hasPropertyValue(): Value: [" + value + "] => Property Value[" + propertyValue + "]");

		if (StringUtils.isNotBlank(propertyValue) && value.equalsIgnoreCase(propertyValue)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
