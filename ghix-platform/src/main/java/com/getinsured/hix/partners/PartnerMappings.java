package com.getinsured.hix.partners;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement(name = "PartnerMappings")
public class PartnerMappings {
	
	private static PartnerMappings partnerMappings = null;
	private static Logger logger = LoggerFactory.getLogger(PartnerMappings.class);
	private List<PartnerMap> partnerMap;
	
	public PartnerMap getPartnerMapForId(String id){
		logger.info("Total maps available:"+this.partnerMap.size());
		for(PartnerMap pMap: this.partnerMap){
			if(pMap.getId().equals(id)){
				return pMap;
			}
		}
		return null;
	}
	
	@XmlElement(name="PartnerMap")
	public List<PartnerMap> getPartnerMap() {
		return partnerMap;
	}

	public void setPartnerMap(List<PartnerMap> pMapList) {
		logger.info("****Setting partner maps "+pMapList.size());
		this.partnerMap = pMapList;
	}
	@SuppressWarnings("serial")
	public static PartnerMappings getPartnerMappings(String location) {
		 InputStream resource = null;
		 if(partnerMappings == null){
		        try {
		            JAXBContext jc = JAXBContext.newInstance(PartnerMappings.class,PartnerMap.class,LocalAttribute.class,ContextAttribute.class);
		            Unmarshaller u = jc.createUnmarshaller();
		            File mappingFile = new File(location,"PartnerMapping.xml");
		            if(!mappingFile.exists()){
		            	throw new FileNotFoundException("Partner Mapping file not available [Path:]"+mappingFile.getAbsolutePath()){
							public synchronized Throwable fillInStackTrace(){
		            			return null;
		            		}
		            	};
		            }
		            resource = new FileInputStream(mappingFile);
		            partnerMappings = (PartnerMappings) u.unmarshal( resource );   
		        } catch(Exception e) {
		            logger.error("Error initializing the partner maping information",e);
		        }finally{
		        	if(resource != null){
		        		try {
							resource.close();
						} catch (IOException e) {
							//Ignored
						}
		        	}
		        }
		 }
		 return partnerMappings;
	 }
	
	 public static PartnerMappings getPartnerMappings() {
		 InputStream resource = null;
		 if(partnerMappings == null){
		        try {
		            JAXBContext jc = JAXBContext.newInstance(PartnerMappings.class,PartnerMap.class,LocalAttribute.class,ContextAttribute.class);
		            Unmarshaller u = jc.createUnmarshaller();
		            resource = PartnerMappings.class.getClassLoader().getResourceAsStream("saml/PartnerMapping.xml");
		            if(resource == null){
		            	logger.info("Failed to load the resource");
		            	return null;
		            }
		            partnerMappings = (PartnerMappings) u.unmarshal( resource );   
		        } catch(Exception e) {
		            logger.error("Error initializing the partner maping information",e);
		        }finally{
		        	if(resource != null){
		        		try {
							resource.close();
						} catch (IOException e) {
							//Ignored
						}
		        	}
		        }
		 }
		 return partnerMappings;
	 }

	 // TODO: Create JUnit test instead of this:
	 public static void main(String[] args){
		 PartnerMappings pm = PartnerMappings.getPartnerMappings();
		 PartnerMap pmap = pm.getPartnerMapForId("DRX_RO");
		 System.out.println("Host: " + pmap.getIdpHostUrl());
		 System.out.println("Domain: "+ pmap.getTenantDomain());
		 System.out.println("Tenant supported: " + pmap.isTenantDomainSupported());
		 System.out.println("-----");
		 pmap = pm.getPartnerMapForId("DRX_RETAIL");
		 System.out.println("Host: " + pmap.getIdpHostUrl());
		 System.out.println("Domain: "+ pmap.getTenantDomain());
		 System.out.println("Tenant supported: " + pmap.isTenantDomainSupported());
	 }
}
