package com.getinsured.hix.partners;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class PartnerMap {
	
	private String id;
	
	private Class<?> clz;
	
	private String clsName;
	
	private String idpHostUrl;
	
	private String applicationId;
	
	private HashMap<String,Method> targetClassGetters = new HashMap<>();
	
	private List<LocalAttribute> localAttributes;
	
	private List<ContextAttribute> contextAtributes ;
	
	private List<String> targetUris;
	
	private boolean validated = false;
	
	private boolean tenantDomainSupported = true;
	
	private String tenantDomain = null;
	
	private String partnerTransPortHandler = null;
	
	private PartnerTransport partnerTransport;
	
	
	
	
	public PartnerMap(){
		
	}
	
	
	
	public boolean addLocalAttribute(LocalAttribute lAttr){
		if(this.localAttributes == null){
			this.localAttributes = new ArrayList<>();
		}
		if(!this.targetClassGetters.containsKey(lAttr.getName())){
			throw new RuntimeException("No attribute with name "+lAttr.getName()+" can be retrieved for "+this.id);
		}
		if(this.targetUris.contains(lAttr.getUri())){
			throw new RuntimeException("URI "+lAttr.getUri()+" already added for "+this.id+" please check the mapping file for duplicates");
		}
		this.targetUris.add(lAttr.getUri());
		return this.localAttributes.add(lAttr);
	}
	
	public boolean addContextAttribute(ContextAttribute ctxAttr){
		if(this.contextAtributes == null){
			this.contextAtributes = new ArrayList<>();
		}
		if(this.targetUris.contains(ctxAttr.getUri())){
			throw new RuntimeException("URI "+ctxAttr.getUri()+" already added for "+this.id +" please check the mapping file for duplicates");
		}
		this.targetUris.add(ctxAttr.getUri());
		return this.contextAtributes.add(ctxAttr);
	}
	
	public void validate(){
		if(validated){
			return;
		}
		if(this.localAttributes != null){
			for(LocalAttribute lAttr: this.localAttributes){
				if(!this.targetClassGetters.containsKey(lAttr.getName())){
					throw new RuntimeException("No attribute with name "+lAttr.getName()+" can be retrieved for "+this.id);
				}
				if(this.targetUris != null && this.targetUris.contains(lAttr.getUri())){
					throw new RuntimeException("URI "+lAttr.getUri()+" already added for "+this.id+" please check the mapping file for duplicates");
				}
				if(this.targetUris == null){
					this.targetUris = new ArrayList<>();
				}
				this.targetUris.add(lAttr.getUri());
			}
		}
		if(this.contextAtributes != null){
			for(ContextAttribute ctxAttr: this.contextAtributes){
				if(this.targetUris != null && this.targetUris.contains(ctxAttr.getUri())){
					throw new RuntimeException("URI "+ctxAttr.getUri()+" already added for "+this.id +" please check the mapping file for duplicates");
				}
				if(this.targetUris == null){
					this.targetUris = new ArrayList<>();
				}
				this.targetUris.add(ctxAttr.getUri());
			}
		}
		this.validated = true;
	}
	
	public HashMap<String, String> getPartnerParameters(Object obj, HashMap<String,String> context) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		this.validate();
		if(!obj.getClass().getName().equals(this.clz.getName())){
			throw new RuntimeException("Illegal call to retrieve partnet parameter for partner :"
							+this.id+" Target Class "
							+this.clz.getName()
							+" provided "
							+obj.getClass().getName());
		}
		HashMap<String,String> parnersParamMap = new HashMap<>();
		Object tmpVal = null;
		if(this.localAttributes != null){
			for(LocalAttribute attr: this.localAttributes){
				tmpVal = this.targetClassGetters.get(attr.getName()).invoke(obj);
				if(tmpVal == null && attr.isMandatory()){
					throw new RuntimeException("Attribute :"+attr.getName()+" is mandatory for partner id:"+this.id);
				}
				if(tmpVal != null){
					parnersParamMap.put(attr.getUri(), tmpVal.toString());
				}
			}
		}
		if(this.contextAtributes != null){
			for(ContextAttribute cAttr: this.contextAtributes){
				if(context != null){
					tmpVal = context.get(cAttr.getName());
				}
				if(tmpVal == null && cAttr.isMandatory()){
					throw new RuntimeException("Context Attribute :"+cAttr.getName()+" is mandatory for partner id:"+this.id);
				}
				if(tmpVal != null){
					parnersParamMap.put(cAttr.getUri(), tmpVal.toString());
				}
			}
		}
		return parnersParamMap;
	}
	
	@XmlAttribute(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@XmlTransient
	public Class<?> getClz() {
		return clz;
	}

	public void setClz(Class<?> clz) {
		this.clz = clz;
		String fieldName = null;
		Method fieldGetter = null;
		try {
			for(PropertyDescriptor propertyDescriptor : 
			    Introspector.getBeanInfo(clz).getPropertyDescriptors()){
				fieldGetter = propertyDescriptor.getReadMethod();
				if(fieldGetter == null){
					continue;
				}
				fieldName = propertyDescriptor.getName();
				targetClassGetters.put(fieldName, fieldGetter);
			}
		} catch (IntrospectionException e) {
			throw new RuntimeException("Invalid operation while setting the target class",e);
		}
	}
	
	@XmlElement(name="LocalAttribute")
	public List<LocalAttribute> getLocalAttributes() {
		return localAttributes;
	}

	public void setLocalAttributes(List<LocalAttribute> localAttributes) {
		this.localAttributes = localAttributes;
	}
	
	@XmlElement(name="ContextAttribute")
	public List<ContextAttribute> getContextAtributes() {
		return contextAtributes;
	}

	public void setContextAtributes(List<ContextAttribute> contextAtributes) {
		this.contextAtributes = contextAtributes;
	}


	@XmlAttribute(name="targetClass")
	public String getClsName() {
		return clsName;
	}



	public void setClsName(String clsName) {
		try{
			this.clz = Class.forName(clsName);
			this.setClz(clz);
		}catch(Exception e){
			throw new RuntimeException("No class with name :"+clsName+" is available");
		}
		this.clsName = clsName;
	
	}


	@XmlAttribute(name="idpHostUrl")
	public String getIdpHostUrl() {
		return idpHostUrl;
	}



	public void setIdpHostUrl(String idpHost) {
		this.idpHostUrl = idpHost;
	}


	@XmlAttribute(name="applicationId")
	public String getApplicationId() {
		return applicationId;
	}



	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}


	@XmlAttribute(name="tenantDomainSupported")
	public boolean isTenantDomainSupported() {
		return tenantDomainSupported;
	}



	public void setTenantDomainSupported(boolean tenantDomainSupported) {
		this.tenantDomainSupported = tenantDomainSupported;
	}


	@XmlAttribute(name="tenantDomain")
	public String getTenantDomain() {
		return tenantDomain;
	}



	public void setTenantDomain(String tenantDomain) {
		this.tenantDomain = tenantDomain;
	}


	@XmlAttribute(name="partnerTransPortHandler")
	public String getPartnerTransPortHandler() {
		return partnerTransPortHandler;
	}



	public void setPartnerTransPortHandler(String partnerTransPortHandler) {
		try{
			PartnerTransport transPort = (PartnerTransport) Class.forName(partnerTransPortHandler).newInstance();
			this.partnerTransport = transPort;
		}catch(Exception e){
			throw new RuntimeException("Error initializing the partner transport handler ",e);
		}
		
	}


	@XmlTransient
	public PartnerTransport getPartnerTransport() {
		return partnerTransport;
	}



	public void setPartnerTransport(PartnerTransport partnerTransport) {
		this.partnerTransport = partnerTransport;
	}
	
	
	
	
	
}
