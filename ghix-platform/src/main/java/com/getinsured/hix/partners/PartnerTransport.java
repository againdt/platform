package com.getinsured.hix.partners;

public interface PartnerTransport {
	
	public String getPartnerKey();
	public String encryptData(Object dataObj);

}
