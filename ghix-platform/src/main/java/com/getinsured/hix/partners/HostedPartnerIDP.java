package com.getinsured.hix.partners;

import java.util.HashMap;

import com.getinsured.hix.platform.util.GhixAESCipherPool;

public class HostedPartnerIDP implements PartnerTransport {

	@Override
	public String getPartnerKey() {
		return "ctx".intern();
	}

	@Override
	public String encryptData(Object obj) {
		//Its always going to be HashMap<String, String>
		@SuppressWarnings("unchecked")
		HashMap<String,String> parameters = (HashMap<String, String>) obj;
		return GhixAESCipherPool.encryptParameterMap(parameters);
	}

}
