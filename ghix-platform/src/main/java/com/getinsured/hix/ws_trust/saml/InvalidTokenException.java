package com.getinsured.hix.ws_trust.saml;

import com.getinsured.hix.platform.util.exception.GIException;

public class InvalidTokenException extends GIException {

	private static final long serialVersionUID = 1L;

	public InvalidTokenException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public InvalidTokenException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public InvalidTokenException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
