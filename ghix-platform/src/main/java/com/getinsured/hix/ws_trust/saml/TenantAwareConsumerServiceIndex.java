package com.getinsured.hix.ws_trust.saml;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.affiliate.repository.PlatformAffiliateFlowRepository;
import com.getinsured.affiliate.repository.PlatformAffiliateRepository;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.platform.repository.TenantSSOConfigurationRepository;
import com.getinsured.hix.platform.service.TenantService;


@Component
public class TenantAwareConsumerServiceIndex {
	private static Map<Long, Map<String, Integer>> tenantIndex = Collections.synchronizedMap(new HashMap<Long, Map<String, Integer>>());
	
	@Autowired
	private TenantService tenantService;
	@Autowired
	private PlatformAffiliateRepository affiliateRepo;
	@Autowired
	private TenantSSOConfigurationRepository tenantSSOConfigurationRepository;
	@Autowired
	private PlatformAffiliateFlowRepository affiliateFlowRepo;
	
	private static Logger logger  = LoggerFactory.getLogger(TenantAwareConsumerServiceIndex.class);
	
	@PostConstruct
	public void initIndexes(){
		List<TenantSSOConfiguration> tenantSSOConfigurations = tenantSSOConfigurationRepository.findAll();
		
		List<TenantSSOConfiguration> ssoEnabledTenantConfigurations = tenantSSOConfigurations.stream().filter(
				config -> config.getAuthMethod().equals(TenantSSOConfiguration.AuthenticationMethod.WEB_BASED_SSO))
				.collect(Collectors.toList());
		ssoEnabledTenantConfigurations.forEach(config -> {
			Map<String, Integer> serviceIndex = null;
			int index = 0;
			long tenantId = config.getTenantId();
			serviceIndex = tenantIndex.get(tenantId);// Default for the tenant
			if(serviceIndex == null){
				serviceIndex = Collections.synchronizedMap(new HashMap<String, Integer>());
				tenantIndex.put(tenantId, serviceIndex);
			}
			TenantDTO tenant = tenantService.getTenant(tenantId);
			serviceIndex.put(tenant.getUrl().trim(), index++);
			List<Affiliate> tenantAffiliates = this.affiliateRepo.getAffiliatesByTenantId(tenantId);
			for(Affiliate affiliate :tenantAffiliates){
				if(affiliate.getUrl() != null){
					serviceIndex.put(affiliate.getUrl().trim(), index++);
				}
				List<AffiliateFlow> flows = affiliateFlowRepo.findByAffiliateId(affiliate.getAffiliateId());
				for(AffiliateFlow flow: flows){
					if(flow.getUrl() != null){
						serviceIndex.put(flow.getUrl().trim(), index++);
					}
				}
			}
		});
	}
	
	public static Integer getAssertionConsumerIndexForUrl(Long tenantId, String url){
		if(logger.isInfoEnabled()){
			logger.info("Extracting ACS index for tenant Id:"+tenantId+" for URL:"+url);
		}

		Map<String, Integer> serviceIndex = tenantIndex.get(tenantId);
		if(serviceIndex != null && url != null){
			// for localhost there is no index
			Integer index = serviceIndex.get(url);

			if(index != null)
			{
				return index;
			}
		}

		return 0; // Default for the tenant
	}

}
