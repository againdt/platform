package com.getinsured.hix.ws_trust.saml;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class GhixModuleAccessToken {
	
	private GhixSecureToken token;
	private static final Logger logger = LoggerFactory.getLogger(GhixModuleAccessToken.class);

	public GhixModuleAccessToken(GhixSecureToken token){
		this.token = token;
	}
	
	public int getModuleAccessUser(GhixSecureToken token) throws InvalidTokenException{
		String ivStr = token.getBase64EncodedIv();
		if(ivStr == null){
			throw new InvalidTokenException("No IV found, invalid token");
		}
		logger.debug("IV Str received:"+ivStr);
		String tokenStr = token.getBase64EncodedToken();
		if(tokenStr == null){
			throw new InvalidTokenException("No token found");
		}
		logger.debug("Token Str received:"+tokenStr);
		
		byte[] iv = Base64.decodeBase64(ivStr);
		
		try{
			Cipher cipher = getDecryptionCipher(GhixPlatformConstants.TOKEN_VALIDATION_KEY, iv);
			return SAMLTokenValidator.validateModuleAccessToken(token, 3*60*1000, "ghix-web", cipher);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to get module user id wit error:"+e.getMessage());
		}
		
	}
	
	public int getModuleAccessUser(GhixSecureToken token, UserService userService) throws InvalidTokenException{
		String ivStr = token.getBase64EncodedIv();
		if(ivStr == null){
			throw new InvalidTokenException("No IV found, invalid token");
		}
		logger.debug("IV Str received:"+ivStr);
		String tokenStr = token.getBase64EncodedToken();
		if(tokenStr == null){
			throw new InvalidTokenException("No token found");
		}
		logger.debug("Token Str received:"+tokenStr);
		
		byte[] iv = Base64.decodeBase64(ivStr);
		
		try{
			Cipher cipher = getDecryptionCipher(GhixPlatformConstants.TOKEN_VALIDATION_KEY, iv);
			return SAMLTokenValidator.validateModuleAccessToken(token, 3*60*1000, "ghix-web", cipher, userService);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to get module user id wit error:"+e.getMessage());
		}
		
	}
	
	private static Cipher getEncryptionCipher(String password) throws Exception{
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		return cipher;
	}
	
	private static Cipher getDecryptionCipher(String password, byte[] iv) throws Exception{
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secret,new IvParameterSpec(iv));
		return cipher;
	}
	
	public static GhixModuleAccessToken getModuleAccessToken() {
		int clientId = -1;
		AccountUser temp = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null){
			Object principle = auth.getPrincipal();
			if(principle instanceof AccountUser ){
				temp = (AccountUser)principle;
				clientId = temp.getId();
			}
		}else{
			throw new GIRuntimeException("No logged in user available, can't generate batch access token");
		}
		if(clientId == -1){
			throw new GIRuntimeException("No logged in user [id=-1] available, can't generate batch access token");
		}
		GhixModuleAccessToken token = null;
		try {
			Cipher cipher = getEncryptionCipher(GhixPlatformConstants.TOKEN_VALIDATION_KEY);
			token = SAMLTokenValidator.generateModuleAccessToken(clientId, "ghix-web",cipher);
			
		} catch (Exception e) {
			throw new GIRuntimeException("Failed to generate the module access token:"+e.getMessage());
		}
		return token;
	}
	
	public String toString(){
		return "tokenIv="+this.token.getBase64EncodedIv()+"&tokenData="+this.token.getBase64EncodedToken();
	}
}
