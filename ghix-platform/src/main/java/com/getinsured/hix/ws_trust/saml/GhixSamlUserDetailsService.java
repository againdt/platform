package com.getinsured.hix.ws_trust.saml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.NameID;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.UserSession;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.config.FileBasedConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.repository.IRoleRepository;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRoleRepository;
import com.getinsured.hix.platform.security.repository.IUserSessionRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.security.tokens.GiBinaryToken;
import com.getinsured.hix.platform.security.tokens.SecureTokenFactory;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.platform.util.SSOIdentityProviderUtils;

@Service
public class GhixSamlUserDetailsService implements SAMLUserDetailsService {
	private static final Logger logger = LoggerFactory
			.getLogger(GhixSamlUserDetailsService.class);
	private static final String OEP_START_DATE_FORMAT = "MM/dd/yyyy";
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private IRoleRepository roleRepository;
	@Autowired
	private IUserRoleRepository userRoleRepo;
	@Autowired IUserSessionRepository sessionRepo;
	@Autowired SessionTrackerService sessionTracker;
	@Autowired private SCIMUserManager scimUserManager;
	private static  boolean tenantAware = "on".equalsIgnoreCase(System.getProperty("tenant.enabled"));
	private static boolean wso2Env = "true".equals(System.getProperty("wso2"));
	
	
	private boolean beforeOEP() throws ParseException {
		String date = FileBasedConfiguration.getConfiguration().getProperty("consumers.PreOEPRestrictionDate.MMddyyyy");
		if(date == null) {
			return false;
		}
		SimpleDateFormat sfd = new SimpleDateFormat(OEP_START_DATE_FORMAT);
		String oepStartDate = date;
		Date oepSdate = sfd.parse(oepStartDate);
		Date now = new Date();
		if(now.after(oepSdate)) {
			return false;
		}
		return true;
	}
	
	@Override
	public Object loadUserBySAML(SAMLCredential credential)
			throws UsernameNotFoundException {
		Assertion assertion = credential.getAuthenticationAssertion();
		NameID nameId = assertion.getSubject().getNameID();
		String relayState = credential.getRelayState();
		Map<String,String> loginContext = null;
		if(logger.isInfoEnabled()) {
			logger.info("AuthenticationAssertion:Subject:nameId:value :: {} with Relay State {} ",nameId.getValue(), relayState);
		}
		if(relayState != null && relayState.length() > 16) {
			GiBinaryToken token = null;
			try {
				token = SecureTokenFactory.processBinaryToken(relayState, GhixPlatformConstants.TOKEN_VALIDATION_KEY);
				loginContext = token.getAllParameters();
			} catch (Exception e) {
				logger.error("Error processing the token from relayState {}, assuming not a valid token",relayState,e);
			}
		}
		if(isTenantSameAsCurrent(credential)){
			List<AttributeStatement> attributeStatements = assertion
					.getAttributeStatements();
			Set<GrantedAuthority> authorities = null;
			try {
				authorities = this.extractAuthorities(attributeStatements);
			} catch (GIException e) {
				logger.error(e.getMessage(), e);
			}
			AuthnStatement authnStatement = assertion.getAuthnStatements().get(0);
			List<Attribute> x = credential.getAttributes();
			SAMLUser user = new SAMLUser(assertion.getSubject().getNameID()
					.getValue(), null, assertion.getIssuer().getValue(), null,
					assertion.getID(), null, assertion.getIssueInstant(),
					authnStatement.getAuthnInstant(), authorities, x);
			logger.info("Done creating SAML User");
			AccountUser guser = getGhixUser(user, nameId);
			if(loginContext != null) {
				guser.setLoginContext(loginContext);
			}
			return guser;
		}else{
			logger.error("Tenant code received is empty or does not match with current tenant");
			throw new UsernameNotFoundException("Tenant domain returned from WSO2 does not match with current tenant");
		}
	}
	
	private boolean isTenantSameAsCurrent(SAMLCredential credential){
		if(wso2Env && tenantAware){
			TenantDTO tenantDto = TenantContextHolder.getTenant();
			String code = credential.getAttributeAsString("http://wso2.org/claims/giTenantId".intern());	
			if(code == null){
				logger.error("No tenant code received, denying login");
			}
			logger.debug("Received Tenant code from logged in user:{}",code);
			return tenantDto.getCode().compareToIgnoreCase(code) == 0;
		}else{
			return true;
		}

	}
	
	private boolean isPrivilegedUser(AccountUser user){
		Set<UserRole> userRoles = user.getUserRole();
		boolean isPrivilegedRole = false;
		UserRole tmp = null;
		Iterator<UserRole> cursor = userRoles.iterator();
		while(cursor.hasNext()){
			tmp = cursor.next();
			if(tmp.isDefaultRole()){
				if(tmp.getRole().getPrivileged() == 1){
					isPrivilegedRole = true;
					break;
				}
			}
		}
		return isPrivilegedRole;
	}
	
	private void validateNewUserAcount(AccountUser user){
		boolean phixMultiTenantEnv = "true".equals(System.getProperty("wso2"));
		if(logger.isInfoEnabled()){
			logger.info("Validating logged in user for multi tenant SSO platform:"+phixMultiTenantEnv);
		}
		ArrayList<GiAuditParameter> paramsNotAvailable = new ArrayList<GiAuditParameter>();
		boolean valid = true;
		String username = user.getUsername();
		StringBuilder sb = new StringBuilder();
		if(StringUtils.isEmpty(username)){
			valid = false;
			sb.append("SAML User name not available,");
			GiAuditParameter param = new GiAuditParameter("SAML User name", "Not available");
			paramsNotAvailable.add(param);
		}

		if(username != null)
		{
			if (username.toLowerCase().endsWith("ghix.com"))
			{
				valid = false;
				sb.append(" ghix.com accounts are not allowed to login");
				GiAuditParameter param = new GiAuditParameter("Invalid account login attempt", username);
				paramsNotAvailable.add(param);
			}
		}
		// Security Question and answer are not a required attributes for PHIX
		if(StringUtils.isEmpty(user.getSecurityQuestion1()) && !phixMultiTenantEnv){
			valid = false;
			sb.append(" Sec question not available,");
			GiAuditParameter param = new GiAuditParameter("SAML Security Question", "Not available");
			paramsNotAvailable.add(param);
		}
		if(StringUtils.isEmpty(user.getSecurityAnswer1()) && !phixMultiTenantEnv){
			valid = false;
			sb.append(" Sec answer not available,");
			GiAuditParameter param = new GiAuditParameter("SAML Security answer", "Not available");
			paramsNotAvailable.add(param);
		}
		if(StringUtils.isEmpty(user.getEmail())){
			valid = false;
			sb.append(" Email not available");
			GiAuditParameter param = new GiAuditParameter("SAML User Email", "Not available");
			paramsNotAvailable.add(param);
		}
		if(StringUtils.isEmpty(user.getExtnAppUserId())){
			valid = false;
			sb.append(" SCIM id not available");
			GiAuditParameter param = new GiAuditParameter("SAML External App Id (SCIM ID)", "Not available");
			paramsNotAvailable.add(param);
		}
		if(!valid){
			logger.error("User attribute validation failed for user "+user.getUsername()+", not allowing the user to login "+sb.toString());
			GiAuditParameter param = new GiAuditParameter("IDALINK USER AUTO PROVISIONING", "BLOCKED");
			paramsNotAvailable.add(param);
			GiAuditParameterUtil.add(paramsNotAvailable);
			throw new UsernameNotFoundException("Invalid account state, please check the audit logs");
		}
	}
	
//	private void logDuplicateEmailAccount(AccountUser user){
//		ArrayList<GiAuditParameter> params = new ArrayList<GiAuditParameter>();
//		GiAuditParameter param1 = new GiAuditParameter("Action:", "Self Provisioning Denied");
//		GiAuditParameter param2 = new GiAuditParameter("Reason:", "Duplicate account found for the same email");
//		GiAuditParameter param3 = new GiAuditParameter("Email:", user.getEmail());
//		params.add(param1);
//		params.add(param2);
//		params.add(param3);
//		GiAuditParameterUtil.add(params);
//	}
	
	private void checkSessionMultiplicity(AccountUser dbUser){
		int id = dbUser.getId();
		if(logger.isInfoEnabled()) {
			logger.info("Checking session multiplicity for user:{}", id);
		}
		List<UserSession> existingSessions = this.sessionRepo.findActiveUserSession(id);
		List<UserSession> invalidatedSession = null;
		if(existingSessions != null && existingSessions.size() > 0){
			// Check if multiple Sessions are allowed
			if(!this.sessionTracker.checkSessionMultiplicityAllowed(this.isPrivilegedUser(dbUser))){
				invalidatedSession = this.sessionTracker.invalidateAllSessions(existingSessions, id);
				if(logger.isInfoEnabled()){
					logger.info("Invalidated existing session "+invalidatedSession.size());
				}
			}else {
				if(logger.isInfoEnabled()){
					logger.info("User with {} allowed to have multiple sessions");
				}
			}
		}
	}
	private void updateDBUserBySAML(AccountUser domainUser, AccountUser dbUser, boolean userProfileAttributesAvailable){
		List<UserRole> userRoles;
		ArrayList<GiAuditParameter> paramsChanged = new ArrayList<GiAuditParameter>();
		if(!userProfileAttributesAvailable){
			logger.debug("User profile data not received, profile merge not required");
			return;
		}
		if(userAttributesHaveChanged(domainUser, dbUser,paramsChanged)) {
			dbUser.setLastUpdatedBy(dbUser.getId());
			dbUser = this.userRepository.save(dbUser);
			GiAuditParameterUtil.add(paramsChanged);
		}
		userRoles = this.userRoleRepo.findByUser(dbUser);
		dbUser.setUserRole(new HashSet<UserRole>(userRoles));
	}
	
	private AccountUser getGhixUser(SAMLUser user, NameID nameId) {
		String idpId = nameId.getValue();
		AccountUser domainUser = null;
		AccountUser dbUser = null;
		boolean userProfileAttributesAvailable = true;
		boolean jitCreateEnabled = GhixPlatformConstants.PLAFORM_IDENTITY_JIT_CREATE;
		boolean jitSyncEnabled = GhixPlatformConstants.PLAFORM_IDENTITY_JIT_SYNC;
		String defaultJitProvisioningRole = GhixPlatformConstants.PLAFORM_IDENTITY_DEFAULT_ROLE;
		String userLookupKey = GhixPlatformConstants.PLATFORM_IEDNTITY_LOOKUP_FIELD;
		
		
		if(jitSyncEnabled) {
			//We want to extract the user attributes from the SAML
			if("true".equals(System.getProperty("wso2"))){
				domainUser = SSOIdentityProviderUtils.getAccountUserFromSAMLAttributes(user.getSamlAttributes());
			}else{
				domainUser = SAMLAttributeMap.getAccountUserFromSamlAttrubutes(user.getSamlAttributes());
			}
			validateNewUserAcount(domainUser);
		}
		switch(userLookupKey.trim()) {
			case "NameId":
				if(logger.isDebugEnabled()) {
					logger.debug("Looking up user by Name Id received:"+idpId);
				}
				dbUser = this.userRepository.findByExtnAppUserId(idpId);
				try {
					if(beforeOEP() && dbUser != null) {
						List<String> userRoleNames = this.userRoleRepo.findUserRoleNames(dbUser.getId());
						if(userRoleNames.contains("INDIVIDUAL") || userRoleNames.contains("EXTERNAL_ASSISTER")) {
							logger.error("Denying access to consumers pre-oep");
							dbUser = null;
						}
					}
				} catch (ParseException e) {
					logger.error("Error checking the OEP date",e);
					dbUser = null;
				}
				if(dbUser != null && logger.isDebugEnabled()) {
					logger.debug("Found user with external app id:"+dbUser.getExtnAppUserId());
				}
				break;
			case "ExternalId":
				if(domainUser == null) {
					throw new UsernameNotFoundException("No user information available from SAML Attributes received");
				}
				String externalAppId = domainUser.getExtnAppUserId();
				if(logger.isDebugEnabled()) {
					logger.debug("Looking up user by external app user id received:"+externalAppId);
				}
				dbUser = this.userRepository.findByExtnAppUserId(externalAppId);
				break;
			default:
				throw new UsernameNotFoundException("Unknown lookup key provided "+userLookupKey);
		}
		
		if(dbUser != null){
			// We found the local record
			checkSessionMultiplicity(dbUser);
			if(jitSyncEnabled) {
				// This will sync the user attributes and save the updated data, if any
				updateDBUserBySAML( domainUser,  dbUser, userProfileAttributesAvailable);
			}else {
				List<UserRole> userRoles = this.userRoleRepo.findByUser(dbUser);
				dbUser.setUserRole(new HashSet<UserRole>(userRoles));
			}
			domainUser = dbUser;
		}else{
			if(logger.isDebugEnabled()) {
				logger.debug("Failed to retrieve user locally, checking if JIT Enabled");
			}
			if(!jitCreateEnabled) {
				logger.error("User not available and JIT not enabled, can not proceed");
				ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
				if (sra!= null){
					sra.getRequest().getSession().invalidate();
				}
				// No local record available and we can not create one
				throw new AccessDeniedException("You do not have permission to access this site at this time");
			}
			// We did not find this user by external app id, lets make sure we do not have this user existing by email id
			AccountUser dupe = this.userRepository.findByEmail(domainUser.getEmail());
			if(dupe != null){
				if(logger.isInfoEnabled()) {
					logger.info("Found an existing user with same email, but different SCIMId:" + dupe.getEmail());
				}
				//Retrieve user account from wso2 for this scim id
				if(scimUserManager != null){
					AccountUser scimUser = scimUserManager.findByScimId(dupe.getExtnAppUserId());
					if(scimUser != null){
						logger.info("Updating user by SCIMId with user name :" + dupe.getUsername());
						updateDBUserBySAML( scimUser,  dupe, userProfileAttributesAvailable);
					}else{
						logger.error("User found in database and does not exist on WSO2 user name :" + dupe.getUsername() + " ScimId : " + dupe.getExtnAppUserId());
					}
				}else{
					logger.debug("scimUserManager not found");
				}
			}
			logger.info("Setting defaults for new SSO user with Role: {}",defaultJitProvisioningRole);
			this.setNewProfileDefaults(domainUser, defaultJitProvisioningRole);
		}
		if(logger.isInfoEnabled()) {
			logger.info("User {} has logged in", domainUser.getId() );
		}
		return domainUser;
	}

	private boolean userAttributesHaveChanged(AccountUser samlMappedAccountUser, AccountUser dbUser, ArrayList<GiAuditParameter> paramsChanged) {
		boolean isAttrChanged = false;
		String samlEmail = samlMappedAccountUser.getEmail();
		String dbEmail = dbUser.getEmail();
		String samlSecQuestion = samlMappedAccountUser.getSecurityQuestion1();
		String samlSecAnswer1 = samlMappedAccountUser.getSecurityAnswer1();
		String samlSecAnswer2 = samlMappedAccountUser.getSecurityAnswer2();
		String dbSecQuestion = dbUser.getSecurityQuestion1();
		String dbSecAnswer1 = dbUser.getSecurityAnswer1();
		String dbSecAnswer2 = dbUser.getSecurityAnswer2();
		String firstName = samlMappedAccountUser.getFirstName();
		String lastName = samlMappedAccountUser.getLastName();
		
		Date passwordLastUpdatedTimeStamp = samlMappedAccountUser.getPasswordLastUpdatedTimeStamp();
		
		//Check if Email changed
		if (StringUtils.isNotBlank(samlEmail) && StringUtils.isNotBlank(dbEmail) && !samlEmail.equalsIgnoreCase(dbEmail)) {
			dbUser.setUserName(samlEmail.toLowerCase());
			dbUser.setEmail(samlEmail.toLowerCase());
			paramsChanged.add(new GiAuditParameter("Email",samlEmail));
			isAttrChanged = true;
		}
		//Check for last and first name changes
		if(firstName != null && !firstName.equalsIgnoreCase(dbUser.getFirstName())){
			dbUser.setFirstName(firstName);
			paramsChanged.add(new GiAuditParameter("First Name:",firstName));
			isAttrChanged = true;
		}
		if(lastName !=null && !lastName.equalsIgnoreCase(dbUser.getLastName())){
			dbUser.setLastName(lastName);
			paramsChanged.add(new GiAuditParameter("Last Name:",lastName));
			isAttrChanged = true;
		}
		//Check if Security questions / answer changed
		if(samlSecQuestion != null && !samlSecQuestion.equalsIgnoreCase(dbSecQuestion)){
			dbUser.setSecurityQuestion1(samlSecQuestion);
			paramsChanged.add(new GiAuditParameter("Security Question:","<Masked>"));
			isAttrChanged = true;
		}
		if(samlSecAnswer1 != null && !samlSecAnswer1.equalsIgnoreCase(dbSecAnswer1)){
			dbUser.setSecurityAnswer1(samlSecAnswer1);
			isAttrChanged = true;
			paramsChanged.add(new GiAuditParameter("Security Answer1","<Masked>"));
		} 
		if(samlSecAnswer2 != null && !samlSecAnswer2.equalsIgnoreCase(dbSecAnswer2)){
			dbUser.setSecurityAnswer1(samlSecAnswer2);
			isAttrChanged = true;
			paramsChanged.add(new GiAuditParameter("Security Answer2","<Masked>"));
		} 
		if(passwordLastUpdatedTimeStamp != null){
			Date existingDate = dbUser.getPasswordLastUpdatedTimeStamp();
			if(passwordLastUpdatedTimeStamp.after(existingDate)){
				dbUser.setPasswordLastUpdatedTimeStamp(passwordLastUpdatedTimeStamp);
				isAttrChanged = true;
				paramsChanged.add(new GiAuditParameter("Password Time Stamp","<Masked>"));
			}
		}
		
		return isAttrChanged;
	}
	
	private void setNewProfileDefaults(AccountUser accountUser, String defUserRoleName) {
		String defPassword = GhixEncryptorUtil.generateUUIDHexString(12);
		accountUser.setUserName(accountUser.getUserName().toLowerCase());
		accountUser.setPassword(defPassword);
		accountUser.setHasGhixProvisioned(false);  // indicates that the user needs to provision in GHIX DB
		accountUser.setGhixProvisionRoleName(defUserRoleName); // new users default role
		Role defRole = this.roleRepository.findIdByName(defUserRoleName);
		if(defRole != null){
			accountUser.setDefRole(defRole);
		}else{
			throw new GIRuntimeException("Error while creating a provisioning a new user with role:"+defUserRoleName+" No such role found");
		}
		accountUser.setConfirmed(1); // enables the user account
		accountUser.setAuthenticated(true); //User has already been authenticated
	}
	
	
	// This takes care of taking away user had previously and are deleted from the Identity Manager
	/**
	 * 
	 * @param dbUser
	 * @param userSamlRolesFromDB these are the roles from DB may not be associated with Users
	 */
	@Transactional
	private Set<UserRole> updateDBUserForSAMLRoles(AccountUser dbUser, List<Role> userSamlRolesFromDB) {
		List<UserRole> existingDbuserRoles = this.userRoleRepo.findByUser(dbUser);
		/*if(existingDbuserRoles.size() == 0){
			logger.info("user does not have any role assigned, defaulting to \"INDIVIDUAL\" role");
			Role individualRole = this.roleRepository.findIdByName("INDIVIDUAL");
			if(individualRole != null){
				UserRole userRole = new UserRole();
				userRole.setUser(dbUser);
				userRole.setRole(individualRole);
				userRole.setRoleFlag('Y');
				this.userRoleRepo.saveAndFlush(userRole);
				existingDbuserRoles.add(userRole);
			}
		}*/
		HashSet<UserRole> returnRoleSet =  new HashSet<UserRole>();
		/*Set<Role> currentUserRoles = new HashSet<>();
		String existingRolename = null;
		UserRole existingUserRole = null;
		//Iterate thru all the existing user roles
		Iterator<UserRole> cursor = existingDbuserRoles.iterator();
		//List<UserRole> newDbRolesAdded = new ArrayList<>();
		while(cursor.hasNext()){
			existingUserRole = cursor.next();
			boolean userStillHasThisRole = false;
			existingRolename = existingUserRole.getRole().getName();
			logger.info("Evaluating existing user role "+existingRolename+ " From:"+userSamlRolesFromDB);
			// Check the SAML role from existing roles we retrieved from the database
			for(Role samlRole:userSamlRolesFromDB){
				if(samlRole.equals(existingUserRole.getRole())){
					logger.info("Role match found for role "+samlRole.getName());
					userStillHasThisRole = true;
					currentUserRoles.add(samlRole);
					break;
				}
			}
			if(!userStillHasThisRole){
				logger.info("User doesn't have "+existingRolename+" role anymore, removing it");
				//Delete this role from the current roles in our data structure and database
				cursor.remove();
				//this.userRoleRepo.delete(existingUserRole);
			}
		}
		// Now check for newly subscribed role
		for(Role role: userSamlRolesFromDB){
			logger.info("Checking for any new roles ["+role.getName()+"]");
			if(currentUserRoles.contains(role)){
				logger.info("user already has this role, continuing");
				continue;
			}else{
				logger.error("User has role "+role.getName()+" not provisioned with us, not allowing this user to login");
			}
		}*/
		returnRoleSet.addAll(existingDbuserRoles);
		return returnRoleSet;
	}

	/**
	 * This call should not return more than 1 user role as Identity manager is suppose to have one and only one YHI role for a user
	 * which will be the default role for the user. if there are more than one role make the first one as default role
	 * @param samlRoles
	 * @return
	 */
	@SuppressWarnings("unused")
	private List<Role> getLocalRepoForSAMLRoles(List<String> samlRoles){
		if(samlRoles == null || samlRoles.size() == 0){
			return new ArrayList<>();
		}
		int samlRoleReceived = samlRoles.size();
		int dbRoleExtracted = 0;
		logger.debug("Looking for ["+samlRoleReceived+"] local roles "+samlRoles);
		List<Role> dbList = this.roleRepository.findRolesByName(samlRoles);
		dbRoleExtracted = dbList.size();
		logger.debug("Found ["+dbRoleExtracted+"] roles"+dbList);
		if(samlRoleReceived > dbRoleExtracted){
			logger.warn("There appears to be roles created at IM and not yet provisioned in local database.. Ignoring for now");
		}
		return dbList;
	}
	
	private Set<GrantedAuthority> extractAuthorities(
			List<AttributeStatement> attributeStatements) throws GIException {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (AttributeStatement attributeStatement : attributeStatements) {
			for (Attribute attribute : attributeStatement.getAttributes()) {
				if (GrantedAuthority.class.getName().equalsIgnoreCase(
						attribute.getName())) {
					// logger.debug("found Granted Authorities.");

					for (XMLObject xmlObj : attribute.getAttributeValues()) {
						if (xmlObj instanceof XSString)
							authorities.add(new SimpleGrantedAuthority(
									((XSString) xmlObj).getValue()));
					}
				}
			}
		}
		return authorities;
	}

}
