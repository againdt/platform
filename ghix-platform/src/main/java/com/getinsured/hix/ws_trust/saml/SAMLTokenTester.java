package com.getinsured.hix.ws_trust.saml;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashSet;
import java.util.Vector;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.getinsured.timeshift.TimeShifterUtil;


public class SAMLTokenTester {
	static int tokens = 200;
	static String password = "ABN%56GT&23";

	private static class TokenGeneration implements Runnable {
		public static Vector<GhixSecureToken> tokens = new Vector<GhixSecureToken>();
		private String password;

		TokenGeneration(String pass) {
			this.password = pass;
		}

		@Override
		public void run() {
			try {
				long start = TimeShifterUtil.currentTimeMillis();
				GhixSecureToken token = SAMLTokenValidator.generateToken(
						this.password, "YHIUAT");
				tokens.add(token);
				System.out.println("Generate Token ["+Thread.currentThread().getId()+"],"+ (TimeShifterUtil.currentTimeMillis() - start));
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| InvalidKeySpecException | NoSuchPaddingException
					| IllegalBlockSizeException | BadPaddingException
					| UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private static class TokenValidation implements Runnable {
		private GhixSecureToken token = null;
		private HashSet<String> trustedParties = new HashSet<String>();
		private String password = null;

		TokenValidation(GhixSecureToken token, String password) {
			trustedParties.add("YHIUAT");
			trustedParties.add("IDALINK");
			trustedParties.add("YHISIT");
			this.password = password;
			this.token = token;
		}

		@Override
		public void run() {
			try {
				long start = TimeShifterUtil.currentTimeMillis();
				SAMLTokenValidator.validateToken(this.password, this.token,
						60 * 1000, this.trustedParties);
				System.out.println("Validate Tokes:["+Thread.currentThread().getId()+"],"+ (TimeShifterUtil.currentTimeMillis() - start));
			} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidTokenException | InvalidKeySpecException | NoSuchPaddingException | InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e){
				e.printStackTrace();
			}

		}
	}

	public static void main(String[] args) throws InterruptedException {
		
		if(args != null && args.length > 0){
			try{
				tokens = Integer.parseInt(args[0]);
			}catch(NumberFormatException ne){
				System.out.println("Bad token number provided, expecting a number from command line, using default 200");
				tokens = 500;
			}
		}
		System.out.println("Initializing testing for ["+tokens+"] concurrent tokens");
		Thread t = null;
		for (int i = 0; i < tokens; i++) {
			t = new Thread(new TokenGeneration(password));
			t.start();
			// t.join();
		}
		Thread.sleep(2000);
		System.out.println("Now decrypting all the tokens:"
				+ TokenGeneration.tokens.size());
		for (GhixSecureToken token : TokenGeneration.tokens) {
			t = new Thread(new TokenValidation(token, password));
			t.start();
			//t.join();
		}
		Thread.sleep(2000);
		System.out.println("Done decrypting all the tokens");
	}
}
