package com.getinsured.hix.ws_trust.saml;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.saml.context.SAMLContextProviderImpl;
import org.springframework.security.saml.context.SAMLMessageContext;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class GhixSamlContextProvider extends SAMLContextProviderImpl {
	private static final Logger logger = LoggerFactory.getLogger(GhixSamlContextProvider.class);
	private static String[] trustedPartiesArr = null;
	private static HashSet<String> trustedParties = new HashSet<String>();

	static
	{
		if(GhixPlatformConstants.TRUSTED_PARTIES != null)
		{
		trustedPartiesArr = GhixPlatformConstants.TRUSTED_PARTIES.split(",");
			for (String trustedParty : trustedPartiesArr)
			{
			trustedParties.add(trustedParty);
		}
	}
		else {
			logger.warn("No trusted parties configured for SAML Context [saml.trusted.parties] configuration property not found");
		}
	}
	
	public GhixSamlContextProvider(){
		super();
		logger.info("Initialized GhixContextProvider with trusted parties: {}", GhixPlatformConstants.TRUSTED_PARTIES);
	}
	
	public boolean validateToken(GhixSecureToken token, int validity){
		boolean valid = false;
		try{
			valid = SAMLTokenValidator.validateToken(GhixPlatformConstants.TOKEN_VALIDATION_KEY, token, validity, trustedParties);
		}catch(Exception e){
			logger.error("SAML Token validation from request URL failed with "+e.getMessage());
		}
		return valid;
	}
	
	public GhixSecureToken getToken(int validity){
		GhixSecureToken token = null;
		try{
			token = SAMLTokenValidator.generateToken(GhixPlatformConstants.TOKEN_VALIDATION_KEY,GhixPlatformConstants.SAML_ALIAS);
		}catch(Exception e){
			logger.error("SAML Token generation failed with message "+e.getMessage());
		}
		return token;
	}
	
	public void populateGenericContext(HttpServletRequest request, HttpServletResponse response, SAMLMessageContext context) throws MetadataProviderException {
		boolean thirdPartySamlTokenValid = false;
		GhixSecureToken  token =null;
		context.setRelayState(GhixPlatformConstants.FORCE_AUTHENTICATION); //Default, force the authentication
		String tokenIv = request.getParameter("tokenIv");
		String tokenData = request.getParameter("tokenData");
			
		if(logger.isDebugEnabled()) {
			logger.debug("SAML: TOKEN IV: "+tokenIv+" TOKEN DATA: "+tokenData);
		}	
		if(tokenIv != null && tokenData != null){
			logger.debug("Attempting to validate the received token");
			token = new GhixSecureToken(tokenIv, tokenData);
			//1 min for tokens coming from URL
			thirdPartySamlTokenValid = this.validateToken(token, GhixPlatformConstants.URL_TOKEN_VALIDITY);
			if(thirdPartySamlTokenValid){
				context.setRelayState(GhixPlatformConstants.ALLOW_PASSTHROUGH);// 1-> force the authentication, 0- allow WSO2 to decide
			}else{
				logger.error("Token received but validation failed, forcing authentication");
			}
		}
		super.populateGenericContext(request, response, context);
	}
}
