package com.getinsured.hix.ws_trust.saml;

import java.util.Formatter;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.saml.SAMLRelayStateSuccessHandler;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.service.TenantService;

@Component
public class TenantAwareLoginSuccessHandler extends SAMLRelayStateSuccessHandler {
	private static final int TENANT=0;
	private static final int AFFILIATE=1;
	private static final int FLOW=2;
	
	@Autowired(required=false)
	private TenantService tenantService;
	
	private Logger logger = LoggerFactory.getLogger(TenantAwareLoginSuccessHandler.class);
	
	private boolean enabled = false;
	
	@PostConstruct
	public void init(){
		String tmp = System.getProperty("tenant.enabled");
		if(tmp != null && tmp.equalsIgnoreCase("on".intern())){
			this.enabled = true;
		}
	}
	public String getTargetURL(String relayState) {
		if(!this.enabled){
			logger.error("Multi tenant mode is not available and TenantAwareSucessHandler is being used, please check".intern());
			throw new RuntimeException("Check if system is configured for multi tenancy");
		}
		if(logger.isInfoEnabled()){
			logger.info("Received relayState:".intern()+relayState);
		}
		String url = null;
		StringBuilder sb = new StringBuilder(256);
		Formatter fmt = new Formatter(sb);
		String urlFormat = "%s/account/user/loginSuccess";
		TenantDTO tenant = null;
		Affiliate aff = null;
		AffiliateFlow aflow = null;
		String[] params=relayState.split(",");
		int type = -1;
		String baseUrl = null;
		if(params.length > 0){
			String tmpId = null;
			for(int i = 0; i < params.length; i++){
				if(params[i].startsWith("fid#")){
					tmpId = params[i].substring(4);
					if(tmpId.equals("-1")){
						continue;
					}else{
						int id = Integer.parseInt(tmpId);
						aflow =this.tenantService.getAffiliateFlowById(id);
						baseUrl = aflow.getUrl();
						if(logger.isDebugEnabled()){
							logger.debug("Affiliate Flow is available but no URL set, Affiliate or Tenant URL will be used if available".intern());
						}
						if(baseUrl != null && type < FLOW){
							type=FLOW;
						}
					}
				}else if(params[i].startsWith("aid#")){
					tmpId = params[i].substring(4);
					if(tmpId.equals("-1")){
						continue;
					}else{
						long id = Long.parseLong(tmpId);
						aff = this.tenantService.getAffiliateById(id);
						baseUrl = aff.getUrl();
						if(logger.isDebugEnabled()){
							logger.debug("Affiliate is available but no URL set, Tenant URL will be used if available".intern());
						}
						if(baseUrl != null && type < AFFILIATE){
							type=AFFILIATE;
						}
					}
				}else if(params[i].startsWith("tid#")){
					tmpId = params[i].substring(4);
					if(tmpId.equals("-1")){
						// Should never happen
						continue;
					}else{
						type = TENANT;
						long id = Long.parseLong(tmpId);
						tenant= this.tenantService.getTenant(id);
					}
				}
			}
			switch(type){
				case FLOW:
					fmt.format(urlFormat,aflow.getUrl());
					break;
				case AFFILIATE:
					fmt.format(urlFormat,aff.getUrl());
					break;
				case TENANT:
					fmt.format(urlFormat,tenant.getUrl());
					break;
			}
			sb.trimToSize();
			url = sb.toString();
	        fmt.close();
		}
		if(logger.isInfoEnabled()){
			logger.info("Received relaayState pointing to type:"+type+ "For URL:"+url);
		}
        return url;
    }
}
