package com.getinsured.hix.ws_trust.saml;

import java.io.Serializable;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.joda.time.DateTime;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.impl.XSAnyImpl;
import org.opensaml.xml.schema.impl.XSStringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class SAMLUser implements Principal, Serializable , UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5183053526600498165L;
	
	private static final Logger logger = LoggerFactory.getLogger(SAMLUser.class);
	private final String name;
	private final String authenticationResponseIssuingEntityName;
	private final String authenticationAssertionIssuingEntityName;
	private final String authenticationResponseID;
	private final String authenticationAssertionID;
	private final DateTime authenticationResponseIssueInstant;
	private final DateTime authenticationAssertionIssueInstant;
	private final DateTime authenticationIssueInstant;
	private final HashMap<String, String> userAttributesMap;

	private Set<GrantedAuthority> authorities;

	public SAMLUser(String name,
			String authenticationResponseIssuingEntityName,
			String authenticationAssertionIssuingEntityName,
			String authenticationResponseID, String authenticationAssertionID,
			DateTime authenticationResponseIssueInstant,
			DateTime authenticationAssertionIssueInstant,
			DateTime authenticationIssueInstant,
			Collection<? extends GrantedAuthority> authorities,
			List<Attribute> userAttributes) {
		super();
		StringBuilder builder = new StringBuilder();
		this.name = name;
		this.authenticationResponseIssuingEntityName = authenticationResponseIssuingEntityName;
		this.authenticationAssertionIssuingEntityName = authenticationAssertionIssuingEntityName;
		this.authenticationResponseID = authenticationResponseID;
		this.authenticationAssertionID = authenticationAssertionID;
		this.authenticationResponseIssueInstant = authenticationResponseIssueInstant;
		this.authenticationAssertionIssueInstant = authenticationAssertionIssueInstant;
		this.authenticationIssueInstant = authenticationIssueInstant;
		this.authorities = Collections
				.unmodifiableSet(sortAuthorities(authorities));
		if(userAttributes.size() > 0){
			this.userAttributesMap = new HashMap<>(userAttributes.size());
			for(Attribute attr:userAttributes){
				 String attrName = attr.getName();
				 List<XMLObject> vals = attr.getAttributeValues();
				 for(XMLObject val: vals){
					 if(val instanceof XSAnyImpl) {
						 builder.append(((XSAnyImpl)val).getTextContent()+",");
					 }else if(val instanceof XSStringImpl) {
						 builder.append(((XSStringImpl)val).getValue()+",");
					 }
					
				 }
				 int lidx = builder.lastIndexOf(",");
				 this.userAttributesMap.put(attrName, builder.substring(0, lidx)); 
				 builder.setLength(0);
			 }
		}else{
			this.userAttributesMap =new HashMap<>();
		}
	}
	
	public String getAssertion(String claimId){
		return this.userAttributesMap.get(claimId);
	}
	
	public String getTenantCode(){
		return this.getAssertion("http://wso2.org/claims/giTenantId".intern());
	}
	
	public String getAuthenticationResponseIssuingEntityName() {
		return authenticationResponseIssuingEntityName;
	}

	public String getAuthenticationAssertionIssuingEntityName() {
		return authenticationAssertionIssuingEntityName;
	}

	public DateTime getAuthenticationResponseIssueInstant() {
		return authenticationResponseIssueInstant;
	}

	public DateTime getAuthenticationAssertionIssueInstant() {
		return authenticationAssertionIssueInstant;
	}

	public DateTime getAuthenticationIssueInstant() {
		return authenticationIssueInstant;
	}

	public String getAuthenticationResponseID() {
		return authenticationResponseID;
	}

	public String getAuthenticationAssertionID() {
		return authenticationAssertionID;
	}

	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getName() {
		return name;
	}
	
	public boolean setSAMLAttribute(String key, String val){
		
		return false;
	}

	/**
	 * Returns true if this object's name is equal to the name of the passed in
	 * arg.
	 * 
	 * 
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}

		if (obj.getClass() != getClass()) {
			return false;
		}
		SAMLUser rhs = (SAMLUser) obj;
		return new EqualsBuilder().append(name, rhs.name).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(517, 43).append(name).toHashCode();

	}

	@Override
	public String toString() {

		 ToStringBuilder x = new ToStringBuilder(this)
				.append("name", name)
				.append("\nauthenticationResponseIssuingEntityName",
						authenticationResponseIssuingEntityName)
				.append("\nauthenticationAssertionIssuingEntityName",
						authenticationAssertionIssuingEntityName)
				.append("\nauthenticationResponseID", authenticationResponseID)
				.append("\nauthenticationAssertionID", authenticationAssertionID)
				.append("\nauthenticationResponseIssueInstant",
						authenticationResponseIssueInstant)
				.append("\nauthenticationAssertionIssueInstant",
						authenticationAssertionIssueInstant)
				.append("\nauthenticationIssueInstant",
						authenticationIssueInstant)
				.append("\nauthorities", authorities);
		 if(this.userAttributesMap != null){
			 Iterator<Entry<String, String>> cursor = this.userAttributesMap.entrySet().iterator();
			 while(cursor.hasNext()){
				 Entry<String, String> entry = cursor.next();
				 x.append(entry.getKey()+":"+entry.getValue());
			 }
		 }
		 return x.toString();
	}
	
	public String getAttribute(String attrName){
		if(this.userAttributesMap == null){
			logger.warn("No attribute available for user "+this.getName());
			return null;
		}
		return this.userAttributesMap.get(attrName);
	}
	
	public HashMap<String, String> getSamlAttributes(){
		return this.userAttributesMap;
	}

	// Taken From Spring Security's User impl
	private static SortedSet<GrantedAuthority> sortAuthorities(
			Collection<? extends GrantedAuthority> authorities) {
		SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<GrantedAuthority>(
				new AuthorityComparator());

		for (GrantedAuthority grantedAuthority : authorities) {
			sortedAuthorities.add(grantedAuthority);
		}

		return sortedAuthorities;
	}

	// Taken From Spring Security's User impl
	private static class AuthorityComparator implements
			Comparator<GrantedAuthority>, Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2490388631339334388L;

		public int compare(GrantedAuthority g1, GrantedAuthority g2) {

			if (g2.getAuthority() == null) {
				return -1;
			}

			if (g1.getAuthority() == null) {
				return 1;
			}

			return g1.getAuthority().compareTo(g2.getAuthority());
		}
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

}
