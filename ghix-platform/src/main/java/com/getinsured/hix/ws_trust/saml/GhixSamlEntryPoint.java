package com.getinsured.hix.ws_trust.saml;


import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.context.SAMLMessageContext;
import org.springframework.security.saml.websso.WebSSOProfileOptions;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

public final class GhixSamlEntryPoint extends SAMLEntryPoint {

	private static final Logger logger = LoggerFactory
			.getLogger(GhixSamlEntryPoint.class);

	protected WebSSOProfileOptions getProfileOptions(
			SAMLMessageContext context, AuthenticationException exception)
			throws MetadataProviderException {
		String lang = "en";
		String hostName = null;
		Long tenantId = -1L;
		String relayState = context.getRelayState();
		WebSSOProfileOptions ssoProfileOptions;
		if (defaultOptions != null) {
			ssoProfileOptions = defaultOptions.clone();
		} else {
			ssoProfileOptions = new WebSSOProfileOptions();
		}
		if (relayState == null || relayState.equalsIgnoreCase(GhixPlatformConstants.FORCE_AUTHENTICATION)) {
			logger.info("No relay state found, forcing authentication");
			ssoProfileOptions.setForceAuthN(true);
		} else {
			ssoProfileOptions.setForceAuthN(false);
		}
		//PHIX
		String tmp = System.getProperty("tenant.enabled");
		if(tmp != null && tmp.equalsIgnoreCase("on")){
			hostName = TenantUtil.getHostnameFromUrl(TenantContextHolder.getRequesturl());
			Integer flowId = TenantContextHolder.getFlowId();
			if(flowId == null){
				flowId = -1;
			}
			Long affiliateId = TenantContextHolder.getAffiliateId();
			if(affiliateId == null){
				affiliateId = -1L;
			}
			TenantDTO tenant = TenantContextHolder.getTenant();
			if(tenant != null){
				tenantId = tenant.getId();
				String code = tenant.getCode();
				relayState = "tid#"+tenantId+",tcd#"+code+",fid#"+flowId +",aid#"+ affiliateId +",lang#"+lang;
			}
			logger.info("Setting relayState:"+relayState);
			context.setRelayState(relayState);
		}
		ssoProfileOptions.setAssertionConsumerIndex(TenantAwareConsumerServiceIndex.getAssertionConsumerIndexForUrl(tenantId, hostName));
		return ssoProfileOptions;

	}
}
