package com.getinsured.hix.ws_trust.saml;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * This class represents the mapping between the identity server attributes and
 * local service provider roles
 *
 * @author chaudhary_a
 *
 */
public class SAMLAttributeMap {
	private static Logger logger = LoggerFactory
			.getLogger(SAMLAttributeMap.class);
	private static HashMap<String, AttributeDefinition> attrMap = new HashMap<>();
	private static HashMap<String,Method> accountUserFieldMap = null;
	static {
		InputStream inputMap = null;
		
		Document doc = null;
		DocumentBuilderFactory factory = null;
		try {

			if("true".equals(System.getProperty("wso2")))
			{
				inputMap = SAMLAttributeMap.class.getClassLoader().getResourceAsStream("saml/SAMLAttributeMap_SSO.xml");
			}
			else
			{
				inputMap = SAMLAttributeMap.class.getClassLoader().getResourceAsStream("saml/SAMLAttributeMap.xml");
			}

			factory = GhixUtils.getDocumentBuilderFactoryInstance();
			doc = factory.newDocumentBuilder().parse(inputMap);
			buildMap(doc);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			logger.error("Failed to load the attribute map", e);
		}finally{
			IOUtils.closeQuietly(inputMap);
		}
		Field[] fields = AccountUser.class.getDeclaredFields();
		String fieldName = null;
		Method fieldSetter = null;
		if(fields.length > 0){
			accountUserFieldMap = new HashMap<>();
			for(Field field: fields){
				fieldName = field.getName();
				try {
					fieldSetter = AccountUser.class.getDeclaredMethod("set"+Character.toUpperCase(fieldName.charAt(0))+fieldName.substring(1), String.class);
				} catch (NoSuchMethodException | SecurityException e) {
					logger.warn("No field setter found for field:"+fieldName);
					continue;
				}
				accountUserFieldMap.put(field.getName(), fieldSetter);
			}
		}

	}

	private static void buildMap(Document doc) throws IllegalArgumentException {
		Node accountUserNode = doc.getDocumentElement();
		String rootName = accountUserNode.getNodeName();
		if (!rootName.equalsIgnoreCase("AccountUser")) {
			throw new IllegalArgumentException(
					"Invalid attribute map provided, Expected \"AccountUser\" as root element found \""
							+ rootName + "\"");
		}
		NodeList fieldNodeList = null;
		Node tmpFieldNode = null;
		Node attrNode = null;
		AttributeDefinition def = null;
		boolean multiValue = false;
		String uri = null;
		String delimChar = ",";
		String fieldName = null;
		int len = 0;

		fieldNodeList = accountUserNode.getChildNodes();
		len = fieldNodeList.getLength();
		for (int i = 0; i < len; i++) {
			tmpFieldNode = fieldNodeList.item(i);
			if (tmpFieldNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			fieldName = tmpFieldNode.getNodeName();
			NamedNodeMap attributeNodes = tmpFieldNode.getAttributes();
			attrNode = attributeNodes.getNamedItem("uri");
			if (attrNode == null) {
				logger.warn("No URI mapping found for " + fieldName);
				continue;
			}
			uri = attrNode.getNodeValue();
			attrNode = attributeNodes.getNamedItem("multiValue");
			if (attrNode != null) {
				multiValue = attrNode.getNodeValue().equalsIgnoreCase("true");
			}else{
				multiValue=false;
			}
			attrNode = attributeNodes.getNamedItem("delimChar");
			if (attrNode != null) {
				delimChar = attrNode.getNodeValue();
			}
			def = new AttributeDefinition(uri, multiValue, delimChar);
			attrMap.put(fieldName, def);
		}
	}

	public static AccountUser getAccountUserFromSamlAttrubutes(HashMap<String,String> userSamlAttributes){
		AccountUser user = new AccountUser();
		if(userSamlAttributes == null || userSamlAttributes.size() ==0){
			return user;
		}
		Set<Entry<String,Method>> mapSet = accountUserFieldMap.entrySet();
		Iterator<Entry<String, Method>> userFieldsCursor = mapSet.iterator();
		Entry<String, Method> entry = null;
		String fieldName = null;
		String fieldValue = null;
		Method userFieldSetter = null;
		String fieldUri = null;
		AttributeDefinition fieldDef = null;
		while(userFieldsCursor.hasNext()){
			 entry = userFieldsCursor.next();
			 fieldName = entry.getKey();
			 userFieldSetter = entry.getValue();
			 fieldDef = attrMap.get(fieldName);
			 if(fieldDef == null){
				 if(logger.isDebugEnabled()){
					 logger.debug("No field mapping available for -> "+fieldName);
				 }
				 continue;
			 }
			 fieldUri = fieldDef.getUri();
			 fieldValue = userSamlAttributes.get(fieldUri);
			 if(logger.isDebugEnabled()){
				 logger.debug("Received Claim with URI:"+fieldUri+" With value:"+fieldValue);
			 }
			 if(fieldValue != null){
				 if(userFieldSetter == null){
					 logger.warn("No user field with the name "+fieldName+" available, check user mapping");
					 continue;
				 }
				 if(logger.isDebugEnabled()){
					 logger.debug("Setting field:"+fieldName+" with value:"+fieldValue);
				 }
				 try {
					userFieldSetter.invoke(user, fieldValue);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					logger.error("Failed to set the field value for field "+fieldName,e);
				}

			}else{
				logger.warn("No value received Claim with URI:"+fieldUri);
			}
		}
		return user;
	}

	public static String[] getUserAttribute(String attrName, SAMLUser user) {
		if (attrMap == null) {
			logger.warn("Local attribute map not initialized, returning empty role list");
			return null;
		}
		AttributeDefinition def = attrMap.get(attrName.trim());
		if (def == null) {
			logger.warn("No mapping found for " + attrName + " Available map:"
					+ attrMap);
			return new String[]{};
		}
		String attrUri = def.getUri();
		String value = user.getAttribute(attrUri);
		if(value == null){
			logger.warn("No value found for "+attrName);
			return new String[]{};
		}
		logger.debug("Checking if [" + attrName + "] is Multivalue");
		if (def.isMultivalue()) {
			logger.debug("Multi value attribute [" + attrName + "]:" + value);
			return value.split(def.getDelimChar());
		}
		logger.debug("Simple value attribute [" + attrName + "]:" + value);
		return new String[] { value };
	}

	private static class AttributeDefinition {
		private String uri;
		private boolean multivalue;
		private String delimChar = ",";

		AttributeDefinition(String uri, boolean multiValue, String delimChar) {
			this.uri = uri;
			this.multivalue = multiValue;
			this.delimChar = delimChar;
		}

		String getUri() {
			return uri;
		}

		boolean isMultivalue() {
			return multivalue;
		}

		String getDelimChar() {
			return this.delimChar;
		}

		public String toString() {
			return "[URI:" + this.uri + "],[multiValue:" + this.multivalue
					+ "],[delimChar:" + this.delimChar + "]";
		}
	}

}
