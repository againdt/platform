package com.getinsured.hix.ws_trust.saml;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.affiliate.repository.PlatformAffiliateFlowRepository;
import com.getinsured.affiliate.repository.PlatformAffiliateRepository;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.platform.repository.TenantSSOConfigurationRepository;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

@Component
public class TenantSpMetadata{
	private static Map<Long, String> providers = Collections.synchronizedMap(new HashMap<Long, String>());
	private Logger logger = LoggerFactory.getLogger(TenantSpMetadata.class);
	@Autowired
	private TenantSSOConfigurationRepository tenantSSOConfigurationRepository;
	@Autowired
	private TenantService tenantService;
	@Autowired
	private PlatformAffiliateRepository affiliateRepo;
	@Autowired
	private PlatformAffiliateFlowRepository affiliateFlowRepo;
	
	private VelocityEngine velocityEngine;
	private static HashMap<Long, String> tenantCodes = new HashMap<>(5);
	
	private Template metadataTemplate = null;
	private Template acsTemplate = null;
	

	public TenantSpMetadata() {
	}
	
	private String getVelocityLogFile() {
		String logFileOrLocation = GhixPlatformConstants.VELOCITY_LOG;
		String absolutePath = null;
		if(logFileOrLocation == null) {
			logger.warn("No velocity log file location provided in configuration file, using default");
			return absolutePath;
		}
		File f = new File(logFileOrLocation);
		if(!f.exists() || !f.canWrite()) {
			logger.warn("Velocity log file location provided {} does not exist or not writable, will use default",f.getAbsolutePath());
			return absolutePath;
		}
		if(f.isDirectory()) {
			File velocityLog = new File (f,"velocity.log");
			absolutePath = velocityLog.getAbsolutePath();
		}else if(f.isFile()) {
			absolutePath = f.getAbsolutePath();
		}
		if(logger.isInfoEnabled()) {
			logger.info("Using velocity log file: {}",absolutePath);
		}
		return absolutePath;
	}
	
	@PostConstruct
	public synchronized void initMetadataTemplates() throws MetadataProviderException {
		if(!GhixPlatformConstants.PLATFORM_NODE_PROFILE.equalsIgnoreCase("web") || "false".equalsIgnoreCase(System.getProperty("wso2"))) {
			return;
		}
		velocityEngine = new VelocityEngine();
		String fileLoc = getVelocityLogFile();
		if(fileLoc != null) {
			velocityEngine.setProperty("runtime.log", fileLoc);
		}
		if(GhixPlatformConstants.EXTERNAL_TEMPLATE_ENABLED) {
			velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "file"); 
			velocityEngine.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, GhixPlatformConstants.EXTERNAL_TEMPLATE_LOCATION);
		}else{
			velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
			velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		}

	    try {
			velocityEngine.init();

			// If external template is enabled, we are going to receive full diretory path where
			// templates are going to be:
			if(GhixPlatformConstants.EXTERNAL_TEMPLATE_ENABLED)
			{
				metadataTemplate = velocityEngine.getTemplate("metadata.vm");
				acsTemplate = velocityEngine.getTemplate("acs.vm");
			}
			// Otherwise we will use our classpath + relative directory path + file name.
			else
			{
				metadataTemplate = velocityEngine.getTemplate("saml/metadata.vm");

				// If we are in localhost development + sso, we need different acs.vm template to take into account
				// non-https, 8080 port and /hix path.
				if("true".equals(System.getProperty("localdev")))
				{
					acsTemplate = velocityEngine.getTemplate("saml/acs-localhost.vm");
				}
				else
				{
					acsTemplate = velocityEngine.getTemplate("saml/acs.vm");
				}
			}
		} catch (ResourceNotFoundException | ParseErrorException e) {
			logger.error("Failed to initialize metadata template engine", e);
		}

		if(tenantSSOConfigurationRepository == null){
			throw new MetadataProviderException("Autowiring of SSO configuration repo failed");
		}
		List<TenantSSOConfiguration> tenantSSOConfigurations = tenantSSOConfigurationRepository.findAll();
		if(tenantSSOConfigurations == null || tenantSSOConfigurations.size() == 0){
			logger.info("None of the tenants are configured for SSO");
			return;
		}
		List<TenantSSOConfiguration> ssoEnabledTenantConfigurations = tenantSSOConfigurations.stream().filter(
				config -> config.getAuthMethod().equals(TenantSSOConfiguration.AuthenticationMethod.WEB_BASED_SSO))
				.collect(Collectors.toList());
		for(TenantSSOConfiguration config: ssoEnabledTenantConfigurations){
			try{
				CharArrayWriter acsWriter = new CharArrayWriter(4096);
				CharArrayWriter metadataWriter = new CharArrayWriter(4096);
				long tenantId = config.getTenantId();
				
				
				TenantDTO tenant = tenantService.getTenant(tenantId);
				String entityId = tenant.getCode();
				tenantCodes.put(tenantId, entityId);
				String tenantBaseUrl = tenant.getUrl();
				String baseUrl = tenantBaseUrl;
				addAcsForUrl(tenantBaseUrl, acsWriter, tenantId, true, entityId);
				List<Affiliate> tenantAffiliates = this.affiliateRepo.getAffiliatesByTenantId(tenantId);
				for(Affiliate affiliate :tenantAffiliates){
					baseUrl = affiliate.getUrl();
					if(baseUrl != null){
						addAcsForUrl(baseUrl, acsWriter, tenantId, false, entityId);
					}
					List<AffiliateFlow> flows = affiliateFlowRepo.findByAffiliateId(affiliate.getAffiliateId());
					for(AffiliateFlow flow: flows){
						baseUrl = flow.getUrl();
						if(baseUrl != null){
							addAcsForUrl(baseUrl, acsWriter, tenantId, false, entityId);
						}
					}
					
				}
				providers.put(tenantId, this.createMetadata(tenantBaseUrl, metadataWriter, acsWriter, entityId));
				logger.info("Loaded Metadata for tenant: {} with code {}", tenantId, entityId);
			}catch(Exception e){
				throw new MetadataProviderException("Faied to load the metadata",e);
			}
		}
	}
	
	public String getTenantSPMetadata(long tenantId){
		return providers.get(tenantId);
	}
	
	private void addAcsForUrl(String url, CharArrayWriter writer, long tenantId, boolean isDefault, String entityId)
			throws ResourceNotFoundException, 
			ParseErrorException, 
			MethodInvocationException, 
			IOException {
		VelocityContext vc = new VelocityContext();
		Integer index = TenantAwareConsumerServiceIndex.getAssertionConsumerIndexForUrl(tenantId, url);
		if(logger.isDebugEnabled()){
			logger.debug("Received [{}] Index: {} for URL: {}", tenantId, index, url);
		}
		vc.put("baseUrl".intern(), url.trim());
		vc.put("index".intern(), index);
		vc.put("default".intern(), Boolean.toString(isDefault));
		vc.put("alias".intern(), entityId.trim());
		this.acsTemplate.merge(vc, writer);
	}
	
	public String getTenantCode(long tenantId){
		return tenantCodes.get(tenantId);
	}

	

	private String createMetadata(String tenantBaseUrl, CharArrayWriter writer, CharArrayWriter acsWriter, String entityId) throws ResourceNotFoundException, ParseErrorException, MethodInvocationException, IOException {
		VelocityContext vc = new VelocityContext();
		vc.put("tenantBaseUrl".intern(), tenantBaseUrl);
		vc.put("entityId", entityId);
		String mdAcs = acsWriter.toString();
		vc.put("mdAcs".intern(), mdAcs);
		vc.put("alias".intern(), entityId.trim());
		this.metadataTemplate.merge(vc, writer);
		String spXml = writer.toString();
		if(logger.isDebugEnabled()){
			logger.debug(spXml);
		}
		return spXml;
	}
	
	/**
	 * This will override the existing configuration if any
	 * @param config
	 */
	public boolean addTenantConfiguration(TenantSSOConfiguration config){
		CharArrayWriter acsWriter = new CharArrayWriter(4096);
		CharArrayWriter metadataWriter = new CharArrayWriter(4096);
		try{
			long tenantId = config.getTenantId();
			
			logger.info("Metadata for tenant: {}", tenantId);
			TenantDTO tenant = tenantService.getTenant(tenantId);
			String entityId = tenant.getCode();
			String tenantBaseUrl = tenant.getUrl();
			String baseUrl = tenantBaseUrl;
			addAcsForUrl(tenantBaseUrl, acsWriter, tenantId, true, entityId);
			List<Affiliate> tenantAffiliates = this.affiliateRepo.getAffiliatesByTenantId(tenantId);
			for(Affiliate affiliate :tenantAffiliates){
				baseUrl = affiliate.getUrl();
				if(baseUrl != null){
					addAcsForUrl(baseUrl, acsWriter, tenantId, false, entityId);
				}
				List<AffiliateFlow> flows = affiliateFlowRepo.findByAffiliateId(affiliate.getAffiliateId());
				for(AffiliateFlow flow: flows){
					baseUrl = flow.getUrl();
					if(baseUrl != null){
						addAcsForUrl(baseUrl, acsWriter, tenantId, false, entityId);
					}
				}
				
			}
			providers.put(tenantId, this.createMetadata(tenantBaseUrl, metadataWriter, acsWriter, entityId));
			return true;
		}catch(Exception e){
			logger.error("Faied to load the metadata for tenant "+config.getTenantId(),e);
			return false;
		}finally{
			metadataWriter.close();
			acsWriter.close();
		}
	}

	public boolean refreshTenantConfiguration(long tenantId){
		if(!GhixPlatformConstants.PLATFORM_NODE_PROFILE.equalsIgnoreCase("web")){
			return true;
		}
		CharArrayWriter acsWriter = new CharArrayWriter(4096);
		CharArrayWriter metadataWriter = new CharArrayWriter(4096);
		try{
			logger.info("Metadata for tenant: {}", tenantId);
			TenantDTO tenant = tenantService.getTenant(tenantId);
			String entityId = tenant.getCode();
			String tenantBaseUrl = tenant.getUrl();
			String baseUrl = tenantBaseUrl;
			addAcsForUrl(tenantBaseUrl, acsWriter, tenantId, true, entityId);
			List<Affiliate> tenantAffiliates = this.affiliateRepo.getAffiliatesByTenantId(tenantId);
			for(Affiliate affiliate :tenantAffiliates){
				baseUrl = affiliate.getUrl();
				if(baseUrl != null){
					addAcsForUrl(baseUrl, acsWriter, tenantId, false, entityId);
				}
				List<AffiliateFlow> flows = affiliateFlowRepo.findByAffiliateId(affiliate.getAffiliateId());
				for(AffiliateFlow flow: flows){
					baseUrl = flow.getUrl();
					if(baseUrl != null){
						addAcsForUrl(baseUrl, acsWriter, tenantId, false, entityId);
					}
				}
				
			}
			providers.put(tenantId, this.createMetadata(tenantBaseUrl, metadataWriter, acsWriter, entityId));
			logger.info("Tenant Id {} metadata cache refreshed", tenantId);
			return true;
		}catch(Exception e){
			logger.error("Faied to refresh the metadata for tenant "+tenantId,e);
			return false;
		}finally{
			metadataWriter.close();
			acsWriter.close();
		}
	}

	
	
}
