package com.getinsured.hix.platform.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Esignature;
import com.getinsured.hix.platform.repository.IEsignatureRepository;
import com.getinsured.hix.platform.service.EsignatureService;

@Service("EsignatureService")
@Repository
@Transactional
public class EsignatureServiceImpl implements EsignatureService{
	@Autowired private IEsignatureRepository esignatureRepository;	
	
	@Override
	public List<Esignature> findByAccountUser(AccountUser givenUserObj) {
		return esignatureRepository.findByUser(givenUserObj);
	}
	
	@Override
	public Esignature findByModuleRefId(String moduleName, Integer refId) {
		return esignatureRepository.findByModuleRefId(moduleName, refId);
	}
	
	@Override	
	public Esignature saveEsignature(Esignature givenEsignatureObj) {
		return esignatureRepository.save(givenEsignatureObj);
	}
	
}