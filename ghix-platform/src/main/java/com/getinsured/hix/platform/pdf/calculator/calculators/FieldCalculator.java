package com.getinsured.hix.platform.pdf.calculator.calculators;

import com.fasterxml.jackson.databind.JsonNode;
import com.getinsured.hix.platform.util.exception.ApplicationDataException;
import com.getinsured.hix.platform.util.exception.ConfigurationException;
import com.getinsured.hix.platform.util.exception.FieldCalculatorNotFoundException;

/**
 * Created by ivanbastidas on 4/24/14.
 */
public interface FieldCalculator {
    String execute(String applicationJson, JsonNode fieldValue) throws ApplicationDataException, ConfigurationException, FieldCalculatorNotFoundException;
}