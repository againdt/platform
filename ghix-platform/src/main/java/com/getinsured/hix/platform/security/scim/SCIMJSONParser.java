/**
 * 
 */
package com.getinsured.hix.platform.security.scim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author Biswakesh.Praharaj
 * 
 */
public class SCIMJSONParser {
	private static final Logger LOGGER = Logger.getLogger(SCIMJSONParser.class);

	/**
	 * This method parser the error response
	 * 
	 * @param responseStatus
	 * @param response
	 */
	public GIException processCreateUserErrorResponse(int responseStatus,
			JSONObject errorJson) {
		JSONArray jsonArray = null;
		@SuppressWarnings("rawtypes")
		Iterator iterator = null;
		String description = null;
		JSONObject tmp = null;
		String code = null;
		GIException gx = null;
		try {
			gx = new GIException();
			jsonArray = (JSONArray) errorJson.get("Errors");
			
			if(jsonArray != null) {
				iterator = jsonArray.iterator();
				while (iterator.hasNext()) {
					tmp = (JSONObject) iterator.next();
					description = (String) tmp.get("description");
					code = (String) tmp.get("code");
					if (null == description || description.length() <= 0) {
						description = "User creation failed";
					}
					if (null == code || code.length() <= 0) {
						code = "500";
					}
					gx.setErrorCode(Integer.valueOf(code));
					gx.setErrorMsg(description);
					gx.setErrorSeverity("CRITICAL");
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("USER COULD NOT BE CREATED");
						LOGGER.info("ERR CODE: " + code + " DESCRIPTION: "
								+ description);
					}
				}
			}else {
				gx.setErrorMsg("Error info not available in responseJson, responseStatus: " + responseStatus);
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("USER COULD NOT BE CREATED");
					LOGGER.info("Errors not available in responseJson, responseStatus: " + responseStatus);
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE PARSING RSP:", e);
		}
		return gx;
	}

	public void processCreateUserSuccessResponse(int responseStatus,
			JSONObject userJsonObject, AccountUser accountUser) throws GIException {
		String id = null;
		String userName = null;
		/*String firstName = null;
		String lastName = null;
		String dateOfBirth = null;
		String ssn = null;
		String mail = null;
		String phoneNumber = null;
		String prefMethComm = null;
		String preferredLanguage = null;
		String securityQuestion = null;
		String securityAnswer = null;
		String ridpFlag = null;*/
		try {
			id = (String) userJsonObject.get(SCIMClientConstants.ID_ATTR);
			userName = (String) userJsonObject.get(SCIMClientConstants.USER_NAME_ATTR);
			/*userJsonObject = (JSONObject) userJsonObject
					.get(SCIMClientConstants.WSO2_EXTENSION_ATTR);
			firstName = (String) userJsonObject
					.get(SCIMClientConstants.FIRST_NAME_ATTR);
			lastName = (String) userJsonObject
					.get(SCIMClientConstants.LAST_NAME_ATTR);
			dateOfBirth = (String) userJsonObject.get(SCIMClientConstants.DOB_ATTR);
			ssn = (String) userJsonObject.get(SCIMClientConstants.SSN_ATTR);
			mail = (String) userJsonObject.get(SCIMClientConstants.MAIL_ATTR);
			phoneNumber = (String) userJsonObject
					.get(SCIMClientConstants.PHONE_NUMBER_ATTR);
			prefMethComm = (String) userJsonObject
					.get(SCIMClientConstants.PREF_METH_COMM_ATTR);
			preferredLanguage = (String) userJsonObject
					.get(SCIMClientConstants.PREFERRED_LANGUAGE_ATTR);
			securityQuestion = (String) userJsonObject
					.get(SCIMClientConstants.SECURITY_QUESTION_ATTR);
			securityAnswer = (String) userJsonObject
					.get(SCIMClientConstants.SECURITY_ANSWER_ATTR);
			ridpFlag = (String) userJsonObject
					.get(SCIMClientConstants.RIDP_FLAG_ATTR);*/
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("USER CREATED SUCCESSFULLY");
			}
			accountUser.setExtnAppUserId(id);
			accountUser.setUserName(userName);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE PARSING DATA: ", e);
			throw new GIException("Error while processing SCIM response",e);
		}
	}
	
	public AccountUser processFetchUserResponse(JSONObject userJsonObject) throws GIException {
		String id = null;
		String userName = null;
		String firstName = null;
		String lastName = null;
		String dateOfBirth = null;
		String ssn = null;
		String mail = null;
		String phoneNumber = null;
		String prefMethComm = null;
		String preferredLanguage = null;
		String securityQuestion = null;
		String securityAnswer = null;
		String ridpFlag = null;
		JSONArray jsonArray = null;
		JSONObject childJsonObject = null;
		JSONObject wso2ExtensionObject = null;
		AccountUser accountUser = null;
		try {
			accountUser = new AccountUser();
			jsonArray = (JSONArray)userJsonObject.get("Resources");
			if (null != jsonArray) {
				// There is always one user available for a given email id.
				// Hence, one can safely fetch the only element in the array
				childJsonObject = (JSONObject)jsonArray.get(0);
				id = (String) childJsonObject.get(SCIMClientConstants.ID_ATTR);
				userName = (String) childJsonObject.get(SCIMClientConstants.USER_NAME_ATTR);
				
				wso2ExtensionObject = (JSONObject) childJsonObject
						.get(SCIMClientConstants.WSO2_EXTENSION_ATTR);
				firstName = (String) wso2ExtensionObject
						.get(SCIMClientConstants.FIRST_NAME_ATTR);
				lastName = (String) wso2ExtensionObject
						.get(SCIMClientConstants.LAST_NAME_ATTR);
				dateOfBirth = (String) wso2ExtensionObject.get(SCIMClientConstants.DOB_ATTR);
				ssn = (String) wso2ExtensionObject.get(SCIMClientConstants.SSN_ATTR);
				mail = (String) wso2ExtensionObject.get(SCIMClientConstants.MAIL_ATTR);
				phoneNumber = (String) wso2ExtensionObject
						.get(SCIMClientConstants.PHONE_NUMBER_ATTR);
				prefMethComm = (String) wso2ExtensionObject
						.get(SCIMClientConstants.PREF_METH_COMM_ATTR);
				preferredLanguage = (String) wso2ExtensionObject
						.get(SCIMClientConstants.PREFERRED_LANGUAGE_ATTR);
				securityQuestion = (String) wso2ExtensionObject
						.get(SCIMClientConstants.SECURITY_QUESTION_ATTR);
				securityAnswer = (String) wso2ExtensionObject
						.get(SCIMClientConstants.SECURITY_ANSWER_ATTR);
				ridpFlag = (String) wso2ExtensionObject
						.get(SCIMClientConstants.RIDP_FLAG_ATTR);
				
				accountUser.setExtnAppUserId(id);
				accountUser.setUserName(userName);
				accountUser.setFirstName(firstName);
				accountUser.setLastName(lastName);
				accountUser.setEmail(mail);
				accountUser.setPhone(phoneNumber);
				accountUser.setCommunicationPref(preferredLanguage);
				accountUser.setSecurityQuestion1(securityQuestion);
				accountUser.setSecurityAnswer1(securityAnswer);
			}
			
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("USER CREATED SUCCESSFULLY");
			}
			
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE PARSING DATA: ", e);
			throw new GIException("Error while processing SCIM response",e);
		}
		return accountUser;
	}
	
	
	
	

	/**
	 * Method which parses the List groups response to fetch
	 * 
	 * @param response
	 *            String containing the list groups response
	 * @return List containing names of groups
	 */
	@SuppressWarnings("rawtypes")
	public List<String> processListGroupsResponse(String response) {
		JSONParser jsonParser = null;
		JSONObject jsonObject = null;
		JSONArray jsonArray = null;
		Iterator jsonIterator = null;
		List<String> groups = null;
		try {
			jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(response);
			jsonArray = (JSONArray) jsonObject.get("Resources");
			if (null != jsonArray && !jsonArray.isEmpty()) {
				jsonIterator = jsonArray.iterator();
				groups = new ArrayList<String>();
				while (jsonIterator.hasNext()) {
					JSONObject groupJsonObject = (JSONObject) jsonIterator
							.next();
					String groupName = (String) groupJsonObject
							.get("displayName");
					groups.add(groupName);
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE FETCHING GRPS: ", e);
		}
		return groups;
	}

	/**
	 * Caller needs to check for null return
	 * 
	 * @param entity
	 * @return
	 * @throws GIException
	 */
	public JSONObject getJsonResponseFromEntity(HttpEntity entity)
			throws GIException {
		Exception ex = null;
		JSONObject obj = null;
		try {
			JSONParser parser = new JSONParser();
			InputStream is = entity.getContent();
			obj = (JSONObject) parser.parse(new BufferedReader(
					new InputStreamReader(is)));
		} catch (IOException ie) {
			ex = ie;
		} catch (ParseException e) {
			ex = e;
		}
		if (ex != null) {
			throw new GIException(
					"Failed to process JSON response received from WSO2", ex);
		}
		return obj;
	}
	
	public JSONObject getJsonResponseFromString(String jsonContent)
			throws GIException {
		Exception ex = null;
		JSONObject obj = null;
		try {
			JSONParser parser = new JSONParser();
			obj = (JSONObject) parser.parse(jsonContent);
		}  catch (ParseException e) {
			ex = e;
		}
		if (ex != null) {
			throw new GIException(
					"Failed to process JSON response received from WSO2", ex);
		}
		return obj;
	}

}
