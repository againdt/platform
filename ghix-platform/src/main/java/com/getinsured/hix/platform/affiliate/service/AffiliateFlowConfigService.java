package com.getinsured.hix.platform.affiliate.service;

import javax.servlet.http.HttpServletRequest;

import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.ConfigurationDTO;

public interface AffiliateFlowConfigService {

	void setFlowConfigurationInSession(HttpServletRequest request,
			Long affiliateId, AffiliateFlow affFlow, String showCallUsNumber,
			Object logoExists, String logoUrl, ConfigurationDTO affiliateConfig, Object hasEmployer);

	void setAncillaryConfigInSession(final HttpServletRequest httpRequest,
			ConfigurationDTO tenantConfiguration,
			ConfigurationDTO affiliateConfiguration);

	void setAffiliateConfigurationInSession(
			final HttpServletRequest httpRequest, Long affiliateId,
			Object affiliateHasEmployer, ConfigurationDTO affiliateConfig);

	void setFlowIdIfNotSetAlready(HttpServletRequest httpRequest,
			Long affiliateId);
}
