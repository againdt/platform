package com.getinsured.hix.platform.emailstat.service;

import java.util.Map;


public interface IEmailEventsService {

	boolean validateEmailEvent(Map<String, Object> emailStat);

	void saveEmailEvent(Map<String, Object> emailEventData);
	
}
