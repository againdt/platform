package com.getinsured.hix.platform.util.exception;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.platform.security.tokens.CSRFTokenTag;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class DropOversizeFilesMultipartResolver extends CommonsMultipartResolver {

	private static final Logger LOGGER = LoggerFactory.getLogger(DropOversizeFilesMultipartResolver.class);

	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	private String PM_ON_EXCHANGE_DISCLAIMER = "onExchangeDisclaimer";
	private String PM_OFF_EXCHANGE_DISCLAIMER = "offExchangeDisclaimer";
	private static HashSet<Character> illegalCharacters = new HashSet<Character>();

	static {
		illegalCharacters.add('<');
		illegalCharacters.add('>');
		illegalCharacters.add('\'');
		illegalCharacters.add('?');
		illegalCharacters.add('*');
		illegalCharacters.add('/');
		illegalCharacters.add('\\');
		illegalCharacters.add(':');
		illegalCharacters.add('%');
		illegalCharacters.add('#');
		illegalCharacters.add('|');
		illegalCharacters.add('{');
		illegalCharacters.add('}');
		illegalCharacters.add('~');
		illegalCharacters.add('\"');
	}
	/**
	 * Parse the given servlet request, resolving its multipart elements.
	 * 
	 * Thanks Alexander Semenov @ http://forum.springsource.org/showthread.php?62586
	 * 
	 * @param request
	 *            the request to parse
	 * @return the parsing result
	 */

	@SuppressWarnings("unchecked")
	@Override
	protected MultipartParsingResult parseRequest(final HttpServletRequest request) {

		String encoding = determineEncoding(request);
		FileUpload fileUpload = prepareFileUpload(encoding);
		List<FileItem> fileItems;
		try {
			fileItems = ((ServletFileUpload) fileUpload).parseRequest(request);
			Iterator<FileItem> it = fileItems.iterator();
			while(it.hasNext()) {
				CommonsMultipartFile file = new CommonsMultipartFile(it.next());
				char[] chars = file.getOriginalFilename().toCharArray();
				for(char c:chars) {
					 if(illegalCharacters.contains(c)) {
						 LOGGER.error("Illegal file name {} provided , Ref URL {} and Session Id:{}",file,request.getContextPath(), request.getSession().getId());
						 throw new MultipartException("Illegal file name provided with session");
					 }
				 }
			}
		} catch (FileUploadBase.SizeLimitExceededException ex) {
			request.setAttribute(GhixPlatformConstants.MAXUPLOADSIZEEXCEEDED_EXCEPTION, ex);
			fileItems = Collections.EMPTY_LIST;
		} catch (FileUploadException ex) {
			throw new MultipartException("Could not parse multipart servlet request", ex);
		}

		return parseFileItems(fileItems, encoding);
	}

	@Override
	public MultipartHttpServletRequest resolveMultipart(final HttpServletRequest request) throws MultipartException {
		Assert.notNull(request, "Request must not be null");

		MultipartParsingResult parsingResult = parseRequest(request);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("parsingResult--->" + parsingResult.getMultipartParameters());
		}
		Map<String, String[]> initialMap = parsingResult.getMultipartParameters();
		Map<String, String[]> finalMap = new HashMap<String, String[]>();
		boolean csrfFlag = false;
		String csrfParamName = CSRFTokenTag.CSRF_PARAMETER_NAME;
		String csrfTokenParameter = request.getHeader(csrfParamName);

		if (csrfTokenParameter == null || csrfTokenParameter.trim().isEmpty()) {
			csrfParamName = "X-CSRF-TOKEN".intern();
			// Lets try standard header
			csrfTokenParameter = request.getHeader(csrfParamName);
		}

		if (csrfTokenParameter != null && csrfTokenParameter.equals(request.getSession().getAttribute("csrftoken"))) {
			csrfFlag = true;
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("csrfFlag = " + csrfFlag + ", csrf token in session : "
					+ request.getSession().getAttribute("csrftoken") + ", csrf token in request header:"
					+ csrfTokenParameter);
		}

		for (Map.Entry me : initialMap.entrySet()) {
			String key = (String) me.getKey();
			LOGGER.debug("Key" + key);
			String value[] = (String[]) me.getValue();
			String[] cleanValue = new String[value.length];

			for (int i = 0; i < value.length; i++) {
				if (value[i].startsWith(GhixJasyptEncrytorUtil.GENC_TAG_START)) {
					cleanValue[i] = ghixJasyptEncrytorUtil.decryptGencStringByJasypt(value[i]);
				} else if(key.equalsIgnoreCase(PM_ON_EXCHANGE_DISCLAIMER)
						|| key.equalsIgnoreCase(PM_OFF_EXCHANGE_DISCLAIMER)) {
					cleanValue[i] = value[i];
				} else {
					cleanValue[i] = XssHelper.stripXSS(value[i]);
				}
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Value:" + cleanValue[i]);
				}
			}
			if (!csrfFlag && key.equals("csrftoken")) {
				csrfFlag = true;
				if (!cleanValue[0].equals(request.getSession().getAttribute("csrftoken"))) {

					LOGGER.error("Possible CSRF Attack : CSRF token not found - ");
					throw new MultipartException("Missing CSRF token");

				}
			}
			finalMap.put(key, cleanValue);
		}
		if (!csrfFlag) {
			throw new MultipartException("Missing CSRF token");
		}

		return new DefaultMultipartHttpServletRequest(request, parsingResult.getMultipartFiles(), finalMap,
				parsingResult.getMultipartParameterContentTypes());

	}
}
