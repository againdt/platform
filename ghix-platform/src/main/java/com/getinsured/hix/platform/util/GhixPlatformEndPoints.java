/**
 *
 */
package com.getinsured.hix.platform.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Captures all EndPoints details for different GHIX artifacts.
 * Under GHIX Application we have 2 main categories:-
 * 		1. GHIX-WEB URL --> used by
 * 					a. GHIX-WEB to access its own resources like NoticeTypes etc.
 * 					b. To make reverse call to GHIX-WEB from any Service WARS; for example:- GHIX-PLANDISPLAY consumes GHIX-WEB REST CLIENT to get PLAN-MGMT data.
 *
 * 		2. Service WARS --> used by
 * 					a. GHIX-WEB and other modules to invoke REST service exposed by respective Service WARS.
 * 					   example:- GHIX-WEB and GHIX-SHOP consumes PLAN_DISPLAY services.
 * 								 GHIX-SHOP consumes GHIX-AHBX services.
 *
 *
 *
 * Overall structure is to have a serviceURL constant for a module and an inner class to have all services exposed.
 * 			example:- "ghixPlandisplaySvcURL" and an inner class PlandisplayEndpoints which will have all services exposed by PLAN-DISPLAY module
 *
 *
 * PLS NOTE:- configuration.properties will only have serviceURL constant for a module configured and "NO REST service URL mapping".
 * 			  Please add properties under #EndPoints details for different GHIX artifacts - Starts section in configuration.properties files.
 *
 *
 */

@Component
public final class GhixPlatformEndPoints {
	
	public static final String LOGIN_PAGE = "/account/user/login";
	public static String GHIXWEB_SERVICE_URL;
	public static String GHIXHIX_SERVICE_URL;
	public static String GHIX_AHBX_URL;
	public static String APPSERVER_URL;
	public static String GHIX_AFFILIATE_SVC_URL;
	public static String APP_URL;

	@Value("#{configProp['ghixAffiliateServiceURL']}")
	public void setGhixAffiliateServiceUrl(String ghixAffiliateServiceUrl) {
		GHIX_AFFILIATE_SVC_URL = ghixAffiliateServiceUrl;
	}
	
	@Value("#{configProp['ghixWebServiceUrl']}")
	public void setGhixWebServiceUrl(String ghixWebServiceUrl) {
		GHIXWEB_SERVICE_URL = ghixWebServiceUrl;
	}
	
	/* 
	 * Please discuss with Ops if for some reason SVC/ batch needs to talk to WEB via TLS / https
	 * All internal calls between nodes happens without certs or firewalls for performance reasons.
	 * or use the GhixEndpoint.GHIXWEB_SERVICE_URL which uses ghixWebServiceUrl property which uses https 
	 */
	@Value("#{configProp['ghixHixServiceURL']}")
	public void setGhixHixServiceUrl(String ghixWebServiceUrl) {
		GHIXHIX_SERVICE_URL = ghixWebServiceUrl;
	}

	@Value("#{configProp['ghixAHBXServiceURL']}")
	public void setGhixAHBXServiceURL(String ghixAHBXServiceURL) {
		GHIX_AHBX_URL = ghixAHBXServiceURL;
	}

	@Value("#{configProp['appServer']}")
	public void setAPPSERVER_URL(String aPPSERVER_URL) {
		APPSERVER_URL = aPPSERVER_URL;
	}	

	@Value("#{configProp['appUrl']}")
	public void setAPP_URL(String appURL) {
		APP_URL = appURL;
	}

	public static class AHBXEndPoints{
		public static final String PLATFORM_VALIDATE_ADDRESS_OEDQ = GHIX_AHBX_URL + "platform/validateaddress/oedq";
		public static final String PLATFORM_ADOBELIVECYCLE = GHIX_AHBX_URL + "platform/createnotice/adobelivecycle";
		public static final String PLATFORM_UCM_CREATECONTENT = GHIX_AHBX_URL + "platform/ecm/createcontent";
		public static final String PLATFORM_UCM_GET_FILE_CONTENT = GHIX_AHBX_URL + "platform/ecm/getfilecontent";

		//Shop urls
		public static final String GET_EMPLOYER_DETAILS  = GHIX_AHBX_URL + "employer/getEmployer";
		public static final String GET_CASE_ORDER_ID     = GHIX_AHBX_URL + "employer/sendOrderId";

		//Delegation urls
		public static String DELEGATIONCODE_RESPONSE = GHIX_AHBX_URL + "delegation/validation/delegationcode";

		//Enrollment Urls
		public static final String ENROLLMENT_IND21_CALL_URL = GHIX_AHBX_URL + "enrollment/sendCarrierUpdatedData";
		public static final String ENROLLMENT_IND20_CALL_URL = GHIX_AHBX_URL + "enrollment/individualplanselection";
		//public static final String ENROLLMENT_IND53_CALL_URL = GHIX_AHBX_URL + "enrollment/verifypin";

		//Agent/Broker Rest URLS (ghix-web to ghix-ahbx)
		public static String SEND_ENTITY_DESIGNATION_DETAIL = GHIX_AHBX_URL + "entity/senddesignationdetail";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_STATUS = GHIX_AHBX_URL + "broker/retrieveindividualBOBStatus";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_DETAIL= GHIX_AHBX_URL + "broker/retrieveindividualbobdetails";
		public static String GET_EXTERNAL_EMPLOYER_BOB_DETAIL = GHIX_AHBX_URL + "broker/retrieveemployerbobdetails";
		public static String GET_EXTERNAL_EMPLOYER_BOB_STATUS = GHIX_AHBX_URL + "broker/retrieveemployersbobstatus";

		// sync plan and issuer data url
		public static final String SYNC_PLAN_DATA = GHIX_AHBX_URL + "plan/syncWithAHBX";
		public static final String SYNC_ISSUER_DATA = GHIX_AHBX_URL + "issuer/syncWithAHBX";

		//Agent/Assister/Enrollment Entity Rest URLs (ghix-web to ghix-ahbx and ghix-entity to ghix-ahbx)
		public static String SEND_ENTITY_DETAIL = GHIX_AHBX_URL + "entity/sendentitydetail";
	}

}
