package com.getinsured.hix.platform.sms.service.factory;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.hix.platform.sms.service.SmsService;
import com.getinsured.hix.platform.sms.service.jpa.SmsDuo;
import com.getinsured.hix.platform.sms.service.jpa.SmsTwilio;
import com.twilio.sdk.TwilioRestClient;

public final class SmsFactory {

	private static final String UNKNOWN_SMS_PROVIDER = "Unknown Sms provider configured";
	private static final String TWILIO_NOT_CONFIGURED = "TWILIO is not configured properly. Please supply correct value for accountSid, authToken and fromPhone";
	private static final String NO_SMS_PROVIDER = "No SMS Provider has been configured.";
	private static final String DUO_SECURITY_NOT_CONFIGURED = "DUO SECURITY is not configured properly. Please supply correct value for host, integrationKey and secretKey";

	protected static final Logger LOGGER = Logger.getLogger(SmsFactory.class);

	private enum PROVIDER{TWILIO,DUOSECURITY,NONE};

	private SmsFactory() {}

	/**
	 * static method to return concrete SMS PROVIDER. In our case TWILIO based on smsProvider.
	 * 
	 * @param smsProvider
	 * @return concrete SMS provider implementation
	 * @throws IllegalArgumentException, if parameters not configured properly
	 */
	public static SmsService createSmsService(Properties smsProvider) {

		LOGGER.info("smsProvider details - " + smsProvider);

		String provider = (String) smsProvider.get("provider");

		SmsService smsService = null;
		if (PROVIDER.TWILIO.toString().equals(provider)) {
			String accountSid = (String) smsProvider.get("accountSid");
			String authToken =  (String) smsProvider.get("authToken");
			String fromPhone =  (String) smsProvider.get("fromPhone");

			if (StringUtils.isEmpty(accountSid) || StringUtils.isEmpty(authToken) || StringUtils.isEmpty(fromPhone)){
				LOGGER.error(TWILIO_NOT_CONFIGURED);
				throw new IllegalArgumentException(TWILIO_NOT_CONFIGURED);
			}
			TwilioRestClient client = new TwilioRestClient(accountSid, authToken);
			smsService = new SmsTwilio(client, fromPhone);
		}
		else if(PROVIDER.DUOSECURITY.toString().equals(provider)){
			String host = (String) smsProvider.get("host");
			String integrationKey =  (String) smsProvider.get("integrationKey");
			String secretKey =  (String) smsProvider.get("secretKey");
			String fromPhone =  (String) smsProvider.get("fromPhone");

			if (StringUtils.isEmpty(host) || StringUtils.isEmpty(integrationKey) || StringUtils.isEmpty(secretKey)){
				LOGGER.error(DUO_SECURITY_NOT_CONFIGURED);
				throw new IllegalArgumentException(DUO_SECURITY_NOT_CONFIGURED);
			}
			smsService = new SmsDuo(host, integrationKey,secretKey,fromPhone);
		}
		
		else if (PROVIDER.NONE.toString().equals(provider)) {
			LOGGER.info(NO_SMS_PROVIDER);
		}
		else{
			LOGGER.warn(UNKNOWN_SMS_PROVIDER);
		}

		return smsService;

	}
}
