package com.getinsured.hix.platform.accountactivation;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * Helper class to send Account Activation email in the creator-created flow.
 *
 * @author Ekram Ali Kazi
 *
 */
@Component
public class AccountActivationEmail extends NotificationAgent {
	@Value("#{configProp.exchangename}")
	private String exchangeName;
	
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson) notificationContext.get("ACTIVATION_JSON");
		AccountActivation accountActivation = (AccountActivation) notificationContext.get("ACCOUNT_ACTIVATION");
		Map<String,String> bean = new HashMap<String, String>();
		String activationUrl = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL + "account/user/activation/" + accountActivation.getActivationToken();

		bean.put("name", activationJsonObj.getCreatedObject().getFullName());
		bean.put("exchangename", exchangeName );
		bean.put("activationUrl", activationUrl );
		return bean;
	}
	
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson) notificationContext.get("ACTIVATION_JSON");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", activationJsonObj.getCreatedObject().getEmailId());
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(!"NV".equalsIgnoreCase(stateCode)) {
			data.put("Subject", "A record has been created for you on the " + exchangeName + " Exchange");
		}
		
		if(activationJsonObj != null && activationJsonObj.getCreatedObject() != null && activationJsonObj.getCreatedObject().getCustomeFields() != null ){
			
			if(StringUtils.isNotBlank(activationJsonObj.getCreatedObject().getCustomeFields().get("fromEmailAddress"))) {
				data.put("From",activationJsonObj.getCreatedObject().getCustomeFields().get("fromEmailAddress"));
			}
			
			if(StringUtils.isNotBlank(activationJsonObj.getCreatedObject().getCustomeFields().get("emailSubject"))) {
				data.put("Subject",activationJsonObj.getCreatedObject().getCustomeFields().get("emailSubject"));
			}
		}
		
		return data;
	}
	
	public Notice generateEmail(Map<String, Object> notificationContext){
		
		Map<String, String> tokens = this.getTokens(notificationContext);
		Map<String, String> emailData = this.getEmailData(notificationContext);
		try{
			Location location = (Location) notificationContext.get("LOCATION");
			return this.generateEmail(this.getClass().getSimpleName(), location, emailData, tokens);
		}catch(Exception e){
			throw new GIRuntimeException("Faailed to retrieve the notive for account activation enmail, Message:"+e.getMessage(),e);
		}
		
	}
	

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}