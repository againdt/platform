package com.getinsured.hix.platform.util;

import java.util.Collection;
import java.util.List;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class CollectionConverterUtil {
	
	@SuppressWarnings("rawtypes")
	public static List toList(Collection objects){
		if(objects !=null && objects instanceof List){
			return (List) objects;
		}else{
			return null;
		}
		
	}

}
