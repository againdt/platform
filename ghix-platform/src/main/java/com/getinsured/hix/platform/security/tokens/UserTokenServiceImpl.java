/**
 * 
 */
package com.getinsured.hix.platform.security.tokens;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.UserTokens;
import com.getinsured.hix.platform.security.repository.IUserTokensRepository;

/**
 * @author Biswakesh
 *
 */
@Service("userTokenService")
public class UserTokenServiceImpl implements UserTokenService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserTokenServiceImpl.class);
	
	@Autowired private IUserTokensRepository iUserTokensRepository;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.platform.security.tokens.UserTokenService#findDeprecatedTokens(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<UserTokens> findDeprecatedTokens(String userIdentifier,
			String sourceName, String eventName) {
		List<UserTokens> userTokensList = null;
		
		try {
			userTokensList = iUserTokensRepository.findUnusedTokensWithSameEventAndSource(userIdentifier, sourceName, eventName); 
		} catch(Exception ex) {
			logger.error("ERR: WHILE FETCHING DEPRECATED TOKENS: ",ex);
		}
		
		return userTokensList;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.platform.security.tokens.UserTokenService#findUserToken(java.lang.String)
	 */
	@Override
	public UserTokens findUserToken(String token) {
		UserTokens userTokens = null;
		
		try {
			userTokens = iUserTokensRepository.findUserToken(token);
		} catch(Exception ex) {
			logger.error("ERR: WHILE FETCHING USR TOKENS: ",ex);
		}
		
		return userTokens;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.platform.security.tokens.UserTokenService#deprecateTokens(java.util.List)
	 */
	@Override
	public void deprecateTokens(List<UserTokens> userTokens) {
		
		try {
			for (UserTokens userToken : userTokens) {
				userToken.setStatus('0');
			}
			iUserTokensRepository.save(userTokens);
		} catch(Exception ex) {
			logger.error("ERR: WHILE TRYING TO DEPRECATE TOKENS: ",ex);
		}

	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.platform.security.tokens.UserTokenService#createUserToken(com.getinsured.hix.model.UserTokens)
	 */
	@Override
	public void createUserToken(UserTokens userTokens) {
		
		try {
			iUserTokensRepository.saveAndFlush(userTokens);
		} catch(Exception ex) {
			logger.error("ERR: WHILE TRYING TO SAVE TOKEN: ",ex);
		}
		
	}

}
