package com.getinsured.hix.platform.util.exception;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.getinsured.hix.model.GIMonitor;

// @ControllerAdvice
public class GIRestExceptionResolver /*extends ResponseEntityExceptionHandler */{
	
	private static final Logger LOGGER = Logger.getLogger(GIRestExceptionResolver.class);
//	@Autowired
//	private GIExceptionHandler giExceptionHandler;
//
//	// @ExceptionHandler(value = { Exception.class })
//	private ResponseEntity<Object> resolveException(Exception exception, WebRequest request) {
//		ResponseEntity<Object> response = null;
//		final HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.TEXT_PLAIN);
//
//		try {
//			GIMonitor giMonitor = giExceptionHandler.recordException(exception);
//			if(giMonitor != null) {
//				response = new ResponseEntity<Object>(giMonitor.getId(), headers, HttpStatus.PARTIAL_CONTENT);
//			}else{
//				response = new ResponseEntity<Object>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
//				LOGGER.fatal("Internal server error: ", exception);
//			}
//		} catch (Exception ex) {
//			LOGGER.fatal("Failed to record exception: ", ex);
//			/*response = handleExceptionInternal(ex, ExceptionUtils.getFullStackTrace(ex), headers,
//					HttpStatus.INTERNAL_SERVER_ERROR, request);*/
//		}
//
//		return response;
//	}

}
