package com.getinsured.hix.platform.util;

import javax.crypto.Cipher;

public class CipherContext {
	
	private Cipher cipher;
	private int cipherMode = -1;
	
	public CipherContext(Cipher cipher, int mode){
		this.cipher = cipher;
		this.cipherMode = mode;
	}
	public Cipher getCipher() {
		return cipher;
	}
	public void setCipher(Cipher cipher) {
		this.cipher = cipher;
	}
	public int getCipherMode() {
		return cipherMode;
	}
	public void setCipherMode(int cipherMode) {
		this.cipherMode = cipherMode;
	}
}
