package com.getinsured.hix.platform.security.tokens;

import java.security.SecureRandom;
import java.util.HashSet;

import org.apache.commons.codec.binary.Base64;

import com.getinsured.timeshift.TimeShifterUtil;

/**
 * CSRF token generation helper.
 */
public class CSRFToken
{
  /**
   * Size of random bytes used by {@link #getNewToken()}.
   *
   * Current value is {@value #CSRF_TOKEN_LENGTH}.
   */
  public static final int CSRF_TOKEN_LENGTH = 32;

  private static SecureRandom random = new SecureRandom();

  static
  {
    byte[] seed = random.generateSeed(256);
    random.setSeed(seed);
  }

  /**
   * Returns random CSRF token.
   *
   * @return new pseudo-random token.
   * @see #CSRF_TOKEN_LENGTH
   */
  public static String getNewToken()
  {
    byte[] buf = new byte[CSRF_TOKEN_LENGTH];
    random.nextBytes(buf);
    return Base64.encodeBase64URLSafeString(buf);
  }

  public static void main(String[] args)
  {
    HashSet<String> tokenSet = new HashSet<String>();
    String token = null;
    long start = TimeShifterUtil.currentTimeMillis();
    for (int i = 0; i < 10000000; i++)
    {
      token = getNewToken();

      if (!tokenSet.add(token))
      {
        System.out.println("Duplicate token produced, bad algorithm attempt [" + i + "]");
      }
    }

    System.out.println("Took:" + (TimeShifterUtil.currentTimeMillis() - start) + " ms to generate & validate " + tokenSet.size() + " tokens, last token:" + token);
  }
}
