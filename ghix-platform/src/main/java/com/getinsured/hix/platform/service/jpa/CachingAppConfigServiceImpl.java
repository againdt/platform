package com.getinsured.hix.platform.service.jpa;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.model.ConfigPageData;
import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.model.Tenant2TenantDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.repository.GIAppConfigRepository;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.service.GIAppConfigService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@EnableScheduling
public class CachingAppConfigServiceImpl implements GIAppConfigService {
	
	private static Logger logger = LoggerFactory.getLogger(CachingAppConfigServiceImpl.class);
	private Map<Long, TenantProperties> cacheSet = new ConcurrentHashMap<Long, TenantProperties>();
	private static  boolean tenantAware = "on".equalsIgnoreCase(System.getProperty("tenant.enabled"));

	private static long tenantUnawareSystemTenantId = 100000l;

	@Autowired
	private GIAppConfigRepository gIAppConfigRepository;

	@Autowired
	private TenantRepository tenantRepository;

	private TenantDTO defaultTenant;

	@PersistenceUnit
	private EntityManagerFactory emf;

	@PostConstruct
	private void initDefaultTenant() {
		defaultTenant = Tenant2TenantDTO.Current.convert(tenantRepository.findByCode("GINS"));
		refresh(); // Initial Refresh
	}

	@Scheduled(cron="0 0 * * * ?")
	public  void refresh() {
		List<GIAppProperties> allProps = gIAppConfigRepository.findAll();

		if(logger.isInfoEnabled())
		{
			logger.info("Initiating Refresh, Found : {} properties, creating cache", allProps.size());
		}

		TenantProperties tenant = null;
		Long tenantId = tenantUnawareSystemTenantId;
		for (GIAppProperties property : allProps) {
			if(property != null) {
				if(tenantAware){
					tenantId = (property.getTenantId() == null) ? defaultTenant.getId() : property.getTenantId();
				}
				tenant = this.cacheSet.get(tenantId);
				if (tenant != null) {
					tenant.wrap(property.getPropertyKey(), property);
				} else {
					tenant = new TenantProperties();
					tenant.wrap(property.getPropertyKey(), property);
					this.cacheSet.put(tenantId, tenant);
				}
			}
		}
	}

	@Override
	public Object getPropertyValue(String propertyName) {
		return getProperty(propertyName, this.defaultTenant.getId());
	}

	private Object getProperty(String propertyName, long tenantId) {
		long requiredTenantId = tenantId;
		if(!tenantAware){
			requiredTenantId = tenantUnawareSystemTenantId;
		}
		TenantProperties tenantProps = this.cacheSet.get(requiredTenantId);
		Object val = null;
		if (tenantProps != null && (val = tenantProps.get(propertyName)) != null) {
			if(logger.isTraceEnabled()){
				logger.trace("Cache Hit:Returning config property:{} From cache, tenant Id:{} is System tenant aware: {}", propertyName, requiredTenantId, tenantAware);
			}
			return val;
		}
		// Reaching here means we do not have this property available in the
		// cache
		if(logger.isInfoEnabled())
		{
			logger.info("Cache Miss: Retrieving the property from DB , for tenant:{} Will cache it for further use till we refresh", requiredTenantId);
		}

		GIAppProperties value = null;
		if(tenantAware){
			value = gIAppConfigRepository.findByPropertyKeyAndTenantId(propertyName, requiredTenantId);
		}else{
			value = gIAppConfigRepository.findByPropertyKey(propertyName);
		}
		val = validateProperty(StringUtils.EMPTY, value);

		if(logger.isTraceEnabled())
		{
			logger.trace("Received value for: {}", propertyName);
		}

		if (tenantProps != null) { // Tenant available in the cache but property not
			tenantProps.wrap(propertyName, value);
		} else {
			tenantProps = new TenantProperties();
			tenantProps.wrap(value.getPropertyKey(), value);

			if(tenantProps != null)
			{
				this.cacheSet.put(requiredTenantId, tenantProps);
			}
		}
		return val;
	}

	@Override
	public Object getPropertyValue(String propertyName, Long tenantId) {
		// To handle scenarios where getPropertyValues is called for
		// tenant.enabled = on but there is no tenant id in the context (and it
		// is not needed? HIX-73324)
		Long actualTenantId = tenantId == null ? defaultTenant.getId() : tenantId;
		return getProperty(propertyName, actualTenantId);
	}

	/**
	 * This function should never be used in a multi tenant scenario
	 */
	@Override
	public Map<String, String> getPropertyValues(String propertyName) {
		long requiredTenantId = this.defaultTenant.getId();
		if(!tenantAware){
			requiredTenantId = tenantUnawareSystemTenantId;
		}
		Long tenantId = requiredTenantId;
		TenantProperties props = null;

		if(this.cacheSet.containsKey(tenantId))
		{
			props = this.cacheSet.get(tenantId);
		}

		if(props != null){
			return props.getAllStartingWith(propertyName);
		}
		return new HashMap<String,String>();
	}

	@Override
	public Map<String, String> getPropertyValues(String propertyName, Long tenantId) {
		// To handle scenarios where getPropertyValues is called for
		// tenant.enabled = on but there is no tenant id in the context (and it
		// is not needed? HIX-73324)
		Long actualTenantId = tenantId == null ? defaultTenant.getId() : tenantId;
		TenantProperties tenant = this.cacheSet.get(actualTenantId);
		return tenant.getAllStartingWith(propertyName);
	}

	public GIAppConfigRepository getGIAppConfigRepository() {
		return gIAppConfigRepository;
	}

	public void setGIAppConfigRepository(GIAppConfigRepository gIAppConfigRepository) {
		this.gIAppConfigRepository = gIAppConfigRepository;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GIRuntimeException.class)
	public void addTenantAppConfigurations(Long tenantId) {
		if (tenantId != null) {
			String queryStr = "INSERT INTO GI_APP_CONFIG ( ID, PROPERTY_KEY, PROPERTY_VALUE,"
					+ " DESCRIPTION, CREATED_BY, CREATION_TIMESTAMP, LAST_UPDATED_BY,"
					+ " LAST_UPDATE_TIMESTAMP, TENANT_ID ) ( SELECT GI_APP_CONFIG_SEQ.nextval,"
					+ " gi.PROPERTY_KEY, gi.PROPERTY_VALUE, gi.DESCRIPTION,"
					+ " (SELECT id from users where username = 'exadmin@ghix.com' and tenant_id = (select id from tenant where code = 'GINS')),"
					+ " CURRENT_TIMESTAMP,"
					+ " (SELECT id from users where username = 'exadmin@ghix.com' and tenant_id = (select id from tenant where code = 'GINS')), "
					+ " CURRENT_TIMESTAMP, :tenantId FROM GI_APP_CONFIG gi, tenant t"
					+ " where t.name = 'GetInsured' AND gi.TENANT_ID = t.ID )";
			EntityManager em = null;
			Query query = null;
			try {
				em = emf.createEntityManager();
				if (!em.getTransaction().isActive()) {
					em.getTransaction().begin();
				}
				query = em.createNativeQuery(queryStr);
				query.setParameter("tenantId", tenantId);
				int count = query.executeUpdate();

				if(logger.isInfoEnabled())
				{
					logger.info("Number of records inserted in GI_APP_CONFIG: {}", count);
				}
				em.getTransaction().commit();
			} catch (Exception e) {
				if(logger.isErrorEnabled())
				{
					logger.error("Failed to insert records in GI_APP_CONFIG for a tenant", e);
				}

				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
				throw new GIRuntimeException(e);
			} finally {
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
		}
	}

	private String validateProperty(String propertyValue, GIAppProperties giAppProperty) {
		if (null != giAppProperty) {
			propertyValue = giAppProperty.getPropertyValue();
			try {
				ConfigPageData.validateConfigProperty(giAppProperty.getPropertyKey(), giAppProperty.getPropertyValue());
			} catch (InvalidOperationException | ConfigValidationException e) {
				throw new GIRuntimeException(
						"Failed to validate the property [" + giAppProperty.getPropertyKey() + "] ", e);
			}
		}
		return propertyValue;
	}

	private static class TenantProperties
	{
		Map<String, GIAppProperties> properties = new ConcurrentHashMap<String, GIAppProperties>();

		public void wrap(String property, GIAppProperties value)
		{
			if(property == null || value == null)
			{
				if(logger.isWarnEnabled())
				{
					logger.warn("Unable to put property {} with value {}", property, value);
				}
			}
			else
			{
				this.properties.put(property, value);
			}
		}

		public Object get(String property)
		{
			GIAppProperties wrapper = null;

			// If property passed is null, .get() will throw NPE on ConcurrentHashMap, so we check
			// if it's there before getting it.
			if(this.properties.containsKey(property))
			{
				wrapper = this.properties.get(property);
			}

			if (wrapper == null)
			{
			//	logger.warn("No value found for property {} Check if applicable to the target environment", property);
				return null;
			}

			return wrapper.getPropertyValue();
		}

		public Map<String, String> getAllStartingWith(String match)
		{
			Map<String, String> returnSet = new HashMap<String, String>();
			Set<String> keySet = this.properties.keySet();
			Iterator<String> cursor = keySet.iterator();
			String tmp = null;
			while (cursor.hasNext())
			{
				tmp = cursor.next();
				if (tmp.toLowerCase().startsWith(match.toLowerCase()))
				{
					returnSet.put(tmp, this.properties.get(tmp).getPropertyValue());
				}
			}
			return returnSet;
		}
	}
}
