package com.getinsured.hix.platform.payment.util;

import java.util.Random;

/**
 *  This is the utility class for finance module.
 *  This class has utility methods for payments.
 * 
 * @author panda_p
 * @since 20-Nov-2012
 */

public final class PaymentUtil {
	
	private PaymentUtil() {		
	}
	
	public static final String alphaNumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static Random rnd = new Random();
	
	/**
	 * Method getCardTypeCode
	 * @param cardType String
	 * @return String	 
	 */
	public static String getCardTypeCode(String cardType){
		
		if(cardType.equals("visa")){
			return "001";
		}else if(cardType.equals("mastercard")){
			return "002";
		}else if(cardType.equals("americanexpress")){
			return "003";
		}else{
			return null;
		}
	}
	
	/**
	 * Method getCardType
	 * @param cardCode String
	 * @return String	 
	 */
	public static String  getCardType(String cardCode)
	{
		if(cardCode.equals("001")){
			return "visa";
		}else if(cardCode.equals("002")){
			return "mastercard";
		}else if(cardCode.equals("003")){
			return "americanexpress";
		}else{
			return null;
		}
	}

	/**
	 * Method generateMerchantReferenceCode
	 * @return String	 
	 */
	public static String generateMerchantReferenceCode()
	{
		   StringBuilder sb = new StringBuilder( 16 );
		   for( int i = 0; i < 16; i++ ) 
		   {
		      sb.append( alphaNumeric.charAt( rnd.nextInt(alphaNumeric.length()) ) );
		   }
		   return sb.toString();
	}
}