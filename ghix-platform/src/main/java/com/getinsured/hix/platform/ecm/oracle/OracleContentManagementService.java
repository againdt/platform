package com.getinsured.hix.platform.ecm.oracle;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.platform.ecm.CMISErrors;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.dto.platform.ecm.Document;
import com.getinsured.hix.dto.platform.ecm.Folder;
import com.getinsured.hix.dto.platform.ecm.Image;
import com.getinsured.hix.dto.platform.ecm.MetadataElement;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformRequest.ECMRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.ecm.CMISSessionUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

public class OracleContentManagementService implements ContentManagementService {

	private static final String METADATA_MIME_TYPE = "mimeType";

	private static final String AHBX_UCM_CALL_FAILED = "AHBX UCM call failed - ";

	private static final Logger LOGGER = Logger.getLogger(OracleContentManagementService.class);

	@Autowired private RestTemplate restTemplate;

	public OracleContentManagementService() {
		super();
	}

	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, boolean skipAntiVirusCheck) throws ContentManagementServiceException {
		return createContent(relativePath, fileName, dataBytes, category, subCategory, type);
	}

	@Override
	public String createContent(String relativePath, String fileName,
			byte[] dataBytes, String category, String subCategory, String type) throws ContentManagementServiceException {
		return createContent(relativePath, fileName, dataBytes);
	}

	@Override
	public String createContent(String relativePath, String fileName,
			byte[] dataBytes) throws ContentManagementServiceException {

		String mimeType = validateIncomingContent(fileName, dataBytes);

		/**
		 * form platformRequest for creating content in AHBX UCM
		 */
		/**
		 * populate metadata HashMap with mimeType for Release1
		 */
		PlatformRequest platformRequest = new PlatformRequest();
		ECMRequest ecmRequest = platformRequest.new ECMRequest();
		ecmRequest.setBytes(dataBytes);

		HashMap<String, String> metadata = new HashMap<String, String>();
		metadata.put(METADATA_MIME_TYPE, mimeType);
		ecmRequest.setMetadata(metadata);
		platformRequest.setEcmRequest(ecmRequest);

		/**
		 * Rest WS call to GHIX-AHBX module
		 */
		String ahbxResponse = restTemplate.postForObject(AHBXEndPoints.PLATFORM_UCM_CREATECONTENT, platformRequest, String.class);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamGenericObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX OEDQ call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			LOGGER.warn(AHBX_UCM_CALL_FAILED +  platformResponse.getErrCode() + platformResponse.getErrMsg());
			throw new ContentManagementServiceException(AHBX_UCM_CALL_FAILED +  platformResponse.getErrMsg());
		}

		return platformResponse.getAhbxEcmId();
	}

	private String validateIncomingContent(String fileName, byte[] dataBytes) {
		if (StringUtils.isEmpty(fileName)){
			throw new IllegalArgumentException(CMISErrors.FILE_NAME_CANNOT_BE_NULL);
		}

		String mimeType = CMISSessionUtil.getAHBXMime(fileName);
		if (StringUtils.isEmpty(mimeType)){
			throw new IllegalArgumentException(CMISErrors.UNSUPPORTED_DOCUMENT_TYPE);
		}

		if (dataBytes == null || dataBytes.length <= 0){
			throw new IllegalArgumentException(CMISErrors.DATA_BYTES_CANNOT_BE_NULL);
		}
		return mimeType;
	}

	@Override
	public String deleteContent(String contentId)
			throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public String updateContent(String contentId, String mimeType,
			byte[] newContent) throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public String updateContent(String contentId, String mimeType,
			byte[] newContent,String checkinNote, String User) throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, boolean skipAntiVirusCheck)
			throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, String checkinNotes, String user,
			boolean skipAntiVirusCheck) throws ContentManagementServiceException {
		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}


	@Override
	public String updateContentMetadata(MetadataElement metadataElement)
			throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public String updateContentMetadata(MetadataElement metadataElement,
			List<String> tags) throws ContentManagementServiceException {
		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public Content getContentByPath(String relativePath)
			throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public Content getContentById(String contentId) throws ContentManagementServiceException {

		/**
		 * form platformRequest for getting content object by id from AHBX UCM
		 */
		PlatformRequest platformRequest = new PlatformRequest();
		ECMRequest ecmRequest = platformRequest.new ECMRequest();
		ecmRequest.setDocId(contentId);
		platformRequest.setEcmRequest(ecmRequest);

		/**
		 * Rest WS call to GHIX-AHBX module
		 */
		String ahbxResponse = restTemplate.postForObject(AHBXEndPoints.PLATFORM_UCM_GET_FILE_CONTENT, platformRequest, String.class);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamGenericObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX OEDQ call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			LOGGER.warn(AHBX_UCM_CALL_FAILED +  platformResponse.getErrCode() + platformResponse.getErrMsg());
			throw new ContentManagementServiceException(AHBX_UCM_CALL_FAILED +  platformResponse.getErrMsg());
		}

		return platformResponse.getGiContent();
	}

	@Override
	public byte[] getContentDataByPath(String relativePath)
			throws ContentManagementServiceException {

		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public byte[] getContentDataById(String contentId) throws ContentManagementServiceException {

		Content content = getContentById(contentId);

		if (content instanceof Document){
			Document documentContent = (Document) content;
			return documentContent.getDocumentStream().getContent();
		} else if (content instanceof Image){
			Image imageContent = (Image) content;
			return imageContent.getImageStream();
		} else if (content instanceof Folder){
			// This should never happen for document type
			//Folder folderContent = (Folder) content;
			return new byte[0];
		}

		return new byte[0]; // safe exit
	}

	@Override
	public Content getContentByPath(String relativePath, boolean latestVer)
			throws ContentManagementServiceException {
		throw new ContentManagementServiceException(CMISErrors.NOT_YET_IMPLEMENTED);
	}

	@Override
	public String createContent(String relativePath, String fileName,
			String dataString, String category, String subCategory, String type) throws ContentManagementServiceException {
		throw new ContentManagementServiceException(CMISErrors.ONLY_APPLICABLE_FOR_COUCH);
	}

}