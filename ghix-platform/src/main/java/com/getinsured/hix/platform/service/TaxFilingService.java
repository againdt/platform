package com.getinsured.hix.platform.service;

import com.getinsured.hix.model.TaxYear;

public interface TaxFilingService {
	
	TaxYear findTaxFilingDateByYear(String yearOfEffectiveDate);
	
}
