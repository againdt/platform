package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.affiliate.model.Affiliate;

public interface AffiliateTenantUnawareRepository extends TenantUnawareRepository<Affiliate, Long> {
	List<Affiliate> findByUrl(String url);

	@Query("SELECT affiliate FROM Affiliate affiliate WHERE affiliate.id = :affiliateId")
	Affiliate findById(@Param("affiliateId") Long affiliateId);
}
