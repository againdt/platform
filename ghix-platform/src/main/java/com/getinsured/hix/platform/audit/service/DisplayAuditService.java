package com.getinsured.hix.platform.audit.service;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.history.RevisionRepository;

/**
 * 
 * @author EkramAli Kazi
 * Interface for Display Audit
 *
 */
public interface DisplayAuditService {

	/**
	 * fetches details for given entityId
	 * @param repoBeanName
	 * @param className
	 * @param requiredFieldsMap
	 * @param entityId
	 * @return list of map. each map contains one row. map has value as column name and its corresponding value.
	 * @throws IllegalArgumentException, RuntimeException, AssertError
	 */
	List<Map<String, String>> findRevisions(String repoBeanName, String className, Map<String, String> requiredFieldsMap, Integer entityId);
	
	/**
	 * this method works exactly like 'findRevisions(-,-,-,-) method except return type (<String, Object>) to get the actual objects returned from the database. 
	 */
	List<Map<String, Object>> findAudRevisions(String repoBeanName, String className, Map<String, String> requiredFieldsMap, Integer entityId);

	<T> List<Map<String, String>> findRevisions(RevisionRepository<T, Integer, Integer> repository,
			String modelName, Map<String, String> requiredFieldsMap, Integer entityId);

	void setApplicationContext(ApplicationContext context);
}
