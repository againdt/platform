package com.getinsured.hix.platform.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.ws.transport.http.WsdlDefinitionHandlerAdapter;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

/*
 * This class is used to dynamically configure the SOAP endpoint URLs.
 * @author: Ratnakar Malla
 * 
 */
@Service
public class ConfigurableWsdlDefinitionHandlerAdapter extends
		WsdlDefinitionHandlerAdapter {
		
	
	
	    @Override
	    protected String transformLocation(String location, HttpServletRequest request) {
	    	String endpointUrl="";
	    	String secureURLs = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SECURE_WEB_SVC_URLS);
	    	if ("Y".equals(secureURLs))
	    	{
	    		String exchangeURL = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
	    		if (exchangeURL!=null)
	    		{
	    			if (exchangeURL.indexOf("http://")!=-1)
	    			{
	    				exchangeURL=exchangeURL.substring(exchangeURL.indexOf("http://")+7);
	    			}
	    		}
	    		endpointUrl="https://"+exchangeURL+":443"+ request.getContextPath()+"/"; 
	    	} else
	    	{
	    		endpointUrl="http://"+ request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+"/";
	    	}
	    	StringBuilder url = new StringBuilder(endpointUrl);
	        if (location.startsWith("/") && endpointUrl.endsWith("/")) {
	            url.append(location.substring(1));
	        } else {
	            url.append(location);
	        }
	        return url.toString();
	    }
	 
	   
	    
}
