package com.getinsured.hix.platform.security.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CapTeam;
import com.getinsured.hix.model.CapTeamMembers;
import com.getinsured.hix.platform.repository.TenantAwareRepository;




public interface IGroupUsersRepository extends TenantAwareRepository<CapTeamMembers, Integer>{

	/**
	 * @author kaul_s
	 * @return List<CapTeamMembers>
	 * @param groupId
	 * @return
	 */
	@Query("SELECT ug.userId FROM CapTeamMembers ug WHERE ug.groupId= :groupId and ug.endDate IS NULL")
	List<Integer> findUsersByGroupId(@Param("groupId") Integer groupId);

	/**
	 * @author kaul_s
	 * @return void
	 * @param groupId
	 * @param userList
	 */
	@Transactional
	@Modifying
	@Query("DELETE FROM CapTeamMembers ug WHERE ug.groupId= :groupId AND ug.userId IN (:userList)")
	void deleteByGroupAndUsers(@Param("groupId") Integer groupId, @Param("userList") List<Integer> userList);
	
	
	/**
	 * @author kaul_s
	 * @return void
	 * @param groupId
	 * @param userList
	 */
	@Transactional
	@Modifying
	@Query("UPDATE CapTeamMembers ug  set ug.endDate= :endDate WHERE ug.groupId= :groupId AND ug.userId IN (:userList)")
	void deleteByGroupAndUsersWithEndDate(@Param("groupId") Integer groupId, @Param("userList") List<Integer> userList,@Param("endDate") Date endDate); 
	/**
	 * @author kaul_s
	 * @return List<CapTeamMembers>
	 * @param groupId
	 * @param userList
	 */
	@Transactional
	@Modifying
	@Query("SELECT ug FROM CapTeamMembers ug WHERE ug.groupId= :groupId AND ug.userId = :userId")
	List<CapTeamMembers> findByGroupAndUsersId(@Param("groupId") Integer groupId, @Param("userId") Integer userId);
	
	@Transactional
	@Modifying
	@Query("SELECT ug FROM CapTeamMembers ug WHERE ug.groupId= :groupId AND ug.endDate IS NULL")
	List<CapTeamMembers> findActiveTeamMembers(@Param("groupId") Integer groupId);
	
	@Transactional
	@Modifying
	@Query("SELECT ug FROM CapTeamMembers ug WHERE ug.userId = :userId AND ug.endDate IS NULL")
	List<CapTeamMembers> findActiveTeamMemberByUserId(@Param("userId") Integer userId);
	
	/**
	 * @author kaul_s
	 * @return List<CapTeamMembers>
	 * @param groupId
	 * @param userList
	 */
	@Query(value="SELECT u.id, u.first_Name, u.last_Name,  ug.start_Date, ug.end_Date FROM Cap_Team_Members ug, users u WHERE ug.user_Id =u.id AND ug.cap_team_id = :groupId AND ug.end_date IS NULL",nativeQuery = true)
	List<Object[]> findByGroupIdAndEndDate(@Param("groupId") Integer groupId);
	
	/**
	 * @author kaul_s
	 * @return void
	 * @param groupId
	 */
	@Transactional
	@Modifying
	@Query("DELETE FROM CapTeamMembers ug WHERE ug.groupId= :groupId ")
	void deleteByGroup(@Param("groupId") Integer groupId);
	
	/**
	 * @author kaul_s
	 * @return List<CapTeamMembers Id>
	 * @param userId
	 * @return
	 */
	@Query("SELECT ug.groupId FROM CapTeamMembers ug WHERE ug.userId= :userId")
	List<Integer> findGroupsByUserId(@Param("userId") Integer userId);
	
	@Query("select u from CapTeamMembers ug, AccountUser u where  u.id= ug.userId and ug.groupId=:groupId))")
	List<AccountUser> getAccountUsersByGroupId(@Param("groupId") Integer groupId);
	
	@Query("select u from CapTeamMembers ug, AccountUser u where  ug.endDate IS NULL and u.id= ug.userId and ug.groupId=:groupId))") 
	List<AccountUser> getActiveAccountUsersByGroupIdAndEndDate(@Param("groupId") Integer groupId);
		
	@Query("select u from AccountUser u where u.id IN (select ug.userId from CapTeamMembers ug, CapTeam gou where  gou.id= ug.groupId and gou.groupLeadUserId=:leadId) order by u.firstName, u.lastName")
	List<AccountUser> getAccountUsersByLeadId(@Param("leadId") Integer leadId);
	
	@Query("select u from AccountUser u where u.id IN (select ug.userId from CapTeamMembers ug, CapTeam gou where  gou.id= ug.groupId and gou.groupLeadUserId=:leadId and ug.endDate IS NULL) order by u.firstName, u.lastName")
	List<AccountUser> getActiveAccountUsersByLeadIdAndEndDate(@Param("leadId") Integer leadId);
	
	@Query("select distinct(groupId) from CapTeamMembers ug where ug.userId=:userId)")
	List<Integer> getGroupIdByUserId(@Param("userId") int userId);

	@Query("from CapTeam gou WHERE gou.id IN (select a.groupId from CapTeamMembers a where a.userId=:userId)")
	List<CapTeam> getGroupByUserId(@Param("userId") int userId);
	
	@Query("select ug.userId FROM CapTeamMembers ug WHERE  ug.groupId= :groupId")
	List<Integer> getGroupMembers(@Param("groupId") Integer groupId);
	
	@Query("select u from AccountUser u,UserRole ur, Role r where u.id=ur.user.id and ur.role.id=r.id and r.name in ('CSR','REMOTE_AGENT') and u.id NOT IN( select userId from CapTeamMembers where userId IS NOT NULL)")
	List<AccountUser> getAvailableCSRUser();
	
	@Query(value="select t.user_id, u.first_name ,u.last_name, t.start_date, t.end_date from cap_team_members t inner join (select user_id, max(end_date) as MaxDate from " +
			" cap_team_members where user_id not in(select user_id from cap_team_members where end_date is null) group by user_id) tm on t.user_id = tm.user_id and t.end_date = tm.MaxDate and t.end_date is NOT NULL inner join users u on u.id = t.user_id",nativeQuery = true)
	List<Object[]> getAvailableCSRUsersWithEndDate();

	
	@Query("select t.groupName,t.id,u.firstName,u.lastName, u.id,c.endDate, max(ctl.startDate), c.userId from CapTeam t,AccountUser u,CapTeamMembers c,CapTeamLeaders ctl where t.id = c.groupId and t.groupLeadUserId = u.id and  ctl.userId=t.groupLeadUserId and  ctl.endDate IS NULL and  c.endDate IS NULL and  c.userId= :teamMemberId group by  t.groupName,t.id,u.firstName,u.lastName, u.id,c.endDate, c.userId ")
	List<Object[]> findTeamMemberByUserId(@Param("teamMemberId") Integer teamMemberId);
	
	@Query("select max(c.startDate),max(c.endDate)  from CapTeamMembers c where c.groupId= :teamId")
	List<Object[]> findMaxStartEndDateWithTeamMemberByTeamId(@Param("teamId") Integer teamId);
	
	@Query("select max(ctl.startDate),max(ctl.endDate)  from CapTeamLeaders ctl where ctl.groupId= :teamId")
	List<Object[]> findMaxStartEndDateWithTeamLeaderByTeamId(@Param("teamId") Integer teamId);
	
	@Query("select max(c.startDate) from CapTeam c where c.id= :teamId")
	List<Object[]> getTeamStartDate(@Param("teamId") Integer teamId);

	@Query("select max(c.endDate)  from CapTeamMembers c where c.userId= :teamMemberId")
	Date findMaxEndDateForTeamMemberById(@Param("teamMemberId") Integer teamMemberId);
	
	@Query("select max(c.startDate), max(c.endDate)  from CapTeamMembers c where c.userId= :teamMemberId")
	List<Object[]> findMaxStartEndDateForTeamMemberById(@Param("teamMemberId") Integer teamMemberId);
	
	/**
	 * @author kaul_s
	 * @return void
	 * @param groupId
	 */
	@Transactional
	@Modifying
	@Query("Update CapTeamMembers ctm set ctm.endDate= :endDate WHERE ctm.userId IN (:teamMemberList) and ctm.endDate IS NULL")
	void deleteTeamMemberByEndDate(@Param("teamMemberList") List<Integer> teamMemberList, @Param("endDate") Date endDate);
	
	/**
	 * @author kaul_s
	 * @return void
	 * @param groupId
	 */
	@Transactional
	@Modifying
	@Query("Update CapTeamMembers ctm set ctm.endDate= :endDate WHERE ctm.groupId= :groupId and ctm.endDate IS NULL")
	void deleteByGroupAndEndDate(@Param("groupId") Integer groupId, @Param("endDate") Date endDate);
	
	@Query("select t.groupName,t.id,u.firstName,u.lastName, u.id,c.endDate, max(c.startDate), c.userId  from CapTeam t,AccountUser u,CapTeamMembers c,CapTeamLeaders ctl where t.id = c.groupId and t.groupLeadUserId = u.id and  ctl.userId=t.groupLeadUserId and  ctl.endDate IS NULL and  c.endDate IS NULL and  c.userId IN (:teamMemberList) group by  t.groupName,t.id,u.firstName,u.lastName, u.id,c.endDate, c.userId ")
	List<Object[]> findTeamMembersByUserIds(@Param("teamMemberList") List<Integer> teamMemberList);

	@Query(value="select t.user_id, u.first_name ,u.last_name, t.start_date, t.end_date from cap_team_members t inner join (select user_id, max(end_date) as MaxDate from " +
			" cap_team_members where user_id not in(select user_id from cap_team_members where end_date is null) group by user_id) tm on t.user_id = tm.user_id and t.end_date = tm.MaxDate and t.end_date is NOT NULL and t.tenant_Id =:tenantId inner join users u on u.id = t.user_id ",nativeQuery = true)
	List<Object[]> getAvailableCSRUsersWithEndDate(@Param("tenantId") BigDecimal tenantId);

	@Query(nativeQuery=true,
			value= "select tm.id, (u.first_name || ' ' || u.last_name) as user_name from cap_team_members tm " +
				"inner join users u on u.id = tm.user_id " +
				"where cap_team_id = ( " +
				"select cap_team_id from cap_team_leaders where end_date is null and user_id= :userId) and end_date is null " +
				"order by (u.first_name || ' ' || u.last_name)")
	List<Object[]> agentIdNameByLeadUserId(@Param("userId")Integer userId);
	
	@Query(nativeQuery=true,
	   value= "select ca.id, (u.first_name || ' ' || u.last_name) AS user_name FROM cap_team_members tm "
	     + " inner join cap_agents ca on ca.user_id = tm.user_id INNER JOIN users u ON u.id= tm.user_id "
	     + " and u.id = ca.user_id WHERE cap_team_id in  (SELECT cap_team_id FROM cap_team_leaders WHERE "
	     + " end_date IS NULL AND user_id= :userId ) AND end_date IS NULL ORDER BY (u.first_name || ' ' || u.last_name)")
	 List<Object[]> agentIdNameByLeadUserIdNew(@Param("userId")Integer userId);
	 
	 @Query(nativeQuery=true,
			   value=    "select ca.id, (u.first_name || ' ' || u.last_name) AS user_name FROM  cap_agents ca INNER JOIN users u ON u.id= ca.user_id"
					   + " where ca.status='Active'   ORDER BY (u.first_name || ' ' || u.last_name)")
	 List<Object[]> allActiveAgentIdName();
}
