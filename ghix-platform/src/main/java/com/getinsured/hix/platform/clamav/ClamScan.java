package com.getinsured.hix.platform.clamav;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.timeshift.TimeShifterUtil;

@Component
@Qualifier("clamScan")
public class ClamScan {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClamScan.class);
    public static final int CHUNK_SIZE = 2048;
    private static final byte[] INSTREAM = "zINSTREAM\0".getBytes();
    private int timeout;
    private int port;
    
	
    @Value("#{configProp['clamav.enabled'] != null ? configProp['clamav.enabled']:'false'}")
	private String clamAVEnabled;
	
	@Value("#{configProp['clamav.poolsize'] != null ? configProp['clamav.poolsize'] :'10'}")
	private String poolsize;
	
	
	@Autowired (required=false)
	private ClamConnectionFactory factory;
	
	
	private ClamAVConnectionPool pool;
	
    public ClamScan() {
    	
    }
    
    @PreDestroy
    public void closeAll(){
    	//this.pool.
    	this.pool.clear();
    	this.pool.close();
    }

    @PostConstruct
	public void initialize() throws ContentManagementServiceException {
    	if(this.clamAVEnabled.equalsIgnoreCase("true") && factory == null){
    		throw new ContentManagementServiceException("Clam Connection facory not available, please check the configuration if all Clam AV properties are defined correctly");
    	}
    	GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxIdle(1);
        config.setMaxTotal(Integer.parseInt(poolsize));
        config.setTestOnBorrow(true);
        config.setTestOnReturn(true);
        config.setBlockWhenExhausted(true);
        pool = new ClamAVConnectionPool(factory, config);
    }
    
    public void printPoolStatistics(){
    	//LOGGER.debug("POOL : Active Count:"+pool.getNumActive()+" Idle Count:"+pool.getNumIdle()+" Waiting "+pool.getNumWaiters());
    }

    public String cmd(byte[] cmd) {
        ClamConnection conn = null;
            try {
            	conn = this.pool.borrowObject();
            	LOGGER.debug("Valid after borrow:"+conn.validate());
                conn.write(cmd);
                return conn.readString();
            } catch (IOException e) {
                LOGGER.error("error writing " + new String(cmd) + " command", e);
                return null;
            } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				this.pool.returnObject(conn);
			}
            return null;
    }

    /**
     * The method to call if you already have the content to scan in-memory as a byte array.
     *
     * @param in the byte array to scan
     * @return the result of the scan
     * @throws IOException
     */
    public ScanResult scan(byte[] in)  {
        return scan(new ByteArrayInputStream(in));
    }

    /**
     * The preferred method to call. This streams the contents of the InputStream to clamd, so
     * the entire content is not loaded into memory at the same time.
     *
     * @param in the InputStream to read.  The stream is NOT closed by this method.
     * @return a ScanResult representing the server response
     */
    public ScanResult scan(InputStream in) {
    	return scan(in,1);
    }
    
    public ScanResult scan(InputStream in, int attempts) {
    	Exception exception = null;
        ClamConnection conn = null;
        String response = "";
        long start = TimeShifterUtil.currentTimeMillis();
        try {  // finally to close resources
        	conn = this.pool.borrowObject(10000);
            int read = CHUNK_SIZE;
            byte[] buffer = new byte[CHUNK_SIZE];
            conn.write(INSTREAM);
            while (read == CHUNK_SIZE) {
                read = in.read(buffer);
                if (read > 0) { // if previous read exhausted the stream
                        conn.writeInt(read);
                        conn.write(buffer, 0, read);
                }
            }
	        conn.writeInt(0);
	        response = conn.readString();
        }catch(Exception ex){
        	exception = ex;
        }finally {
        	if(exception != null){
        		LOGGER.error("Error ["+exception.getMessage()+"] scanning the data");
        		conn.finish(start);
        		conn.close();
        	}
        	this.pool.returnObject(conn);
        }
        
        // Reaching here means we are done with max 3 attempts
        if(exception != null){
        	LOGGER.error("Error scanning data, number of attempts "+attempts+" giving up now",exception);
        	return new ScanResult(exception);
        }
        if (LOGGER.isDebugEnabled()){
        	LOGGER.debug("Response: " + response);
        }
        return new ScanResult(response.trim());
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    /**
     * Socket timeout in milliseconds
     *
     * @param timeout socket timeout in milliseconds
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
    
    public static void main(String args[]) throws ContentManagementServiceException, InterruptedException{
    	ClamScan clam = new ClamScan();
    	ClamConnectionFactory factory = new ClamConnectionFactory();
    	factory.setClamAVEnabled("true");
    	factory.setHost("sfo-av1.ghixqa.com");
    	factory.setClamAVTimeout("20000");
    	factory.setClamAVPort("3310");
    	clam.factory = factory;
    	clam.clamAVEnabled="true";
    	clam.poolsize="10";
    	clam.initialize();
    	for(;;){
    		
	    	for(int i = 0; i < 5; i++){
	    		
	    		Thread clamClient = new Thread(new Runnable(){
					@Override
					public void run() {
						java.io.InputStream f;
						try {
							//clam.printPoolStatistics();
							f = new java.io.FileInputStream(new java.io.File("/Users/chaudhary_a/work/xmldata.xml"));
							ScanResult result = clam.scan(f);
							System.out.println("Result:"+result.getStatus());
							//LOGGER.debug("result : " + result.getResult());
						} catch (java.io.FileNotFoundException e) {
							e.printStackTrace();
						} 
						
					}
	    			
	    		});
	    		clamClient.setName("Client:"+i);
	    		clamClient.start();
	    		//clamClient.join(); // Activating this call will make a single object used from the pol
	    	}
	    	for(int i = 0; i < 6; i++){
		    	try {
					Thread.sleep(60*1000);
					Thread.yield();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    	}
    	}
    	//clam.closeAll();
    	
    	
    }
}
