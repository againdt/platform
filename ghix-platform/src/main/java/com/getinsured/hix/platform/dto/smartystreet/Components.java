package com.getinsured.hix.platform.dto.smartystreet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Sunil Desu
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Components {

	private String primary_number;
	private String street_name;
	private String street_predirection;
	private String street_postdirection;
	private String street_suffix;
	private String secondary_number;
	private String secondary_designator;
	private String extra_secondary_number;
	private String extra_secondary_designator;
	private String pmb_designator;
	private String pmb_number;
	private String city_name;
	private String default_city_name;
	private String state_abbreviation;
	private String zipcode;
	private String plus4_code;
	private String delivery_point;
	private String delivery_point_check_digit;

	public String getPrimary_number() {
		return primary_number;
	}

	public void setPrimary_number(String primary_number) {
		this.primary_number = primary_number;
	}

	public String getStreet_name() {
		return street_name;
	}

	public void setStreet_name(String street_name) {
		this.street_name = street_name;
	}

	public String getStreet_predirection() {
		return street_predirection;
	}

	public void setStreet_predirection(String street_predirection) {
		this.street_predirection = street_predirection;
	}

	public String getStreet_postdirection() {
		return street_postdirection;
	}

	public void setStreet_postdirection(String street_postdirection) {
		this.street_postdirection = street_postdirection;
	}

	public String getStreet_suffix() {
		return street_suffix;
	}

	public void setStreet_suffix(String street_suffix) {
		this.street_suffix = street_suffix;
	}

	public String getSecondary_number() {
		return secondary_number;
	}

	public void setSecondary_number(String secondary_number) {
		this.secondary_number = secondary_number;
	}

	public String getSecondary_designator() {
		return secondary_designator;
	}

	public void setSecondary_designator(String secondary_designator) {
		this.secondary_designator = secondary_designator;
	}

	public String getExtra_secondary_number() {
		return extra_secondary_number;
	}

	public void setExtra_secondary_number(String extra_secondary_number) {
		this.extra_secondary_number = extra_secondary_number;
	}

	public String getExtra_secondary_designator() {
		return extra_secondary_designator;
	}

	public void setExtra_secondary_designator(String extra_secondary_designator) {
		this.extra_secondary_designator = extra_secondary_designator;
	}

	public String getPmb_designator() {
		return pmb_designator;
	}

	public void setPmb_designator(String pmb_designator) {
		this.pmb_designator = pmb_designator;
	}

	public String getPmb_number() {
		return pmb_number;
	}

	public void setPmb_number(String pmb_number) {
		this.pmb_number = pmb_number;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getDefault_city_name() {
		return default_city_name;
	}

	public void setDefault_city_name(String default_city_name) {
		this.default_city_name = default_city_name;
	}

	public String getState_abbreviation() {
		return state_abbreviation;
	}

	public void setState_abbreviation(String state_abbreviation) {
		this.state_abbreviation = state_abbreviation;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPlus4_code() {
		return plus4_code;
	}

	public void setPlus4_code(String plus4_code) {
		this.plus4_code = plus4_code;
	}

	public String getDelivery_point() {
		return delivery_point;
	}

	public void setDelivery_point(String delivery_point) {
		this.delivery_point = delivery_point;
	}

	public String getDelivery_point_check_digit() {
		return delivery_point_check_digit;
	}

	public void setDelivery_point_check_digit(String delivery_point_check_digit) {
		this.delivery_point_check_digit = delivery_point_check_digit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Components [primary_number=");
		builder.append(primary_number);
		builder.append(", street_name=");
		builder.append(street_name);
		builder.append(", street_predirection=");
		builder.append(street_predirection);
		builder.append(", street_postdirection=");
		builder.append(street_postdirection);
		builder.append(", street_suffix=");
		builder.append(street_suffix);
		builder.append(", secondary_number=");
		builder.append(secondary_number);
		builder.append(", secondary_designator=");
		builder.append(secondary_designator);
		builder.append(", extra_secondary_number=");
		builder.append(extra_secondary_number);
		builder.append(", extra_secondary_designator=");
		builder.append(extra_secondary_designator);
		builder.append(", pmb_designator=");
		builder.append(pmb_designator);
		builder.append(", pmb_number=");
		builder.append(pmb_number);
		builder.append(", city_name=");
		builder.append(city_name);
		builder.append(", default_city_name=");
		builder.append(default_city_name);
		builder.append(", state_abbreviation=");
		builder.append(state_abbreviation);
		builder.append(", zipcode=");
		builder.append(zipcode);
		builder.append(", plus4_code=");
		builder.append(plus4_code);
		builder.append(", delivery_point=");
		builder.append(delivery_point);
		builder.append(", delivery_point_check_digit=");
		builder.append(delivery_point_check_digit);
		builder.append("]");
		return builder.toString();
	}

}
