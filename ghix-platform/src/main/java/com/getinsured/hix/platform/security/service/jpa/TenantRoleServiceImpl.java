package com.getinsured.hix.platform.security.service.jpa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ActiveFlag;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.TenantRole;
import com.getinsured.hix.model.TenantRoleDTO;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.ITenantRoleRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.security.service.TenantRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Service(value="tenantRoleService")
public class TenantRoleServiceImpl implements TenantRoleService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TenantRoleServiceImpl.class);

	@Autowired
	private EntityManagerFactory emf;
	
	@Autowired
	private ITenantRoleRepository iTenantRoleRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private static final int SUB_LIST_SIZE = 500;
	private static final int ACTIVE_USER_CONFIRMED_FLAG = 1;
	private static final int INACTIVE_USER_CONFIRMED_FLAG = 0;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GIRuntimeException.class)
	public void addTenantRoles(Long tenantId) {
		if(tenantId != null){
			EntityManager em = null;
			String queryStr = "INSERT "+
					" INTO TENANT_ROLES "+
					" ( "+
					"  ID, "+
					"  ROLE_ID, "+
					"  TENANT_ID, "+
					"  CREATION_TIMESTAMP, "+
					"  LAST_UPDATE_TIMESTAMP, "+
					"  IS_ACTIVE "+
					" ) "+
					" (SELECT "+
					"  TENANT_ROLES_SEQ.NEXTVAL, "+
					"  r.id,t.id,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP, "+
					"  (case when t.name=\'GetInsured\' then \'Y\' else r.IS_TENANT_DEFAULT end)  "+
					"  FROM tenant t, roles r "+
					"  where t.id = :tenantId )";

			Query query = null;


			try{
				em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}

				query = em.createNativeQuery(queryStr);
				query.setParameter("tenantId",tenantId);
				
				int count = query.executeUpdate();
				
				LOGGER.info("Number of records inserted in TENANT_ROLES: "+count);

				em.getTransaction().commit();
			}catch(Exception e){
				LOGGER.error("Exception occured while inserting records in TENANT_ROLES table");
				if(em.getTransaction().isActive()){
					em.getTransaction().rollback();
				}
				throw new GIRuntimeException(e);
			}finally{
				if(em != null && em.isOpen()){
					em.close();
				}
			}
		}

	}

	@Override
	public String getTenantRoles(String tenantName) {

		List<Object[]> resultList = iTenantRoleRepository.findTenantRoleMapping(tenantName);
		List<TenantRoleDTO> tenantRoleDTOs = null;
		TenantRoleDTO tenantRoleDTO = null;
		String response = null;
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		if(resultList != null){
			tenantRoleDTOs = new ArrayList<TenantRoleDTO>();
			for(Object[] resultArr : resultList){
				tenantRoleDTO = new TenantRoleDTO();
				tenantRoleDTO.setRoleName(resultArr[0] != null ? resultArr[0].toString() : null);
				tenantRoleDTO.setTenantDefault(ActiveFlag.Y.toString().
								equals(resultArr[1] != null ? resultArr[1].toString(): null) 
								? true : false);
				tenantRoleDTO.setActive(ActiveFlag.Y.toString().
						equals(resultArr[2] != null ? resultArr[2].toString(): null) 
						? true : false);
				tenantRoleDTO.setTenantRoleId(ghixJasyptEncrytorUtil.encryptStringByJasypt(resultArr[3] != null ? resultArr[3].toString() : null));
				tenantRoleDTOs.add(tenantRoleDTO);
			}
			response = gson.toJson(tenantRoleDTOs);
		}
		
//		List<TenantRoleDTO> tenantRoleDTOs = null;
//		TenantRoleDTO tenantRoleDTO = null;
//		String response = null;
//		List<TenantRole> tenantRoles = iTenantRoleRepository.findByTenantName(tenantName);
//		tenantRoleDTOs = new ArrayList<TenantRoleDTO>();
//		for(TenantRole tenantRole : tenantRoles){
//			tenantRoleDTO = new TenantRoleDTO();
//			tenantRoleDTO.setRoleName(tenantRole.getRole().getName());
//			tenantRoleDTO.setTenantDefault(ActiveFlag.Y.equals(tenantRole.getIsActive()) ? true : false);
//			tenantRoleDTO.setActive(ActiveFlag.Y.equals(tenantRole.getIsActive()) ? true : false); 
//			tenantRoleDTOs.add(tenantRoleDTO);
//		}
//		response = new Gson().toJson(tenantRoleDTOs);
		
		return response;
	}

	private AccountUser getLoggedInUser() {
		AccountUser loggedInUser = null;
		try {
			loggedInUser = userService.getLoggedInUser();
		} catch (InvalidUserException e) {
			LOGGER.error("Invalid user",e);
		}

		return loggedInUser;
	}
	

	private static <T> List<List<T>> getSubList(List<T> list, final int subListSize){
		List<List<T>> subLists = new ArrayList<List<T>>();
		final int N = list.size();
		for (int i = 0; i < N; i += subListSize) {
			subLists.add(new ArrayList<T>(list.subList(i,Math.min(N, i + subListSize))));
		}
		
		return subLists;
	}
	
	
	
	@Override
	@Transactional
	public String updateTenantRolesMapping(List<TenantRoleDTO> tenantRolesDTOs) {

		List<Long> tenantRolesIdList = new ArrayList<Long>();
		Map<String,Boolean> tenantRoleMap = new HashMap<String,Boolean> ();


		Set<String> activatedRolesSet = new HashSet<String>();
		Set<String> deactivatedRolesSet =  new HashSet<String>();
		AccountUser loggedInUser = getLoggedInUser();

		if(loggedInUser !=null && userService.hasAccessToUpdateUser(loggedInUser) && !tenantRolesDTOs.isEmpty()){
			Long tenantId = tenantRolesDTOs.get(0).tenantId;

			for(TenantRoleDTO tenantRolesDTO : tenantRolesDTOs){
				if(tenantRolesDTO.getTenantRoleId() != null) {
					String tenantRoleId = ghixJasyptEncrytorUtil.decryptStringByJasypt(tenantRolesDTO.getTenantRoleId());
					LOGGER.info("tenantRoleId: "+tenantRoleId);
					tenantRolesIdList.add(Long.valueOf(tenantRoleId));
					tenantRoleMap.put(tenantRoleId, tenantRolesDTO.isActive());

					if(tenantRolesDTO.isActive()){
						activatedRolesSet.add(tenantRolesDTO.getRoleName());
					}else{
						deactivatedRolesSet.add(tenantRolesDTO.getRoleName());
					}
				}
			}

			if(activatedRolesSet != null && !activatedRolesSet.isEmpty()){

				List<Integer> activatedUserIds = userService.getAccountUserIdsForRoleNames(new ArrayList<String>(activatedRolesSet), tenantId);
				updateUserStatusAndConfirmedFields(activatedUserIds,AccountUser.user_status.Activate.toString(), ACTIVE_USER_CONFIRMED_FLAG);
				
			}
			if(deactivatedRolesSet !=null && !deactivatedRolesSet.isEmpty()){
				List<Integer> deactivatedUserIds = userService.getAccountUserIdsForRoleNames(new ArrayList<String>(deactivatedRolesSet), tenantId);
				updateUserStatusAndConfirmedFields(deactivatedUserIds,AccountUser.user_status.Deactivate.toString(), INACTIVE_USER_CONFIRMED_FLAG);
			}


			List<TenantRole> tenantRoles = iTenantRoleRepository.findByIdIn(tenantRolesIdList);

			for(TenantRole tenantRole : tenantRoles){
				if(tenantRoleMap.get(String.valueOf(tenantRole.getId())) !=null){
					ActiveFlag isRoleActive = tenantRoleMap.get(
							String.valueOf(tenantRole.getId())) 
							? ActiveFlag.Y : ActiveFlag.N;

					tenantRole.setIsActive(isRoleActive);
				}
				iTenantRoleRepository.save(tenantRole);
			}
			return "SUCCESS";
		} else{
			return "FAILURE";
		}

	}
	
	private void updateUserStatusAndConfirmedFields(List<Integer> userIds,String status, int confirmed){
		if (userIds != null && !userIds.isEmpty()) {
			List<List<Integer>> usersSubList = getSubList(userIds,SUB_LIST_SIZE);
			for(List<Integer> userIdList  : usersSubList){
				LOGGER.info("Size of activated userIds subList : "+userIdList.size());
				userService.updateUserStatusAndConfirmedFields(userIdList,status,confirmed);
			}

		}
	}

	@Override
	public void addOpsadminUserForTenant(Long tenantId) {
		
		if(tenantId != null){
			EntityManager em = null;
			String uuid = UUID.randomUUID().toString();
			String insertUserSql = 	"Insert into USERS (ID,COMMUNICATION_PREF,CONFIRMED,CREATION_TIMESTAMP,EMAIL,FIRST_NAME,LAST_NAME,PASSWORD,RECOVERY,TITLE,LAST_UPDATE_TIMESTAMP,USERNAME,PHONE,SECURITY_QUESTION_1, "
					+ "SECURITY_ANSWER_1,SECURITY_QUESTION_2,SECURITY_ANSWER_2,SECURITY_QUESTION_3,SECURITY_ANSWER_3,PIN,PIN_EXPIRATION,PASS_RECOVERY_TOKEN,PASS_RECOVERY_TOKEN_EXPIRATION, "
					+ "FFM_USER_ID,USER_NPN,STATUS,STARTDATE,ENDDATE,ISSUPERUSER,RETRY_COUNT,SEC_QUE_RETRY_COUNT,PWD_LAST_UPDATED,UUID,LASTLOGIN,EXTN_APP_USERID,LAST_UPDATED_BY,TENANT_ID) "
					+ "values "
					+ "(USERS_SEQ.NEXTVAL,'Email',1,SYSTIMESTAMP,'opsadmin@getinsured.com','Operations','Admin','$2a$10$I.0Vmi4.6CcQRkog81L4I.NyKMxX0vTDEPkJPFbDCYOTOp9KBFKUa', '"+uuid+"',null,SYSTIMESTAMP,'opsadmin@getinsured.com','4084621703','What was your childhood nickname?',"
					+ "'answer_1','what school did you attend for sixth grade?','answer_2','In what city does your nearest sibling live?','answer_3',null,null,null,null,"
					+ "null,null,'Active',null,null,1,0,0,SYSTIMESTAMP,'ccbebb61-89c3-4e85-8869-fd1ccf1c6a9d',SYSTIMESTAMP,null,null,:tenantId)";
			
			String insertUserRolesSql = "Insert Into User_Roles(Id,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,Role_Id,User_Id,Default_Role ) "
					+ "Select User_Roles_Seq.Nextval, Systimestamp, Systimestamp, Roles.Id, "
					+ "(SELECT ID from USERS where username= 'opsadmin@getinsured.com' and TENANT_ID = :tenantId), 'Y' "
					+ "From Roles, Users "
					+ "Where Roles.Name = 'OPERATIONS_ADMIN' And Users.Username = 'opsadmin@getinsured.com' And Users.TENANT_ID = :userTenantId";
			
			Query query = null;


			try{
				em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}

				query = em.createNativeQuery(insertUserSql);
				query.setParameter("tenantId",tenantId);
				
				int count = query.executeUpdate();
				LOGGER.info("Number of records inserted in USERS: " + count);
				
				query = em.createNativeQuery(insertUserRolesSql);
				query.setParameter("tenantId",tenantId);
				query.setParameter("userTenantId",tenantId);
				count = query.executeUpdate();
				LOGGER.info("Number of records inserted in user_roles: " + count);

				em.getTransaction().commit();
			}catch(Exception e){
				LOGGER.error("Exception occured while inserting records in users, user_roles tenant_roles tables", e);
				if(em.getTransaction().isActive()){
					em.getTransaction().rollback();
				}
				throw new GIRuntimeException(e);
			}finally{
				if(em != null && em.isOpen()){
					em.close();
				}
			}
		}
	}
}
