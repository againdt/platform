package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.QueryData;

public interface IQueryDataRepository extends JpaRepository<QueryData, Integer> {
	
	public QueryData findByName(String queryName);
	
}
