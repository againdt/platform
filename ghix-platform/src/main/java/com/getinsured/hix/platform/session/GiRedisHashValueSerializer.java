package com.getinsured.hix.platform.session;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.redis.serializer.RedisSerializer;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.session.UnitTestGiRedisHashValueSerializer.User;
import com.getinsured.hix.platform.util.GhixGson;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.google.gson.Gson;

/**
 * Custom serializer and deserializer that should override Spring Redis' default algorithms.
 * <p>
 *   This class provides ability to try different deserialization strategies, such as:
 *   <ul>
 *     <li>Java Deserialization</li>
 *     <li>JSON Deserialization</li>
 *   </ul>
 * </p>
 */
public class GiRedisHashValueSerializer implements RedisSerializer<Object> {
	private static SecureRandom random = new SecureRandom();
	private Converter<Object, byte[]> serializer = new SerializingConverter();
	private Converter<byte[], Object> deserializer = new DeserializingConverter();
	private static Logger log = LoggerFactory.getLogger(GiRedisHashValueSerializer.class);

	static final byte[] EMPTY_ARRAY = new byte[0];
	public final int JSON_SERIALIZE = 1000;
	public final int JAVA_SERIALIZE = 2000;

	public Object deserialize(byte[] bytes) {
		if (isEmpty(bytes)) {
			return null;
		}
		try {
			return deserializer.convert(bytes);
		} catch (Exception ex) {

			if(log.isErrorEnabled())
			{
				log.error("Failed to de-serialize the object, dumped the data, now attemting to de-serialize using JSON");
			}

			Object obj = readObject(bytes);
			if(obj instanceof Exception){ // We failed reading using GhixGson object as well
				if(log.isErrorEnabled())
				{
					log.error("Failed to de-serialize the object using native serializationy dump enable? = {}", GhixPlatformConstants.SESSION_DEBUG, ex);
					log.error("Attempt to de-serialize the object using JSON serialization = {}", GhixPlatformConstants.SESSION_DEBUG, (Exception) obj);
				}

				if(GhixPlatformConstants.SESSION_DEBUG){
					dumpBinary(bytes);
				}
                
				//throw new GIRuntimeException("De-serialization failed refer earlier errors reported");
			}
			return obj;
		}
	}

	private void dumpBinary(byte[] data){
		File f = new File("De_SerializationObject_"+random.nextLong()+".obj");

		if(log.isErrorEnabled())
		{
			log.error("File Path: {}", f.getAbsolutePath());
		}

		try {
			FileOutputStream fos = new FileOutputStream(f);
			//fos.write(("File Path:"+f.getAbsolutePath()+"\n").getBytes());
			fos.write(data);
			fos.flush();
			fos.close();
		} catch (Exception e) {
			if(log.isErrorEnabled())
			{
				log.error("Exception occurred while writing binary file: {}", f.getAbsolutePath(), e);
			}
		}
	}
	
	private byte[] appendIdentifier(byte[] objBuf, int identifier, Class<?> T){
		String clsName = T.getName();
		byte[] clsNameBytes = clsName.getBytes();
		byte[] identifierBytes = ByteBuffer.allocate(Integer.BYTES).putInt(identifier).array();
		byte[] type = ByteBuffer.allocate(clsNameBytes.length).put(clsNameBytes).array();
		byte[] typeLen = ByteBuffer.allocate(Integer.BYTES).putInt(type.length).array();
		byte[] objLen = ByteBuffer.allocate(Integer.BYTES).putInt(objBuf.length).array();
		
		byte[] out = new byte[Integer.BYTES+Integer.BYTES+type.length+Integer.BYTES+objBuf.length];
		int offset = 0;
		System.arraycopy(identifierBytes, 0, out, offset, Integer.BYTES);
		offset += Integer.BYTES;
		System.arraycopy(typeLen, 0, out,offset, Integer.BYTES);
		offset += Integer.BYTES;
		System.arraycopy(type, 0, out,offset, type.length);
		offset += type.length;
		System.arraycopy(objLen, 0, out,offset, Integer.BYTES);
		offset += Integer.BYTES;
		System.arraycopy(objBuf, 0, out,offset, objBuf.length);
		return out;
	}
	
	private Object readObject(byte[] objBuf) {
		Exception ex = null;
		Object object = null;
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		try{
			Class<?> typeClass = null;
			ByteBuffer buf = ByteBuffer.wrap(objBuf, 0, objBuf.length);
			int identifier = buf.getInt();
			int typeLen = buf.getInt();
			byte[] type = new byte[typeLen];
			buf.get(type, 0, typeLen).array();
			String clsName = new String(type);
			if(log.isDebugEnabled()){
				log.debug("Attemting to read the type: {}", clsName);
			}
			typeClass = Class.forName(clsName);
			int objectBytes = buf.getInt();
			byte[] objectByteBuffer = new byte[objectBytes];
			buf.get(objectByteBuffer, 0, objectBytes);
			object = deserializer.convert(objectByteBuffer);
			switch(identifier){
				case JSON_SERIALIZE:{
					String jsonString  = (String)object;
					return gson.fromJson(jsonString, typeClass);
				}
				case JAVA_SERIALIZE:{
					return object;
				}
			}
		}catch(Exception e){
			ex = e;
		}
		if(ex != null){
			return ex;
		}
		return object;
	}
	
	public byte[] serialize(Object object) {
		if (object == null) {
			return EMPTY_ARRAY;
		}
		if(!(object instanceof GhixGson)){
			return serializer.convert(object);
		}
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		Object toSerialize = object;
		Class<?> clsName = object.getClass();
		if(log.isDebugEnabled()){
			log.debug("**Serializing** : {}", toSerialize.getClass().getName());
		}
		clsName = toSerialize.getClass();

		if(log.isDebugEnabled())
		{
			log.debug("**JSON Serializationn** : {}", clsName);
		}

		int identifier = JSON_SERIALIZE;
		toSerialize = gson.toJson(toSerialize, toSerialize.getClass());
		return appendIdentifier(serializer.convert(toSerialize),identifier, clsName);
	}

	private boolean isEmpty(byte[] data) {
		return (data == null || data.length == 0);
	}

	public static void main(String[] args){
		UnitTestGiRedisHashValueSerializer.User tom = new User();
		tom.setName("Tom");
		tom.setAge(29);
		GiRedisHashValueSerializer serializer = new GiRedisHashValueSerializer();
		byte[] tomBytes = serializer.serialize(tom);
		UnitTestGiRedisHashValueSerializer.User cloneTom = (User) serializer.deserialize(tomBytes);

		if(log.isDebugEnabled())
		{
			log.debug("Name: {} Age: {} ", cloneTom.getName(), cloneTom.getAge());
		}
	}
}

class UnitTestGiRedisHashValueSerializer{
	//static class User implements GhixGson{
	static class User implements Serializable{
		String name;
		int age;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
	}
}