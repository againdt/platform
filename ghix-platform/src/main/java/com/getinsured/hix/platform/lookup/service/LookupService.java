package com.getinsured.hix.platform.lookup.service;

import java.util.List;

import com.getinsured.hix.model.LookupValue;

/**
 * @author ajinkya m
 * 
 */
public interface LookupService {

	List<LookupValue> getLookupValueList(String typeName);
	
//	List<LookupValue> getLookupValueListForAeeStatus(String typeName);
	
	List<LookupValue> getLookupValueListEligibiltyStatus(String typeName);

	List<LookupValue> getLookupValueListForCountiesServed(String typeName);
	
	String getLookupValueLabel(String typeName, String lookup_value_code);

	String getLookupValueCode(String typeName, String lookup_value_label);

	List<LookupValue> getDependentLookupValueList(String typeName,
			String parent_lookup_value_code);
	
	Integer getlookupValueIdByTypeANDLookupValueCode(String name,String lookupValueCode);
	
	LookupValue findLookupValuebyId(int id);
	
	LookupValue getlookupValueByTypeANDLookupValueCode(String type,String lookupValueCode);
	
	LookupValue getlookupValueByTypeANDLookupValueLabel(String type,String lookupValueLabel);
	
	/**
	 * Method 'populateLanguageNames' to populate languages from Lookup table.
	 *
	 * @author chalse_v 
	 * 
	 * @param lookUpTerm The string term to lookup.
	 * @return languageList Collection of languages.
	 */		
	List<String> populateLanguageNames(String lookUpTerm);
	
	List<LookupValue> getLookupValueListForHoursOfOperation(String name);
	
	List<String> getLookupValueLabelList(String name);

List<LookupValue> getLookupValueListForEnrollmentStatusEdit();

	

List<LookupValue> getLookupValueList(String typeName, String language);

//List<LookupValue> getLookupValueListForAeeStatus(String typeName, String language);

List<LookupValue> getLookupValueListEligibiltyStatus(String typeName, String language);

List<LookupValue> getLookupValueListForCountiesServed(String typeName, String language);

String getLookupValueLabel(String typeName, String lookup_value_code, String language);

//String getLookupValueCode(String typeName, String lookup_value_label, String language);

List<LookupValue> getDependentLookupValueList(String typeName,
		String parent_lookup_value_code, String language);

Integer getlookupValueIdByTypeANDLookupValueCode(String name,String lookupValueCode, String language);


LookupValue getlookupValueByTypeANDLookupValueCode(String type,String lookupValueCode , String language);

LookupValue getlookupValueByTypeANDLookupValueLabel(String type,String lookupValueLabel, String language);

/**
 * Method 'populateLanguageNames' to populate languages from Lookup table.
 *
 * @author chalse_v 
 * 
 * @param lookUpTerm The string term to lookup.
 * @return languageList Collection of languages.
 */		
List<String> populateLanguageNames(String lookUpTerm, String language);

List<LookupValue> getLookupValueListForHoursOfOperation(String name, String language);

List<String> getLookupValueLabelList(String name, String language);

List<LookupValue> getLookupValueListForEnrollmentStatusEdit(String language);

List<LookupValue> getLookupValueListForBOBFeed(String typeName,	List<String> lookupValueCode);

List<LookupValue> getLookupValueListForAppEvents(String typeName);

/**
 * Method to Get LOOKUP_VALUE by LOOKUP_VALUE_CODE, PARENT_LOOKUP_CODE & PARENT_LOOKUP_TYPE
 * Jira Id: HIX-79052
 * @since 03rd November 2015
 * @param lookupValueCode String
 * @param parentLookupCode String
 * @param parentLookupTypeName String
 * @return
 */
LookupValue getlookupValueBylookupCodeAndParentLookupCode(String lookupValueCode, String parentLookupCode, String parentLookupTypeName);

}
