/**
 * 
 */
package com.getinsured.hix.platform.util;

import java.beans.Encoder;
import java.beans.Expression;
import java.beans.PersistenceDelegate;
import java.beans.XMLEncoder;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author panda_p
 *
 */
public class GhixXMLEncoder extends XMLEncoder {
	
	/**
	 * 
	 */
	public GhixXMLEncoder(OutputStream out) {
		super(out);
		//this.out=out;
		this.setPersistenceDelegate();
	}

	
	public void setPersistenceDelegate() {
		Encoder encoder = new Encoder();
		encoder.setPersistenceDelegate(Timestamp.class, new PersistenceDelegate()
		    {
		        protected Expression instantiate( Object oldInstance, Encoder out )
		        {
		            Date date = ( Date )oldInstance;
		            Long time = Long.valueOf( date.getTime() );
		            return new Expression( date, date.getClass(), "new", new Object[]{time} );
		        }
		    } );
		
		encoder.setPersistenceDelegate(java.sql.Date.class, new PersistenceDelegate()
        {
            protected Expression instantiate( Object oldInstance, Encoder out )
            {
                java.sql.Date date = ( java.sql.Date )oldInstance;
                Long time = Long.valueOf( date.getTime() );
                return new Expression( date, date.getClass(), "new", new Object[]{time} );
            }
        } );
	}

}
