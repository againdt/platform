package com.getinsured.hix.platform.config;

import java.util.Set;

public interface ConfigurationChangeListener {
	public Set<String> getSubscribedConfigurationList();
	public void handleConfigurationChange(String name, String currentProperty);
}
