package com.getinsured.hix.platform.couchbase.helper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("nonBinaryCouchSupport")
public class NonBinaryCouchSupport extends BaseCouchSupportClass {

}
