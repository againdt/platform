package com.getinsured.hix.platform.util.datalisting;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.util.StringUtils;

//import org.hibernate.tool.hbm2x.StringUtils;

public class Paginator extends TagSupport {
	private static final long serialVersionUID = 1L;
 
	private int pageSize = 10;
	private int resultSize;
	private boolean firstAndLastView = false;
	private boolean hideTotal = false;
	private boolean showDynamicPageSize = false;
	
    private HttpServletRequest request;
    private JspWriter out;
    private String customFunctionName;
   
    private String getURL(int pageNumber) throws UnsupportedEncodingException{
    	StringBuilder url = new StringBuilder(this.request.getAttribute("javax.servlet.forward.request_uri").toString()).append("?");
    	
    	url.append("pageNumber="+pageNumber+"&pageSize="+pageSize+"&");
    	Enumeration<?> keys = request.getParameterNames(); 
    	while (keys.hasMoreElements() ) {  
  	      	String key   = (String)keys.nextElement();  
  	      	String value = (request.getParameter(key) == null) ? "" : request.getParameter(key);
  	      	
  	      	if(key.equals("pageNumber") || key.equals("pageSize") || value.equals("") ){
  	      		continue;
  	      	}
  	      	
  	      	if(key.equals("csrftoken")){
  	      		key="";
  	      		value="";
  	      	}
  	      	else{
      		url.append(URLEncoder.encode(key, "UTF-8"));
      		url.append("=");
      		url.append(URLEncoder.encode(value, "UTF-8")+"&");
  	      	}
    	}
    	url = url.deleteCharAt( url.lastIndexOf( "&" ) );
    	return url.toString();
    }
    
    private void setProperties(){
    	this.request = (HttpServletRequest)pageContext.getRequest();
    	this.out = pageContext.getOut();
    }
	
    private String createPagination() throws UnsupportedEncodingException{
    	int currPage = (this.request.getParameter("pageNumber") == null )||(StringUtils.isEmpty(this.request.getParameter("pageNumber"))) ? 1 : Integer.parseInt(this.request.getParameter("pageNumber"));
    	int noOfPage = ( (resultSize % pageSize) > 0 ) ? (resultSize/pageSize)+1 : (resultSize/pageSize);
    	int startpage = ((currPage/10)*10)+1;
		if((currPage%10) == 0)
		{
			startpage -=10;
		}
		int endPage=(noOfPage < (startpage+9))? noOfPage : (startpage+9);
    	StringBuilder pagination = new StringBuilder(" <div class='pagination'> <ul> ");
		if(noOfPage > 1) {
			if(startpage > 10){
				pagination.append( "<li></li>" );
				 if(customFunctionName != null && !customFunctionName.isEmpty()) {
                     pagination.append( "<li><a href='javascript:"+customFunctionName+"(\""+ this.getURL(startpage-1) +"\")'>Prev</a></li>" );
	             } else {
	                     pagination.append( "<li><a href='"+ this.getURL(startpage-1) +"'>Prev</a></li>" );
	             }
    		}
    		for(int i=startpage;i <= endPage; i++) {
    			pagination.append( "<li class='"+ ((currPage == i) ? "active" : "") +"'>" );
    			if(customFunctionName != null && !customFunctionName.isEmpty()) {
                    pagination.append( "<a href='javascript:"+customFunctionName+"(\""+ this.getURL(i) +"\")'>"+ i +"</a></li>" );
                } else {
                            pagination.append( "<a href='"+ this.getURL(i) +"'>"+ i +"</a></li>" );
                }
    			pagination.append( "<li></li>" );
    		}
			if(endPage < noOfPage){
				if(customFunctionName != null && !customFunctionName.isEmpty()) {
                    pagination.append( "<li><a href='javascript:"+customFunctionName+"(\""+ this.getURL(endPage+1) +"\")' data-label='last page'>Next</a></li>" );
	            } else {
	                    pagination.append( "<li><a href='"+ this.getURL(endPage+1) +"' data-label='last page'>Next</a></li>" );
	            }
    		}
    	}
    	/*if(noOfPage > 1) {
    		if(firstAndLastView && currPage > 1){
    			pagination.append( "<li><a href='"+ this.getURL(1) +"'>First</a></li>" );
    		}
    		if(currPage > 1){
    			pagination.append( "<li><a href='"+ this.getURL(currPage-1) +"'>Prev</a></li>" );
    		}
    		for(int i=1;i <= noOfPage; i++) {
    			pagination.append( "<li class='"+ ((currPage == i) ? "active" : "") +"'>" );
    			pagination.append( "<a href='"+ this.getURL(i) +"'>"+ i +"</a></li>" );
    		}
    		if(currPage < noOfPage){
    			pagination.append( "<li><a href='"+ this.getURL(currPage+1) +"'>Next</a></li>" );
    		}
    		if(firstAndLastView && currPage < noOfPage){
    			pagination.append( "<li><a href='"+ this.getURL(noOfPage) +"'>Last</a></li>" );
    		}
    	}*/
    	if(!hideTotal) {
    		pagination.append( "<li>&nbsp;&nbsp;[ Total : "+ resultSize +" ]</li>" );
    	}
    	pagination.append( "</ul>" );
    	
    	if( showDynamicPageSize ) {
    		int[] options = {10, 15, 20, 25};
    		String defaultSelected = "";
    		String onChange = "";
    		String dynamicUrl = this.getURL(1);
    		
    		if(customFunctionName != null && !customFunctionName.isEmpty()) {
    			onChange = " onChange='"+customFunctionName+"(this.value)' ";
            } else {
            	onChange = " onChange='window.location=this.value'";
            }
    		pagination.append( "<span name='dynamicPageSizeSpan' class='pull-right' id='dynamicPageSizeSpan'>");
	    	pagination.append( "Display Rows:<select class='input-mini' name='pageSizeSelect' id='pageSizeSelect' "+ onChange +">" );
	    	for (int i = 0; i < options.length; i++) {
	    		defaultSelected =  (pageSize == options[i] ) ? "selected" : "";
	    		dynamicUrl = dynamicUrl.replaceAll("&pageSize=[0-9]{1,2}", "&pageSize="+options[i]);
	    		pagination.append( "<option value='"+dynamicUrl+"' "+ defaultSelected +">" + options[i]+"</option>" );
   	      	}
	    	pagination.append( "</select></span>" );
    	}
    	
    	pagination.append( "</div>" );
    	
    	return pagination.toString();
    }
    
	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		try {
	            out.write(this.createPagination());
	        } catch (Exception ex) {
	            throw new JspException("Error in Paginator tag", ex);
	        }
	    return SKIP_BODY;
	}
	 
    public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getResultSize() {
		return resultSize;
	}

	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}

	public boolean getFirstAndLastView() {
		return firstAndLastView;
	}

	public void setFirstAndLastView(boolean firstAndLastView) {
		this.firstAndLastView = firstAndLastView;
	}

	public boolean getHideTotal() {
		return hideTotal;
	}

	public void setHideTotal(boolean hideTotal) {
		this.hideTotal = hideTotal;
	}
	
	public boolean isShowDynamicPageSize() {
		return showDynamicPageSize;
	}

	public void setShowDynamicPageSize(boolean showDynamicPageSize) {
		this.showDynamicPageSize = showDynamicPageSize;
	}

	public String getCustomFunctionName() {
        return customFunctionName;
	}
	
	public void setCustomFunctionName(String customFunctionName) {
	        this.customFunctionName = customFunctionName;
	}
}