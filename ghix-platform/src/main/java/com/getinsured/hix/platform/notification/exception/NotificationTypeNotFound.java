package com.getinsured.hix.platform.notification.exception;

public class NotificationTypeNotFound extends Exception
{
	public NotificationTypeNotFound()
	{
		super("Notification Type not found.");
	}

	public NotificationTypeNotFound(String message)
	{
		super(message);
	}

	public NotificationTypeNotFound(String message, Throwable cause) {
		super(message, cause);
	}

	public NotificationTypeNotFound(Throwable cause) {
		super(cause);
	}
}
