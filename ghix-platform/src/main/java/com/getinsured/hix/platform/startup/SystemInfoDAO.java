package com.getinsured.hix.platform.startup;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Modified SystemInfo class
 * 
 * @author Nikhil Talreja
 * @since 20 June, 2013
 * 
 */

//@Component
@Deprecated
public class SystemInfoDAO implements ApplicationListener<ContextRefreshedEvent> {

//	private static final String MB_TXT = " MB";
//	private static final int UNIT_SIZE = 1024;
//	private static final Logger LOGGER = LoggerFactory.getLogger(SystemInfoDAO.class);
//	
//	private static final String DATEDUAL = "datedual";
//	private static final String DB_TIMEZONE = "DBTIMEZONE";
//	private static final String SESSION_TIMEZONE = "SESSIONTIMEZONE";
//	private static final String DB_PING_SQL = "select DBTIMEZONE, SESSIONTIMEZONE, to_char(sysdate, 'Dy DD-Mon-YYYY HH24:MI:SS') as \"datedual\" from dual";
//	private static final double TWO_GB = 2048.0;
//	
//	@Value("#{configProp['address_validator_source']}")
//	private String addressValidatorComponent;
//
//	@Value("#{configProp['ecm.type']}")
//	private String ecmType;
//
//	@Value("#{configProp['sms.provider']}")
//	private String smsProvider;
//	
//	private List<SystemInfoRec> records; 
	private SystemInfoRec record;
//	
//	public List<SystemInfoRec> getRecords() {
//		return records;
//	}
//
//	public void setRecords(List<SystemInfoRec> records) {
//		this.records = records;
//	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
//		for(SystemInfoRec record : getSystemInfo()){
//			if(LOGGER.isTraceEnabled()) {
//				LOGGER.trace(record.getGroup() + "::" + record.getAttribute() + "::" + record.getValue() + "::" + record.getErrorMessage() + 
//						"::" + record.getThreshold());
//			}
//		}
	}

	/**
	 * This is for Quick Testing Purpose
	 * 
	 * @param str
	 */
	public static void main(String str[]) {
//		SystemInfoDAO sysInfo = new SystemInfoDAO();
//		for(SystemInfoRec record : sysInfo.getSystemInfo()){
//			if(LOGGER.isTraceEnabled()) {
//				LOGGER.trace(record.getGroup() + "::" + record.getAttribute() + "::" + record.getValue() + "::" + record.getErrorMessage() + "::" + record.getThreshold());
//			}
//			//System.out.println(record.getGroup() + "::" + record.getAttribute() + "::" + record.getValue() + "::" + record.getErrorMessage() + "::" + record.getThreshold());
//		}
	}

	/**
	 * <p>
	 * This method calls all the individual methods. Every method loads specific
	 * details as indicated in each method.
	 * </p>
	 * 
	 * @author Sunil Desu
	 */
	public List<SystemInfoRec> loadSystemInfo() {
		
		ArrayList<SystemInfoRec> records = new ArrayList<SystemInfoRec>();
//		
//		// User details
//		loadUserDetails();
//		// OS details
//		loadOSDetails();
//		// Java details
//		loadJavaDetails();
//		// TimeZone, Date information
//		loadTimeInformation();
//		// JVM Parameters
//		loadJVMParameters();
//		// Memory Information
//		loadMemoryInformation();
//		// Disk Space Information
//		loadDiskSpaceInformation();
//		// Application Build Number
//		loadApplicationBuildNumber();
//		// Application Details
//		loadApplicationConfigDetails();
//		// Load Database Information
////		loadDatabaseInfo();
//		// Load Database Information
//		loadLogFileInfo();
//		// Load Application Configuration
//		loadGlobalConfiguration();
		return records;
	}

	public List<SystemInfoRec> getSystemInfo() {
		
//		try{
//			return loadSystemInfo();
//		}
//		catch (Exception e){
//			LOGGER.error(e.getMessage(),e);
			return null;
//		}
	}
	
	/**
	 * GHIX Application configuration
	 */
	private void loadApplicationConfigDetails() {
		
//		//ECM type
//		record = new SystemInfoRec();
//		record.setGroup("AppConfig");
//		record.setAttribute("ECM Type");
//		record.setValue(ecmType);
//		records.add(record);
//		
//		//Address Validator Type
//		record = new SystemInfoRec();
//		record.setGroup("AppConfig");
//		record.setAttribute("Address Validator Type");
//		record.setValue(addressValidatorComponent);
//		records.add(record);
//		
//		//SMS Provider
//		record = new SystemInfoRec();
//		record.setGroup("AppConfig");
//		record.setAttribute("SMS Provider");
//		record.setValue(smsProvider);
//		records.add(record);
	}

	/**
	 * Java Information
	 */
	private void loadJavaDetails() {
		
//		//Java version
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("java.version");
//		record.setValue(System.getProperty("java.version"));
//		records.add(record);
//		
//		//JVM Model
//		String jvmModel = System.getProperty("sun.arch.data.model");
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("JVM Bit size");
//		record.setValue(jvmModel);
//		record.setThreshold("64");
//		if(StringUtils.equalsIgnoreCase(jvmModel, "32")){
//			record.setErrorMessage("Warning ! Your system is running a 32-bit JVM</b>");
//		}
//		records.add(record);
//		
//		//Java Vendor
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("java.vendor");
//		record.setValue(System.getProperty("java.vendor"));
//		records.add(record);
//		
//		//Java home
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("java.home");
//		record.setValue(System.getProperty("java.home"));
//		records.add(record);
//		
//		//Java Boot classpath
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("Boot Classpath");
//		record.setValue(ManagementFactory.getRuntimeMXBean().getBootClassPath());
//		records.add(record);
//		
//		//Java classpath
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("Classpath"); 
//		record.setValue(ManagementFactory.getRuntimeMXBean()
//				.getClassPath());
//		records.add(record);
//		
//		//Java library path
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("Library Path");
//		record.setValue(ManagementFactory.getRuntimeMXBean()
//				.getLibraryPath());
//		records.add(record);
//		
//		//JVM details
//		record = new SystemInfoRec();
//		record.setGroup("JavaInfo");
//		record.setAttribute("Virtual Machine Details"); 
//		record.setValue(ManagementFactory
//				.getRuntimeMXBean().getVmName()
//				+ ","
//				+ ManagementFactory.getRuntimeMXBean().getVmVendor()
//				+ ","
//				+ ManagementFactory.getRuntimeMXBean().getVmVersion());
	}

	/**
	 * OS user details
	 */
	private void loadUserDetails() {
		
//		//User name
//		record = new SystemInfoRec();
//		record.setGroup("UserInfo");
//		record.setAttribute("user.name");
//		record.setValue(System.getProperty("user.name"));
//		records.add(record);
//		
//		//User Directory
//		record = new SystemInfoRec();
//		record.setGroup("UserInfo");
//		record.setAttribute("user.dir");
//		record.setValue(System.getProperty("user.dir"));
//		records.add(record);
//				
//		//User Home directory
//		record = new SystemInfoRec();
//		record.setGroup("UserInfo");
//		record.setAttribute("user.home");
//		record.setValue(System.getProperty("user.home"));
//		records.add(record);		

	}
	
	/**
	 * OS details
	 */
	private void loadOSDetails() {
		
//		//OS Architecture
//		record = new SystemInfoRec();
//		record.setGroup("OSInfo");
//		record.setAttribute("OS Architecture");
//		record.setValue(ManagementFactory
//				.getOperatingSystemMXBean().getArch());
//		records.add(record);
//		
//		//Number of processors
//		record = new SystemInfoRec();
//		record.setGroup("OSInfo");
//		record.setAttribute("Number of processors");
//		record.setValue(""+ManagementFactory
//				.getOperatingSystemMXBean().getAvailableProcessors());
//		records.add(record);
//
//		//OS Name
//		record = new SystemInfoRec();
//		record.setGroup("OSInfo");
//		record.setAttribute("OS Name");
//		record.setValue(ManagementFactory.getOperatingSystemMXBean()
//				.getName());
//		records.add(record);
//		
//		//OS Version
//		record = new SystemInfoRec();
//		record.setGroup("OSInfo");
//		record.setAttribute("OS Version");
//		record.setValue(ManagementFactory.getOperatingSystemMXBean()
//				.getVersion());
//		records.add(record);
	}
	
	/**
	 * Time Zone Information
	 */
	private void loadTimeInformation() {
		
//		//Time Zone
//		record = new SystemInfoRec();
//		record.setGroup("TimeInfo");
//		record.setAttribute("user.timezone");
//		record.setValue(System.getProperty("user.timezone"));
//		records.add(record);
//
//		//Language
//		record = new SystemInfoRec();
//		record.setGroup("TimeInfo");
//		record.setAttribute("user.language");
//		record.setValue(System.getProperty("user.language"));
//		records.add(record);
//		
//		TimeZone tz = TimeZone.getDefault();
//		
//		//Default Time Zone
//		record = new SystemInfoRec();
//		record.setGroup("TimeInfo");
//		record.setAttribute("Default TimeZone");
//		record.setValue(tz.getDisplayName());
//		records.add(record);
//
//		// OS Version
//		record = new SystemInfoRec();
//		record.setGroup("TimeInfo");
//		record.setAttribute("System Date/Time");
//		record.setValue(new TSDate().toString());
//		records.add(record);
		
	}

	/**
	 * This method loads the JVM parameters into the SYSINFO map.
	 * 
	 * @author Sunil Desu
	 */
	private void loadJVMParameters() {

//		try {
//			LOGGER.info("Loading JVMArgs.");
//			RuntimeMXBean runtimemxBean = ManagementFactory.getRuntimeMXBean();
//			List<String> arguments = runtimemxBean.getInputArguments();
//			if(arguments != null && arguments.size() > 0){
//				for (String jvmParam : arguments) {
//					record = new SystemInfoRec();
//					record.setGroup("JVMArgs");
//					record.setAttribute("ARG");
//					record.setValue(jvmParam);
//					
//					//Check if -Xmx <= 2GB
//					if(StringUtils.contains(jvmParam, "-Xmx")){
//						record.setThreshold("2GB (2048m)");
//						String maxHeap = StringUtils.replace(jvmParam, "-Xmx", "");
//						maxHeap = StringUtils.replace(maxHeap, "m", "");
//						
//						if(StringUtils.isNumeric(maxHeap)){
//						
//							double heapSize = Double.parseDouble(maxHeap);
//							
//							if(heapSize <= TWO_GB){
//								record.setErrorMessage("-Xmx should be more than 2GB (2048m)");
//							}
//						}
//					}
//					
//					records.add(record);
//				}
//			}
//			else{
//				record = new SystemInfoRec();
//				record.setGroup("JVMArgs");
//				record.setAttribute("ARG");
//				record.setValue("");
//				record.setErrorMessage("JVM Arguments are missing");
//				records.add(record);
//			}
//		} catch (Exception ex) {
//			LOGGER.error(""+ex);
//		}
	}

	/**
	 * System Memory Information
	 */
	private void loadMemoryInformation() {
		
//		LOGGER.info("Loading memory information.");
//		int mb = UNIT_SIZE * UNIT_SIZE;
//		MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
//		MemoryUsage muHeap = mxBean.getHeapMemoryUsage();
//		MemoryUsage muNonHeap = mxBean.getNonHeapMemoryUsage();
//		
//		double usedSpaceRatio = (double)muHeap.getUsed()/muHeap.getMax();
//		usedSpaceRatio = Math.round(usedSpaceRatio * 100.0) / 100.0;
//		double freeSpaceRatio = (double)muHeap.getCommitted()/muHeap.getMax();
//		freeSpaceRatio = Math.round(freeSpaceRatio * 100.0) / 100.0;
//		
//		DecimalFormat df = new DecimalFormat("##.##");
//		
//		// Maximum heap memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("heapMaxMemory");
//		record.setValue(muHeap.getMax() / mb + MB_TXT);
//		records.add(record);
//
//		// Initial Heap Memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("heapInitMemory");
//		record.setValue(muHeap.getInit() / mb + MB_TXT);
//		records.add(record);
//
//		// Used heap memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("heapUsedMemory");
//		record.setValue(muHeap.getUsed() / mb + MB_TXT + " (" + (df.format(usedSpaceRatio*100)) +"%)");
//		record.setThreshold("90%");
//		records.add(record);
//		
//		// Free heap memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("heapCommittedMemory");
//		record.setValue(muHeap.getCommitted() / mb	+ MB_TXT + " (" + (df.format(freeSpaceRatio*100)) +"%)");
//		record.setThreshold("10%");
//		if(freeSpaceRatio <= 0.1){
//			record.setErrorMessage("Warning ! Low Heap Space. Free Heap space should be more than 10%");
//		}
//		records.add(record);
//		
//		usedSpaceRatio = (double)muNonHeap.getUsed()/muNonHeap.getMax();
//		usedSpaceRatio = Math.round(usedSpaceRatio * 100.0) / 100.0;
//		freeSpaceRatio = (double)muNonHeap.getCommitted()/muNonHeap.getMax();
//		freeSpaceRatio = Math.round(freeSpaceRatio * 100.0) / 100.0;
//		
//		// Maximum non-heap memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("nonHeapMaxMemory");
//		record.setValue(muNonHeap.getMax() / mb + MB_TXT);
//		records.add(record);
//
//		// Initial non-heap Memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("nonHeapInitMemory");
//		record.setValue(muNonHeap.getInit() / mb + MB_TXT);
//		records.add(record);
//
//		// Used non-heap memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("nonHeapUsedMemory");
//		record.setValue(muNonHeap.getUsed() / mb + MB_TXT + " ("
//				+ (df.format(usedSpaceRatio * 100)) + "%)");
//		record.setThreshold("90%");
//		records.add(record);
//		
//		// Free non-heap memory
//		record = new SystemInfoRec();
//		record.setGroup("MemoryInfo");
//		record.setAttribute("nonHeapCommittedMemory");
//		record.setValue(muNonHeap.getCommitted() / mb + MB_TXT + " ("
//				+ (df.format(freeSpaceRatio * 100)) + "%)");
//		record.setThreshold("10%");
//		if (freeSpaceRatio <= 0.1) {
//			record.setErrorMessage("Warning ! Low Heap Space. Free Heap space should be more than 10%");	
//		}
//		records.add(record);

	}
	
	/**
	 * Disk Space information Information
	 */
	private void loadDiskSpaceInformation() {

//		final String dir = System.getProperty("user.dir");
//		File file = new File(dir);
//		
//		// total disk space in bytes.
//		long totalSpace = file.getTotalSpace();
//		long usableSpace = file.getUsableSpace();
//		long freeSpace = file.getFreeSpace();
//		
//		double spaceRatio = (double)freeSpace/totalSpace;
//		spaceRatio = Math.round(spaceRatio * 100.0)/100.0;
//		DecimalFormat df = new DecimalFormat("##.##");
//		
//		// Total disk space
//		record = new SystemInfoRec();
//		record.setGroup("DISKInfo");
//		record.setAttribute("Total size");
//		record.setValue(totalSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT);
//		record.setThreshold("90%");
//		records.add(record);
//
//		// Usable disk space
//		record = new SystemInfoRec();
//		record.setGroup("DISKInfo");
//		record.setAttribute("Usable Space free");
//		record.setValue(usableSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT + " (" + (df.format(spaceRatio*100)) +"%)");
//		record.setThreshold("10%");
//		if(spaceRatio <= 0.1){
//			record.setErrorMessage("Warning ! Low Disk Space. Usable disk space should be more than 10%)");
//		}
//		records.add(record);
//		
//		// Free disk space
//		record = new SystemInfoRec();
//		record.setGroup("DISKInfo");
//		record.setAttribute("Space free");
//		record.setValue(freeSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT + " (" + (df.format(spaceRatio*100)) +"%)");
//		record.setThreshold("10%");
//		if(spaceRatio <= 0.1){
//			record.setErrorMessage("Warning ! Low Disk Space. Free disk space should be more than 10%)");
//		}
//		records.add(record);
	}
	
	/**
	 * Bamboo Build Details
	 */
	private void loadApplicationBuildNumber() {
		
//		// Build number
//		record = new SystemInfoRec();
//		record.setGroup("BuildInfo");
//		record.setAttribute("Build Number");
//		record.setValue(GhixPlatformConstants.BUILD_NUMBER);
//		record.setThreshold("Not NULL");
//		if(StringUtils.isEmpty(GhixPlatformConstants.BUILD_NUMBER)){
//			record.setErrorMessage("Build Number is missing");
//		}
//		records.add(record);
//
//		// Build date
//		record = new SystemInfoRec();
//		record.setGroup("BuildInfo");
//		record.setAttribute("Build Date");
//		record.setValue(GhixPlatformConstants.BUILD_DATE);
//		record.setThreshold("Not NULL");
//		if(StringUtils.isEmpty(GhixPlatformConstants.BUILD_DATE)){
//			record.setErrorMessage("Build Date is missing");
//		}
//		records.add(record);
//		
//		// Branch Name
//		record = new SystemInfoRec();
//		record.setGroup("BuildInfo");
//		record.setAttribute("Branch Name");
//		record.setValue(GhixPlatformConstants.BUILD_BRANCH_NAME);
//		record.setThreshold("Not NULL");
//		if(StringUtils.isEmpty(GhixPlatformConstants.BUILD_BRANCH_NAME)){
//			record.setErrorMessage("Branch Name is missing");
//		}
//		records.add(record);
	}
	
	/**
	 * Database connection details
	 */
	@Deprecated
	private void loadDatabaseInfo() {
		
//		String DBTIMEZONE = "";
//		String SESSIONTIMEZONE = "";
//		String dateDual = "";
//		Connection conn = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		long startTime;
//		long endTime;
//		
//		try {
//			
//			InitialContext ic = new InitialContext();
//			Context xmlContext = (Context) ic.lookup("java:comp/env");
//			DataSource dataSource = (DataSource) xmlContext.lookup("jdbc/ghixDS");
//			
//			conn = dataSource.getConnection();
//			
//			if(conn == null){
//				LOGGER.error("loadDatabaseInfo: connection is null");
//				return;
//			}
//			DatabaseMetaData mtdt = conn.getMetaData();
//			ps = conn.prepareStatement(DB_PING_SQL);
//			startTime = TimeShifterUtil.currentTimeMillis();
//			rs = ps.executeQuery();
//			endTime = TimeShifterUtil.currentTimeMillis();
//			while (rs.next()) {
//				dateDual = rs.getString(DATEDUAL);
//				DBTIMEZONE = rs.getString(DB_TIMEZONE);
//				SESSIONTIMEZONE = rs.getString(SESSION_TIMEZONE);
//			}
//			
//			// Database driver
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("Database Driver");
//			record.setValue(mtdt.getDriverName());
//			records.add(record);
//
//			// Database connection URL
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("Database URL");
//			record.setValue(mtdt.getURL());
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("DB Ping: USER: ");
//			record.setValue(mtdt.getUserName());
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("DB Ping: Date: ");
//			record.setValue(dateDual);
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("DB Ping: DB Timezone: ");
//			record.setValue(DBTIMEZONE);
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("DB Ping: Session Timezone: ");
//			record.setValue(SESSIONTIMEZONE);
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("DB Ping: Time taken to query (ms):");
//			record.setValue(String.valueOf(endTime - startTime));
//			records.add(record);
//		}
//		catch (Exception exception) {
//			LOGGER.error(exception.getMessage(),exception);
//			record = new SystemInfoRec();
//			record.setGroup("DatabaseInfo");
//			record.setAttribute("DB Ping");
//			record.setValue("Exception Occured while DB ping");
//			record.setErrorMessage(exception.getMessage());
//			records.add(record);
//		} finally {
//			if (null != rs) {
//				try {
//					rs.close();
//					rs = null;
//				} catch (Exception sqlException) {
//					LOGGER.error(sqlException.getMessage(),sqlException);
//				}
//			}
//			if (ps != null) {
//				try {
//					ps.close();
//				} catch (Exception sqlException) {
//					LOGGER.error(sqlException.getMessage(),sqlException);
//				}
//			}
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (Exception sqlException) {
//					LOGGER.error(sqlException.getMessage(),sqlException);
//				}
//			}
//		}
	}
		
	
	
	/**
	 * Log files information
	 */
	private void loadLogFileInfo() {
		
//		String ghixHome = System.getenv("GHIX_HOME");
//		
//		if(null == ghixHome || ghixHome.isEmpty()){
//			//This helps in default situations and while running from STS.
//			ghixHome="C:/workfolder/ghix";
//		}
//	    File ghixHomeDir = new File(ghixHome);
//	    
//	    //We are looking for all log4j files in GHIX_HOME (except target folders)
//	    findLogConfigFiles(ghixHomeDir);
	}
	
	/**
	 * Finds all log4j.xml files in a directory
	 * @author Nikhil Talreja
	 * @since Apr 5, 2013
	 * @param File file, HashMap<String, String> temp
	 * @return void
	 */
	public void findLogConfigFiles(File file) {

//		if (file.isFile()
//				&& file.getName().matches("^[A-Za-z0-9_\\-]*log4j.xml")
//				&& !file.getAbsolutePath().toLowerCase().contains("target")) {
//			
//			record = new SystemInfoRec();
//			record.setGroup("LogFileInfo");
//			record.setAttribute("File Path");
//			record.setValue(file.getAbsolutePath());
//			records.add(record);
//		} else if (file.isDirectory()) {
//			File[] listOfFiles = file.listFiles();
//			if (listOfFiles != null) {
//				for (int i = 0; i < listOfFiles.length; i++) {
//					findLogConfigFiles(listOfFiles[i]);
//				}
//			}
//		}
		
	}
	
	/**
	 * Loads global configuration from file appconf/ghix-configuration
	 * @author Nikhil Talreja
	 * @since July 29, 2013
	 * @return void
	 */
	public void loadGlobalConfiguration(){
		
//		try {
//			//Adding Application Configuration
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("Exchange Type");
//			record.setValue(DynamicPropertiesUtil.getPropertyValue("global.ExchangeType"));
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("State Name");
//			record.setValue(DynamicPropertiesUtil.getPropertyValue("global.StateName"));
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("State Code");
//			record.setValue(DynamicPropertiesUtil.getPropertyValue("global.StateCode"));
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("Exchange Name");
//			record.setValue(DynamicPropertiesUtil.getPropertyValue("global.ExchangeName"));
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("Exchange Phone");
//			record.setValue(DynamicPropertiesUtil.getPropertyValue("global.ExchangePhone"));
//			records.add(record);
//			
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("Exchange URL");
//			record.setValue(DynamicPropertiesUtil.getPropertyValue("global.ExchangeURL"));
//			records.add(record);
//			
//		} catch (Exception e) {
//			LOGGER.error(e.getMessage());
//			record = new SystemInfoRec();
//			record.setGroup("AppConfig");
//			record.setAttribute("");
//			record.setValue("");
//			record.setThreshold("Application Configuration should not be NULL");
//			record.setErrorMessage("Error occured while loading application configuration");
//			records.add(record);
//		}
	}
	
}
