package com.getinsured.hix.platform.security;

import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.getinsured.hix.platform.util.GhixAESCipherPool;

public class SecureUrl extends TagSupport {
	
	private static final long serialVersionUID = 1;
	
	private Map <String,Object> paramMap;
	private List<String>  paramList;
	private String var;
	
	@Override
	public int doStartTag() throws JspException {
		try {
		        String encryptedString = "";  
				if(null != paramMap){
					encryptedString = GhixAESCipherPool.encryptGenericParameterMap(paramMap);
		           }else if(null != paramList){
		        	   encryptedString =GhixAESCipherPool.encryptParameterList(paramList);
		           }

				if(null != var && var != "") {
					pageContext.setAttribute(var, encryptedString);
				}else{
					pageContext.getOut().println(encryptedString);
				}
	        } catch (Exception ex) {
	            throw new JspException("Error in Shop employer navigation tag", ex);
	        }
	    return SKIP_BODY;
	}

	public Map <String,Object> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map <String,Object> paramMap) {
		this.paramMap = paramMap;
	}

	public List<String> getParamList() {
		return paramList;
	}

	public void setParamList(List<String> paramList) {
		this.paramList = paramList;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
}
