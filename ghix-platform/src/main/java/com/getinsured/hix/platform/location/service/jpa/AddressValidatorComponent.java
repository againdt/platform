package com.getinsured.hix.platform.location.service.jpa;

import java.util.List;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;

public interface AddressValidatorComponent {

	public List<Location> validateAddress(Location address) throws Exception;

	public AddressValidationResponse validateAddress(LocationDTO address) throws Exception;
}
