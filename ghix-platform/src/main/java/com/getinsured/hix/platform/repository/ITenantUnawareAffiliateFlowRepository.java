package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.affiliate.model.AffiliateFlow;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface ITenantUnawareAffiliateFlowRepository extends TenantUnawareRepository<AffiliateFlow,Integer>{
	
	@Query(value="select flow from AffiliateFlow flow where flow.affiliate.affiliateId = :affiliateId and flow.tenantId = :tenantId ")
	public List<AffiliateFlow> getAffiliateFlows(@Param("affiliateId" )Long affiliateId,@Param("tenantId") Long tenantId);
	
	public AffiliateFlow findByAffiliateflowIdAndTenantId(Integer affiliateFlowId,Long tenantId);

}
