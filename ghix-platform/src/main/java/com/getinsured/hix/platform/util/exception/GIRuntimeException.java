package com.getinsured.hix.platform.util.exception;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

/**
 * GIRuntimeException class to capture below parameters:-
 * 		errorCode, errorMsg, severity, component name, exception occurred time.
 * 		This also captures nestedStackTrace to handle and log exception raised by REST enabled modules.
 * 
 * @author Ekram Ali Kazi
 *
 */
public class GIRuntimeException extends RuntimeException {

	protected static final long serialVersionUID = 1L;
	public static String ERROR_CODE_UNKNOWN = "UNKNOWN-0000";

	public enum Component {

		AEE("AEE"),
		BATCH("BATCH"),
		CAP("CAP"),
		CRM("CRM"),
		EAPP("EAPP"),
		ELIGIBILITY("ELIGIBILITY"),
		EMPLOYER_EMPLOYEE("EMPLOYER_EMPLOYEE"),
		EMX("EMX"),
		ENROLLMENT("ENROLLMENT"),
		FEEDS("FEEDS"),
		FINANCE("FINANCE"),
		LCE("LCE"),
		PLANDISPLAY("PLANDISPLAY"),
		PLANMGMT("PLANMGMT"),
		PLATFORM("PLATFORM"),
		TKM("TKM"),
		UNKNOWN("UNKNOWN");

		private final String component;

		Component(String component) {
			this.component = component;
		}

		public String getComponent() {
			return component;
		}
	}

	public enum Severity {

		FATAL("FATAL"),
		HIGH("HIGH"),
		LOW("LOW");

		private final String severity;

		Severity(String severity) {
			this.severity = severity;
		}

		public String getSeverity() {
			return severity;
		}
	}

	protected String errorCode;
	protected String errorMsg;
	protected String nestedStackTrace;
	protected String errorSeverity;
	protected Date eventTime;
	protected Integer giMonitorId;
	protected String component;

	public GIRuntimeException() {
		super();
	}
	public GIRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
	public GIRuntimeException(String message) {
		super(message);
	}

	public GIRuntimeException(String message, String nestedStackTrace) {
		super(message);
		this.nestedStackTrace = nestedStackTrace;
	}
	public GIRuntimeException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * 
	 * @param ghixErrorCode
	 * @param aThrowable
	 * @param nestedStackTrace - to capture and handle exception thrown by REST module
	 * @param severity
	 */
	public GIRuntimeException(String ghixErrorCode, Throwable aThrowable, String nestedStackTrace, Severity severity )
	{
		super(aThrowable);
		this.errorCode = ghixErrorCode;
		this.errorSeverity = severity.getSeverity();
		this.eventTime = new TSDate();
		this.nestedStackTrace = nestedStackTrace;
	}

	/**
	 * 
	 * @param aThrowable
	 * @param nestedStackTrace  - to capture and handle exception thrown by REST module
	 * @param severity
	 * @param url
	 */
	public GIRuntimeException(Throwable aThrowable, String nestedStackTrace, Severity severity, String component, Integer giMonitorId)
	{
		super(aThrowable);
		this.errorSeverity = severity.getSeverity();
		this.eventTime = new TSDate();
		this.nestedStackTrace = nestedStackTrace;
		this.component = component;
		this.giMonitorId = giMonitorId;
	}

	/**
	 * 
	 * @param ghixErrorCode
	 * @param aThrowable
	 * @param nestedStackTrace
	 * @param severity
	 * @param component
	 * @param giMonitorId
	 */
	public GIRuntimeException(String ghixErrorCode,Throwable aThrowable, String nestedStackTrace, Severity severity, String component, Integer giMonitorId)
	{
		super(aThrowable);
		this.errorCode = ghixErrorCode;
		this.errorSeverity = severity.getSeverity();
		this.eventTime = new TSDate();
		this.nestedStackTrace = nestedStackTrace;
		this.component = component;
		this.giMonitorId = giMonitorId;
	}

	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public Integer getGiMonitorId() {
		return giMonitorId;
	}
	public void setGiMonitorId(Integer giMonitorId) {
		this.giMonitorId = giMonitorId;
	}

	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getNestedStackTrace() {
		return nestedStackTrace;
	}
	public void setNestedStackTrace(String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}
	public String getErrorSeverity() {
		return errorSeverity;
	}
	public void setErrorSeverity(String errorSeverity) {
		this.errorSeverity = errorSeverity;
	}

	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
}
