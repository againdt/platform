package com.getinsured.hix.platform.payment.util;

public class CyberSourceKeyConstants 
{
	public static final String PAYSUBSCRIPTIONCREATESERVICE_RUN_KEY = "paySubscriptionCreateService_run";
	public static final String MERCHANTID_KEY = "merchantID";
	public static final String MERCHANTREFERENCECODE_KEY = "merchantReferenceCode";
	public static final String BILLTO_FIRSTNAME_KEY = "billTo_firstName";
	public static final String BILLTO_LASTNAME_KEY = "billTo_lastName";
	public static final String BILLTO_STREET1_KEY = "billTo_street1";
	public static final String BILLTO_CITY_KEY = "billTo_city";
	public static final String BILLTO_STATE_KEY = "billTo_state";
	public static final String BILLTO_POSTALCODE_KEY = "billTo_postalCode";
	public static final String BILLTO_COUNTRY_KEY = "billTo_country";
	public static final String BILLTO_EMAIL_KEY = "billTo_email";
	public static final String BILLTO_PHONENUMBER_KEY = "billTo_phoneNumber";
	public static final String PURCHASETOTALS_CURRENCY_KEY = "purchaseTotals_currency";
	public static final String SUBSCRIPTION_TITLE_KEY = "subscription_title";
	public static final String SUBSCRIPTION_PAYMENTMETHOD_KEY = "subscription_paymentMethod";
	public static final String CARD_CARDTYPE_KEY = "card_cardType";
	public static final String CARD_ACCOUNTNUMBER_KEY = "card_accountNumber";
	public static final String CARD_EXPIRATIONMONTH_KEY = "card_expirationMonth";
	public static final String CARD_EXPIRATIONYEAR_KEY = "card_expirationYear";
	public static final String CHECK_ACCOUNTNUMBER_KEY = "check_accountNumber";
	public static final String CHECK_SECCODE_KEY = "check_secCode";
	public static final String CHECK_ACCOUNTTYPE_KEY = "check_accountType";
	public static final String CHECK_BANKTRANSITNUMBER_KEY = "check_bankTransitNumber";
	
	public static final String PAYSUBSCRIPTIONUPDATESERVICE_RUN_KEY = "paySubscriptionUpdateService_run";
	public static final String RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY = "recurringSubscriptionInfo_subscriptionID"; 
	public static final String RECURRINGSUBSCRIPTIONINFO_STATUS_KEY = "recurringSubscriptionInfo_status";
	
	public static final String PAYSUBSCRIPTIONRETRIEVESERVICE_RUN_KEY = "paySubscriptionRetrieveService_run";
	
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONMONTH_KEY = "paySubscriptionRetrieveReply_cardExpirationMonth";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONYEAR_KEY = "paySubscriptionRetrieveReply_cardExpirationYear";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CARDTYPE_KEY = "paySubscriptionRetrieveReply_cardType";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CARDACCOUNTNUMBER_KEY = "paySubscriptionRetrieveReply_cardAccountNumber";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_PHONENUMBER_KEY = "paySubscriptionRetrieveReply_phoneNumber";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CITY_KEY = "paySubscriptionRetrieveReply_city";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_POSTALCODE_KEY = "paySubscriptionRetrieveReply_postalCode";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_STREET1_KEY = "paySubscriptionRetrieveReply_street1";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_TITLE_KEY = "paySubscriptionRetrieveReply_title";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_EMAIL_KEY = "paySubscriptionRetrieveReply_email";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_STATE_KEY = "paySubscriptionRetrieveReply_state";
	
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTNUMBER_KEY = "paySubscriptionRetrieveReply_checkAccountNumber";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKBANKTRANSITNUMBER_KEY = "paySubscriptionRetrieveReply_checkBankTransitNumber";
	public static final String PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTTYPE_KEY = "paySubscriptionRetrieveReply_checkAccountType";
	
	public static final String ECCREDITSERVICE_RUN_KEY = "ecCreditService_run";
	public static final String PURCHASETOTALS_GRANDTOTALAMOUNT_KEY = "purchaseTotals_grandTotalAmount";
	public static final String ECAUTHENTICATESERVICE_RUN_KEY = "ecAuthenticateService_run";
	public static final String ECDEBITSERVICE_RUN_KEY = "ecDebitService_run";
	
	public static final String CCAUTHSERVICE_RUN_KEY = "ccAuthService_run";
	public static final String CCCREDITSERVICE_RUN_KEY = "ccCreditService_run";
	
	public static final String PAYSUBSCRIPTIONDELETESERVICE_RUN_KEY = "paySubscriptionDeleteService_run";
	
	public static final String PT_CURRENCY = "USD";
	/*public static final String MERCHANT_ID = "vimo";*/
	public static final String MERCHANT_CODE = "MRC-14344";
	public static final String SUBSCRIPTION_STATUS_CANCEL = "cancel";
	
	public static final String CCCAPTURESERVICE_RUN_KEY = "ccCaptureService_run";
	public static final String CCCAPTURESERVICE_AUTHREQUESTID_KEY = "ccCaptureService_authRequestID";
	
	public static final String SEND_TO_PRODUCTION_FLAG = "sendToProduction";
	public static final String TARGET_API_REVISION = "targetAPIVersion";
	public static final String KEYS_DIRECTORY = "keysDirectory";
	public static final String CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID     = "ecCreditService_debitRequestID";
	public static final String PAYSUBSCRIPTION_CREATEREPLY_SUBSCRIPTIONID_KEY   = "paySubscriptionCreateReply_subscriptionID";
	public static final String RECURRINGSUBSCRIPTIONINFO_FREQUENCY = "on-demand";

	/**
	 * Disbursement information.
	 */
	public static final String CHECK_SECCODE_CCD = "CCD";
	public static final String CHECK_SECCODE_WEB = "WEB";
	
	/*
	 * Constants related to Recurring billing
	 */
	public static final String IS_RECURRING_KEY = "IS_RECURRING";
	public static final String RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY = "recurringSubscriptionInfo_frequency";
	public static final String RECURRINGSUBSCRIPTIONINFO_AMOUNT_KEY = "recurringSubscriptionInfo_amount";
	public static final String RECURRINGSUBSCRIPTIONINFO_STARTDATE_KEY = "recurringSubscriptionInfo_startDate";
	public static final String RECURRINGSUBSCRIPTIONINFO_NUMBEROFPAYMENTS_KEY = "recurringSubscriptionInfo_numberOfPayments";
	
}
