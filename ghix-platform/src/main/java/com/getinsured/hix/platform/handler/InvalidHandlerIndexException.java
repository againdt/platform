package com.getinsured.hix.platform.handler;

public class InvalidHandlerIndexException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidHandlerIndexException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidHandlerIndexException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidHandlerIndexException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidHandlerIndexException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidHandlerIndexException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
