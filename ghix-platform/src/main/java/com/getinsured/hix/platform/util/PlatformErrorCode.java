/**
 * 
 */
package com.getinsured.hix.platform.util;


/**
 * This class is used to define and fetch platform related error codes.
 * Please note that all error codes defined here also need to be updated
 * in the database table gi_error_codes using liquibase.
 * 
 * @author Biswakesh
 * 
 */
public enum PlatformErrorCode {
	PLATFORM_00001("PLATFORM-00001"), PLATFORM_00002("PLATFORM-00002"), PLATFORM_00003(
			"PLATFORM-00003"), PLATFORM_00004("PLATFORM-00004"), PLATFORM_00005(
			"PLATFORM-00005"), PLATFORM_00006("PLATFORM-00006");

	public String getErrorCode() {
		return errorCode;
	}

	private final String errorCode;

	private PlatformErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
