package com.getinsured.hix.platform.location.service.jpa;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;

/**
 * This is default Address Validator. This is used when none of the validator
 * components were chosen. It won't do anything. Just returns the given address.
 * 
 * @author polimetla_b
 * @since 11/2/2012
 */
public class AddressValidatorDefault implements AddressValidatorComponent {


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.location.service.LocationValidatorService#
	 * validateLocation(com.getinsured.hix.platform.location.model.Location)
	 */
	public List<Location> validateAddress(Location address) throws Exception {

		List<Location> list = new ArrayList<Location>();
		list.add(address);

		return list;
	}

	@Override
	public AddressValidationResponse validateAddress(LocationDTO address)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



}
