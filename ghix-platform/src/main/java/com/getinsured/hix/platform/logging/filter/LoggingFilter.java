package com.getinsured.hix.platform.logging.filter;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.TimeshiftContext;

/**
 * 
 * @author Sunil Desu
 * 
 */
public class LoggingFilter implements Filter {

	private static final boolean IS_MULTI_TENANT_ENABLED = System.getProperty("tenant.enabled") == null ? false : "on".equals(System.getProperty("tenant.enabled"));

	private static final String REQUEST_CORRELATION_ID = "X-requestCorrelationId".intern();
	private static final String START_TIME = "START_TIME".intern();
	private static final String TENANT_ID = "tenantId".intern();
	private static final String USER_ID = "userId".intern();
	private static final String EQUALS = "=".intern();
	private static final String COMMA = ",".intern();

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)	throws IOException, ServletException {

		try {
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if(sra != null) {
				String user = sra.getRequest().getHeader("X-USER".intern());
				String offset = sra.getRequest().getHeader("X-USER-OFFSET".intern());
				TimeShifterUtil.setUserOffset(user, offset);
			}else {
				  HttpServletRequest req = (HttpServletRequest) servletRequest;
			      String tsUser = req.getHeader("X-USER".intern());
			      String userOffset = req.getHeader("X-USER-OFFSET".intern());
			      
			      if(tsUser != null) {
			    	  TimeShifterUtil.setUserOffset(tsUser, userOffset);
			      }
			}
			
			long startTime = TimeShifterUtil.currentTimeMillis();
			HttpServletRequest request = (HttpServletRequest) servletRequest;
			HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
			String requestCorrelationId = request.getHeader(REQUEST_CORRELATION_ID);
			setRequestContextInformation(request);
			if (requestCorrelationId == null || requestCorrelationId.trim().equals("")) {
				StringBuffer correlationId = new StringBuffer();
				correlationId.append(REQUEST_CORRELATION_ID);
				correlationId.append(EQUALS);
				correlationId.append(UUID.randomUUID().toString());
				requestCorrelationId = correlationId.toString();

				if (IS_MULTI_TENANT_ENABLED) {
					requestCorrelationId = requestCorrelationId.concat(appendTenantInformation());
				}
				
				AccountUser user = getLoggedInUser();
				if (null != user) {
					requestCorrelationId = requestCorrelationId.concat(appendUserInformation(user));
					String requestUri = request.getRequestURI();
					if(requestUri.compareToIgnoreCase("/hix/".intern()) == 0){
						StringBuffer landingPageUrlBuf = new StringBuffer(GhixConstants.APP_URL).append("/account/user/loginSuccess".intern());
						httpResponse.sendRedirect(landingPageUrlBuf.toString());
						return;
					}					
				}
				

			}
			
			RequestCorrelationContext.getCurrent().setCorrelationId(requestCorrelationId);
			
			MDC.put(REQUEST_CORRELATION_ID, requestCorrelationId);
			MDC.put(START_TIME, String.valueOf(startTime));
			
			chain.doFilter(servletRequest, servletResponse);
		}
		finally
		{
			clearMDCAndRequestCorrelationContext();
			TimeshiftContext.clearCurrent();
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
		clearMDCAndRequestCorrelationContext();
	}

	private void clearMDCAndRequestCorrelationContext()
	{
		try
		{
	  MDC.remove(REQUEST_CORRELATION_ID);
      MDC.remove(START_TIME);
	  MDC.clear();
		}
		catch(Exception e)
		{
			/* NoSuchMethodException */
		}
		finally
		{
	  RequestCorrelationContext.clearCurrent();
	}
	}
	
	private AccountUser getLoggedInUser() {
		AccountUser user = null;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(null!=auth){
				Object  principal= auth.getPrincipal();
				if(null!=principal && principal instanceof AccountUser) {
						user = (AccountUser) principal;
				}
			}
		} catch (Exception ex) {
			// In case of exception, there is nothing that can be done. Side
			// effect will be that there will be no user information. To
			// let the flow proceed, catching the exception and doing a no op.
		}
		return user;
	}

	private String appendTenantInformation(){
		
		final TenantDTO tenant = TenantContextHolder.getTenant();
		
		if (null != tenant && null != tenant.getId()) {
		    final StringBuffer tenantInfo = new StringBuffer();

			tenantInfo.append(COMMA);
			tenantInfo.append(TENANT_ID);
			tenantInfo.append(EQUALS);
			tenantInfo.append(tenant.getId());
			
			return tenantInfo.toString();
		}
		
		return "";
	}
	
	private String appendUserInformation(final AccountUser user) {
		final StringBuffer userInfo = new StringBuffer();

		userInfo.append(COMMA);
		userInfo.append(USER_ID);
		userInfo.append(EQUALS);
		userInfo.append(user.getId());
		if(user.getActiveModuleId()!=0 && user.getActiveModuleName() != null && !user.getActiveModuleName().trim().equals("")){
			userInfo.append(COMMA);
			userInfo.append("activeModuleName=");
			userInfo.append(user.getActiveModuleName());
			userInfo.append(",activeModuleId=");
			userInfo.append(user.getActiveModuleId());
		}
		return userInfo.toString();
	}
	
	private void setRequestContextInformation(final HttpServletRequest request){
		try{
			RequesterNameHolder.getCurrent().setContextName(request.getContextPath());
		}catch(Exception ex){
			// In case of exception, there is nothing that can be done. Side
			// effect will be that there will be no requester information. To
			// let the flow proceed, catching the exception and doing a no op.
		}
	}
}
