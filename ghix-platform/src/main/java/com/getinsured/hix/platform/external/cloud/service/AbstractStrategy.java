package com.getinsured.hix.platform.external.cloud.service;

import java.net.MalformedURLException;
import java.net.URL;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

abstract class AbstractStrategy implements ExternalCloudStrategy{
	
	private static final String MALFORMED_URL_FOR_UPLOADED_RESOURCE_ON_CLOUD = "Malformed URL for uploaded resource on cloud - ";

	public abstract String uploadResource(String relativePath, String fileName, byte[] dataBytes);

	@Override
	public URL upload(String relativePath, String fileName, byte[] dataBytes) {
		// 1. upload to external cloud
		String urlString = uploadResource(relativePath, fileName, dataBytes);
		
		URL url = null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {
			throw new GIRuntimeException(MALFORMED_URL_FOR_UPLOADED_RESOURCE_ON_CLOUD + urlString, e);
		}
		
		return url;
	}

}
