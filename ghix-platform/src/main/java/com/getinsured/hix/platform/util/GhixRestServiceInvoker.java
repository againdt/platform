package com.getinsured.hix.platform.util;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

/**
 * Invoke REST Services
 */
@Component
public class GhixRestServiceInvoker {
	
	private static final Logger logger = Logger.getLogger(GhixRestServiceInvoker.class);

	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private UserService userService;

	/**
	 * Invoke Rest Service
	 * 
	 * @param request
	 * 			Request Object
	 * @param endpoint
	 * 			The Rest Service URL
	 * @param methodType
	 * 			HTTP Method Type e:g POST/GET/PUT etc.
	 * @return the response xml as String
	 */
	public String invokeRestService(Object request, String endpoint, HttpMethod methodType, String component) {
		String response = null;
		ResponseEntity<String> responseEntity = null;
		
		try {
			responseEntity = ghixRestTemplate.exchange(endpoint, getUserName(), methodType, MediaType.APPLICATION_JSON, String.class, request);

			if (responseEntity != null) {
				response = responseEntity.getBody();
				if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
					throw new GIRuntimeException(null, null, Severity.HIGH, component, Integer.valueOf(response));
				}
			}
		} catch(GIRuntimeException giException){
			throw giException;
		} catch(NumberFormatException exception){
			throw new GIRuntimeException(null, response, Severity.HIGH, component, null);
		} catch(Exception exception){
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, component, null);
		}

		return response;
	}
	
	public <T> T invokeRestService(Object request, String endpoint, HttpMethod methodType, String component, Class<T> responseType) {
		HttpStatus response = null;
		ResponseEntity<T> responseEntity = null;
		
		try {
			responseEntity = (ResponseEntity<T>) ghixRestTemplate.exchange(endpoint, getUserName(), methodType, MediaType.APPLICATION_JSON, responseType, request);

			if (responseEntity != null) {
				response = responseEntity.getStatusCode();
				if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
					throw new GIRuntimeException(null, null, Severity.HIGH, component, 404);
				}
			}
			else{
				throw new GIRuntimeException(null, null, Severity.HIGH, component, 404);
			}
		} catch(GIRuntimeException giException){
			throw giException;
		} catch(NumberFormatException exception){
			//throw new GIRuntimeException(null, response, Severity.HIGH, component, null);
		} catch(Exception exception){
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, component, null);
		}

		return responseEntity.getBody();
	}
	
	private String getUserName() throws Exception{
		String userName = null;
		
		try {
			AccountUser user = userService.getLoggedInUser();
			if(user!=null){
				userName = user.getUsername();
			}
		} catch(Exception exception){
			logger.error("Error getting logged in user:"+exception.getMessage(),exception);
			return null;
		}
		return userName;
	}
}
