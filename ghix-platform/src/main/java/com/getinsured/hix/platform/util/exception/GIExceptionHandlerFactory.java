package com.getinsured.hix.platform.util.exception;

import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class GIExceptionHandlerFactory {
	
	private static GIExceptionHandler giExceptionHandler;
	
	public static void init(GIExceptionHandler giExceptionHandler) {
		GIExceptionHandlerFactory.giExceptionHandler = giExceptionHandler;
	}
	
	public static GIMonitor recordCriticalException(Component component,String ghixErrorCode,String errorMessage,Exception exception){
		return giExceptionHandler.recordCriticalException(	component, // Component name
															ghixErrorCode, //ghixErrorCode
															SecurityUtil.sanitizeForLogging(errorMessage), //errorMessage
															exception //Exception
															);
	}
	
}
