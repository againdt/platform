package com.getinsured.hix.platform.security.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.cap.CapTeamMemberDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CapTeam;
import com.getinsured.hix.model.CapTeamLeaders;
import com.getinsured.hix.model.CapTeamMembers;
import com.getinsured.hix.model.GroupInfo;
import com.getinsured.hix.model.GroupInfoFilter;
import com.getinsured.hix.platform.util.exception.CAPException;

public interface GroupService {

	CapTeam addGroup(String name, Integer groupLeadUserId) throws CAPException;
	CapTeam editGroup(Integer groupId, String name, Integer groupLeadUserId)throws CAPException;
	CapTeam findById(int groupId) throws CAPException;
	List<CapTeam> findAll();
	List<GroupInfo> getGroupInfoList(GroupInfoFilter groupInfoFilter) throws CAPException;
	Map<Integer, String> getAvailableCSRUser();
	
	

	boolean updateGroupMembers(Integer groupId, List<Integer> userId) throws CAPException;
	
	boolean deleteGroupMembers(Integer groupId,	List<Integer> userId);
	
	boolean addNewUsersToGroup(Integer groupId,	List<Integer> userId);
	
	boolean deleteByGroupId(Integer groupId);
	Map<Integer, String> getAccountUsersByGroupId( Integer groupId);
	
	List<AccountUser> getAccountUsersListByGroupId( Integer groupId) throws CAPException;
	
	Map<Integer, String> getAccountUsersByLeadId( Integer leadId);
	
	Map<String, String> getAccountUsersByLeadIdNew( Integer leadId);
	
	AccountUser getUserGroupLead(Integer userId);
	boolean addNewUsersToGroupWithStartDate(Integer groupId, Integer userId,
			Date startDate);
	
	boolean doesGroupExist(String teamName);
	
	CapTeam addGroup(String name, Integer parentTeamId,Integer groupLeadUserId,String startDate) throws CAPException;
	
	CapTeam editGroup(int teamId, String teamName, Integer managerId,
			String endDate, String endDate2);
	List<CapTeamMemberDto> getTeamMembersByGroupId(Integer groupId);
	List<CapTeamMemberDto> getAvailableCsrUsersWithDate();
	CapTeamLeaders getTeamLeaderInfo(Integer groupId, Integer leadId);
	CapTeamMemberDto getTeamMemberInfo(Integer teamMemberId);
	String getMaximumDateForDeleteTeam(Integer teamMemberId );
	void deleteByGroupIdAndEndDate(Integer groupId, String endDate)
			throws CAPException;
	
	Map<Integer, CapTeamMemberDto> getTeamMembersInfo(List<Integer> userIds);
	void deleteTeamMembersWithEndDate(List<Integer> teamMemberIds,
			String endDate) throws CAPException;
	boolean addNewUsersToGroupWithStartDate(Integer groupId,
			List<Integer> userIds, Date startDate);
	boolean updateGroupMembersWithDate(Integer groupId, List<Integer> userIds,
			String teamMemberDate) throws CAPException;
	List<Integer> findActiveUsersInGroup(Integer groupId);
	CapTeamMembers findActiveUsersByUserId(Integer userId);
	Date getMaxEndDateForUser(Integer userId);
	List<AccountUser> getActiveAccountUsersByGroupIdAndEndDate(Integer groupId)
			throws CAPException;
	Map<Integer, String> getActiveAccountUsersByLeadId(Integer leadId);
}
