package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.repository.TenantUnawareRepository;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface ITenantUnawareUserRepository extends TenantUnawareRepository<AccountUser,Integer>{
	
	@Query(" Select u.id FROM AccountUser u, UserRole ur " +
			" WHERE u.id = ur.user.id AND u.tenantId = :tenantId AND ur.role.id IN " +
			" (SELECT r.id FROM Role r WHERE r.name IN (:roleNameList)) ")
	List<Integer> getAccountUserIdsForRoleNames(@Param("roleNameList") List<String> roleNameList,@Param("tenantId") Long tenantId);

}
