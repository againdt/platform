package com.getinsured.hix.platform.notices.jpa;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.getinsured.hix.model.QueryData;
import com.getinsured.hix.platform.notices.QueryDataService;
import com.getinsured.hix.platform.repository.IQueryDataRepository;

@Service("queryDataService")
public class QueryDataServiceImpl implements QueryDataService {

	@Autowired IQueryDataRepository queryDataRepository;
	private EntityManager entityManager;
	 
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
	     this.entityManager = entityManager;
	}
	 
	@Override
	public QueryData findByName(String queryName){
		return queryDataRepository.findByName(queryName);
	}
	
	@Override
	public String mergeQueries(String tagQuery, String subQuery){
		StringBuilder queryStr = new StringBuilder();
		
		if(StringUtils.isNotEmpty(tagQuery)){
			queryStr.append(tagQuery);
		}
		if(StringUtils.isNotEmpty(subQuery)){
			queryStr.append(subQuery);
		}
		return queryStr.toString();
	}
	
	@Override
	public String generateDynamicQuery(String tagQueryName, String subQueryName, String jsonTagValues) {
		QueryData tagQueryData = this.findByName(tagQueryName);
		QueryData subQueryData = this.findByName(subQueryName);
		
		Query hqlQuery = entityManager.createNativeQuery(this.mergeQueries(tagQueryData.getTagQuery(), subQueryData.getSubscriberQuery()));
		List<Object> employeeList = hqlQuery.getResultList();
		String empdata = "Employee Data : -";
		if(employeeList.size() > 0){
		for(Object employeeOBJ : employeeList) {
				Object[] rowArray = (Object[]) employeeOBJ;
				String data = "";
				for (int i = 0; i < rowArray.length; i++) {
					data +=	rowArray[i] + " | ";
				}
				empdata += data + "\n";
			}
		}
		return empdata;
	}

	@Override
	public String generateDynamicQueryWithData(String tagQueryName, String subQueryName, Map<String, String> employeeData) throws JsonParseException, JsonMappingException, IOException {
		
		QueryData tagQueryData = this.findByName(tagQueryName);
		QueryData subQueryData = this.findByName(subQueryName);
        System.out.println(employeeData);
        String finalQuery = this.mergeQueries(tagQueryData.getTagQuery(), subQueryData.getSubscriberQuery());
        Set<String> keys = employeeData.keySet();
        for(String key: keys){
        	finalQuery = StringUtils.replace(finalQuery, ":"+key, employeeData.get(key));
        } 
        System.out.println(finalQuery);
		Query hqlQuery = entityManager.createNativeQuery(finalQuery);
		
		List<Object> employeeList = hqlQuery.getResultList();
		String empdata = "Employee Data : -";
		if(employeeList.size() > 0){
			for(Object employeeOBJ : employeeList) {
				Object[] rowArray = (Object[]) employeeOBJ;
				String data = "";
				for (int i = 0; i < rowArray.length; i++) {
					data +=	rowArray[i] + " | ";
				}
				empdata += data + "\n";
			}
		}
		return empdata;
	}
	
}
