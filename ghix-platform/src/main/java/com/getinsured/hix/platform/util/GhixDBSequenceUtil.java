package com.getinsured.hix.platform.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * Utility method to return nextval from given db sequence.
 *
 * @author golubenko_y
 */
@Component
public class GhixDBSequenceUtil
{
    private static final Logger logger = LoggerFactory.getLogger(GhixDBSequenceUtil.class);
    
    @PersistenceUnit
    private EntityManagerFactory emf;

    /**
     * Returns next sequence by executing native SQL statement.
     * <p>
     *     Native query is currently: <code>
     *         SELECT &lt;sequenceName&gt;.nextval FROM dual
     *     </code>
     * </p>
     * @param sequenceName name of the sequence
     * @return <code>nextval</code> for given sequence.
     */
    public String getNextSequenceFromDB(final String sequenceName)
    {
        String nextSequenceString = null;
        EntityManager em = null;

        try
        {
            em = emf.createEntityManager();
            String queryString = null;
            Object nextSequenceNo = null;
            
            if("Oracle".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
	    		queryString = "select "+sequenceName+".nextval from dual";
	    	}else if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
	    		queryString = "select nextval('"+sequenceName+"')";
	    	}

	    	if(queryString!=null){
	            Query query = em.createNativeQuery(queryString);
	            nextSequenceNo = query.getSingleResult();
	    	}

            if (nextSequenceNo != null)
            {
                nextSequenceString = nextSequenceNo.toString();
            }
            else
            {
                if(logger.isErrorEnabled())
                {
                    logger.error("Could not generate sequence.nextval for given sequence name: {}, DB  returned: {}",
                            sequenceName, nextSequenceNo);
                }

                throw new GIRuntimeException("Could not get sequence.nextval for given sequence name: " +
                        sequenceName + ", returned value from DB is NULL");
            }
        }
        catch (Exception e)
        {
            throw new GIRuntimeException(e.getMessage(), e);
        }
        finally
        {
            if(em != null)
            {
              em.clear();
              
              try
              {
                em.close();
              }
              catch(IllegalStateException ex) 
              {
                logger.error("Unable to close entity manager factory, it's managed by application container", ex);
              }
            }
        }

        return nextSequenceString;
    }
}
