package com.getinsured.hix.platform.emailstat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.getinsured.hix.platform.emailstat.model.EmailEvents;


public interface IEmailEventsRepository extends JpaRepository<EmailEvents, Integer>{

}
