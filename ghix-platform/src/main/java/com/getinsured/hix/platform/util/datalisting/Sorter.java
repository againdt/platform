package com.getinsured.hix.platform.util.datalisting;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class Sorter extends TagSupport {
	private static final long serialVersionUID = 1L;
 
	private String sortBy;
	private String title;
	private String sortOrder;
    private HttpServletRequest request;
    private JspWriter out;
    private String customFunctionName;
   
    private String getURL() throws UnsupportedEncodingException{
    	StringBuilder url = new StringBuilder(this.request.getAttribute("javax.servlet.forward.request_uri").toString()).append("?");
    	
    	url.append("sortBy="+ this.sortBy +"&");
		url.append("changeOrder=true&");
		url.append("sortOrder="+ this.sortOrder +"&");
		
		List<String> prameterList   = new ArrayList<String>();
		prameterList.add("sortBy");
		prameterList.add("changeOrder");
		prameterList.add("sortOrder");
		prameterList.add("pageNumber");
		
    	Enumeration<?> keys = request.getParameterNames(); 
    	while (keys.hasMoreElements() ) {  
  	      	String key   = (String)keys.nextElement();  
  	      	String value = (request.getParameter(key) == null) ? "" : request.getParameter(key);
  	      	if(prameterList.contains(key) || value.equals("") ){
  	      		continue;
  	      	}
	  	  
  	      	if(key.equals("csrftoken")){
		      		key="";
		      		value="";
		    }else{
      		url.append(URLEncoder.encode(key, "UTF-8"));
      		url.append("=");
      		url.append(URLEncoder.encode(value, "UTF-8")+"&");
    	}
    	}
    	
    	url = url.deleteCharAt( url.lastIndexOf("&" ) );
    	return url.toString();
    }
    
    private void setProperties(){
    	this.request = (HttpServletRequest)pageContext.getRequest();
    	this.out = pageContext.getOut();
    }
	
    private String createSort() throws UnsupportedEncodingException{

    	String sort = null;
        if (customFunctionName != null && !customFunctionName.isEmpty()) {
            
            sort = " <a href='javascript:"+ customFunctionName + "(\"" + this.getURL() + "\")'>" + this.title +" </a>";
	    } else {
	            
	            sort = " <a href='"+ this.getURL() +"'>"+ this.title +" </a>";
	    }
    	    	
    	return sort;
    }
    
	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		try {
	            out.write(this.createSort());
	        } catch (Exception ex) {
	            throw new JspException("Error in Paginator tag", ex);
	        }
	    return SKIP_BODY;
	}
		
	
	
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public String getCustomFunctionName() {
        return customFunctionName;
	}
	
	public void setCustomFunctionName(String customFunctionName) {
	        this.customFunctionName = customFunctionName;
	}
}
