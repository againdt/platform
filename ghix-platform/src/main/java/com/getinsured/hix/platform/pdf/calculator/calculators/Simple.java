package com.getinsured.hix.platform.pdf.calculator.calculators;

import net.minidev.json.JSONArray;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.getinsured.hix.platform.util.exception.ApplicationDataException;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

/**
 * Created by ivanbastidas on 4/24/14.
 */
public class Simple implements FieldCalculator {

    private static final Logger logger = Logger.getLogger(Simple.class);

    @Override
    public String execute(String applicationJson, JsonNode fieldValue) throws ApplicationDataException {
        try {
            String fieldJsonPath = fieldValue.asText();
            if (fieldJsonPath.equals("")) {
                return "";
            }

            if (checkAutoConstant(fieldJsonPath)) {
                return fieldJsonPath;
            }

            Object valuePath = JsonPath.read(applicationJson, fieldJsonPath);
            if (valuePath instanceof JSONArray) {
                JSONArray array = (JSONArray) valuePath;
                if (array != null && !array.isEmpty()) {
                    if (array.get(0) instanceof String) {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0, size = array.size(); i < size; i++) {
                            sb.append(array.get(i));
                            if (i + 1 < size) {
                                sb.append(", ");
                            }
                        }
                        return sb.toString();
                    }
                }
            } else if (valuePath != null) {
				Object o = JsonPath.read(applicationJson, fieldJsonPath);
                return String.valueOf(o);
            }
        } catch (PathNotFoundException e) {
            logger.debug(String.format("Path '%s' not found", fieldValue));
        }
        return null;
    }

    protected boolean checkAutoConstant(String jsonPath) {
        return (!jsonPath.equals("") && !jsonPath.substring(0, 1).equals("$"));
    }

}
