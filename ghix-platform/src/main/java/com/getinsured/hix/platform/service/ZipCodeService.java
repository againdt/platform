package com.getinsured.hix.platform.service;

import java.util.List;

import com.getinsured.hix.model.ZipCode;

public interface ZipCodeService {
	
	public ZipCode findByZipCode(String zip);
	
	List<ZipCode> findByZipAndStateCode(String zip, String stateCode);
	
	public List<ZipCode> getCityList(String city);
	
	public List<ZipCode> getZipList(String zipCode);
	
	public boolean validateZipAndCountyCode(String zipCode, String countyCode);
	
	public List<ZipCode> findByZip(String zip);
	
	public boolean validateZipByState(String statename, String zipCode);
	
	public List<String> getZipByCountyCodeAndZips(List<String> zips,String countyCode);

	public ZipCode getZipcodeByZipAndCountyCode(String zip, String countyCode);
	
	public String findCountyNameByCountyCode(String countyCode);
	
	public List<String> getCountyCodes(List<String> zips,String countyCode);
	
	public List<ZipCode> getCountyListForState(String stateCode);
	
	public List<ZipCode> getCountyListForZipAndState(String zip,String stateCode);

	public String getCountyCodeByStateAndCounty(String state, String county);
	
	public String findCountyNameByZipStateAndCountyCD(String zip, String state, String countyCode);
	
	public String findStateFIPSByZipStateAndCountyCD(String zip, String state,String countyCode);
	
	public boolean validateZipAndCountyCodeAndState(String zipCode, String countyCode,String stateCode);

	List<String> getStateListByAreaCode(String areaCode);


}
