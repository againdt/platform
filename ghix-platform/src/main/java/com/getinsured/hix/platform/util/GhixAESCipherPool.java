package com.getinsured.hix.platform.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.timeshift.TimeShifterUtil;

public class GhixAESCipherPool{
	public static final String FORMAT = "yyyy-MMM-dd HH:mm:ss zzz"; 
	private static Logger logger = LoggerFactory.getLogger(GhixAESCipherPool.class);
	private static SecretKeySpec secret = null;
	//Encryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeEncryptorPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyEncryptorPool = new Stack<Cipher>();
	
	//Decryption banks , total cipher object count across these banks is always equal to the pool size
	private static Stack<Cipher> activeDecryptionPool = new Stack<Cipher>();
	private static Stack<Cipher> standbyDecryptionPool = new Stack<Cipher>();
	private static SecureRandom random  = new SecureRandom();
	private static final int POOL_SIZE = 100;
	private static boolean poolInitialized = false;
	
	
	public static String decrypt( String encryptedStr) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		String plainText = null;
		long start = TimeShifterUtil.currentTimeMillis();
		try{
			if(encryptedStr == null){
				throw new RuntimeException("IV and input value for decryption are required for decryption");
			}
			if(!poolInitialized){
				initializCipherPools(POOL_SIZE);
			}
			byte[] encData = Base64.decodeBase64(encryptedStr); 
			byte[] iv = new byte[16];
			System.arraycopy(encData, 0, iv, 0, 16);
			byte[] cipherTextToken = new byte[encData.length-16];
			System.arraycopy(encData, 16, cipherTextToken, 0, encData.length-16);
			Cipher cipher = null;
			try{
				cipher = getActiveDecryptorPool().pop();
			}catch(EmptyStackException es){
				logger.warn("Validate token failed to retrieve the cipher environment from the pool, initializing new");
				cipher = getDecryptionCipherEnv();
			}
			cipher.init(Cipher.DECRYPT_MODE, secret,new IvParameterSpec(iv));
			plainText = new String(cipher.doFinal(cipherTextToken), "UTF-8");
			standbyDecryptionPool.push(cipher); // Return this cipher to the pool
			int hashIndex = plainText.indexOf("#");
			if(hashIndex != -1){
				return plainText.substring(hashIndex+1);
			}
		}catch(IndexOutOfBoundsException | ArrayStoreException ae){
			throw new RuntimeException("decryption failed with :"+ae.getMessage(), ae);
		}
		if(logger.isTraceEnabled()){
			logger.info("Decryption took:"+(TimeShifterUtil.currentTimeMillis()-start)+" ms");
		}
		return plainText;
	}
	
	
	private static synchronized void initializCipherPools(int poolSize ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(poolInitialized){
			return;
		}
		
		String password = GhixSecretKey.getEncryptionKey(); // Can not be null
		Cipher cipher = null;
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			activeEncryptorPool.add(cipher);
		}
		for(int i = 0; i < poolSize; i++){
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			activeDecryptionPool.add(cipher);
		}
		poolInitialized = true;
	}
	
	private static Cipher getDecryptionCipherEnv() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Decryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		return cipher;
	}
	
	private static synchronized Cipher getEncryptionCipherEnv() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		if(secret == null){
			// An impossible condition considering pools have already been initialized
			throw new RuntimeException("Encryption Environment: Fatal Error, pools should have been initialized by now");
		}
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		return cipher;
	}
	
	private static synchronized Stack<Cipher> getActiveEncryptorPool(){
		Stack<Cipher> emptyBank;
		try{
			activeEncryptorPool.peek();
		}catch(EmptyStackException es){
			emptyBank = activeEncryptorPool;
			activeEncryptorPool = standbyEncryptorPool;
			standbyEncryptorPool = emptyBank;
		}
		return activeEncryptorPool;
	}
	
	private static synchronized Stack<Cipher> getActiveDecryptorPool(){
		Stack<Cipher> emptyBank;
		try{
			activeDecryptionPool.peek();
		}catch(EmptyStackException es){
			emptyBank = activeDecryptionPool;
			activeDecryptionPool = standbyDecryptionPool;
			standbyDecryptionPool = emptyBank;
		}
		return activeDecryptionPool;
	}
	
	/**
	 * 
	 * @param password
	 * @param trustedEntityName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 */
	public static String encrypt(String plainText) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		long start = TimeShifterUtil.currentTimeMillis();
		String returnStr = null;
		if(!poolInitialized){
			initializCipherPools(POOL_SIZE);
		}
		Cipher cipher = null;
		try{
			cipher = getActiveEncryptorPool().pop();
		}catch(EmptyStackException es){
			logger.warn("Generate Token Failed to get cipher env from the pool, initializing a new one");
			cipher = getEncryptionCipherEnv();
		}
		String plainTextIn = random.nextDouble()+"#"+plainText;
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal((plainTextIn).getBytes("UTF-8"));
		byte[] enc = new byte[iv.length+ciphertext.length];
		System.arraycopy(iv, 0, enc, 0, iv.length);
		System.arraycopy(ciphertext, 0, enc, iv.length, ciphertext.length);
		standbyEncryptorPool.push(cipher); //Send the cipher environment to standby pool
		returnStr =new String(Base64.encodeBase64URLSafe(enc));
		if(logger.isTraceEnabled()){
			logger.trace("Encryption took:"+(TimeShifterUtil.currentTimeMillis()-start)+" ms");
		}
		return returnStr;
	}
	
	public static String encryptParameterList(List<String> parameters){
		if(parameters.size() == 0){
			return "";
		}
		StringBuilder builder = new StringBuilder();
		
		try {
			for (String param : parameters) {
				if(builder.length() > 0) {
					builder.append("/");
				}
				builder.append(encrypt(param));
			}
			
			return builder.toString();
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			logger.error("Failed to encrypt the List",e);
		}
		return null;
	}
	
	public static String encryptParameterMap(Map<String,String> parameters){
		if(parameters.size() == 0){
			return "";
		}
		StringBuilder builder = new StringBuilder();
		Set<Entry<String, String>> entries = parameters.entrySet();
		Iterator<Entry<String, String>> cursor = entries.iterator();
		Entry<String, String> entry = null;
		while(cursor.hasNext()){
			entry = cursor.next();
			builder.append(entry.getKey()+"="+entry.getValue()+"&");
		}
		String str = builder.toString().trim();
		try {
			return encrypt(str.substring(0, str.length()-1));
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to encrypt the map",e);
		}
		return null;
	}
	
	/**
	 * This method should be called from JSP as there is no type checking in JSP code, and ends up in adding other data types 
	 * in map. 
	 * @param parameters
	 * @return
	 */
	public static String encryptGenericParameterMap(Map<String,Object> parameters){
		if(parameters.size() == 0){
			return "";
		}
		StringBuilder builder = new StringBuilder();
		Set<Entry<String, Object>> entries = parameters.entrySet();
		Iterator<Entry<String, Object>> cursor = entries.iterator();
		Entry<String, Object> entry = null;
		Object val = null;
		String key = null;
		while(cursor.hasNext()){
			entry = cursor.next();
			key = entry.getKey();
			val = entry.getValue();
			builder.append(key+"="+val.toString()+"&");
		}
		String str = builder.toString().trim();
		try {
			return encrypt(str.substring(0, str.length()-1));
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to encrypt the map",e);
		}
		return null;
	}
	
	public static Map<String,String> decryptParameterMap(String encryptedMap){
		HashMap<String, String> map = new HashMap<String, String>();
		if(encryptedMap == null || encryptedMap.length() == 0){
			return map;
		}
		
		try {
			String decrypted = decrypt(encryptedMap);
			String paramSets[] = decrypted.split("&");
			int equalIdx = -1;
			for(String str : paramSets){
				equalIdx = str.indexOf("=");
				map.put(str.substring(0,equalIdx), str.substring(equalIdx+1));
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	public static void main(String[] args) throws Exception{
		String testString = "20-";
		
		for(int i = 0; i < 10; i++){
			String enc = encrypt(testString+i);
			System.out.println("Encrypted:"+enc.toString());
			System.out.println("Decrypted:"+decrypt(enc));
		}
	}
}
