package com.getinsured.hix.platform.emailstat.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="email_stats")
public class EmailStats implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMAIL_STATS_SEQ")
	@SequenceGenerator(name = "EMAIL_STATS_SEQ", sequenceName = "EMAIL_STATS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="notice_id")
	private int noticeId;

	@Column(name="meta_data")
	private String metaData;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}
	
}
