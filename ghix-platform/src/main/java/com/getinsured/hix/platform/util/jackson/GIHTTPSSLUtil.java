package com.getinsured.hix.platform.util.jackson;

import java.io.File;
import java.net.URL;
import java.security.KeyStore;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.apache.log4j.Logger;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

/**
 * <p>
 * GIHTTPSSLUtil can be used to generate {@link HttpClient}  with the provided {@link KeyStore} of the
 * HTTPS server against a list of trusted certificates and to authenticate to
 * the HTTPS server using a private key.
 * </p>
 * 
 * <p>
 *		Before Using GIHTTPSSLUtil.getHTTPSClient try to furnish following steps.
 * </p>
 * <ul>
 * 		<li>Convert the provided pfx certificate file into jks file to act as KeyStore for HTTPS client authentication.
 * 			<ul>
 * 				<li>If certificate is in pfx format
 * 				<p>
 * 					Download jetty-6.1.11 jar.Keep PFX file and jar in same folder and fire below command.
 * 					<pre>filepath>java -classpath .;jetty-6.1.11.jar org.mortbay.jetty.security.PKCS12Import test-getinsured.pfx output.jks</pre>
 * 					For simplicity use the same password for the key as that of the keystore
 * 				</p>				
 * 				</li>
 * 
 * 				<li>if the certificate in cer format
 * 				<p>
 * 					Use JDK keytool utility to generate a keystore
 * 					<pre>filepath>keytool -import -file jacksoncacert.cer -keystore jacksontrustore</pre>
 * 					For simplicity use the same password for the key as that of the keystore
 * 				</p>
 * 				</li>
 * 			</ul>
 * 		</li>
 * 		<li>Convert the provided certificate into jks with private key to act as trustStore for HTTPS server authentication. 
 * 			<ul>
 * 				<li>If certificate is in pfx format
 * 				<p>
 * 					1.Import the pfx file in Internet Explorer with private key.
 * 					2.Export the added certificate with private key in cer fromat.
 * 					Now follow the steps for cer to jks convertion 
 * 				</p>
 * 				</li>
 * 				<li>if the certificate in cer format
 * 				<p>
 * 					Use JDK keytool utility to generate a keystore
 * 					<pre>filepath>keytool -import -file jacksoncacert.cer -keystore jacksontrustore</pre>
 * 					For simplicity use the same password for the key as that of the keystore
 * 				</p>
 * 				</li>
 * 			</ul>
 * 		</li>
 * </ul>
 * 
 * 
 * 
 * @since  29 Nov 2013
 * @author Biswakalyan
 *
 *
 */
@SuppressWarnings("deprecation")
public class GIHTTPSSLUtil {
	private static final Logger LOGGER = Logger.getLogger(GIHTTPSSLUtil.class);
	private String destinationWSDL;
	private String keyStoreFilePath;
	private String trustStoreFilePath;
	private String keyStoreFileName;
	private String trustStoreFileName;
	private String keyStorePassword;
	private String trustStorePassword;
	private File keyStoreFile;
	private File trustStoreFile;
	
	public String getDestinationWSDL() {
		return destinationWSDL;
	}

	public void setDestinationWSDL(String destinationWSDL) {
		this.destinationWSDL = destinationWSDL;
	}

	public String getKeyStoreFilePath() {
		return keyStoreFilePath;
	}

	public void setKeyStoreFilePath(String keyStoreFilePath) {
		this.keyStoreFilePath = keyStoreFilePath;
	}

	public String getTrustStoreFilePath() {
		return trustStoreFilePath;
	}

	public void setTrustStoreFilePath(String trustStoreFilePath) {
		this.trustStoreFilePath = trustStoreFilePath;
	}

	public String getKeyStoreFileName() {
		return keyStoreFileName;
	}

	public void setKeyStoreFileName(String keyStoreFileName) {
		this.keyStoreFileName = keyStoreFileName;
	}

	public String getTrustStoreFileName() {
		return trustStoreFileName;
	}

	public void setTrustStoreFileName(String trustStoreFileName) {
		this.trustStoreFileName = trustStoreFileName;
	}

	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public void setTrustStorePassword(String trustStorePassword) {
		this.trustStorePassword = trustStorePassword;
	}

	/**
	 * 
	 * Constructor for GIHTTPSSLUtil. a destinationWSDL and a keystore and a
	 * truststore file must be given. Otherwise SSL context initialization error
	 * will result in {@link GIAuthSSLProtocolSocketFactory}.
	 * 
	 * @param destinationWSDL
	 * @param keyStoreFilePath
	 * 			String full file path of the keystore file for HTTPS client
	 *            authentication.This should not contain the file name with path. 
	 * @param keyStoreFileName
	 * 			String file Name of the keystore file for HTTPS client
	 *            authentication.
	 * @param keyStorePassword
	 * 			String password of the keystore file. 
	 * @param trustStoreFilePath
	 * 			String full file path of the truststore file for HTTPS
	 *            server authentication.This should not contain the file name with path.
	 * @param trustStoreFileName
	 * 			String file Name of the truststore file for HTTPS client
	 *            authentication.
	 * @param trustStorePassword
	 * 			String password of the truststore file. 
	 * 
	 * @throws GIRuntimeException
	 */
	public GIHTTPSSLUtil(String destinationWSDL,String keyStoreFilePath,String keyStoreFileName,String keyStorePassword,
			String trustStoreFilePath,String trustStoreFileName,String trustStorePassword)throws GIRuntimeException{
		if(StringUtils.isNotEmpty(destinationWSDL)&& StringUtils.isNotEmpty(keyStoreFileName)&& StringUtils.isNotEmpty(trustStoreFileName)){
			try {
				this.destinationWSDL = destinationWSDL;
				this.keyStoreFilePath = keyStoreFilePath;
				this.trustStoreFilePath = trustStoreFilePath;
				this.keyStorePassword = keyStorePassword==null?"":keyStorePassword;
				this.trustStorePassword = trustStorePassword==null?"":trustStorePassword;
				if(StringUtils.isNotEmpty(keyStoreFilePath) && StringUtils.isNotEmpty(trustStoreFilePath)){
					this.keyStoreFile = new File(keyStoreFilePath+File.separator+keyStoreFileName);
					this.trustStoreFile = new File(trustStoreFilePath+File.separator+trustStoreFileName);
					LOGGER.info("keystore path:"+keyStoreFilePath+". and truststore path provided:"+trustStoreFilePath);
				}else{
					this.keyStoreFile = new ClassPathResource(keyStoreFileName).getFile();
					this.trustStoreFile = new ClassPathResource(trustStoreFileName).getFile();
				}
			} catch (Exception e) {
				//Just making a Log for exception to overcome server start up issue if file not found.
				LOGGER.error("keystore and truststore files are not available for SSL handshaking.", e);
			}
			
		}else{
			throw new GIRuntimeException("PLATFORM-00001", null, "Provided file names for keystores and destination url should not be empty.", Severity.HIGH);
		}
	}
	
	
	
	/**
	 * Attempts to get a new http clinet {@link HttpClient} with configured protocol {@link Protocol}, host and url for https service.
	 * This Helps to configure provided SSL certificate service call.  
	 * 
	 * 
	 * @return {@link HttpClient}
	 * @throws GIRuntimeException
	 */
	public HttpClient getHTTPSClient() throws GIRuntimeException{
		 HttpClient httpclient = new HttpClient();
		try {
			URL keystoreUrl = null,truststoreUrl = null;
			URL destinationUrl = new URL(getDestinationWSDL());
			
			if(keyStoreFile.isFile() && trustStoreFile.isFile() && StringUtils.isNotEmpty(getKeyStorePassword())){
				keystoreUrl = keyStoreFile.toURI().toURL();
				truststoreUrl = trustStoreFile.toURI().toURL();
			}else{
				throw new GIRuntimeException("PLATFORM-00001", null, "Provided file path define a dirotory, This should speicify a file.", Severity.HIGH);
			}
			
			Protocol customProtocol = new Protocol(destinationUrl.getProtocol(), new GIAuthSSLProtocolSocketFactory( keystoreUrl, getKeyStorePassword(),truststoreUrl,getTrustStorePassword()), destinationUrl.getPort());
			Protocol.registerProtocol("https", customProtocol);
			httpclient.getHostConfiguration().setHost(destinationUrl.getHost(), destinationUrl.getPort(), customProtocol);
			
		} catch (Exception e) {
			throw new GIRuntimeException("PLATFORM-00001",e,null,Severity.HIGH);
		}
		return httpclient;
	}
	
}
