package com.getinsured.hix.platform.accountactivation;

import java.text.SimpleDateFormat;
import com.getinsured.timeshift.util.TSDate;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;

/**
 * @author chopra_s
 *
 */
@Component("ReferralAccountActivationEmail")
@DependsOn("dynamicPropertiesUtil")
public class ReferralAccountActivationEmail extends AccountActivationEmail {

	protected String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
	protected String exchangePhone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE);
	protected String exchangeURL = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);

	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj  = (ActivationJson) notificationContext.get("ACTIVATION_JSON");
		Map<String, String> data = new HashMap<>();
		data.put("To", activationJsonObj.getCreatedObject().getEmailId());
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(!"NV".equalsIgnoreCase(stateCode)) {
			data.put("Subject", "A record has been created for you on the " + exchangeName + " Exchange");
		}
		data.put("KeyId", (String)notificationContext.get("KeyId"));
		data.put("KeyName", (String)notificationContext.get("KeyName"));
		return data;
	}
	
	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson) notificationContext.get("ACTIVATION_JSON");
		AccountActivation accountActivation = (AccountActivation) notificationContext.get("ACCOUNT_ACTIVATION");
		Location location = (Location) notificationContext.get("LOCATION");
		Map<String,String> bean = new HashMap<>();
		String activationUrl = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL + "account/user/activation/" + accountActivation.getActivationToken();
		bean.put("userAccountName", activationJsonObj.getCreatedObject().getFullName());
	
		Map<String, String> map = activationJsonObj.getCreatedObject().getCustomeFields();

		TSDate date = new TSDate();
		bean.put("exchangeName", exchangeName );
		bean.put("accessCode", map.getOrDefault("accessCode", ""));
		bean.put("caseNumber", map.getOrDefault("caseNumber", ""));
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		bean.put("spanishDate", formatter.format(date));
		bean.put("todaysDate", DateUtil.dateToString(date, "MMMM dd, YYYY"));
		bean.put("exchangePhone", exchangePhone);
		bean.put("exchangeURL", exchangeURL);
		bean.put("expirationDays", map.getOrDefault("expirationDays", "0"));
		bean.put("activationUrl", activationUrl );
		bean.put("addressLine1", location.getAddress1());
		bean.put("addressLine2", location.getAddress2());
		bean.put("cityName", location.getCity());
		bean.put("stateCode", location.getState());
		bean.put("pinCode", location.getZip());
		bean.put("name", activationJsonObj.getCreatedObject().getFullName());
		bean.put("externalAppId", map.getOrDefault("externalAppId", ""));

		return bean;
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
