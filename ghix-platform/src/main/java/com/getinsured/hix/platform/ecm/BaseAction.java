package com.getinsured.hix.platform.ecm;

import org.apache.log4j.Logger;

public abstract class BaseAction {
	private String contentId;
	private String contentPath;
	private String fieldName;
	private Object content;

	protected static final Logger LOGGER = Logger.getLogger(BaseAction.class);

	public abstract void initialize(Object object) throws ContentManagementServiceException;

	public abstract java.lang.Object execute()
			throws ContentManagementServiceException;

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public String getContentPath() {
		return contentPath;
	}

	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}
}
