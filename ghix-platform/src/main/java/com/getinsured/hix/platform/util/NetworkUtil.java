package com.getinsured.hix.platform.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetworkUtil {
	private static Logger logger = LoggerFactory.getLogger(NetworkUtil.class);
	
	// For now supporting IPV4 Addresses only
	public static List<String> getLocalIpAddress() 
	{
		List<String> addresses = new ArrayList<>();
		String address = null;
		try 
		{
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
			{
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
				{
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) 
					{
						address = inetAddress.getHostAddress().toString();
						if(InetAddressUtils.isIPv4Address(address)){
							addresses.add(address);
						}
					}
				}
			}
		} 
		catch (SocketException ex) 
		{
			logger.error("Failed finding the IP address of this node".intern(),ex);
		}
		return addresses;
	}
	
	public boolean isHostReachable(String ip, int timeout){
		boolean state = false;
	    try {
	        state = InetAddress.getByName(ip).isReachable(timeout);
	    } catch (IOException e) {
	        //Parse error here
	    }

	    return state;
	}
}
