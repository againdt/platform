package com.getinsured.hix.platform.clamav;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class ClamConnection {
	
	private static Logger LOGGER = LoggerFactory.getLogger(ClamConnection.class);
	private static final int RESPONSE_CHUNK_SIZE = 1024;
	private static long objectCount = 0;
	private Socket socket = null;
	private InputStream in = null;
	private DataOutputStream out = null;
	private String host;
	private int port;
	private int timeout;
	private long objectId = 0;
	private HashMap<Long, StringBuilder> localBuffer = new HashMap<>();
	private SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm:SS");
	private long lastActiveTime;
	private ClamAvPing pingThread;
	private Timer timer;
	private boolean closed = false;
	
	
	
	public ClamConnection(String host, int port, int timeout){
		this.host = host;
		this.port = port;
		this.timeout = timeout;
		this.objectId = objectCount++;
		this.establishConnectionIfRequired();
	}
	
	public void log(String message){
		if(LOGGER.isDebugEnabled()){
			long id = Thread.currentThread().getId();
			StringBuilder sb = this.localBuffer.get(id);
			if(sb == null){
				sb = new StringBuilder();
				this.localBuffer.put(id, sb);
			}
			sb.append("DEBUG :["+sdf.format(new TSDate(System.currentTimeMillis()))+"]----OBJECT_ID:"+this.objectId+"-THREAD ID:"+id+"----"+message+"\n");
		}
	}
	
	public void printLog(){
		if(LOGGER.isDebugEnabled() ){
			StringBuilder sb = this.localBuffer.remove(Thread.currentThread().getId());
			if(sb != null){
				LOGGER.debug(sb.toString());
			}
		}
	}
	
	private void establishConnectionIfRequired(){
		if(this.socket == null || !socket.isConnected()){
			socket = new Socket();
	        try {
	            socket.connect(new InetSocketAddress(host, port));
	            socket.setSoTimeout(timeout);
	            write(new String("zIDSESSION\0").getBytes());
	            lastActiveTime = TimeShifterUtil.currentTimeMillis();
	            log("Connection to "+host+" established successfully");
	            if(pingThread == null){
					log("OBJECT_ID:"+objectId+"---Ping deamon initiated");
					pingThread = new ClamAvPing(this);
				}
				timer = new Timer();
				timer.schedule(pingThread,1000,2*60*1000);
	        } catch (IOException e) {
	            LOGGER.error("could not connect to clamd server ["+host+" at port:"+port+"] failed with error: "+e.getMessage(), e);
	            if(socket.isConnected()){
	            	try {
	            		write(new String("zEND\0").getBytes());
						socket.close();
						socket = null;
						closed = true;
					} catch (IOException e1) {
						//Ignored
					}
	            }
	            throw new GIRuntimeException("Connect to :"+host+" at port:"+port+" Failed with error:"+e.getMessage(), e);
	        }
		}
	}
	
	public void write(byte[] bytes) throws IOException{
		if(this.out == null){
			this.out = new DataOutputStream(socket.getOutputStream());
		}
		try{
		this.out.write(bytes);
			this.lastActiveTime = TimeShifterUtil.currentTimeMillis();
		}catch(IOException e){
			log("Error writing the data to Clam AV "+e.getMessage());
			throw e;
		}
	}
	
	public void writeInt(int i) throws IOException{
		if(this.out == null){
			this.out = new DataOutputStream(socket.getOutputStream());
		}
		try{
		this.out.writeInt(i);
			this.lastActiveTime = TimeShifterUtil.currentTimeMillis();
		}catch(IOException e){
			log("Error writing the data to Clam AV "+e.getMessage());
			throw e;
		}
	}
	
	public void write(byte[] bytes, int startIndex, int len) throws IOException{
		if(this.out == null){
			this.out = new DataOutputStream(socket.getOutputStream());
		}
		try{
		this.out.write(bytes, startIndex, len);
			this.lastActiveTime = TimeShifterUtil.currentTimeMillis();
		}catch(IOException e){
			log("Error writing the data to Clam AV "+e.getMessage());
			throw e;
		}
	}
	
	public int read() throws IOException{
		if(this.in == null){
			this.in = socket.getInputStream();
		}
		try{
		return this.in.read();
		}catch(IOException e){
			log("Error reading the data from Clam AV "+e.getMessage());
			throw e;
		}
	}
	
	public int read(byte[] buf, int offset, int len) throws IOException{
		if(this.in == null){
			this.in = socket.getInputStream();
		}
		try{
		return this.in.read(buf, offset, len);
		}catch(IOException e){
			log("Error reading the data from Clam AV "+e.getMessage());
			throw e;
		}
	}
	
	public String readString() throws IOException{
		if(this.in == null){
			this.in = socket.getInputStream();
		}
		InputStreamReader reader = new InputStreamReader(in);
		StringBuilder sb = new StringBuilder();
		char[] buffer = new char[RESPONSE_CHUNK_SIZE];
		int read = RESPONSE_CHUNK_SIZE;
		while (read == RESPONSE_CHUNK_SIZE)
        {
			try{
            read = reader.read(buffer);
			}catch(IOException e){
				log("Error reading the data from Clam AV "+e.getMessage());
				throw e;
			}
            sb.append(buffer, 0, read);
            
            if (read == -1)
            {
                break;
            }
            
        }
		sb.trimToSize();
		String response = sb.toString();
		log("Response:"+response);
		this.lastActiveTime = TimeShifterUtil.currentTimeMillis();
		return response;
	}
	
	public void close(){
		if(closed){
			return;
		}
		log("Closing the Clam connection");
		try{
			if(timer != null){
				this.timer.cancel();
			}
			if(this.socket.isConnected()){
				write(new String("zEND\0").getBytes());
				this.socket.close();
			}
			if(this.in != null){
				this.in.close();
				this.in = null;
			}
			if(this.out != null){
				this.out.close();
				this.out = null;
			}
			this.printLog();
			this.socket = null;
			this.timer = null;
		}catch(Exception ie){
			LOGGER.error("Error closing the Clam AV connection, Can be ignored "+ie.getMessage());
		}
		closed = true;
	}

	public boolean validate() {
		if(closed){
			return false;
		}
		if(this.socket != null && this.socket.isConnected() ){
			return true;
		}
		log("Connection validation failed, pooled object elgible for discard");
		return false;
	}

	public void finish(long start) {
		log("Finished scanning in "+(TimeShifterUtil.currentTimeMillis()-start)+" ms");
		this.printLog();
		
	}

	public long getLastPingedTime() {
		return this.lastActiveTime;
	}

	public void setLastPingedTime(long lastPingedTime) {
		this.lastActiveTime = lastPingedTime;
	}

	public long getObjectId() {
		return this.objectId;
	}

}
