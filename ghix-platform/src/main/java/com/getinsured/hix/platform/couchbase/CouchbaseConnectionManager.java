package com.getinsured.hix.platform.couchbase;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.getinsured.hix.platform.config.CBEnvironment;

/**
 * Creates a connection to the Couchbase server(s) and
 * opens configured buckets for use.
 *
 * @see com.getinsured.hix.platform.config.CacheConfiguration
 * @see CBEnvironment
 */
@Service("couchbaseConnectionManager")
public class CouchbaseConnectionManager
{
  private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseConnectionManager.class);

  @Value("#{configProp['couchbase.nodes']}")
  private List<String> nodes = new ArrayList<>();

  @Value("#{configProp['couchbase.binary.bucketname']}")
  private String binaryBucketName;

  @Value("#{configProp['couchbase.nonbinary.bucketname']}")
  private String nonbinaryBucketName;

  @Value("#{configProp['couchbase.password']}")
  private String password;

  @Autowired
  private CBEnvironment environment;

  private Cluster cluster;
  private Bucket binaryBucket;
  private Bucket nonbinaryBucket;

  /**
   * Initializes connection to Couchbase cluster and
   * opens configured buckets.
   */
  @PostConstruct
  public void createSession()
  {
    if (nodes != null && nodes.size() != 0)
    {
      try
      {
        cluster = CouchbaseCluster.create(environment.environment(), nodes);

        binaryBucket = cluster.openBucket(binaryBucketName, password);
        nonbinaryBucket = cluster.openBucket(nonbinaryBucketName, password);

        if (LOGGER.isInfoEnabled())
        {
          LOGGER.info("Couchbase server(s): {}", String.join(",", nodes));
          LOGGER.info("Couchbase bucket(s):");
          LOGGER.info("             binary: {}", binaryBucket);
          LOGGER.info("         non-binary: {}", nonbinaryBucket);
        }
      } catch (Exception e)
      {
        if (LOGGER.isErrorEnabled())
        {
          LOGGER.error("Couchbase: Unable to get connection to node(s): {}", nodes);
          LOGGER.error("Couchbase: buckets: {}, {}", binaryBucketName, nonbinaryBucketName);
          LOGGER.error("Couchbase: environment used: {}", environment.environment());
          LOGGER.error("Couchbase: connection exception:", e);
        }
      }
    }
    else
    {
      if (LOGGER.isWarnEnabled())
      {
        LOGGER.warn("Couchbase not configured.");
      }
    }
  }

  /**
   * Cleaning up Couchbase connections to cluster and buckets.
   */
  @PreDestroy
  public void preDestroy()
  {
    if(LOGGER.isInfoEnabled())
    {
      LOGGER.info("@PreDestroy called on CouchbaseConnectionManager, doing clean-up.");
    }

    if(nonbinaryBucket != null)
    {
      boolean closed = nonbinaryBucket.close();

      if(LOGGER.isInfoEnabled())
      {
        LOGGER.info("Closed non-binary bucket: {}", closed);
      }
    }

    if(binaryBucket != null)
    {
      boolean closed = binaryBucket.close();

      if(LOGGER.isInfoEnabled())
      {
        LOGGER.info("Closed binary bucket: {}", closed);
      }
    }

    if (cluster != null)
    {
      boolean disconnected = cluster.disconnect();

      if (disconnected && LOGGER.isInfoEnabled())
      {
        LOGGER.info("Disconnected from couchbase successfully");
      }
      else if (!disconnected)
      {
        if (LOGGER.isErrorEnabled())
        {
          LOGGER.error("Unable to cleanly disconnect from the couchbase! If current JVM process was killed, it's ok.");
        }
      }
    }
  }

  public List<String> getNodes()
  {
    return nodes;
  }

  public void setNodes(List<String> nodes)
  {
    this.nodes = nodes;
  }

  public String getBinaryBucketName()
  {
    return binaryBucketName;
  }

  public void setBinaryBucketName(String binaryBucketName)
  {
    this.binaryBucketName = binaryBucketName;
  }

  public String getNonbinaryBucketName()
  {
    return nonbinaryBucketName;
  }

  public void setNonbinaryBucketName(String nonbinaryBucketName)
  {
    this.nonbinaryBucketName = nonbinaryBucketName;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public CBEnvironment getEnvironment()
  {
    return environment;
  }

  public void setEnvironment(CBEnvironment environment)
  {
    this.environment = environment;
  }

  public Cluster getCluster()
  {
    return cluster;
  }

  public void setCluster(Cluster cluster)
  {
    this.cluster = cluster;
  }

  public Bucket getBinaryBucket()
  {
    return binaryBucket;
  }

  public void setBinaryBucket(Bucket binaryBucket)
  {
    this.binaryBucket = binaryBucket;
  }

  public Bucket getNonbinaryBucket()
  {
    return nonbinaryBucket;
  }

  public void setNonbinaryBucket(Bucket nonbinaryBucket)
  {
    this.nonbinaryBucket = nonbinaryBucket;
  }
}
