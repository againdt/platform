package com.getinsured.hix.platform.gimonitor.service;

import java.util.Date;

import com.getinsured.hix.dto.exceptions.GIMonitorDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIMonitor;

public interface GIMonitorService {

	
	GIMonitor saveOrUpdateGIMonitor(GIMonitor giMonitor);

	GIMonitor saveOrUpdateErrorLog(String errorCode, Date eventTime,
			String exceptionClassName, String exceptionStackTrace,
			AccountUser user);

	GIMonitor saveOrUpdateErrorLog(String errorCode, Date eventTime, String exceptionClassName, String exceptionStackTrace,
			AccountUser user, String url, String component, Integer giMonitorId);
	
	GIMonitorDTO persistGIMonitor(GIMonitorDTO giMonitor); 
	
}
