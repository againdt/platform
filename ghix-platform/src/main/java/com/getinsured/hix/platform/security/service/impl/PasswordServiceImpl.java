/**
 * Declares the CRUD operations to manage User Profile
 * @author venkata_tadepalli
 * @since 11/30/2012
 */
package com.getinsured.hix.platform.security.service.impl;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.PasswordService;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("passwordService")
@Repository
@Transactional
public class PasswordServiceImpl implements PasswordService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PasswordServiceImpl.class);

	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private GhixEncryptorUtil ghixEncryptorUtil;

	/**
	 * HIX-32110 : Modify Change/Reset Password Process to accept salted password.
	 * 			   Updates ecrypted password for the given User
	 * 
	 * <p>
	 * 	Use the autoSave (true /false) to avoid multiple commits of the same user in same transaction
	 * </p>
	 * 
	 * @param user  Accoutn user object 
	 * @param newPassword
	 * @param autoSave 
	 * @return user  returns the give user object by updated the password policy data
	 * @throws GIException
	 */
	@Override
	@Transactional
	public AccountUser updatePassword(AccountUser accountUser, String newPassword,
			boolean autoSave) throws GIException {
		

		if (StringUtils.isEmpty(newPassword)) {
			String errMsg = "Change Password value should not null or empty";
			LOGGER.error(errMsg);
			throw new GIException(errMsg);
		}
		if (accountUser != null) {
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Calling change password for the user ::" + accountUser.getId());
			}
			String userUuid = accountUser.getUuid();
			newPassword = newPassword + userUuid;
			String encPwd = ghixEncryptorUtil.getEncryptedPassword(newPassword,
					userUuid);
			accountUser.setPassword(encPwd);
			accountUser.setPasswordLastUpdatedTimeStamp(new TSDate());
			accountUser.setRetryCount(0);
			accountUser.setSecQueRetryCount(0);
			accountUser.setConfirmed(1);
			accountUser.setStatus("Active");
			accountUser.setPasswordRecoveryToken(null);
			accountUser.setPasswordRecoveryTokenExpiration(null);
			accountUser.setLastUpdatedBy(accountUser.getId());
			if (autoSave) {
				userRepository.save(accountUser);
			}
		}
		return accountUser;
	}

	/**
	 * If there are multiple roles, lowest will be considered
	 * @param user
	 * @return
	 */
	@Override
	public int getPasswordExpiryPeriod(AccountUser user) {
		Set<UserRole> userRoles = user.getUserRole();
		int expiryDayCount = -1;
		UserRole tmp = null;
		Iterator<UserRole> cursor = userRoles.iterator();
		int days = -1;
		String roleEvaluated = null;
		Role tmpRole = null;
		while(cursor.hasNext()){
			tmp = cursor.next();
			if(!tmp.isActiveUserRole()){
				// Skip the role if it is not active
				continue;
			}
			tmpRole = tmp.getRole();
			days = tmpRole.getPasswordExpiryPeriod();
			
			if(expiryDayCount == -1){
				roleEvaluated = tmpRole.getName();
				expiryDayCount = days;
			}else if(expiryDayCount > days){
				roleEvaluated = tmpRole.getName();
				expiryDayCount = days;
			}
		}
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Evaluated role :"+roleEvaluated+" With expiration days:"+expiryDayCount);
		}
		return expiryDayCount;
	}
}
