package com.getinsured.hix.platform.content;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 *
 * Simple class to un-marshal json resource bundle file and form properties.
 *
 */
public final class JsonUtils {

	private static final String DOT = ".";
	private static final String EMPTY = "";
	private static final String DEFAULT_ENCODING = "UTF-8";

	private JsonUtils() {}

	public static Properties readJson(String resourceString) throws IOException {
		Properties props = new Properties();
		JsonParser jsonParser = new JsonParser();
		try {
			JsonElement json = jsonParser.parse(resourceString);
			readJson(json, props, EMPTY);
		} catch (JsonSyntaxException e) {
			throw e;
		}

		return props;

	}

	public static Properties readJson(Resource resource) throws IOException {
		Properties props = new Properties();
		InputStream is = resource.getInputStream();
		JsonParser jsonParser = new JsonParser();
		try {
			JsonElement json = jsonParser.parse(IOUtils.toString(is, DEFAULT_ENCODING));
			readJson(json, props, EMPTY);
		} catch (JsonSyntaxException | IOException e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(is);
		}

		return props;

	}

	private static void readJson(JsonElement json,Properties properties,String key) {
		//handle primitive
		if(json.isJsonPrimitive()){
			properties.put(key, json.getAsString());
			return;
		}
		Set<Entry<String, JsonElement>> set =  json.getAsJsonObject().entrySet();
		for(Entry<String, JsonElement> e : set){

			if(e.getValue().isJsonObject() || e.getValue().isJsonArray()){
				if(e.getValue().isJsonObject()){
					readJson(e.getValue().getAsJsonObject(), properties,
							key.equals(EMPTY) ? e.getKey() : (key+DOT+e.getKey()));
				}else if(e.getValue().isJsonArray()){
					int i=0;
					for(JsonElement jsonElement : e.getValue().getAsJsonArray()){
						readJson(jsonElement, properties,
								key.equals(EMPTY) ? e.getKey()+DOT+i : (key+DOT+e.getKey()+DOT+i));
						i++;
					}
				}
			}else if(e.getValue().isJsonPrimitive()){
				properties.put(key.equals(EMPTY) ? e.getKey() : (key+DOT+e.getKey()), e.getValue().getAsString());
			}
		}
	}

	public static void main(String[] args) throws IOException {

		String resourceString = new String(Files.readAllBytes(Paths.get(args[0])));
		System.out.println(resourceString);
		Properties props = readJson(resourceString);

		System.out.println(props);
	}
}
