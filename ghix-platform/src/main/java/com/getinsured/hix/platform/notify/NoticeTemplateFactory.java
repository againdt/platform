package com.getinsured.hix.platform.notify;

import java.io.IOException;
import java.io.Writer;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

@Component
public class NoticeTemplateFactory {
	@Autowired
	private NoticeTemplateLoader templateLoader;
	@Autowired ResourceTemplateLoader fileTemplateLoader;
	@Autowired GenericEcmTemplateLoader genericEcmResourceLoader;
	private Configuration config;
	private static Logger logger = LoggerFactory.getLogger(NoticeTemplateFactory.class);
	
	@PostConstruct
	public void init(){
		TemplateLoader[] templateLoaders = {templateLoader, fileTemplateLoader, genericEcmResourceLoader};
		MultiTemplateLoader mtl = new MultiTemplateLoader(templateLoaders);
		config = new Configuration();
		config.setTemplateLoader(mtl);
		config.setDefaultEncoding("UTF-8");
		config.setCacheStorage(new freemarker.cache.MruCacheStorage(40, 100));
		config.setTemplateExceptionHandler(new GiTemplateExceptionHandler());
		config.setLocalizedLookup(false);
	}
	
	public Template getTemplate(String name) throws TemplateNotFoundException{
		Template template = null;
		try {
			template = config.getTemplate(name);
		} catch (IOException e) {
			throw new TemplateNotFoundException("Faied to load template for:"+name, e);
		}
		return template;
	}
	
	public NoticeType getNoticeType(String name){
		return templateLoader.getNoticeType(name);
	}
	
	private static class GiTemplateExceptionHandler implements TemplateExceptionHandler{

		@Override
		public void handleTemplateException(TemplateException exp, Environment env, Writer out) throws TemplateException {
			logger.error("Template Error encountered", exp);
			throw new GIRuntimeException("Template Error encountered", exp);
		}
		
	}

}
