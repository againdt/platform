package com.getinsured.hix.platform.account.inboxnotification;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.model.AccountUser;

/**
 * Class to provide EMAIL_CLAZZ name and dynamic content within email body
 *
 */
public final class SecureInboxNotificationEmailType {

	private static final String EMPTY = "";
	private static final String EXCHANGEPHONE = "exchangephone";
	private static final String HIX = "/hix";
	private static final String INBOXURL = "inboxurl";
	private static final String EXCHANGEURL = "exchangeurl";
	private static final String USER_NAME = "UserName";
	private static final String EXCHANGENAME = "exchangename";
	private static final String HOST = "host";
	private static final String PRIVACY_STATEMENT = "privacy_statement";
	private static final String CONTACT_INFORMATION = "contact_information";
	private static final String FOOTER_YEAR = "footerYear";

	private SecureInboxNotificationEmailType() { }

	public static final String EMAIL_CLAZZ = "SecureInboxNotificationEmail";

	public static Map<String, String> setTokens(Map<String, String> emailData, AccountUser userObj) {

		Map<String,String> bean = new HashMap<String, String>();

		bean.put(HOST,emailData.get(HOST));
		bean.put(EXCHANGENAME,emailData.get(EXCHANGENAME));
		if(userObj != null && userObj.getFullName() != null){
			bean.put(USER_NAME,userObj.getFullName());
		}
		else{
			bean.put(USER_NAME,EMPTY);
		}
		bean.put(EXCHANGEURL,emailData.get(EXCHANGEURL));
		String inboxurl=emailData.get(INBOXURL);
		bean.put(INBOXURL,HIX+inboxurl);
		bean.put(EXCHANGEPHONE,emailData.get(EXCHANGEPHONE));
		bean.put(FOOTER_YEAR,Integer.toString(TSCalendar.getInstance().get(Calendar.YEAR) ));
		bean.put(PRIVACY_STATEMENT, emailData.get(PRIVACY_STATEMENT));
		bean.put(CONTACT_INFORMATION, emailData.get(CONTACT_INFORMATION));

		return bean;
	}

}
