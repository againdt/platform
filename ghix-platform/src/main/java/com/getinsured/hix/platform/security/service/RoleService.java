package com.getinsured.hix.platform.security.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Sort;

import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserRole;

public interface RoleService {

	String EMPLOYER_ROLE = "EMPLOYER";
	String EMPLOYEE_ROLE = "EMPLOYEE";
	String ISSUER_ROLE = "ISSUER";
	String BROKER_ROLE = "BROKER";
	String ISSUER_REP_ROLE = "ISSUER_REPRESENTATIVE";
	String ISSUER_ENROLLMENT_REP_ROLE = "ISSUER_ENROLLMENT_REPRESENTATIVE";
	String ADMIN_ROLE = "ADMIN";
	String CSR_ROLE = "CSR";
	String INDIVIDUAL_ROLE = "INDIVIDUAL";
	String ENTITY_ADMIN_ROLE = "ASSISTERENROLLMENTENTITYADMIN";
	String ASSISTER_ROLE = "ASSISTER";
	String ENROLLMENT_ENTITY = "assisterenrollmententity";
	String OPERATIONS_ROLE = "OPERATIONS";
	String ISSUER_ADMIN_ROLE = "ISSUER_ADMIN";
	String SALES_MANAGER = "SALES_MANAGER";
	String AFFILIATE_ADMIN = "AFFILIATE_ADMIN";
	String BROKER_ADMIN_ROLE = "BROKER_ADMIN";
	String AGENCY_MANAGER = "AGENCY_MANAGER";
	String APPROVED_ADMIN_STAFF_L1 = "APPROVEDADMINSTAFFL1";
	String APPROVED_ADMIN_STAFF_L2 = "APPROVEDADMINSTAFFL2";

	// Role Flags
	char ROLE_FLAG_DEFAULT = 'Y';
	char ROLE_FLAG_NOT_DEFAULT = 'N';
	
	//Added for HIX-27184 and HIX-27305
	char ROLE_IS_ACTIVE_DEFAULT = 'Y';
	char ROLE_IS_ACTIVE_NOT_DEFAULT = 'N';

	Role findRoleByName(String name);

	List<Role> findAll();
	
	List<Role> findAll(Sort sort);

	Role findById(int id);
	
	List<Role> findRolesByName(List<String> list);	
	
	List<Role> findNonAdminUsers();

	List<Role> findAdminRolesForUsers();
	
	List<Role> findPrivilegedRoles();
	
	List<String> findAllRoleName();
	
	Role findRoleByUserId(int id);

	List<Role> findAllRoleTypes(Sort sort);

	List<String> findAllRoleTypesByName(Sort sort);

	List<UserRole> findProvisioningOptionsForExistingRoles(
			Set<UserRole> existingUserRoles);
	List<Role> findAllUserCanAdd();

	List<Role> findAllUserCanUpdate();

	List<Role> findAllUserCanUpdate(Sort sort);

	List<Role> findAllUserCanAdd(Sort sort);

	List<String> findAllManagedRoleNames();
	
}