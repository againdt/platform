package com.getinsured.hix.platform.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link GhixEncryptorUtil} is a util class for generating UUID and encryption using jasypt.
 * 
 * Note: <b>Need to add properties values for Value annotation. For now use only 
 * the static methods of this class. Will update once we need encryption logic in our application.
 * Don't use parameterize constructor if properties and annotation are not set. </b> 
 * @since 1 March 2014
 * @author nayak_b
 *
 */
@Component
public class GhixEncryptorUtil {
	
	private int passwordStrength=10; //HIX-32109 : the value should be 4<=passwordStrength=>31
	
	// Copied from UUID implementation :)
    private static volatile SecureRandom numberGenerator = null;
    private static final long MSB = 0x8000000000000000L;
	private static final Logger LOGGER = LoggerFactory.getLogger(GhixEncryptorUtil.class);
    /**
     * 
	 * This method will return a random unique value of 32 char length.
	 * Copied from UUID implementation :)
	 * @author nayak_b
	 * @since 24-Feb-2014
     * @return 32 char string value
     */
    public static String generateRandom32Hex() {
        SecureRandom ng = numberGenerator;
        if (ng == null) {
        	ng = new SecureRandom();
            numberGenerator =  ng;
        }

        return Long.toHexString(MSB | ng.nextLong()) + Long.toHexString(MSB | ng.nextLong());
    }
    
    /**
     * 
	 * This method will return a random string of size(parameter as argument) char length. The string generated from {@link UUID}
	 * @author nayak_b
	 * @since 24-Feb-2014
	 * @param int size this mention the maximum char of the random string. size must be greater than 12 and less/equal 32 else result null.
     * @return string value of size as method argument.
     */
    public static String generateUUIDHexString(int size) {
    	String hexString = null;
    	final int ten = 10, thirtyTwo = 32;
    	if(size>ten && size<=thirtyTwo){
    		String uuid = UUID.randomUUID().toString();
    		uuid = uuid.replaceAll("[^0-9a-zA-Z]+", "");
    		hexString = uuid.substring(0, size);
    	}
        return hexString;
    }
    
	
	/**
	 * <p>
	 * Encrypts the data by salt and BCrypt. Uses the password strength and password salt (defined by GI app configuration).
	 * This will throw exception that thrown from BCrypt api
	 * 
	 * <p>
	 * JIRAs : HIX-32109
	 * @author Venkata Tadepalli
	 * @since 17-March-2014
	 * @throws Exception
	 * @param inputData to encrypt.
	 * @return string after encrypting the given value. 
	 */
	public String getBCryptedData(String inputData, String inputSalt){

		String hashedPassword = "";
		SecureRandom salt;
		try {
			salt = SecureRandom.getInstance("SHA1PRNG");
			salt.setSeed(inputSalt.getBytes());
			LOGGER.debug("getBCryptedData: salt: " + salt.getAlgorithm() + " for inputSalt : " + inputSalt + " inputData = " + inputData) ;
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(this.passwordStrength,salt);
		
			hashedPassword = passwordEncoder.encode(inputData);
			LOGGER.debug("hashedPassword = " + hashedPassword) ;
			
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("Error in creating the hashed password");
		}
		
		return hashedPassword;
	}

	/**
	 * <p>
	 * Encrypts the data by salt and BCrypt. Uses the password strength and password salt (defined by GI app configuration).
	 * This will throw exception that thrown from BCrypt api
	 * 
	 * <p>
	 * JIRAs : HIX-32109
	 * @author Venkata Tadepalli
	 * @since 17-March-2014
	 * @throws Exception
	 * @param inputData to encrypt.
	 * @return string after encrypting the given value. 
	 */
	public  String getEncryptedPassword(String inputPassword,String inputSalt){
		
		String strEncPassword=getBCryptedData(inputPassword,inputSalt);
		
		return strEncPassword;
	}
	

	
	
	/**
	 * <p>
	 * Returns the Password Strength
	 * <p>
	 * JIRAs : HIX-32109
	 * @author Venkata Tadepalli
	 * @return the defPasswordStrength
	 */
	public int getPasswordStrength() {
		return passwordStrength;
	}

	/**
	 * <p>
	 * Sets the Password Strength, if the input 'pwdStrength' value is not in between 4 and 31 then uses default 10.
	 * <p>
	 * JIRAs : HIX-32109
	 * @author Venkata Tadepalli
	 * @param defPasswordStrength the defPasswordStrength to set
	 */
	public void setPasswordStrength(int pwdStrength) {
		
		if (4 >= pwdStrength && pwdStrength <= 31){
			this.passwordStrength = pwdStrength;// if the input 'pwdStrength' value is not in between 4 and 31 then uses default 10;
		} 		
		
	}

	/**
	 * <p>
	 * If the input rawPassword matches with the input encPassword then returns 'true' else 'false'
	 * <p>
	 * JIRAs : HIX-32109
	 * @author Venkata Tadepalli
	 * @return the defPasswordStrength
	 */
	public boolean verifyEncryptedPassword(String rawPassWord,String encPassWord) {
		
		
		PasswordEncoder passwordEncoderEnc = new BCryptPasswordEncoder();
		return passwordEncoderEnc.matches(rawPassWord, encPassWord);
	}

	
	public static void main(String [] args) {
		//GhixEncryptorUtil ghixEncryptorUtil = new GhixEncryptorUtil();
		//String yourpassword="ghix123";
		//String uniquesalt=UUID.randomUUID()+"";
		//yourpassword=yourpassword+uniquesalt;
		//String encPwd = ghixEncryptorUtil.getEncryptedPassword(yourpassword, uniquesalt);
		
		//System.out.println("uniquesalt ::"+uniquesalt);
		//System.out.println("EncPassword ::"+encPwd);
		//GhixEncryptorUtil.userCase1();
		//GhixEncryptorUtil.userCase2();
		//String pwdV="Pass1234"+uniquesalt;
		//String encPwdV=encPwd;
		
		
		//System.out.println("Verify ::"+ghixEncryptorUtil.verifyEncryptedPassword(pwdV, encPwdV));

	}
	
	
	public static void userCase1() {
		/*GhixEncryptorUtil ghixEncryptorUtil = new GhixEncryptorUtil();
		int noOfRecs=2;
		String yourpassword="Pass1234";
		
		List<String> userSalts=new ArrayList<String>();
		List<String> userPwds=new ArrayList<String>();
		for (int i=0;i<noOfRecs;i++){
			String uniquesalt=UUID.randomUUID()+"";//"c5a666b-336d-4582-ba7c-c68a8d4724cf";//
			userSalts.add(uniquesalt);
		}
		*/
		//System.out.println("Generate Encrypt Pwds::...." );
		//System.out.println("UserId\tPassword\tSalt\t\t\t\t\t\tEncrypted PWD\t\t\t\t\t\tTime to Gen");
		//System.out.println("*******************************************************************************************************************************************");
		//for (int i=0;i<noOfRecs;i++){
			//Date start=new TSDate();
			//String userPwd=ghixEncryptorUtil.getEncryptedPassword(yourpassword,userSalts.get(i));
			//userPwds.add(i,userPwd);
			//Date end=new TSDate();
			//System.out.println("user"+i+"\t"+yourpassword+"\t"+userSalts.get(i)+"\t"+userPwds.get(i)+"\t"+(end.getTime()-start.getTime())+" ms");
		//}
		
		//System.out.println("\r\nUse Case 1:: Default BCrypt PWD verification ::" );
		//System.out.println("***************************************************" );
		//int userIdx=0;
		//System.out.println("UserId  		\t::user"+userIdx);
		//System.out.println("Password		\t::"+yourpassword);
		//System.out.println("User EncPassword	 \t::"+userPwds.get(userIdx));
		//System.out.println("Input Password  	 \t::"+userPwds.get(userIdx));
		//Date start=new TSDate();
		//System.out.println("VerifyPassword Result	\t::"+ghixEncryptorUtil.verifyEncryptedPassword(yourpassword,userPwds.get(userIdx)));
		//Date end=new TSDate();
		//System.out.println("Time to Verify		 \t::"+(end.getTime()-start.getTime())+" ms");
		
		//System.out.println("\r\nUse Case 2:: Default BCrypt PWD verification (swap user pwds)::" );
		//System.out.println("*******************************************************************" );
		//userIdx=0;
		//System.out.println("UserId  		\t::user"+userIdx);
		//System.out.println("Password		\t::"+yourpassword);
		//System.out.println("User EncPassword	 \t::"+userPwds.get(userIdx));
		//System.out.println("Input Password  	 \t::"+userPwds.get(userIdx+1)+"   <-- this is enc pwd of user"+userIdx+1);
		//start=new TSDate();
		//System.out.println("VerifyPassword Result	\t::"+ghixEncryptorUtil.verifyEncryptedPassword(yourpassword,userPwds.get(userIdx+1)));
		//end=new TSDate();
		//System.out.println("Time to Verify		 \t::"+(end.getTime()-start.getTime())+" ms");
		
		

	}	
	
	public static void userCase2() {
		/*GhixEncryptorUtil ghixEncryptorUtil = new GhixEncryptorUtil();
		int noOfRecs=2;
		String yourpassword="Pass1234";
		
		List<String> userSalts=new ArrayList<String>();
		List<String> userPwds=new ArrayList<String>();
		for (int i=0;i<noOfRecs;i++){
			String uniquesalt=UUID.randomUUID()+"";//"c5a666b-336d-4582-ba7c-c68a8d4724cf";//
			userSalts.add(uniquesalt);
		}*/
		
		//System.out.println("Generate Encrypt Pwds (user password+salt)::...." );
		//System.out.println("UserId\tPassword\tSalt\t\t\t\t\t\tEncrypted PWD\t\t\t\t\t\tTime to Gen");
		//System.out.println("*******************************************************************************************************************************************");
		//for (int i=0;i<noOfRecs;i++){
			//Date start=new TSDate();
			// userPwd=ghixEncryptorUtil.getEncryptedPassword(yourpassword+userSalts.get(i),userSalts.get(i));
			//userPwds.add(i,userPwd);
			//Date end=new TSDate();
			//System.out.println("user"+i+"\t"+yourpassword+"\t"+userSalts.get(i)+"\t"+userPwds.get(i)+"\t"+(end.getTime()-start.getTime())+" ms");
		//}
		
		/*System.out.println("\r\nUse Case 1:: Default BCrypt PWD verification ::" );
		System.out.println("***************************************************" );*/
		//int userIdx=0;
		/*System.out.println("UserId  		\t::user"+userIdx);
		System.out.println("Password		\t::"+yourpassword);
		System.out.println("User EncPassword	 \t::"+userPwds.get(userIdx));
		System.out.println("Input Password  	 \t::"+userPwds.get(userIdx));*/
		//Date start=new TSDate();
		//System.out.println("VerifyPassword Result	\t::"+ghixEncryptorUtil.verifyEncryptedPassword(yourpassword+userSalts.get(userIdx),userPwds.get(userIdx)));
		//Date end=new TSDate();
		/*System.out.println("Time to Verify		 \t::"+(end.getTime()-start.getTime())+" ms");
		
		System.out.println("\r\nUse Case 2:: Default BCrypt PWD verification (swap user pwds)::" );
		System.out.println("*******************************************************************" );*/
		//userIdx=0;
		/*System.out.println("UserId  		\t::user"+userIdx);
		System.out.println("Password		\t::"+yourpassword);
		System.out.println("User EncPassword	 \t::"+userPwds.get(userIdx));
		System.out.println("Input Password  	 \t::"+userPwds.get(userIdx+1)+"   <-- this is enc pwd of user"+userIdx+1);*/
		//start=new TSDate();
		//System.out.println("VerifyPassword Result	\t::"+ghixEncryptorUtil.verifyEncryptedPassword(yourpassword+userSalts.get(userIdx),userPwds.get(userIdx+1)));
		//end=new TSDate();
		//System.out.println("Time to Verify		 \t::"+(end.getTime()-start.getTime())+" ms");
		
		

	}		

}
