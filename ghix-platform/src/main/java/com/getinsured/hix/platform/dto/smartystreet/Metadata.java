package com.getinsured.hix.platform.dto.smartystreet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Sunil Desu
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata {

	private String record_type;
	private String zip_type;
	private String county_fips;
	private String county_name;
	private String carrier_route;
	private String rdi;
	private String latitude;
	private String longitude;

	public String getRecord_type() {
		return record_type;
	}

	public void setRecord_type(String record_type) {
		this.record_type = record_type;
	}

	public String getZip_type() {
		return zip_type;
	}

	public void setZip_type(String zip_type) {
		this.zip_type = zip_type;
	}

	public String getCounty_fips() {
		return county_fips;
	}

	public void setCounty_fips(String county_fips) {
		this.county_fips = county_fips;
	}

	public String getCounty_name() {
		return county_name;
	}

	public void setCounty_name(String county_name) {
		this.county_name = county_name;
	}

	public String getCarrier_route() {
		return carrier_route;
	}

	public void setCarrier_route(String carrier_route) {
		this.carrier_route = carrier_route;
	}

	public String getRdi() {
		return rdi;
	}

	public void setRdi(String rdi) {
		this.rdi = rdi;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Metadata [record_type=");
		builder.append(record_type);
		builder.append(", zip_type=");
		builder.append(zip_type);
		builder.append(", county_fips=");
		builder.append(county_fips);
		builder.append(", county_name=");
		builder.append(county_name);
		builder.append(", carrier_route=");
		builder.append(carrier_route);
		builder.append(", rdi=");
		builder.append(rdi);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append("]");
		return builder.toString();
	}

}
