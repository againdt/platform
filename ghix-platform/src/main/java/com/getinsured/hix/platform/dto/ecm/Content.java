package com.getinsured.hix.platform.dto.ecm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;



public class Content {

	// instance variables

	private String contentId = null;

	private Date creationDate = null;

	private Date modifiedDate = null;

	private String url = null;

	private String description = null;

	private String title = null;

	private String mimeType = null;

	private String originalFileName = null;

	private String docType = null;

	private String securityGroup;

	private Datasource dataSource = null;

	private String folderId;

	private String checkInComments;

	private Map<String,String>  customAttributeList = new HashMap<String,String>();

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getSecurityGroup() {
		return securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	public Datasource getDataSource() {
		return dataSource;
	}

	public void setDataSource(Datasource dataSource) {
		this.dataSource = dataSource;
	}

	public Map<String,String> getCustomAttributeList() {
		return customAttributeList;
	}

	public void setCustomAttributeList(Map<String,String> customAttributeList) {
		this.customAttributeList = customAttributeList;
	}

	public boolean isEmpty(){
		return contentId != null ? false : true;
	}

/*	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	public String getFolderId() {
		return folderId;
	}

	@Override
	public String toString() {
		return "Content [contentId=" + contentId + ", creationDate="
				+ creationDate + ", modifiedDate=" + modifiedDate + ", url="
				+ url + ", description=" + description + ", title=" + title
				+ ", mimeType=" + mimeType + ", originalFileName="
				+ originalFileName + ", docType=" + docType
				+ ", securityGroup=" + securityGroup + ", dataSource="
				+ dataSource + ", folderId=" + folderId + ", checkInComments="
				+ checkInComments + ", customAttributeList="
				+ customAttributeList + "]";
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	public String getCheckInComments() {
		return checkInComments;
	}

	public void setCheckInComments(String checkInComments) {
		this.checkInComments = checkInComments;
	}



}
