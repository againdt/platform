package com.getinsured.hix.platform.logging.filter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.platform.couchbase.dto.CouchDocument;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author Sunil Desu
 *
 */
public class CouchbaseLog extends CouchDocument {

	@Expose
	private String logLevel;
	@Expose
	private String loggerName;
	@Expose
	private String threadName;
	@Expose
	private Date logTimestamp;
	@Expose
	private String hostName;
	@Expose
	private String hostAddress;

	@Expose
	private String message;
	@Expose
	private HashMap<String, Object> locationInfo;
	@Expose
	private HashMap<String, Object> exceptionInfo;
	@Expose
	private HashMap<String, Object> customData;

	@Expose
	private Map<Object, Object> mdc;
	@Expose
	private String ndc;

	public String getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	public String getLoggerName() {
		return loggerName;
	}

	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	public Date getLogTimestamp() {
		return logTimestamp;
	}

	public void setLogTimestamp(Date logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HashMap<String, Object> getLocationInfo() {
		if (locationInfo == null) {
			locationInfo = new HashMap<String, Object>();
		}
		return locationInfo;
	}

	public void setLocationInfo(HashMap<String, Object> locationInfo) {
		this.locationInfo = locationInfo;
	}

	public HashMap<String, Object> getExceptionInfo() {
		if (exceptionInfo == null) {
			exceptionInfo = new HashMap<String, Object>();
		}
		return exceptionInfo;
	}

	public void setExceptionInfo(HashMap<String, Object> exceptionInfo) {
		this.exceptionInfo = exceptionInfo;
	}

	public Map<String, Object> getCustomData() {
		if (customData == null) {
			customData = new HashMap<String, Object>();
		}
		return customData;
	}

	public void setCustomData(HashMap<String, Object> customData) {
		this.customData = customData;
	}

	public Map<Object, Object> getMdc() {
		return mdc;
	}

	public void setMdc(Map<Object, Object> mdc) {
		this.mdc = mdc;
	}

	public String getNdc() {
		return ndc;
	}

	public void setNdc(String ndc) {
		this.ndc = ndc;
	}

}