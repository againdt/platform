package com.getinsured.hix.platform.security;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RolePermission;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantPermission;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.repository.IRolePermissionRepository;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.SecurityUtils;
import com.getinsured.hix.platform.security.service.TenantPermissionService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.IPRestrictor;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * A custom PermissionEvaluator implementation that checks whether a given
 * permission exists for a particular user.
 * 
 * @author venkata_tadepalli
 * @since 11/20/2012
 */
@Component("customPermissionEvaluator")
public class CustomPermissionEvaluator implements PermissionEvaluator {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomPermissionEvaluator.class);

	@Autowired
	private AuditAuthorizationDetails auditAuthorizationDetails;
	@Autowired
	private TenantPermissionService tenantPermissionService;
	@Autowired
	private IPRestrictor ipRestrictor;
	@Autowired
	private RoleService roleService;
	@Autowired
	private IRolePermissionRepository rolePermissionsRepo;
	
	private static final String USER_ACTIVE_ROLE_NAME = "userActiveRoleName".intern();

	private Map<String, Set<String>> rolePermissionCache = Collections.synchronizedMap(new HashMap<String, Set<String>>());
	
	public Set<String> getPermissionsForRole(String roleName){
		Set<String> rolePerms = this.rolePermissionCache.get(roleName);
		if(rolePerms == null) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Permissions not cached for role {}, fetching from the DB", roleName);
			}
			List<RolePermission> perms = this.rolePermissionsRepo.getPermissionsByRole(roleName);
			rolePerms = new HashSet<String>();
			for(RolePermission permissions: perms) {
				rolePerms.add(permissions.getPermission().getName());
			}
			this.rolePermissionCache.put(roleName, rolePerms);
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Permissions available for role {} are {}",roleName,rolePerms);
		}
		return rolePerms;
	}
	
	/**
	 * Convenience method to chek the permissions of interest in one call
	 * First uses the current role i.e. one the roles user has that is curently in use
	 * in a switch role functionality.
	 * 
	 * if none, uses the default role
	 * 
	 * @param permissions set of interest
	 * @return a map of permissions and result
	 */
	public Map<String , Boolean> getUserPermissions(Set<String> permissions){
		AccountUser user = SecurityUtils.getLoggedInUser();
		HashMap<String, Boolean> allowedPerms = new HashMap<>(permissions.size());
		Iterator<String> permCursor = null;
		if(user == null || user.getUserName().equalsIgnoreCase("anonymous")) {
			LOGGER.warn("Anonymous access, denying all permissions");
			return Collections.unmodifiableMap(allowedPerms);
		}
		String role = user.getCurrentUserRole();
		if(role == null) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("No current role set for the user, using default");
			}
			Role tmprole = this.getUserDefaultRole(user);
			if(tmprole != null) {
				role = tmprole.getName();
			}
		}else {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Using current role {} ",role);
			}
		}
		if(role == null) {
			LOGGER.warn("Failed to determine the user role denying all permissions");
			return Collections.unmodifiableMap(allowedPerms);
		}
		permCursor = permissions.iterator();
		while(permCursor.hasNext()) {
			String perm = permCursor.next();
			allowedPerms.put(permCursor.next(), this.getPermissionsForRole(role).contains(perm));
		}
		return Collections.unmodifiableMap(allowedPerms);
	}
	
	/**
	 * Checks if a logged in user has a perticular permission
	 * @param permission
	 * @return
	 */
	public boolean hasPermission(String permission) {
		AccountUser user = SecurityUtils.getLoggedInUser();
		if(user == null || user.getUserName().equalsIgnoreCase("anonymous")) {
			LOGGER.warn("Anonymous access, denying permission {}",permission);
			return false;
		}
		String role = user.getCurrentUserRole();
		if(role == null) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("No current role set for the user, using default");
			}
			Role tmprole = this.getUserDefaultRole(user);
			if(tmprole != null) {
				role = tmprole.getName();
			}
		}else {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Using current role {} to check {} permission",role, permission);
			}
		}
		if(role == null) {
			LOGGER.warn("Failed to determine the user role denying permission {}",permission);
			return false;
		}
		if(this.getPermissionsForRole(role).contains(permission)) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Permission {} available for role {}", permission, role);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
		boolean accessAllowed = true;
		Role currentRole = null;
		Role defaultRole = null;
		HttpSession session = null;
		boolean userSwitchedRole = false;
		AccountUser user = null;
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Check the user :  " + authentication.getName() + " , hasPermission Permission : "
						+ permission);
			}
			Object principal = authentication.getPrincipal();
			if (principal != null && principal instanceof AccountUser) {
				user = (AccountUser) principal;
				defaultRole = this.getUserDefaultRole(user);
				session = this.getSession();
				currentRole = this.getLoggedInUserrRole(user, defaultRole, session);
				
				if (currentRole != null) {
					if (LOGGER.isTraceEnabled()) {
						LOGGER.trace(
								"Retrieved user role " + currentRole.getLabel() + " Checking if login is restricted");
					}
					//Check if the login is restriced
					if (currentRole.getLoginRestricted() == 1) {
						if (LOGGER.isTraceEnabled()) {
							LOGGER.trace("login for role [" + currentRole.getLabel() + "] is restricted, checking");
						}
						if (!this.ipRestrictor.checkIpAccess()) {
							logAccessDenied(permission, currentRole.getName());
							accessAllowed = false;
						}
					}
					if (accessAllowed) {
						// Current role is the effective role, may not be the actual user role
						Set<String> perms = this.getPermissionsForRole(currentRole.getName());
						accessAllowed = perms.contains(permission);
					}
					if (accessAllowed) {
						// TODO change this logic once filter is enabled
						if (LOGGER.isTraceEnabled()) {
							LOGGER.trace("Permissions cleared, checking the tenant context");
						}
						TenantDTO tenant = TenantContextHolder.getTenant();
						if (tenant != null && tenant.getId() != null) {
							List<TenantPermission> tenantPermissions = tenantPermissionService
									.findByTenantIdAndPermissionName(tenant.getId(), String.valueOf(permission));
							if (tenantPermissions == null || tenantPermissions.isEmpty()) {
								LOGGER.warn("Tenant [" + tenant.getId() + "] does not have [" + permission
										+ "] permission, denying access");
								accessAllowed = false;
							}
						}
					}
					auditAuthorizationDetails(permission, accessAllowed, currentRole.getName());
				} else {
					accessAllowed = false;
					LOGGER.error("Permission check invoked for user without a role?, should never see this message");
				}
				if( user.getSwitchedRoleName() != null && user.getSwitchedRoleName().compareToIgnoreCase(defaultRole.getName()) != 0){
					userSwitchedRole = true;
				}
			} else {
				LOGGER.error("Permission check invoked for request URI: {},without any principal in the context",
						this.getSourceUrl());
				accessAllowed = false;
			}
		} catch (Exception e) {
			LOGGER.error(" Error while evaluating permissions, denying access ", e);
			accessAllowed = false;
		}
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Check the user :  " + authentication.getName() + " , hasPermission Permission : " + permission
					+ " result :: " + accessAllowed + " for URI " + this.getSourceUrl());
		}
		
		if(!accessAllowed) {
			String activeRoleName = null;
			if(userSwitchedRole) {
				activeRoleName = user.getSwitchedRoleName();
			}
			else if(session != null) {
				activeRoleName = (String)session.getAttribute(USER_ACTIVE_ROLE_NAME);
			}else {
				activeRoleName = defaultRole.getName();
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("session is null, active role name set to: {}", activeRoleName);
				}
			}
			accessAllowed = this.processOverrides(currentRole.getName(), defaultRole.getName(), accessAllowed, (String)permission, activeRoleName, userSwitchedRole);
		}
		auditAuthorizationDetails(permission, accessAllowed);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Check the user :  " + authentication.getName() + " , hasPermission Permission : " + permission
					+ " result :: " + accessAllowed);
		}
		return accessAllowed;
	}
	
	private HttpSession getSession() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		
		if (sra == null) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("No servlet request context available for thread [" + Thread.currentThread().getName()
						+ "] Node profile " + GhixPlatformConstants.PLATFORM_NODE_PROFILE + " With IP:"
						+ GhixPlatformConstants.LOCAL_NODE_IP
						+ " Can not use the session to check for the current role external redirect");
			}
			return null;
		}
		// We have the request context available, this means we have the request and the
		// session
		HttpServletRequest req = sra.getRequest();
		return req.getSession(false); 
	}
	
	public Role getLoggedInUserrRole(AccountUser user) {
		// We are interested in existing sessions only
		return this.getLoggedInUserrRole(user, null, null);
	}

	/**
	 * This method will try to return a role currently associated with a logged in
	 * user For users with ultiple roles, it wil try to find the role user is
	 * curently using by the way of session parameter set at the time of role switch
	 * or the login if the session is not accessible, it will return the user's
	 * default role
	 * 
	 * if a session available but current role is not set, it will return the user's
	 * default role
	 * 
	 * if current role is set nut user does not have that role, it will assume its a
	 * switch role feature and will return the switched role
	 * 
	 * If a switched role is not a valid role in GI system, will throw an exception
	 * 
	 * @param user
	 * @return
	 */
	public Role getLoggedInUserrRole(AccountUser user, Role defaultRole, HttpSession session) {
		// We are interested in existing sessions only
		if(defaultRole == null) {
			defaultRole = this.getUserDefaultRole(user);
		}
		if (session == null) {
			session = this.getSession();
		}
		if(session  == null) {
			if (LOGGER.isInfoEnabled()) {
			LOGGER.info(
					"Attempt to find the session for the logged in user found a null session, possibly a the session has been terminated or user closed the browser while request was still in process");
			}
			return defaultRole;
		}
		String currentRoleName = (String) session.getAttribute(USER_ACTIVE_ROLE_NAME);
		if (currentRoleName == null) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No current role set for the user:" + user.getUserName()
					+ " Expected session parameter \"userActiveRoleName\" will use user's default role");
			}
			return defaultRole;
		}else {
			boolean externalRedirect = SecurityUtils.isExternalRedirect();
			if(externalRedirect) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("External redirect for {} will use user's default role",user.getUserName());
				}
				session.setAttribute(USER_ACTIVE_ROLE_NAME, defaultRole.getName());
				return defaultRole;
			}
		}
		// Current role name is set in the session
		Role currentRole = null;
		Boolean switchedRoleAction = (Boolean) session.getAttribute("switchedRoleAction".intern());
		Role switchedFromRole = (Role) session.getAttribute("switchedFromRole".intern());
		String switchedFromRoleName = "Not available".intern();
		if (switchedFromRole != null) {
			switchedFromRoleName = switchedFromRole.getName();
		}
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("1: Switched Role Action {}, switchedFromRole {}, currentRoleName {}", switchedRoleAction,
					switchedFromRoleName, currentRoleName);
		}
		if (null != switchedRoleAction && null != switchedFromRole) {
			currentRoleName = switchedFromRole.getName();
		}
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("2:Switched Role Action {}, switchedFromRole {}, currentRoleName {}", switchedRoleAction,
					switchedFromRoleName, currentRoleName);
		}
		for (UserRole tmp : user.getUserRole()) {
			if (!tmp.isActiveUserRole()) {
				continue;
			}
			if (tmp.getRole().getName().equalsIgnoreCase(currentRoleName)) {
				return tmp.getRole();
			}
		}
		LOGGER.info(
				"No role with name {} available for this user, this could be an assumed role (switch role), from {}",
				currentRoleName, switchedFromRole);
		currentRole = roleService.findRoleByName(currentRoleName);
		// Check if we still do not have a role
		if (currentRole == null) {
			throw new GIRuntimeException("Failed to find the current logged in for the current user :"
					+ user.getUserName() + " No role available with name:" + currentRoleName
					+ " Should not have seen this, check if tenant has this role enabled, if this is a multitenant System");
		}
		return currentRole;
	}

	private Role getUserDefaultRole(AccountUser user) {
		Set<UserRole> userRoles = user.getUserRole();
		for (UserRole urole : userRoles) {
			if (!urole.isActiveUserRole()) {
				continue;
			}
			if (urole.isDefaultRole()) {
				return urole.getRole();
			}
		}
		return null;
	}
	
	
	private boolean processOverrides(String evaluatedRole, String defaultRole, boolean previousResult, String permission, String userActiveRoleFromSession, boolean userSwitchedRole) {
		Set<String> permissions=null;
		if(userSwitchedRole) {
			permissions = this.getPermissionsForRole(userActiveRoleFromSession);
		}else {
			permissions = this.getPermissionsForRole(defaultRole);
		}
		
		if(permissions.contains("INDIVIDUAL_IMPERSONATION")) {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			String consumerRoleName = stateCode.equalsIgnoreCase("PHIX")?"CONSUMER":"INDIVIDUAL";
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Logged in default role {} has impersonation privileges, checking if target permission {} belongs to {} The role evaluated {}", defaultRole, permission, consumerRoleName, evaluatedRole);
			}
			HttpSession session = this.getSession();
			Set<String> consumerPermissions = this.getPermissionsForRole(consumerRoleName);
			if(consumerPermissions.contains(permission)) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Logged in default role {} allowed target permission {} via override", defaultRole, permission);
				}
				
				if(session != null) {
					
					session.setAttribute(USER_ACTIVE_ROLE_NAME, consumerRoleName);
				}
				return true;
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Checking {} for {}", defaultRole, permission);
			}
			Set<String> defaultRolePermissions = null;
			if(userSwitchedRole) {
				defaultRolePermissions = this.getPermissionsForRole(userActiveRoleFromSession);
			}
			else{
				defaultRolePermissions = this.getPermissionsForRole(defaultRole);
			}
			if(defaultRolePermissions.contains(permission)) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Role {} allowed target permission {} via override", defaultRole, permission);
				}
				if(session != null) {
					session.setAttribute(USER_ACTIVE_ROLE_NAME, consumerRoleName);
				}
				return true;
			}
		}
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Logged in default role {} denied target permission {} ", defaultRole, permission);
		}
		return false;
	}

	private String getSourceUrl() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if(sra != null) {
			HttpServletRequest req = sra.getRequest();
			if(req != null) {
				return req.getRequestURI();
			}
		}
		return null;
	}

	private void logAccessDenied(Object permission, String loggedInAs) {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest req = sra.getRequest();
		HttpSession session = req.getSession(false);
		if (session == null) {
			LOGGER.info(
					"Attempt to retrieve the session failed to find the session while logging access denied log permission, possibly user closed the browser before request could finish processing");
		} else {
			session.invalidate();
		}
		Map<String, String> params = new HashMap<String, String>();
		params.put("Reason", "IPRestrictions applied");
		params.put("Logged In As", loggedInAs);
		auditAuthorizationDetails.logUnSuccessfulAuth(permission, params);
	}

	private void auditAuthorizationDetails(Object permission, boolean result, String loggedInAs) {
		HashMap<String, String> customParam = new HashMap<String, String>();
		if (loggedInAs != null) {
			customParam.put("Logged In role", loggedInAs);
		}
		if (!result) {
			auditAuthorizationDetails.logUnSuccessfulAuth(permission, customParam);
		} else {
			auditAuthorizationDetails.logSuccessfulAuth(customParam);
		}
	}

	private void auditAuthorizationDetails(Object permission, boolean result) {
		if (!result) {
			auditAuthorizationDetails.logUnSuccessfulAuth(permission);
		} else {
			auditAuthorizationDetails.logSuccessfulAuth();
		}
	}

	/**
	 * Another hasPermission signature. We may implement this.
	 *
	 */
	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType,
			Object permission) {
		boolean result = false;
		Role defaultRole = null;
		Role curentRole = null;
		HttpSession session = null;
		AccountUser user = null;
		boolean userSwitchedRole = false;
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Check the user :  ".intern() + authentication.getName() + " , hasPermission Permission : ".intern()
						+ permission + " and has the correct module set: ".intern() + targetType);
			}
			if (authentication.getPrincipal() instanceof AccountUser) {
				user = (AccountUser) authentication.getPrincipal();
				defaultRole = this.getUserDefaultRole(user);
				session = this.getSession();
				curentRole = this.getLoggedInUserrRole(user, defaultRole, session);
				if (curentRole == null) {
					LOGGER.error("No current role found for user:" + user.getUsername());
					return false;
				}
				if (curentRole.getLoginRestricted() == 1) {
					if (!this.ipRestrictor.checkIpAccess()) {
						LOGGER.info("Denying permission as role has IP restrictions".intern());
						logAccessDenied(permission, targetType);
						return false;
					}
				}
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("Logged in user role {}", curentRole.getName());
				}
				String roleName = curentRole.getName();
				Set<String> rolePerms = this.getPermissionsForRole(roleName);
				result= roleName.equalsIgnoreCase(targetType) && rolePerms.contains(permission);
				
				if( user.getSwitchedRoleName() != null && user.getSwitchedRoleName().compareToIgnoreCase(defaultRole.getName()) != 0){
					userSwitchedRole = true;
				}
			}
		} catch (Exception e) {
			LOGGER.error(" has Permission :; " + e.getMessage());
		}

		if(!result) {
			String activeRoleName = null;
			if(userSwitchedRole) {
				activeRoleName = user.getSwitchedRoleName();
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("User switched role name {}",activeRoleName);
				}
			}
			else if(session != null) {
				activeRoleName = (String)session.getAttribute(USER_ACTIVE_ROLE_NAME);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("User switched role name from the session {}",activeRoleName);
				}
			}else {
				activeRoleName = defaultRole.getName();
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("session is null, active role name set to: {}", activeRoleName);
				}
			}
			
			result = this.processOverrides(curentRole.getName(), defaultRole.getName(), result, (String)permission,activeRoleName, userSwitchedRole);
		}
		auditAuthorizationDetails(permission, result);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Check the user :  " + authentication.getName() + " , hasPermission Permission : " + permission
					+ " result :: " + result);
		}
		return result;
	}
}