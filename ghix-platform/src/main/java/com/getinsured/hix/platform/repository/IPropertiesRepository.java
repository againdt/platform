package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.GIAppProperties;

/**
 * Repository interface for working with {@link GIAppProperties} (backed by <code>gi_app_config</code> table).
 * <p>
 *   Consider using methods that take <code>tenantId</code> as parameter, even thought this class is
 *   implementing {@link TenantAwareRepository}, because when methods with <code>tenantId</code> are used
 *   we are caching results. Otherwise caching cannot be done, because <code>tenantId</code> is set with
 *   {@link org.hibernate.annotations.Filter} and not visible to the interceptor that is invoked on
 *   methods with {@link org.springframework.cache.annotation.Cacheable} annotation.
 * </p>
 */
public interface IPropertiesRepository extends TenantAwareRepository<GIAppProperties, Integer> {
	GIAppProperties findBypropertyKey(String propertyKey);

	@Query("SELECT g FROM GIAppProperties g where g.propertyKey LIKE :propertyKey || '%' order by g.propertyKey")
	List<GIAppProperties> getPropertyKeyList(@Param("propertyKey") String  propertyKey );

	@Query("SELECT g FROM GIAppProperties g where g.tenantId = :tenantId AND LOWER(g.propertyKey) LIKE LOWER(:propertyKey || '%') order by g.propertyKey")
	List<GIAppProperties> getPropertyKeyList(@Param("propertyKey") String propertyKey, @Param("tenantId") Long tenantId);

	@Query("SELECT g FROM GIAppProperties g where g.tenantId = :tenantId AND LOWER(g.propertyKey) LIKE LOWER(:section || '.%' || :propertyKey || '%') order by g.propertyKey")
	List<GIAppProperties> searchByPropertyKey(@Param("section") String section, @Param("propertyKey") String propertyKey, @Param("tenantId") Long tenantId);

	@Query("SELECT g FROM GIAppProperties g where LOWER(g.propertyKey) LIKE LOWER(:section || '.%' || :propertyKey || '%') order by g.propertyKey")
	List<GIAppProperties> searchByPropertyKey(@Param("section") String section, @Param("propertyKey") String propertyKey);

	@Query("SELECT g FROM GIAppProperties g where g.propertyKey LIKE :propertyKey || '%' order by g.propertyKey")
	Page<GIAppProperties> getPropertyKeyList(@Param("propertyKey") String propertyKey, Pageable pageable);

	@Query("SELECT g FROM GIAppProperties g where g.tenantId = :tenantId AND g.propertyKey LIKE :propertyKey || '%' order by g.propertyKey")
	Page<GIAppProperties> getPropertyKeyList(@Param("propertyKey") String propertyKey, Pageable pageable, @Param("tenantId") Long tenantId);

	GIAppProperties findById(Integer id);
}