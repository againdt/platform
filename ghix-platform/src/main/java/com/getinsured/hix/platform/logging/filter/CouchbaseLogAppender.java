package com.getinsured.hix.platform.logging.filter;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.couchbase.service.CouchBucketService;
import com.google.gson.Gson;

/**
 * 
 * @author Sunil Desu
 *
 */
@Component
@DependsOn(value = "couchBucketService")
public class CouchbaseLogAppender extends AppenderSkeleton {

	@Autowired
	private CouchBucketService couchBucketService;

	@Override
	public void close() {
	}

	@Override
	public boolean requiresLayout() {
		return true;
	}

	@Override
	protected void append(LoggingEvent event) {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		try {
				Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
				CouchbaseLog couchbaseLog = gson.fromJson(this.layout.format(event), CouchbaseLog.class);
				//String id = couchBucketService.createDocument(couchbaseLog);
				couchBucketService.createDocument(couchbaseLog);
		} catch (Exception ex) {
			//FIXME Discuss
			//Do Nothing
			ex.printStackTrace();
		}
	}

}
