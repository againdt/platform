package com.getinsured.hix.platform.dto.ecm;

import java.util.Arrays;




public class Image extends Content {

	// instance variables

	private int width = 0;

	private int height = 0;

	private int resolution = 0;

	private int bitDepth = 0;

	private String mimeType = null;

	private byte[] imageStream = null;

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getResolution() {
		return resolution;
	}

	public void setResolution(int resolution) {
		this.resolution = resolution;
	}

	public int getBitDepth() {
		return bitDepth;
	}

	public void setBitDepth(int bitDepth) {
		this.bitDepth = bitDepth;
	}

	@Override
	public String getMimeType() {
		return mimeType;
	}

	@Override
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public byte[] getImageStream() {
		return imageStream.clone();
	}

	public void setImageStream(byte[] imageStream) {
		this.imageStream = imageStream.clone();
	}

	@Override
	public String toString() {
		return "Image [width=" + width + ", height=" + height + ", resolution="
				+ resolution + ", bitDepth=" + bitDepth + ", mimeType="
				+ mimeType + ", imageStream=" + Arrays.toString(imageStream)
				+ "]";
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
	

}
