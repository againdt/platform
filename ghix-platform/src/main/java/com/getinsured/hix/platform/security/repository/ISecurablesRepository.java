package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.SecurableTarget;
import com.getinsured.hix.model.Securables;
import com.getinsured.hix.platform.dto.batch.UserDTO;

@Repository("iSecurablesRepository")
public interface ISecurablesRepository extends JpaRepository<AccountUser, Integer>, RevisionRepository<Securables, Integer, Integer> {
	
	@Query("SELECT new AccountUser(au.id, au.firstName, au.lastName, au.lastLogin) from AccountUser as au "+
			" WHERE au.status!='Inactive-dormant' Or au.status is null ")
			List<AccountUser> getUserListToInactivate();
	
	
	@Query("SELECT new com.getinsured.hix.platform.dto.batch.UserDTO(u.id,u.userName,s.userName,u.userNPN,s.password,s.applicationData,st.targetId) "
			+ " FROM Securables s"
			+ " ,SecurableTarget st"
			+ " ,AccountUser u "			
			+ " WHERE u.id=st.targetId AND st.id=s.securableTarget.id AND s.applicationData='Yes' and st.targetName='USER'")
	List<UserDTO> findEnabledFfmUsers();
	
	
//	Use to find agents/users qualified for FFM proxy
//	TODO change the name of the query once environment becomes stable
	@Query("SELECT new com.getinsured.hix.platform.dto.batch.UserDTO(u.id,u.userName,s.userName,u.userNPN,s.password,s.applicationData,st.targetId) "
			+ " FROM Securables s"
			+ " ,SecurableTarget st"
			+ " ,AccountUser u "			
			+ " WHERE u.id=st.targetId AND st.id=s.securableTarget.id AND s.applicationData='Yes' and st.targetName='USER' and u.id= :userId")
	UserDTO findFfmProxyEnabledUserByUserId(@Param("userId") Integer userId);
	
//	Use to find agents qualified for agent redirect to FFM
	@Query("SELECT new com.getinsured.hix.platform.dto.batch.UserDTO(u.id,u.userName,s.userName,u.userNPN,s.password,s.applicationData,st.targetId) "
			+ " FROM Securables s"
			+ " ,SecurableTarget st"
			+ " ,AccountUser u "			
			+ " WHERE u.id=st.targetId AND st.id=s.securableTarget.id AND st.targetName='USER' AND u.id= :userId")
	UserDTO findFfmEnabledUserByUserId(@Param("userId") Integer userId);
	
	@Query("SELECT sc.userName,sc.password FROM SecurableTarget scblTarget,Securables sc where sc.securableTarget.id = scblTarget.id and  scblTarget.targetId = :targetId and scblTarget.targetName= :targetName " +
			" and sc.applicationData='CAPAGENTCONSUMERINFO' order by sc.created desc ")
	public List<Object[]> findCapAgentInfoByTargetIdAndTargetType(@Param("targetId") Integer targetId,@Param("targetName") SecurableTarget.TargetName targetName);

	@Query("SELECT sc FROM SecurableTarget scblTarget,Securables sc where sc.securableTarget.id = scblTarget.id and scblTarget.targetName='USER' and  scblTarget.targetId = :targetId" )
	public Securables findExistedFfmUserInfoByTargetId(@Param("targetId") Integer targetId);

}