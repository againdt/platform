package com.getinsured.hix.platform.util;

import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringResult;
import org.springframework.xml.transform.TransformerHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;



public final class SoapHelper {

	private static Logger LOGGER = Logger.getLogger(SoapHelper.class);

	private static final String LOG_GLOBAL_ID = "LOG_GLOBAL_ID";
	private static final String LOG_CORRELATION_ID 	= "LOG_CORRELATION_ID";


	private static Matcher errorsCodeMatcher = Pattern.compile(".+(<(\\w+)?(:)?errors>.*<(\\w+)?(:)?code>(\\w+)</(\\w+)?(:)?code>).+",Pattern.DOTALL).matcher("");

	private static Matcher wsdlMatcher = Pattern.compile("(http(s)?:.*\\d)+(.*)").matcher("");
	private static Matcher headerGlobalIdMatcher = Pattern.compile(".+(<(\\w+)?(:)?LOG_GLOBAL_ID>(.+)</(\\w+)?(:)?LOG_GLOBAL_ID>).+",Pattern.DOTALL).matcher("");
	private static Matcher headerCorrelationIdMatcher = Pattern.compile(".+(<(\\w+)?(:)?LOG_CORRELATION_ID>(.+)</(\\w+)?(:)?LOG_CORRELATION_ID>).+",Pattern.DOTALL).matcher("");
	private static Matcher applicationIdMatcher = Pattern.compile(".+(<(\\\\\\\\w+)?(:)?applicationId>(.+)</(\\\\\\\\w+)?(:)?applicationId>).+", Pattern.DOTALL).matcher("");

	/**
	 * Add the end point name in this EndPoint enum.
	 * @author nayak_b
	 *
	 */
	public enum EndPoint {
		DEFAULT("DEFAULT"),
		IND19("IND19"),
		IND20("IND20"),
		IND21("IND21"),
		AUTORENEWAL("AUTORENEWAL");
		
		@SuppressWarnings("unused")
		private String value;

        private EndPoint(String value) {
                this.value = value;
        }
	}
	
	/**
	 * This map stores methodName (operationName) from EndPoint.java as key and Endpoint_function as value e.g. IND19
	 * For Example:-
	 *
	 * ExchangePlanManagementServicesEndpoint.java has below methods exposed as operations
	 * transferPlan
	 * validateAndTransformDataTemplate
	 *
	 */
	private static Map<String, String> soapServerFunctionNameMap = new HashMap<String, String>();

	static{
		soapServerFunctionNameMap.put("transferPlan", "SERFF-TRANSFERPLAN");
		soapServerFunctionNameMap.put("validateAndTransformDataTemplate", "SERFF-VALIDATEANDTRANSFORMDATATEMPLATE");
		soapServerFunctionNameMap.put("getHouseholdId", "IND19");
		soapServerFunctionNameMap.put("getLowestPremium", "IND23");
		soapServerFunctionNameMap.put("deDesignateBroker", "IND48");
		soapServerFunctionNameMap.put("generateEntityRecordId", "IND65");
		soapServerFunctionNameMap.put("saveIssuerRemittanceDetail", "IND42");
		soapServerFunctionNameMap.put("getBenchMarkPremium", "IND22");
		soapServerFunctionNameMap.put("getEmployerPlanSelection", "IND06");
		soapServerFunctionNameMap.put("updateEnrollmentStatus", "EMPLOYER-ENROLLMENTSTATUS");
		soapServerFunctionNameMap.put("getAdminUpdateIndividual","IND57");
		soapServerFunctionNameMap.put("individualDisEnrollment","IND56");
		soapServerFunctionNameMap.put("employerDisEnrollment","IND29");
		soapServerFunctionNameMap.put("prepareATResponse", "ERP-TRANSFERACCOUNT");
		soapServerFunctionNameMap.put("processAutoRenewalInformation", "AUTORENEWAL");
		soapServerFunctionNameMap.put("enrollmentReinstatement", "IND69");
	}

	/**
	 * This map stores methodName (operationName) from EndPoint.java as key and wsdl as value
	 * To get this details:-
	 *
	 * ExchangePlanManagementServicesEndpoint.java has below methods exposed as operations
	 * transferPlan
	 * validateAndTransformDataTemplate
	 *
	 * From wsdl http://localhost:8080/exchange-hapi/services/ExchangePlanManagement.wsdl---> get everything after host and port no to get value
	 *
	 * So Value --> /exchange-hapi/services/ExchangePlanManagement.wsdl
	 */
	private static Map<String, String> soapServerWSDLMap = new HashMap<String, String>();

	static{
		soapServerWSDLMap.put("transferPlan", "/exchange-hapi/services/ExchangePlanManagement.wsdl");
		soapServerWSDLMap.put("validateAndTransformDataTemplate", "/exchange-hapi/services/ExchangePlanManagement.wsdl");
		soapServerWSDLMap.put("getHouseholdId", "/ghix-plandisplay/webservice/plandisplay/endpoints/IndividualInformationService.wsdl");
		soapServerWSDLMap.put("getLowestPremium", "/ghix-shop/webservice/endpoints/EmployeeLowestPremiumService.wsdl");
		soapServerWSDLMap.put("deDesignateBroker", "/ghix-broker/webservice/DeDesignateBrokerService.wsdl");
		soapServerWSDLMap.put("generateEntityRecordId", "/ghix-broker/webservice/GenerateEntityRecordIdService.wsdl");
		soapServerWSDLMap.put("saveIssuerRemittanceDetail", "/ghix-finance/webservice/finance/endpoints/issuerRemittanceServiceEndpoint.wsdl");
		soapServerWSDLMap.put("getBenchMarkPremium", "/ghix-planmgmt/webservice/planmgmt/endpoints/BenchMarkPlanService.wsdl");
		soapServerWSDLMap.put("getEmployerPlanSelection", "/ghix-shop/webservice/endpoints/EmployerPlanSelectionService.wsdl");
		soapServerWSDLMap.put("updateEnrollmentStatus", "/ghix-shop/webservice/endpoints/EmployerEnrollmentStatusService.wsdl");
		soapServerWSDLMap.put("getAdminUpdateIndividual", "/ghix-enrollment/webservice/enrollment/endpoints/adminUpdateIndividualService.wsdl");
		soapServerWSDLMap.put("individualDisEnrollment", "/ghix-enrollment/webservice/enrollment/endpoints/individualDisenrollmentService.wsdl");
		soapServerWSDLMap.put("employerDisEnrollment", "/ghix-enrollment/webservice/enrollment/endpoints/employerDisEnrollmentService.wsdl");
		soapServerWSDLMap.put("prepareATResponse", "/ghix-eligibility/endpoints/AccountTransfer/accountTransfer.wsdl");
		soapServerWSDLMap.put("processAutoRenewalInformation", "/ghix-plandisplay/webservice/plandisplay/endpoints/AutoRenewalService.wsdl");
		soapServerWSDLMap.put("enrollmentReinstatement", "/ghix-enrollment/webservice/enrollmentReinstatementService.wsdl");
	}

	// it doesn't make sense to instantiate this class
    private SoapHelper() {}
    
    private static Map<String, Jaxb2Marshaller> marshallers = Collections.synchronizedMap(new HashMap<String,Jaxb2Marshaller>());

    /**
     * To marshal given object.
     *
     * @param object
     * @return string as xml
     *
     * @throws JAXBException
     * @throws PropertyException
     */
    @SuppressWarnings("rawtypes")
	public static String marshal(Object object) throws JAXBException, PropertyException {
		Class clazz = null;
		if (object instanceof JAXBElement) {
			JAXBElement jaxbElement = (JAXBElement) object;
			clazz = jaxbElement.getDeclaredType();
		} else {
			clazz = object.getClass();
		}
		String clzPackage = clazz.getPackage().getName();
		Jaxb2Marshaller jaxbMarshaller = marshallers.get(clzPackage);
		if(jaxbMarshaller == null){
			jaxbMarshaller = new Jaxb2Marshaller();
			jaxbMarshaller.setContextPath(clzPackage);
			marshallers.put(clzPackage, jaxbMarshaller);
		}
		StringResult result = new StringResult();
		jaxbMarshaller.marshal(object, result);
		return result.toString();
	}

    /**
     * extract ResponseCode from given responsePayload.
     *
     * @param response
     * @return responseCode
     */
    public static String extractResponseCode(String response) {
    	/*Ekram : HIX-33045 - Added for Eligibility RP module */
    	if (StringUtils.isEmpty(response)){
    		/** safety-net to handle exception condition */
    		return "ERROR";
    	}
    	
    	Matcher responseMatcher = Pattern.compile(".+(<(\\w+)?(:)?responsecode>(\\w+)</(\\w+)?(:)?responsecode>).+",Pattern.DOTALL).matcher("");
    	
    	if(responseMatcher.reset(StringUtils.lowerCase(response)).matches()) {
    		String responseCodeStr = responseMatcher.group(4);
    		return getResponseCode(response, responseCodeStr);
    	} else if (errorsCodeMatcher.reset(StringUtils.lowerCase(response)).matches()) {
    		String errorsCodeStr = errorsCodeMatcher.group(6);
    		return getResponseCode(response, errorsCodeStr);
    	} else {
    		/** always return 200 - SUCCESS for valid payload */
    		return "200";
    	}
    }

	private static String getResponseCode(String response, String responseCodeStr) {
		if (StringUtils.isNotEmpty(responseCodeStr)){
			/** valid response code */
			return responseCodeStr;
		} else {
			/** safety-net to handle coding error */
			LOGGER.error("responseCode found as empty for response - " + response);
			return "ERROR";
		}
	}

	/**
     * returns function name for given Endpoint methodName.
     * <b> This is applicable for only Servers exposed by GI.
     *
     * @param methodName
     * @return function name e.g. IND35
     */
    public static String getEndpointFunctionName(String methodName){

		if (soapServerFunctionNameMap.containsKey(methodName)){
    		return soapServerFunctionNameMap.get(methodName);
    	}

    	return StringUtils.EMPTY;
    }

    /**
     * returns wsdl for given url and Endpoint methodName.
     * <b> This is applicable for only Servers exposed by GI.
     *
     * @param url
     * @param methodName
     * @return wsdl
     */
    public static String getWSDL(String url, String methodName){

    	if(wsdlMatcher.reset(url).matches()) {
    		if (soapServerWSDLMap.containsKey(methodName)){
        		return wsdlMatcher.group(1) + soapServerWSDLMap.get(methodName);
        	}
    	}

    	return StringUtils.EMPTY;
    }

    
    public static void extractHeader(Source source, HttpServletRequest request) throws Exception {
		String correlationId = null, globalId = null;
		String message = transferSource(source);
		if (message != null) {
            if(headerGlobalIdMatcher.reset(message).matches()){
            	globalId = headerGlobalIdMatcher.group(4);
            }
            if(headerCorrelationIdMatcher.reset(message).matches()){
            	correlationId = headerCorrelationIdMatcher.group(4);
            }
            request.setAttribute(LOG_CORRELATION_ID, correlationId );
        	request.setAttribute(LOG_GLOBAL_ID, globalId );
        }
	}
    
    public static void extractHeader(SoapMessage soapMessage, HttpServletRequest request) throws Exception {
		
		if(soapMessage.getSoapHeader()!=null){
			
			Iterator<SoapHeaderElement> it= soapMessage.getSoapHeader().examineAllHeaderElements();
			
			while(it.hasNext()){
				SoapHeaderElement soapHeaderElement = it.next();
				LOGGER.info("HEADER PARAMETER : " + soapHeaderElement.getName().getLocalPart() + ", HEADER PARAMETER VALUE : " + soapHeaderElement.getText());	
				
				if(soapHeaderElement.getName()!=null && soapHeaderElement.getName().getLocalPart().contains("LOG_CORRELATION_ID")){
					request.setAttribute(LOG_CORRELATION_ID, soapHeaderElement.getText()!=null?soapHeaderElement.getText():"");
				}else if(soapHeaderElement.getName()!=null && soapHeaderElement.getName().getLocalPart().contains("LOG_GLOBAL_ID")){
					request.setAttribute(LOG_GLOBAL_ID, soapHeaderElement.getText()!=null?soapHeaderElement.getText():"");
				}else{
					LOGGER.error("Soap header found Null.Cant't extract any value. HEADER: " + soapMessage.getSoapHeader());
				}
			}
		}
		
		
	}
    
    public static String transferSource(Source source) throws Exception{
		String message = null;
		if (source != null) {
			StringWriter writer = new StringWriter();
            Transformer transformer = new TransformerHelper().createTransformer();
    	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    	    transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.transform((Source) source, new StreamResult(writer));
            message = writer.toString();
            LOGGER.info("Soap message: "+message);
		}else{
        	LOGGER.error("source/ Message for soap is empty " + source);
        }
		return message;
	}
    
    public static void setResponseSoapHeader(SoapMessage soapMessage, HttpServletRequest httpServletRequest) throws Exception {
        org.springframework.ws.soap.SoapHeader soapHeader = soapMessage.getSoapHeader();
        String globalId = (httpServletRequest.getAttribute(LOG_GLOBAL_ID)!=null)?httpServletRequest.getAttribute(LOG_GLOBAL_ID).toString():"";
        String correlationId = (httpServletRequest.getAttribute(LOG_CORRELATION_ID)!=null)?httpServletRequest.getAttribute(LOG_CORRELATION_ID).toString():"";
        if(soapHeader.getResult() instanceof SAXResult){
        	QName correleationQname = new QName("http://webservice.hix.getinsured.com/logcorrelation",LOG_CORRELATION_ID);
        	QName globalQname = new QName("http://webservice.hix.getinsured.com/logglobal",LOG_GLOBAL_ID);
        	
        	SoapHeaderElement correlationElement = soapHeader.addHeaderElement(correleationQname);
        	SoapHeaderElement globalElement = soapHeader.addHeaderElement(globalQname);
        	
        	correlationElement.setText(correlationId);
        	globalElement.setText(globalId);
        }else{
        	 DOMResult xmlHeader = (DOMResult) soapHeader.getResult();
             Node headerNode = xmlHeader.getNode();   

             Element correlationIdElement = headerNode.getOwnerDocument().createElementNS("http://webservice.hix.getinsured.com/logcorrelation", LOG_CORRELATION_ID);
             correlationIdElement.appendChild(headerNode.getOwnerDocument().createTextNode(correlationId));              
             headerNode.appendChild(correlationIdElement);
             
             Element globalIdElement = headerNode.getOwnerDocument().createElementNS("http://webservice.hix.getinsured.com/logglobal", LOG_GLOBAL_ID);
             globalIdElement.appendChild(headerNode.getOwnerDocument().createTextNode(globalId));              
             headerNode.appendChild(globalIdElement);
        }
        // This is for logging the soap message. 
        transferSource(soapMessage.getEnvelope().getSource());
    }
    
	public static Long getSsapApplicationId(String endpointFunctionName, String requestPayload) {
		if ((endpointFunctionName != null)
				&& (endpointFunctionName.equals("AUTORENEWAL") || endpointFunctionName.equals("IND19"))) {

			if (applicationIdMatcher.reset(requestPayload).matches()) {

				return Long.parseLong(applicationIdMatcher.group(4));

			} else {
				return null;
			}
		} else {
			return null;
		}
	}
   
}
