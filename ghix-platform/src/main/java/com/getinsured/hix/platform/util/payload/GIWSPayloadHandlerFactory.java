package com.getinsured.hix.platform.util.payload;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.util.ModuleAwareClassLoader;
import com.getinsured.hix.platform.util.ModuleContextProvider;
import com.getinsured.hix.platform.util.SoapHelper.EndPoint;



/**
 * Factory class to get the handler.
 * @author nayak_b
 *
 */
public final class GIWSPayloadHandlerFactory {
	private static Logger logger = LoggerFactory.getLogger(GIWSPayloadHandlerFactory.class);
	private GIWSPayloadHandlerFactory(){
		
	}
	
	
private static Map<String, String> soapHandlerClassMap = new HashMap<String, String>();
	
	//This Map should always contain the endpoint name and the handler class with full package name.
	static{
		//One example would be.
		//TODO
		//Please remove the below commented line once we have one actual example.
		soapHandlerClassMap.put(EndPoint.IND19.toString(), "com.getinsured.hix.webservice.plandisplay.payload.IND19GIPayloadHandler");
		soapHandlerClassMap.put(EndPoint.AUTORENEWAL.toString(), "com.getinsured.hix.webservice.plandisplay.payload.AutoRenewalGIPayloadHandler");
		soapHandlerClassMap.put(EndPoint.IND21.toString(), "com.getinsured.ahbx.enrollment.payload.IND21GIPayloadHandler");
		soapHandlerClassMap.put(EndPoint.IND20.toString(), "com.getinsured.ahbx.enrollment.payload.IND20GIPayloadHandler");
	}
	/**
	 * This method should return the handler depending on the EndPointName name. Need to add else if for all new handler.
	 * @param endpointFunction
	 * @return
	 */
	/*public static GIWSPayloadHanlder getHandler(String endpointFunction){
		GIWSPayloadHanlder defaultPayloadHandler = null;
		logger.info("getHandle called with " + endpointFunction);
		
		 * Following if else provided for example. When we are going to add a new handler need to add
		 * else if condition for specific endpoint and return the specific handler object.   
		 * 
		try{
			//One example would be
			//TODO
			//Please remove the below commented if block once we have one actual example.
			if(EndPoint.IND19.toString().equalsIgnoreCase(endpointFunction)){
				defaultPayloadHandler = (GIWSPayloadHanlder)ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.IND19.toString())).newInstance();
			}else if(EndPoint.AUTORENEWAL.toString().equalsIgnoreCase(endpointFunction)){
				defaultPayloadHandler = (GIWSPayloadHanlder)ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.AUTORENEWAL.toString())).newInstance();
			}else if(EndPoint.IND20.toString().equalsIgnoreCase(endpointFunction)){
				defaultPayloadHandler = (GIWSPayloadHanlder)ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.IND20.toString())).newInstance();
			}else if(EndPoint.IND21.toString().equalsIgnoreCase(endpointFunction)){
				defaultPayloadHandler = (GIWSPayloadHanlder)ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.IND21.toString())).newInstance();
			}
		}catch(Exception e){
			defaultPayloadHandler = new DefaultPayloadHandler();
		}
		//Add else if when ever required.
		if(defaultPayloadHandler == null){
			defaultPayloadHandler = new DefaultPayloadHandler();
		}
		
		return defaultPayloadHandler;
	}*/
	
	public static GIWSPayloadHanlder getHandler(String endpointFunction){
		GIWSPayloadHanlder defaultPayloadHandler = null;
		Class<?> handlerClass = null;
		
		/*
		 * Following if else provided for example. When we are going to add a new handler need to add
		 * else if condition for specific endpoint and return the specific handler object.   
		 * */
		try{
			//One example would be
			//TODO
			//Please remove the below commented if block once we have one actual example.
			if(EndPoint.IND19.toString().equalsIgnoreCase(endpointFunction)){
				handlerClass = ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.IND19.toString()));
				defaultPayloadHandler = (GIWSPayloadHanlder) ModuleContextProvider.getModuleBeanByClass(handlerClass);
			}else if(EndPoint.AUTORENEWAL.toString().equalsIgnoreCase(endpointFunction)){
				handlerClass = ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.AUTORENEWAL.toString()));
				defaultPayloadHandler = (GIWSPayloadHanlder) ModuleContextProvider.getModuleBeanByClass(handlerClass);
			}else if(EndPoint.IND20.toString().equalsIgnoreCase(endpointFunction)){
				handlerClass = ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.IND20.toString()));
				defaultPayloadHandler = (GIWSPayloadHanlder) ModuleContextProvider.getModuleBeanByClass(handlerClass);
			}else if(EndPoint.IND21.toString().equalsIgnoreCase(endpointFunction)){
				handlerClass = ModuleAwareClassLoader.forName(soapHandlerClassMap.get(EndPoint.IND21.toString()));
				defaultPayloadHandler = (GIWSPayloadHanlder) ModuleContextProvider.getModuleBeanByClass(handlerClass);
			}
		}catch(Exception e){
			defaultPayloadHandler = new DefaultPayloadHandler();
			logger.info("Exception in getHandler", e);
		}
		//Add else if when ever required.
		if(defaultPayloadHandler == null){
			defaultPayloadHandler = new DefaultPayloadHandler();
		}
		
		return defaultPayloadHandler;
	}

}
