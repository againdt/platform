package com.getinsured.hix.platform.auditor.advice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chopra_s
 * 
 */
class GiAuditContextHolder {

	private GiAuditContextHolder() {
	}

	private static ThreadLocal<Map<String, Object>> instance = new ThreadLocal<Map<String, Object>>() {
		@Override
		protected Map<String, Object> initialValue() {
			return new HashMap<>();
		}
	};
	
	/**
	 * Clear the AuditContext
	 * 
	 */
	public static void clear() {
		instance.get().clear();
	}

	/**
	 * Retrieve the the Context data
	 * @return Map
	 */
	public static Map<String, Object> get() {
		return instance.get();
	}

	/**
	 * Set data in context map
	 * 
	 * @param key
	 * @param value
	 * 
	 */
	public static void put(String key, Object value) {
		instance.get().put(key, value);
	}
}
