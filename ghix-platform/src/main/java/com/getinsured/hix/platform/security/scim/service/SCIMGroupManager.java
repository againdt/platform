package com.getinsured.hix.platform.security.scim.service;

import java.util.List;

import org.wso2.charon.core.objects.Group;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.exception.GIException;

public interface SCIMGroupManager {
	
	/**
	 * This method is used to find the SCIM Group by using a filter
	 * 
	 * @param filter
	 *            String containing filter used to fetch group
	 * @return Group object containing SCIM Group attributes
	 * @throws GIException
	 */
	Group findGroup(String filter) throws GIException;
	
	/**
	 * This method is used to find the SCIM Group by using a filter
	 * 
	 * @param filter
	 *            String containing filter used to fetch group
	 * @return Group object containing SCIM Group attributes
	 * @throws GIException
	 */
	List<String> findGroups() throws GIException;
	
	/**
	 * Method to add a user to a group using user's email id and target group
	 * name
	 * 
	 * @param emailId
	 *            String containing email address
	 * @param groupName
	 *            String containing group name
	 * @throws GIException
	 */
	void addUserToGroup(String emailId, AccountUser accountUser,
			String groupName) throws GIException;

}
