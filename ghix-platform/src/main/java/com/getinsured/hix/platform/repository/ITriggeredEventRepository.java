package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.TriggeredEvent;

public interface ITriggeredEventRepository extends JpaRepository<TriggeredEvent, Integer> {

}
