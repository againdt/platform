package com.getinsured.hix.platform.config;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;

import com.getinsured.hix.platform.couchbase.service.CouchBucketService;
import com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument;

public class CoucbaseHealthIndicator extends AbstractHealthIndicator{
	
	
	private static final String RANDOM_ID = "PT::LOC::123";
	private CouchBucketService couchBucketService;
	
	public CoucbaseHealthIndicator(CouchBucketService couchBucketService) {
		this.couchBucketService = couchBucketService;
	}

	@Override
	protected void doHealthCheck(Builder builder) throws Exception {
		try{
			couchBucketService.getDocumentById(RANDOM_ID, CouchEcmDocument.class);
			builder.up().build();
		} catch(Exception e){
			builder.down(e);
		}
	}

}
