package com.getinsured.hix.platform.location.service.jpa;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AddressValidatorResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.location.service.AddressValidatorService;

/**
 * This is Location Validator API. It will be used by VIMO Services internally.
 * This implements Strategy Design Pattern. Based on configuration in
 * .properties file address validator component will be chosen. If the value is
 * empty, default component will be used and it returns the given value without
 * doing anything. This is not runtime configuration.
 * <p>
 * FIXME add performance monitoring and usage monitoring
 * <p>
 * 
 * @author polimetla_b
 * @since 10/30/2012
 */
@Service("addressValidatorService")
@Repository
public class AddressValidatorServiceImpl implements AddressValidatorService {

	@Value("#{configProp['address_validator_source']}")
	private String validatorComponent;

	private AddressValidatorComponent context;

	@Autowired
	private AddressValidatorSmartyStreet addressValidatorSmartyStreet;
	@Autowired
	private AddressValidatorUSPS addressValidatorUSPS;
	@Autowired
	private AddressValidatorDefault addressValidatorDefault;
	@Autowired
	private AddressValidatorOEDQ addressValidatorOEDQ;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AddressValidatorServiceImpl.class);

	/**
	 * This is implementing Strategy Pattern based on validatorComponent
	 * property in properties file.
	 */
	@PostConstruct
	public void contextInitilizer() {
		if(LOGGER.isTraceEnabled()){
			LOGGER.trace("contextInitilizer - Address Validator Configured for: " + validatorComponent);
		}
		if ("smartystreet".equalsIgnoreCase(validatorComponent)) {
			context = addressValidatorSmartyStreet;
			return;
		}
		if ("usps".equalsIgnoreCase(validatorComponent)) {
			context = addressValidatorUSPS;
			return;
		}
		if ("oedq".equalsIgnoreCase(validatorComponent)) {
			context = addressValidatorOEDQ;
			return;
		}
		context = addressValidatorDefault;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.location.service.LocationValidatorService#
	 * validateLocation(com.getinsured.hix.platform.location.model.Location)
	 */
	@Override
	public AddressValidatorResponse validateAddress(Location address) {

		AddressValidatorResponse response = new AddressValidatorResponse();
		response.startResponse();
		response.setValidatorName(validatorComponent);
		try {
			LOGGER.debug("validatorComponent =====>" + validatorComponent);
			List<Location> result = context.validateAddress(address);

			response.setList(result);
			response.setStatus("success");
		} catch (Exception ex) {
			response.setStatus("failed");
			response.setErrMsg(ex.getMessage());
			LOGGER.warn("Exception==>", ex);
		}
		response.endResponse();
		return response;
	}

	@Override
	public AddressValidationResponse validateAddress(LocationDTO address) throws Exception{
		
		AddressValidationResponse response = new AddressValidationResponse();
		response = context.validateAddress(address);
		return response;
	}

	public String getValidatorComponent() {
		return validatorComponent;
	}

	public void setValidatorComponent(String validatorComponent) {
		this.validatorComponent = validatorComponent;
	}

	// public void postProcessAfterInitialization() throws Exception {
	// contextInitilizer();
	//
	// }

	public AddressValidatorSmartyStreet getAddressValidatorSmartyStreet() {
		return addressValidatorSmartyStreet;
	}

	public void setAddressValidatorSmartyStreet(
			AddressValidatorSmartyStreet addressValidatorSmartyStreet) {
		this.addressValidatorSmartyStreet = addressValidatorSmartyStreet;
	}

	public AddressValidatorUSPS getAddressValidatorUSPS() {
		return addressValidatorUSPS;
	}

	public void setAddressValidatorUSPS(
			AddressValidatorUSPS addressValidatorUSPS) {
		this.addressValidatorUSPS = addressValidatorUSPS;
	}

	public AddressValidatorDefault getAddressValidatorDefault() {
		return addressValidatorDefault;
	}

	public void setAddressValidatorDefault(
			AddressValidatorDefault addressValidatorDefault) {
		this.addressValidatorDefault = addressValidatorDefault;
	}

	public AddressValidatorOEDQ getAddressValidatorOEDQ() {
		return addressValidatorOEDQ;
	}

	public void setAddressValidatorOEDQ(AddressValidatorOEDQ addressValidatorOEDQ) {
		this.addressValidatorOEDQ = addressValidatorOEDQ;
	}

}
