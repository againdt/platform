package com.getinsured.hix.platform.security.session;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserSession;
import com.getinsured.hix.model.UserSessionStatus;
import com.getinsured.hix.platform.security.repository.IUserSessionRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TimeShifterUtil;

@Service("sessionTrackerService")
@DependsOn("platformConstants")
public class SessionTrackerServiceImpl implements SessionTrackerService,
		ApplicationListener<SessionDestroyedEvent> {

	public static final String DENY_ALL = "deny_all";
	public static final String ALLOWED = "allowed";
	public static final String PRIVILEGED = "deny_privileged";
	private static boolean sessionTrackingEnabled = true;

	private static final Logger logger = LoggerFactory
			.getLogger(SessionTrackerServiceImpl.class);
	@Autowired
	private HIXHTTPClient client;
	@Autowired
	private IUserSessionRepository repo;
	@Autowired
	private UserService userService;
	
	@Autowired
	private EntityManagerFactory emf;
	
	private static Map<String, HttpSession> sessionMap = Collections
			.synchronizedMap(new HashMap<String, HttpSession>());
	private static Map<String, String> urlMap = Collections
			.synchronizedMap(new HashMap<String, String>());
	private int localNodeSessionTrackerPort = GhixPlatformConstants.LOCAL_NODE_PORT;
	private String localNodeAddress = GhixPlatformConstants.LOCAL_NODE_IP
			+ (localNodeSessionTrackerPort > 0 ? (":" + localNodeSessionTrackerPort)
					: "");
	
	@PostConstruct
	public void initService(){
		sessionTrackingEnabled = GhixPlatformConstants.PLATFORM_NODE_PROFILE.trim().equalsIgnoreCase("web");
	}
	
	
	
	
	private boolean checkNodeProfile(){
		if(!sessionTrackingEnabled){
			return false;
		}
		return true;
	}
	
	@Override
	public UserSession invalidateSession(UserSession userSession) {
		
		if(!checkNodeProfile() && null == userSession ){
			return null;
		}
		
		if(logger.isDebugEnabled()){
			logger.debug("Invalidating a remote session for user:" + userSession.getUser().getId());
		}
		
		long sessionTimeOut = -1;
		UserSession savedSession = null;
		String status= null;
		String userSessionNodeIp = userSession.getNodeId();
		if (!userSessionNodeIp.equalsIgnoreCase(localNodeAddress)) {
			if (!invalidateRemoteSession(
					GhixPlatformConstants.LOCAL_NODE_PROTOCOL,
					userSessionNodeIp, userSession.getSessionId())) {
				logger.error("Failed to invalidate the remote session with id:"+userSession.getSessionId()+" at:"+userSessionNodeIp);
				throw new GIRuntimeException(
						"Failed invalidating the remote session ["+userSession.getSessionId()+"] at "
								+ userSessionNodeIp);
			}
			// Session has been invalidated and saved by the remote node
			savedSession = userSession;
		} else {
			String memMapSessionId = userSession.getSessionId();
			if(logger.isDebugEnabled()){
			logger.debug("Found local session [" + memMapSessionId
					+ "] with status:" + userSession.getStatus()
					+ " Closing it");
			}
			try {
				HttpSession httpSession = sessionMap.get(memMapSessionId);
				if(httpSession != null){
					sessionTimeOut = httpSession.getMaxInactiveInterval()*1000;
					httpSession.invalidate();
					if(logger.isInfoEnabled()){
						logger.info("Removed session ["+memMapSessionId+"] from MemMap");
					}
					sessionMap.remove(memMapSessionId);
				}else{
					if(logger.isInfoEnabled()){
						logger.info("No cache session found in memory for id:"+memMapSessionId+" Probably an old cached session created on "+userSessionNodeIp+" could not be cleaned due to server re-start");
						// Nothing to invalidate
					}
				}
			} catch (IllegalStateException ie) {
				logger.error("Error encountered while invalidating the session ["+memMapSessionId+"]:Message "+ie.getMessage()+", Ignoring");
			}
			userSession.setStatus(UserSessionStatus.CLOSED.toString());
			long timeNow = TimeShifterUtil.currentTimeMillis();
			long timeCreated = userSession.getLoginTime().getTime();
			if (sessionTimeOut > 0 && ((timeNow - timeCreated) >= sessionTimeOut)) {
				if(logger.isDebugEnabled()){
					logger.debug("Session with Id:" + userSession.getSessionId()
						+ " for user ["+userSession.getUser().getId()+"] Has timedout..Time now ["+timeNow+"] Created at ["+timeCreated+","+userSession.getLoginTime()+"] Inactive for: ["+(timeNow - timeCreated)+"] ..");
				}
				status = UserSessionStatus.TIMEDOUT.toString();
			} else {
				status = UserSessionStatus.CLOSED.toString();
			}
			userSession.setStatus(status);
			userSession.setLogoutTime(new Timestamp(timeNow));
			savedSession = repo.saveAndFlush(userSession);
		}
		if(savedSession == null){
			logger.error("Failed to invalidate the session:"+userSession.getSessionId()+" Created for:"+userSession.getUser().getId()+" Created At:"+userSession.getLoginTime());
		}
		return savedSession;
	}
	
	@Override
	public List<UserSession> invalidateAllSessions(List<UserSession> sessions, int userId) {
		List<UserSession> invalidatedSessions = new ArrayList<UserSession>();
		if(!checkNodeProfile()){
			return invalidatedSessions;
		}
		
		if(sessions == null){
			logger.error("No user session provided for invalidation, expected minimum 1");
			return invalidatedSessions;
		}
		UserSession invalidated = null;
		for(UserSession toBeInvalidated: sessions){
			if(logger.isDebugEnabled()){
				logger.debug("Invalidating session {} for user {}",toBeInvalidated.getSessionId(),userId);
			}
			if((invalidated = this.invalidateSession(toBeInvalidated)) != null){
				invalidatedSessions.add(invalidated);
			}
		}
		return invalidatedSessions;
	}
	
	private boolean invalidateRemoteSession(String protocol, String server,
			String sessionId) {
		
		if(!checkNodeProfile()){
			return false;
		}
		logger.info("Invalidating a remote session on:" + server);

		String serverUrl = urlMap.get(server);
		if (serverUrl == null) {
			serverUrl = protocol + "://" + server
					+ "/hix/invalidateSession?sessionId=";
			urlMap.put(server, serverUrl);
		}
		try {
			String encSessionId = GhixAESCipherPool.encrypt(sessionId);
			String response = client.getGETData(serverUrl + encSessionId);
			if (response.contains("SUCCESS")) {
				logger.info("Successfully invalidated remote session at Node:"
						+ serverUrl);
				logger.debug("RESPONSE RECEIVED:" + response);
				return true;
			}
		} catch (IOException  e) {
			this.markUserSessionTimedOut(sessionId, true);
			logger.error("Failed to process the session invalidation request cleanly error:"+e.getMessage()+" Server Node:"+server+" is either not available or permanently removed from the cluster, in either case user login attemot is a valid one, marking the stale record to timed out");
			return true;
		} catch (Exception e) {
			logger.error("Failed to process the session invalidation request: Error message:"+e.getMessage(),e);
		}
		return false;
	}

	public boolean checkSessionMultiplicityAllowed(boolean privileged) {
		boolean allowed = false;
		if (GhixPlatformConstants.MULTIPLE_SESSION_MODE
				.equalsIgnoreCase(ALLOWED)) {
			allowed=true;
		} else if (GhixPlatformConstants.MULTIPLE_SESSION_MODE
				.equalsIgnoreCase(DENY_ALL)) {
			allowed= false;
		} else if (GhixPlatformConstants.MULTIPLE_SESSION_MODE
				.equalsIgnoreCase(PRIVILEGED)) {
			if (privileged) {
				allowed= false;
			} else {
				allowed= true;
			}
		} else {
			logger.error("Invalid SESSION MODE configured "
					+ GhixPlatformConstants.MULTIPLE_SESSION_MODE +" Not allowing multiple sessions");
		}
		if(logger.isDebugEnabled()){
			logger.debug("Session multiplicity check for isPrivileged ["+privileged+"]:"+allowed);
		}
		return allowed;
	}

	@Override
	public UserSession fetchUserSession(int userId) {
		List<UserSession> userSessionList = null;
		UserSession userSession = null;
		
		try {
			userSessionList = repo.findRegisteredUserSession(userId);
			if(null != userSessionList && !userSessionList.isEmpty()) {
				userSession = userSessionList.get(0);
			}
		} catch (Exception ex) {
			
		}
		
		return userSession;
	}

	/**
	 * This should be called only after invalidating all other sessions
	 */
	@Override
	public boolean createUserSession(HttpSession httpSession, String browserIp,
			AccountUser user, boolean privileged) {
		if(!checkNodeProfile()){
			return true; //Always return true, we are not tracking the session on this node 
		}
		String sessionId = httpSession.getId();
		boolean savedSession = false;
		EntityManager manager = this.emf.createEntityManager();
		// At this point , if remote session exists, we have invalidated that
		// session
		
		UserSession session = new UserSession();
		
		session.setUser(user);
		session.setBrowserIp(this.getBrowserIpAddress());
		session.setNodeId(localNodeAddress);
		session.setSessionId(sessionId);
		session.setStatus(UserSessionStatus.IN_PROGRESS.toString());
		logger.info("Saving session with user {}", user.toString());
		try {
			manager.getTransaction().begin();
			user = manager.find(AccountUser.class, user.getId());
			session.setUser(user);
			manager.persist(session);
			manager.getTransaction().commit();
			sessionMap.put(sessionId, httpSession);
			savedSession = true;
		}catch(Exception e) {
			logger.error("Failed saving the session {}", e.getMessage());
		}finally {
			manager.close();
		}
		return savedSession;
	}

	@Override
	public boolean createUserSessionOnRegistration(HttpSession httpSession,
			String browserIp, long userId, String sessionVal, boolean privileged) {
		long sessionTimeOut = httpSession.getMaxInactiveInterval() * 1000;
		logger.info("Max allowed session Inactive interval:" + sessionTimeOut);
		
		String sessionId = httpSession.getId();
		
		if(!checkNodeProfile()){
			return false;
		}
		AccountUser user = this.userService.findById((int)userId);
		try {
			logger.info("Registering new session");
			UserSession savedSession = null;
			// At this point , if remote session exists, we have invalidated that
			// session
			UserSession session = new UserSession();
			session.setUser(user);
			session.setBrowserIp(this.getBrowserIpAddress());
			session.setNodeId(localNodeAddress);
			session.setSessionId(sessionId);
			session.setStatus(UserSessionStatus.INIT.toString());
			session.setSessionVal(sessionVal);
			savedSession = repo.saveAndFlush(session);
			if (savedSession != null) {
				sessionMap.put(sessionId, httpSession);
				logger.info("Session with Id:" + sessionId + " registered with ID:"
						+ savedSession.getId() + "for user " + userId);
				return true;
			} else {
				logger.error("No saved session returned, I was supposed to get a saved session here");
			}
		} catch (Exception ex) {
			logger.error("Failed to create the user session for user:" + userId
					+ " from browser IP:" + browserIp + " from Node:"
					+ GhixPlatformConstants.LOCAL_NODE_IP);
		}
		
		return true;
	}

	@Override
	public boolean markUserSessionClosed(String sessionId) {
		
		if(!checkNodeProfile()){
			return false;
		}
		boolean success = false;
		boolean debug = logger.isDebugEnabled();
		if(debug){
			logger.debug("Session close event for session Id:" + sessionId);
		}
		UserSessionStatus currentStatus = UserSessionStatus.CLOSED;
		HttpSession memMapedSession = sessionMap.get(sessionId);
		if (memMapedSession == null) {
				logger.error("No in memory session found for session id:"
					+ sessionId + " Could be an abandoned session, already cleaned up");
			success=true;
		}else{
			try{
				memMapedSession.invalidate();
				if(logger.isInfoEnabled()) {
					logger.info("Invalidaed the session [{}] from the memmap",sessionId);
				}
				success = true;
			}catch(IllegalStateException ie){
				logger.error("Error encountered ["+ie.getMessage()+ "]  while invalidating session with ID:"+sessionId+" probably already closed");
			}
			if(debug){
				logger.debug("Mark Close:Removing session ["+sessionId+"] from MemMap");
			}
			sessionMap.remove(sessionId);
		}
		// Now clean up the database, we are interested in local sessions only
		List<UserSession> sessions = repo.findLocalActiveSessionsBySessionId(localNodeAddress, sessionId);
		if (sessions != null){
			if(sessions.size() == 0) {
				if(logger.isInfoEnabled()) {
					logger.info("Anonymous session found for session id: {}", sessionId);
				}
			}
			if(sessions.size() > 1){
				logger.error("Help, Multiple sessions ["+sessions.size()+"] found for session id:"+sessionId+" Don't know what to do with those sessions");
				currentStatus = UserSessionStatus.DUP_STALE_SESSION;
			}
		
			for(UserSession tempSession:sessions){
				if(logger.isDebugEnabled()){
					logger.error("Session ID:"+tempSession.getSessionId()+"\t[User Id]"+tempSession.getUser().getId()+"\t[Node Id]"+ tempSession.getNodeId()+"\t[Logged In]"+ tempSession.getLoginTime());
				}
				tempSession.setStatus(currentStatus.toString());
				tempSession.setLogoutTime(new TSTimestamp());
				tempSession = repo.saveAndFlush(tempSession);
				if(tempSession != null){
					if(logger.isInfoEnabled()) {
						logger.info("Update DB session {} Successfully",sessionId);
					}
				}else{
					logger.error("User Session update did not return the saved session, could be an issue");
				}
			}
		}
		return success;
	}

	@Override
	public boolean markUserSessionLoggedOutByApplication(String sessionId) {

		if(!checkNodeProfile()){
			return false;
		}
		HttpSession memMapedSession = sessionMap.get(sessionId);
		UserSession tmp = null;
		if (memMapedSession == null) {
			logger.info("No in memory session found for session id:"
					+ sessionId);
		}else{
			logger.info("Mark Logout:Removing session ["+sessionId+"] from MemMap");
			sessionMap.remove(sessionId);
		}
		List<UserSession> sessions = repo.findLocalActiveSessionsBySessionId(localNodeAddress, sessionId);
		if (sessions != null ){
			if (sessions.size() == 0) {
				logger.info("In memory session is not available, no session in repository for session Id:"
						+ sessionId + " nothing to do");
				return false;
			}
			for (UserSession session : sessions) {
				session.setStatus(UserSessionStatus.USER_LOGOUT_BY_APPLICATION.toString());
				session.setLogoutTime(new TSTimestamp());
				tmp = repo.saveAndFlush(session);
				if (tmp == null) {
					logger.error("UserSession update by app failed for session Id:"
								+ sessionId);
				}
			}
		}
		return true;
	}
	
	@Override
	public boolean markUserSessionLoggedOut(String sessionId) {
		if(logger.isDebugEnabled()) {
			logger.debug("User logout event for session Id: {}",sessionId);
		}
		if(!checkNodeProfile()){
			return false;
		}
		HttpSession memMapedSession = sessionMap.get(sessionId);
		UserSession tmp = null;
		if (memMapedSession == null) {
			if(logger.isDebugEnabled()) {
				logger.debug("No in memory session found for session id:{}", sessionId);
			}
		}else{
			sessionMap.remove(sessionId);
		}
		List<UserSession> sessions = repo.findLocalActiveSessionsBySessionId(localNodeAddress, sessionId);
		if (sessions != null ){
			if (sessions.size() == 0) {
				if(logger.isDebugEnabled()) {
					logger.debug("In memory session is not available, no session in repository for session Id: {} nothing to do",sessionId);
				}
				return false;
			}
			for (UserSession session : sessions) {
				session.setStatus(UserSessionStatus.USER_LOGOUT.toString());
				session.setLogoutTime(new TSTimestamp(System.currentTimeMillis()));
				tmp = repo.saveAndFlush(session);
				if (tmp == null) {
					logger.error("UserSession update failed for session Id:"
								+ sessionId);
				}
			}
		}
		return true;
	}

	@Override
	public void cleanupMemoryMappedSessions() {
		if(!checkNodeProfile()){
			return;
		}
		int availableSessions = sessionMap.size();
		if(logger.isTraceEnabled()){
			logger.trace("Initiating MemMap clean up with "+sessionMap.size()+" Sessions");
		}
		long sessionInactivityTime = -1l;
		List<String> sessionsToClose = new ArrayList<String>();
		Set<Entry<String, HttpSession>> sessionsSet = sessionMap.entrySet();
		Entry<String, HttpSession> entry = null;
		HttpSession temp = null;
		synchronized(sessionMap){
			Iterator<Entry<String, HttpSession>> cursor = sessionsSet.iterator();
			while (cursor.hasNext()) {
				entry = cursor.next();
				temp = entry.getValue();
				try {
					sessionInactivityTime = temp.getMaxInactiveInterval() * 1000;// Seconds
																					// to
																					// Millis
					long currentTime = TimeShifterUtil.currentTimeMillis();
					long currentInactiveTime = currentTime - temp.getLastAccessedTime();
					if (currentInactiveTime >= sessionInactivityTime) {
						if(logger.isInfoEnabled()){
							logger.info("Marking session:" +entry.getKey()+" for close, found inactive for "+currentInactiveTime+" Maximum allowed:"+sessionInactivityTime);
						}
						sessionsToClose.add(entry.getKey());
					}
				} catch (IllegalStateException ie) {
					// We have to mark this session closed anyways, session has
					// already been invalidated
					logger.warn("Illegal session state for session Id:"
							+ entry.getKey() + " Marking for close");
					sessionsToClose.add(entry.getKey());
				}
			}
		}
		
		// Now check stale sessions in the database
		List<UserSession> staleSessions = this.repo.findLocalActiveSessions(localNodeAddress);
		for(UserSession session: staleSessions){
			if(!sessionMap.containsKey(session.getSessionId())){
				//Not in memory map but in DB, close it
				if(logger.isInfoEnabled()){
					logger.info("Marking session:" +session.getSessionId()+" for close, No MemMap entry found, could be a stale session");
				}
				session.setLogoutTime(new TSTimestamp(System.currentTimeMillis()));
				session.setStatus(UserSessionStatus.STALE_SESSION.toString());
				repo.save(session);
			}
		}
		for (String sessionId : sessionsToClose) {
			markUserSessionClosed(sessionId);
		}
		if(logger.isInfoEnabled()){
			logger.info("Done cleaning up the session memory map, started with:"
				+ availableSessions + " Now available:" + sessionMap.size());
		}
	}

	@Override
	public boolean killUserActiveSession(int userId) {
		logger.debug("Initiating local active sessions kill");
		if(!checkNodeProfile()){
			return false;
		}
		List<UserSession> sessions = repo.findActiveUserSession(userId);
		String remoteNodeIp;
		if (sessions != null && sessions.size() > 0) {
			for (UserSession session : sessions) {
				remoteNodeIp = session.getNodeId();
				if (remoteNodeIp != localNodeAddress) {
					logger.info("Initiating remote session invalidation for session:"+session.getSessionId());
					invalidateRemoteSession(
							GhixPlatformConstants.LOCAL_NODE_PROTOCOL,
							remoteNodeIp, session.getSessionId());
				} else {
					markUserSessionClosed(session.getSessionId());
				}
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.ApplicationListener#onApplicationEvent(org
	 * .springframework.context.ApplicationEvent) Only reason we are receiving
	 * this event because a session is destroyed because 1) User Logged out :
	 * User Session will indicate the STATUS = USER_LOGOUT/ Memory map is
	 * already cleaned 2) User timed out: User Session will indicate STATUS =
	 * IN_PROGRESS
	 */
	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {
		if(!checkNodeProfile()){
			return;
		}
		String sessionId = event.getId();
		
		HttpSession session = sessionMap.get(sessionId);
		if (session != null) {
			if(logger.isInfoEnabled()){
				logger.info("SESSION DESTROYED Event received:" + sessionId+" attenting to clean up");
			}
			markUserSessionTimedOut(sessionId);
			sessionMap.remove(sessionId);
		}
		TimeShifterUtil.cleanup(sessionId);
	}

	public void markUserSessionTimedOut(String sessionId) {
		if(logger.isDebugEnabled()){
			logger.debug("User timedout event for session Id:" + sessionId);
		}
		if(!checkNodeProfile()){
			return;
		}
		List<UserSession> sessions = repo.findLocalActiveSessionsBySessionId(localNodeAddress, sessionId);
		if (sessions != null ){
			if (sessions.size() == 0) {
				if(logger.isDebugEnabled()){
					logger.debug("No session in repository for session Id:"
						+ sessionId + " nothing to do");
				}
			}
			UserSession tmp = null;
			for (UserSession session : sessions) {
				session.setStatus(UserSessionStatus.TIMEDOUT.toString());
				session.setLogoutTime(new TSTimestamp(System.currentTimeMillis()));
				tmp = repo.saveAndFlush(session);
				if (tmp == null) {
					logger.error("UserSession update for timeout failed for session Id:"
								+ sessionId);
				}
			}
		}
	}
	
	public void markUserSessionTimedOut(String sessionId, boolean force) {
		if(logger.isDebugEnabled()){
			logger.debug("User timedout event for session Id:" + sessionId);
		}
		if(!checkNodeProfile()){
			return;
		}
		List<UserSession> sessions = null;
		if(force){
			// fetch all the active sessions from all the nodes
			sessions = repo.findActiveSessionBySessionId(sessionId);
		}else{
			// Fetch only the active local sessions
			sessions = repo.findLocalActiveSessionsBySessionId(localNodeAddress, sessionId);
		}
		if (sessions != null ){
			if (sessions.size() == 0) {
				if(logger.isDebugEnabled() && force){
					logger.debug("No session in repository for session Id:"
						+ sessionId + " Nothing to do");
				}
			}
			UserSession tmp = null;
			for (UserSession session : sessions) {
				session.setStatus(UserSessionStatus.TIMEDOUT.toString());
				session.setLogoutTime(new TSTimestamp(System.currentTimeMillis()));
				tmp = repo.saveAndFlush(session);
				if (tmp == null) {
					logger.error("UserSession update for timeout failed for session Id:"
								+ sessionId);
				}
			}
		}
	}
	
	@Override
	public boolean markUserSessionInitToInProgress(String sessionId, int userId) {
		if(logger.isDebugEnabled()){
			logger.debug("User timedout event for user Id:" + userId);
		}
		
		if(!checkNodeProfile()){
			return true;
		}
		List<UserSession> sessions = repo.findRegisteredUserSession(userId);
		
		if (sessions != null ){
			if (sessions != null && sessions.size() == 0) {
				logger.info("In memory session is not available, no session in repository for user Id:"
						+ userId + " nothing to do");
				return false;
			}
			UserSession tmp = null;
			for (UserSession session : sessions) {
				if (session.getStatus().equalsIgnoreCase(
						UserSessionStatus.INIT.toString())) {
					session.setSessionId(sessionId);
					session.setStatus(UserSessionStatus.IN_PROGRESS.toString());
					tmp = repo.saveAndFlush(session);
					if (tmp == null) {
						logger.error("UserSession update for in progress failed for user Id:"
								+ userId);
					}
				}
			}
		}
		return true;
	}
	
	private void storeObject(StringBuilder sb, Object obj, String sessionAttrKey) {
		try {
			if(obj instanceof AccountUser){
				AccountUser user = (AccountUser)obj;
				obj = "AccountUser#".intern()+user.getId();
			}
			ByteArrayOutputStream data = new ByteArrayOutputStream();
			Base64OutputStream bos = new Base64OutputStream(data);
			ObjectOutputStream objout = new ObjectOutputStream(bos);
			objout.writeObject(obj);
			objout.flush();
			bos.flush();
			bos.close();
			data.flush();
			objout.close();
			sb.append(new String(data.toByteArray()));
		} catch (IOException ignored) {
			logger.error("Error Storing the object for key "+sessionAttrKey, ignored);
		}
	}

	private Object getObject(String str, String sessionKey) {
		try {
			ByteArrayInputStream data = new ByteArrayInputStream(str.getBytes());
			Base64InputStream bos = new Base64InputStream(data);
			ObjectInputStream objin = new ObjectInputStream(bos);
			Object obj = objin.readObject();
			objin.close();
			if(obj instanceof String){
				String tmp = (String)obj;
				if(tmp.startsWith("AccountUser#".intern())){
					String id = tmp.substring("AccountUser#".intern().length());
					try{
						obj = this.userService.findById(Integer.parseInt(id));
					}catch(Exception ne){
						// Ignored
					}
				}
			}
			return obj;
		} catch (IOException | ClassNotFoundException ignored) {
			logger.error("Error retrieving the object fromthe session for key "+sessionKey, ignored);
		}
		return null;
	}
	/**
	 * @param request
	 * @param newAccountUser
	 */
	
	public void storeHttpSessionInDB(HttpServletRequest request,
			AccountUser newAccountUser) {
		if(!checkNodeProfile()){
			return;
		}
		HttpSession localHttpSession = request.getSession(false);
		StringBuilder sb = new StringBuilder();
		if (null != localHttpSession) {
			Enumeration<String> sessionAttrEnums = localHttpSession
					.getAttributeNames();
			while (sessionAttrEnums.hasMoreElements()) {
				String sessionAttrKey = sessionAttrEnums.nextElement();
				Object sessionAttrVal = localHttpSession
						.getAttribute(sessionAttrKey);
				sb.append(sessionAttrKey).append(":");
				storeObject(sb, sessionAttrVal,sessionAttrKey);
						sb.append(sessionAttrVal).append(",");
			}
			Role currUserDefRole = userService
					.getDefaultRole(newAccountUser);
			boolean isPrivileged = currUserDefRole.getPrivileged() == 1;
			createUserSessionOnRegistration(
					localHttpSession, request.getRemoteAddr(),
					newAccountUser.getId(), sb.toString(), isPrivileged);
		}
	}
	
	/**
	 * @param request
	 * @param userSession
	 * @param httpSession
	 * @param user
	 * @return
	 */
	@Override
	public HttpSession replaceHttpSessionWithDBSession(HttpServletRequest request,
			UserSession userSession, HttpSession httpSession, AccountUser user) {
		if(!checkNodeProfile()){
			return null;
		}
		if (null != userSession) {
			httpSession = request.getSession();
			String sessionAttrs = userSession.getSessionVal();
			if (null != sessionAttrs && !sessionAttrs.isEmpty()) {
				if (logger.isInfoEnabled()) {
					logger.info("DB SESSION RETRIEVAL: USR: "
							+ user.getUsername() + " ATTRS: " + sessionAttrs);
				}

				String[] sessionParams = sessionAttrs.split(",");

				if (null != httpSession) {
					for (String sessionParam : sessionParams) {
						if (null != sessionParam && !sessionParam.isEmpty()
								&& sessionParam.indexOf(":") > 0) {
							String[] sessionAttr = sessionParam.split(":");
							String sessionKey = sessionAttr[0];
							Object sessionVal = "";
							if (sessionAttr.length == 2) {
								sessionVal = getObject(sessionAttr[1], sessionKey);
							}
							if (sessionKey.equalsIgnoreCase("sUser")) {
								httpSession.setAttribute("sUser", user);
							} else {
								httpSession
										.setAttribute(sessionKey, sessionVal);
							}

						}
					}
				}
			}
		}
		
		return httpSession;
	}
	
	private String getBrowserIpAddress(){
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest req = sra.getRequest();
		String ipAddress = req.getHeader("X-FORWARDED-FOR");
		if (StringUtils.isEmpty(ipAddress)) {
			ipAddress = req.getRemoteAddr();
		}
		return ipAddress;
	}




	@Override
	public boolean createUserSession(HttpSession session, String browserIp, long userId, boolean privileged) {
		AccountUser user = this.userService.findById((int)userId);
		return this.createUserSession(session, browserIp, user, privileged);
	}
}
