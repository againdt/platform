package com.getinsured.hix.platform.emailstat.service;

import java.util.Map;

import com.getinsured.hix.platform.util.GhixUtils.EMAIL_STATS;



public interface IEmailStatService {

	void saveEmailStat(Map<EMAIL_STATS, String> emailStatData, int noticeId);
	
	
}
