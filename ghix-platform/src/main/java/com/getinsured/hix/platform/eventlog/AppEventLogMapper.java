package com.getinsured.hix.platform.eventlog;

import com.getinsured.hix.model.AppEvent;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class AppEventLogMapper {

	
	/**
	 * Copies the dto data to the entity
	 * @param dto
	 * @return
	 */
	public AppEvent dtoToEntity(EventDto dto) {
		AppEvent appEvent = null;

		if(dto != null) {
			appEvent = new AppEvent();
			
			EventInfoDto info = dto.getEventInfoDto();
			appEvent.setName(info.getEventName());
			appEvent.setType(info.getEventType());
			
			appEvent.setModuleId(dto.getModuleId());
			appEvent.setModuleName(dto.getModuleName());
			
			// User info
			EventUserInfo userInfo = dto.getLoggedInUserInfo();
			appEvent.setCreatedBy(userInfo.getUserId());
			appEvent.setCreatedByUserRole(userInfo.getUserRole());
			appEvent.setCreatedByUserName(userInfo.getUserName());
			appEvent.setCreationTimeStamp(dto.getEventCreationTimeStamp());
			
			Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
			dto.getEventInfoDto().setComments(""); // HIX-100746 Removed comments from json string
			String eventJson = gson.toJson(dto);
			appEvent.setEventJson(eventJson);
		}
		
		return appEvent;
	}
}
