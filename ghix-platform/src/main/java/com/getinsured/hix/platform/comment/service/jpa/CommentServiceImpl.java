/**
 * 
 */
package com.getinsured.hix.platform.comment.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Comment;
import com.getinsured.hix.platform.comment.repository.ICommentRepository;
import com.getinsured.hix.platform.comment.service.CommentService;

/**
 * @author panda_p
 * 
 */
@Service("commentService")
@Repository
public class CommentServiceImpl implements CommentService {

	@Autowired
	private ICommentRepository commentRepository;

	/*åå
	 * (non-Javadoc)
	 * åå
	 * @see
	 * com.getinsured.hix.platform.comment.service.CommentService#saveComment
	 * (com.getinsured.hix.platform.comment.model.Comment)
	 */
	@Override
	@Transactional
	public Comment saveComment(Comment comment) {
		// TODO Auto-generated method stub
		return commentRepository.save(comment);
	}

	@Override
	public List<Comment> findByCommentTargetId(Integer commentTargetId) {
		// TODO Auto-generated method stub
		return commentRepository.findByCommentTargetId(commentTargetId);
	}

	@Override
	public Comment findCommentById(Integer id) {
		return commentRepository.findById(id);
	}

	@Transactional
	@Override
	public void deleteComment(Integer commentId) {
		commentRepository.deleteCommentTextById(commentId);
	}

	@Transactional
	public String getCommentTextById(Integer commentId) {
		return commentRepository.getCommentTextById(commentId);
	}
}
