package com.getinsured.hix.platform.ecm.couchbase;

import java.nio.charset.StandardCharsets;

import com.getinsured.timeshift.util.TSDate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.dto.platform.ecm.CMISErrors;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.dto.platform.ecm.MetadataElement;
import com.getinsured.hix.platform.couchbase.CouchBaseUtil;
import com.getinsured.hix.platform.couchbase.dto.CouchBinary;
import com.getinsured.hix.platform.couchbase.service.CouchBucketService;
import com.getinsured.hix.platform.ecm.CMISSessionUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.MediaTypeNotSupported;
import com.getinsured.hix.platform.ecm.MimeConfig;
import com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument;

@Service("couchEcmService")
public class CouchEcmService implements ContentManagementService {

	@Autowired
	private CouchBucketService couchBucketService;

	@SuppressWarnings("unused")
	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes) throws ContentManagementServiceException {
		// 1. validate incoming content
		final String mimeType = validateIncomingContent(fileName, dataBytes);

		final String type = CMISSessionUtil.getType(fileName);

		CouchEcmDocument document = formDocument(relativePath, fileName, dataBytes, mimeType, null, null, null, type);

		final String documentId = couchBucketService.createDocument(document);

		final String[] documentContentIds = couchBucketService.createBinaryDocument(dataBytes,
				document.getBinaryMetaData().getContentLink());

		return documentId;

	}

	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, boolean skipAntiVirusCheck) throws ContentManagementServiceException {

		return uploadDocument(relativePath, fileName, dataBytes, category, subCategory, type, skipAntiVirusCheck);
	}

	private String uploadDocument(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, boolean skipAntiVirusCheck) throws ContentManagementServiceException {
		
		String mimeType=null;
		try {
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if(sra == null) {
				mimeType = validateIncomingContent(fileName, dataBytes);
			}else {
				mimeType = MimeConfig.getMimeConfig().validateMimeTypeFromSession(dataBytes,fileName) ;
			}
		} catch (MediaTypeNotSupported e) {
			throw new ContentManagementServiceException(e.getMessage(),e);
		}
		
		//String mimeType = validateIncomingContent(fileName, dataBytes);
		validateCategories(category, subCategory);

		if (StringUtils.isEmpty(type)) {
			type = CMISSessionUtil.getType(fileName);
		}

		CouchEcmDocument document = formDocument(relativePath, fileName, dataBytes, mimeType, null, category,
				subCategory, type);

		final String documentId = couchBucketService.createDocument(document);

		final String[] documentContentIds = couchBucketService.createBinaryDocument(dataBytes,
				document.getBinaryMetaData().getContentLink());

		return documentId;
	}

	@SuppressWarnings("unused")
	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type) throws ContentManagementServiceException {

		return uploadDocument(relativePath, fileName, dataBytes, category, subCategory, type, false);

	}

	private void validateCategories(String category, String subCategory) {

		if (StringUtils.isEmpty(category)) {
			throw new IllegalArgumentException(CMISErrors.CATEGORY_CANNOT_BE_NULL);
		}

		if (StringUtils.isEmpty(subCategory)) {
			throw new IllegalArgumentException(CMISErrors.SUB_CATEGORY_CANNOT_BE_NULL);
		}

	}

	private void validateType(String type) {
		if (StringUtils.isEmpty(type)) {
			throw new IllegalArgumentException(CMISErrors.TYPE_CANNOT_BE_NULL);
		}
	}

	@Override
	public String createContent(String relativePath, String fileName, String content, String category,
			String subCategory, String type) throws ContentManagementServiceException {

		validateIncomingContent(fileName, content);
		validateCategories(category, subCategory);
		validateType(type);

		CouchEcmDocument document = formDocument(relativePath, fileName, null, "text/plain", content, category,
				subCategory, type);

		return couchBucketService.createDocument(document);

	}

	private void validateIncomingContent(String fileName, String dataString) {
		if (StringUtils.isEmpty(fileName)) {
			throw new IllegalArgumentException(CMISErrors.FILE_NAME_CANNOT_BE_NULL);
		}
	}

	private CouchEcmDocument formDocument(String relativePath, String fileName, byte[] dataBytes, String mimeType,
			String content, String category, String subCategory, String type) {
		CouchEcmDocument document = new CouchEcmDocument();
		document.setRelativePath(relativePath);
		document.setContent(content);
		document.getMetaData().setCategory(category);
		document.getMetaData().setSubCategory(subCategory);
		document.getMetaData().setCreationDate(new TSDate());
		document.getMetaData().setModifiedDate(new TSDate());
		document.getMetaData().setCreatedBy(CMISSessionUtil.getEcmUsername());
		document.getMetaData().setModifiedBy(CMISSessionUtil.getEcmUsername());

		if (dataBytes != null) {
			CouchBinary couchBinary = new CouchBinary();
			couchBinary.setFileName(fileName);
			couchBinary.setFileSize(dataBytes.length);
			couchBinary.setMimeType(mimeType + "; charset=UTF-8");
			couchBinary.setHasContent(true);
			couchBinary.setDocType(type);
			couchBinary.setNumberOfParts(CouchBaseUtil.numberOfParts(couchBinary.getFileSize()));
			document.setBinaryMetaData(couchBinary);
		}
		return document;
	}

	private String validateIncomingContent(String fileName, byte[] dataBytes) {
		if (StringUtils.isEmpty(fileName)) {
			throw new IllegalArgumentException(CMISErrors.FILE_NAME_CANNOT_BE_NULL);
		}

		String mimeType = CMISSessionUtil.getMime(fileName);
		if (StringUtils.isEmpty(mimeType)) {
			throw new IllegalArgumentException(CMISErrors.UNSUPPORTED_DOCUMENT_TYPE);
		}

		if (dataBytes == null || dataBytes.length <= 0) {
			throw new IllegalArgumentException(CMISErrors.DATA_BYTES_CANNOT_BE_NULL);
		}
		return mimeType;
	}

	@Override
	public byte[] getContentDataById(String contentId) throws ContentManagementServiceException {
		byte[] document = couchBucketService.getBinaryDocumentById(contentId);
		return document;
	}

	@Override
	public Content getContentById(String contentId) throws ContentManagementServiceException {

		CouchEcmDocument document = couchBucketService.getDocumentById(contentId, CouchEcmDocument.class);
		return formContentObject(document);

	}

	@Override
	public String deleteContent(String contentId) throws ContentManagementServiceException {
		CouchEcmDocument document = couchBucketService.getDocumentById(contentId, CouchEcmDocument.class);
		if (null == document) {
			throw new ContentManagementServiceException(CMISErrors.COUCH_DOCUMENT_NOT_FOUND);
		}
		couchBucketService.deleteDocument(document.getId());
		if (document.getBinaryMetaData() != null && document.getBinaryMetaData().isHasContent()) {
			couchBucketService.deleteBinaryDocument(document);
		}
		return contentId;
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent)
			throws ContentManagementServiceException {

		updateContentAndMimeType(contentId, mimeType, newContent);
		return contentId;
	}

	private void updateContentAndMimeType(String contentId, String mimeType, byte[] newContent)
			throws ContentManagementServiceException {
		if (newContent == null || newContent.length <= 0) {
			throw new IllegalArgumentException(CMISErrors.DATA_BYTES_CANNOT_BE_NULL);
		}
		CouchEcmDocument document = couchBucketService.getDocumentById(contentId, CouchEcmDocument.class);
		if (null == document) {
			throw new ContentManagementServiceException(CMISErrors.COUCH_DOCUMENT_NOT_FOUND);
		}

		document.getMetaData().setModifiedDate(new TSDate());
		if (document.getBinaryMetaData() == null) {
			document.setBinaryMetaData(new CouchBinary());
			document.getBinaryMetaData().setHasContent(true);
		}
		document.getBinaryMetaData().setMimeType(mimeType);
		couchBucketService.updateBinaryDocument(document , newContent);
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, String checkinNotes, String user)
			throws ContentManagementServiceException {

		updateContentAndMimeType(contentId, mimeType, newContent);
		return contentId;
	}

	@SuppressWarnings("unused")
	private void updateMetaData(MetadataElement metadataElement) throws ContentManagementServiceException {
		if (StringUtils.isEmpty(metadataElement.getContentId())) {
			throw new IllegalArgumentException(CMISErrors.FILE_NAME_CANNOT_BE_NULL);
		}
		CouchEcmDocument document = couchBucketService.getDocumentById(metadataElement.getContentId(),
				CouchEcmDocument.class);
		if (null == document) {
			throw new ContentManagementServiceException(CMISErrors.COUCH_DOCUMENT_NOT_FOUND);
		}
		if (document.getBinaryMetaData() != null) {
			document.getBinaryMetaData().setMimeType(metadataElement.getMimeType());
		}
		document.setDescription(metadataElement.getDescription());
		document.getMetaData().setModifiedDate(new TSDate());
		final String documentId = couchBucketService.updateDocument(document);
	}

	@Override
	public String updateContentMetadata(MetadataElement metadataElement) throws ContentManagementServiceException {
		updateMetaData(metadataElement);
		return metadataElement.getContentId();
	}

	@Override
	public String updateContentMetadata(MetadataElement metadataElement, List<String> tags)
			throws ContentManagementServiceException {
		updateMetaData(metadataElement);
		return metadataElement.getContentId();
	}

	@Override
	public Content getContentByPath(String queryStr) throws ContentManagementServiceException {

		CouchEcmDocument document = getCouchDocument(queryStr);
		return formContentObject(document);
	}

	private Content formContentObject(CouchEcmDocument document) {
		Content content1 = new Content();
		content1.setContentSource(Content.COUCH);
		if (document != null) {
			content1.setContentId(document.getId());
			content1.setCreationDate(document.getMetaData().getCreationDate());
			content1.setDescription(document.getDescription());
			content1.setModifiedDate(document.getMetaData().getModifiedDate());
			content1.setRelativePath(document.getRelativePath());
			if (document.getBinaryMetaData() != null) {
				content1.setMimeType(document.getBinaryMetaData().getMimeType());
				content1.setOriginalFileName(document.getBinaryMetaData().getFileName());
				content1.setTitle(document.getBinaryMetaData().getFileName());

				Map<String, String> customAttributeList = new HashMap<String, String>(1);
				customAttributeList.put("contentSize", document.getBinaryMetaData().getFileSize() != 0
						? document.getBinaryMetaData().getFileSize() + "" : StringUtils.EMPTY);
				content1.setCustomAttributeList(customAttributeList);
			}

			content1.setContent(document.getContent());
		}
		return content1;
	}

	@Override
	public byte[] getContentDataByPath(String queryStr) throws ContentManagementServiceException {
		CouchEcmDocument document = getCouchDocument(queryStr);

		String docId = null;
		if (document.getBinaryMetaData() != null){
			if (document.getBinaryMetaData().isHasContent()){
				docId = document.getId();
				return getContentDataById(docId);
			}
		}

		String contentStr = document.getContent();

		if (StringUtils.isBlank(contentStr) && StringUtils.isBlank(docId)){
			throw new ContentManagementServiceException("No byte or content information found!");
		}

		return contentStr.getBytes(StandardCharsets.UTF_8);


	}

	private CouchEcmDocument getCouchDocument(String queryStr) throws ContentManagementServiceException {
		List<CouchEcmDocument> result = couchBucketService.query(queryStr, CouchEcmDocument.class);

		if (result == null || result.size() == 0){
			throw new ContentManagementServiceException("No object found!");
		}

		if (result.size() > 1) {
			throw new ContentManagementServiceException("More than 1 object found for given query!");
		}

		return result.get(0);
	}

	@Override
	public Content getContentByPath(String queryStr, boolean latestVer) throws ContentManagementServiceException {
		return getContentByPath(queryStr);
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, boolean skipAntiVirusCheck)
			throws ContentManagementServiceException {
		return updateContent(contentId, mimeType, newContent);
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, String checkinNotes, String user,
			boolean skipAntiVirusCheck) throws ContentManagementServiceException {
		return updateContent(contentId, mimeType, newContent, checkinNotes, user);
	}

}
