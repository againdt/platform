package com.getinsured.hix.platform.security.service.impl;

import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.security.service.GiUserAudit;

@Service("userAudit")
public class GiUserAuditImpl implements GiUserAudit {
	
	@GiAudit(transactionName = "User Accounts", eventType = EventTypeEnum.USER_ACCOUNTS, eventName = EventNameEnum.DEACTIVATE_ACCOUNTS)
	public void createDeActivateAudit(GiAuditParameter...auditParameters ){
		GiAuditParameterUtil.add(auditParameters);
	}
		
}
