package com.getinsured.hix.platform.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface TenantAwareRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

}
