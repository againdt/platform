package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.UserRole;

public interface IUserRoleRepository extends JpaRepository<UserRole, Integer>{
	 List<UserRole> findByUser(AccountUser user);
	
	@Query("select ur from UserRole ur where ur.user.id=:userId")
	List<UserRole> findAllUserRolesByUserId(@Param("userId")Integer userId);
	
	@Query("select ur from UserRole ur where ur.user.id=:userId")
	 UserRole findByUserId(@Param("userId") Integer userId);
	
	@Query("select ur from UserRole ur where ur.role.id=:roleId")
	List<UserRole> findByRoleId(@Param("roleId")String roleId);
	
	@Query("select distinct(r.name) from UserRole ur,Role r,AccountUser au where au.id=ur.user.id and ur.role.id=r.id")
	List<String> findAllUsersRoleNames();
	
	
	@Query("select distinct(r.name) from UserRole ur,Role r where ur.user.id=:userId and ur.role.id=r.id")
	List<String> findUserRoleNames(@Param("userId") Integer userId);
	
}