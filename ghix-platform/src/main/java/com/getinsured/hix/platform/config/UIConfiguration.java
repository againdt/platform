package com.getinsured.hix.platform.config;

public class UIConfiguration {

	
	public enum UIConfigurationEnum implements PropertiesEnumMarker
	{  
		BOOTSTRAP_LESS_FILE ("ui.BootstrapLessFile"),
		ACCENT_LESS_FILE ("ui.AccentLessFile"),
		BRANDFILE ("ui.BrandFile"),
		BRANDNAME ("ui.BrandName"),
		BRANDNAME_ABBR ("ui.BrandNameAbbr"),
		THEMEJS_FILE ("ui.ThemeJSFile"),
		THEMEQUICKFIXES_FILE ("ui.ThemeQuickfixesFile"),
		THEMEIE8_FILE ("ui.ThemeIE8File"),
		SUPPORTED_CHROME_VERSION("ui.Browser.Chrome.Version"),
		SUPPORTED_IE_VERSION("ui.Browser.InternetExplorer.Version"),
		SUPPORTED_FIREFOX_VERSION("ui.Browser.Firefox.Version"),
		BRANDFAVICON("ui.faviconIcon"),
		SSAP_FINANCIAL_ENABLED("ui.ssap.financialflow");

		private final String value;	  
		@Override
		public String getValue(){return this.value;}
		UIConfigurationEnum(String value){
	        this.value = value;
	    }
	};
}
