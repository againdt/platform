package com.getinsured.hix.platform.couchbase.helper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@Qualifier("binaryCouchSupport")
@DependsOn("couchDocumentHelper")
public class BinaryCouchSupport extends BaseCouchSupportClass {

}
