package com.getinsured.hix.platform.ecm;

public final class ECMConstants {

	private ECMConstants() {};
	
	public static final class Issuer{
		public static final String DOC_CATEGORY = "IS"; 
		public static final String DOC_SUB_CATEGORY_SUPPORT = "SUPP";
		public static final String DOC_SUB_CATEGORY_QRATING = "QRATING";
	}
	
	public static final class PlanMgmt{
		public static final String DOC_CATEGORY = "PM"; 
		public static final String DOC_SUB_CATEGORY = "PLAN";
	}
	
	public static final class Serff{
		public static final String DOC_CATEGORY = "SF"; 
		public static final String DOC_SUB_CATEGORY = "PLAN";
	}
	
	public static final class Provider {
		public static final String DOC_CATEGORY = "PD";
		public static final String DOC_SUB_CATEGORY = "PROVIDER";
	}
	
	public static final class DirectConsumer{
		public static final String DOC_CATEGORY = "DC"; 
		public static final String APPLICATION_CONFIGURATION = "CONFIG"; // eApp application configurations
		public static final String CARRIER_CONFIGURATION = "CRRAPP"; // Carrier configurations
		public static final String CONSUMER_DATA = "CONDAT"; 
	}
	
	public static final class TicketMgmt{
		public static final String DOC_CATEGORY = "TK"; 
		public static final String ATTACHMENT = "ATTACH"; 
		public static final String TEMPLATE = "TMPL"; 
		public static final String DOCUMENT_VERFICATION = "DOCVER"; 
	}
	
	public static final class CAP{
		public static final String DOC_CATEGORY = "CP"; 
		public static final String APPLICATION_DOCUMENT = "APPDOC"; 
		public static final String SEARCH_CONSUMER_SUB_CATEGORY= "SERCON";
	}
	
	public static final class Platform{
		public static final String DOC_CATEGORY = "PT"; 
		public static final String NOTICE = "NOTICE"; 
		public static final String ATTACHMENT = "ATTACH"; 
	}
	
	public static final class Eligibility{
		public static final String DOC_CATEGORY = "EL"; 
		public static final String RIDP = "RIDP"; 
		public static final String SSAP_VERIFICATION = "VERIFI"; 
	}
	
	
	public static final class Broker{
		public static final String DOC_CATEGORY = "BR"; 
		public static final String PHOTO = "PHOTO"; 
		public static final String SSAP_VERIFICATION = "VERIFI"; 
		public static final String BROKER_DOCUMENTS = "BRDOC";
	}
	
	public static final class Entity{
		public static final String DOC_CATEGORY = "EN"; 
		public static final String PHOTO = "PHOTO"; 
		public static final String ASSISTER_DOC = "ASSIST"; 
		public static final String ENTITY_DOC = "ENTITY"; 
	}
	
	public static final class Finance{
		public static final String DOC_CATEGORY = "FI";
		public static final String SINGLE_TXN_REPORT = "TXNRPT";
		public static final String PAYMENT_EVT_REPORT = "EVTRPT";
		public static final String EMPLOYER_INVOICES = "EMPINV";
	}
	
	public static final class Shop{
		public static final String EMPLOYER_DOC_CATEGORY = "EM";
		public static final String EMPLOYEE_DOC_CATEGORY = "EE";
		public static final String FRT_DOCUMENT = "FRTDOC";
		public static final String SUPPORT_DOCUMENT = "DOCS";
		public static final String APPEAL_DOCUMENT = "APPEAL";
	}
	
	public static final class ADMIN{
		public static final String DOC_CATEGORY = "AD"; 
		public static final String ADMIN_DOCUMENT = "ADMDOC"; 
	}
	
	public static final class ENROLLMENT{
		public static final String DOC_CATEGORY = "ENRL"; 
		public static final String ENROLLMENT_DOCUMENT = "DOCS";
		public static final String ENROLLMENT_1095_PDF = "ENR1095";
		public static final String ENROLLMENT_WORKBENCH = "ENRLWORKBENCH";
	}
	
	public static final class FFM{
		public static final String DOC_CATEGORY = "AUTOFILLPROXY";
		public static final String DOC_SUB_CATEGORY = "MAPFILES";
	}
	
	public static final class Agency {
		public static final String DOC_CATEGORY = "AGENCY"; 
		public static final String AGENCY_DOCUMENT = "DOCS";
	}
}
