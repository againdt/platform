package com.getinsured.hix.platform.logging;

import org.apache.logging.log4j.Logger;

public class Log4j2LoggerAdaptor implements GhixLogger {

	final transient Logger logger;
	// WARN: Log4j2LoggerAdaptor constructor should have only package access so
	// that
	// only Log4j2LoggerFactory be able to create one.
	Log4j2LoggerAdaptor(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void trace(String message) {
		logger.trace(message);
	}

	@Override
	public void trace(String message, Object arg) {
		logger.trace(message, arg);
	}

	@Override
	public void trace(String message, Object arg1, Object arg2) {
		logger.trace(message, arg1, arg2);
	}

	@Override
	public void trace(String message, Object... arguments) {
		logger.trace(message, arguments);
	}

	@Override
	public void trace(String message, Throwable t) {
		logger.trace(message, t);
	}

	@Override
	public void debug(String message) {
		logger.debug(message);
	}

	@Override
	public void debug(String message, Object arg) {
		logger.debug(message, arg);
	}

	@Override
	public void debug(String message, Object arg1, Object arg2) {
		logger.debug(message, arg1, arg2);
	}

	@Override
	public void debug(String message, Object... arguments) {
		logger.debug(message, arguments);
	}

	@Override
	public void debug(String message, Throwable t) {
		logger.debug(message, t);
	}

	@Override
	public void info(String message) {
		logger.info(message);
	}

	@Override
	public void info(String message, Object arg) {
		logger.info(message,arg);
	}

	@Override
	public void info(String message, Object arg1, Object arg2) {
		logger.info(message,arg1,arg2);
	}

	@Override
	public void info(String message, Object... arguments) {
		logger.info(message,arguments);
	}

	@Override
	public void info(String message, Throwable t) {
		logger.info(message,t);
	}

	@Override
	public void warn(String message) {
		logger.warn(message);
	}

	@Override
	public void warn(String message, Object arg) {
		logger.warn(message,arg);
	}

	@Override
	public void warn(String message, Object... arguments) {
		logger.warn(message,arguments);
	}

	@Override
	public void warn(String message, Object arg1, Object arg2) {
		logger.warn(message,arg1,arg2);
	}

	@Override
	public void warn(String message, Throwable t) {
		logger.warn(message,t);
	}

	@Override
	public void error(String message) {
		logger.error(message);
	}

	@Override
	public void error(String message, Object arg) {
		logger.error(message,arg);
	}

	@Override
	public void error(String message, Object arg1, Object arg2) {
		logger.error(message,arg1,arg2);
	}

	@Override
	public void error(String message, Object... arguments) {
		logger.error(message,arguments);
	}

	@Override
	public void error(String message, Throwable t) {
		logger.error(message,t);
	}
	
	@Override
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}
	
	@Override
	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}
	
	@Override
	public void info(Object message) {
		 logger.info(message);
	}
	
	@Override
	public void info(Object message, Throwable t) {
		logger.info(message,t);
	}
	
	@Override
	public void fatal(String message) {
		logger.fatal(message);
		
	}
	

}
