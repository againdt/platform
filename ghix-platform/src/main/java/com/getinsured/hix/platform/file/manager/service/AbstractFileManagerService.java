package com.getinsured.hix.platform.file.manager.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.file.manager.FileManagerService;

public abstract class AbstractFileManagerService implements FileManagerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileManagerService.class);
	
	public String getBasePath(){
		String basePath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRINTMAILPATH);
		if(StringUtils.isEmpty(basePath)){
			LOGGER.error("Base path canot be null. Please configure global varriable 'global.PrintMailPath'. ");
		}
		return basePath;
	}

}
