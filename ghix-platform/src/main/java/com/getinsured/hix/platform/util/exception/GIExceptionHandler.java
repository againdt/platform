package com.getinsured.hix.platform.util.exception;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Component(value="giExceptionHandler")
public class GIExceptionHandler {

	private static final Logger LOGGER = Logger.getLogger(GIExceptionHandler.class);
	
	@Autowired 
	private GIMonitorService giMonitorService;
	@Autowired 
	private UserService userService;
	
	private static final String CRITICAL_EXCEPTIONS_LOGGING_ENABLED = "global.critical.exceptions.logging.enabled";
	private static final String ON_FLAG = "ON";
	
	public GIMonitor recordFatalException(com.getinsured.hix.platform.util.exception.GIRuntimeException.Component component,String errorMessage){
		return recordException(new GIFatalRuntimeException(component,null,errorMessage));
	}

	public GIMonitor recordFatalException(com.getinsured.hix.platform.util.exception.GIRuntimeException.Component component, String errorMessage, Exception e) {
		return recordException(new GIFatalRuntimeException(component,null,errorMessage,e));
	}
	
	public GIMonitor recordFatalException(com.getinsured.hix.platform.util.exception.GIRuntimeException.Component component, String ghixErrorCode, String errorMessage, Exception e) {
		return recordException(new GIFatalRuntimeException(component,ghixErrorCode,errorMessage,e));
	}

	public GIMonitor recordCriticalException(com.getinsured.hix.platform.util.exception.GIRuntimeException.Component component,String ghixErrorCode,String errorMessage){
		return recordException(new GIRuntimeException(ghixErrorCode, null, errorMessage, Severity.HIGH, component != null ? component.getComponent() : null, null));
	}

	public GIMonitor recordCriticalException(com.getinsured.hix.platform.util.exception.GIRuntimeException.Component component,String ghixErrorCode,String errorMessage, Exception e){
		return recordException(new GIRuntimeException(ghixErrorCode, e, errorMessage, Severity.HIGH, component != null ? component.getComponent() : null, null));
	}
	
	public GIMonitor recordException(Exception e){
		return resolveAndHandleException(e);
	}

	private GIMonitor resolveAndHandleException(Exception exception){
		GIMonitor giMonitor = null;
		if(exception instanceof GIRuntimeException) {
			boolean logExceptionInGIMonitor = false;
			GIRuntimeException giRuntimeException = (GIRuntimeException) exception;
			String component = StringUtils.isEmpty(giRuntimeException.getComponent()) ? com.getinsured.hix.platform.util.exception.GIRuntimeException.Component.UNKNOWN.getComponent() :giRuntimeException.getComponent() ; 
			if(Severity.FATAL.getSeverity().equals(giRuntimeException.getErrorSeverity())){
				LOGGER.fatal("Fatal Exception: ["+component+"] ",giRuntimeException);
				logExceptionInGIMonitor = true;
			}else{
				String logExceptionInGIMonitorProperty = DynamicPropertiesUtil.getPropertyValue(CRITICAL_EXCEPTIONS_LOGGING_ENABLED);
				if(ON_FLAG.equalsIgnoreCase(logExceptionInGIMonitorProperty)){
					logExceptionInGIMonitor = true;
				}else{
					logExceptionInGIMonitor = false;
				}
				LOGGER.error("GIRuntimeException:  ["+component+"] ", giRuntimeException);
			}
			if(logExceptionInGIMonitor){
				giMonitor = giMonitorService.saveOrUpdateErrorLog(
											StringUtils.isEmpty(giRuntimeException.getErrorCode()) ? GIRuntimeException.ERROR_CODE_UNKNOWN : giRuntimeException.getErrorCode(),
											giRuntimeException.getEventTime() == null ? new TSDate() : giRuntimeException.getEventTime(),
											giRuntimeException.getClass().getName(),
											ExceptionUtils.getFullStackTrace(giRuntimeException) + "\n\nCaused by: " + giRuntimeException.getNestedStackTrace(),
											getUser(), getRequestURL(), giRuntimeException.getComponent(),
											giRuntimeException.getGiMonitorId());
			}
		} else {
			LOGGER.error("Run time exception: ", exception);
			giMonitor = giMonitorService.saveOrUpdateErrorLog(
					GIRuntimeException.ERROR_CODE_UNKNOWN, new TSDate(),
					exception.getClass().getName(),
					ExceptionUtils.getFullStackTrace(exception), getUser(),
					getRequestURL(), null, null);
		}
		return giMonitor;
	}


	private AccountUser getUser(){
		AccountUser user  = null;

		try {
			user = userService.getLoggedInUser();
		} catch(Exception exception){
			LOGGER.warn("No logged in user found.");
		}

		return user;
	}

	private String getRequestURL() {
		//TODO: Find out if there is way to get the request url from thread local context provided by spring
		return TenantContextHolder.getRequesturl();
	}
}
