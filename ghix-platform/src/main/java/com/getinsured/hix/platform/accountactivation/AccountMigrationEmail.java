package com.getinsured.hix.platform.accountactivation;

import com.getinsured.hix.platform.util.DateUtil;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component("AccountMigrationEmail")
public class AccountMigrationEmail extends ReferralAccountActivationEmail {
    private static final String autoRenewalValidityEnd = "10/11/2019";

    @Override
    public Map<String, String> getTokens(Map<String, Object> notificationContext) {
        Map<String, String> tokens = super.getTokens(notificationContext);

        Date currentDate = new Date();
        Date autoRenewalValidityEndDate = DateUtil.StringToDate(autoRenewalValidityEnd,"MM/dd/yyyy");
        tokens.put("hideAutoRenewal", "Y");
        if(currentDate.compareTo(autoRenewalValidityEndDate) <= 0) {
            tokens.put("hideAutoRenewal", "N");
        }

        return tokens;
    }
}
