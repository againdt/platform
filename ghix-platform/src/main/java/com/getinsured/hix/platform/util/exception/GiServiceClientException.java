package com.getinsured.hix.platform.util.exception;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.getinsured.hix.platform.util.DateRange;

public class GiServiceClientException implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 688441396654608166L;
	private String errCodeStr = null;
	private List<DateRange> serviceDownTimeSchedule;
	private LinkedList<GiServiceClientException> stack = null;
	private String message;

	public GiServiceClientException(String message) {
		this.message = message;
	}

	public GiServiceClientException() {
		// TODO Auto-generated constructor stub
	}

	public String getErrCodeStr() {
		return errCodeStr;
	}

	public void setErrCodeStr(String errCodeStr) {
		this.errCodeStr = errCodeStr;
	}


	public List<DateRange> getServiceDownTimeSchedule() {
		return serviceDownTimeSchedule;
	}

	public void setServiceDownTimeSchedule(List<DateRange> serviceDownTimeSchedule) {
		this.serviceDownTimeSchedule = serviceDownTimeSchedule;
	}

	public static GiServiceClientException fromJsonResponse(String response,GiServiceClientException parent) throws GIException, java.text.ParseException{
		Object tmp = null;
		JSONParser parser  = new JSONParser();
		JSONObject obj = null;
		String msg = null;
		GiServiceClientException wsClientExp ;
		GiServiceClientException childWsClientExp ;

		try {
			obj = (JSONObject) parser.parse(response);
		} catch (ParseException e) {
			return new GiServiceClientException(response);
		}
		tmp = obj.get("EXCEPTION_MESSAGE");
		if(tmp != null){
			msg = (String)tmp;
			wsClientExp = new GiServiceClientException(msg);
		}else{
			wsClientExp = new GiServiceClientException();
		}

		tmp = obj.get("ERROR_CODE");
		if(tmp != null){
			String err = (String) tmp;
			wsClientExp.setErrCodeStr(err);
		}
		tmp = obj.get("SERVICE_UNDER_MAINT");
		if(tmp != null){
			JSONObject schObj = null;
			ArrayList<DateRange> scheduleList = new ArrayList<DateRange>();
			JSONArray scheduleArray = (JSONArray)tmp;
			int len = scheduleArray.size();
			DateRange tmpDateRange = null;
			for(int i = 0; i < len; i++){
				schObj = (JSONObject) scheduleArray.get(i);
				tmpDateRange = new DateRange();
				tmpDateRange.setStartDate(DateRange.getDate((String)schObj.get("MAINT_START_TIME")));
				tmpDateRange.setEndDate(DateRange.getDate((String)schObj.get("MAINT_END_TIME")));
				scheduleList.add(tmpDateRange);
			}
			wsClientExp.setServiceDownTimeSchedule(scheduleList);
		}
		tmp = obj.get("EXCEPTION_CAUSE");
		if(tmp != null){
			childWsClientExp = fromJsonResponse(((JSONObject)tmp).toJSONString(),wsClientExp);
			if(childWsClientExp != null){
				wsClientExp.addCausedBy(childWsClientExp);
			}
		}
		return wsClientExp;
	}

	public LinkedList<GiServiceClientException> getStack() {
		return stack;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private void addCausedBy(GiServiceClientException childWsClientExp) {
		if(this.stack == null){
			this.stack = new LinkedList<GiServiceClientException>();
		}
		this.stack.add(childWsClientExp);
	}

	public String toString(){
		return this.message;
	}

	public void printStackTrace(){
		String tmp = null;
		if(this.stack != null && this.stack.size() > 0){
			for(GiServiceClientException we: this.stack){
				tmp = we.getMessage();
				if(tmp == null || tmp.length() == 0){
					continue;
				}
				System.err.println(tmp);
			}
		}
	}

	public void printStackTrace(OutputStream out){
		if(out == null){
			return;
		}
		String tmp;
		try{
		if(this.stack != null && this.stack.size() > 0){
			for(GiServiceClientException we: this.stack){
				tmp = we.getMessage();
				if(tmp == null || tmp.length() == 0){
					continue;
				}
				out.write(we.getMessage().getBytes());

			}
			out.flush();
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
