package com.getinsured.hix.platform.emailstat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.getinsured.hix.platform.emailstat.model.EmailStats;

public interface IEmailStatRepository extends JpaRepository<EmailStats, Integer>{
	
}
