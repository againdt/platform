package com.getinsured.hix.platform.feature;

import org.aspectj.lang.ProceedingJoinPoint;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;


public class GiFeatureAdvice {
	
	public Object checkFeature(ProceedingJoinPoint point, GiFeature giFeature) throws Throwable {

		String isFeatureEnabled = DynamicPropertiesUtil.getPropertyValue(giFeature.value());
		if(isFeatureEnabled != null && isFeatureEnabled.equalsIgnoreCase("Y")) {
			return point.proceed();
		}

		return null;
	}

}
