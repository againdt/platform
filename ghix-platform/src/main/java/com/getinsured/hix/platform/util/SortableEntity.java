package com.getinsured.hix.platform.util;

import java.util.Set;

public interface SortableEntity {
	public Set<String> getSortableColumnNamesSet();
}
