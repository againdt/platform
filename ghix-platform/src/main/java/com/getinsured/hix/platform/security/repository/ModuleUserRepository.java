package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ModuleUser;

@Repository
@Transactional(readOnly = true)
public class ModuleUserRepository {
	@Autowired
	private IModuleUserRepository moduleUserRepository;
	
	public ModuleUser findById(Integer id) {
		if( moduleUserRepository.exists(id) ){
			return moduleUserRepository.findById(id);
		}
		return null;
    }

	public List<ModuleUser> findAll() {
       return moduleUserRepository.findAll();
    }

    @Transactional
    public ModuleUser save(ModuleUser moduleUser) {
            return moduleUserRepository.save(moduleUser);
    }
    
    @Transactional
    public void delete(ModuleUser moduleUser) {
    	 moduleUserRepository.delete(moduleUser);
    }
}
