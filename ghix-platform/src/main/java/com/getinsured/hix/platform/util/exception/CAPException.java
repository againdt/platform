package com.getinsured.hix.platform.util.exception;


public class CAPException extends GIRuntimeException {
	
	private static final long serialVersionUID = 1L;

	
	public enum TeamError {
		
		MANAGE_TEAM("CAP-10000"),
		ADD_ERROR("CAP-10001"),
		FIND_BY_ID("CAP-10002"),
		ACCOUNT_USER_LIST_BY_GROUP_ID("CAP-10003"),
		UPDATE_GROUP_MEMBERS("CAP-10004"),
		EDIT_ERROR("CAP-10005");
		
		private final String id;
		
        TeamError(String id) {
			this.id = id;
			
		}

		public String getId() {
			return id;
		}	
	}
	public CAPException(TeamError teamErrors, Throwable aThrowable, String nestedStackTrace, Severity severity) 
	{
		super(teamErrors.id,aThrowable,nestedStackTrace,severity);
	}
	
	
	
}
