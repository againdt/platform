package com.getinsured.hix.platform.db;

import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.hibernate.jpa.HibernateEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.ConnectionHandle;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.jpa.DefaultJpaDialect;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.util.ReflectionUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class GHIXJpaDialect extends DefaultJpaDialect {

	private static final long serialVersionUID = -1763031099547074204L;
	protected boolean exceptionIfHeaderMissing = false;
	private static final Logger log = LoggerFactory.getLogger(RequestMapperInterceptor.class);
	
	@Override
	public Object beginTransaction(EntityManager entityManager, TransactionDefinition definition)
			throws PersistenceException, SQLException, TransactionException {

		if (definition.getTimeout() != TransactionDefinition.TIMEOUT_DEFAULT) {
			getSession(entityManager).getTransaction().setTimeout(definition.getTimeout());
		}
		super.beginTransaction(entityManager, definition);
		return prepareTransaction(entityManager, definition.isReadOnly(), definition.getName());
	}

	@Override
	public Object prepareTransaction(EntityManager entityManager, boolean readOnly, String name)
			throws PersistenceException {

		Session session = getSession(entityManager);
		
		FlushMode flushMode = session.getFlushMode();
		FlushMode previousFlushMode = null;
		if (readOnly) {
			// We should suppress flushing for a read-only transaction.
			session.setFlushMode(FlushMode.MANUAL);
			previousFlushMode = flushMode;
		}
		else {
			// We need AUTO or COMMIT for a non-read-only transaction.
			if (flushMode.lessThan(FlushMode.COMMIT)) {
				session.setFlushMode(FlushMode.AUTO);
				previousFlushMode = flushMode;
			}
		}
		final SessionTransactionData data = new SessionTransactionData(session, previousFlushMode);
		session.doWork(new Work() 
			{
				@Override
				//@SuppressWarnings("deprecation")
				public void execute(final Connection connection) throws SQLException
				{
					if(GhixPlatformConstants.DATABASE_TYPE == null || !"oracle".equalsIgnoreCase(GhixPlatformConstants.DATABASE_TYPE)){
						if(log.isDebugEnabled()){
							log.debug("GI JPA Dialect: GhixPlatformConstants.DATABASE_TYPE:" + GhixPlatformConstants.DATABASE_TYPE);
						}
						return;
					}
					
					try
					{
						final String userId = getLoggedInUser();
						data.setConnection(connection);
						if(log.isTraceEnabled()){
							log.debug("GI JPA Dialect: Client Properties:");
						}
						if (userId != null)
						{
							final String prepSql = "{ call DBMS_SESSION.SET_IDENTIFIER(?) }";
							final CallableStatement cs = connection.prepareCall(prepSql);
							cs.setString(1, userId);
							cs.execute();
							cs.close();
							if(log.isTraceEnabled()){
								log.trace("Set CLIENT_IDENTIFIER [" + userId + "]");
							}
						}
					}
					catch (final Exception e)
					{
						log.error("Did not set CLIENT_IDENTIFIER for OracleConnection, "
						        + "are we booting application (or junit)? if so it's ok. [" + e.getMessage() + "]");
					}
				}
			});		
		return data;
	}

	@Override
	public void cleanupTransaction(Object transactionData) {
		((SessionTransactionData) transactionData).resetFlushMode();
	}

	@Override
	public ConnectionHandle getJdbcConnection(EntityManager entityManager, boolean readOnly)
			throws PersistenceException, SQLException {

		Session session = getSession(entityManager);
		return new HibernateConnectionHandle(session);
	}

	@Override
	public DataAccessException translateExceptionIfPossible(RuntimeException ex) {
		if (ex instanceof HibernateException) {
			return SessionFactoryUtils.convertHibernateAccessException((HibernateException) ex);
		}
		if (ex instanceof PersistenceException && ex.getCause() instanceof HibernateException) {
			return SessionFactoryUtils.convertHibernateAccessException((HibernateException) ex.getCause());
		}
		return EntityManagerFactoryUtils.convertJpaAccessExceptionIfPossible(ex);
	}

	protected Session getSession(EntityManager em) {
		if (em instanceof HibernateEntityManager) {
			return ((HibernateEntityManager) em).getSession();
		}
		else {
			Object delegate = em.getDelegate();
			if (delegate instanceof Session) {
				return (Session) delegate;
			}
			else {
				throw new IllegalStateException(
						"Cannot obtain native Hibernate Session from given JPA EntityManager: " + em.getClass());
			}
		}
	}


	private static class SessionTransactionData {

		private final Session session;

		private final FlushMode previousFlushMode;

		public SessionTransactionData(Session session, FlushMode previousFlushMode) {
			this.session = session;
			this.previousFlushMode = previousFlushMode;
		}

		public void resetFlushMode() {
			if (this.previousFlushMode != null) {
				this.session.setFlushMode(this.previousFlushMode);
			}
		}
		
		private Connection connection;

		/**
		 * Sets <code>java.sql.Connection</code> object.
		 * 
		 * @param connection database connection object.
		 */
		public void setConnection(final Connection connection)
		{
			this.connection = connection;
		}

		/**
		 * Returns database connection object.
		 * @return javax.sql.Connection
		 */
		@SuppressWarnings("unused")
		public Connection getConnection()
		{
			return connection;
		}	
	}


	private static class HibernateConnectionHandle implements ConnectionHandle {

		private final Session session;

		private static volatile Method connectionMethod;

		public HibernateConnectionHandle(Session session) {
			this.session = session;
		}

		public Connection getConnection() {
			try {
				if (connectionMethod == null) {
					// reflective lookup to bridge between Hibernate 3.x and 4.x
					connectionMethod = this.session.getClass().getMethod("connection");
				}
				return (Connection) ReflectionUtils.invokeMethod(connectionMethod, this.session);
			}
			catch (NoSuchMethodException ex) {
				throw new IllegalStateException("Cannot find connection() method on Hibernate session", ex);
			}
		}

		public void releaseConnection(Connection con) {
			JdbcUtils.closeConnection(con);
		}
	}


	/**
	 * Returns logged in user id (from <code>AccountUsers</code> table)
	 * 
	 * @return logged in user id or -1 if user is not available.
	 */
	protected String getLoggedInUser()
	{
		final SecurityContext ctx = SecurityContextHolder.getContext();

		if(ctx != null)
		{
			final Authentication auth = ctx.getAuthentication();

			if(auth != null)
			{
				final Object principal = auth.getPrincipal();
				
				// PII Information leak, commenting out the principal from the log
				log.trace("GHIXJpaDialect::getLoggedInUser() - UserAccount: ");// + principal);

				if(principal != null && (principal instanceof AccountUser))
				{
					final AccountUser accountUser = (AccountUser)principal;
					return String.valueOf(accountUser.getId());
				}
			}
		}

		return "-1";
	}			
	
}
