package com.getinsured.hix.platform.security.tokens;

/******************************
 * DO NOT MAKE ANY CHANGES TO THIS FILE
 * This token will be used for partner integrations while they use our library 
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.getinsured.hix.ws_trust.saml.InvalidTokenException;

public class SecureTokenFactory {
	private static SecureRandom random = new SecureRandom();
	/**
	 * Generates a binary token for supplied map of key value pairs using a user defined password
	 * Encryption uses AES 256 algorithm to encrypt the data
	 * @param password
	 * @param data
	 * @return GIBinaryToken object containing the Cipher data and the initialization vector and a token generation timestamp
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 */
	public static GiBinaryToken generateBinaryToken(String password, Map<String,String> data) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException{
		Map<String,String> lData = data;
		if(lData == null) {
			if(lData == null) {
				lData = new HashMap<String,String>(1);
			}
		}
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1".intern());
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES".intern());
		/* Encrypt the message. */
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding".intern());
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		ByteArrayOutputStream dataBuf = new ByteArrayOutputStream(1024);
		lData.put("PRIVATE_TIMESTAMP".intern(), Long.toString(System.currentTimeMillis()));
		DataOutputStream dout = new DataOutputStream(dataBuf);
		Iterator<Entry<String, String>> cursor = lData.entrySet().iterator();
		dout.writeDouble(random.nextDouble()); // Add some random bytes
		dout.writeInt(lData.size());
		while(cursor.hasNext()) {
			Entry<String, String> entry = cursor.next();
			dout.writeUTF(entry.getKey());
			dout.writeUTF(entry.getValue());
		}
		dout.flush();
		dout.close();
		byte[] iv = cipher.getIV();
		byte[] ciphertext = cipher.doFinal(dataBuf.toByteArray());
		GiBinaryToken token = new GiBinaryToken();
		token.setIv(iv);
		token.setCipherData(ciphertext);
		return token;
	}
	
	/**
	 * Processes a GiBinarytoken containing the encrypted data and initialization vector. Once this fnction returns, the token will have all the 
	 * Encrypted parameters correcty decrypted and populated. Caller needs to call {@GiBinaryToken.isValid(long validityPeriod)} to check if the 
	 * token is still valid to process.
	 * @param token
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 */
	public static GiBinaryToken processBinaryToken(String token, String password) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException{
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1".intern());
		KeySpec spec = new PBEKeySpec(password.toCharArray(), password.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES".intern());
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding".intern());
		GiBinaryToken bToken = new GiBinaryToken(token);
		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(bToken.getIv()));
		byte[] cipherData = bToken.getCipherData();
		byte[] data = cipher.doFinal(cipherData);
		DataInputStream din = new DataInputStream(new ByteArrayInputStream(data));
		din.readDouble();//Discard it
		int size = din.readInt();
		if(size > 0) {
			Map<String,String> parameters = new HashMap<String,String>();
			for(int i = 0; i < size; i++) {
				parameters.put(din.readUTF(), din.readUTF());
			}
			String timeStamp = parameters.get("PRIVATE_TIMESTAMP".intern());
			bToken.setTimeSTamp(Long.parseLong(timeStamp));
			bToken.setTokenParameters(parameters);
			return bToken;
		}
		return null;
	}
	
	public static void main(String[] args) throws InterruptedException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidTokenException, IOException{
		HashMap<String,String> data = new HashMap<String,String>();
		// Add as many parameter as you would like
		data.put("user", "9999999999");
		data.put("beneficiary", "1234567890");
		String pass = "ABN%56GT&23";
		GiBinaryToken tokenSent = SecureTokenFactory.generateBinaryToken(pass, data);
		System.out.println("**************** Token sent in payload ******************");
		String tokenStr = tokenSent.toString();
		System.out.println(tokenStr);
		//Thread.sleep(3000);
		GiBinaryToken parsed = SecureTokenFactory.processBinaryToken(tokenStr, pass);
		System.out.println("**************** Token received from the payload ******************");
		System.out.println("Logged In user:"+parsed.getParameter("user"));
		System.out.println("Beneficiary:"+parsed.getParameter("beneficiary"));
		System.out.println("Token valid:"+parsed.isValid(2));
	}
}
