package com.getinsured.hix.platform.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileBasedConfiguration {
	private File configFile = null;
	private GhixProperties properties;
	private List<ConfigurationChangeListener> listeners = Collections.synchronizedList(new ArrayList<>());
	private Timer timer;
	private long lastUpdateTime = -1l;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private static FileBasedConfiguration config;
	public static FileBasedConfiguration getConfiguration(){
		if(config == null){
			config = new FileBasedConfiguration();
		}
		return config;
	}
	private FileBasedConfiguration(){
		String ghixHome = System.getProperty("GHIX_HOME");
		configFile = new File(ghixHome+File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+"configuration.properties");
		logger.info("Loading properties from {}",configFile.getAbsolutePath());
		if(configFile.exists()){
			this.lastUpdateTime = configFile.lastModified();
			this.properties = new GhixProperties();
			try {
				FileInputStream configStream = new FileInputStream(configFile);
				this.properties.load(configStream);
				configStream.close();
			} catch (IOException e) {
				logger.error("Error loading properties file {} failed with message {}",configFile.getAbsolutePath(), e.getMessage(), e);
				throw new RuntimeException("Invalid configuration file "+e.getMessage(),e);
			}
		}else{
			logger.error("No config found at {}",configFile.getAbsolutePath());
		}
		initRefresh(configFile);
	}
	
	public FileBasedConfiguration(String configFilePath){
		configFile = new File(configFilePath);
		if(configFile.exists()){
			this.properties = new GhixProperties();
			try {
				FileInputStream configStream = new FileInputStream(configFile);
				this.properties.load(configStream);
				configStream.close();
			} catch (IOException e) {
				logger.error("Error loading properties file {} failed with message {}",configFile.getAbsolutePath(), e.getMessage(), e);
			}
		}
		initRefresh(configFile);
	}
	
	@PreDestroy
	public void cleanup(){
		logger.info("Cancelling the refresh timer, shutting down");
		this.timer.cancel();
	}
	
	private void initRefresh(File configFile){
		logger.info("Initiating the scheduler for config refresh");
		timer = new Timer();
		timer.schedule(new ConfigRefreshTask(),10000,2*60*1000);
	}
	
	public void addConfigurationChangeListener(ConfigurationChangeListener configChangeLIstener){
		this.listeners.add(configChangeLIstener);
	}
	
	public String getProperty(String nname){
		if(nname == null) {
			logger.error("No proprty name provided for lookup");
			return null;
		}
		return this.properties.getProperty(nname);
	}
	
	public Boolean getBoolean(String propName, boolean defaultVal) {
		String val = this.getProperty(propName);
		if(val == null) {
			return defaultVal;
		}
		return Boolean.valueOf(val);
	}
	
	private class ConfigRefreshTask extends TimerTask{
		private Logger logger = LoggerFactory.getLogger(getClass());
		private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
		@Override
		public void run() {
			long currentUpdated = configFile.lastModified();
			logger.info("Checking if configuration update is required Last modified on {} Current Update time {}, difference {} ms",sdf.format(new Date(lastUpdateTime)), sdf.format(new Date(currentUpdated)),(currentUpdated-lastUpdateTime));
			if(currentUpdated > lastUpdateTime){
				lastUpdateTime = currentUpdated;
				logger.info("Configuration file {} updated, initiating refresh",configFile.getAbsolutePath());
				Properties props = new Properties();
				try{
					FileInputStream configStream = new FileInputStream(configFile);
					props.load(configStream);
					configStream.close();
				}catch(Exception e){
					logger.error("Error loading properties file {} failed with message {}",configFile.getAbsolutePath(), e.getMessage(), e);
					return;
				}
				Enumeration<?> cursor = props.propertyNames();
				String name;
				String existingProperty;
				String currentProperty;
				while(cursor.hasMoreElements()){
					name = (String) cursor.nextElement();
					existingProperty = properties.getProperty(name);
					currentProperty = props.getProperty(name);
					if(existingProperty == null && currentProperty == null){
						// Both are null continue
						continue;
					}
					if(
							(existingProperty != null && existingProperty.trim().length() == 0) && 
							(currentProperty != null && currentProperty.trim().length() == 0)) {
						// Both are empty continue;
						continue;
					}
					if(existingProperty != null && currentProperty != null && !existingProperty.equalsIgnoreCase(currentProperty)){
						logger.info("Property {} found updated, checking for interested listeners from available {} listeners",name, listeners.size());
						for(ConfigurationChangeListener listner: listeners){
							if(listner.getSubscribedConfigurationList().contains(name)){
								logger.info("Notifying listener {}",listner.getClass().getName());
								listner.handleConfigurationChange(name,currentProperty);
							}
						}
						properties.setProperty(name, currentProperty);
					}
					// New properety
					if(!properties.contains(name) &&  currentProperty != null) {
						properties.setProperty(name, currentProperty);
					}
					
				}
				
			}
			
		}
		
		
	}
}
