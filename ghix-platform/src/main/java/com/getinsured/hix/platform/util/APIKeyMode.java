package com.getinsured.hix.platform.util;

public class APIKeyMode {
		public static final String DEFAULT_APIKEY_HEADER="x-api-key";
		public static final String HMAC = "HMAC";
		public static final String PLAIN = "PLAIN";
		public static final String BASIC = "BASIC";
		public static final String JWT = "JWT";
		public static final String HMAC_WITH_ID = "HMAC_WITH_ID";
		public static final String HMAC_NOANCE = "HMAC_NOANCE";
		private String key;
		private String mode;
		private String secret;
		private String headerName = DEFAULT_APIKEY_HEADER;

		APIKeyMode(String key, String mode, String secret, String headerName){
			this.key = key;
			this.mode = mode;
			this.secret =secret;
			this.setHeaderName(headerName);
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getMode() {
			return mode;
		}

		public void setMode(String mode) {
			this.mode = mode;
		}
		

		public String getSecret() {
			return secret;
		}

		public void setSecret(String secret) {
			this.secret = secret;
		}

		public String getHeaderName() {
			return headerName;
		}

		public void setHeaderName(String headerName) {
			if(headerName != null && headerName.length() > 0){
				this.headerName = headerName;
			}
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			result = prime * result + ((mode == null) ? 0 : mode.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			APIKeyMode other = (APIKeyMode) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			if (mode == null) {
				if (other.mode != null)
					return false;
			} else if (!mode.equals(other.mode))
				return false;
			return true;
		}
	}