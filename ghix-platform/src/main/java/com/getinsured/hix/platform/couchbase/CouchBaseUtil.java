package com.getinsured.hix.platform.couchbase;

import java.util.ArrayList;
import java.util.List;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.view.ViewRow;

public class CouchBaseUtil {
	private final static int MAX_DOC_SIZE_IN_MB = 10;

	private final static int MAX_DOC_SIZE_IN_BYTES = MAX_DOC_SIZE_IN_MB * 1024 * 1024;

	private CouchBaseUtil() {
	}

	public static int listSize(List<?> data) {
		return data != null ? data.size() : 0;
	}

	public static <T> T firstFromList(List<T> data) {
		return listSize(data) != 0 ? data.get(0) : null;
	}

	public static List<JsonObject> convertViewResultToJsonObjectResult(List<ViewRow> allRows) {
		List<JsonObject> data = new ArrayList<>();
		for (ViewRow row : allRows) {
			data.add(row.document().content());
		}
		return data;
	}

	public static JsonArray createJSONArgumentsArray(Object... args) {
		JsonArray argArray = JsonArray.create();
		for (Object arg : args) {
			argArray.add(arg);
		}
		return argArray;
	}

	public static int numberOfParts(int filesize) {
		int numberOfParts = 1;
		if (filesize > MAX_DOC_SIZE_IN_BYTES) {
			numberOfParts = (int) (filesize / MAX_DOC_SIZE_IN_BYTES) + (filesize % MAX_DOC_SIZE_IN_BYTES == 0 ? 0 : 1);
		}
		return numberOfParts;
	}

	public static byte[][] splitBytes(byte[] source) {
		final int totalSize = source.length;
		final int numberOfParts = numberOfParts(totalSize);
		byte[][] dataArray = new byte[numberOfParts][];
		int start = 0;
		for (int i = 0; i < numberOfParts; i++) {
			if (start + MAX_DOC_SIZE_IN_BYTES > totalSize) {
				dataArray[i] = new byte[totalSize - start];
				System.arraycopy(source, start, dataArray[i], 0, totalSize - start);
			} else {
				dataArray[i] = new byte[MAX_DOC_SIZE_IN_BYTES];
				System.arraycopy(source, start, dataArray[i], 0, MAX_DOC_SIZE_IN_BYTES);
			}
			start += MAX_DOC_SIZE_IN_BYTES;
		}

		return dataArray;
	}
}
