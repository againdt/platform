/**
 * 
 */
package com.getinsured.hix.platform.util.exception;

/**
 * @author panda_p
 *
 */
public class CaseOrderIdSaveException extends Exception{

	
	/**
	 * 
	 */
	public CaseOrderIdSaveException() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 */
	public CaseOrderIdSaveException(String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}

	private int a;
	public CaseOrderIdSaveException(int b) {
		a=b;
	}
	
	public String toString(){
	     return ("CaseOrderIdSaveException =  "+a) ;
	}
	
	public CaseOrderIdSaveException(String message, Throwable throwable) {
        super(message, throwable);
    }
	public CaseOrderIdSaveException(Throwable throwable) {
        super(throwable);
    }
}
