package com.getinsured.hix.platform.location.service.jpa;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.dto.smartystreet.Analysis;
import com.getinsured.hix.platform.dto.smartystreet.SmartyStreetResponse;
import com.getinsured.hix.platform.util.HIXHTTPClient;

/**
 * This implements Address Validator with SmartyStreet.com
 * 
 * @author polimetla_b
 * @since 11/1/2012
 */
public class AddressValidatorSmartyStreet implements AddressValidatorComponent {

	@Autowired private RestTemplate restTemplate;
	
	private static Set<String> VALID_DPV_MATCH_CODES = new HashSet<String>();
	
	static{
		VALID_DPV_MATCH_CODES.add("Y");
	}

	private String ss_authid;

	private String ss_authkey;

	private String ss_url;

	private String ss_candidates;

	/**
	 * This http client is used to communicate with smarty streets server.
	 */
	private HIXHTTPClient hIXHTTPClient;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AddressValidatorSmartyStreet.class);

	private String constructSmartyStreetUrl(LocationDTO address) throws UnsupportedEncodingException {
		final String DEFAULT_ENCODING = "UTF-8";
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(ss_url)
		        .queryParam("auth-id", ss_authid.trim())
		        .queryParam("auth-token", ss_authkey.trim())
		        .queryParam("candidates", ss_candidates.trim());
		
		String addressLine1 = address.getAddressLine1();
		if(StringUtils.isNotBlank(address.getAddressLine2())){
			addressLine1 = addressLine1.concat(" "+address.getAddressLine2());
		}
		
		builder = builder.queryParam("street", URLEncoder.encode(addressLine1, DEFAULT_ENCODING))
		        .queryParam("city", URLEncoder.encode(address.getCity(), DEFAULT_ENCODING))
		        .queryParam("state", URLEncoder.encode(address.getState(), DEFAULT_ENCODING))
				.queryParam("zipcode", URLEncoder.encode(address.getZipcode(), DEFAULT_ENCODING));
	
		return builder.build(false).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.location.service.LocationValidatorService#
	 * validateLocation(com.getinsured.hix.platform.location.model.Location)
	 */
	public List<Location> validateAddress(Location address) throws Exception {

		// FIXME comment this code during production.
		LOGGER.debug("===== validateLocation Begin ===== ");
		LOGGER.debug("ss_authid =====>" + ss_authid);
		LOGGER.debug("ss_authkey =====>" + ss_authkey);

		// Step 1: Form the URL
		String url = getSmartStreetUrl(address);
		// Step 2: Get the data from Smarty Street
		String ssResponse = hIXHTTPClient.getGETData(url);
		// LOGGER.debug(ssResponse);
		// Step 3: Parse the data
		List<Location> list = parseSmartyStreetData(ssResponse);
		// LOGGER.debug("===== validateLocation End ===== ");
		return list;
	}

	private ObjectMapper mapper = new ObjectMapper();

	/**
	 * Parse the incoming JSON data to objects.
	 * 
	 * @param data
	 * @return
	 */
	public List<Location> parseSmartyStreetData(String data) throws Exception{
		List<Location> list = new ArrayList<Location>();
		
			if (null == data || data.trim().length() <= 0){
				return null;
			}

			JsonNode rootNode = mapper.readTree(data);
			// LOGGER.debug("size==>" + rootNode.size());

			for (int i = 0; i < rootNode.size(); i++) {

				JsonNode addRoot = rootNode.get(i);

				if (null == addRoot){
					continue;
				}

				JsonNode component = addRoot.get("components");
				JsonNode metadata = addRoot.get("metadata");

				Location a1 = new Location();
				if (null != component) {
					
					// ----------- Begin of Address Line 1
					StringBuilder addressLine1 = new StringBuilder();
					
					String primary_number = getNodeValue(component.get("primary_number"));
					if (null != primary_number && primary_number.trim().length() > 0) {
						addressLine1.append(primary_number).append(" ");
					}
					
					String streetPredict = getNodeValue(component.get("street_predirection"));
					if (null != streetPredict && streetPredict.trim().length() > 0) {
						addressLine1.append(streetPredict).append(" ");
					}
										
					addressLine1.append(getNodeValue(component.get("street_name"))).append(" ");
					
					String suffix = getNodeValue(component.get("street_suffix"));
					if (null != suffix && suffix.trim().length() > 0) {
						addressLine1.append(suffix);
					}
					
					String streetPostdirection = getNodeValue(component.get("street_postdirection"));
					if (null != streetPostdirection && streetPostdirection.trim().length() > 0) {
						addressLine1.append(" ").append(streetPostdirection);
					}
					
					
					// ----------- End of Address Line 1

					a1.setAddress1(addressLine1.toString().trim().replaceAll("  ", " "));
					a1.setCity(getNodeValue(component.get("city_name")));
					a1.setState(getNodeValue(component
							.get("state_abbreviation")));
					a1.setZip(getNodeValue(component.get("zipcode")));
					a1.setAddOnZip(getNodeIntValue(component.get("plus4_code")));
				}

				if (null != metadata) {
					a1.setLat(getNodeDoubleValue(metadata.get("latitude")));
					a1.setLon(getNodeDoubleValue(metadata.get("longitude")));
					a1.setRdi(getNodeRDIValue(metadata.get("rdi")));
					a1.setCounty(getNodeValue(metadata.get("county_name")));
					
				}

				list.add(a1);

			}
		
		return list;
	}

	/**
	 * Validate the node and return data as String
	 * 
	 * @param element
	 * @return
	 */
	private String getNodeValue(JsonNode element) {
		if (null == element){
			return "";
		}

		return element.asText();
	}

	private String getNodeRDIValue(JsonNode element) {
		if (null == element){
			return "";
		}

		// R: Residential, C:Commercial, U:Unknown
		// Returns 1st Charectar
		if (element.asText() != null && element.asText().length() > 0){
			return "" + element.asText().charAt(0);
		}
		else{
			return "";
		}
	}

	/**
	 * @param element
	 * @return
	 */
	private int getNodeIntValue(JsonNode element) {
		if (null == element){
			return 0;
		}

		if (null == element.asText() || element.asText().trim().length() <= 0){
			return 0;
		}
		return Integer.parseInt(element.asText());
	}

	/**
	 * @param element
	 * @return
	 */
	private double getNodeDoubleValue(JsonNode element) {
		if (null == element){
			return 0;
		}

		if (null == element.asText() || element.asText().trim().length() <= 0){
			return 0;
		}

		return Double.parseDouble(element.asText());
	}

	/**
	 * HTTP Client is having issues in using URLBuilder and other ways, hence
	 * assembling manually.
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	private String getSmartStreetUrl(Location address) {

		// url =
		// "https://api.qualifiedaddress.com/street-address?auth-id=20d19bc4-f212-4df9-bbba-4207936f11ee&auth-token=FxPvVlYFlFFDGcJLqUKigrV9W50V19OhYn1Ek84SBXIPkdppI%2BnMp7fv6wZb%2BvDlQv3f4EgsDuThS%2FRiV3XjRw%3D%3D&street=2110%20Newmarket%20Parkway%20Suite%20200%20Marietta%20GA%2030067&street2=&city=&state=&zipcode=&candidates=10";

		StringBuilder newURL = new StringBuilder();

		newURL.append(ss_url).append("?");
		newURL.append("auth-id").append("=").append(ss_authid.trim())
				.append("&");
		newURL.append("auth-token").append("=").append(ss_authkey.trim())
				.append("&");
		newURL.append("candidates").append("=").append(ss_candidates.trim())
				.append("&");

		newURL.append(getEncodedString("street", address.getAddress1()));
		newURL.append(getEncodedString("city", address.getCity()));
		newURL.append(getEncodedString("state", address.getState()));
		if (address.getZip()!=null && address.getZip().length() > 0){
			newURL.append(getEncodedString("zipcode", "" + address.getZip()));
		}

		//LOGGER.info("==>" + newURL.toString());

		return newURL.toString();
	}

	/**
	 * Return the Encoded string.
	 * 
	 * @param key
	 * @param data
	 * @return
	 */
	private String getEncodedString(String key, String data) {
		String queryString = "";
		try {
			
			//Step 1: Check for key
			if(null == key || key.length() <=0){
				return queryString;
			}
			
			//Step 2: Encode if data is not null;
			if (null != data && data.length() > 0) {
				queryString = key + "="
						+ URLEncoder.encode(data.trim(), "UTF-8") + "&";
			}
			else
			{
				queryString = key + "="
						+ URLEncoder.encode("", "UTF-8") + "&";
			}
		} catch (Exception ex) {
			// Nothing to do.
		}
		return queryString;
	}

	public String getSs_authid() {
		return ss_authid;
	}

	public void setSs_authid(String ss_authid) {
		this.ss_authid = ss_authid;
	}

	public String getSs_authkey() {
		return ss_authkey;
	}

	public void setSs_authkey(String ss_authkey) {
		this.ss_authkey = ss_authkey;
	}

	public String getSs_url() {
		return ss_url;
	}

	public void setSs_url(String ss_url) {
		this.ss_url = ss_url;
	}

	public String getSs_candidates() {
		return ss_candidates;
	}

	public void setSs_candidates(String ss_candidates) {
		this.ss_candidates = ss_candidates;
	}

	public void sethIXHTTPClient(HIXHTTPClient hIXHTTPClient) {
		this.hIXHTTPClient = hIXHTTPClient;
	}
	
	@Override
	public AddressValidationResponse validateAddress(LocationDTO address) throws Exception {
		
		AddressValidationResponse response = new AddressValidationResponse();
		response.setInput(address);
		
		String url = constructSmartyStreetUrl(address);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<List<SmartyStreetResponse>> smartyStreetResponse = restTemplate.exchange(new URI(url), HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<SmartyStreetResponse>>() {});

		if(null!=smartyStreetResponse && null!= smartyStreetResponse.getBody() && smartyStreetResponse.getBody().size()>0){
			parseSmartyStreetResponse(response, smartyStreetResponse.getBody());
		}
		
		return response;
	}

	private void parseSmartyStreetResponse(AddressValidationResponse response, List<SmartyStreetResponse> smartyStreetResponseList){
		SmartyStreetResponse smartyStreetResponse = smartyStreetResponseList.get(0);

		if (null == smartyStreetResponse || (null != smartyStreetResponse && null != smartyStreetResponse.getAnalysis()	&& null != smartyStreetResponse.getAnalysis().getDpv_match_code())) {
			if (VALID_DPV_MATCH_CODES.contains(smartyStreetResponse.getAnalysis().getDpv_match_code().trim())) {
				response.setValid(true);
			}
			if (null == smartyStreetResponse.getAnalysis().getFootnotes()) {
				response.setExactMatch(true);
				return;
			}
		}

		/**
		 * If not an exact match, create suggestions
		 */
		List<LocationDTO> suggestions = new ArrayList<LocationDTO>(0);
		for (SmartyStreetResponse smartyStreetResponseEntry : smartyStreetResponseList) {
			LocationDTO suggestion = new LocationDTO();
			suggestion.setAddressLine1(smartyStreetResponseEntry.getDelivery_line_1());
			suggestion.setAddressLine2(smartyStreetResponseEntry.getDelivery_line_2());
			suggestion.setCity(smartyStreetResponseEntry.getComponents().getCity_name());
			suggestion.setState(smartyStreetResponseEntry.getComponents().getState_abbreviation());
			suggestion.setZipcode(smartyStreetResponseEntry.getComponents().getZipcode());

			Analysis analysis = smartyStreetResponseEntry.getAnalysis();
			if (null != analysis) {
				if ("S".equalsIgnoreCase(analysis.getDpv_match_code())) {
					suggestion.setRedundantSecondaryNumber(true);
				}
				
				String footnoteString = analysis.getFootnotes();
				
				Set<String> footnotes = new HashSet<String>();
				if(null!=footnoteString && footnoteString.trim().length()>0){
					String[] footnotesArray = footnoteString.split("#");
					footnotes.addAll(Arrays.asList(footnotesArray));
				}
				
				if(footnotes.contains("A")){
					suggestion.setZipcodeCorrected(true);
					response.setValid(false);
					footnotes.remove("A");
				}
				
				if(footnotes.contains("B")){
					suggestion.setCityStateSpellingFixed(true);
					response.setValid(false);
					footnotes.remove("B");
				}
				
				if(footnotes.contains("H")){
					suggestion.setMissingSecondaryNumber(true);
					footnotes.remove("H");
				}
				
				if(footnotes.contains("M")){
					suggestion.setFixedStreetSpelling(true);
					response.setValid(false);
					footnotes.remove("M");
				}
				
				if(footnotes.contains("N")){
					suggestion.setFixedAbreviations(true);
					footnotes.remove("N");
				}
				
				if(!footnotes.isEmpty()){
					suggestion.setOtherReason(true);
					response.setValid(false);
				}
				
			}

			suggestions.add(suggestion);
		}
		response.setSuggestions(suggestions);

	}
	
	
}
