/**
 * GHIXSFTPClient.java
 */

package com.getinsured.hix.platform.ftp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

public class GHIXSFTPClient {
	
	private static final int BUFFER_SIZE = 1024;
	private static final int MAX_CONTENT_SIZE = 1024*1024;
	private static final String INVALID_FTP_CLIENT = "Please initialize the SFTP Client with host and credentials";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GHIXSFTPClient.class);

	// SFTP Login Credentials
	private String hostname;
	private String username;
	private String password;
	private int port;
	

	private boolean bInited = false;
	
	private Session client = null;
	private ChannelSftp sftpChannel = null;
	private int connectAttempts = 0;
	private static final int MAX_RETRY = 5;
	public GHIXSFTPClient(String hostname, String username, String password, int port) {
		super();
		this.hostname = hostname;
		this.username = username;
		this.password = password;
		this.port = port;
		bInited = true;
		LOGGER.info("GHIXSFTPClient initialized for host " + hostname);		
	}
	
	/**
	 * This method initializes the SFTP connection
	 * 
	 * @param remoteFileName
	 * @return byte[] content of the file
	 * @throws Exception
	 */
	public void initConnection() throws SocketException, IOException {
		if (null != client) {
			if (client.isConnected()) {
					return;
			}else{
				LOGGER.info("SFTP connection to host:"+this.hostname+":"+this.port+" is lost, tryin to establish the session again");
				if(connectAttempts > MAX_RETRY){
					throw new GIRuntimeException("Failed to connect to "+this.hostname+" max retry reached");
				}
				LOGGER.info("Connection dropped trying again Exception occurred, trying again");
				this.connectAttempts++;
				this.client = null; //reset the sesion to null
				initConnection();
			}
		}
		LOGGER.info("SFTP client being instantiated.");
		JSch sftpClient = new JSch();
		try {
			client = sftpClient.getSession(username,hostname, port);
			client.setConfig("StrictHostKeyChecking", "no");
			client.setPassword(password);
			LOGGER.info("Establishing connection as "+this.username+"@"+this.hostname);
			client.connect(60*1000);
			Channel channel = client.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;
			this.connectAttempts = 0; //reset the count
			LOGGER.info("SFTP client instance created. Login successful, connection attempt count reset to 0");
		} catch (JSchException e) {
			if(connectAttempts > MAX_RETRY){
				throw new GIRuntimeException("Failed to connect to "+this.hostname+" max retry reached");
			}
			LOGGER.info("Connection dropped trying again Exception occurred, trying again");
			this.connectAttempts++;
			this.client = null;
			initConnection();
		}
	}

	/**
	 * This method takes a remoteFileName and it will get the InputStream content for
	 * the file. The file is searched in the root folder of the FTP Server for
	 * the login account
	 * 
	 * @param remoteFileName
	 * @return InputStream of the file
	 * @throws Exception
	 */
	public InputStream getFileInputStream(String remoteFileName) throws Exception {
		
		LOGGER.info("getFileInputStream() START");
		//LOGGER.info("Getting file content for " + remoteFileName);
		
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		InputStream inputStream = null;
		// BufferedInputStream inbf = null;
		try {
			initConnection();
			LOGGER.info("Connection establised.");
			inputStream = sftpChannel.get(remoteFileName);
			LOGGER.info("Input stream received is null: " + (null == inputStream));
			return inputStream;
		} catch (Exception ex) {
			LOGGER.info("Exception while reading file");
			throw ex;
		} finally {
			LOGGER.info("getFileInputStream() END");
		}
	}

	/**
	 * This method takes a remoteFileName and it will get the OutputStream content for
	 * the file. The file is searched in the root folder of the FTP Server for
	 * the login account
	 * 
	 * @param remoteFileName
	 * @return OutputStream of the file
	 * @throws Exception
	 */
	public OutputStream putFileOutputStream(String remoteFileName) throws Exception {
		
		LOGGER.info("putFileOutputStream() START");
		LOGGER.info("Getting file content for " + remoteFileName);
		
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		OutputStream outputStream = null;
		// BufferedInputStream inbf = null;
		try {
			initConnection();
			LOGGER.info("Connection establised.");
			outputStream = sftpChannel.put(remoteFileName);
			LOGGER.info("Output stream received is null: " + (null == outputStream));
			return outputStream;
		} catch (Exception ex) {
			LOGGER.info("Exception while reading file");
			throw ex;
		} finally {
			LOGGER.info("putFileOutputStream() END");
		}
	}
	
	/**
	 * This method takes a remoteFileName and it will get the byte content for
	 * the file. The file is searched in the root folder of the FTP Server for
	 * the login account
	 * 
	 * @param remoteFileName
	 * @return byte[] content of the file
	 * @throws Exception
	 */
	public byte[] getFileContent(String remoteFileName) {
		InputStream in = null;
		BufferedInputStream inbf = null;
		int contentSize = MAX_CONTENT_SIZE;
		byte[] content = new byte[MAX_CONTENT_SIZE]; // 1MB
		byte[] buf = new byte[1024]; //1K
		try {
			initConnection();
			in = sftpChannel.get(remoteFileName);
			inbf = new BufferedInputStream(in);
			int read = -1;
			int totalBytes = 0;
			while((read = inbf.read(buf)) > 0){
				if(totalBytes +read > contentSize){
					content = growContextBuffer(contentSize, content, BUFFER_SIZE);
					contentSize += BUFFER_SIZE;
				}
				System.arraycopy(buf,0,content,totalBytes,read);
				totalBytes += read;
			}
			if(totalBytes > MAX_CONTENT_SIZE*2){
				LOGGER.warn("***** WARNING ***** File size exceeds 2 MB, Please consider streaming in chunks ***** WARNING ******");
			}
			return exactContentBuffer(content,totalBytes);
		} catch (IOException | SftpException ex) {
			throw new GIRuntimeException("Failed to get the file content for file:"+remoteFileName,ex);
		} finally {
			try {
				if (inbf != null){
					inbf.close();
				}
				if (in != null){
					in.close();
				}
			} catch (IOException e) {
				LOGGER.error("Exception while closing steams..ignoring");
				//Ignore
			}
		}
	}

	private byte[] exactContentBuffer(byte[] content, int totalBytes) {
		byte[] buf = new byte[totalBytes];
		System.arraycopy(content, 0, buf, 0, totalBytes);
		return buf;
	}

	/**
	 * Increases the capacity, Remote stream doesn't tell us how many bytes are available from the stream
	 * @param content
	 * @param i
	 * @return
	 */
	private byte[] growContextBuffer(int maxContentSize, byte[] content, int increaseBy) {
		LOGGER.debug("Increasing buffer size by "+increaseBy);
		byte[] tmp = new byte[maxContentSize+increaseBy];
		System.arraycopy(content, 0, tmp, 0, content.length);
		return tmp;
	}

	/**
	 * This method allows to move a file into any remote folder within the root
	 * folder and a target filename. For e.g. you can move a file named
	 * file1.txt and move it inside of a /success folder with the name
	 * file1-123.txt
	 * 
	 * @param remoteFileName
	 *            This is the original file name that you want to move
	 * @param remoteTargetFolder
	 *            This is the target folder that you want the above file to be
	 *            moved. This folder will be created inside of the FTP root
	 *            folder if it is not present
	 * @param remoteTargetFileName
	 *            This is the target file name
	 * @throws Exception
	 */
	public void moveFile(String sourceFolder, String sourceFileName,
			String remoteTargetFolder, String remoteTargetFileName)
			throws Exception {
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		try {
			initConnection();
			
			try {
				sftpChannel.mkdir(remoteTargetFolder);
			}
			catch (SftpException ex) {
			}
			String sourceFile = null;
			if(StringUtils.isNotBlank(sourceFolder)){
				sourceFile = sourceFolder + File.separator + sourceFileName;
			}
			else{
				sourceFile = sourceFileName;
			}
			String targetFile = remoteTargetFolder + File.separator + remoteTargetFileName;
			
			sftpChannel.rename(sourceFile, targetFile);
			
			//Clean up : Delete inprocess folder.
			/*if(StringUtils.isNotBlank(sourceFolder)){
				sftpChannel.rmdir(sourceFolder);
			}*/
			
		} catch (Exception ex) {
			LOGGER.error("Error occured while moving file",ex);
			throw ex;
		}
	}
	
	/**
	 * Method is used to create directory to remote location.
	 * @param remoteFolserName
	 * @throws Exception
	 */
	public void createDirectory(String remoteFolserName)
			throws Exception {

		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		try {
			initConnection();
			sftpChannel.mkdir(remoteFolserName);
		} catch (Exception e) {
			LOGGER.error("Error occured while creating directory",e);
			throw e;
		}
	}
	
	public void close(){
		try {
			if(client != null){
				client.setTimeout(60); // Let this thread die
			}
			if(this.sftpChannel != null){
				this.sftpChannel.disconnect();
				this.sftpChannel.quit();
			}
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			LOGGER.warn("Exception setting timeout while closing the client..Ignoring: "+e.getMessage());
		}
		this.client = null; // reset the reference	
	}

	/**
	 * Method to upload file from local to remote location
	 * 
	 * @param args
	 * @throws Exception
	 */
	public void uplodadFile(InputStream localFile, String remoteFileName)
			throws Exception {

		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		try {
			initConnection();
			sftpChannel.put(localFile, remoteFileName);
		} catch (Exception e) {
			LOGGER.error("Error occured while uploading file",e);
			throw e;
		}
	}
	
	/*
	public static void main(String[] args) throws Exception {

		GHIXSFTPClient sftp = new GHIXSFTPClient( "74.205.108.146","csruser","GHIX123#", 22);
		byte[] content = sftp.getFileContent("Aanchal.jpg");
		sftp.close();
		byte[] content2 = sftp.getFileContent("Outbound_call_leads.csv");
		
		//sftp.testSFTP();
		LOGGER.info("Done transferring the file, transferred "+content.length +" bytes, closing the session");
		StringBuffer filePath = new StringBuffer("c:").append(File.separator).append("hub_services").append(File.separator).append("Aanchal.jpg");
		FileOutputStream fos = null;
		
		try{
			fos = new FileOutputStream(new File(filePath.toString()));
		fos.write(content);
		}finally{
			IOUtils.closeQuietly(fos);
		}
		
		sftp.close();
	}
   */
	
	/*
	private void testSFTP() {
		JSch client1 = new JSch();
		Session session;
		try {
			// session = client1.getSession("csruser", "10.178.228.205", 22);
			session = client1.getSession("csruser", "74.205.108.146", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword("GHIX123#");
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;

			try {
				LOGGER.info("Directory:" + sftpChannel.pwd());
				//System.out.println("File list:"						+ sftpChannel.ls(sftpChannel.pwd()));
				sftpChannel.cd("\\serff");
				LOGGER.info("Directory after cd:" + sftpChannel.pwd());
				
				// Code performing read operation 
				InputStream in = sftpChannel.get("csrfilepoller.pl");

				BufferedInputStream inbf = null;
				try {
					LOGGER.info("Input stream is " + in);
					inbf = new BufferedInputStream(in);
					byte buffer[] = new byte[BUFFER_SIZE];
					int readCount;
					StringBuffer resultString = new StringBuffer();
					while ((readCount = inbf.read(buffer)) > 0) {
						resultString.append(new String(buffer, 0, readCount));
					}
					;
					
					LOGGER.info("File content is:\n" + resultString.toString());
					

				} catch (Exception ex) {
					LOGGER.error("Exception while reading file",ex);
				} finally {
					try {
						if (inbf != null){
							inbf.close();
						}
						if (in != null){
							in.close();
						}
					} catch (Exception e) {
						LOGGER.error("Exception while closing steams",e);

					}
				}
				
				// Code to perform move operation
				sftpChannel.rename("CSRAdvancePaymentDetermination.xml", "inprocess/CSRAdvancePaymentDetermination.xml");
				//sftpChannel.rename("inprocess/CSRAdvancePaymentDetermination.xml", "CSRAdvancePaymentDetermination.xml");
				sftpChannel.exit();
				session.disconnect();
			} catch (SftpException e) {				
				LOGGER.error("",e);
			}

		} catch (JSchException e) {
			LOGGER.error("",e);
		}
	}
	*/
	
	
	public List<String> getFileNames(String folderName){
		List<String> list = new ArrayList<String>();
			try{
				initConnection();
				LOGGER.info("Connection obtained");
				  @SuppressWarnings("unchecked")
					Vector<ChannelSftp.LsEntry> vector = sftpChannel.ls(folderName); 
				  if(null!=vector){
					  for(ChannelSftp.LsEntry entry : vector) {
						  String fileNames = entry.getFilename();
						  LOGGER.info("fileNames ::"+fileNames);
						  if(fileNames!= null &&!fileNames.equals(".") && !fileNames.equals("..")){
					    	list.add(fileNames);
					    	}
					    }
				  }
				    
			}catch(Exception e){
				LOGGER.error("Exception occured in geeting file names" ,e.getMessage());
		}
		return list ;
	}
	
	
	public Map<String, InputStream> getFilesFromDirectory(String FolderName) throws Exception {

        Map<String, InputStream> fileMap = null;

        if (!bInited) {
                throw new Exception(INVALID_FTP_CLIENT);
        }

        try {
                initConnection();
                fileMap = new HashMap<String, InputStream>();

                @SuppressWarnings("unchecked")
                Vector<ChannelSftp.LsEntry> list = sftpChannel.ls(FolderName);
                String fileName = null;
                InputStream inputStream = null;

                for (ChannelSftp.LsEntry entry : list) {
                        fileName = entry.getFilename();

                        if (!fileName.equals(".") && !fileName.equals("..")) {
                                inputStream = sftpChannel.get(FolderName + entry.getFilename());
                                fileMap.put(fileName, inputStream);
                        }
                }
        }
        catch (Exception e) {
        	LOGGER.error("Error occured while getting files from directory",e);
            throw e;
        }
        return fileMap;
}

	
	public boolean isRemoteDirectoryExist(String path) throws Exception {
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		try {
			initConnection();
			sftpChannel.stat(path);
		} catch (SftpException e) {
			if (e.id == 2) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * This method is used to check if file is a directory or not
	 * @param path
	 * @return
	 * @throws Exception
	 */
	
	public boolean isRemoteDirectory(String path) throws Exception {
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		SftpATTRS statAttribute = null;
		try {
			initConnection();
			statAttribute =  sftpChannel.stat(path);
			return statAttribute.isDir();
		} catch (SftpException e) {
				return false;
		}
	}
	
	
	/**
	 * Method to append to a  file from local to remote location
	 * 
	 * @param args
	 * @throws Exception
	 */
	public void appendFile(InputStream localFile, String remoteFileName)
			throws Exception {

		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		try {
			initConnection();
			sftpChannel.put(localFile, remoteFileName, ChannelSftp.APPEND );
		} catch (Exception e) {
			LOGGER.error("Error occured while uploading file",e);
			throw e;
		}
	}
}
