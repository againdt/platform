package com.getinsured.hix.platform.util.exception;



/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class GIFatalRuntimeException extends GIRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1890248835312240801L;

	public GIFatalRuntimeException(Component component,String ghixErrorCode,String errorMessage){
		super(ghixErrorCode, null, errorMessage, Severity.FATAL, component != null ? component.getComponent() : null, null);
	}
	
	public GIFatalRuntimeException(Component component,Throwable aThrowable){
		super(null, aThrowable, "-", Severity.FATAL, component != null ? component.getComponent() : null, null);
	}
	
	public GIFatalRuntimeException(Component component,String ghixErrorCode,String errorMessage, Throwable aThrowable){
		super(ghixErrorCode, aThrowable, errorMessage, Severity.FATAL, component != null ? component.getComponent() : null, null);
	}
}
