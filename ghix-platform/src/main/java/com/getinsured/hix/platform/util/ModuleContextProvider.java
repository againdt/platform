package com.getinsured.hix.platform.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component("moduleContext")
@Scope("prototype")
public class ModuleContextProvider{
	private static Logger logger = LoggerFactory.getLogger(ModuleContextProvider.class);
	private ApplicationContext moduleContexts = null;
	private boolean initialized = false;
	private static Map<String, ApplicationContext> moduleAppContexts = Collections.synchronizedMap(new HashMap<String, ApplicationContext>());
	
	@PostConstruct
	public void init() {
		if(initialized) {
			logger.info("This bean has already been initialized, this should be avoided");
		}
		initialized = true;
	}
	
	public synchronized void setModuleContext(ApplicationContext context){
		this.moduleContexts = context;
		moduleAppContexts.put(context.getApplicationName(), context);
	}
	
	public synchronized static Object getModuleBean(String name) {
		Iterator<Entry<String, ApplicationContext>> cursor = moduleAppContexts.entrySet().iterator();
		Entry<String, ApplicationContext> entry = null;
		Object bean = null;
		Map<String, String> beanProviders = new HashMap<>(); 
		while(cursor.hasNext()) {
			entry = cursor.next();
			if(logger.isInfoEnabled()) {
				logger.info("Checking the application: {}", entry.getKey());
			}
			try {
				bean = entry.getValue().getBean(name);
				beanProviders.put(entry.getKey(), bean.getClass().getName());
			}catch(Exception e) {
				logger.info("No bean available for {} in application {}", name, entry.getKey());
			}
		}
		if(beanProviders.size() > 1) {
			logger.error("Multiple modules have defined the same bean, not sure which one is required");
			Iterator<Entry<String, String>> providerCursor = beanProviders.entrySet().iterator();
			Entry<String, String>  ctx = null;
			while(providerCursor.hasNext()) {
				ctx = providerCursor.next();
				logger.error("Application {} has defined the bean with name {} for class {}", ctx.getKey(), name, ctx.getValue());
			}
			throw new RuntimeException("Multiple bean with name "+name+" found");
		}
		return bean;
	}
	
	public synchronized static Object getModuleBeanByClass(Class<?> clz) {
		Iterator<Entry<String, ApplicationContext>> cursor = moduleAppContexts.entrySet().iterator();
		Entry<String, ApplicationContext> entry = null;
		Object bean = null;
		Map<String, String> beanProviders = new HashMap<>(); 
		while(cursor.hasNext()) {
			entry = cursor.next();
			if(logger.isInfoEnabled()) {
				logger.info("Checking the application: {}", entry.getKey());
			}
			try {
				bean = entry.getValue().getBean(clz);
				beanProviders.put(entry.getKey(), bean.getClass().getName());
			}catch(Exception e) {
				logger.info("No bean available for {} in application {}", clz.getName(), entry.getKey());
			}
		}
		if(beanProviders.size() > 1) {
			logger.error("Multiple modules have defined the same bean, not sure which one is required");
			Iterator<Entry<String, String>> providerCursor = beanProviders.entrySet().iterator();
			Entry<String, String>  ctx = null;
			while(providerCursor.hasNext()) {
				ctx = providerCursor.next();
				logger.error("Application {} has defined the bean with name {} for class {}", ctx.getKey(), clz.getName(), ctx.getValue());
			}
			throw new RuntimeException("Multiple bean with name "+clz.getName()+" found");
		}
		return bean;
	}
	
	public  Object getBean(Class<?> clz, String name){
		Map<String, ?> beans = null;
		beans = moduleContexts.getBeansOfType(clz);
		if(beans == null || beans.size() == 0){
			return null;
		}
		if(beans.size() > 1){
			throw new GIRuntimeException("Multiple ocurrences found for type:"+clz.getName()+" Names:"+beans.keySet());
		}
		// We have exactle one item
		return beans.entrySet().iterator().next().getValue();
	}
	
	public  Object getBean(String name, String contextName ){
		Object bean = moduleContexts.getBean(name);
		return bean;
	}
	
	/**
	 * Do not use this method, its provided for backward compatibility and results are not guranteed
	 * Unless caller is sure about the uniqueness of the bean name, this method should not be invoked
	 * @param name of the desired bean
	 * @return
	 */
	public  Object getBean(String name){
		return this.moduleContexts.getBean(name);
	}
	
	public  Object getBean(Class<?> name){
		return this.moduleContexts.getBean(name);
	}
	
	public  List<?> getAllBeans(Class<?> clzName, String contextName){
		Map<String, ?> beans = null;
		beans = moduleContexts.getBeansOfType(clzName);
		if(beans == null || beans.size() == 0){
			return null;
		}
		if(beans.size() > 1){
			throw new GIRuntimeException("Multiple ocurrences found for type:"+clzName.getName()+" Names:"+beans.keySet());
		}
		Iterator<?> cursor = beans.entrySet().iterator();
		List<Object> list = new ArrayList<>();
		while(cursor.hasNext()){
			@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String, Object>) cursor.next();
			list.add(entry.getValue());
		}
		return list;
	}
	
	public  Object getBeanByTypeAndName(Class<?> clzName, String name, String contextName){
		
		Map<String, ?> beans = null;
		beans = moduleContexts.getBeansOfType(clzName);
		if(beans == null || beans.size() == 0){
			return null;
		}
		Object bean = null;
		Iterator<?> cursor = beans.entrySet().iterator();
		while(cursor.hasNext()){
			@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String, Object>) cursor.next();
			if(entry.getKey().equals(name)){
				bean = entry.getValue();
			}
		}
		return bean;
	}
}
