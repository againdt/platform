package com.getinsured.hix.platform.repository;

import java.text.DateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

//import com.getinsured.hix.model.Employee;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

import org.springframework.beans.factory.ObjectFactory;

import com.getinsured.hix.model.GhixNotificationType;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.GhixNoticeUser;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;

@Repository
@Transactional(readOnly = true)
public class NoticeTypeRepository {

	private static final Logger LOGGER = Logger.getLogger(NoticeTypeRepository.class);

	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired
	private INoticeTypeRepository noticeTypeRepository;

	public NoticeType findById(Integer id) {
		return noticeTypeRepository.findById(id);
	}

	public NoticeType findByEmailClass(String emailClass) throws NotificationTypeNotFound {
		return findByEmailClassAndLanguage(emailClass, GhixLanguage.US_EN);
	}
	
	public NoticeType findByQueuedEmailClass(String queuedEmailClass) throws NotificationTypeNotFound {
		return noticeTypeRepository.findByQueuedEmailClass(queuedEmailClass);
	}

	public NoticeType findByEmailClassAndLanguage(String emailClass, GhixLanguage language) throws NotificationTypeNotFound {
		NoticeType noticeType = noticeTypeRepository.findByEmailClassAndLanguage(emailClass, language);

		isNoticeTypeActive(emailClass, language, noticeType);
		return noticeType;
	}

	private void isNoticeTypeActive(String emailClass, GhixLanguage language, NoticeType noticeType) throws NotificationTypeNotFound {

		Date today = new TSDate();
		if (noticeType != null){
			if (today.before(noticeType.getEffectiveDate())){
				LOGGER.warn("Notice Type is not active as of today for emailClass and language " + emailClass + "  " + language + " - today = " + today );
				throw new NotificationTypeNotFound("Notice Type is not active as of today for emailClass and language " + emailClass + "  " + language);
			} else if (today.after(noticeType.getTerminationDate())){
				LOGGER.warn("Notice Type is not active as of today for emailClass and language " + emailClass + "  " + language + " - today = " + today );
				throw new NotificationTypeNotFound("Notice Type is not active as of today for emailClass and language " + emailClass + "  " + language);
			}
		}
	}

	public List<NoticeType> findAll() {
		return noticeTypeRepository.findAll();
	}

	public Page<NoticeType> findAll(Pageable pageable){
		return noticeTypeRepository.findAll(pageable);
	}

	@Transactional
	public NoticeType save(NoticeType noticeType) {
		return noticeTypeRepository.save(noticeType);
	}

	public void flush(){
		noticeTypeRepository.flush();
	}
	
	
  public  Map<String,Object> searchNotification(Map<String,Object> searchCriteria){
		
		QueryBuilder<NoticeType> notificationQuery = delegateFactory.getObject();
		notificationQuery.buildSelectQuery(NoticeType.class);
		DateFormat formatter;
		
		String name = null;
		if(searchCriteria != null && searchCriteria.get("name") != null && !searchCriteria.get("name").equals("") ){
			name = (String) searchCriteria.get("name");
		}
		
		GhixNoticeUser user =null;
		if(searchCriteria != null && searchCriteria.get("user") != null){
			user = GhixNoticeUser.valueOf((String) searchCriteria.get("user"));
		}
		
		GhixNotificationType type =  null;
		if(searchCriteria != null && searchCriteria.get("type") != null){
			type = GhixNotificationType.valueOf((String) searchCriteria.get("type"));
		}
		
		GhixNoticeCommunicationMethod method =  null;
		if(searchCriteria != null && searchCriteria.get("method") != null){
			method = GhixNoticeCommunicationMethod.valueOf((String) searchCriteria.get("method"));
		}
		
		
	
		if (name != null && name.length() > 0)
		{			
			notificationQuery.applyWhere("notificationName", name.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		if (user != null)
		{			
			notificationQuery.applyWhere("user", user, DataType.ENUM, ComparisonType.LIKE);
		}
		
		if (type != null)
		{			
			notificationQuery.applyWhere("type", type, DataType.ENUM, ComparisonType.LIKE);
		}
		
		if (method != null)
		{			
			notificationQuery.applyWhere("method", method, DataType.ENUM, ComparisonType.LIKE);
		}
		
		if(searchCriteria != null && searchCriteria.get("hideTerminated") != null){
			
			if("NO".equals(searchCriteria.get("hideTerminated")))
			{
				Date today = new TSDate();				
				notificationQuery.applyWhere("terminationDate", today, DataType.DATE, ComparisonType.GE);				
			}
			
		}
		
		if(searchCriteria != null && searchCriteria.get("sortBy") != null && searchCriteria.get("sortOrder") != null){
			notificationQuery.applySort(searchCriteria.get("sortBy").toString(), SortOrder.valueOf(searchCriteria.get("sortOrder").toString()));  
		}
		
		List<String> columns = new ArrayList<String>();
		columns.add("id");
        columns.add("notificationName");
        columns.add("language");
        columns.add("type");
        columns.add("updated");
        columns.add("templateEditable");
        notificationQuery.applySelectColumns(columns);
		
		Map<String,Object> notificationListAndRecordCount = new HashMap<String,Object>();
		try {
			//List<Announcement> announcements = announcementQuery.getRecords(startRecord, pageSize);
			notificationQuery.setFetchDistinct(true);
			List<Map<String, Object>> notifications = notificationQuery.getData((Integer)searchCriteria.get("startRecord"), (Integer)searchCriteria.get("pageSize"));
			notificationListAndRecordCount.put("notificationlist", notifications);
			notificationListAndRecordCount.put("recordCount", notificationQuery.getRecordCount());
		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("",e);
		}
		return notificationListAndRecordCount;
	}
  
  
	public NoticeType findByNameAndLanguage(String notificationName, GhixLanguage language) throws NotificationTypeNotFound {
		NoticeType noticeType = noticeTypeRepository.findByNotificationNameAndLanguage(notificationName, language); 

		//isNoticeTypeActive(name, language, noticeType);
		return noticeType;
	}
  
	
}

