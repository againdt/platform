package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ActiveFlag;
import com.getinsured.hix.model.TenantPermission;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Transactional
public interface ITenantPermissionRepository extends JpaRepository<TenantPermission, Long> {
	
	@Query(value= "select tp from TenantPermission tp  where tp.permission.name = :permission and tp.tenantId = :tenantId and tp.isActive = :isActive")
	List<TenantPermission> findByTenantIdAndPermissionName(@Param("tenantId") long tenantId,@Param("permission") String permission,@Param("isActive") ActiveFlag isActive);
	
	@Query(nativeQuery= true,value="INSERT"+
		" INTO TENANT_PERMISSIONS"+
		" ("+
			" ID,"+
			" PERMISSION_ID,"+
			" TENANT_ID,"+
			" CREATION_TIMESTAMP,"+
			" LAST_UPDATE_TIMESTAMP"+
			" IS_ACTIVE"+
		" )"+
		" ("+
			" SELECT"+
			" TENANT_PERMISSIONS_SEQ.NEXTVAL,"+
			" p.id,t.id,systimestamp,systimestamp,COALESCE(p.IS_DEFAULT,'N') "+
			" FROM tenant t, permissions p"+
			" where t.name= :tenantName"+
		" )"
		)
	void addTenantPermissions(@Param("tenantName") String tenantName);
	
	public List<TenantPermission> findByIdIn(List<Long> tenantPermissionIdList);
}
