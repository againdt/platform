package com.getinsured.hix.platform.account.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.model.InboxMsgResponse;
import com.getinsured.hix.model.InboxUserAddressBookResp;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * <p>
 * What to do with pagination - Use Search Criteria.
 * <p>
 * FIXME - What is the plan for sorting data....for now we sort on message ID.
 * <p>
 * JIRA HIX-1135
 * 
 * @author polimetla_b
 * @since 11/6/2012
 * 
 */
public interface InboxMsgService {

	/**
	 * <p>
	 * Provide searchable attributes in InboxMsg form. This searches only
	 * database. No Alfresco.
	 * <p>
	 * If the startDate and endDate are null, search for first 100 messages.
	 * This is configurable item from properties file.
	 * 
	 * @param msg
	 * @return
	 */
	InboxMsgResponse searchMessages(InboxMsg msg, Calendar startDate,
			Calendar endDate);

	/**
	 * Get Message tree for given message id. This is useful for administrators
	 * to track the flow.
	 * 
	 * @param msg
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	InboxMsgResponse getMessageTree(InboxMsg msg, Calendar startDate,
			Calendar endDate);

	/**
	 * <p>
	 * If the message is having CLOB attribute set to TRUE, user need to load
	 * this on demand basis.
	 * <p>
	 * 
	 * @param messageID
	 * @return String representation of the clob
	 */
	String getMessageCLOB(long messageID);

	/**
	 * This is two phase search.
	 * <p>
	 * Step 1: Search in Alfresco for given user.
	 * <p>
	 * Step 2: Search in Database for given file ids
	 * <p>
	 * Step 3: Remove search results which messages are not archived/ deleted.
	 * <p>
	 * If the startDate and endDate are null, search for first 100 messages.
	 * This is configurable item from properties file.
	 * <p>
	 * 
	 * @param msg
	 * @return
	 */
	InboxMsgResponse searchAttachments(InboxMsg msg, Calendar startDate, Calendar endDate) throws GIException;

	/**
	 * Method to save a message in DB
	 * 
	 * <P>
	 * Possible incoming status is N-New, O-Open, P-Replied, F-Forwarded,
	 * A-Archived, R-Read, S-Sent, D-Deleted, C-Compose
	 * 
	 * @author talreja_n
	 * @param msg
	 *            - Message to be saved
	 * @return object of InboxMsgResponse which contains a list of messages that
	 *         have been saved
	 * @throws GIException
	 *             - In case cloning of Object fails
	 * @throws ContentManagementServiceException 
	 */
	InboxMsgResponse saveMessage(InboxMsg msg) throws GIException, ContentManagementServiceException;

	/**
	 * During compose, user can cancel the draft. Mark as delete.
	 * 
	 * @param msg
	 * @return
	 */
	InboxMsgResponse cancelMessage(InboxMsg msg) throws GIException;

	/**
	 * After inserting the message, user can add attachments.
	 * 
	 * @param msg
	 * @param attachment
	 * @return
	 * @throws ContentManagementServiceException 
	 */
	InboxMsgResponse addAttachment(InboxMsg msg, byte[] attachment) throws GIException, ContentManagementServiceException;
	
	/**
	 * Removes attachments from a message and Alfresco
	 * 
	 * @author Nikhil Talreja
	 * @since 19 December 2012
	 * @param msg
	 * @param attachment
	 * @return InboxMsgResponse Object containing state of operation
	 * @throws GIException, ContentManagementServiceException 
	 */
	InboxMsgResponse removeAttachment(InboxMsg msg, String fileName) throws GIException, ContentManagementServiceException;
	
	/**
	 * Return the file content for given document ID. This is used to download
	 * the file attachments.
	 * 
	 * @param msg
	 * @return
	 */
	byte[] getAttachment(InboxMsgResponse inboxMsgResponse, String documentID) throws GIException, ContentManagementServiceException;

	/**
	 * This returns user address book for given user for a particular role. User can communicate with
	 * only these groups.
	 * 
	 * @param userID
	 * @return
	 */
	InboxUserAddressBookResp getUserAddressBook(long userID, String role);

	/**
	 * Update User Addressbook. This is used by admins only.
	 * 
	 * @param userID
	 * @param messageList
	 * @return
	 */
	InboxUserAddressBookResp saveUserAddressBook(long userID, List<InboxMsg> messageList);

	InboxUserAddressBookResp deleteUserAddressBook(long userID,
			List<InboxMsg> messageList);
	
	
	/**
	 * This method retrieves the message details for the given messageId.
	 * 
	 * @param messageID
	 * @return object of InboxMsg
	 */
	@Deprecated
	InboxMsg readMessage(long messageID);


	/**
	 * Method is used to get messages from InboxMsg base on the owner and status.
	 * @param ownerUserId, Owner User Id
	 * @param statusList, Status List
	 * @param pageable, Pageable object
	 * @return InboxMsg data in Page format
	 */
	Page<InboxMsg> findByOwnerUserIdAndStatusIn(long ownerUserId, List<InboxMsg.STATUS> statusList, Pageable pageable) throws GIException;
	
	Page<InboxMsg> findByModuleIdAndModuleNameAndStatusIn(long moduleId, String moduleName, List<InboxMsg.STATUS> statusList, Pageable pageable);
	
	/**
	 * Method is used to get messages from InboxMsg base on search criteria with status is not a Delete.
	 * @param ownerUserId, Owner User Id
	 * @param searchText, search text
	 * @param pageable, Pageable object
	 * @return InboxMsg data in Page format
	 */
	Page<InboxMsg> getMessagesBySearchCriteria(long ownerUserId,long moduleId,String moduleName,String searchText, Pageable pageable) throws GIException;
	
	/**
	 * Method to find a message by its id.
	 * @author Nikhil Talreja
	 * @param id, Primary key of the message
	 * @return InboxMsg object in
	 */
	InboxMsg findMessageById(long id);
	
	/**
	 * Method is used to modify inbox message with Archive, Read or Unread status.
	 * @param ownerUserId, owner user id in long
	 * @param inboxMsgIds, inbox messsage ids in String.
	 * @param inboxMsgStatus, inbox message status in enum.
	 * @return InboxMsgResponse Object containing state of operation
	 * @throws GIException
	 */
	InboxMsgResponse secureInboxAction(long ownerUserId, String inboxMsgIds, InboxMsg.STATUS inboxMsgStatus) throws GIException;
	
	/**
	 * Method to count the no. of messages created by a user on the current day
	 * This count cannot be more than 100
	 * @author Nikhil Talreja
	 * @param id, Primary key of the message
	 * @return InboxMsg object in
	 */
	Long countMessagesPerDayForUser(long ownerId);
	
	/**
	 * Method is used to get count of Unread Messages for respected owner user.
	 * @param ownerUserId, owner user id in long form.
	 * @return Long, number of count
	 */
	Long countUnreadMessagesByOwner(long ownerUserId);
	
	Long countUnreadMessagesByModule(long moduleId, String moduleName);
	
	/**
	 * Method to update a message in DB
	 * 
	 * <P>
	 * Possible incoming status is N-New, O-Open, P-Replied, F-Forwarded,
	 * A-Archived, R-Read, S-Sent, D-Deleted, C-Compose
	 * 
	 * @author Sunil Desu
	 * @param msg
	 *            - Message to be saved
	 * @return object of InboxMsgResponse which contains a list of messages that
	 *         have been saved
	 * @throws GIException
	 *             - In case cloning of Object fails
	 * @throws ContentManagementServiceException 
	 */
	InboxMsgResponse updateMessage(InboxMsg msg) throws GIException, ContentManagementServiceException;	
}
