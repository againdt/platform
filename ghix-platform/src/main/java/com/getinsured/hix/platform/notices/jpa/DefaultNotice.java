package com.getinsured.hix.platform.notices.jpa;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.UIConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notification.NoticeTmplHelper;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;


import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
/**
 * Default Notice implementation based on Flying Saucer to generate PDF and upload to ECM (Alfresco).
 *
 * @author EkramAli Kazi
 *
 */
    


@Component
class DefaultNotice extends AbstractNotice {


	private static final String TENDIGITCONSTANT = "0123456789";
	@Autowired private ContentManagementService ecmService;
	@Autowired private NoticeTmplHelper noticeTmplHelper;
	@Autowired private  ApplicationContext appContext;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultNotice.class);
	
	public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	
	public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
	//private static final String TODAYSDATE = null;
	private static final String TODAYSDATE = "2131231";

	public byte[] getPreviewContent(String templateContent, Map<String, Object> tokens) throws NoticeServiceException{

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		byte[] pdfBytes= "Not available".getBytes();

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			getAllAdditionalTokens(tokens);
			
			tmpl.process(tokens, sw);
			
			pdfBytes = generatePdf(sw.toString());
			
		} catch (Exception e) {
			if(!GhixPlatformConstants.REMOTE_EMAIL_ENABLED) {
				LOGGER.warn("Exception found in getPreviewContent() template review- ", e);
				throw new NoticeServiceException();
			}
		}
		return pdfBytes;
	}
	
	
	public static void main (String at[]) throws IOException, NoticeServiceException
	{
		DefaultNotice df= new DefaultNotice();
		InputStream is = new FileInputStream("/Users/joshi_n/Desktop/GetInsured/iex/ghix-config/src/main/resources/notificationTemplate/mn/EE016DemographicChangeNotification.html");
		
		
		File file = new File("/Users/joshi_n/Desktop/GetInsured/outputfile31.pdf");
        FileOutputStream fos = null;
 		String data=  (IOUtils.toString(is));
 		Map<String, String> tokens= new HashMap<String,String>();
 		tokens.put(TemplateTokens.USER_FULL_NAME, "Name Here");
 		tokens.put(TemplateTokens.ADDRESS_LINE_1, "Address line1 ");
 		tokens.put(TemplateTokens.ADDRESS_LINE_2, "Address line2 ");
 		tokens.put(TemplateTokens.CITY_NAME, "City");
 		tokens.put(TemplateTokens.STATE_CODE, "State Code");
 		tokens.put(TemplateTokens.PIN_CODE, "Zipcode");
 		tokens.put(TemplateTokens.TODAYS_DATE, TODAYSDATE);
 		data=df.templateContentWithTokensReplaced(tokens,data);
		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType("omit");
		tidy.setXHTML(true);
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.parse(new StringReader(data), writer);
		writer.close();
		 try {
             
	            fos = new FileOutputStream(file);
	            // Writes bytes from the specified byte array to this file output stream 
	            fos.write(df.generatePdf(writer.toString()));
	        }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 finally {
	            // close the streams using close method
	            try {
	                if (fos != null) {
	                    fos.close();
	                }
	            }
	            catch (IOException ioe) {
	                System.out.println("Error while closing stream: " + ioe);
	            }
		 }
		
	}
	
private String templateContentWithTokensReplaced(Map<String, String> tokens, String templateContent) throws NoticeServiceException {
		
		//String templateContent = readTemplateContent(location);
		if(templateContent == null) {
			return null;
		}
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
		}
		return sw.toString();

	}
	
	/**
	 * This method only called for preview not for actual population of data.
	 * @param tokens
	 * @return
	 * @throws NoticeServiceException 
	 */
	private Map<String, Object> getAllAdditionalTokens(Map<String, Object> tokens) throws NoticeServiceException {
		
		Map<String,String> templateTokens = new HashMap<String, String>();			
		templateTokens.put(TemplateTokens.HOST,GhixPlatformEndPoints.GHIXHIX_SERVICE_URL);
		templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		templateTokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		
		templateTokens.put(TemplateTokens.TODAYS_DATE, TODAYSDATE);
				
		//the hard coded value added. This method only called for preview not for actual population of data.
		templateTokens.put(TemplateTokens.NOTICE_UNIQUE_ID, TENDIGITCONSTANT);
		String headerContent = this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_HEADER_LOCATION);
		tokens.put(TemplateTokens.HEADER_CONTENT, (headerContent != null) ? headerContent:"not_applicable");
		String footerContent = this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_FOOTER_LOCATION);
		tokens.put(TemplateTokens.FOOTER_CONTENT, (footerContent != null)?footerContent:"not_applicable");
		return tokens;
	}
	
	private String getTemplateContentWithTokensReplaced(Map<String, String> tokens, String location) throws NoticeServiceException {
		
		String templateContent = readTemplateContent(location);
		if(templateContent == null) {
			return null;
		}
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
		}
		return sw.toString();

	}
	


	@Override
	public String createPDFDocument(NoticeType noticeType, Map<String, Object> tokens, String ecmFilePath, String ecmFileName) throws NoticeServiceException {
		if (StringUtils.isEmpty(noticeType.getTemplateLocation())){
			throw new NoticeServiceException("Template Location not found for notice name - " + noticeType.getNotificationName() + " and language - " + noticeType.getLanguage());
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("createPDFDocument::barcode token = {}", tokens.get("barcode"));
		}
		String templateContent = readTemplateContent(noticeType.getTemplateLocation());
		LOGGER.debug("createPDFDocument::templateContent = {}", templateContent);
		if(templateContent == null) {
			if(GhixPlatformConstants.REMOTE_EMAIL_ENABLED ) {
				return UUID.randomUUID().toString();
			}
		}

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);

			if (LOGGER.isDebugEnabled()) {
				templateConfig.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
			} else {
				templateConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			}

			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			tmpl.process(tokens, sw);
		} catch(TemplateException te) {
			LOGGER.warn("Template processing error - ", te);
		} catch (Exception e) {
			LOGGER.warn("Exception found in notice template - ", e);
		}

		byte[] pdfBytes;
		try {
			pdfBytes = generatePdf(sw.toString());
		} catch (Exception e) {
			LOGGER.error("Error invoking flying saucer api - ", e);
			throw new NoticeServiceException("Error invoking flying saucer api - " + e.getMessage(), e);
		}

		try {
			return ecmService.createContent(ecmFilePath, ecmFileName, pdfBytes, ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.NOTICE, null);
		} catch (ContentManagementServiceException e) {
			throw new NoticeServiceException(e);
		}

	}
	
	
	private String readTemplateContent(String path) throws NoticeServiceException {
		InputStream inputStreamUrl = null;
		try {
			if (StringUtils.isEmpty(path)){
				throw new NoticeServiceException("Template Location not found for notice name - " + path);
			}
			if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE).equalsIgnoreCase("N")){
				inputStreamUrl = getTemplateFromResources(path);
			}
			else{
				String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
						+ path;
				
				inputStreamUrl = getTemplateFromECM(ecmTemplateFolderPath);
			}
			return IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (Exception cmse) {
			if(!GhixPlatformConstants.REMOTE_EMAIL_ENABLED) {
				LOGGER.error("Error reading template"  , cmse);
				throw new NoticeServiceException("Error reading template from  - " + path, cmse);
			}
		} 
		finally
		{
			IOUtils.closeQuietly(inputStreamUrl);
		}
		return null;
	}
	
	private byte[] generatePdf(String incomingData) throws IOException, NoticeServiceException {
		
		//Replace all img url path from GHIXWEB_SERVICE_URL to GHIXHIX_SERVICE_URL
		String serverLogoUrl = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL+"resources/img/";//+DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE);
		String localLogoUrl = GhixPlatformEndPoints.GHIXHIX_SERVICE_URL+"resources/img/";//+DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE);
		String finalData = new String(StringUtils.replace(formData(incomingData), serverLogoUrl, localLogoUrl));
		
		LOGGER.debug("generatePdf::finalData = {}", finalData);

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try{
			/** If resources (images, css, etc) are not found,
			 * then ITextRenderer logs "<strong>java.io.IOException: Stream closed</strong>" exception without throwing it
			 * So we can't catch such exception thrown by 3rd party jar
			 *
			 * Also, stack trace gets printed in the PDF on Alfresco
			 *
			 */
			ITextRenderer renderer = new ITextRenderer();
			renderer.getSharedContext().setReplacedElementFactory(new B64ImgReplacedElementFactory(renderer.getSharedContext().getReplacedElementFactory()));
			renderer.setDocumentFromString(finalData);
			renderer.layout();
			renderer.createPDF(os);
		}catch(Exception e){
			throw new NoticeServiceException(e);
		}
		finally{
			os.close();
		}

		return os.toByteArray();
	}

	private String formData(String data) throws IOException {

		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType("omit");
		tidy.setXHTML(true);
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.parse(new StringReader(data), writer);
		writer.close();
		return writer.toString();
	}
	
	private InputStream getTemplateFromECM(String path) throws ContentManagementServiceException {
		return new ByteArrayInputStream(
				noticeTmplHelper.readBytesByPath(path));
	}
	
	private InputStream getTemplateFromResources(String path) throws IOException {

		final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

		String newPath = path;

		// If we are not on PHIX, and stateCode is two-letter state code of some sort.
		if(!stateCode.toLowerCase().contains("phix") && stateCode.length() == 2) {
			newPath = path.replace("notificationTemplate/", "notificationTemplate/" + stateCode.toLowerCase() + "/");
		}

		Resource resource = appContext.getResource("classpath:" + newPath);

		if(!resource.exists()) {
			LOGGER.warn("Template file Resource: {} does not exist", path);
			throw new IOException("Unable to get template from resource path: " + path);
		}

		if(!resource.isReadable()){
			throw new IOException("Template file Resource:"+ path +" is not readable");
		}

		LOGGER.info("Resource found: {}", path);

		return resource.getInputStream();
	}
}
