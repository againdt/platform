package com.getinsured.hix.platform.auditor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;

/**
 * Annotation which indicates that a controller method needs auditing
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface GiAudit {
	String transactionName();
	EventTypeEnum eventType();
	EventNameEnum eventName();
}