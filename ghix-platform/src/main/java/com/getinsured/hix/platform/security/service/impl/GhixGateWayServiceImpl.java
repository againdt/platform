///**
// * Declares the Ghix GateWay Service operations to uri traffic with ghix applicaitons
// * @author venkata_tadepalli
// * @since 02/01/2014
// */
package com.getinsured.hix.platform.security.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.security.service.GhixGateWayService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

@Service("ghixGateWayService")
public class GhixGateWayServiceImpl implements GhixGateWayService {
	String ghixGWayKey = this.getClass().getSimpleName();
	@Autowired
	GhixJasyptEncrytorUtil ghixJasypt;
	private static final String GHIX_CID = "ghixCid";
	/**
	 * method to encrypt the url. accepts HttpServletRequest and url
	 * 
	 * @param request
	 * @param url
	 * @return conversationId
	 * @throws UnsupportedEncodingException
	 */
	public String setConversationUrl(HttpServletRequest request, String url) throws UnsupportedEncodingException {
		// 1. Generate gwCipher = encrypted cipher (salt);
		// Concatnate to encrypt the request url
		String ghixCid = UUID.randomUUID().toString();

		url = ghixJasypt.encryptStringByJasypt(url);
		request.getSession().setAttribute(GHIX_CID, ghixCid);
		request.getSession().setAttribute(ghixCid, (String) url);

		return ghixCid;
	}

	/**
	 * method to decrypt the url. accepts HttpServletRequest
	 * 
	 * @param request
	 * @return url
	 * @throws UnsupportedEncodingException
	 */
	public String getConversationUrl(HttpServletRequest request) throws UnsupportedEncodingException {

		String ghixCid = (String) request.getSession().getAttribute(GHIX_CID);

		if (StringUtils.isBlank(ghixCid)) {
			return null;
		}

		String conversationUrl = (String) request.getSession().getAttribute(ghixCid);

		if (StringUtils.isBlank(conversationUrl)) {
			return null;
		}

		conversationUrl = ghixJasypt.decryptStringByJasypt(conversationUrl);
		request.getSession().removeAttribute(ghixCid);
		request.getSession().removeAttribute(GHIX_CID);
		return conversationUrl;
	}

}