package com.getinsured.hix.platform.security.service.jpa;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.cap.CapTeamMemberDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CapTeam;
import com.getinsured.hix.model.CapTeamLeaders;
import com.getinsured.hix.model.CapTeamMembers;
import com.getinsured.hix.model.GroupInfo;
import com.getinsured.hix.model.GroupInfoFilter;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.repository.IGroupRepository;
import com.getinsured.hix.platform.security.repository.IGroupUsersRepository;
import com.getinsured.hix.platform.security.repository.ITeamLeaderRepository;
import com.getinsured.hix.platform.security.service.GroupService;
import com.getinsured.hix.platform.util.exception.CAPException;
import com.getinsured.hix.platform.util.exception.CAPException.TeamError;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

@Service("GroupService")
public class GroupServiceImpl implements GroupService {
	private static int COUNT_COLUMN_NUMBER =6;
	private static long COUNT_MIN_SIZE =0l;
	private static long COUNT_MAX_SIZE =100l;
	
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired IGroupRepository groupRepository;	
	@Autowired IGroupUsersRepository iGroupUsersRepository;
	@Autowired ITeamLeaderRepository teamLeadersRepository;
	private static final Logger LOGGER = Logger.getLogger(GroupServiceImpl.class);
	
	@Override
	@Transactional
	public CapTeam addGroup(String name, Integer groupLeadUserId)throws CAPException{
		
		CapTeam newGroup = new CapTeam();
		newGroup.setGroupName(name);
		newGroup.setGroupLeadUserId(groupLeadUserId);
		newGroup.setStartDate(new TSDate());
		CapTeam addedGroup = null;
		
		try {
			long count = groupRepository.doesGroupExist(name);
			if(count > 0){
				addedGroup = new CapTeam();
				addedGroup.setId(-1); 
			}else{
			
				addedGroup = groupRepository.save(newGroup);
				addedGroup = groupRepository.save(newGroup);
			}
			
		} catch (Exception e) {
			LOGGER.error("Exception occurred while saving: ", e);	
			throw  new CAPException(TeamError.ADD_ERROR, null, e.getMessage(), Severity.LOW);
		}
		
		return addedGroup;	
	}

	@Override
	@Transactional
	public CapTeam editGroup(Integer groupId, String name, Integer groupLeadUserId) throws CAPException {
		
		CapTeam group = groupRepository.findById(groupId);
		
		//create a new team row in the DB with today's start date
		CapTeam newGroup = new CapTeam();
		newGroup.setGroupName(name);
		newGroup.setGroupLeadUserId(groupLeadUserId);
		newGroup.setStartDate(new TSDate());
		newGroup.setCreated(new TSDate());
		if(group != null) {
			try{
				
				
				long count = groupRepository.doesGroupExist(name, groupId);
				if(count > 0){
					group = new CapTeam();
					group.setId(-1); 
				}else{
					//edit the name
					if(!StringUtils.isEmpty(name) && !group.getGroupName().equalsIgnoreCase(name)) {
						newGroup.setGroupName(name);
					}
					//edit the team leader
					if(groupLeadUserId != null && group.getGroupLeadUserId()!=groupLeadUserId) {
						newGroup.setGroupLeadUserId(groupLeadUserId);
						// set the end date of the team which is updated.
						group.setEndDate(new TSDate());
						
						// set the team id of the new team as the team_id of the existing team
						newGroup.setUpdated(new TSDate());
						newGroup = groupRepository.save(newGroup);
					}
					
					group = groupRepository.save(group);
				}
				
			}catch (Exception e) {
				LOGGER.error("Exception occurred while updating group: ", e);	
				throw  new CAPException(TeamError.EDIT_ERROR, null, e.getMessage(), Severity.LOW);
			}
			
		}
		
		return group;
	}

		
	
	
	
	@Override
	public boolean updateGroupMembers(Integer groupId, List<Integer> userIds) throws CAPException {
		boolean bStatus = false;
		List<Integer> newUserList = userIds;
		
		try {
			if(!groupRepository.exists(groupId)) {
				LOGGER.error("Invalid Group groupId , not available in the DB: " + groupId);
				return bStatus;
			}

			List<Integer> existingUsers = iGroupUsersRepository.findUsersByGroupId(groupId);
			for (Iterator<Integer> iterator = newUserList.iterator(); iterator.hasNext();) {
				Integer userId = iterator.next();
				
				if(existingUsers.contains(userId)) {
					existingUsers.remove(existingUsers.indexOf(userId));
					iterator.remove(); // remove from the new user list
				}				
			}
			
			//Users to be deleted
			if(!existingUsers.isEmpty()) {
				bStatus = deleteGroupMembers(groupId, existingUsers);				
			}
			
			//new users to be added
			if(!newUserList.isEmpty()) {
				bStatus = addNewUsersToGroup(groupId, newUserList);
			}
			
			////update group with updated date.
			if(bStatus){
				CapTeam capTeam = groupRepository.findById(groupId);
				capTeam.setUpdated(new TSDate());
				groupRepository.save(capTeam);
			}
			
			if(existingUsers.isEmpty() && newUserList.isEmpty()) {
				bStatus = true;
			}
		} catch (Exception e) {
			LOGGER.error("Cannnot update GroupUser", e);
			bStatus = false;
			throw  new CAPException(TeamError.UPDATE_GROUP_MEMBERS, null, e.getMessage(), Severity.LOW);
		}
		
		return bStatus;
	}

	@Override
	@Transactional
	public boolean deleteGroupMembers(Integer groupId, List<Integer> userId){
		boolean bStatus = false;
		
		try {
			if(null!=userId&&!userId.isEmpty()){
				iGroupUsersRepository.deleteByGroupAndUsers(groupId, userId);
				LOGGER.info("User Ids has been removed. count: " + userId.size());
			}
			else{
				iGroupUsersRepository.deleteByGroup(groupId);
				LOGGER.info("User Ids has been removed. based on group ");
			}
			bStatus = true;
			
		} catch (Exception e) {
			LOGGER.error("Unable to delete the users ", e);
		}
		
		return bStatus;
	}

	@Override
	public boolean addNewUsersToGroup(Integer groupId, List<Integer> userIdList) {
		List<CapTeamMembers> addUsers = new ArrayList<CapTeamMembers>();
		for (Integer userId : userIdList) {
			CapTeamMembers queueUser = new CapTeamMembers();
			queueUser.setUserId(userId);
			queueUser.setGroupId(groupId);
			
			addUsers.add(queueUser);
		}
		
		try {
			List<CapTeamMembers> addedUsers = iGroupUsersRepository.save(addUsers);
			return (addedUsers.size() == addUsers.size());
		} catch (Exception e) {
			LOGGER.error("Unable to add the users ", e);
			
		}
		
		return false;
	}
	
	@Override
	public boolean addNewUsersToGroupWithStartDate(Integer groupId, Integer userId, Date startDate) {
			
		CapTeamMembers queueUser = new CapTeamMembers();
		queueUser.setStartDate(startDate);
			
		
		try {
			CapTeamMembers addedUser = iGroupUsersRepository.save(queueUser);
			if(null!=addedUser){
				return true;
			}
		} catch (Exception e) {
			LOGGER.error("Unable to add the users ", e);
		}
		return false;
	}
	
	@Override
	public boolean addNewUsersToGroupWithStartDate(Integer groupId,
			List<Integer> userIds, Date startDate) {

		if (null != userIds && userIds.size() > 0 && null != startDate) {
			List<CapTeamMembers> teamMembers = new ArrayList<CapTeamMembers>();
			for (Integer userId : userIds) {
				CapTeamMembers teamMember = new CapTeamMembers();
				teamMember.setStartDate(startDate);
				teamMember.setUserId(userId);
				teamMember.setGroupId(groupId);
				teamMembers.add(teamMember);
			}

			try {
				teamMembers = iGroupUsersRepository.save(teamMembers);
				if (null != teamMembers) {
					return true;
				}
			} catch (Exception e) {
				LOGGER.error("Unable to add the users ", e);
			}
		}
		return false;
	}

	@Override
	public CapTeam findById(int groupId) throws CAPException {
		CapTeam group = null;
		try{
			group = groupRepository.findById(groupId);
		}catch (Exception e) {
			LOGGER.error("Unable to find the group ", e);
			throw  new CAPException(TeamError.FIND_BY_ID, null, e.getMessage(), Severity.LOW);
		}
		return group;
		
	}

	@Override
	@Transactional
	public boolean deleteByGroupId(Integer groupId) throws CAPException {
		boolean bStatus = false;
		try{
			iGroupUsersRepository.deleteByGroup(groupId);
			groupRepository.deleteByGroupId(groupId);
		}catch (Exception e) {
			LOGGER.error("Unable to delete the group ", e);
		}
		return bStatus;
	}
	
	@Override
	@Transactional
	public void deleteByGroupIdAndEndDate(Integer groupId,String endDate) throws CAPException {
		try{
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			Date teamStartDate = null;
			teamStartDate = dateFormat.parse(endDate);
			
			iGroupUsersRepository.deleteByGroupAndEndDate(groupId,teamStartDate);
			groupRepository.deleteGroupByGroupIdAndEndDate(groupId, teamStartDate);
			teamLeadersRepository.deleteByGroupAndEndDate(groupId, teamStartDate);
		}catch (Exception e) {
			LOGGER.error("Unable to delete the group ", e);
		}
	}


	@Override
	@Transactional
	public void deleteTeamMembersWithEndDate(List<Integer> teamMemberIds,String endDate) throws CAPException {
		try{
			if(StringUtils.isEmpty(endDate)){
				LOGGER.error("Unable to delete the team members. EndDate returned empty");
				return;
			}
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			Date teamStartDate = null;
			teamStartDate = dateFormat.parse(endDate);
			iGroupUsersRepository.deleteTeamMemberByEndDate(teamMemberIds,teamStartDate);
			
		}catch (Exception e) {
			LOGGER.error("Unable to delete the group ", e);
		}
	}
	
	

	@Override
	public List<CapTeam> findAll() {
		List<CapTeam> groups = Collections.emptyList();
		try {
			groups =groupRepository.findActiveGroupsSortedByName();		
		} catch (Exception e) {
			LOGGER.error("Unable to get the groups ", e);
		}
	   return groups;
	}	
	
	@Override
	public List<GroupInfo> getGroupInfoList(GroupInfoFilter groupInfoFilter) throws CAPException {
		StringBuilder query = new StringBuilder("select g.id , g.groupName as groupName ,g.groupLeadUserId as leadId,us.firstName as fname,us.lastName as lname,");
		query.append("(select count(u) from CapTeamMembers u where g.id = u.groupId and u.endDate IS NULL) as count,g.created as created, g.updated as updated, " +
					 "g.startDate from CapTeam g, AccountUser us where g.groupLeadUserId= us.id and g.endDate is NULL");
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
			   query.append(" and g.tenantId = ").append(TenantContextHolder.getTenant().getId()).append(" ");
			  }
		Map<String, Object> mapQueryParam = new HashMap<>();
		setQueryParam(groupInfoFilter, query, mapQueryParam);
		List<GroupInfo> groupInfos = new ArrayList<>();
		EntityManager em = null;
		try {					
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			Query hqlQuery = em.createQuery(query.toString());
					
			// Set query parameters
			setNamedQueryParams(hqlQuery, mapQueryParam);
			
			@SuppressWarnings("unchecked")
			List<Object> resultList = hqlQuery.getResultList();
			for (Object row : resultList) {
				Object[] rowArray = (Object[]) row;
				GroupInfo groupInfo = new GroupInfo();
				if(null!=rowArray[0]){
					groupInfo.setId((Integer)rowArray[0]);
				}
				groupInfo.setGroupName((String)rowArray[1]);
				groupInfo.setGroupLeadUserId((Integer)rowArray[2]);
				groupInfo.setGroupLeadFirstName((String)rowArray[3]);
				groupInfo.setGroupLeadLastName((String)rowArray[4]);
				if(null!=rowArray[5]){
					groupInfo.setGroupSize(((Long)rowArray[5]).intValue());
				}
				if(null!=rowArray[6]){
					groupInfo.setCreated((Date)rowArray[6]);
				}
				if(null!=rowArray[7]){
					groupInfo.setUpdated((Date)rowArray[7]);
				}
				if(null!=rowArray[8]){
					groupInfo.setStartDate((Date)rowArray[8]);
				}
				groupInfos.add(groupInfo);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to search Teams", e);
			
			throw  new CAPException(TeamError.MANAGE_TEAM, null, e.getMessage(), Severity.LOW);
			
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		return groupInfos;
	}  

	private void setNamedQueryParams(Query hqlQuery, Map<String, Object> mapQueryParam) {
		for(String param : mapQueryParam.keySet()){
			hqlQuery.setParameter(param, mapQueryParam.get(param));
		}
	}
	
	private void setQueryParam(GroupInfoFilter groupInfoFilter,
			StringBuilder query, Map<String, Object> mapQueryParam) {
		if(groupInfoFilter.getTeamId()!= null){
			query.append(" and g.id = :groupId");
			mapQueryParam.put("groupId", groupInfoFilter.getTeamId());
		}
		
		if(groupInfoFilter.getGroupLeadId()!= null){
			query.append(" and g.groupLeadUserId = :groupLeadId");
			mapQueryParam.put("groupLeadId", groupInfoFilter.getGroupLeadId());
		}
		
		if(groupInfoFilter.getGroupMinSize()!= null || groupInfoFilter.getGroupMaxSize()!= null){
			query.append(" and (select count(u) from CapTeamMembers  u where g.id = u.groupId) between :groupMinSize and :groupMaxSize");
//			and (select count(*) from CapTeamMembers u where g.ID = u.GROUPS_OF_USERS_ID) between 0 and 1
			mapQueryParam.put("groupMinSize", groupInfoFilter.getGroupMinSize()!=null?new Long(groupInfoFilter.getGroupMinSize()):COUNT_MIN_SIZE);
			mapQueryParam.put("groupMaxSize", groupInfoFilter.getGroupMaxSize()!=null?new Long(groupInfoFilter.getGroupMaxSize()):COUNT_MAX_SIZE);
		}
		
//		if(groupInfoFilter.getGroupMaxSize()!= null){
//			query.append(" and (select count(u) from CapTeamMembers u where g.id = u.groupId) >= :groupMaxSize");
//			mapQueryParam.put("groupMaxSize", new Long(groupInfoFilter.getGroupMaxSize()));
//		}
		if(groupInfoFilter.getTeamUserId()!= null){
			/*query.append(" and ug.userId = :teamUserId"); //TODO Later
			mapQueryParam.put("teamUserId", new Long(groupInfoFilter.getTeamUserId()));*/
		}
		query.append(" order BY ");
		if(groupInfoFilter.getSortColumn() == GroupInfoFilter.sort.groupSize){
			query.append(COUNT_COLUMN_NUMBER);
		}else if(groupInfoFilter.getSortColumn() == GroupInfoFilter.sort.leadName){
			query.append(" fname, lname ");
		}else{
			query.append(" groupName");
		}
		
		
		if(!groupInfoFilter.isSortDirection()){
			query.append(" DESC");
		}else{
			query.append(" ASC");
		}
	}
	
	
	@Override
	public Map<Integer, String> getAvailableCSRUser(){
		Map<Integer, String> usersMap = new HashMap<>();
		try {
			List<AccountUser> users = iGroupUsersRepository.getAvailableCSRUser();
			
			usersMap = getUserIdAndNameMap(users);
		} catch (Exception e) {
			LOGGER.error("Unable to get CSR user", e);
		}
		return usersMap;
	}
	
	@Override
	public Map<Integer, String> getAccountUsersByGroupId(Integer groupId) {
		Map<Integer, String> usersMap = new HashMap<>();
		try {
			List<AccountUser> groupMembers = iGroupUsersRepository.getAccountUsersByGroupId(groupId);
			usersMap = getUserIdAndNameMap(groupMembers);
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		return usersMap;
	}
	
	@Override
	public List<AccountUser> getAccountUsersListByGroupId( Integer groupId) throws CAPException{
		List<AccountUser> groupMembers = new ArrayList<>();
		try {
			groupMembers = iGroupUsersRepository.getAccountUsersByGroupId(groupId);
			
		} catch (Exception e) {
			LOGGER.error("Unable to get the group member ", e);
			throw  new CAPException(TeamError.ACCOUNT_USER_LIST_BY_GROUP_ID, null, e.getMessage(), Severity.LOW);
		}
		return groupMembers;
	}
	
	@Override
	public List<AccountUser> getActiveAccountUsersByGroupIdAndEndDate( Integer groupId) throws CAPException{
		List<AccountUser> groupMembers = new ArrayList<>();
		try {
			groupMembers = iGroupUsersRepository.getActiveAccountUsersByGroupIdAndEndDate(groupId);
			
		} catch (Exception e) {
			LOGGER.error("Unable to get the group member ", e);
			throw  new CAPException(TeamError.ACCOUNT_USER_LIST_BY_GROUP_ID, null, e.getMessage(), Severity.LOW);
		}
		return groupMembers;
	}
	
	@Override
	public Map<Integer, String> getAccountUsersByLeadId( Integer leadId){
		Map<Integer, String> usersMap = new HashMap<>();
		try {
			List<AccountUser> groupMembers = iGroupUsersRepository.getAccountUsersByLeadId(leadId);
			usersMap = getUserIdAndNameMap(groupMembers);
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		return usersMap;
	}
	
	@Override
	public Map<Integer, String> getActiveAccountUsersByLeadId( Integer leadId){
		Map<Integer, String> usersMap = new HashMap<>();
		try {
			List<AccountUser> groupMembers = iGroupUsersRepository.getActiveAccountUsersByLeadIdAndEndDate(leadId);
			usersMap = getUserIdAndNameMap(groupMembers);
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		return usersMap;
	}
	
	private Map<Integer, String> getUserIdAndNameMap(List<AccountUser> users ) {
		if(users != null){
			Map<Integer, String> usersMap = new LinkedHashMap<>();
			for(AccountUser accountUser : users){
				usersMap.put(accountUser.getId(), accountUser.getFullName());
			}
		  return usersMap;
		}
		return Collections.emptyMap();
	}
	
	
	
	public Map<String, String> getAccountUsersByLeadIdNew( Integer leadId){
		Map<String, String> usersMap = new HashMap<>();
		try {
			List<AccountUser> groupMembers = iGroupUsersRepository.getAccountUsersByLeadId(leadId);
			usersMap = getUserIdAndNameMapNew(groupMembers);
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		return usersMap;
	}
	
	
	
	private Map<String, String> getUserIdAndNameMapNew(List<AccountUser> users ) {
		if(users != null){
			Map<String, String> usersMap = new LinkedHashMap<>();
			for(AccountUser accountUser : users){
				usersMap.put(accountUser.getId()+"", accountUser.getFullName());
			}
		  return usersMap;
		}
		return Collections.emptyMap();
	}
	
	

	@Override
	public AccountUser getUserGroupLead(Integer userId) {
		
		AccountUser user = null;
		try{
			user = groupRepository.getUserGroupLead(userId);
		}catch (Exception e) {
			LOGGER.error("Unable to find the groupLead ", e);
		}
		return user;
	}
	
	@Override
	public boolean doesGroupExist(String teamName){
		Long count = groupRepository.doesGroupExist(teamName);
		boolean teamExists = false;
		if(count > 0){
			teamExists = true;
		}
		return teamExists;
	}

	@Override
	@Transactional
	public CapTeam addGroup(String name, Integer parentTeamId,
			Integer groupLeadUserId, String startDate) throws CAPException {

		CapTeam newGroup = new CapTeam();
		CapTeamLeaders teamLeader = new CapTeamLeaders();
		CapTeam addedGroup = null;
		newGroup.setGroupName(name);
		newGroup.setGroupLeadUserId(groupLeadUserId);
		try {
			if (StringUtils.isNotEmpty(startDate)) {

				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
				Date teamStartDate = null;
				teamStartDate = dateFormat.parse(startDate);
				newGroup.setStartDate(teamStartDate);
				teamLeader.setStartDate(teamStartDate);
			}

			if (null != parentTeamId) {
				newGroup.setParentId(parentTeamId);
			}
			
			
			addedGroup = groupRepository.save(newGroup);
			
			teamLeader.setGroupId(addedGroup.getId());
			teamLeader.setUserId(groupLeadUserId);
			teamLeadersRepository.save(teamLeader);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while saving: ", e);
			throw new CAPException(TeamError.ADD_ERROR, null, e.getMessage(),
					Severity.LOW);
		}

		return addedGroup;
	}
	
	@Override
	@Transactional
	public CapTeam editGroup(int teamId, String teamName, Integer managerId,String startDate,
			String endDate) throws CAPException {
		 
		CapTeam group = groupRepository.findById(teamId);
		if (group != null) {
			try {
				if (!StringUtils.isEmpty(teamName) && !group.getGroupName().equalsIgnoreCase(teamName)) {
					group.setGroupName(teamName);
					group.setLastTeamNameUpdate(new  Date());
				}
				// edit the team leader
				if (managerId != null) {
					CapTeamLeaders activeTeamLeader  =teamLeadersRepository.findByGroupIdAndUserIdAndEndDate(group.getId(),group.getGroupLeadUserId());
					if (null==activeTeamLeader) {
						LOGGER.error("Error while get active team lead of the team occurred for team id: "+group.getId() + "and lead id:"+managerId);
						return group;
					}
					Date leaderEndDate = null;
					Date newLeaderStartDate = null;
					
					if (StringUtils.isNotEmpty(endDate) && StringUtils.isNotEmpty(startDate) && !activeTeamLeader.getId().equals(managerId)) {
						DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
						leaderEndDate  = dateFormat.parse(endDate);
						activeTeamLeader.setEndDate(leaderEndDate);
						newLeaderStartDate = dateFormat.parse(startDate);
						teamLeadersRepository.save(activeTeamLeader);// save the old tema leader with end date
						CapTeamLeaders newTeamLeader = new CapTeamLeaders();
						newTeamLeader.setGroupId(group.getId());
						newTeamLeader.setStartDate(newLeaderStartDate);
						newTeamLeader.setUserId(managerId);
						teamLeadersRepository.save(newTeamLeader);// save the new team leader with start date and same team id
						
						group.setGroupLeadUserId(managerId);// save the new manager for the team
					}
					
				}
				groupRepository.save(group);// save the group
			} catch (Exception e) {
				LOGGER.error("Exception occurred while updating group: ", e);
				throw new CAPException(TeamError.EDIT_ERROR, null,
						e.getMessage(), Severity.LOW);
			}
			
			

		}

		return group;
	}
	
	@Override
	public List<CapTeamMemberDto> getTeamMembersByGroupId(Integer groupId) {
		List<CapTeamMemberDto> groupMembers = new ArrayList<CapTeamMemberDto>();
		try {
			List<Object[]> teamMembersInfo = iGroupUsersRepository.findByGroupIdAndEndDate(groupId);
			return getCapTeamMemberDto(teamMembersInfo);
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		
		return groupMembers;
	}
	
	private List<CapTeamMemberDto> getCapTeamMemberDto(List<Object[]> teamMembersInfo) {
		List<CapTeamMemberDto> teamMemberDtoList =  new ArrayList<CapTeamMemberDto>();
		if(null!=teamMembersInfo && teamMembersInfo.size()>0){
			for (Object[] memberInfo : teamMembersInfo) {
				CapTeamMemberDto teamMemberDto = new CapTeamMemberDto();
				 BigDecimal userId = (BigDecimal)memberInfo[0];
				teamMemberDto.setUserId(userId.intValue());
				teamMemberDto.setUserName((String)(memberInfo[1]+ "  " + memberInfo[2]));
				if(null!=memberInfo[3]){
					String date = new SimpleDateFormat("MM-dd-yyyy").format(memberInfo[3]);	
					teamMemberDto.setStartDate(date);
				}
				
				if(null!=memberInfo[4]){
					String date = new SimpleDateFormat("MM-dd-yyyy").format(memberInfo[4]);	
					teamMemberDto.setEndDate(date);
				}
				
				teamMemberDtoList.add(teamMemberDto);
			}
		}
		return teamMemberDtoList;
	}

	@Override
	public List<CapTeamMemberDto> getAvailableCsrUsersWithDate(){
		List<CapTeamMemberDto> availableCsrs = new ArrayList<CapTeamMemberDto>();
		try {
			List<AccountUser> users = iGroupUsersRepository.getAvailableCSRUser();
			for (AccountUser accountUser : users) {
				CapTeamMemberDto teamMemberDto = new CapTeamMemberDto();
				teamMemberDto.setUserId(accountUser.getId());
				teamMemberDto.setStartDate(null);
				teamMemberDto.setEndDate(null);
				teamMemberDto.setUserName(accountUser.getFirstName()+" "+accountUser.getLastName());
				availableCsrs.add(teamMemberDto);
			}
			List<Object[]> oldTeamMembers =null;
			if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
				BigDecimal tenantId = new BigDecimal(TenantContextHolder.getTenant().getId());
				oldTeamMembers  = iGroupUsersRepository.getAvailableCSRUsersWithEndDate(tenantId);
		        
			}
			else{
				oldTeamMembers  = iGroupUsersRepository.getAvailableCSRUsersWithEndDate();
			}
			List<CapTeamMemberDto> csrsFromTeamMembers = new ArrayList<CapTeamMemberDto>();
			csrsFromTeamMembers = getCapTeamMemberDto(oldTeamMembers);
			if(null!=csrsFromTeamMembers && csrsFromTeamMembers.size()>0){
				availableCsrs.addAll(getCapTeamMemberDto(oldTeamMembers));
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		
		return availableCsrs;
	}
	
	@Override
	public CapTeamLeaders getTeamLeaderInfo(Integer groupId, Integer leadId){
		CapTeamLeaders teamLeader = new CapTeamLeaders();
		try {
			teamLeader = teamLeadersRepository.findByGroupIdAndUserIdAndEndDate(groupId, leadId);
		} catch (Exception e) {
			LOGGER.error("Unable to get the group ", e);
		}
		
		return teamLeader;
	}
	
	@Override
	public CapTeamMemberDto getTeamMemberInfo(Integer teamMemberId){
		
		CapTeamMemberDto teamMemberDto =null;
		try {
			List<Object[]> teamMemberInfo = iGroupUsersRepository.findTeamMemberByUserId(teamMemberId);
			if(null!=teamMemberInfo && teamMemberInfo.size()>0){
				teamMemberDto = mapToTeamMemberDto(teamMemberInfo.get(0));
			}
		} catch (Exception e) {
			LOGGER.error("Error while mapping team info for the team member with id: "+teamMemberId+ " "+ e);
					
		}
		return teamMemberDto;
	}

	private CapTeamMemberDto mapToTeamMemberDto(Object[] teamMemberInfo) {
		CapTeamMemberDto teamMemberDto = null;
		if (null != teamMemberInfo && teamMemberInfo.length > 0) {
			teamMemberDto = new CapTeamMemberDto();
			teamMemberDto.setTeamName((String)teamMemberInfo[0]);
			teamMemberDto
					.setTeamId((Integer)teamMemberInfo[1]);
			
			teamMemberDto
			.setTeamLeadName((String)teamMemberInfo[2]+" "+(String)teamMemberInfo[3]);
			teamMemberDto
			.setTeamLeadId((Integer)teamMemberInfo[4]);
			if (null != teamMemberInfo[6]) {
				DateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss");
				String teamLeadStartDate = null;
				try {
					teamLeadStartDate = dateFormat.format(teamMemberInfo[6]);
				} catch (Exception e) {
					LOGGER.error("Error while parsing start date of team lead for the given team member: "
							+ e);
				}

				teamMemberDto.setTeamLeadStartDate(teamLeadStartDate);
			}
			
			Integer userId = ((Integer) teamMemberInfo[7]);
			teamMemberDto.setUserId(userId);
		}
		return teamMemberDto;
	}

	@Override
	public String getMaximumDateForDeleteTeam(Integer teamMemberId) {

		Date maxDate = null;
		String maxDateString = null;
		List<Object[]> teamMemberMaxDates = iGroupUsersRepository
				.findMaxStartEndDateWithTeamMemberByTeamId(teamMemberId);
		List<Object[]> teamLeaderMaxDates = iGroupUsersRepository
				.findMaxStartEndDateWithTeamLeaderByTeamId(teamMemberId);

		if (null != teamLeaderMaxDates && teamLeaderMaxDates.size() > 0) {
			Object[] array = teamLeaderMaxDates.get(0);
			for (int i = 0; i < array.length; i++) {
				if (null != array[i]) {
					Date temp = (Date) array[i];
					if (null == maxDate) {
						maxDate = temp;
					}
					if (maxDate.before(temp)) {
						maxDate = temp;
					}
				}
			}
		}

		if (null != teamMemberMaxDates && teamMemberMaxDates.size() > 0) {
			Object[] array = teamMemberMaxDates.get(0);
			for (int i = 0; i < array.length; i++) {
				if (null != array[i]) {
					Date temp = (Date) array[i];
					if (null == maxDate) {
						maxDate = temp;
					}
					if (maxDate.before(temp)) {
						maxDate = temp;
					}
				}
			}
		}

		if (null!=maxDate) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			try {
				maxDateString = dateFormat
						.format(maxDate);
			} catch (Exception e) {
				LOGGER.error("Error while parsing start date of team lead for the given team member: "
						+ e);
			}
		}

	return maxDateString;
	
	}
		
	@Override
	public Map<Integer, CapTeamMemberDto> getTeamMembersInfo(List<Integer> userIds) {
		Map<Integer, CapTeamMemberDto> mapCapTeamDto = new HashMap<>();
		
		List<Object[]> userList = iGroupUsersRepository.findTeamMembersByUserIds(userIds);
		for (Object[] userData : userList) {
			CapTeamMemberDto capTeamMemberDto = mapToTeamMemberDto(userData);
			mapCapTeamDto.put(capTeamMemberDto.getUserId(), capTeamMemberDto);
		}
		
		return mapCapTeamDto;
	}
	
	@Override
	public boolean updateGroupMembersWithDate(Integer groupId, List<Integer> userIds,String teamMemberDate) throws CAPException {
		boolean bStatus = false;
		List<Integer> newUserList = userIds;
		
		try {
			if(!groupRepository.exists(groupId)) {
				LOGGER.error("Invalid Group groupId , not available in the DB: " + groupId);
				return bStatus;
			}

			List<Integer> existingUsers = iGroupUsersRepository.findUsersByGroupId(groupId);
			for (Iterator<Integer> iterator = newUserList.iterator(); iterator.hasNext();) {
				Integer userId = iterator.next();
				
				if(existingUsers.contains(userId)) {
					existingUsers.remove(existingUsers.indexOf(userId));
					iterator.remove(); // remove from the new user list
				}				
			}
			
				Date teamDate = null;
				
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
				teamDate = dateFormat.parse(teamMemberDate);
				
			
			
			//Users to be deleted
			if(!existingUsers.isEmpty()) {
				iGroupUsersRepository.deleteTeamMemberByEndDate(existingUsers, teamDate);
				bStatus=true;
			}
			
			//new users to be added
			if(!newUserList.isEmpty()) {
				bStatus = addNewUsersToGroupWithStartDate(groupId, newUserList,teamDate);	
			}
			
			////update group with updated date.
			if(bStatus){
				CapTeam capTeam = groupRepository.findById(groupId);
				capTeam.setUpdated(new TSDate());
				groupRepository.save(capTeam);
			}
			
			if(existingUsers.isEmpty() && newUserList.isEmpty()) {
				bStatus = true;
			}
		} catch (Exception e) {
			LOGGER.error("Cannnot update GroupUser", e);
			bStatus = false;
			throw  new CAPException(TeamError.UPDATE_GROUP_MEMBERS, null, e.getMessage(), Severity.LOW);
		}
		
		return bStatus;
	}
	
	@Override
	public List<Integer> findActiveUsersInGroup(Integer groupId){
		List<Integer> userIds = null;
		if(null!=groupId){
			userIds = iGroupUsersRepository.findUsersByGroupId(groupId);
		}
		return userIds;	 
	}

	@Override
	public CapTeamMembers findActiveUsersByUserId(Integer userId){
		List<CapTeamMembers> capTeamMembers = null;
		CapTeamMembers capTeamMember = null;
		if(null!=userId){
			capTeamMembers = iGroupUsersRepository.findActiveTeamMemberByUserId(userId);
			if(null!=capTeamMembers && capTeamMembers.size()>0){
				capTeamMember = capTeamMembers.get(0);
			}
				
		}
		return capTeamMember;	 
	}
	
	@Override
	public Date getMaxEndDateForUser(Integer userId) {
		Date maxDate = null;
		List<Object[]> dateList =iGroupUsersRepository.findMaxStartEndDateForTeamMemberById(userId);

		Object[] data = ((dateList !=null) ? dateList.get(0) : null);
		Date startDate = (Date) ((data !=null) ? data[0] : null);
		Date endDate   = (Date) ((data !=null) ? data[1] : null);

		if(startDate == null) {
			maxDate = endDate;
		} else if(endDate == null) {
			maxDate = startDate;
		} else if(startDate !=null && endDate !=null) {
			maxDate = (DateTimeComparator.getDateOnlyInstance().compare(startDate, endDate) < 0) ? endDate : startDate;
		}
		
		return maxDate;
	}

}
