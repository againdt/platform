package com.getinsured.hix.platform.cache;

import java.io.Serializable;
import java.util.List;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface CacheableObject extends Serializable {
	
	/**
	 * 
	 * ObjectKey acts as the cache region where the object needs to be cached
	 */
	 String getObjectKey();
	
	/**
	 * 
	 * Key used to store the object in cache
	 */
	String getKey();
	

}
