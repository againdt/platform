package com.getinsured.hix.platform.interceptors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

public class GhixLocaleChangeInterceptor extends LocaleChangeInterceptor {
  /**
   * Default name of the locale specification parameter: "lang".
   */
  private static final String DEFAULT_PARAM_NAME = "lang";

  private String paramName = DEFAULT_PARAM_NAME;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws ServletException {

		boolean enableMultiLanguage = "Y".equalsIgnoreCase(
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ENABLEMULTILANGUAGE)
		);

		if (!enableMultiLanguage) {
			return true;
		}

		String newLocale = request.getParameter(this.paramName);

		if (newLocale != null) {
			LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
			if (localeResolver == null) {
				throw new IllegalStateException("No LocaleResolver found: not in a DispatcherServlet request?");
			}
			localeResolver.setLocale(request, response, StringUtils.parseLocaleString(newLocale));
		}

		return true;
	}

  /**
   * Set the name of the parameter that contains a locale specification
   * in a locale change request. Default is "locale".
   */
  public void setParamName(String paramName) {
    this.paramName = paramName;
  }

  /**
   * Return the name of the parameter that contains a locale specification
   * in a locale change request.
   */
  public String getParamName() {
    return this.paramName;
  }
}
