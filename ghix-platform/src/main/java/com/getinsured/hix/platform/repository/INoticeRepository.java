package com.getinsured.hix.platform.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;

public interface INoticeRepository extends JpaRepository<Notice, Integer> {

	List<Notice> findByKeyNameAndKeyId(@Param("keyName") String keyName, @Param("keyId") Integer keyId);

	@Query("FROM Notice as notice where notice.printable = (:flag) and notice.created >= :startDate and notice.created <= :endDate ")
	List<Notice> getNoticeByPrintableAndCreated(@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("flag") String flag);


	@Modifying
	@Transactional
	@Query("update Notice  "
			+ "set sentDate = :sentDate, "
			+ "status = :status, "
			+ "ecmId = :ecmId, "
			+ "printable = :printable, "
			+ "updated = :updated "
			+ "where id = :noticeId ")
	int updateNotice(@Param("noticeId") int noticeId,
			@Param("sentDate") Date sentDate,
			@Param("status") STATUS status,
			@Param("ecmId") String ecmId,
			@Param("printable") String printable,
			@Param("updated") Date updated
			);

	Notice findById(Integer id);

}