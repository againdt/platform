package com.getinsured.hix.platform.config;

import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.boot.actuate.health.Status;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.getinsured.hix.platform.security.GhixRestTemplate;

public class DependenciesHealthIndicator extends AbstractHealthIndicator{

	private static final String ERROR_READING_CONFIGURATION_PROPERTIES_FILE = "Error reading configuration.properties file";
	private static final String NOT_SPECIFIED = "not specified or none";
	private static final String UNKNOWN_SERVICE_NOT_CONFIGURED_IN_ACTUATOR_CONFIG = "unknown service, not configured in ActuatorConfig";
	private static final String DEPENDENCIES = "dependencies";
	private static final String ACTUATOR_INFO = "actuator/info";

	private Properties configProp;
	private GhixRestTemplate ghixRestTemplate;
	private HashMap<String, String> dependenciesUrlMap;


	public DependenciesHealthIndicator(Properties configProp, HashMap<String, String> dependenciesMap,
			GhixRestTemplate ghixRestTemplate) {

		this.configProp = configProp;
		this.ghixRestTemplate = ghixRestTemplate;
		this.dependenciesUrlMap = new HashMap<>();
		
		Set<String> endpoints = dependenciesMap.keySet();
		for (String endpoint : endpoints) {
			String svcUrl = configProp.getProperty(dependenciesMap.get(endpoint));
			if (null != svcUrl){
				dependenciesUrlMap.put(endpoint, svcUrl + ACTUATOR_INFO);
			} else {
				dependenciesUrlMap.put(endpoint, null);
			}
		}
	}

	@Override
	protected void doHealthCheck(Builder builder) throws Exception {
		if (this.dependenciesUrlMap == null || this.dependenciesUrlMap.size() == 0 ){
			builder.up().withDetail(DEPENDENCIES, NOT_SPECIFIED);
		} else if (this.configProp == null || this.configProp.size() == 0 ){
			builder.down().withDetail(DEPENDENCIES, ERROR_READING_CONFIGURATION_PROPERTIES_FILE);
		} else {
			doDependenciesHealthCheck(builder);
		}
	}
	
	
	private void doDependenciesHealthCheck(Builder builder) {

		Set<String> endpoints = dependenciesUrlMap.keySet();
		final HashMap<String, Node> result = new HashMap<String, Node>();
		
		boolean allUp = true;

		for (String endpoint : endpoints) {

			ResponseEntity<String> responseEntity;
			Node t;

			String svcUrl = dependenciesUrlMap.get(endpoint);
			
			if (StringUtils.isNotBlank(svcUrl)){
				try {
					responseEntity = ghixRestTemplate.exchange(svcUrl, null,
							HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);

					if (HttpStatus.OK == responseEntity.getStatusCode()){
						t = new Node(Status.UP.getCode(), svcUrl, null);
					} else {
						allUp = false;
						t = new Node(Status.DOWN.getCode(), svcUrl, null);
					}
				} catch (Exception e) {
					allUp = false;
					t = new Node(Status.DOWN.getCode(), svcUrl, e.getMessage());
				}
			} else {
				allUp = false;
				t = new Node(Status.UNKNOWN.getCode(), svcUrl, UNKNOWN_SERVICE_NOT_CONFIGURED_IN_ACTUATOR_CONFIG);
			}
			result.put(endpoint, t);
		}

		if (!allUp){
			builder.down().withDetail(DEPENDENCIES, result);
		} else {
			builder.up().withDetail(DEPENDENCIES, result);
		}

	}

	@JsonInclude(Include.NON_NULL)
	private static class Node{
		private String url;
		private String status;
		private String exception;

		public Node(String status, String url, String exception) {
			this.status = status;
			this.url = url;
			this.exception = exception;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getException() {
			return exception;
		}

		public void setException(String exception) {
			this.exception = exception;
		}

	}

}
