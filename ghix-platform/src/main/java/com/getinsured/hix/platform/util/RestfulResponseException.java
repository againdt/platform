package com.getinsured.hix.platform.util;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Defines RESTful Exception Response that we are getting from SVC layer.
 *
 * @author Yevgen Golubenko
 * @since 10/21/17
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestfulResponseException extends RuntimeException {
  @JsonProperty
  private Map<String, Object> details;

  @JsonProperty("stackTrace")
  private List<String> stack;

  @JsonProperty
  private String message;

  @JsonProperty
  private String url;

  @JsonProperty
  private String logId;

  @JsonProperty
  private String tag;

  @JsonProperty
  private int httpStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();

  @JsonProperty
  private Map<String, String[]> parameters;

  /**
   * Constructor with simple message.
   *
   * @param message exception message.
   */
  public RestfulResponseException(String message)
  {
    super(message);
  }

  /**
   * Constructor that takes another exception and
   * wraps it in {@link RestfulResponseException}
   *
   * @param ex exception to wrap.
   */
  public RestfulResponseException(Throwable ex)
  {
    super(ex);
  }

  /**
   * Constructor takes message and wraps given exception.
   *
   * @param message exception message.
   * @param ex another exception.
   */
  public RestfulResponseException(String message, Throwable ex)
  {
    super(message, ex);
  }

  /**
   * Returns details of the exception if any.
   *
   * @return {@code Map} of details.
   */
  public Map<String, Object> getDetails()
  {
    return details;
  }

  public void setDetails(final Map<String, Object> details)
  {
    this.details = details;
  }

  /**
   * List of stack trace lines.
   *
   * @return {@code List} that contains stack trace lines.
   */
  public List<String> getStack()
  {
    return stack;
  }

  public void setStack(final List<String> stack)
  {
    this.stack = stack;
  }

  /**
   * Returns message associated with this exception.
   *
   * @return Exception message.
   */
  public String getMessage()
  {
    return message;
  }

  public void setMessage(final String message)
  {
    this.message = message;
  }

  /**
   * Returns the backend URL where this exception happened.
   *
   * @return Backend URL where this exception happened.
   */
  public String getUrl()
  {
    return url;
  }

  public void setUrl(final String url)
  {
    this.url = url;
  }

  /**
   * Returns {@code HTTP} status associated with this exception.
   * <p>
   *   It is advisable that you check to make sure the status code
   *   and act accordingly.
   * </p>
   * <p>
   *   Generally if you don't want to deal with every possible {@code HTTP} status
   *   code, you can do something like this:
   *     <pre>
   * if({@link HttpStatus#is4xxClientError()}) {
   *    // handle 400 errors
   * } else if({@link HttpStatus#is5xxServerError()}) {
   *    // handle 500 errors
   * } else {
   *    // something else
   * }
   *     </pre>
   * </p>
   * @return {@code HTTP} status code.
   */
  public HttpStatus getHttpStatus()
  {
    return HttpStatus.valueOf(httpStatus);
  }

  public void setHttpStatus(final int httpStatus)
  {
    this.httpStatus = httpStatus;
  }

  /**
   * Returns {@code Map} of URL {@code GET} parameters that backend recieved
   * when this exception happened.
   *
   * @return {@code Map} of {@code GET} parameters.
   */
  public Map<String, String[]> getParameters()
  {
    return parameters;
  }

  public void setParameters(final Map<String, String[]> parameters)
  {
    this.parameters = parameters;
  }

  /**
   * Returns unique log id associated with this exception.
   * <p>
   *   When exception occurs on the backend, there is unique log id
   *   that is generated automatically and added to the exception message.
   *   It is done, so that you can use Splunk or any other tools (unix grep for example)
   *   to quickly find line of this exception in the logs.
   * </p>
   * @return unique log id.
   */
  public String getLogId()
  {
    return logId;
  }

  public void setLogId(String logId)
  {
    this.logId = logId;
  }

  /**
   * GetInsured error tag.
   * <p>
   *   If this exception is tagged, this will return you the tag.
   * </p>
   * @return error tag.
   */
  public String getTag()
  {
    return tag;
  }

  public void setTag(String tag)
  {
    this.tag = tag;
  }
}
