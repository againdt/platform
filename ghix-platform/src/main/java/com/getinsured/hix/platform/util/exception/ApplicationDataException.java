package com.getinsured.hix.platform.util.exception;

/**
 * Created by ivanbastidas on 3/20/14.
 */
public class ApplicationDataException extends Exception {

    public ApplicationDataException(Throwable cause) {
        super("Error: Malformed application data", cause);
    }

    public ApplicationDataException(String message, Throwable cause) {
        super(String.format("Error: Malformed application data, %s", message), cause);
    }

    public ApplicationDataException(String message) {
        super(String.format("Error: Malformed application data, %s", message));
    }
}
