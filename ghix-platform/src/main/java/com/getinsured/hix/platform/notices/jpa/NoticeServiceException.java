package com.getinsured.hix.platform.notices.jpa;

/**
 * Thrown to indicate that a method has failed while processing Notice Service request.
 *
 */
public class NoticeServiceException extends Exception {
	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1053471180051791761L;

	public NoticeServiceException() {
		super();
	}

	public NoticeServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoticeServiceException(String message) {
		super(message);
	}

	public NoticeServiceException(Throwable cause) {
		super(cause);
	}
}
