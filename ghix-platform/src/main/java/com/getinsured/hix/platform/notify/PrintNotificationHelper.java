package com.getinsured.hix.platform.notify;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notify.NotificationDto;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component("printNotificationHelperNew")
public class PrintNotificationHelper {

	private static final String HOST = "{host}";
	private static final String ERROR_CREATING_CONTENT_IN_ECM = "Error creating content in ECM - ";
	private static final String ERROR_CREATING_PDF_USING_FLYING_SAUCER = "Error creating PDF using flying saucer - ";
	private static final String NOTIFICATION_DTO_ERROR = "Either userFullName or location object or ecmRelativePath or ecmFileName missing!";
	private static final String PRINTABLE = "Y";
	private static final String OMIT = "omit";
	private static final String UTF_8 = "UTF-8";

	@Autowired private ContentManagementService ecmService;
	@Autowired private NoticeRepository noticeRepo;

	public Notice generatePrintNotice(NotificationDto printNoticeDto, Notice noticeObj) {
		if (printNoticeDto != null && printNoticeDto.isPrintable()){
			byte[] pdfBytes = null;
			try {
				pdfBytes = generatePdf(noticeObj.getEmailBody());
			} catch (IOException | NoticeServiceException e) {
				throw new GIRuntimeException(ERROR_CREATING_PDF_USING_FLYING_SAUCER + e);
			}
	
			String ecmRelativePath = printNoticeDto.getEcmRelativePath();
			String ecmFileName = printNoticeDto.getEcmFileName();
	
			String ecmDocId = null;
			try {
				ecmDocId = ecmService.createContent(ecmRelativePath, ecmFileName, pdfBytes, ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.NOTICE, null);
			} catch (ContentManagementServiceException e) {
				throw new GIRuntimeException(ERROR_CREATING_CONTENT_IN_ECM + e);
			}
			noticeObj.setEcmId(ecmDocId);
			noticeObj.setPrintable(PRINTABLE);
			noticeObj = noticeRepo.update(noticeObj);
		}
		return noticeObj;
	}

	private byte[] generatePdf(String incomingData) throws IOException, NoticeServiceException {

		/** Replace resources path (images, css, etc) with the full actual host name */
		String finalData = new String(StringUtils.replace(formData(incomingData), HOST, GhixPlatformEndPoints.GHIXWEB_SERVICE_URL));


		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try{
			/** If resources (images, css, etc) are not found,
			 * then ITextRenderer logs "<strong>java.io.IOException: Stream closed</strong>" exception without throwing it
			 * So we can't catch such exception thrown by 3rd party jar
			 *
			 * Also, stack trace gets printed in the PDF on Alfresco
			 *
			 */
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(finalData);
			renderer.layout();
			renderer.createPDF(os);
		}catch(Exception e){
			throw new NoticeServiceException(e);
		}
		finally{
			os.close();
		}

		return os.toByteArray();
	}

	private String formData(String data) throws IOException {

		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType(OMIT);
		tidy.setXHTML(true);
		tidy.setInputEncoding(UTF_8);
		tidy.setOutputEncoding(UTF_8);
		tidy.parse(new StringReader(data), writer);
		writer.close();
		return writer.toString();
	}

	public void validateNotificationData(NotificationDto notificationDto) {
		if (notificationDto != null){

			boolean isLocationObjValid = validateLocationObject(notificationDto);
			boolean isEcmPathsValid = validateEcmPreData(notificationDto);

			if (!isLocationObjValid || !isEcmPathsValid){
				throw new GIRuntimeException(NOTIFICATION_DTO_ERROR);
			}
		}
	}

	private boolean validateLocationObject(NotificationDto notificationDto) {
		return notificationDto != null ? notificationDto.validateAddressDetails() : false;
	}

	private boolean validateEcmPreData(NotificationDto notificationDto) {
		return notificationDto != null ? notificationDto.validateEcmPreData() : false;
	}

}