package com.getinsured.hix.platform.gimonitor.service;

import com.getinsured.hix.model.GIErrorCode;

public interface GIErrorCodeService {

	GIErrorCode saveOrUpdateGIErrorCode(GIErrorCode giErrorCode);
	
	GIErrorCode getGIErrorCodeByErrorCode(String errorCode);
}
