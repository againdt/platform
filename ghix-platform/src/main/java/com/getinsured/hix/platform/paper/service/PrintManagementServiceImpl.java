package com.getinsured.hix.platform.paper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.file.manager.FileManagerService;
import com.getinsured.hix.platform.paper.PrintManagementService;

@Service("printManagementService")
public class PrintManagementServiceImpl implements PrintManagementService {

	@Autowired private FileManagerService fileManagerService;

	@Override
	public boolean postPrintMail(byte[] contentData, String filename,String floderName) {
		return fileManagerService.uploadFile(filename, contentData,floderName);
	}

}
