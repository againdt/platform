package com.getinsured.hix.platform.couchbase.service;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.clamav.ClamScan;
import com.getinsured.hix.platform.clamav.ScanResult;
import com.getinsured.hix.platform.couchbase.CouchBaseUtil;
import com.getinsured.hix.platform.couchbase.CouchbaseConnectionManager;
import com.getinsured.hix.platform.couchbase.dto.CouchBinaryDocument;
import com.getinsured.hix.platform.couchbase.dto.CouchDocument;
import com.getinsured.hix.platform.couchbase.dto.CouchMeta;
import com.getinsured.hix.platform.couchbase.helper.CouchbaseSupport;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ContentManagementService.ErrorCodes;
import com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument;

@Service("couchBucketService")
@DependsOn({ "couchDocumentHelper","couchbaseConnectionManager", "binaryCouchSupport", "nonBinaryCouchSupport" , "clamScan"})
@Lazy
public class CouchBucketService {
	@SuppressWarnings("unused")
	private static final String SUB_CATEGORY_CANNOT_BE_EMPTY = "Sub-category Cannot be empty";

	private static final String CATEGORY_CANNOT_BE_EMPTY = "Category Cannot be empty";

	private static final Logger LOGGER = Logger.getLogger(CouchBucketService.class);

	private static final String BIN = "BIN";

	private static final String NON_BINARY_BUCKET_NOT_AVAILABLE = "Non-Binary Bucket not available";

	private static final String BINARY_BUCKET_NOT_AVAILABLE = "Binary Bucket not available";

	private static final String CB_DOC_ID_SEPARATOR = "::";
	
	@Autowired
	private CouchbaseConnectionManager couchbaseConnectionManager;

	@Autowired
	@Qualifier("binaryCouchSupport")
	private CouchbaseSupport binary;

	@Autowired
	@Qualifier("nonBinaryCouchSupport")
	private CouchbaseSupport nonBinary;
	
	@Autowired private ClamScan clamScan;
	
	@Value("#{configProp['clamav.enabled']}")
	private String clamAVEnabled;
	
	@PostConstruct
	public void createBuckets() throws ContentManagementServiceException {

		if (null != couchbaseConnectionManager.getBinaryBucket()) {
			binary.setBucket(couchbaseConnectionManager.getBinaryBucket());
		} else {
			LOGGER.warn(BINARY_BUCKET_NOT_AVAILABLE);
		}

		if (null != couchbaseConnectionManager.getNonbinaryBucket()) {
			nonBinary.setBucket(couchbaseConnectionManager.getNonbinaryBucket());
		} else {
			LOGGER.warn(NON_BINARY_BUCKET_NOT_AVAILABLE);
		}
			
	}

	public CouchbaseSupport getBinary() {
		return binary;
	}

	public void setBinary(CouchbaseSupport binary) {
		this.binary = binary;
	}

	public CouchbaseSupport getNonBinary() {
		return nonBinary;
	}

	public void setNonBinary(CouchbaseSupport nonBinary) {
		this.nonBinary = nonBinary;
	}
	
	public static String documentId(CouchMeta meta) {

		StringBuilder id = new StringBuilder().append(meta.getCategory() + CB_DOC_ID_SEPARATOR);

		if (StringUtils.isNotBlank(meta.getSubCategory())){
			id.append(meta.getSubCategory() + CB_DOC_ID_SEPARATOR);
		}

		if (StringUtils.isNotBlank(meta.getCustomKey())){
			id.append(meta.getCustomKey());
		} else {
			id.append(UUID.randomUUID().toString());
		}

		return id.toString();
	}

	/*public static String documentId(CouchMeta meta) {
		return UUID.randomUUID().toString() + CB_DOC_ID_SEPARATOR + meta.getCategory() + CB_DOC_ID_SEPARATOR
				+ meta.getSubCategory();
	}*/

	public static String binaryContentId(String documentId) {
		return documentId + CB_DOC_ID_SEPARATOR + BIN;
	}

	public static String binaryDocumentId(String documentId, int partNumber) {
		/**
		 * purposely not appended part number for first document to support
		 * already migrated document without split feature
		 */
		return ((partNumber == 0) ? documentId : documentId + CB_DOC_ID_SEPARATOR + partNumber);
	}

	public String createDocument(String document, String id) {
		return createDocument(nonBinary, document, id);
	}

	public <T extends CouchDocument> String createDocument(T document) {
		validateAndSetId(document);
		return createDocument(nonBinary, document, document.getId());
	}

	private <T extends CouchDocument> void validateAndSetId(T document) {
		validateDocument(document.getMetaData());
		document.setId(documentId(document.getMetaData()));
		if (document.getBinaryMetaData() != null && document.getBinaryMetaData().isHasContent()) {
			document.getBinaryMetaData().setContentLink(binaryContentId(documentId(document.getMetaData())));
		}
	}

	private <T extends CouchDocument> void validateDocument(CouchMeta meta) {
		if (StringUtils.isEmpty(meta.getCategory())) {
			throw new IllegalArgumentException(CATEGORY_CANNOT_BE_EMPTY);
		}
		/*if (StringUtils.isEmpty(meta.getSubCategory())) {
			throw new IllegalArgumentException(SUB_CATEGORY_CANNOT_BE_EMPTY);
		}*/
	}

	private <T> String createDocument(CouchbaseSupport bucket, T document, String id) {
		return bucket.create(document, id);
	}

	public String[] createBinaryDocument(byte[] data, String id) throws ContentManagementServiceException {
		
		if(clamAVEnabled != null && clamAVEnabled.compareToIgnoreCase("true") == 0){
			if(clamScan != null){
		
				ScanResult result = clamScan.scan(data);
				
				if(result != null && result.getStatus() != null && result.getStatus().name() != null){
					if(result.getStatus().name().equals("FAILED")){
						throw new ContentManagementServiceException("Cannot Upload: File is infected :" + result.getSignature(), ErrorCodes.VIRUS_INFECTED_FILE.getErrorCode());
					}else if(result.getStatus().name().equals("ERROR")){
						LOGGER.error("Error in doing anti-virus scan : " + result.getResult());
						String exMessage = null ;
						if(result.getException() != null){
							exMessage = result.getException().getMessage();
						}
						throw new ContentManagementServiceException("Error in doing ClamAV scan:" + exMessage,  ErrorCodes.ERROR_IN_SCAN.getErrorCode());
					}
				}
			}else{
				throw new ContentManagementServiceException("clamScan object not found,  possibly a configuration issue", ErrorCodes.ERROR_CLAM_CONFIG.getErrorCode());
			}
		}
		
		final byte[][] byteDataArray = CouchBaseUtil.splitBytes(data);
		final int numberOfDocs = byteDataArray.length;
		final String[] docIds = new String[numberOfDocs];
		
		
		for (int i = 0; i < numberOfDocs; i++) {
			docIds[i] = createBinaryDocument(binary, byteDataArray[i], binaryDocumentId(id, i));
		}
		return docIds;
	}

	private String createBinaryDocument(CouchbaseSupport bucket, byte[] data, String id) {
		CouchBinaryDocument binaryDocument = new CouchBinaryDocument();
		binaryDocument.setData(data);
		return bucket.createBinary(binaryDocument, id);
	}

	public <T> T getDocumentById(String id, Class<T> type) {
		return getDocumentById(nonBinary, id, type);
	}

	public byte[] getBinaryDocumentById(String contentId) {
		byte[] data = null;
		CouchEcmDocument document = getDocumentById(contentId, CouchEcmDocument.class);
		if (document != null) {
			final int numberOfParts = document.getBinaryMetaData().getNumberOfParts();
			byte[] partData = null;
			data = new byte[document.getBinaryMetaData().getFileSize()];
			int startData = 0;
			for (int i = 0; i < numberOfParts; i++) {
				partData = getBinaryData(binaryDocumentId(document.getBinaryMetaData().getContentLink(), i));
				System.arraycopy(partData, 0, data, startData, partData.length);
				startData += partData.length;
			}
		}
		return data;
	}

	public byte[] getBinaryData(String id) {
		CouchBinaryDocument binaryDocument = getDocumentById(binary, id, CouchBinaryDocument.class);
		if (binaryDocument != null) {
			return binaryDocument.getData();
		} else {
			return null;
		}
	}

	private <T> T getDocumentById(CouchbaseSupport bucket, String id, Class<T> type) {
		return bucket.findById(id, type);
	}

	public void deleteDocument(String id) {
		deleteDocument(nonBinary, id);
	}

	public <T extends CouchDocument> void deleteBinaryDocument(T document) {
		for (int i = 0; i < document.getBinaryMetaData().getNumberOfParts(); i++) {
			deleteBinaryDocument(binaryDocumentId(document.getBinaryMetaData().getContentLink(), i));
		}
	}

	public void deleteBinaryDocument(String id) {
		deleteDocument(binary, id);
	}

	private void deleteDocument(CouchbaseSupport bucket, String id) {
		bucket.delete(id);
	}

	public String updateDocument(String document, String id) {
		return updateDocument(nonBinary, document, id);
	}

	public <T extends CouchDocument> String updateDocument(T document) {
		return updateDocument(nonBinary, document, document.getId());
	}

	private <T> String updateDocument(CouchbaseSupport bucket, T document, String id) {
		return bucket.update(document, id);
	}

	public <T extends CouchDocument> void updateBinaryDocument(T document, byte[] newContent) throws ContentManagementServiceException {
		
		if(clamAVEnabled != null && clamAVEnabled.compareToIgnoreCase("true") == 0){
			if(clamScan != null){
		
				ScanResult result = clamScan.scan(newContent);
				
				if(result != null && result.getStatus() != null && result.getStatus().name() != null){
					if(result.getStatus().name().equals("FAILED")){
						throw new ContentManagementServiceException("Cannot Upload: File is infected :" + result.getSignature(), ErrorCodes.VIRUS_INFECTED_FILE.getErrorCode());
					}else if(result.getStatus().name().equals("ERROR")){
						LOGGER.error("Error in doing anti-virus scan : " + result.getResult());
						String exMessage = null ;
						if(result.getException() != null){
							exMessage = result.getException().getMessage();
						}
						throw new ContentManagementServiceException("Error in doing ClamAV scan:" + exMessage, ErrorCodes.ERROR_IN_SCAN.getErrorCode());
					}
				}
			}else{
				throw new ContentManagementServiceException("clamScan object not found,  possibly a configuration issue", ErrorCodes.ERROR_CLAM_CONFIG.getErrorCode());
			}
		}
		
		validateDocument(document.getMetaData());
		deleteBinaryDocument(document);
		document.getBinaryMetaData().setFileSize(newContent.length);
		document.getBinaryMetaData()
				.setNumberOfParts(CouchBaseUtil.numberOfParts(document.getBinaryMetaData().getFileSize()));
		document.getBinaryMetaData().setContentLink(binaryContentId(documentId(document.getMetaData())));
		updateDocument(document);
		createBinaryDocument(newContent, document.getBinaryMetaData().getContentLink());
	}

	public String updateBinaryDocument(byte[] data, String id) {
		CouchBinaryDocument binaryDocument = new CouchBinaryDocument();
		binaryDocument.setData(data);
		return updateBinaryDocument(binary, binaryDocument, id);
	}

	private String updateBinaryDocument(CouchbaseSupport bucket, CouchBinaryDocument document, String id) {
		return bucket.updateBinary(document, id);
	}
	
	public <T> List<T> query(String statement, Class<T> type){
		return nonBinary.query(statement, type);
	}
}
