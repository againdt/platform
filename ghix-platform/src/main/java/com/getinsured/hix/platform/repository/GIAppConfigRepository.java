package  com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.GIAppProperties;
@Repository(value="gIAppConfigRepository")
public interface GIAppConfigRepository extends TenantAwareRepository<GIAppProperties, Integer> {

	GIAppProperties findByPropertyKey(String propertyName);
	
	GIAppProperties findByPropertyKeyAndTenantId(String propertyName, Long tenantId);

	List<GIAppProperties> findAllByPropertyKeyStartingWith(String propertyName);
	
	List<GIAppProperties> findAllByPropertyKeyStartingWithAndTenantId(String propertyName, Long tenantId);

	@Query(nativeQuery= true, value="INSERT"+
			" INTO GI_APP_CONFIG"+
			" ("+
				" ID,"+
				" PROPERTY_KEY,"+
				" PROPERTY_VALUE,"+
				" DESCRIPTION,"+
				" CREATED_BY,"+
				" CREATION_TIMESTAMP,"+
				" LAST_UPDATED_BY,"+
				" LAST_UPDATE_TIMESTAMP,"+
				" TENANT_ID"+
			" )"+
			" ("+
				" SELECT GI_APP_CONFIG_SEQ.nextval,"+
				" gi.PROPERTY_KEY,"+
				" gi.PROPERTY_VALUE,"+
				" gi.DESCRIPTION,"+
				" NULL,"+
				" CURRENT_TIMESTAMP,"+
				" NULL, "+
				" CURRENT_TIMESTAMP, :tenantId"+
				" FROM GI_APP_CONFIG gi, tenant t"+
				" where t.name = 'GetInsured'"+
				" AND gi.TENANT_ID = t.ID"+
			" )"
				)
	void addConfigPropertiesForTenant(@Param("tenantId") Long tenantId);
}
