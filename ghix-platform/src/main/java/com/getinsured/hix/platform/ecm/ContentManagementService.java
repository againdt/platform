package com.getinsured.hix.platform.ecm;

import java.util.List;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.dto.platform.ecm.MetadataElement;

/**
 * Interface to talk to GHIX ECM.
 *
 * @author Romin, Ekram
 *
 */
public interface ContentManagementService {




	/**
	 * Creates a single content under a given relative path.
	 * <p><ul>
	 * <li><strong> relativePath should not start with /(slash)
	 * <li><strong> relativePath should be separated by / only. Please mind the /
	 * <li><strong> relativePath should not start with ECM Base Path configured in configuration.proprties
	 * <ul><p>
	 * <p> relative path is created dynamically and recursively if doesn't exists. relative path will be created in lower case only irrespective of passed arguments.
	 *
	 * <p><ul>
	 * <li><strong>fileName should have valid extension, example: testfile.txt
	 * <li><strong>supported extensions - txt, css, xml, csv, png, jpg, jpeg, bmp, gif, doc, docx, dot, dotx, xls, xlsx, ppt, pptx, pdf, zip, tar, gz, js
	 * <strong> if fileName already exists in the given relative path, an exception is thrown "An object with name - " + filename + " already exists."
	 * <ul><p>
	 *
	 * Returns the new id which needs to be stored by client.
	 * <p> returned value example: workspace://SpacesStore/262d8556-773d-46cc-8218-4fd38554b688;1.0
	 *
	 * @param relativePath, not null
	 * @param fileName, not null
	 * @param dataBytes, not null
	 * @return new content id, not null
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	String createContent(String relativePath, String fileName, byte[] dataBytes) throws ContentManagementServiceException;


	/**
	 * Updates content for supplied content id.
	 * <p> this method overwrites the actual content. History will maintained for previous data.
	 * <p> new content needs to be set in newContent with default checkIn comment.
	 *
	 * @Update on 12 may 2014 to maintain history for update.
	 * @param contentId, not null
	 * @param mimeType, not null
	 * @param newContent, not null
	 * @return text "Object " + contentId + " contents modified successfully." , null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	String updateContent(String contentId, String mimeType, byte[] newContent) throws ContentManagementServiceException;

	String updateContent(String contentId, String mimeType, byte[] newContent, boolean skipAntiVirusCheck) throws ContentManagementServiceException;

	/**
	 * Updates content for supplied content id.
	 * <p> this method overwrites the actual content. History will maintained for previous data.
	 * <p> new content needs to be set in newContent
	 *
	 * @Update on 12 may 2014 to maintain history for update.
	 * @param contentId, not null
	 * @param mimeType, not null
	 * @param newContent, not null
	 * @param checkinComment, not null
	 * @param user null
	 * @return text "Object " + contentId + " contents modified successfully." , null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */

	String updateContent(String contentId, String mimeType, byte[] newContent,
			String checkinNotes,String user) throws ContentManagementServiceException;

	String updateContent(String contentId, String mimeType, byte[] newContent,
			String checkinNotes,String user, boolean skipAntiVirusCheck) throws ContentManagementServiceException;

	/**
	 * Updates meta data of the content.
	 * <p> new meta data needs to be set in metadataElement object
	 *
	 * @param metadataElement
	 * @return text "Object " + metadataElement.objectId + " modified successfully." , null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	String updateContentMetadata(MetadataElement metadataElement) throws ContentManagementServiceException;

	/**
	 * Updates meta data of the content.
	 * <p> new meta data needs to be set in metadataElement object. Here tags are only to flag update the taggable metadata.
	 *     tags can be a empty list.
	 *
	 * @param metadataElement
	 * @param tags, value example-  list of : workspace://SpacesStore/a91c64bb-29a0-45bc-9ee8-c471dfc1ac17
	 * @return text "Object " + metadataElement.objectId + " modified successfully." , null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	String updateContentMetadata(MetadataElement metadataElement,List<String> tags) throws ContentManagementServiceException;

	/**
	 * Gets content meta data for supplied relative path.
	 * <p><ul>
	 * <li><strong> relativePath should not start with /(slash)
	 * <li><strong> relativePath should be separated by / only. Please mind the /
	 * <li><strong> relativePath should not start with ECM Base Path configured in configuration.proprties
	 * <ul><p>
	 *
	 * @param relativePath, not null
	 * @return content object, null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	Content getContentByPath(String relativePath) throws ContentManagementServiceException;

	/**
	 * Gets content meta data for supplied content id.
	 *
	 * @param contentId, not null
	 * @return content object, null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	Content getContentById(String contentId) throws ContentManagementServiceException;

	/**
	 * Gets actual content data in byte array for supplied relative path.
	 * <p><ul>
	 * <li><strong> relativePath should not start with /(slash)
	 * <li><strong> relativePath should be separated by / only. Please mind the /
	 * <li><strong> relativePath should not start with ECM Base Path configured in configuration.proprties
	 * <ul><p>
	 *
	 * @param relativePath, not null
	 * @return byte array, not null
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	byte[] getContentDataByPath(String relativePath) throws ContentManagementServiceException;

	/**
	 * Gets actual content data in byte array for supplied content id.
	 *
	 * @param contentId, not null
	 * @return byte array, not null
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	byte[] getContentDataById(String contentId) throws ContentManagementServiceException;


	/**
	 * Gets latest content data with latest version.
	 *
	 * @param contentId, not null
	 * @return byte array, not null
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	public Content getContentByPath(String relativePath,boolean latestVer) throws ContentManagementServiceException;


	/**
	 * Creates a single content marking appropriate category and subCategory.
	 * <p><ul>
	 * <li><strong> relativePath should not start with /(slash)
	 * <li><strong> relativePath should be separated by / only. Please mind the /
	 * <li><strong> relativePath should not start with ECM Base Path configured in configuration.proprties
	 * <li><strong> category should be non-empty typically module (e.g. PLAT, D2C )
	 * <li><strong> subCategory should be non-empty typically feature (e.g. NPTL (Notice Template), CFG (Configuration) )
	 * <ul><p>
	 *
	 * <p> category and subCategory are valid for only couchbase and are ignored for Alfresco and Oracle
	 * <p> relative path is stored as is in document.
	 *
	 * <p><ul>
	 * <li><strong>fileName should have valid extension, example: testfile.txt
	 * type is derived from extension
	 * <li><strong>supported extensions - txt, css, xml, csv, png, jpg, jpeg, bmp, gif, doc, docx, dot, dotx, xls, xlsx, ppt, pptx, pdf, zip, tar, gz, js
	 * <strong> if fileName already exists in the given relative path, an exception is thrown "An object with name - " + filename + " already exists."
	 * <ul><p>
	 *
	 * Returns the new id which needs to be stored by client.
	 * <p> returned value example:
	 * 	Alfresco - workspace://SpacesStore/262d8556-773d-46cc-8218-4fd38554b688;1.0
	 *  Couchbse - UUID::<type>::<category>::<subCategory>
	 *
	 * @param relativePath
	 * @param fileName
	 * @param dataBytes
	 * @param category
	 * @param subCategory
	 * @return new content id, not null
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	String createContent(String relativePath, String fileName,
			byte[] dataBytes, String category, String subCategory, String type) throws ContentManagementServiceException;

	String createContent(String relativePath, String fileName,
			byte[] dataBytes, String category, String subCategory, String type, boolean skipAntiVirusCheck) throws ContentManagementServiceException;

	/**
	 * Creates non-Binary document in couchbase.
	 *
	 * @param relativePath
	 * @param fileName
	 * @param dataString
	 * @return
	 * @throws ContentManagementServiceException
	 */
	String createContent(String relativePath, String fileName, String dataString,
			String category, String subCategory, String type)
			throws ContentManagementServiceException;

	/**
	 * Deletes content from CMIS for supplied content id.
	 * <p> Every information (versions and audit information) about the content is removed.
	 * <p><strong> This method ONLY works for DOCUMENT and not for FOLDER
	 *
	 * @param contentId, not null
	 * @return text "Document " + contentId + " deleted successfully", null if not processed
	 * @throws ContentManagementServiceException, {@link IllegalArgumentException}
	 */
	String deleteContent(String contentId) throws ContentManagementServiceException;

	public enum ErrorCodes {

		VIRUS_INFECTED_FILE("VIRUS_INFECTED_FILE"),
		ERROR_IN_SCAN("ERROR_IN_SCAN"),
		ERROR_CLAM_CONFIG("ERROR_CLAM_CONFIG");


		private String errorCode;

		ErrorCodes(String errorCode){
			this.errorCode = errorCode;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
	}
}
