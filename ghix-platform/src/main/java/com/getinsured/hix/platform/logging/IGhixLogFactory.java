package com.getinsured.hix.platform.logging;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public interface IGhixLogFactory {
	// key: name (String), value: a GhixLogger;
	ConcurrentMap<String, GhixLogger> loggerMap = new ConcurrentHashMap<String, GhixLogger>();;
	
	  public GhixLogger getGhixLogger(Class<?> clazz);

}
