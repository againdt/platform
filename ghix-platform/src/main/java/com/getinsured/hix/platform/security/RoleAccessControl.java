package com.getinsured.hix.platform.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RoleProvisioningRule;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.security.repository.IRoleProvisioningRepository;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRoleRepository;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.PlatformErrorCode;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

@Component
@DependsOn("platformConstants")
public class RoleAccessControl {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RoleAccessControl.class);

	public final static int ADD_ROLE_PERMISSION = 0;
	public final static int UPDATE_ROLE_PERMISSION = 1;
	public final static int UPDATE_USERDETAILS_PERMISSION = 2;
	
	public HashSet<String> selfProvisioningRoles = null; 
	
	@Autowired private IRoleProvisioningRepository provisioningRepo;
	@Autowired private IUserRepository userRepo;
	@Autowired private IUserRoleRepository userRoleRepo;
	
	public AccountUser getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			Object principle = auth.getPrincipal();
			if (principle instanceof AccountUser) {
				return (AccountUser) principle;
			}
		}
		return null;
	}
	
	private String getUserDefaultRoleName(AccountUser user){
		Iterable<UserRole> userRoles = null;
		userRoles  = user.getUserRole(); // 
		if(userRoles == null){
			userRoles = this.userRoleRepo.findByUser(user);
		}
		for(UserRole uRole:userRoles){
			if(!uRole.isActiveUserRole()){
				continue;
			}
			if(uRole.isDefaultRole()){
				return uRole.getRole().getName();
			}
		}
		return null;
	}
	
	public String getCurrentUserRoleName() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentRoleName = null;
		if (auth != null) {
			Object principle = auth.getPrincipal();
			if (principle instanceof AccountUser) {
				AccountUser user = (AccountUser) principle;
				currentRoleName = user.getActiveModuleName();
				if (currentRoleName == null) {
					currentRoleName = this.getUserDefaultRoleName(user);
				}
			}
		}
		if(currentRoleName == null){
			LOGGER.info("No user found in security context");
		}else{
			currentRoleName=currentRoleName.toUpperCase();
		}
		return currentRoleName;
	}
	
	public int getCurrentUserRoleId() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		int currentRoleId = -1;
		if (auth != null) {
			Object principle = auth.getPrincipal();
			if (principle instanceof AccountUser) {
				AccountUser user = (AccountUser) principle;
				currentRoleId = user.getActiveModuleId();
				if (user.getActiveModuleName() == null) {
				}else{
					currentRoleId = user.getActiveModuleId();
				}
			}
		}
		LOGGER.info("Logged in user effective role id:"+currentRoleId);
		return currentRoleId;
	}
	
	private boolean getProvisioningPermission(int operation, Role role, HashMap<String,RoleProvisioningRule> rolePermissionMap){
		switch(operation){
			case ADD_ROLE_PERMISSION:{
				return this.canAddRole(role.getName(), rolePermissionMap);
			}
			case UPDATE_ROLE_PERMISSION:{
				return this.canUpdateRole(role.getName(), rolePermissionMap);
			}
			case UPDATE_USERDETAILS_PERMISSION:{
				return this.canUpdateUserDetails(role.getName(), rolePermissionMap);
			}
			default:
				LOGGER.error("Invalid operation provided to determine the provisioning options:"+operation);
				return false;
		}
	}

	/**
	 * Returns the empty list in case of anonymous user or filtered list which could be populated or not depending upon the role
	 * @param listToFilter
	 * @param operation
	 * @return
	 */
	public List<Role> filterRoleListForCurrentUser(List<Role> listToFilter, int operation) {
		if (listToFilter == null) {
			return new ArrayList<Role>();
		}
		List<Role> returnList = null;
		try {
			returnList = new ArrayList<Role>();
			String currentRoleName = this.getCurrentUserRoleName();
			if (currentRoleName == null) {
				return returnList;
			}
			HashMap<String,RoleProvisioningRule> rolePermissionMap = getCurrentRoleProvisioningMap();
			LOGGER.debug("Filtering the list for roles "+currentRoleName+" can assign");
			for (Role tmpRole : listToFilter) {
				if(getProvisioningPermission(operation, tmpRole, rolePermissionMap)){
					returnList.add(tmpRole);
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: filterRoleListForCurrentUser(): ", e);
			throw new GIRuntimeException(
					PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null,
					Severity.HIGH);
		}
		LOGGER.debug("Received list size:"+listToFilter.size()+" for filtering, returned:"+returnList.size());
		return returnList;
	}

	public List<UserRole> filterUserRoleListForCurrentUser(int operation, List<UserRole> listToFilter) {
		if (listToFilter == null) {
			return new ArrayList<UserRole>();
		}
		List<UserRole> returnList = new ArrayList<UserRole>();
		try {
			String currentRoleName = this.getCurrentUserRoleName();
			if (currentRoleName == null) {
				return returnList;
			}
			HashMap<String, RoleProvisioningRule> rolePermissionMap = getCurrentRoleProvisioningMap();
			Role tmp = null;
			for (UserRole tmpUserRole : listToFilter) {
				tmp = tmpUserRole.getRole();
				if(this.getProvisioningPermission(operation, tmp, rolePermissionMap)){
					returnList.add(tmpUserRole);
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: filterUserRoleListForCurrentUser() ", e);
			throw new GIRuntimeException(
					PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null,
					Severity.HIGH);
		}
		LOGGER.info("Received list size:"+listToFilter.size()+" for filtering, returned:"+returnList.size());
		return returnList;
	}

	public List<String> filterRolesForCurrentUser(int operation, List<String> listToFilter) {
		if (listToFilter == null) {
			return new ArrayList<String>();
		}
		List<String> returnList = null;
		try {
			returnList = new ArrayList<String>();
			String currentRoleName = this.getCurrentUserRoleName();
			if (currentRoleName == null) {
				return returnList;
			}
			HashMap<String, RoleProvisioningRule> rolePermissionMap = getCurrentRoleProvisioningMap();
			RoleProvisioningRule rule = null;
			for (String tmpRoleName : listToFilter) {
				rule = rolePermissionMap.get(tmpRoleName);
				if(rule == null || !rule.isActive()){
					continue;
				}
				switch(operation){
					case ADD_ROLE_PERMISSION:
					case UPDATE_ROLE_PERMISSION:
						if(rule.isAddAllowed()){
							returnList.add(tmpRoleName);
						}
					case UPDATE_USERDETAILS_PERMISSION:
						if(rule.isUpdateAllowed()){
							returnList.add(tmpRoleName);
						}
					default:
						LOGGER.error("Invalid operation ["+operation+"] provided for role filter");
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: List<String> filterRolesForCurrentUser(operation, list): ", e);
			throw new GIRuntimeException(
					PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null,
					Severity.HIGH);
		}
		LOGGER.info("Received list size:"+listToFilter.size()+" for filtering, returned:"+returnList.size());
		return returnList;
	}

	public HashMap<String, RoleProvisioningRule> getCurrentRoleProvisioningMap() {
		String currentRoleName = this.getCurrentUserRoleName();
		if(currentRoleName == null){
			return null;
		}
		HashMap<String, RoleProvisioningRule> provisioningMap = new HashMap<String, RoleProvisioningRule>();
		int currentRoleId = this.getCurrentUserRoleId();
		if (currentRoleId == -1) {
			return provisioningMap;
		}
		List<RoleProvisioningRule> provisioningList = this.provisioningRepo.findProvisioningRoles(currentRoleName);
		LOGGER.info("Received :"+provisioningList.size()+" roles for "+currentRoleId);
		for(RoleProvisioningRule tmp: provisioningList ){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Retrieve Managed Role:"+tmp.getManagedRole().getName());
				}
				if(tmp.isActive()){
					provisioningMap.put(tmp.getManagedRole().getName(),tmp);
				}
		}
		return provisioningMap;
	}

	public boolean canUpdateRole(String roleName, HashMap<String,RoleProvisioningRule> roleProvisionRules) {
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		return (rule.isActive() && rule.isAddAllowed());
	}
	
	public boolean canUpdateUserDetails(String roleName, HashMap<String,RoleProvisioningRule> roleProvisionRules) {
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Update:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && rule.isUpdateAllowed());
	}
	
	public boolean canUpdateRole(String roleName) {
		if(roleName == null){
			return false;
		}
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getCurrentRoleProvisioningMap();
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Add:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && rule.isAddAllowed());
	}
	
	public boolean canUpdateUserDetails(String roleName) {
		if(roleName == null){
			return false;
		}
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getCurrentRoleProvisioningMap();
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Update:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && rule.isUpdateAllowed());
	}
	
	public boolean canAddRole(String roleName) {
		if(roleName == null){
			return false;
		}
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getCurrentRoleProvisioningMap();
		if(roleProvisionRules == null){
			//Check if it is an OPERATIONS managed and a non privileged role
			if(checkForNonPrivilegedRole(roleName)){
				LOGGER.info("Role:"+roleName+" is is being created in anonymous state, assuming eligible for self provisioning");
				return true;
			}
			LOGGER.info("\"Role Name:"+roleName+" Can not be added for anonymous session, please check the provisioning rules");
			return false;
		}
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Add:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && rule.isAddAllowed());
	}
	
	public boolean canCreateUserForRole(String roleName) {
		if(roleName == null){
			return false;
		}
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getCurrentRoleProvisioningMap();
		if(roleProvisionRules == null || roleProvisionRules.size() == 0 ){
			LOGGER.info("Role:"+roleName+" is being created in anonymous state, assuming eligible for self provisioning");
			return true;
		}
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Add:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && rule.isAddAllowed());
	}
	
	private boolean checkForNonPrivilegedRole(String roleName) {
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getOperationsProvisioningMap();
		RoleProvisioningRule rule = roleProvisionRules.get(roleName);
		if(rule != null){
			Role managedRole = rule.getManagedRole();
			if(managedRole.getPrivileged() == 0){
				return true;
			}else{
				LOGGER.info("Role:"+roleName+" is a privileged role, managed by "+GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE);
			}
		}else{
			LOGGER.error("Role:"+roleName+" was expected to be enabled for manage privileges by "+GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE+", found none!");
		}
		return false;
	}

	private HashMap<String, RoleProvisioningRule> getOperationsProvisioningMap() {
		String currentRoleName = GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE;
		HashMap<String, RoleProvisioningRule> provisioningMap = new HashMap<String, RoleProvisioningRule>();
		List<RoleProvisioningRule> provisioningList = this.provisioningRepo.findProvisioningRoles(currentRoleName);
		for(RoleProvisioningRule tmp: provisioningList ){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Retrieve Managed Role:"+tmp.getManagedRole().getName());
			}
			provisioningMap.put(tmp.getManagedRole().getName(),tmp);
		}
		return provisioningMap;
	}

	public boolean canAddRole(String roleName, HashMap<String,RoleProvisioningRule> roleProvisionRules) {
		if(roleProvisionRules == null){
			//Check if it is an OPERATIONS managed and a non privileged role
			if(checkForNonPrivilegedRole(roleName)){
				LOGGER.info("Role:"+roleName+" is a non privileged role, assuming eligible for self provisioning, Anonymous session can add this role");
				return true;
			}else{
				return false;
			}
		}
		RoleProvisioningRule rule = roleProvisionRules.get(roleName.toUpperCase());
		if(rule == null){
			LOGGER.info("No rule found for role:"+roleName);
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Add:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && rule.isAddAllowed());
	}

	public boolean hasFullPermission(String rName,
			HashMap<String, RoleProvisioningRule> rolePermissionMap) {
		return canAddRole(rName,rolePermissionMap) && canUpdateRole(rName,rolePermissionMap);
	}

	@SuppressWarnings("unchecked")
	public synchronized String loadAllPrivileges(List<Role> existingRoles) {
		JSONObject obj = new JSONObject();
		String currentRole = this.getCurrentUserRoleName();
		if(currentRole != null && currentRole.equalsIgnoreCase(GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE)){
			JSONArray provisioningRoles = new JSONArray();
			JSONArray managedRoles = new JSONArray();
			for(Role tmp: existingRoles){
				if(tmp.getPrivileged() == 1){
					provisioningRoles.add((JSONAware)tmp);
				}
				managedRoles.add((JSONAware)tmp);
			}
			List<RoleProvisioningRule> allPrivileges = this.provisioningRepo.findAll();
			JSONArray array = new JSONArray();
			for(RoleProvisioningRule tmp: allPrivileges){
				array.add(tmp.toJSONObject());
			}
			obj.put("provisioning_roles", provisioningRoles);
			obj.put("managed_roles", managedRoles);
			obj.put(RoleProvisioningRule.PROVISIONING_RULES_ROOT,array);
		}else{
			throw new GIRuntimeException("No sufficient privileges for this operation");
		}
		return obj.toJSONString();
	}
	
	private RoleProvisioningRule getProvisioningRule(JSONObject obj, HashMap<String,Role> existingRoles){
		Object tmpObj = obj.get("privateId");
		RoleProvisioningRule ruleObj = null;
		RoleProvisioningRule existingRuleObj = null;
		AccountUser currentUser = this.getCurrentUser();
		if(tmpObj == null){
			//Its a new rule
			LOGGER.debug("Received new rule");
			ruleObj = RoleProvisioningRule.getNewRulefromJSONObj(obj, existingRoles);
			if(ruleObj != null){
				ruleObj.setCreatedBy(currentUser);
				ruleObj.setLastUpdatedBy(currentUser);
			}
		}else{
			LOGGER.debug("Received an existing rule, checking if rule actually exists");
			ruleObj = RoleProvisioningRule.getExistingRulefromJSONObj(obj, existingRoles); // This is from the JSON
			if(ruleObj != null){
				existingRuleObj = this.provisioningRepo.findByProvisionId(ruleObj.getProvisionId()); // This one is from the Repo
				if(existingRuleObj != null){
					existingRuleObj.setLastUpdatedBy(currentUser);
					existingRuleObj.setAddAllowed(ruleObj.getAddAllowed());
					existingRuleObj.setIsActive(ruleObj.getIsActive());
					existingRuleObj.setUpdateAllowed(ruleObj.getUpdateAllowed());
					return existingRuleObj;
				}else{
					LOGGER.error("Failed to find the rule with Id:"+ruleObj.getProvisionId());
				}
			}else{
				LOGGER.error("Failed to find the provisioning rule from JSON:"+obj.toJSONString());
				return null;
			}
		}
		LOGGER.debug("Done creating the rule");
		return ruleObj;
	}
	
	@Transactional
	public synchronized List<RoleProvisioningRule> saveOrUpdateAllPrivileges(String jsonStr, HashMap<String,Role> existingRoles) {
		JSONObject obj =null;
		String currentRole = this.getCurrentUserRoleName();
		
		List<RoleProvisioningRule> savedList = null;
		try{
			if(currentRole != null && currentRole.equalsIgnoreCase(GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE)){
				JSONParser parser = new JSONParser();
				obj = (JSONObject)parser.parse(jsonStr);
				JSONObject tmpJasonObj = null;
				JSONArray array = (JSONArray) obj.get(RoleProvisioningRule.PROVISIONING_RULES_ROOT);
				HashSet<RoleProvisioningRule> listToSaveOrUpdate = new HashSet<RoleProvisioningRule>();
				RoleProvisioningRule tempRule;
				for(Object tmp: array){
					tmpJasonObj = (JSONObject)tmp;
					tempRule = this.getProvisioningRule(tmpJasonObj,existingRoles);
					if(tempRule != null){
						if(!listToSaveOrUpdate.add(tempRule)){
							LOGGER.error("Duplicate Provisioning rule found, skipped\n"+tempRule.toJSONString());
						}
					}
				}
				if(listToSaveOrUpdate.size() > 0){
					List<RoleProvisioningRule> list = new ArrayList<RoleProvisioningRule>(listToSaveOrUpdate);
					savedList = this.provisioningRepo.save(list);
					this.provisioningRepo.flush();
				}
			}else{
				throw new GIRuntimeException("Permission Denied: Logged in user does not have sufficient privileges for this operation");
			}
		}catch(Exception e){
			LOGGER.error("Update failed with message",e);
		}
		return savedList;
	}

	@SuppressWarnings("unchecked")
	public String getListAsJson(List<RoleProvisioningRule> savedList) {
		JSONObject obj = new JSONObject();
		if(savedList != null && savedList.size() > 0){
			JSONArray array = new JSONArray();
			for(RoleProvisioningRule tmp: savedList){
				array.add(tmp.toJSONObject());
			}
			obj.put(RoleProvisioningRule.PROVISIONING_RULES_ROOT, array);
		}else{
			obj.put("ERROR", "Nothing to save");
		}
		return obj.toJSONString();
	}

	public boolean canAddOrUpdateRole(String roleName) {
		if(roleName == null){
			return false;
		}
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getCurrentRoleProvisioningMap();
		RoleProvisioningRule rule = roleProvisionRules.get(roleName);
		if(rule == null){
			return false;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Rule:"+rule.getProvisioningRole().getName()+" Managing:"+rule.getManagedRole().getName()+" Add:"+rule.isUpdateAllowed());
		}
		return (rule.isActive() && (rule.isAddAllowed()));
	}

	public List<Role> filterRoleListForAddOrUpdate(List<Role> rolesList) {
		ArrayList<Role> filteredList = new ArrayList<>();
		if(rolesList == null || rolesList.size() == 0){
			return filteredList;
		}
		HashMap<String, RoleProvisioningRule> roleProvisionRules = this.getCurrentRoleProvisioningMap();
		RoleProvisioningRule tmpRule = null;
		for(Role tmpRole: rolesList){
			tmpRule = roleProvisionRules.get(tmpRole.getName());
			if(tmpRule == null){
				LOGGER.info("Current role does not have add or update privileges for role:"+tmpRole.getName());
				continue;
			}
			if(tmpRule.isActive() && (tmpRule.isAddAllowed() || tmpRule.isUpdateAllowed())){
				filteredList.add(tmpRole);
			}
		}
		return filteredList;
	}

	public boolean initiateBootstrap(HashMap<String,Role> existingRoles) {
		Assert.notNull(GhixPlatformConstants.ROLE_PROVISIONING_RULES_FILE, "Role Provisioning bootstrap file must be provided for initializing the module, property missing");
		Assert.notNull(GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE, "Provisioning Role must be provided for bootstrapping, property missing");
		HashMap<String, HashMap<String, RoleProvisioningRule>> allRules = this.getProvisioningRules();
		if(allRules != null && allRules.size() > 0){
			LOGGER.info("Provisioning rules already available, please use the ADMIN UI to add/update the rules, if required");
			return true;
		}
		LOGGER.info("Initiating bootstrapping rule from file");
		String ghixHome = System.getProperty("GHIX_HOME");
		if(ghixHome == null){
			throw new RuntimeException("Expected GHIX_HOME property to be available to bootstrap the role provisioning file , found none");
		}
		String roleProvisioningFilePath = ghixHome+File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+GhixPlatformConstants.ROLE_PROVISIONING_RULES_FILE;
		File bootstrapFile = new File(roleProvisioningFilePath);
		if(!bootstrapFile.exists() || bootstrapFile.isDirectory() || !bootstrapFile.canRead()){
			LOGGER.error("Bootstrap rule file:\""+roleProvisioningFilePath+"\" does not exist or there are permission issues, please check the file, make sure its not a directory and this process had read permission to it");
			throw new RuntimeException("Bootstrapping failed for provisionig rules, Tried using:"+roleProvisioningFilePath);
		}
		JSONParser parser = new JSONParser();
		FileInputStream fis = null;
		InputStreamReader isr = null;
		try {
			fis = new FileInputStream(roleProvisioningFilePath);
			isr = new InputStreamReader(fis);
			JSONObject rulesObj = (JSONObject)parser.parse(isr);
			LOGGER.info("Done parsing the bootstrapping rules");
			List<RoleProvisioningRule> bootstrapRulesList = this.bootstrap(rulesObj, existingRoles);
			if(bootstrapRulesList == null || bootstrapRulesList.size() == 0){
				LOGGER.error("Bootstrapping failed for provisionig rules [file:"+roleProvisioningFilePath+"], no rules loaded");
			}else{
				LOGGER.info("Succesfully loaded:"+bootstrapRulesList.size()+" rules from \""+roleProvisioningFilePath+"\"");
			}
		} catch (IOException | ParseException e) {
			LOGGER.error("Processing of Bootstrap rule file:"+roleProvisioningFilePath+" enountered error:"+e.getMessage(),e);
			throw new RuntimeException("Bootstrapping failed for provisionig rules [file:"+roleProvisioningFilePath+"]",e);
		}finally{
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(isr);
		}
		return true;
	}
	
	@Transactional
	private List<RoleProvisioningRule> bootstrap(JSONObject obj, HashMap<String,Role> existingRoles) {
		Assert.notNull(existingRoles, "Existing roles must be available for provisioning rules");
		List<RoleProvisioningRule> savedList = null;
		JSONObject tmpJasonObj = null;
		JSONArray array = (JSONArray) obj.get(RoleProvisioningRule.PROVISIONING_RULES_ROOT);
		Assert.notEmpty(array, "Parser returned empty array of provisioning rules, check the file format");
		HashSet<RoleProvisioningRule> listToSaveOrUpdate = new HashSet<RoleProvisioningRule>();
		RoleProvisioningRule tempRule;
		for(Object tmp: array){
			tmpJasonObj = (JSONObject)tmp;
			tempRule = this.getBootstrapProvisioningRule(tmpJasonObj,existingRoles);
			if(tempRule != null){
				if(!listToSaveOrUpdate.add(tempRule)){
					LOGGER.error("Duplicate Provisioning rule found, skipped\n"+tempRule.toJSONString());
				}
			}
		}
		Assert.notEmpty(listToSaveOrUpdate," There are no provisioning rules in the file loaded");
		List<RoleProvisioningRule> list = new ArrayList<RoleProvisioningRule>(listToSaveOrUpdate);
		try{
			savedList = this.provisioningRepo.save(list);
			this.provisioningRepo.flush();
		}catch(DataIntegrityViolationException | UnexpectedRollbackException ex){
			Throwable t = ex.getMostSpecificCause();
			LOGGER.warn("Bootstrap expects empty data tables, Integrity voilation is because of race condition, this means rules have already been loaded",t);
			return this.provisioningRepo.findAll();
			
		}
		return savedList;
	}
	
	public HashMap<String, HashMap<String,RoleProvisioningRule>> getProvisioningRules(){
		List<RoleProvisioningRule> allRules = this.provisioningRepo.findAll();
		Iterator<RoleProvisioningRule> ruleCursor = allRules.iterator();
		HashMap<String, HashMap<String,RoleProvisioningRule>> allRulesMap = new HashMap<>();
		RoleProvisioningRule rule = null;
		HashMap<String, RoleProvisioningRule> managedRolesMap = null;
		String provisioningRoleName = null;
		while(ruleCursor.hasNext()){
			rule = ruleCursor.next();
			provisioningRoleName = rule.getProvisioningRole().getName();
			managedRolesMap = allRulesMap.get(provisioningRoleName);
			if(managedRolesMap == null){
				managedRolesMap = new HashMap<>();
				allRulesMap.put(provisioningRoleName, managedRolesMap);
			}
			managedRolesMap.put(rule.getManagedRole().getName(), rule);
		}
		return allRulesMap;
	}
	
	/**
	 * No one should call this method, only for bootstrapping the rules
	 * @param obj
	 * @param existingRoles
	 * @return
	 */
	private RoleProvisioningRule getBootstrapProvisioningRule(JSONObject obj, HashMap<String,Role> existingRoles){
		RoleProvisioningRule ruleObj;
		List<AccountUser> currentOpsUsers = this.userRepo.getUsersByRoleName(GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE);
		Assert.notEmpty(currentOpsUsers, "No user with "+GhixPlatformConstants.ROLE_PROVISIONING_RULES_ROLE+" available in users repository, bootstrapping can not continue");
		AccountUser currentUser = currentOpsUsers.get(0);
		//Its a new rule, but check if we already have this rule available
		String provisioningRoleName = (String) obj.get(RoleProvisioningRule.PROVISIONING_ROLE_KEY);
		String managedRoleName = (String) obj.get(RoleProvisioningRule.MANAGED_ROLE_KEY);
		if(provisioningRoleName == null || managedRoleName == null){
			LOGGER.error("Invalid rule received, Managed role as well provisioning role are required:"+obj.toJSONString());
			return null;
		}
		Role provisioningRole = existingRoles.get(provisioningRoleName);
		Role managedRole = existingRoles.get(managedRoleName);
		if(provisioningRole == null || managedRole == null){
			LOGGER.error("Invalid rule received, Managed role as well provisioning role needs exist before the rule is created:"+obj.toJSONString());
			return null;
		}
		// Don't change the order of provisioning and managed role
		ruleObj = RoleProvisioningRule.getNewRulefromJSONObj(obj, provisioningRole, managedRole);
		if(ruleObj != null){
			ruleObj.setCreatedBy(currentUser);
			ruleObj.setLastUpdatedBy(currentUser);
		}
		//LOGGER.debug("Done creating the rule");
		// This could be a null as well
		return ruleObj;
	}

	/**
	 * Returns the user list based on current user privileges
	 * @param userList
	 * @return
	 */
	public List<AccountUser> filterUserListForCurrentUser(List<AccountUser> userList) {
		LOGGER.info("Filtering the user list for current user");
		ArrayList<AccountUser> filteredList = new ArrayList<AccountUser>();
		HashMap<String, RoleProvisioningRule> currentUserProvisioningSet = this.getCurrentRoleProvisioningMap();
		if(currentUserProvisioningSet == null || userList.size() == 0){
		 return filteredList;
		}
		String defaultRoleName = null;
		RoleProvisioningRule rule;
		Role defaultRole = null;
		for(AccountUser user: userList){
		defaultRole = this.getUserDefaultRole(user);
		 if(defaultRole == null){
			 LOGGER.error("Never expected to see this message, user without a default role: User Id:"+user.getId());
			 continue;
		 }
			 defaultRoleName = defaultRole.getName();
		 rule = currentUserProvisioningSet.get(defaultRoleName);
		 if(rule != null && rule.isActive() && (rule.isAddAllowed() || rule.isUpdateAllowed())){
			 filteredList.add(user);
		 }
		}
		return filteredList;
	}
	
	private Role getUserDefaultRole(AccountUser user){
		Set<UserRole> userRoles = user.getUserRole();
		Iterator<UserRole> cursor = userRoles.iterator();
		UserRole tmp = null;
		while(cursor.hasNext()){
			tmp = cursor.next();
			if(tmp.isDefaultRole()){
				return tmp.getRole();
			}
		}
		return null;
	}
}
