package com.getinsured.hix.platform.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

@Component
public class RedisErrorHandler implements ErrorHandler {

	private static Logger LOGGER = LoggerFactory.getLogger(RedisErrorHandler.class);
    @Override
    public void handleError(Throwable t) {
    	LOGGER.error("Error in listener "+t.getMessage());
    }
}
