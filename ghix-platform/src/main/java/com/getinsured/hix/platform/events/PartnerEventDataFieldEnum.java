package com.getinsured.hix.platform.events;

public enum PartnerEventDataFieldEnum {
	AFFILIATE_ID,
	CART_ITEM_ID,
	ENROLLMENT_ID,
	LEAD_ID,
	EVENT_NAME
}
