package com.getinsured.hix.platform.util.payload;

import org.springframework.ws.context.MessageContext;

import com.getinsured.hix.model.GIWSPayload;


/**
 * This is the default handler. No Operation.
 * @author nayak_b
 *
 */
public class DefaultPayloadHandler implements GIWSPayloadHanlder {

	@Override
	public void handlePayload(GIWSPayload giwsPayload, Object response) {
		//Do implement your logic here. We need to provide our handler in particular module. these handler need to be 
		//loaded by forName. This provide us a flexibilty of loosely coupling.
	}
}
