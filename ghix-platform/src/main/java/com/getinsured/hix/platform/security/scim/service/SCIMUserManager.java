package com.getinsured.hix.platform.security.scim.service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.exception.GIException;

public interface SCIMUserManager {

	/**
	 * Method to create account user
	 * 
	 * @param user
	 *            Object containing GHIX Account user attributes
	 * @return Account user object containing ghix user attributes
	 * @throws GIException
	 */
	AccountUser createUser(AccountUser user, int isPrivileged) throws GIException;

	/**
	 * Method to find the account user for a given email address
	 * 
	 * @param emailId
	 *            String containing email address of user to be fetched
	 * @return AccountUser object containing ghix account user attributes
	 * @throws GIException
	 */
	AccountUser findByEmail(String emailId) throws GIException;

	/**
	 * This method is used to find the SCIM User by using a filter
	 * 
	 * @param filter
	 *            String containing filter used to fetch user
	 * @return AccountUser Object containing GHIX User attributes
	 * @throws GIException
	 */
	AccountUser findUser(String filter) throws GIException;

	/**
	 * This method updates the given GHIX User
	 * 
	 * @param updatedAccountUser
	 *            AccountUser object containing the ghix user attributes
	 * @return Boolean indicating operation status
	 * @throws GIException
	 */
	AccountUser updateUser(AccountUser accountUser) throws GIException;

	/**
	 * This method updates the additional non-ghix attributes of a GHIX/SCIM
	 * User
	 * 
	 * @param id
	 *            String containing uniques wso2 id of user
	 * @param userName
	 *            String containing username of user
	 * @param dob
	 *            String containing date of birth
	 * @param ssn
	 *            String containinig social security number
	 * @param preferredLanguage
	 *            String containing preferred language
	 * @param ridpFlag
	 *            String containing ridpFlag
	 * @return String containing response received from WSO2
	 * @throws GIException
	 */
	String updateUserAdditionalAttribs(String id, String userName, String dob,
			String ssn, String preferredLanguage, String ridpFlag) throws GIException;

	/**
	 * This method changes the password of the given GHIX User
	 * 
	 * @param updatedAccountUser
	 *            AccountUser object containing the ghix user attributes
	 * @return Boolean indicating operation status
	 * @throws GIException
	 */
	boolean changePassword(AccountUser updatedAccountUser,
			String password) throws GIException;
	
	/**
	 * This method authenticates the user on WSO2
	 * 
	 * @param username
	 *          Username of the user
	 * @param password
	 * 			Password of the user	
	 * @return Boolean indicating operation status
	 * @throws GIException
	 */
	boolean authenticate(String username, String password) throws GIException;

	/**
	 * This method is used to change user password using admin privilege
	 * 
	 * @param updatedAccountUser Account user object containing the ghix user attributes
	 * @param newPassword Password of user to be set
	 * @return Boolean indicating operation status
	 * @throws GIException
	 */
	boolean changePasswordByAdmin(AccountUser updatedAccountUser,
			String newPassword) throws GIException;

	boolean changeEmail(AccountUser accountUser) throws GIException;

	AccountUser findByScimId(String scimId) ;
	String getClaimAttributeValue(String userName, String claimUri);
	public String getSCIMUserJSON(String extAppUserId);
}
