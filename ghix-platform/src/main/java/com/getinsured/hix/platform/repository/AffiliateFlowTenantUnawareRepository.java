package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.affiliate.model.AffiliateFlow;

public interface AffiliateFlowTenantUnawareRepository extends TenantUnawareRepository<AffiliateFlow, Integer> {
	
	List<AffiliateFlow> findByUrl(String url);
	
	@Query("select flow from AffiliateFlow flow where flow.ivrNumber = :ivrNumber or flow.ivrNumberTwo = :ivrNumber")
	List<AffiliateFlow> findByIvrNumber(@Param("ivrNumber") Long ivrNumber);

	@Query("SELECT flow FROM AffiliateFlow flow where flow.id = :affiliateFlowId")
	AffiliateFlow getAffiliateFlowByFlowId(@Param("affiliateFlowId") Integer affiliateFlowId);
}


