package com.getinsured.hix.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.getinsured.hix.platform.security.RestHeaderInterceptor;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.hub.platform.security.RemoveSoapHeaderInterceptor;

public final class SecureHttpClient {
	
	private SecureHttpClient(){
	}
	
	private static CloseableHttpClient httpClient = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(SecureHttpClient.class);
	private static final int MAX_CONNECTIONS = 20000;
	private static final int MAX_CONNECTIONS_PER_ROUTE = 1000;
	private static final int MAX_EXECUTIONS = 5;
	private static String location = null;
	private static String file = null;
	private static HashMap<String, String> aliasMapping = new HashMap<>();
	private static RequestConfig defaultRequestConfig = null;
	private static KeyStore trustStore;
	private static boolean shouldContinue = true;
	private static boolean refreshStarted = false;
	private static String password;
	private static boolean initiazing=false;;
	private static final long KEY_STORE_REFRESH_INTERVAL = 24*60*60*1000; // 2 min, should be once in 24 hours
	private static HttpRequestFactory factory = null;
	
	
	
	/**
	 * Connection Request Timeout: Time in millis to get the connecion from the connection manager
	 * Connect Timeout: Time in millis to establish the connection
	 * Socket timeout: Max Idle Time in millis allowed between 2 consecutive data packets, or socket will be timeout
	 */
	static{
		trustStore = loadKeyStore();
		defaultRequestConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(30000)
				.setConnectTimeout(30000)
				.setSocketTimeout(90000).setCookieSpec(CookieSpecs.IGNORE_COOKIES)
				.build();
	}
	
	/**
	 * Module should call this in their @Pre_Destroy if they have obtained the reference to this Client.
	 */
	public void stopRefresh(){
		LOGGER.info("Stopping keystore refresh thread");
		shouldContinue = false;
	}
	
	private static String getCertificateDetails(X509Certificate cert){
		return "Subject:"+cert.getSubjectX500Principal().getName()+"Issues By:"+cert.getIssuerX500Principal().getName()+" Valid through:"+cert.getNotAfter();
	}
	
	public static void refreshKeyStore(){
		if(refreshStarted){
			return;
		}
		refreshStarted = true;
		Thread refreshThread = new Thread(new Runnable(){
			@Override
			public void run() {
				while(shouldContinue){
					try {
						if(!refreshRequired()){
							Thread.sleep(KEY_STORE_REFRESH_INTERVAL);
							continue;
						}
						KeyStore store = loadKeyStore();
						if(store != null){
							Enumeration<String> aliases = store.aliases();
							String alias = null;
							X509Certificate tmp = null;
							X509Certificate existing = null;
							while (aliases.hasMoreElements()) {
								alias = aliases.nextElement();
								if(LOGGER.isTraceEnabled()){
									LOGGER.info("Checking store alias:" + alias);
								}
								tmp = (X509Certificate) store.getCertificate(alias);
								existing = (X509Certificate) trustStore.getCertificate(alias);
								if(existing == null){
									if(LOGGER.isInfoEnabled()){
										LOGGER.info("Found new certificate for "+alias+"::"+getCertificateDetails(tmp));
									}
									trustStore.setCertificateEntry(alias, tmp);
								}else{
									// We have an existing certificate with this alias
									if(!tmp.equals(existing)){
										// The one we have is not the same as this one
										if(LOGGER.isInfoEnabled()){
											LOGGER.info("******* Overriding the existing certificate ***********");
											LOGGER.info("Existing Certificate:"+getCertificateDetails(existing));
											LOGGER.info("Now replaced with "+getCertificateDetails(tmp));
											LOGGER.info("********************************************************");
										}
										trustStore.setCertificateEntry(alias, tmp);
									}
								}
							}	
						}else{
							LOGGER.debug("No keystore loaded, there may not be any change");
						}
					} catch (KeyStoreException e) {
						LOGGER.error("Unexpected Exception reading key store", e);
					} catch (InterruptedException e) {
						// Ignore;
					}
				}
				LOGGER.info("Received signal, Exiting keystore refresh");
				refreshStarted = false;
			}
		}, "Platform Keystore Refresh");
		refreshThread.start();
		refreshStarted = true;
	}
	
			

	private static SSLConnectionSocketFactory getSSLConnectionSocketFactory(final KeyStore trustStore, String password, final String location) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException{
		SSLConnectionSocketFactory sslsf = null;
		char[] passChars = GhixPlatformConstants.PLATFORM_KEY_PASSWORD.toCharArray();
		SSLKeyStrategy sslKeyStrategy  = new SSLKeyStrategy();
		SSLContext sslcontext = SSLContexts
				.custom()
				.loadTrustMaterial(trustStore, new TrustStrategy() {
					public boolean isTrusted(X509Certificate[] chain,
							String authType) throws CertificateException {
						StringBuffer certsReceived = new StringBuffer();
						String connectionServerName = null;
						for (int i = 0; i < chain.length; i++) {
							connectionServerName = chain[i].getIssuerX500Principal().getName();
							certsReceived.append("["+connectionServerName+"] ,");
							if (isCertificateTrusted(trustStore, chain[i])) {
								LOGGER.info("Allowing connection from: " + connectionServerName);
								return true;
							}
						}
						LOGGER.warn("Not allowing SSL Connection from "
								+ connectionServerName
								+ " make sure you have imported the server certificate in the keystore ["
								+ location + "] used, received certificates "+certsReceived);
						return false;
					}
				})
				.loadKeyMaterial(trustStore, passChars,sslKeyStrategy)
				.build();
				sslsf = new SSLConnectionSocketFactory(
						sslcontext, split("Protocols",System.getProperty("https.protocols")),
			            split("Supported Ciphers",System.getProperty("https.cipherSuites")),
						new DefaultHostnameVerifier());
		return sslsf;
	}
	
	private static String[] split(String property,String propertyVal) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Evaluating property:"+property+" For value:"+propertyVal);
		}
		if(propertyVal != null && propertyVal.length() > 0){
			return propertyVal.split(" *, *");
		}
		return null;
	}

	private static boolean isCertificateTrusted(KeyStore store,
			X509Certificate certificate) {
		try {
			Enumeration<String> aliases = store.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				if(LOGGER.isTraceEnabled()){
					LOGGER.debug("Checking store alias:" + alias);
				}
				if (store.getCertificate(alias).equals(certificate)) {
					return true;
				}
			}
		} catch (KeyStoreException e) {
			LOGGER.error("Unexpected Exception reading key store", e);
			return false;
		}
		return true;
	}

	private static boolean refreshRequired(){
		File keyStoreDir = new File(location);
		File refreshFile = new File(keyStoreDir,"refresh.txt");
		if(refreshFile.exists() && refreshFile.isFile()){
			refreshFile.delete();
			return true;
		}
		return false;
	}
	
	private static File getKeyStoreFile(String location,String file, String password){
		File keyStoreDir = new File(location);
		File keyStore = new File(keyStoreDir,file);
		if(!keyStore.exists() || !keyStore.isFile()){
			throw new GIRuntimeException("Key store ["+keyStore.getAbsolutePath()+"] does not exist Or its not a file");
		}
		return keyStore;
	}
	
	/**
	 * Checks if configured aliases have a corresponding private key available in the supplied keystore
	 * @param trustStore
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws UnrecoverableEntryException
	 */
	private static void validateKeyStore(KeyStore trustStore) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException{
		Collection<String> requiredAliases = aliasMapping.values();
		Enumeration<String> aliases = trustStore.aliases();
		String tmp = null;
		while(aliases.hasMoreElements()){
			tmp = aliases.nextElement();
			if(requiredAliases.contains(tmp)){
				
				//Try loading the key
				KeyStore.ProtectionParameter protParam =
			        new KeyStore.PasswordProtection(GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray());
				// get private key
				try {
					KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry)
						trustStore.getEntry(tmp, protParam);
					PrivateKey aliasPrivateKey = pkEntry.getPrivateKey();
					if(aliasPrivateKey == null){
						LOGGER.error("Alias: \""+tmp+"\" is expected to have a coressponding private key entry, found none, review and update sslConf.xml");
						throw new RuntimeException("No private key entry found for alias:"+tmp);
					}
					LOGGER.info("Alias:"+tmp+" is expected to have a coressponding private key entry, ....... true");
				}catch(UnrecoverableEntryException e) {
					// This key is using a different passowrd, ignore it
					LOGGER.info("Failed to retrieve key for alias {}, using default password, ignoring".intern(), tmp);
				}
				
			}
		}
		// finally check if Keystore has all the aliases we have configured
		for(String requiredOne:requiredAliases){
			if(!trustStore.containsAlias(requiredOne)){
				LOGGER.info("Alias:"+requiredOne+" is expected to have a coressponding private key entry, ....... false, Ignoring");
			}
		}
		
	}
	
	private static KeyStore loadKeyStore(){
		KeyStore trustStore = null;
		InputStream instream = null;
		try {
			
			File keyStoreFile = getKeyStoreFile(GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION,GhixPlatformConstants.PLATFORM_KEY_STORE_FILE,GhixPlatformConstants.PLATFORM_KEYSTORE_PASS);
			if(keyStoreFile != null){
				instream = new FileInputStream(keyStoreFile);
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				trustStore.load(instream, GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray());
				LOGGER.info("Done loading trust store...");
				validateKeyStore(trustStore);
			}else{
				LOGGER.debug("Keystore file refresh is not required");
			}
		} catch (Exception e) {
			throw new GIRuntimeException("Failed to initialize the http client", e);
		} finally {
			try {
				if (instream != null) {
					instream.close();
				}
			} catch (IOException e) {
				LOGGER.error(e.getMessage(),e);
			}
		}
		return trustStore;
	}
	
	private static KeyStore loadKeyStore(String keyStoreLocation, String keyStoreFile, String keyStorePass) {
		KeyStore trustStore = null;
		InputStream instream = null;
		try {
			File keyStore = getKeyStoreFile(keyStoreLocation, keyStoreFile,keyStorePass);
			if(keyStore != null){
				instream = new FileInputStream(keyStore);
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				trustStore.load(instream, keyStorePass.toCharArray());
				LOGGER.info("Done loading trust store...");
				validateKeyStore(trustStore);
			}else{
				LOGGER.debug("Keystore file refresh is not required");
			}
		} catch (Exception e) {
			throw new GIRuntimeException("Failed to initialize the http client", e);
		} finally {
			try {
				if (instream != null) {
					instream.close();
				}
			} catch (IOException e) {
				LOGGER.error(e.getMessage(),e);
			}
		}
		return trustStore;
	}
	
	public static synchronized CloseableHttpClient getHttpClient() {
		while(initiazing){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				//ignored
			}
		}
		if(httpClient != null){
			LOGGER.info("Returning existing Http Client, not initializing it again, earlier initiazed from file:["+file+"] located at ["+location+"]");
			return httpClient;
		}
		SSLConfiguration sslConf = SSLConfiguration.getSSLConfiguration();
		sslConf.loadAliasMapping();
		if(trustStore == null){
			LOGGER.error("No trust store available, from file:["+file+"] located at ["+location+"], check earlier errors");
			throw new GIRuntimeException("Failed to initialize secure HttpClient");
		}
		initiazing = true;
		LOGGER.info("Initializing Secure HTTP Client using KeyStore:"+GhixPlatformConstants.PLATFORM_KEY_STORE_FILE+" located at: "+GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION);
		loadKeyStore(GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION,GhixPlatformConstants.PLATFORM_KEY_STORE_FILE,GhixPlatformConstants.PLATFORM_KEYSTORE_PASS);
		PlainConnectionSocketFactory psf = PlainConnectionSocketFactory
				.getSocketFactory();
		
		Registry<ConnectionSocketFactory> registry = null;
		try {
			registry = RegistryBuilder
					.<ConnectionSocketFactory> create().register("https", getSSLConnectionSocketFactory(trustStore,GhixPlatformConstants.PLATFORM_KEYSTORE_PASS, GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION))
					.register("http", psf).build();
		} catch (KeyManagementException | UnrecoverableKeyException
				| NoSuchAlgorithmException | KeyStoreException e) {
			throw new GIRuntimeException("Failed to initialize secure HttpClient", e);
		}
		PoolingHttpClientConnectionManager cm = new PoolingConnectionManagerProxy(
				registry);
		cm.setMaxTotal(MAX_CONNECTIONS);
		cm.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
		HttpClientBuilder builder = null;
		CredentialsProvider credsProvider = sslConf.getCredentialsProvider();
		if(credsProvider != null){
			LOGGER.info("Initializing the Http Client with credentials provider");	
			builder = HttpClients.custom()
					.setConnectionManager(cm)
					.setDefaultCredentialsProvider(credsProvider)
					.setDefaultRequestConfig(defaultRequestConfig)
					.addInterceptorLast(new RestHeaderInterceptor())
					.addInterceptorLast(new GhixSSORequestInterceptor())
					.addInterceptorFirst(new RequestTargetHost())
					.addInterceptorFirst(new RemoveSoapHeaderInterceptor())
					.addInterceptorFirst(new PreemptiveAuthInterceptor())
					.setRetryHandler(new GiHttpRequestRetryHandler())
					.setConnectionManagerShared(true);
		}else{
			builder = HttpClients.custom()
					.setConnectionManager(cm)
					.setDefaultRequestConfig(defaultRequestConfig)
					.addInterceptorFirst(new RequestTargetHost())
					.addInterceptorLast(new RestHeaderInterceptor())
					.addInterceptorLast(new GhixSSORequestInterceptor())
					.addInterceptorFirst(new RemoveSoapHeaderInterceptor())
					.setRetryHandler(new GiHttpRequestRetryHandler())
					.setConnectionManagerShared(true);
		}
		httpClient= builder.build();
		location = GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION;
		file = GhixPlatformConstants.PLATFORM_KEY_STORE_FILE;
		password = GhixPlatformConstants.PLATFORM_KEYSTORE_PASS;
		initiazing=false;
		refreshKeyStore();
		return httpClient;
	}
	
	public static HttpRequestFactory getRequestFactory() {
		if(factory == null) {
			factory = new SecureHttpClient.HttpRequestFactory();
		}
		return factory;
		
	}
	
	public static synchronized CloseableHttpClient getHttpClient(String keyStoreLocation, String keyStoreFile, String keyStorePass) {
		while(initiazing){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				//ignored
			}
		}
		if(httpClient != null){
			LOGGER.info("Returning existing Http Client, not initializing it again, earlier initiazed from file:["+file+"] located at ["+location+"]");
			return httpClient;
		}
		if(trustStore == null){
			LOGGER.error("No trust store available, from file:["+file+"] located at ["+location+"], check earlier errors");
			throw new GIRuntimeException("Failed to initialize secure HttpClient");
		}
		initiazing = true;
		LOGGER.info("Initializing Secure HTTP Client using KeyStore:"+keyStoreFile+" located at: "+keyStoreLocation);
		loadKeyStore(keyStoreLocation,keyStoreFile,keyStorePass);
		PlainConnectionSocketFactory psf = PlainConnectionSocketFactory
				.getSocketFactory();
		
		Registry<ConnectionSocketFactory> registry = null;
		try {
			registry = RegistryBuilder
					.<ConnectionSocketFactory> create().register("https", getSSLConnectionSocketFactory(trustStore, keyStorePass,keyStoreLocation))
					.register("http", psf).build();
		} catch (KeyManagementException | UnrecoverableKeyException
				| NoSuchAlgorithmException | KeyStoreException e) {
			throw new GIRuntimeException("Failed to initialize secure HttpClient");
		}
		PoolingHttpClientConnectionManager cm = new PoolingConnectionManagerProxy(
				registry);
		cm.setMaxTotal(MAX_CONNECTIONS);
		cm.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
		HttpClientBuilder builder = HttpClients.custom()
				.setConnectionManager(cm)
				.setDefaultRequestConfig(defaultRequestConfig)
				.addInterceptorFirst(new GhixSSORequestInterceptor())
				.setRetryHandler(new GiHttpRequestRetryHandler())
				.setConnectionManagerShared(true);
		httpClient= builder.build();
		initiazing = false;
		location = keyStoreLocation;
		file = keyStoreFile;
		password=keyStorePass;
		refreshKeyStore();
		return httpClient;
	}
	
	private static class GiHttpRequestRetryHandler implements HttpRequestRetryHandler {
		public boolean retryRequest(IOException exception, int executionCount,
				HttpContext context) {
			HttpHost targetHost;
			HttpClientContext clientContext = HttpClientContext.adapt(context);
			targetHost = clientContext.getTargetHost();
			String exceptionMessage = "";
			if(exception != null){
				exceptionMessage = exception.getMessage();
			}
			boolean shouldRetry = false;
			if (executionCount >= MAX_EXECUTIONS) {
				LOGGER.error("Maximum retries reached, not able to connect to ["+targetHost.getHostName()+"], No retrying");
				return shouldRetry;
			}
			if (exception instanceof HttpHostConnectException) {
				LOGGER.error("Connect Failed with:"+exceptionMessage+", aborting request to ["+targetHost.getHostName()+"]");
				shouldRetry = false;
			}
			if (exception instanceof InterruptedIOException) {
				LOGGER.error("Connection Timeout with:"+exceptionMessage+", aborting request to ["+targetHost.getHostName()+"]");
				shouldRetry = true;
			}
			if (exception instanceof UnknownHostException) {
				LOGGER.error("Request failed with:"+exceptionMessage+", aborting connect to ["+targetHost.getHostName()+"]");
				shouldRetry = false;
			}
			if (exception instanceof ConnectTimeoutException) {
				LOGGER.error("Connection refused by the server or server is busy ["+targetHost.getHostName()+"] failed with:"+exceptionMessage+",");
				shouldRetry = true;
			}
			if (exception instanceof SSLException) {
				LOGGER.error("SSL handshake failed, aborting connection to server ["+targetHost.getHostName()+"] Error:["+exception.getMessage()+"]");
				shouldRetry = false;
			}
			if (exception instanceof org.apache.http.NoHttpResponseException) {
	            LOGGER.warn("No response from server on " + executionCount + " call");
	            return true;
	        }
			HttpRequest request = clientContext.getRequest();
			boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
			if (idempotent && shouldRetry) {
				// Retry if the request is considered idempotent and there is a valid reason to retry
				shouldRetry = true;
			}else{
				shouldRetry = false;
			}
			LOGGER.error("Retrying connection to "+targetHost.getHostName()+" attempt # ["+executionCount+"]");
			return shouldRetry;
		}
	}
	
	/**
	 * Following methods used for unit testing purposes only.
	 * @param args
	 * @throws Exception
	 */
//	public static void main(String[] args) throws Exception{
//		//GhixPlatformConstants.PLATFORM_KEY_ALIAS = "gidev";
//		GhixPlatformConstants.PLATFORM_KEY_STORE_FILE="formal_testing_keystore_dev.jks";
//		GhixPlatformConstants.PLATFORM_KEYSTORE_PASS="ghix1234";
//		GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION="c:"+File.separatorChar+"sslcert";
//		GhixPlatformConstants.SSOHOST_IP_LIST="localhost";
//		//String x = getGETData("https://ghix-ws-client:4443/ab=true");
//		System.out.println("*********** Response received:"+x+"    *************");
//	}
	
	
//	public static String getGETData(final String url) throws Exception {
//
//		HttpGet httpGet = null;
//		String httpResponse = null;
//		Exception ex = null;
//		try {
//			httpGet = new HttpGet(url);
//			httpGet.addHeader("Host", "localhost:4443");
//			httpResponse = getHttpClient().execute(httpGet,getResponseHandler());
//		} catch (IOException | GIRuntimeException e) {
//			if(httpGet != null){
//				httpGet.abort();
//				httpGet.releaseConnection();
//			}
//			ex = e;
//		} 
//		if(ex != null){
//			throw ex;
//		}
//		return httpResponse;
//	}
	
	public static ResponseHandler<String> getResponseHandler(){
		
		ResponseHandler<String> handler = new ResponseHandler<String>() {

            public String handleResponse(
                    final HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                	//LOGGER.error("Request Failed with status "+response.getStatusLine());
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }

        };
        return handler;
	}
	
	private static class HttpRequestFactory extends HttpComponentsClientHttpRequestFactory {

		private HttpClientContext context = null;
		
		HttpRequestFactory(){
			super(httpClient);
		}
		
		protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {
			context = HttpClientContext.create();
			if(GhixPlatformConstants.OUTBOUND_REQUEST_TIMEOUT > 0) {
				defaultRequestConfig = RequestConfig.custom()
						.setConnectionRequestTimeout(1000)
						.setConnectTimeout(3000)
						.setSocketTimeout(GhixPlatformConstants.OUTBOUND_REQUEST_TIMEOUT).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
			}
			context.setRequestConfig(defaultRequestConfig);
			return context;
		}
	}
}

