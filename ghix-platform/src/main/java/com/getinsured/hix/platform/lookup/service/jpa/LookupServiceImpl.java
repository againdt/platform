package com.getinsured.hix.platform.lookup.service.jpa;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.lookup.repository.ILookupRepository;
import com.getinsured.hix.platform.lookup.service.LookupService;

/**
 * @author ajinkya m
 * 
 */
@Service("lookupService")
public class LookupServiceImpl implements LookupService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LookupServiceImpl.class);

	@Autowired
	private ILookupRepository lookupRepository;

	List<LookupValue> lookupList;

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#typeName}", unless="#result == null")
	public List<LookupValue> getLookupValueList(String typeName) {
		lookupList = lookupRepository.getLookupValueList(typeName);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	
	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#typeName}", unless="#result == null")
	public List<LookupValue> getLookupValueList(String typeName, String language) {
		lookupList = lookupRepository.getLookupValueList(typeName, language);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueCode}", unless="#result == null")
	public String getLookupValueLabel(String name, String lookupValueCode) {
		return lookupRepository.getLookupValueLabelByNameAndLookupValueCode(
				name, lookupValueCode);
	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueCode, #language}", unless="#result == null")
	public String getLookupValueLabel(String name, String lookupValueCode, String language) {
		return lookupRepository.getLookupValueLabelByNameAndLookupValueCode(name, lookupValueCode,  language);
	}
	
	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueLabel}", unless="#result == null")
	public String getLookupValueCode(String name, String lookupValueLabel) {
		return lookupRepository.getLookupValueCodeByNameAndLookupValueLabel(
				name, lookupValueLabel);

	}
	

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueCode}", unless="#result == null")
	public List<LookupValue> getDependentLookupValueList(String name,String lookupValueCode) {
		Integer id = lookupRepository.getlookupValueIdByTypeANDLookupValueCode(name,lookupValueCode);
		return lookupRepository.getDependentLookupValueList(id.toString());

	}
	
	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueCode,#language}", unless="#result == null")
	public List<LookupValue> getDependentLookupValueList(String name,String lookupValueCode, String language) {
		Integer id = lookupRepository.getlookupValueIdByTypeANDLookupValueCode(name,lookupValueCode, language);
		return lookupRepository.getDependentLookupValueList(id.toString());

	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueCode}", unless="#result == null")
	@Transactional(readOnly = true)
	public Integer getlookupValueIdByTypeANDLookupValueCode(String name,String lookupValueCode){
		return lookupRepository.getlookupValueIdByTypeANDLookupValueCode(name,lookupValueCode) ;
	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#name,#lookupValueCode,#language}", unless="#result == null")
	@Transactional(readOnly = true)
	public Integer getlookupValueIdByTypeANDLookupValueCode(String name,String lookupValueCode,  String language){
		return lookupRepository.getlookupValueIdByTypeANDLookupValueCode(name,lookupValueCode, language) ;
	}
	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#id}", unless="#result == null")
	public LookupValue findLookupValuebyId(int id) {

		return lookupRepository.findByLookupValueId(id);
	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#type,#lookupValueCode}", unless="#result == null")
	@Transactional(readOnly = true)
	public LookupValue getlookupValueByTypeANDLookupValueCode(String type,String lookupValueCode){
		if (type ==null || lookupValueCode == null){
			return null;
		}
		return lookupRepository.getlookupValueByTypeANDLookupValueCode(type, lookupValueCode);
	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#type,#lookupValueCode,#language}", unless="#result == null")
	@Transactional(readOnly = true)
	public LookupValue getlookupValueByTypeANDLookupValueCode(String type,String lookupValueCode, String language){
		return lookupRepository.getlookupValueByTypeANDLookupValueCode(type, lookupValueCode, language);
	}
	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#type,#lookupValueLabel}", unless="#result == null")
	@Transactional(readOnly = true)
	public LookupValue getlookupValueByTypeANDLookupValueLabel(String type,String lookupValueLabel){
		return lookupRepository.getlookupValueByTypeANDLookupValueLabel(type , lookupValueLabel);
	}
	
	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#type,#lookupValueLabel,#language}", unless="#result == null")
	@Transactional(readOnly = true)
	public LookupValue getlookupValueByTypeANDLookupValueLabel(String type,String lookupValueLabel, String language){
		return lookupRepository.getlookupValueByTypeANDLookupValueLabel(type , lookupValueLabel, language);
	}

	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#typeName}", unless="#result == null")
	public List<LookupValue> getLookupValueListForCountiesServed(String typeName) {
		List<LookupValue> listLookupValue = null;
		try{
			listLookupValue = lookupRepository.getLookupValueListForCountiesServed(typeName);
		}catch(Exception e){
			LOGGER.error(""+e);
		}
		if (listLookupValue != null && listLookupValue.size() > 0) {
			return listLookupValue;
		} else {
			return new ArrayList<LookupValue>();
		}
	}

	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#typeName,#language}", unless="#result == null")
	public List<LookupValue> getLookupValueListForCountiesServed(String typeName, String language) {
		List<LookupValue> listLookupValue = null;
		try{
			listLookupValue = lookupRepository.getLookupValueListForCountiesServed(typeName, language);
		}catch(Exception e){
			LOGGER.error(""+e);
		}
		if (listLookupValue != null && listLookupValue.size() > 0) {
			return listLookupValue;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	/**
	 * @see com.getinsured.hix.platform.lookup.service.LookupService#populateLanguageNames(java.lang.String)
	 */
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#lookUpTerm}", unless="#result == null")
	public List<String> populateLanguageNames(String lookUpTerm) {
		List<LookupValue> languageLookupValues = null;
		List<String> languageList = new ArrayList<String>();

		try {
			if(null == lookUpTerm) {
				lookUpTerm = "";
			}
			
			languageLookupValues = lookupRepository.getLookupValueListForLanguages(lookUpTerm.trim());
			
			if(null == languageLookupValues || languageLookupValues.isEmpty()) {
				LOGGER.warn("No languages found in Look Up table.");
			}
			else {
				for(LookupValue languageLookupValue: languageLookupValues) {
					if(null != languageLookupValue && null != languageLookupValue.getLookupValueLabel() && !languageLookupValue.getLookupValueLabel().trim().isEmpty()) {
						languageList.add(languageLookupValue.getLookupValueLabel().trim());
					}
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred while populating looked up language list. Exception: " + ex.getMessage(), ex);
		}

		return languageList;
	}
	
	/**
	 * @see com.getinsured.hix.platform.lookup.service.LookupService#populateLanguageNames(java.lang.String)
	 */
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#lookUpTerm,#language}", unless="#result == null")
	public List<String> populateLanguageNames(String lookUpTerm, String language) {
		List<LookupValue> languageLookupValues = null;
		List<String> languageList = new ArrayList<String>();

		try {
			if(null == lookUpTerm) {
				lookUpTerm = "";
			}
			
			languageLookupValues = lookupRepository.getLookupValueListForLanguages(lookUpTerm.trim(), language);
			
			if(null == languageLookupValues || languageLookupValues.isEmpty()) {
				LOGGER.warn("No languages found in Look Up table.");
			}
			else {
				for(LookupValue languageLookupValue: languageLookupValues) {
					if(null != languageLookupValue && null != languageLookupValue.getLookupValueLabel() && !languageLookupValue.getLookupValueLabel().trim().isEmpty()) {
						languageList.add(languageLookupValue.getLookupValueLabel().trim());
					}
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred while populating looked up language list. Exception: " + ex.getMessage(), ex);
		}

		return languageList;
	}
	
/*	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#typeName}", unless="#result == null")
	public List<LookupValue> getLookupValueListForAeeStatus(String typeName) {
		lookupList = lookupRepository.getLookupValueListForAeeStatus(typeName);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}*/
	
	/*@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#typeName,#language}", unless="#result == null")
	public List<LookupValue> getLookupValueListForAeeStatus(String typeName, String language) {
		lookupList = lookupRepository.getLookupValueListForAeeStatus(typeName, language);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}*/
	
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#typeName}", unless="#result == null")
	public List<LookupValue> getLookupValueListEligibiltyStatus(String typeName) {
		lookupList = lookupRepository.getLookupValueListForEligibiltyStatus(typeName);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#typeName,#language}", unless="#result == null")
	public List<LookupValue> getLookupValueListEligibiltyStatus(String typeName, String language) {
		lookupList = lookupRepository.getLookupValueListForEligibiltyStatus(typeName, language);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#name}", unless="#result == null")
	public List<LookupValue> getLookupValueListForHoursOfOperation(String name){
		List<LookupValue> lookupList = null;
		lookupList = lookupRepository.getLookupValueListForHoursOfOperation(name);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#name,#language}", unless="#result == null")
	public List<LookupValue> getLookupValueListForHoursOfOperation(String name, String language){
		List<LookupValue> lookupList = null;
		lookupList = lookupRepository.getLookupValueListForHoursOfOperation(name, language);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#name}", unless="#result == null")
	public List<String> getLookupValueLabelList(String name){
		List<String> lookupList = null;
		lookupList = lookupRepository.getLookupValueLabelList(name);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<String>();
		}
	}

	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#name,#language}", unless="#result == null")
	public List<String> getLookupValueLabelList(String name, String language){
		List<String> lookupList = null;
		lookupList = lookupRepository.getLookupValueLabelList(name, language);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<String>();
		}
	}
	@Override
	@Cacheable(value ="lookupvalue", unless="#result == null")
	public List<LookupValue> getLookupValueListForEnrollmentStatusEdit(){
		List<LookupValue> lookupList = null;
		lookupList = lookupRepository.getLookupValueListForEnrollmentStatusEdit("ENROLLMENT_STATUS");
		return lookupList;
	}
	
	@Override
	@Cacheable(value ="lookupvalue", key = "{#root.methodName,#language}",unless="#result == null")
	public List<LookupValue> getLookupValueListForEnrollmentStatusEdit(String language){
		List<LookupValue> lookupList = null;
		lookupList = lookupRepository.getLookupValueListForEnrollmentStatusEdit("ENROLLMENT_STATUS", language);
		return lookupList;
	}
	
	@Override
	public List<LookupValue> getLookupValueListForBOBFeed(String typeName, List<String> lookupValueCode) {
		lookupList = lookupRepository.getLookupValueListForBOBFeed(typeName, lookupValueCode);

		if (lookupList != null && lookupList.size() > 0) {
			return lookupList;
		} else {
			return new ArrayList<LookupValue>();
		}
	}
	
	@Override
	@Cacheable(value ="lookupvalue", unless="#result == null")
	public List<LookupValue> getLookupValueListForAppEvents(String typeName) {
		List<LookupValue> listLookupValue = null;
		try{
			listLookupValue = lookupRepository.getLookupValueListForAppEvents(typeName);
		}catch(Exception e){
			LOGGER.error(""+e);
		}
		if (listLookupValue != null && listLookupValue.size() > 0) {
			return listLookupValue;
		} else {
			return new ArrayList<LookupValue>();
		}
	}

	@Override
	@Cacheable(value="lookupvalue", key="{#root.methodName,#lookupCode,#parentLookupCode,#parentLookupTypeName}", unless="#result == null")
	@Transactional(readOnly = true)
	public LookupValue getlookupValueBylookupCodeAndParentLookupCode(String lookupValueCode, String parentLookupCode,
			String parentLookupTypeName) {
		return lookupRepository.getlookupValueBylookupCodeAndParentLookupCode(lookupValueCode, parentLookupCode, parentLookupTypeName);
	}

}
