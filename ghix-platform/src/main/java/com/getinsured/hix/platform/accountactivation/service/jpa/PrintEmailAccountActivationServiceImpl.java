package com.getinsured.hix.platform.accountactivation.service.jpa;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.UIConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.notices.jpa.B64ImgReplacedElementFactory;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;


@Service
@Qualifier("printEmailAccountActivationServiceImpl")
//@Scope("step")
public class PrintEmailAccountActivationServiceImpl extends AccountActivationServiceImpl {

	private static final String PRINTABLE = "Y";

	private static final String OMIT = "omit";

	private static final String UTF_8 = "UTF-8";
	private static final String findByKeyNameAndKeyIdQuery = "FROM Notice as notice where notice.keyId = :keyId and notice.keyName = :keyName"; 

	@Autowired private ContentManagementService ecmService;
	@PersistenceUnit private EntityManagerFactory emf;
	
	private boolean saveNotice(Notice noticeObj) {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			em.persist(noticeObj);
			em.getTransaction().commit();
			em.clear();
			
		}catch(Exception e) {
			LOGGER.error("Error saving the notice in the database failes with {}",e.getMessage(),e);
			return false;
		}finally {
			if(em != null){
				em.close();
			}
		}
		return true;
	}
	
	private Notice findNoticeByKeyNameAndKeyId(String keyName, int keyId, EntityManager em ) {
		Notice notice = null;
		Query query = em.createQuery(findByKeyNameAndKeyIdQuery);
		query.setParameter("keyName".intern(), keyName);
		query.setParameter("keyId".intern(), keyId);
		@SuppressWarnings("unchecked")
		List<Notice> noticeList = query.getResultList();
		if(noticeList.size() > 0) {
			notice= noticeList.get(0);
		}else {
			LOGGER.info("No notice found for keyname {} with id {}",keyName,keyId);
		}
		return notice;
	}
	
	
	
	
	
	@Override
	public AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject, Integer expirationDays) throws GIException {
		NotificationAgent activationEmailType = null;
		//1. perform basic incoming data validation
		validateIncomingData(createdObject, creatorObject);

		//2. form ActivationJson object
		ActivationJson jsonObject = createActivationJson(createdObject, creatorObject);

		//3a. get actual repository by invoking
		setRepository(jsonObject.getRepositoryName());

		//3b. check whether created.ObjectId exist in db for respective  objectType
		boolean isExists = ghixCustomRepository.recordExists(createdObject.getObjectId());

		if (!isExists){
			throw new IllegalArgumentException("No record found for createdObjectId - " + createdObject.getObjectId());
		}

		//4. convert it to JSON string
		String jsonString = gson.toJson(jsonObject);

		//5. generate secure random token
		String randomToken = generateRandomToken();

		//6. create new AccountActivation object & populate details & persist record
		AccountActivation accountActivation = formAccountActivationEntity(jsonString, randomToken, createdObject, creatorObject,expirationDays);//HIX-30523
		accountActivation = iAccountActivationRepository.save(accountActivation);

		//7. initiate email sending process
		LOGGER.info("Sending Account Activation e-mail or Print or Both...");
		try {
			if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("emailType")){
				String emailClassName = createdObject.getCustomeFields().get("emailType");
				if(StringUtils.isEmpty(emailClassName)){
					throw new GIException("Valid Email Type is missing");
				}
				activationEmailType = (NotificationAgent) GHIXApplicationContext.getBean(emailClassName);
			}else{
				activationEmailType = accountActivationEmail;
			}

			Location location = getLocationObject(createdObject);
			sendAccountActivation(activationEmailType,accountActivation,jsonObject, location, createdObject );
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("Account Activation e-mail cannot be sent - " + e.getMessage());
			throw new GIException("Account Activation e-mail cannot be sent. Please contact the administrator.", e);
		}
		return accountActivation;
	}



	@Override
	protected void validateCreatedObjectData(CreatedObject createdObject) {

		if (createdObject == null){
			throw new IllegalArgumentException("createdObject cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getFullName())){
			throw new IllegalArgumentException("createdObject.FullName cannot be blank");
		}

		if (createdObject.getObjectId() == null){
			throw new IllegalArgumentException("createdObject.ObjectId cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getRoleName())){
			throw new IllegalArgumentException("createdObject.RoleName cannot be blank");
		}

		boolean isEmailIDMissing = false;
		if (StringUtils.isEmpty(createdObject.getEmailId())){
			isEmailIDMissing = true;
		}

		boolean isLocationMissing = getLocation(createdObject);

		if (isEmailIDMissing && isLocationMissing){
			throw new IllegalArgumentException("createdObject.EmailId or createdObject.location cannot be blank");
		}
	}

	private boolean getLocation(CreatedObject createdObject) {
		Location location = getLocationObject(createdObject);
		return isLocationNull(location);
	}


	private Location getLocationObject(CreatedObject createdObject) {
		Location location = null;
		if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("location")){
			String locationJson = createdObject.getCustomeFields().get("location");
			location = gson.fromJson(locationJson, Location.class);
		}
		return location;
	}

	private boolean isLocationNull(Location location) {
		boolean isLocationMissing = false;
		if (location == null){
			isLocationMissing = true;
		} else {
			if (StringUtils.isEmpty(location.getAddress1()) || StringUtils.isEmpty(location.getCity()) || StringUtils.isEmpty(location.getState())
					|| StringUtils.isEmpty(location.getZip())){
				isLocationMissing = true;
			}
		}
		return isLocationMissing;
	}

	private void sendAccountActivation(NotificationAgent activationEmailType, AccountActivation accountActivation, ActivationJson jsonObject, Location location, CreatedObject createdObject) throws NotificationTypeNotFound  {
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("ACTIVATION_JSON", jsonObject);
		notificationContext.put("ACCOUNT_ACTIVATION", accountActivation);
		notificationContext.put("LOCATION", location);
		notificationContext.put("KeyId", String.valueOf(accountActivation.getId()));
		notificationContext.put("KeyName", Notice.KEY_NAME.ACCOUNT_ACTIVATION.toString());
		Map<String, String> tokens = activationEmailType.getTokens(notificationContext);
		Map<String, String> emailData = activationEmailType.getEmailData(notificationContext);
		
		Notice noticeObj = activationEmailType.generateEmailNoSave(activationEmailType.getClass().getSimpleName(),location, emailData, tokens);

		if (location != null){
			byte[] pdfBytes = null;
			try {
				pdfBytes = generatePdf(noticeObj.getEmailBody());
			} catch (IOException | NoticeServiceException e) {
				throw new GIRuntimeException("Error creating PDF using flying saucer - " + e);
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Done generating PDF");
			}
			String relativePath = "accountactivation/" + createdObject.getRoleName() +"/";
			String ecmFileName = createdObject.getRoleName() + "_AccountActivation_" + createdObject.getObjectId() + (new TSDate().getTime()) + ".pdf";

			String ecmDocId = null;
			try {
				ecmDocId = ecmService.createContent(relativePath, ecmFileName, pdfBytes, ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.NOTICE, null);
				noticeObj.setStatus(STATUS.PDF_GENERATED);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Done saving PDF to external system, sending email");
				}
			} catch (ContentManagementServiceException e) {
				LOGGER.error("Failed to save the email content to external system continuing sending the email",e );
			}
			if (!StringUtils.isEmpty(createdObject.getEmailId())){
				noticeObj = activationEmailType.sendEmailNoSave(noticeObj);
				if(noticeObj.getStatus() == Notice.STATUS.FAILED) {
					LOGGER.error("Email send failed but content saved in external system with id {}",ecmDocId);
				}
			}
			noticeObj.setEcmId(ecmDocId);
			noticeObj.setPrintable(PRINTABLE);
			this.saveNotice(noticeObj);
		}
		
	}

	/**
	 *
	 * @param accountActivation
	 * @return
	 */
	@Override
	public int sendActivationReminders(AccountActivation accountActivation){
		NotificationAgent activationEmailType = null;
		EntityManager em = null;
		if (accountActivation == null || accountActivation.getId() == 0 || StringUtils.isEmpty(accountActivation.getJsonString())){
			throw new GIRuntimeException("Invalid data found for accountActivation ");
		}

		String jsonString = accountActivation.getJsonString();
		ActivationJson jsonObject = gson.fromJson(jsonString, ActivationJson.class);

		CreatedObject createdObject = jsonObject.getCreatedObject();
		if(createdObject .getCustomeFields() != null && createdObject.getCustomeFields().containsKey("emailType")){
			String emailClassName = createdObject.getCustomeFields().get("emailType");
			if(StringUtils.isEmpty(emailClassName)){
				throw new GIRuntimeException("Valid Email Type is missing");
			}
			activationEmailType = (NotificationAgent) GHIXApplicationContext.getBean(emailClassName);
		}else{
			activationEmailType = accountActivationEmail;
		}
		try {
			em = this.emf.createEntityManager();
			Notice notice = findNoticeByKeyNameAndKeyId(Notice.KEY_NAME.ACCOUNT_ACTIVATION.toString(), accountActivation.getId(), em);
			if(notice == null) {
				throw new GIRuntimeException("No Notice found for account activation for - " + accountActivation.getId());
			}
			Location location = getLocationObject(createdObject);
	
			if (!StringUtils.isEmpty(notice.getToAddress())){
				notice = activationEmailType.sendEmailNoSave(notice);
				if(notice.getStatus() ==  STATUS.FAILED) {
					throw new GIRuntimeException("Email send failed");
				}
				if (location != null){
					notice.setPrintable(PRINTABLE);
					notice.setCreated(new TSDate());
					em.merge(notice);
				}
			}
			
		}
		finally{
			if(em != null) {
				em.flush();
				em.close();
			}
		}
		return 0;
	}

	private byte[] generatePdf(String incomingData) throws IOException, NoticeServiceException {

		String serverLogoUrl = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL+"resources/img/"+DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE);
		String localLogoUrl = GhixPlatformEndPoints.GHIXHIX_SERVICE_URL+"resources/img/"+DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE);
		String finalData = new String(StringUtils.replace(formData(incomingData), serverLogoUrl, localLogoUrl));


		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try{
			/** If resources (images, css, etc) are not found,
			 * then ITextRenderer logs "<strong>java.io.IOException: Stream closed</strong>" exception without throwing it
			 * So we can't catch such exception thrown by 3rd party jar
			 *
			 * Also, stack trace gets printed in the PDF on Alfresco
			 *
			 */
			ITextRenderer renderer = new ITextRenderer();
			renderer.getSharedContext().setReplacedElementFactory(new B64ImgReplacedElementFactory(renderer.getSharedContext().getReplacedElementFactory()));
			renderer.setDocumentFromString(finalData);
			renderer.layout();
			renderer.createPDF(os);
		}catch(Exception e){
			throw new NoticeServiceException(e);
		}
		finally{
			os.close();
		}

		return os.toByteArray();
	}

	private String formData(String data) throws IOException {

		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType(OMIT);
		tidy.setXHTML(true);
		tidy.setInputEncoding(UTF_8);
		tidy.setOutputEncoding(UTF_8);
		tidy.parse(new StringReader(data), writer);
		writer.close();
		return writer.toString();
	}

}
