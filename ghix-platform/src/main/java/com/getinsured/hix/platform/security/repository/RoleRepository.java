package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Role;

@Repository
@Transactional(readOnly = true)
public class RoleRepository {
	@Autowired
	private IRoleRepository roleRepository;
	
	public Role findById(Integer id) {
		if( roleRepository.exists(id) ){
			return roleRepository.findById(id);
		}
		return null;
    }

	public List<Role> findAll() {
       return roleRepository.findAll();
    }
	
	public Page<Role> findAll(Pageable pageable) {
	       return roleRepository.findAll(pageable);
	}
	
    @Transactional
    public Role save(Role role) {
            return roleRepository.save(role);
    }
    
    
    public Role findIdByName(String nm){
    	return roleRepository.findIdByName(nm);
    }
}
