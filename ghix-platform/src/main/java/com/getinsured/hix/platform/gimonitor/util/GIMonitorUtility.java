package com.getinsured.hix.platform.gimonitor.util;

import com.getinsured.hix.dto.exceptions.GIMonitorDTO;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.couchbase.dto.gimonitor.GIMonitorDocument;

public class GIMonitorUtility {

	public static final String GI_MONITOR_COUCHBASE_CATEGORY = "PLAT";
	public static final String GI_MONITOR_COUCHBASE_SUB_CATEGORY_EXPN = "EXPN";
	public static final String GI_MONITOR_COUCHBASE_SUB_CATEGORY_ERCD = "ERCD";
	public static final String GI_MONITOR_COUCHBASE_TYPE = "JSON";

	public static GIMonitor convertToGIMonitorDomainObject(GIMonitorDTO giMonitorDTO) {

		GIMonitor giMonitor = new GIMonitor();

		giMonitor.setComponent(giMonitorDTO.getComponent());
		giMonitor.setCreationTimeStamp(giMonitorDTO.getCreationTimeStamp());
		giMonitor.setEventTime(giMonitorDTO.getEventTime());
		giMonitor.setException(giMonitorDTO.getException());
		giMonitor.setExceptionStackTrace(giMonitorDTO.getExceptionStackTrace());
		giMonitor.setUrl(giMonitorDTO.getUrl());
		giMonitor.setUser(giMonitorDTO.getUser());
		// FIXME: Deprecate error code
		// giMonitor.setErrorCode((giMonitorDTO.getErrorCode()));

		return giMonitor;
	}

	/**
	 * Creates GIMonitor document from GIMonitor
	 * 
	 * @param giMonitor
	 * @return GIMonitor document
	 */
	public static GIMonitorDocument convertToGIMonitorDocument(GIMonitorDTO giMonitorDTO) {

		GIMonitorDocument document = new GIMonitorDocument();

		document.setUrl(giMonitorDTO.getUrl());
		document.getMetaData().setCreationDate(giMonitorDTO.getCreationTimeStamp());
		document.getMetaData().setModifiedDate(giMonitorDTO.getEventTime());
		document.setComponent(giMonitorDTO.getComponent());
		document.setException(giMonitorDTO.getException());
		document.setExceptionStackTrace(giMonitorDTO.getExceptionStackTrace());
		document.setErrorCode(giMonitorDTO.getErrorCode());
		if (giMonitorDTO.getUser() != null) {
			String userName = giMonitorDTO.getUser().getUsername();
			document.getMetaData().setModifiedBy(userName);
			document.getMetaData().setCreatedBy(userName);
		}
		document.getMetaData().setCategory(GI_MONITOR_COUCHBASE_CATEGORY);
		document.getMetaData().setSubCategory(GI_MONITOR_COUCHBASE_SUB_CATEGORY_EXPN);
		//document.setType(GI_MONITOR_COUCHBASE_TYPE);

		return document;
	}

}
