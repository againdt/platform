package com.getinsured.hix.platform.um.handlers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.atlassian.httpclient.api.HttpStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.handler.HandlerConstants;
import com.getinsured.hix.platform.handler.RequestHandler;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Component
@DependsOn("configProp")
public class UpdateUserHandler implements RequestHandler {
	public static final String NAME="UPDATE_USER";
	public static final String UPDATE_PARAMETERS_KEY = "UPDATE_PARAMETERS_KEY";
	public static final int EMAIL_UPDATE = 0;
	public static final String UPDATE_USER_OPERATION_KEY = "UPDATE_USER_OPERATION";
	public static final String CURRENT_PASSWORD = "CURRENT_PASSWORD";
	
	public static final int UPDATE_USER = 1;
	public static final int UPDATE_USER_NAME = 2;
	public static final String NEW_USER_NAME = "new_user_name"; 

	
	@Autowired
	private HttpClient restHttpClient;
	@Autowired
	private SCIMUserManager scimUserManager;
	
	
	private Logger logger = LoggerFactory.getLogger(UpdateUserHandler.class);
	
	@Value("#{configProp['platform.im.wso2environment'] != null ? configProp['platform.im.wso2environment'] : '5.0'}")
	private String wso2Environment;
	
	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public List<String> getHandlerPath() {
		return null;
	}

	@Override
	public boolean handleRequest(HashMap<String, Object> context, ServletRequest request, boolean async) {
		boolean success = true;
		boolean wso2Enabled = (Boolean)context.get(WSO2_ENABLED_KEY);
		if(logger.isInfoEnabled()){
			logger.info("Handling pre user create event WSO2 enabled:\""+wso2Enabled+"\"");
		}
		AccountUser user = (AccountUser)context.get(ACCOUNT_USER_KEY);
		if(wso2Enabled){
			success = this.handleWso2Environment(user, context);
		}
		return success;
	}
	
	private boolean updateUserOnWSO2(HashMap<String, Object> context) {
		boolean success = false;
		logger.info("Updating user  for remotely managed user");
		AccountUser user = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		JSONParser parser = new JSONParser();
		switch(wso2Environment){
		case WSO2_ENV_5_3:
		{
			String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"scim/tenant/user/update";
			if(logger.isDebugEnabled()){
				if(uri.indexOf("gi-identity") == -1){
					logger.warn("User update endpoint generally points to \"gi-identity\" component and URL does not seems to be right, please check");
				}
				logger.debug("Using WSO2 Environment 5.3 for URI:"+uri);
			}
			String requestPayload = createUpdateTenantUserRequest(user, context);
			if(requestPayload == null){
				return false;
			}
			if(logger.isTraceEnabled()){//PII LOG
				logger.trace("Sending update user payload:"+requestPayload);
			}
			HttpPost post = new HttpPost(uri);
			HttpEntity payload = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(requestPayload).build();
			post.setEntity(payload);
			try {
				CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
				int status = response.getStatusLine().getStatusCode();
				if(HttpStatus.OK.code != status){
					logger.error("Error response received with status:"+status);
					context.put(HandlerConstants.HANDLER_ERROR, Utils.getResponseString(response.getEntity()));
				}else{
					success= true;
					String updateUserResponse = Utils.getResponseString(response.getEntity());
					JSONObject createUserResponseObj = (JSONObject) parser.parse(updateUserResponse);
					String id = (String) createUserResponseObj.get("id");
					if(logger.isDebugEnabled()){
						logger.debug("Created user with id:"+id);
					}
				}
				response.close();
			} catch (IOException | ParseException e) {
				logger.error("Error creating user",e);
				success = false;
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
			}
			break;
		}
		case WSO2_ENV_5_0:
		{
			if(logger.isDebugEnabled()){
				logger.debug("Using WSO2 Environment 5.0 makse sure we have IEX builds running for Idaho");
			}
			try {
				scimUserManager.updateUser(user);
				success= true;
			} catch (GIException e) {
				logger.error("Error creating user",e);
				success = false;
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
			}
			break;
		}
		default:
			context.put(HandlerConstants.HANDLER_ERROR, "Un supported environment for WSO2 "+wso2Environment);
		}
		
		return success;
	}
	
	private boolean updateUserNameOnWSO2(HashMap<String, Object> context) {
		boolean success = false;
		logger.info("Updating user  for remotely managed user");
		AccountUser user = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		JSONParser parser = new JSONParser();
		switch(wso2Environment){
		case WSO2_ENV_5_3:
		{
			String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"scim/tenant/user/update/username";
			if(logger.isDebugEnabled()){
				if(uri.indexOf("gi-identity") == -1){
					logger.warn("User update endpoint generally points to \"gi-identity\" component and URL does not seems to be right, please check");
				}
				logger.debug("Using WSO2 Environment 5.3 for URI:"+uri);
			}
			String requestPayload = createUpdateUserNameRequest(user, context);
			if(requestPayload == null){
				return false;
			}
			if(logger.isTraceEnabled()){//PII LOG
				logger.trace("Sending update user payload:"+requestPayload);
			}
			HttpPost post = new HttpPost(uri);
			HttpEntity payload = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(requestPayload).build();
			post.setEntity(payload);
			try {
				CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
				int status = response.getStatusLine().getStatusCode();
				if(HttpStatus.CREATED.code != status){
					logger.error("Error response received with status:"+status);
					context.put(HandlerConstants.HANDLER_ERROR, Utils.getResponseString(response.getEntity()));
				}else{
					success= true;
					String updateUserResponse = Utils.getResponseString(response.getEntity());
					JSONObject createUserResponseObj = (JSONObject) parser.parse(updateUserResponse);
					String id = (String) createUserResponseObj.get("id");
					if(logger.isDebugEnabled()){
						logger.debug("Created user with id:"+id);
					}
					user.setExtnAppUserId(id);
				}
				response.close();
			} catch (IOException | ParseException e) {
				logger.error("Error creating user",e);
				success = false;
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
			}
			break;
		}
		case WSO2_ENV_5_0:
		{
			if(logger.isDebugEnabled()){
				logger.debug("Using WSO2 Environment 5.0 makse sure we have IEX builds running for Idaho");
			}
			try {
				scimUserManager.changeEmail(user);
				success= true;
			} catch (GIException e) {
				logger.error("Error creating user",e);
				success = false;
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
			}
			break;
		}
		default:
			context.put(HandlerConstants.HANDLER_ERROR, "Un supported environment for WSO2 "+wso2Environment);
		}
		
		return success;
	}
	
	private boolean handleWso2Environment(AccountUser user, HashMap<String,Object> context) {
		int operation  = (int)context.get(UPDATE_USER_OPERATION_KEY);
		boolean wso2Env = (Boolean)context.get(HandlerConstants.WSO2_ENABLED_KEY);
		boolean success = false;
		
		
		if(user.getExtnAppUserId() == null){
			context.put(HANDLER_ERROR, "User External App Id is required for remote update");
			return false;
		}

		if(wso2Env){
			switch(operation){
			case UPDATE_USER:
					success = updateUserOnWSO2(context);
					break;
			case UPDATE_USER_NAME:
					success = updateUserNameOnWSO2(context);
				break;
			default:
				context.put(HandlerConstants.HANDLER_ERROR, "Unknown operation "+operation);
				success = false;
			}
		}
		
		return success;
	
	}
	
	@SuppressWarnings("unchecked")
	private void put(String attrName, Object val, boolean mandatory, JSONObject payload){
		if(val == null && mandatory){
			throw new RuntimeException("Mandatory attribute "+attrName+" Not provided");
		}
		if(val != null){
			payload.put(attrName, val);
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	private String createUpdateUserNameRequest(AccountUser accountUser, HashMap<String,Object> context) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		TenantDTO tenantDTO = (TenantDTO)context.get(TENANT_DTO_KEY);
		if(tenantDTO == null){
			context.put(HandlerConstants.HANDLER_ERROR, "Tenant Context not provided");
			return null;
		}
		reqJsonObject = new JSONObject();
		put("tenantAdmin".intern(), tenantDTO.getTenantProvisionigUser(),true,reqJsonObject);
		put("tenantPassword".intern(), tenantDTO.getTenantProvisionigUserPassword(), true, reqJsonObject);
		put("tenantDomain".intern(), tenantDTO.getTenantDomain(),true,reqJsonObject);
	
		payloadJsonObject = new JSONObject();
		put("id".intern(), accountUser.getExtnAppUserId(), true, payloadJsonObject);
		put("userName".intern(), accountUser.getUserName(), true, payloadJsonObject);
		put("new_user_name", context.get(NEW_USER_NAME),true,payloadJsonObject);
		put("password", context.get(CURRENT_PASSWORD),true,payloadJsonObject); 
		
		reqJsonObject.put("payload".intern(), payloadJsonObject);
		if (null != reqJsonObject) {
			request = reqJsonObject.toJSONString();
		}
		return request;
	}

	
	@SuppressWarnings("unchecked")
	private String createUpdateTenantUserRequest(AccountUser accountUser, HashMap<String,Object> context) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		TenantDTO tenantDTO = (TenantDTO)context.get(TENANT_DTO_KEY);
		if(tenantDTO == null){
			context.put(HandlerConstants.HANDLER_ERROR, "Tenant Context not provided");
			return null;
		}
		reqJsonObject = new JSONObject();
		put("tenantAdmin".intern(), tenantDTO.getTenantProvisionigUser(),true,reqJsonObject);
		put("tenantPassword".intern(), tenantDTO.getTenantProvisionigUserPassword(), true, reqJsonObject);
		put("tenantDomain".intern(), tenantDTO.getTenantDomain(),true,reqJsonObject);
		payloadJsonObject = new JSONObject();
		put("id".intern(), accountUser.getExtnAppUserId(), true, payloadJsonObject);
		put("userName".intern(), accountUser.getUserName(), true, payloadJsonObject);
		//put("userPassword".intern(), accountUser.getPassword(),false, payloadJsonObject);
		put("firstName".intern(), accountUser.getFirstName(),false,payloadJsonObject);
		put("lastName".intern(), accountUser.getLastName(),false,payloadJsonObject);
		put("mail".intern(), accountUser.getEmail(),false,payloadJsonObject);
		put("prefMethComm".intern(), "Email".intern(),false,payloadJsonObject);
		put("preferredLanguage".intern(), "English".intern(),false,payloadJsonObject);
		put("secQuestion1".intern(), accountUser.getSecurityQuestion1(),false,payloadJsonObject);
		String hashAnswer =  (String) context.get(HASH_ANSWER_1);
		
		if(hashAnswer != null) {
			put("hashAnswer1".intern(),hashAnswer,false,payloadJsonObject);
		}
		
		put("secAnswer1".intern(), accountUser.getSecurityAnswer1(),false,payloadJsonObject);
		
		hashAnswer =  (String) context.get(HASH_ANSWER_2);
		
		if(hashAnswer != null) {
			put("hashAnswer2".intern(),hashAnswer,false,payloadJsonObject);
		}
		
		put("secQuestion2".intern(), accountUser.getSecurityQuestion2(),false,payloadJsonObject);
		put("secAnswer2".intern(), accountUser.getSecurityAnswer2(),false,payloadJsonObject);
	    put("phoneNumber".intern(), accountUser.getPhone(),true,payloadJsonObject);
	    put("tenantCode".intern(), tenantDTO.getCode(),true,payloadJsonObject);
		reqJsonObject.put("payload".intern(), payloadJsonObject);
		if (null != reqJsonObject) {
			request = reqJsonObject.toJSONString();
		}
		return request;
	}

}
