package com.getinsured.hix.platform.service;

import java.util.List;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Esignature;

public interface EsignatureService {
	
	List<Esignature> findByAccountUser(AccountUser givenUserObj);
	
	public Esignature findByModuleRefId(String moduleName, Integer refId);
	
	public Esignature saveEsignature(Esignature givenEsignatureObj);
	
}
