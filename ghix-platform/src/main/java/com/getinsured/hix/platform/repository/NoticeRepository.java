package com.getinsured.hix.platform.repository;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.IsPrinted;
import com.getinsured.hix.model.Notice.PrintableFlag;
import com.getinsured.hix.platform.util.exception.GIException;

@Repository
@Transactional(readOnly = true)
public class NoticeRepository {

	private static final Logger LOGGER = Logger.getLogger(NoticeRepository.class);
	@Autowired
	private INoticeRepository noticeRepository;

	public void flush(){
		noticeRepository.flush();
	}

	public Notice findById(Integer id) {
		if( noticeRepository.exists(id) ){
			return noticeRepository.findById(id);
		}
		return null;
	}

	public List<Notice> findAll() {
		return noticeRepository.findAll();
	}

	public Page<Notice> findAll(Pageable pageable){
		return noticeRepository.findAll(pageable);
	}

	@Transactional
	public Notice save(Notice notice) {
		return noticeRepository.save(notice);
	}
	
	@Transactional
	public Notice updateIsPrintedFlag(IsPrinted isPrintedFlag, Integer noticeId)throws GIException{
		Notice notice = findById(noticeId);
		if(notice != null){
			notice.setIsPrinted(isPrintedFlag);
			return noticeRepository.saveAndFlush(notice);
		}
		else{
			throw new GIException("Unable to find Notice with ID: "+noticeId);
		}
	}


	public Notice update(Notice notice) {

		int recordsUpdated = noticeRepository.updateNotice(notice.getId(), notice.getSentDate(), notice.getStatus(), notice.getEcmId(), notice.getPrintable(), new TSDate());
		if (recordsUpdated != 1) {
			LOGGER.warn("Updating notice record returned result set other than 1 for notice id - " + notice.getId());
		}

		return findById(notice.getId());

	}

	public List<Notice> findByKeyNameAndKeyId(String keyName, Integer keyId){
		return noticeRepository.findByKeyNameAndKeyId(keyName, keyId);
	}

	public List<Notice> getNoticeByPrintableAndCreated(Date startDate,Date endDate, PrintableFlag flag){
		return noticeRepository.getNoticeByPrintableAndCreated(startDate,endDate,flag.toString());
	}
}

