package com.getinsured.hix.platform.util;


public abstract class SCIMClientConstants {

	public static final String CONTENT_TYPE = "application/json";
	public static final String KEYSTORE_PATH = "path.keystore";
	public static final String KEYSTORE_PASSWORD = "keystore.password";
	public static final String USER_ENDPOINT_URL = "user.endpoint";
	public static final String GROUP_ENDPOINT_URL = "group.endpoint";
	public static final String USER_NAME = "provisioning.user.name";
	public static final String PASSWORD = "provisioning.password";
	public static final String ENABLE_OAUTH = "enable.oauth";
	public static final String ACCESS_TOKEN = "oauth.access.token";
	public static final String USER_FILTER = "filter=userNameEq";
	
	public static final String USER_SCIM_FILTER = "filter=idEq";
	
	public static final String GROUP_FILTER = "filter=displayNameEq";
	public static final String VALUE_KEY = "value";
	public static final String DISPLAY_KEY = "display";
	public static final int USR_CREATED_INT = 201;
	
	//SCIM USER ATTRIBUTES
	public static final String ID_ATTR = "id";
	public static final String USER_NAME_ATTR = "userName";
	public static final String FIRST_NAME_ATTR = "firstName";
	public static final String LAST_NAME_ATTR = "lastName";
	public static final String MAIL_ATTR = "mail";
	public static final String DOB_ATTR = "dateOfBirth";
	public static final String SSN_ATTR = "ssn";
	public static final String PHONE_NUMBER_ATTR = "phoneNumber";
	public static final String PREF_METH_COMM_ATTR = "prefMethComm";
	public static final String PREFERRED_LANGUAGE_ATTR = "preferredLanguage";
	public static final String SECURITY_QUESTION_ATTR = "secQuestion";
	public static final String SECURITY_ANSWER_ATTR = "secAnswer";
	public static final String RIDP_FLAG_ATTR = "ridpFlag";
	public static final String ACCOUNT_TYPE_CODE = "accountTypeCode";
	public static final String WSO2_EXTENSION_ATTR = "wso2Extension";
	public static final String WSO2_EXTENSION_SCHEMA = "urn:scim:schemas:extension:wso2:1.0";
	
	//ERR MSGS
	public static final String WSO2_UNAVAILABLE_MSG = "Unable to connect to WSO2";
	public static final String IO_EXCEPTION_MSG = "Error closing response [";
	public static final String UPDATE_ERR_MSG = "ERR: WHILE UPDATING USR VIA SCIM: ";
	
	
}
