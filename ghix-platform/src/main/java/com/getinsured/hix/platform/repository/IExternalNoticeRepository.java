package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.ExternalNotice;

@Repository
public interface IExternalNoticeRepository extends JpaRepository<ExternalNotice,Integer> {
	

}
