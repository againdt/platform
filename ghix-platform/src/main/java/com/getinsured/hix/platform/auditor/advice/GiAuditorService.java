package com.getinsured.hix.platform.auditor.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.auditor.GiAuditDto;

@Service
public class GiAuditorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GiAuditorService.class);

	@Async
	public void doAudit(GiAuditDto giAuditDto){
		LOGGER.info(giAuditDto.toString());
	}

}
