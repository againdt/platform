package com.getinsured.hix.platform.ecm;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "url_mime_config")
public class URLMimeConfig {
	private String url = null;
	private List<MimeString> allowedMimeTypes = null;
	private String strRep = null;
	
	@XmlAttribute(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@XmlElement(name="mime_string")
	public List<MimeString> getAllowedMimeTypes() {
		return allowedMimeTypes;
	}
	public void setAllowedMimeTypes(List<MimeString> allowedMimeTypes) {
		this.allowedMimeTypes = allowedMimeTypes;
	}
	
	public boolean validate(String mediaType) {
		String tmp = mediaType.toLowerCase();
		for(MimeString type: allowedMimeTypes) {
			if(type.getMime().equals(tmp)) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		if(strRep != null) {
			return strRep;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("URL Pattern:");
		sb.append(this.url);
		sb.append("\n");
		for(MimeString type: allowedMimeTypes) {
			sb.append("\tAllowed :\t");
			sb.append(type.getMime());
			sb.append("\n");
		}
		strRep = sb.toString();
		return strRep;
	}
}
