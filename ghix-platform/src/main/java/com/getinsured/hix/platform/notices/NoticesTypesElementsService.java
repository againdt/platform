package com.getinsured.hix.platform.notices;

import java.util.List;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.NoticesTypesElements;

public interface NoticesTypesElementsService {
	
	public List<NoticesTypesElements> getAllElementsByNoticeType(NoticeType noticeType);

}
