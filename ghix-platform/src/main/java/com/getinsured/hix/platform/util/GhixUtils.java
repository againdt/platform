/**
 * Util class used across GHIX application.
 * @author venkata_tadepalli
 * @since 12/08/2012
 */
package com.getinsured.hix.platform.util;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.w3c.tidy.Tidy;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.couchbase.converters.CouchbaseAdapterFactory;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.converters.extended.ISO8601DateConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentCollectionConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentMapConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentSortedMapConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentSortedSetConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernateProxyConverter;
import com.thoughtworks.xstream.hibernate.mapper.HibernateMapper;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * Contains utility methods available for those who include platform as dependency.
 */
public class GhixUtils {
	private static final Matcher RESTRICTEDPATHURL = 
			Pattern.compile(".*(\\.\\.\\/.*|tomcat/conf/.*|server.xml|context.xml|configuration.*|\\.exe|\\.app)",
			Pattern.CASE_INSENSITIVE | Pattern.MULTILINE).matcher("");
	private static final Logger LOGGER = LoggerFactory.getLogger(GhixUtils.class);
	private static String md5SeedKey = "^45%^32976%";
	private static XStream xStreamHibernateXmlObject;
	private static XStream xStreamHibernateJsonObject;
	private static XStream xStreamGenericObject;
	private static XStream xStreamStaxObject;
	private static final String[] STATIC_RESOURCE_EXTENSIONS = {"js", "css", "png", "jpg", "gif", "jpeg", "tiff" , "pdf", "html", "woff", "woff2", "svg" };
	public  enum EMAIL_STATS {AFFILIATE_ID,STATE_CODE,EXCHANGE_URL,FLOW_ID,TENANT_ID }

	protected static final String JSON_DATE_FORMAT = "MMM dd, yyyy hh:mm:ss a";
	private static Gson gsonStatic;
	private static Map<String,IpAddressMatcher> ipMatchers = Collections.synchronizedMap(new HashMap<String,IpAddressMatcher>());
	private static final String OMIT = "omit";
	private static final String UTF_8 = "UTF-8";

	@Autowired private RestTemplate restTemplate;
		
	/**
	 * Returns the given string in Title Case format
	 * @author venkata_tadepalli
	 * @since 12/08/2012
	 * @param stringToConvert String Object
	 * @returns String 
	 */
	public static String getTitleCase(String stringToConvert) {
		String stringToConvertInLowerCase = stringToConvert.toLowerCase();
		String titleCaseString = WordUtils.capitalize(stringToConvertInLowerCase);
		
		return titleCaseString;
	}
	

	/**
	 * This method saves (serializes) any java bean object into xml file
	 * @author Pratap panda
	 * @since 12/24/2012
	 * @param xmlFileLocation
	 * @param objectToSerialize
	 * @throws Exception
	 */
    public void serializeObjectToXML(String xmlFileLocation,Object objectToSerialize) throws Exception {
        FileOutputStream os = null;
        
        try{
        	os = new FileOutputStream(xmlFileLocation);
        	XMLEncoder encoder = new XMLEncoder(os);
            encoder.writeObject(objectToSerialize);
            encoder.close();
        }finally{
        	IOUtils.closeQuietly(os);
        }
        
    }
    /**
     * Reads Java Bean Object From XML File
     * @author Praoap Panda
     * @since 12/24/2012
     * @param xmlFileLocation
     * @return
     * @throws Exception
     */
    public Object deserializeXMLToObject(String xmlFileLocation)
            throws Exception {
        FileInputStream os = null;
        try{
          os = new FileInputStream(xmlFileLocation);
    	  XMLDecoder decoder = new XMLDecoder(os);
          Object deSerializedObject = decoder.readObject();
          decoder.close();
          return deSerializedObject;
        }finally{
        	IOUtils.closeQuietly(os);
        }
      
 
       
    }
    /**
	 * This method saves (serializes) any java bean object into xml String
	 * @author Pratap panda
	 * @since 12/24/2012
	 * @param enrollmentSearch
	 * @throws GIException
	 */
    public static String serializeObjectToXMLString(Object obj) throws GIException {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	GhixXMLEncoder xmlEncoder = new GhixXMLEncoder(baos);
		xmlEncoder.writeObject(obj);
		xmlEncoder.close();
		String xmlString = baos.toString();
		
        return xmlString;
    }
    /**
   	 * This method saves (serializes) any java bean object into xml String
   	 * @author Pratap panda
   	 * @since 12/24/2012
   	 * @param enrollmentSearch
   	 * @throws GIException
   	 */
    public static Object deserializeXMLStringToObject(String xmlString) throws GIException {
    	ByteArrayInputStream bais = new ByteArrayInputStream(xmlString.getBytes());
		XMLDecoder decoder = new XMLDecoder(bais);
		Object object = decoder.readObject();
		decoder.close();

		return object;
	}

    /**
     * XStream's Hibernate package to serialize hibernate objects to xml
     * 
     * @author Pratap panda
   	 * @since 01/03/2013
     * @return
     * @throws GIException
     */
    public static XStream xStreamHibernateXmlMashaling(){
    	if(xStreamHibernateXmlObject != null){
    		return xStreamHibernateXmlObject;
    	}
    	xStreamHibernateXmlObject = new XStream() {
			  protected MapperWrapper wrapMapper(final MapperWrapper next) {
			    return new HibernateMapper(next);
			  }
		};
		xStreamHibernateXmlObject.registerConverter(new HibernateProxyConverter());
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentCollectionConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentMapConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentSortedMapConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(new HibernatePersistentSortedSetConverter(xStreamHibernateXmlObject.getMapper()));
		xStreamHibernateXmlObject.registerConverter(DATE_CONVERTER);
		return xStreamHibernateXmlObject;
    }
    
    private static void registerDateFormats(XStream xStream){
    	String dateFormat = "yyyy-MM-dd HH:mm:ss";
        String[] acceptableFormats = {
        								"yyyy-MM-dd HH:mm:ss.S",
        								"yyyy-MM-dd HH:mm:ss.SS",
        								"yyyy-MM-dd HH:mm:ss.SSS",
        								"yyyy-MM-dd HH:mm:ss.SSSS",
        								"yyyy-MM-dd HH:mm:ss.SSSSS",
        								"yyyy-MM-dd HH:mm:ss.SSSSSS",
        								"yyyy-MM-dd HH:mm:ss.S 'UTC'",
        								"yyyy-MM-dd 'T' HH:mm:ss.SSS",
        								"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        							 };
        xStream.registerConverter(new DateConverter(dateFormat, acceptableFormats));
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T initializeAndUnproxy(T entity) {
        Hibernate.initialize(entity);
        if (entity instanceof HibernateProxy) {
            entity = (T) ((HibernateProxy) entity).getHibernateLazyInitializer()
                    .getImplementation();
        }
        return entity;
    }
    
    /**
     * XStream's Hibernate package to serialize hibernate objects to JSON
     * 
     * @author Pratap panda
   	 * @since 01/03/2013
     * @return
     * @throws GIException
     */
    public static synchronized XStream xStreamHibernateJSONMashaling(){
    	if(xStreamHibernateJsonObject != null){
    		return xStreamHibernateJsonObject;
    	}
    	xStreamHibernateJsonObject = new XStream(new JettisonMappedXmlDriver()) {
			  protected MapperWrapper wrapMapper(final MapperWrapper next) {
			    return new HibernateMapper(next);
			  }
		};
		xStreamHibernateJsonObject.registerConverter(new HibernateProxyConverter());
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentCollectionConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentMapConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentSortedMapConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(new HibernatePersistentSortedSetConverter(xStreamHibernateJsonObject.getMapper()));
		xStreamHibernateJsonObject.registerConverter(DATE_CONVERTER); 
		registerDateFormats(xStreamHibernateJsonObject);
		return xStreamHibernateJsonObject;
    }
    
    public synchronized static XStream getXStreamGenericObject(){
    	if(xStreamGenericObject != null){
    		return xStreamGenericObject;
    	}
    	xStreamGenericObject = new XStream();
    	xStreamGenericObject.registerConverter(DATE_CONVERTER);
    	registerDateFormats(xStreamGenericObject);
		return xStreamGenericObject;
    }
    
    public static ISO8601DateConverter DATE_CONVERTER = new ISO8601DateConverter() { 
    	@Override 
    	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) { 
    		return Date.class.isAssignableFrom(type); 
    	} 
    }; 
    
    /*
     *Based on discussion with the team, Deepa mentioned defaultImplementation statements were added to handle
     *Enrollment XMLs received carrying data typeinformation attached with the XML. That can easily be addressed changing the data type at the 
     *Model level and keepig xStream instance out of custom changes. This is still be an open item 
     *
     */
    public synchronized static XStream getXStreamStaxObject(){
    	if(xStreamStaxObject != null){
    		return xStreamStaxObject;
    	}
    	xStreamStaxObject = new XStream(new StaxDriver());
    	//xStreamStaxObject.addDefaultImplementation(java.sql.Date.class,java.util.Date.class); 
    	//xStreamStaxObject.addDefaultImplementation(java.sql.Timestamp.class,java.util.Date.class); 
    	//xStreamStaxObject.addDefaultImplementation(java.sql.Time.class,java.util.Date.class); 
    	xStreamStaxObject.registerConverter(DATE_CONVERTER); 
    	registerDateFormats(xStreamStaxObject);
    	
		return xStreamStaxObject;
    }
    
    
    public String restGET(String url, Map<String, Object> pathVars)throws GIException {
    	LOGGER.info("URL : "+url);
    	LOGGER.info("pathVars : "+pathVars);
    	String getResp =null;
		try{
			if(pathVars==null){getResp = restTemplate.getForObject(url, String.class);}
			else{getResp = restTemplate.getForObject(url, String.class, pathVars);}
		}catch(RestClientException re){
			LOGGER.info("Message : "+re.getMessage());
			LOGGER.info("Cause : "+re.getCause());
		}
		return getResp;
    }
    
    public String restPOST(String url, Map<String, Object> pathVars)throws RestClientException,GIException {
    
    	return restTemplate.postForObject(url, pathVars, String.class,pathVars);
    }


	public static String getCountyName(String countyCode)
	{
		String countyName = null;
		// ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		// ZipCodeService zipCodeService =  (ZipCodeService) appContext.getBean("zipCodeService");
		ZipCodeService zipCodeService = (ZipCodeService) GHIXApplicationContext.getBean("zipCodeService");
		countyName = zipCodeService.findCountyNameByCountyCode(countyCode);
		return countyName != null ? countyName : "";
	}
	
	 /**@param e
     * @return boolean
	 * the below is takes input as object type (e) like (Integer, String, Character, List<String>, List<Integer>...etc)
	 * and return true when the input object is not null/empty/"null" else return false"
  	 **/
	public static <T> boolean isNotNullAndEmpty(T e){
 	   boolean isNotNUll=false;
 	   if((e!=null) && !(e.toString().isEmpty()) && !(e.toString().equalsIgnoreCase("null"))){
			isNotNUll=true;
 	   }
	  return isNotNUll;
 	}
	
	/**
	 * @author ajmeher
	 * @since 29th Apr 2014
	 * @param throwable
	 * @return
	 */
	public static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
	
	public static String getMD5CheckSum(ArrayList<String> params) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("MD5");
		for(int i = 0; i < params.size(); i++){
			md.update(params.get(i).getBytes());
		}
		return Base64.getEncoder().encodeToString(md.digest());
	}
	
	public static String getGhixSecureCheckSum(ArrayList<String> params) throws NoSuchAlgorithmException{
		byte[] seedBytes = md5SeedKey.getBytes();
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(seedBytes);
		for(int i = 0; i < params.size(); i++){
			md.update(params.get(i).getBytes());
		}
		return Base64.getEncoder().encodeToString(md.digest());
	}
	
	public static boolean validateGhixSecureCheckSum(ArrayList<String> params, String checksum) throws NoSuchAlgorithmException{
		String calculatedCheckSum = null;
		byte[] seedBytes = md5SeedKey.getBytes();
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(seedBytes);
		for(int i = 0; i < params.size(); i++){
			md.update(params.get(i).getBytes());
		}
		calculatedCheckSum = Base64.getEncoder().encodeToString(md.digest());
		return calculatedCheckSum.equals(checksum);
	}
	
	/**
	 * @author Biswakesh This method is used create a factory instance of
	 *         DocumentBuilderFactory and prevent external entity expansion and
	 *         injection attacks
	 * @return Object containing DocumentBuilderFactory instance
	 * @throws ParserConfigurationException
	 */
	public static DocumentBuilderFactory getDocumentBuilderFactoryInstance()
			throws ParserConfigurationException {
		DocumentBuilderFactory factory = null;

		factory = DocumentBuilderFactory.newInstance();
		factory.setExpandEntityReferences(false);

		return factory; 
	}
	
	private static boolean isBlackListedPathURL(String pathUrl){
		return RESTRICTEDPATHURL.reset(pathUrl).matches();
	}
	
	public static boolean isGhixValidPath(String pathUrl){
		return isBlackListedPathURL(pathUrl)?false:true;
	}
		/**
	 * @author Vishaka This method is to prevent external entity expansion and
	 *         injection attacks 
		 * @throws Exception 
	*/
	public static Object inputStreamToObject(InputStream is,  Class<?> c) throws JAXBException, XMLStreamException {
			JAXBContext jc = JAXBContext.newInstance(c);

	        XMLInputFactory xif = XMLInputFactory.newInstance();
	        xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
	        xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
	        XMLStreamReader xsr = xif.createXMLStreamReader(is);

	        Unmarshaller unmarshaller = jc.createUnmarshaller();
	        return unmarshaller.unmarshal(xsr);
	}
	
	@SuppressWarnings("unchecked")
	public static String exceptionToJson(Throwable e){
		if(e == null){
			return null;
		}
		HubServiceException he = null;
		JSONObject obj  = new JSONObject();
		JSONObject error = new JSONObject();
		if(e instanceof HubServiceException){
			he = (HubServiceException)e;
			error.put("ERROR_CODE", he.getErrorCode());
			if(he.getErrorCode().equalsIgnoreCase(RequestStatus.UNDER_MAINT.getStatusCode())){
				error.put("SERVICE_UNDER_MAINT", he.getServiceDownTimeSchedule());
			}
		}
		error.put("EXCEPTION_TYPE", e.getClass().getName());
		
		String message = e.getMessage();
		if(message != null)
		{
			message = message.replaceAll("\"","");
		}
		error.put("EXCEPTION_MESSAGE", message);
		Exception tmpExp = (Exception) e.getCause();
		if(tmpExp != null){
			error.put("EXCEPTION_CAUSE", exceptionToJson(e.getCause()));
		}
		obj.put("EXCEPTION", error);
		
		String response = obj.toJSONString();
		response = response.replaceAll("\\\\", "");
		response = response.replaceAll("(\"\\{)", "\\{");
		response = response.replaceAll("(\\}\")", "\\}");
		return response;
	}
    
    public static String getSHA256CheckSum(ArrayList<String> params) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] seedBytes = md5SeedKey.getBytes();
		md.update(seedBytes);
		//byte[] hash = digest.digest(text.getBytes("UTF-8"));
		//MessageDigest md = MessageDigest.getInstance("MD5");
		for(int i = 0; i < params.size(); i++){
			md.update(params.get(i).getBytes("UTF-8"));
		}
		return new String(org.apache.commons.codec.binary.Base64.encodeBase64URLSafe(md.digest()));
	}
	
	public static boolean validateSHA256CheckSum(ArrayList<String> params, String checksum) throws NoSuchAlgorithmException{
		String calculatedCheckSum = null;
		byte[] seedBytes = md5SeedKey.getBytes();
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(seedBytes);
		for(int i = 0; i < params.size(); i++){
			md.update(params.get(i).getBytes());
		}
		calculatedCheckSum = new String(org.apache.commons.codec.binary.Base64.encodeBase64URLSafe(md.digest()));
		return calculatedCheckSum.equals(checksum);
	}

	public static boolean isStaticResourceRequest(final HttpServletRequest httpRequest) {
		String requestUrl = httpRequest.getRequestURL().toString();
		if(StringUtils.isNotBlank(requestUrl)) {
			for (String extension : STATIC_RESOURCE_EXTENSIONS) {
				if(requestUrl.endsWith(extension)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static String parseForHtmlContent(String content)
	{
		String linkStr = "[link]";
		String urlStr = "[url]";
		String urlTextStr = "[urlText]";
		
		if(content !=null && content.indexOf(linkStr) >=0 && content.indexOf(urlStr) >=0 && content.indexOf(urlTextStr) >= 0){
			
			int linkStartIndex = content.indexOf(linkStr);
			int linkEndIndex = content.lastIndexOf(linkStr);
			String content1 = content.substring(0,linkStartIndex);
			String content2 = content.substring(linkEndIndex+linkStr.length(),content.length());
		
			int urlStartIndex = content.indexOf(urlStr) + urlStr.length();
    		int urlEndIndex = content.lastIndexOf(urlStr);

    		String url = content.substring(urlStartIndex,urlEndIndex);

    		int urlcontentStartIndex = content.indexOf(urlTextStr) + urlTextStr.length();
    		int urlcontentEndIndex = content.lastIndexOf(urlTextStr);

    		String urlcontent = content.substring(urlcontentStartIndex,urlcontentEndIndex);

    		String output = content1+ "<a href="+url+" target='_blank' >"+urlcontent+"</a>"+content2;
    		return output;
		}else{
			return content;
		}

		
	}

	public static boolean checkIpInrange(String ipToCheck, String allowedRange){
		IpAddressMatcher matcher = ipMatchers.get(allowedRange);
		if(matcher == null){
			matcher = new IpAddressMatcher(allowedRange);
			ipMatchers.put(allowedRange, matcher);
		}
		return matcher.matches(ipToCheck);
	}
	
	public static boolean checkIpInrange(HttpServletRequest request, String allowedRange){
		IpAddressMatcher matcher = ipMatchers.get(allowedRange);
		if(matcher == null){
			matcher = new IpAddressMatcher(allowedRange);
			ipMatchers.put(allowedRange, matcher);
		}
		return matcher.matches(request);
	}

	/**
	 * Returns configured static instance of {@link Gson} de/serializer.
	 *
	 * @return configured {@link Gson} instance.
	 */
	public static Gson platformGson()
	{
		if(gsonStatic == null)
		{
			GsonBuilder builder = new GsonBuilder();
			builder.serializeNulls();
			builder.disableHtmlEscaping();
			builder.setDateFormat(JSON_DATE_FORMAT);
			builder.registerTypeAdapterFactory(new CouchbaseAdapterFactory());
			gsonStatic = builder.create();
		}
		return gsonStatic;
	}

	/**
	 * Parses given (X)HTML data and returns the result.
	 *
	 * <p>
	 *   This method produces no warnings or errors.
	 * </p>
	 * <p>
	 *   <b>Remember</b> that after parsing and cleaning given (x)html data, this
	 *   method will not provide any feedback as to what was removed, deemed invalid,
	 *   or any other parsing errors/warnings will not be reported. Perhaps it would
	 *   be safer to use {@link #parseHtmlData(String, PrintWriter)} so that result
	 *   of the parsing can be written out to the given {@code PrintWriter}.
	 * </p>
	 * @param data (X)HTML data.
	 * @return parsed data, with removed/cleaned up invalid tags.
	 * @throws IOException if {@link IOException} occurs.
	 * @see #parseHtmlData(String, PrintWriter)
	 */
	public static String parseHtmlData(String data) throws IOException
	{
		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setShowErrors(0);
		tidy.setQuiet(true);
		tidy.setErrout(null);
		tidy.setTidyMark(false);
		tidy.setDocType(OMIT);
		tidy.setXHTML(true);
		tidy.setInputEncoding(UTF_8);
		tidy.setOutputEncoding(UTF_8);
		tidy.parse(new StringReader(data), writer);
		writer.close();
		return writer.toString();
	}

	/**
	 * Parses given (X)HTML data and returns the result.
	 *
	 * <p>
	 *   This method will write warnings, errors and summary into given
	 *   {@link PrintWriter}
	 * </p>
	 * <p>
	 * If you feel absolutely safe that you don't need any feedback about JTidy's parsing results,
	 * use {@link #parseForHtmlContent(String)} method.
	 * </p>
	 *
	 * @param data (X)HTML data.
	 * @param out {@code PrintWriter} to write errors/warnings/summary to.
	 * @return parsed data, with removed/cleaned up invalid tags.
	 * @throws IOException if {@link IOException} occurs.
	 * @see #parseHtmlData(String)
	 */
	public static String parseHtmlData(String data, PrintWriter out) throws IOException
	{
		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setErrout(out);
		tidy.setTidyMark(false);
		tidy.setDocType(OMIT);
		tidy.setXHTML(true);
		tidy.setInputEncoding(UTF_8);
		tidy.setOutputEncoding(UTF_8);
		tidy.parse(new StringReader(data), writer);
		writer.close();
		return writer.toString();
	}
}