package com.getinsured.hix.platform.ecm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.platform.util.GhixPlatformConstants;
import org.apache.commons.lang.StringUtils;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.UNSUPPORTED_DOCUMENT_TYPE;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.DATA_BYTES_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.FILE_NAME_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISConstants.ZERO;

@XmlRootElement(name = "MimeConfig")
public class MimeConfig {
	private static MimeConfig config = null;
	private static Tika tika = new Tika();
	private String profile = null;
	private HashMap<String, URLMimeConfig> urlConfigs;
	private HashMap<String, String> urlSupportedMimeExtensionList = new HashMap<>();
	private List<URLMimeConfig> urlConfigList = null;
	private static Logger logger = LoggerFactory.getLogger(MimeConfig.class);
	private static long lastModifiedTime = -1l;
	private static TikaConfig t_config = TikaConfig.getDefaultConfig();
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	private String getExtensions(String url, URLMimeConfig m_config) {
		String list = this.urlSupportedMimeExtensionList.get(url);
		if(list != null) {
			return list;
		}
		Set<String> set = new HashSet<>();
		List<MimeString> allowedMimes = m_config.getAllowedMimeTypes();
		for(MimeString str: allowedMimes) {
			try {
				MimeType mime = t_config.getMimeRepository().forName(str.getMime());
				List<String> allEx = mime.getExtensions();
				for(String tmp: allEx) {
					int idx = tmp.indexOf('.');
					if(idx > -1) {
						tmp = tmp.substring(idx+1);
					}
					set.add(tmp);
				}
			} catch (MimeTypeException e) {
				logger.error("No mime type plugin available {}",str.getMime(),e);
			}
		}
		StringBuilder sb = new StringBuilder();
		for(String s:set) {
			sb.append(s+",");
		}
		sb.trimToSize();
		list = sb.deleteCharAt(sb.length()-1).toString();
		this.urlSupportedMimeExtensionList.put(url, list);
		return list;
	}
	
	public List<String> getAllSupportedExtensions(String mimeType){
		MimeType mime = null;
		try {
			mime = t_config.getMimeRepository().forName(mimeType);
		} catch (MimeTypeException e) {
			logger.error("Unkown mime type {}",mimeType,e);
			return new ArrayList<>();
		}
		return mime.getExtensions();
	}
	
	private static boolean isStale(File f) {
		long currentModifiedStamp = f.lastModified();
		boolean modified =  currentModifiedStamp != lastModifiedTime;
		if(modified && logger.isInfoEnabled()) {
			logger.info("Mime config modified");
		}
		return modified;
	}
	
	public static MimeConfig getMimeConfig() {
	 InputStream resource = null;
	 MimeConfig tempConfig = null;
	 File mappingFile = new File(GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION,"MimeConfig.xml");
 	 if(!mappingFile.exists()){
 		logger.error ("Mime config file not available [Path:]"+mappingFile.getAbsolutePath());
 		return null;
     }
	 if(config == null || isStale(mappingFile)){
	        try {
	        	if(logger.isInfoEnabled()) {
	    			logger.info("Loading/Re-loading mime configuration from {}",mappingFile.getAbsolutePath());
	    		}
	            JAXBContext jc = JAXBContext.newInstance(MimeConfig.class,URLMimeConfig.class,MimeString.class);
	            Unmarshaller u = jc.createUnmarshaller();
	            resource = new FileInputStream(mappingFile);
	            tempConfig= (MimeConfig) u.unmarshal( resource );
	            lastModifiedTime = mappingFile.lastModified();
	        } catch(Exception e) {
	            logger.error("Error initializing the partner maping information, will continue using the existing configuration if available",e);
	        }finally{
	        	if(resource != null){
	        		try {
						resource.close();
					} catch (IOException e) {
						//Ignored
					}
	        	}
	        }
	 }
	 if(tempConfig != null) {
		 config = tempConfig;
	 }
	 return config;
	}
	
	@XmlElement(name="url_mime_config")
	public List<URLMimeConfig> getUrlConfigList() {
		return urlConfigList;
	}
	public void setUrlConfigList(List<URLMimeConfig> urlConfigList) {
		this.urlConfigList = urlConfigList;
	}
	
	public String detectMimeType(InputStream is) throws IOException{
		String mime = tika.detect(is);
		if(logger.isDebugEnabled()) {
			logger.info("Detected mime type :"+mime);
		}
		return mime;
	}
	
	public String validateIncomingContent(byte[] dataBytes, String fileName) {
		if (StringUtils.isEmpty(fileName)) {
			throw new IllegalArgumentException(FILE_NAME_CANNOT_BE_NULL);
		}

		if (dataBytes == null || dataBytes.length <= ZERO) {
			throw new IllegalArgumentException(DATA_BYTES_CANNOT_BE_NULL);
		}
		
		String mimeType = tika.detect(dataBytes);
		if (StringUtils.isEmpty(mimeType)) {
			throw new IllegalArgumentException(UNSUPPORTED_DOCUMENT_TYPE);
		}
		
		return mimeType;
	}
	
	private String buildRegExUrl(String url) {
		StringBuilder sb = new StringBuilder();
		String[] pathElements = url.split("/");
		int len = pathElements.length;
		if(len > 0) {
			pathElements[len -1] = "*";
		}
		for(int i = 0; i < len; i++) {
			sb.append(pathElements[i]);
			sb.append("/");
		}
		sb.trimToSize();
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public String validateMimeTypeFromSession(byte[] dataBytes, String fileName) throws MediaTypeNotSupported{
		if(urlConfigs == null) {
			this.urlConfigs = new HashMap<>();
		}
		String url = null;
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra == null) {
			throw new RuntimeException("No session context available");
		}
		url = sra.getRequest().getRequestURI();
		if(url == null) {
			logger.info("No URL found from the session, assuming internal flow, using legacy function for file {}",fileName);
			return CMISSessionUtil.getMime(fileName);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Evaluating URL:"+ url+" for content type");
		}
		String mimeType = null;
		URLMimeConfig temp = this.urlConfigs.get(url);
		if(temp == null) {
			Iterator<URLMimeConfig> cursor = this.urlConfigList.iterator();
			while(cursor.hasNext()) {
				temp = cursor.next();
				this.urlConfigs.put(temp.getUrl(), temp);
			}
		}
		temp = this.urlConfigs.get(url);
		if(temp == null) {
			// Check if this is a regex
			String strTemp = null;
			strTemp = this.buildRegExUrl(url);
			if (logger.isDebugEnabled()) {
				logger.debug("URL: {} not registered, checking for regex {}", url, strTemp);
			}
			url = strTemp;
		}
		temp = this.urlConfigs.get(url);
		if(temp == null) {
			logger.error("No mime type registered for URL {} using legacy function to use the file extension for file {}, FixIt", url, fileName);
			return CMISSessionUtil.getMime(fileName);
		}
		mimeType = tika.detect(dataBytes);
		if(!temp.validate(mimeType)) {
			throw new MediaTypeNotSupported("Unknown file type, please upload one of these types:"+this.getExtensions(url, temp));
		}
		if(logger.isDebugEnabled()) {
			logger.debug("URL {} supports mime type {} supported extensions {}",url,mimeType, this.getExtensions(url, temp));
		}
		return mimeType;
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		MimeConfig conf = MimeConfig.getMimeConfig();
		System.out.println(conf.urlConfigList.get(0).toString());
		String mime = conf.urlConfigList.get(0).getAllowedMimeTypes().get(0).getMime();
		URLMimeConfig t = conf.urlConfigList.get(0);
		System.out.println(conf.getExtensions("x", t));
		File mappingFile = new File(GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION,"GI.txt");
		String x = conf.detectMimeType(new FileInputStream(mappingFile));
		System.out.println(x);
		
		System.out.println(conf.buildRegExUrl("/hix/user/account/login"));
	}
}
