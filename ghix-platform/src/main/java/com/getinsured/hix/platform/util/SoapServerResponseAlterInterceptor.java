package com.getinsured.hix.platform.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.interceptor.EndpointInterceptorAdapter;
import org.springframework.ws.soap.SoapMessage;

/**
 * SoapServerResponeseAlterInterceptor to handle request/response
 * for incoming SOAP Web Services. 
 * 
 * @author nayak_b
 */
public class SoapServerResponseAlterInterceptor extends EndpointInterceptorAdapter {
	private static final Logger LOGGER = LoggerFactory.getLogger(SoapServerResponseAlterInterceptor.class); 
	
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
    	LOGGER.info("Soap Server response handler.");
    	SoapMessage soapMessage = (SoapMessage) messageContext.getResponse();
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	SoapHelper.setResponseSoapHeader(soapMessage,request);
    	request.setAttribute("responseMessageContext", soapMessage); // HIX-45155 Enhance Payload Handler factory
		return true;
    }
    
    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint)
    		throws Exception {
    	LOGGER.info("Soap Server Request handler.");
    	SoapMessage soapMessage = (SoapMessage) messageContext.getRequest();
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		SoapHelper.extractHeader(soapMessage, request);
		request.setAttribute("requestMessageContext", soapMessage); // HIX-45155 Enhance Payload Handler factory
    	return true;
    }
    

}

