package com.getinsured.hix.platform.couchbase.dto;

import com.google.gson.annotations.Expose;

public class CouchBinary {
	@Expose
	private String contentLink;
	@Expose
	private boolean hasContent;
	@Expose
	private String docType;
	@Expose
	private String fileName;
	@Expose
	private int fileSize;
	@Expose
	private String mimeType;
	@Expose
	private int numberOfParts = 1; // TODO set the value to 0 after executing migrated data script

	public int getNumberOfParts() {
		return numberOfParts;
	}

	public void setNumberOfParts(int numberOfParts) {
		this.numberOfParts = numberOfParts;
	}

	public String getContentLink() {
		return contentLink;
	}

	public void setContentLink(String contentLink) {
		this.contentLink = contentLink;
	}

	public boolean isHasContent() {
		return hasContent;
	}

	public void setHasContent(boolean hasContent) {
		this.hasContent = hasContent;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

}
