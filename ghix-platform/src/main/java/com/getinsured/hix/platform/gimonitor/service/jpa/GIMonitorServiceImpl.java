package com.getinsured.hix.platform.gimonitor.service.jpa;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.dto.exceptions.GIMonitorDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIErrorCode;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.couchbase.dto.gimonitor.GIMonitorDocument;
import com.getinsured.hix.platform.couchbase.service.CouchBucketService;
import com.getinsured.hix.platform.gimonitor.repository.GIMonitorRepository;
import com.getinsured.hix.platform.gimonitor.service.GIErrorCodeService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.gimonitor.util.GIMonitorUtility;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Service("giMonitorService")
public class GIMonitorServiceImpl implements GIMonitorService {

	private static final Logger logger = LoggerFactory.getLogger(GIMonitorServiceImpl.class);
	private enum STORE{ORACLE, COUCH};

	@Autowired
	private GIMonitorRepository giMonitorRepository;
	@Autowired 
	private GIErrorCodeService giErrorCodeService;

	@Autowired
	private CouchBucketService couchBucketService;
	private SecureRandom random = new SecureRandom();
	
	private String getFQComponentName(String component) {
		StringBuilder sb = new StringBuilder();
		if (component != null) {
			sb.append(component);
			sb.append("@");
		}
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest servletRequest;
		if (sra != null) {
			servletRequest = sra.getRequest();
			if (servletRequest != null) {
				sb.append(":" + servletRequest.getContextPath());
			}
		}
		try {
			sb.append("::" + Inet4Address.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			// ignored
		}
		sb.trimToSize();
		return sb.toString();
	}

	@Override
	@Transactional
	public GIMonitor saveOrUpdateGIMonitor(GIMonitor giMonitor) {

		logger.error("An exception happened, logging to GImonitor "+giMonitor.getExceptionStackTrace());
		if (logger.isDebugEnabled()){
			logger.debug("Saving/Updating GIMonitor object - "+ giMonitor);
		}
		AccountUser user = giMonitor.getUser();
		this.validateUserContext(user, giMonitor);
		if(giMonitor.getUser() == null){
			logger.warn("Updating monitor without any user context, no valid user context available");
		}
		if(giMonitor.getErrorCode()== null){
			GIErrorCode errorCodeObject = giErrorCodeService.getGIErrorCodeByErrorCode(GIRuntimeException.ERROR_CODE_UNKNOWN);
			giMonitor.setErrorCode(errorCodeObject);
			giMonitor.setComponent(this.getFQComponentName(errorCodeObject.getModuleName()));
		}
		try{
			giMonitor = giMonitorRepository.save(giMonitor);
		}catch(Exception e){
			// Generate an ID for the reference as DB update failed
			logger.error("Failed saving GIMonitor",e);
			int monitorId = random.nextInt();
			logger.error("Exception encountered, dumping into the log file: Monitor Id ["+monitorId+"]:"+giMonitor.getExceptionStackTrace());
			giMonitor.setId(monitorId);
		}
		return giMonitor;
	}
	
	@Override
	@Transactional
	public GIMonitor saveOrUpdateErrorLog(String errorCode, Date eventTime,
			String exceptionClassName, String exceptionStackTrace,
			AccountUser user) {
		return saveOrUpdateErrorLog(errorCode, eventTime, exceptionClassName, exceptionStackTrace, user, null, null, null);
	}
	
	@Override
	@Transactional
	public GIMonitor saveOrUpdateErrorLog(String errorCode, Date eventTime,
			String exceptionClassName, String exceptionStackTrace,
			AccountUser user, String url, String component, Integer giMonitorId) {

		logger.error("An exception happened, logging to GImonitor "+exceptionStackTrace);
		if (logger.isDebugEnabled()){
			logger.debug("Saving/Updating GIMonitor.");
		}
		
		GIErrorCode errorCodeObject = null;
		GIMonitor giMonitor = null;
		
		/**
		 * GI Monitor object is already created hence not re creating anything. 
		 */
		
		if(giMonitorId!=null){
			giMonitor = new GIMonitor();
			giMonitor.setId(giMonitorId);
		} else {
			if(GhixUtils.isNotNullAndEmpty(errorCode)){
				errorCodeObject = giErrorCodeService.getGIErrorCodeByErrorCode(errorCode);
				if (errorCodeObject == null){
					errorCodeObject = giErrorCodeService.getGIErrorCodeByErrorCode(GIRuntimeException.ERROR_CODE_UNKNOWN);
				}
				if(StringUtils.isBlank(component)){
					component = errorCodeObject.getModuleName();
				}
				
			}
			
			giMonitor = new GIMonitor();
			giMonitor.setComponent(this.getFQComponentName(component));
			giMonitor.setErrorCode(errorCodeObject);
			giMonitor.setEventTime(eventTime);
			giMonitor.setUrl(url);
			giMonitor.setException(exceptionClassName);
			giMonitor.setUser(user);
			giMonitor.setExceptionStackTrace(exceptionStackTrace);
			this.validateUserContext(user, giMonitor);
			if(giMonitor.getUser() == null){
				logger.warn("Updating monitor without any user context, no valid user context available");
			}
			try{
				giMonitor = giMonitorRepository.save(giMonitor);
			}catch(Exception e){
				// Generate an ID for the reference as DB update failed
				logger.error("Failed saving GIMonitor",e);
				int monitorId = random.nextInt();
				logger.error("Exception encountered, dumping into the log file: Monitor Id ["+monitorId+"]:"+giMonitor.getExceptionStackTrace());
				giMonitor.setId(monitorId);
			}
		}
		return giMonitor;
	}

	@Override
	@Transactional
	public GIMonitorDTO persistGIMonitor(GIMonitorDTO giMonitorDTO) {

		GIMonitorDTO persistedGIMonitor = null;
		
		if (giMonitorDTO != null) {
			if (STORE.COUCH.toString().equalsIgnoreCase(GhixPlatformConstants.GIMONITOR_STORE)) {
				persistedGIMonitor = saveGIMonitorToCouchBase(giMonitorDTO);
			} else {
				persistedGIMonitor = saveGIMonitorToDB(giMonitorDTO);
			}
		}

		return persistedGIMonitor;
	}

	private GIMonitorDTO saveGIMonitorToDB(GIMonitorDTO giMonitorDTO) {

		GIMonitor giMonitor = GIMonitorUtility.convertToGIMonitorDomainObject(giMonitorDTO);
		giMonitor = this.saveOrUpdateGIMonitor(giMonitor);
		giMonitorDTO.setId(giMonitor.getId().toString());
		return giMonitorDTO;
	}

	private GIMonitorDTO saveGIMonitorToCouchBase(GIMonitorDTO giMonitorDTO) {

		GIMonitorDocument document = GIMonitorUtility.convertToGIMonitorDocument(giMonitorDTO);
		String id = couchBucketService.createDocument(document);
		giMonitorDTO.setId(id);

		return giMonitorDTO;
	}
	
	private void validateUserContext(AccountUser user, GIMonitor giMonitor){
		if(user != null && user.getId() == 0){
			giMonitor.setUser(null);
			String exception = giMonitor.getException();
			exception += "Bad user context, No user id available, probably external id mismatch";
			giMonitor.setException(exception);
		
			if(logger.isDebugEnabled()){
				logger.debug("User context validated for user :"+user.getEmail()+" Id:"+user.getId());
			}
		}
	}
}
