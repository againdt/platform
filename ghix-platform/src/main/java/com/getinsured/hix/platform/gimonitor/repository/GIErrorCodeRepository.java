package com.getinsured.hix.platform.gimonitor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.GIErrorCode;

@Repository("giErrorCodeRepository")
public interface GIErrorCodeRepository extends JpaRepository<GIErrorCode, Integer>{

	List<GIErrorCode> findByErrorCode(String errorCode);

}
