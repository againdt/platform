package com.getinsured.hix.platform.util;

import java.io.Serializable;
import java.security.Principal;

import org.apache.commons.codec.digest.HmacUtils;
import org.apache.http.annotation.Immutable;
import org.apache.http.auth.BasicUserPrincipal;
import org.apache.http.auth.Credentials;
import org.apache.http.util.LangUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Immutable
public class GICredentials implements Credentials, Serializable {
	private static final long serialVersionUID = 1L;
	private final BasicUserPrincipal principal;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private String authMode = null;
	private String password = null;
	private String apiSecretKey = null;
	

	public GICredentials(final String userName, final String password, final String authMode, final String apiSecretKey) {
	        if(authMode == null){
	        	throw new RuntimeException("Auth mode expected supported types \"BASIC\", \"HMAC\", \"HMAC_NOANCE\", Please check sslConf.xml");
	        }
	        this.authMode = authMode;
	        switch(authMode){
	        	case APIKeyMode.HMAC:
	        		// Allow fall through
	        	case APIKeyMode.HMAC_NOANCE:{
		        	if(apiSecretKey == null) {
		        		throw new RuntimeException("API Secret key is mandatory for HMAC and HMAC_NOANCE type of authentication, please check sslConf.xml");
		        	}
		        	//Allow fall through
	        	}
	        	case APIKeyMode.BASIC:{
	        		if(password == null) {
	        			throw new RuntimeException("Password is mandatory for HMAC and HMAC_NOANCE and BASIC types of authentication, please check sslConf.xml");
	        		}
	        		if(userName == null) {
	        			throw new RuntimeException("Username is mandatory for HMAC and HMAC_NOANCE and BASIC types of authentication, please check sslConf.xml");
	        		}
	        		break;
	        	}
	        	default:{
	        		throw new RuntimeException("API Key mode ["+authMode+"] not available");
	        	}
	        }
	        this.password = password;
	        this.apiSecretKey = apiSecretKey;
	        this.principal = new BasicUserPrincipal(userName);
	}

	@Override
	public Principal getUserPrincipal() {
		return this.principal;
	}

	public String getUserName() {
		return this.principal.getName();
	}

	@Override
	public String getPassword() {
        switch(authMode){
	        case APIKeyMode.HMAC:{
	        	logger.info("Using HMAC for basic authentication");
	        	return HmacUtils.hmacSha256Hex(apiSecretKey.getBytes(), password.getBytes());
	        }
	        case APIKeyMode.HMAC_NOANCE:{
	        	String time = Long.toString(System.currentTimeMillis());
	        	String sKey = this.apiSecretKey+time;
        		String pass = HmacUtils.hmacSha256Hex(sKey, password)+"@"+time;
        		return pass;
	        }
	        case APIKeyMode.BASIC:{
	        	return this.password;
	        }
//	        case APIKeyMode.JWT:{
//	        	Not supported yet
//	        	
//	        }
	        default:{
	        	throw new RuntimeException("API Key mode ["+authMode+"] not available");
	        }
        }
	}

	@Override
	public int hashCode() {
		return this.principal.hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof GICredentials) {
			final GICredentials that = (GICredentials) o;
			if (LangUtils.equals(this.principal, that.principal)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return this.principal.toString();
	}

}
