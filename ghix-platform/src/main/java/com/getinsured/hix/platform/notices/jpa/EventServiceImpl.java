package com.getinsured.hix.platform.notices.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Event;
import com.getinsured.hix.platform.notices.EventService;
import com.getinsured.hix.platform.repository.IEventRepository;

@Service("EventService")
public class EventServiceImpl implements EventService {

	@Autowired private IEventRepository eventRepository;
	
	@Override
	public Event findByEventName(String eventName) {
		return eventRepository.findByEventName(eventName);
	}
}
