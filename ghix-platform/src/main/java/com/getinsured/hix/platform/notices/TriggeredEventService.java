package com.getinsured.hix.platform.notices;


public interface TriggeredEventService {
	
	String createTriggeredEvent(String eventName, String jsonEventData);
}
