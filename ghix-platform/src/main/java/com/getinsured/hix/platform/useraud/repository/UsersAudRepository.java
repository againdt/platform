package com.getinsured.hix.platform.useraud.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.UsersAud;

@Repository("usersAudRepository")
public interface UsersAudRepository extends JpaRepository<UsersAud, Integer>{

	@Query(value = "SELECT password FROM UsersAud where id = :userId and password like '$2a%' GROUP BY password ORDER BY MAX(rev) DESC, password") 
	List<String> getPasswordsFromHistory(Pageable pageable, @Param("userId") int userId);

}
