package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

public interface IRoleRepository extends TenantAwareRepository<Role, Integer> {
	 Role findIdByName(String name);
	 
	 Role findById(int id);
	 
	@Query("FROM Role role WHERE role.name IN ?1")
	List<Role> findRolesByName(List<String>  roleNames);
		
	@Query("FROM Role role WHERE role.name NOT LIKE ('%ADMIN')  AND role.name !='OPERATIONS' ")
	List<Role> findNonAdminUsers();
	
	@Query("FROM Role role WHERE role.name IN ('ADMIN','BROKER_ADMIN','ISSUER_ADMIN','CSR','OPERATIONS', 'SALES_MANAGER','RECONCILIATION_ADMIN')")
	List<Role> findAdminRolesForUsers();
	
	@Query("FROM Role role WHERE role.privileged =1")
	List<Role> findPrivilegedRoles();
	
	@Query("FROM Role role WHERE role.id in(select ur.role.id from UserRole ur where ur.user.id=:userId and ur.roleFlag = 'Y')")
	Role findRoleByUserId(@Param("userId") int userId);

	

}

