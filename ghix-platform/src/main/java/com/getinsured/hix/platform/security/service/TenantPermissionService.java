package com.getinsured.hix.platform.security.service;

import java.util.List;

import com.getinsured.hix.model.TenantPermission;
import com.getinsured.hix.model.TenantPermissionRoleMappingDTO;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface TenantPermissionService {
	
	public List<TenantPermission> findByTenantIdAndPermissionName(long tenantId, String permission);
	
	public void addTenantPermissions(String tenantName);
	
	public String getTenantPermissionMapping(String tenantName);
	
	public String updateTenantPermissionMapping(List<TenantPermissionRoleMappingDTO> tenantPermissionRoleMappingDTOs);

}
