package com.getinsured.hix.platform.config;

public class SHOPConfiguration {

	public enum SHOPConfigurationEnum implements PropertiesEnumMarker
	{
		EMPLOYER_DISPLAY_TOBACCOUSE ("shop.employer.DisplayTobaccoUse"),
		EMPLOYER_DISPLAY_NATIVE_AMR ("shop.employer.DisplayNativeAmerican"),
		EMPLOYER_MIN_CONTRIB_EMP ("shop.employer.MinContributionForEmployee"),
		EMPLOYER_MIN_CONTRIB_DEP ("shop.employer.MinContributionForDependent"),
		EMPLOYER_DEFAULT_CONTRIB_EMP ("shop.employer.DefaultContributionForEmployee"),
		EMPLOYER_DEFAULT_CONTRIB_DEP ("shop.employer.DefaultContributionForDependent"),
		EMPLOYER_MAX_EMP_SHOP ("shop.employer.MaxEmployeeForShop"),
		EMPLOYER_CUST_EFF_DATE ("shop.employer.CustomEffectiveDate"),
		EMPLOYER_PARTICIPATIN_REQ ("shop.employer.ParticipationRequired"),
		EMPLOYER_EHB_COVERED ("shop.employer.EhbCovered"),
		EMPLOYER_QUOTING_ZIP ("shop.employer.QuotingZip"),
		EMPLOYER_INIT_OPEN_ENROLLMENT_PERIOD ("shop.employer.InitialOpenEnrollmentPeriod"),
		EMPLOYER_APPROACHING_PERIOD ("shop.employer.ApproachingPeriod"),
		EMPLOYEE_MAX_TERMINATION_DAYS  ("shop.employee.MaxTerminationDays"),
		EMPLOYER_ENROLLMENT_END_NOTIFYDAY ("shop.employee.EnrollmentEndNotifyDay"),
		EMPLOYER_ENROLLMENT_END_NOTIFYDAY_FOR_PARTICIPATION ("shop.employer.EnrollmentEndNotifyDayForParticipation"),
		EMPLOYER_NEW_HIRE_ELIGIBILITY_DAYS_BEFORE ("shop.employer.NewHireEligibilityDaysBefore"),
		EMPLOYER_NEW_HIRE_ELIGIBILITY_DAYS_AFTER ("shop.employer.NewHireEligbilityDaysAfter"),
		EMPLOYER_DISPLAY_DENTAL_PLANS ("shop.employer.DisplayDentalPlans"),
		EMPLOYEE_FRTDOCUMENT_REQUIRED  ("shop.employee.FRTDocumentRequired"),
		EMPLOYEE_WARDDOCUMENT_REQUIRED  ("shop.employee.WardDocumentRequired"),
		EMPLOYER_DAYS_BEFORE_RENEWAL ("shop.employer.DaysBeforeRenewal"),
		EMPLOYER_COVERAGE_END_GROUP_TERMINATION ("shop.employer.CoverageYearEndGroupTermination"),
		EMPLOYER_ENROLLMENT_CUTOFF_DAY ("shop.employer.EnrollmentCutOffDay"),
		EMPLOYER_FTECOUNT_TAXCREDIT ("shop.employer.FTECountForTaxCredit"),
		SHOP_REMINDERNOTIFICATIONS ("shop.ReminderNotifications"),
		EE_REMINDER_DAYS_BEFORE_OPEN_ENR_END_DATE ("shop.EEReminderDaysBeforeOpenEnrEndDate"),
		EMPLOYER_DASHBOARD_KEY ("shop.employer.DashboardLink"),
		EMPLOYER_REMINDER_ELIGIBILITY_REMINDER_TIME ("shop.ERReminderEligibilityReminderTime"),
		EMPLOYER_SAMPLEPLANS_BUTTON ("shop.employer.SamplePlansButton"),
		EMPLOYER_OPTIONAL_EMPLOYEE_EMAIL ("shop.OptionalEmployeeEmail"),
		EMPLOYER_ENROLLMENTSTATUS_EARLY ("shop.earlyEREEenrollStatusToActiveAndPayRecvd"), 
		EMPLOYER_NEWHIRE_OPEN_ENROLLMENT_PERIOD ("shop.employer.NewHireOpenEnrollmentPeriod"),
		EMPLOYER_OPEN_ENROLLMENT_LASTDAY ("shop.employer.OpenEnrollmentLastDay");

		private final String value;
		@Override
		public String getValue(){return this.value;}
		SHOPConfigurationEnum(String value){
	        this.value = value;
	    }
	};

}
