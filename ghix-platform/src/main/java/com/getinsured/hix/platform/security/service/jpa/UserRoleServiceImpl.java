package com.getinsured.hix.platform.security.service.jpa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.Role.IS_EXCLUSIVE;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.security.RoleAccessControl;
import com.getinsured.hix.platform.security.duo.DuoAdminApi;
import com.getinsured.hix.platform.security.duo.DuoAdminApi.DuoAdminApiReturnStatus;
import com.getinsured.hix.platform.security.repository.IUserRoleRepository;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.util.PlatformErrorCode;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

@Service("user_roles")
@Repository
public class UserRoleServiceImpl implements UserRoleService{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRoleServiceImpl.class);

	@Autowired private IUserRoleRepository userRoleRepository;
	@Autowired private RoleService roleService;
	@Autowired private RoleAccessControl accessControl;

	@Override
	public List<UserRole> findByUser(AccountUser user) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findAllUserRolesByUserId(user.getId());
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return userRoleList;
	}

	@Override
	public List<UserRole> findByUserForUpdate(AccountUser user) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByUser(user);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterUserRoleListForCurrentUser(RoleAccessControl.UPDATE_ROLE_PERMISSION,userRoleList);
	}

	@Override
	public List<UserRole> findByUserForAdd(AccountUser user) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByUser(user);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterUserRoleListForCurrentUser(RoleAccessControl.ADD_ROLE_PERMISSION,userRoleList);
	}


	@Override
	public Set<UserRole> setUserRole(String roleName, AccountUser user){
		UserRole userRoleObj = null;
		Role role = null;
		Set<UserRole> userRoleList = null;

		try {
			if(accessControl.canAddRole(roleName)){
				userRoleObj = new UserRole();
				role = roleService.findRoleByName(roleName);
				userRoleObj.setRole(role);
				userRoleObj.setUser(user);
				userRoleObj.setRoleFlag('Y');
				userRoleList = new HashSet<UserRole>();
				userRoleList.add(userRoleObj);
			}else{
				LOGGER.error("Current logged in user:"+accessControl.getCurrentUserRoleName()+" Does not have sufficient privileges to provision role:"+roleName);
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO SET USR ROLE: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return userRoleList;
	}

	/**
	 * Returning null from this method will indicate operation failed
	 */
	@Override
	@Transactional
	public UserRole saveUserRole(UserRole userrole){
		if(userrole == null){
			LOGGER.error("No user role provided for save operation");
			return null;
		}
		UserRole userRole = null;
		try {
			if(accessControl.canUpdateRole(userrole.getRole().getName())){
				userRole= userRoleRepository.saveAndFlush(userrole);
			}
			else{
				LOGGER.error("Permission denied, current user with role:"+accessControl.getCurrentUserRoleName()+
						" does not have sufficient privileges to update Role:"+userrole.getRole().getName());
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO SAVE USR ROLE: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return userRole;
	}

	/**
	 * Returning null from this method will indicate operation failed
	 */
	@Override
	@Transactional
	public UserRole addUserRole(UserRole userrole){
		String errMsg = null;
		IS_EXCLUSIVE isExclusive = IS_EXCLUSIVE.N;
		boolean allowSave = false;
		if(userrole == null){
			LOGGER.error("No user role provided for save operation");
			return null;
		}
		UserRole userRole = null;
		try {
			if(userrole.getRoleFlag() == RoleService.ROLE_FLAG_DEFAULT){
				LOGGER.info("Allowing Self provisioning user role "+userrole.getRole().getName());
				allowSave = true; //Self provisioning, default role is always created at the time of user creation
			}
			else{
				// If this role is not a default role, then this user is an existing user and being added a role
				isExclusive = userrole.getRole().getIsExclusive();
				if(isExclusive.name().equals(IS_EXCLUSIVE.Y.name())){
					errMsg = "Permission denied, exclusive roles can not be added to any other role";
					LOGGER.error(errMsg);
					throw new RuntimeException(errMsg);
				}
				if(!accessControl.canAddRole(userrole.getRole().getName())){
					errMsg = "Permission denied, current user with role:"+accessControl.getCurrentUserRoleName()+
							" does not have sufficient privileges to update Role:"+userrole.getRole().getName();
					LOGGER.error(errMsg);
					throw new RuntimeException(errMsg);
				}
				allowSave = true;
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO SAVE USR ROLE: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		if(allowSave){
			userRole= userRoleRepository.saveAndFlush(userrole);
		}
		return userRole;
	}

	@Override
	public List<UserRole> getUserListByRoleId(String roleId) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByRoleId(roleId);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FIND USR LIST BY ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return userRoleList;
	}

	@Override
	public List<UserRole> getUpdateUseRoleListForRole(String roleId) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByRoleId(roleId);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FIND USR LIST BY ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}

		return accessControl.filterUserRoleListForCurrentUser(RoleAccessControl.UPDATE_ROLE_PERMISSION, userRoleList);
	}


	@Override
	public List<String> getAllUsersRoles() {
		List<String> rolesList = null;

		try {
			rolesList = userRoleRepository.findAllUsersRoleNames();
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO GET ALL USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return rolesList;
	}

	@Override
	public List<String> getAllUsersRolesForUpdate() {
		List<String> rolesList = null;

		try {
			rolesList = userRoleRepository.findAllUsersRoleNames();
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO GET ALL USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterRolesForCurrentUser(RoleAccessControl.UPDATE_ROLE_PERMISSION, rolesList);
	}

	@Override
	public List<String> getAllUsersRolesForAdd() {
		List<String> rolesList = null;

		try {
			rolesList = userRoleRepository.findAllUsersRoleNames();
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO GET ALL USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterRolesForCurrentUser(RoleAccessControl.ADD_ROLE_PERMISSION, rolesList);
	}

	@Override
	public List<UserRole> findUserRoleByUserId(AccountUser user) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByUser(user);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH USR ROLE BY ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return userRoleList;
	}

	@Override
	public List<UserRole> findUserRoleByUserForUpdate(AccountUser user) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByUser(user);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH USR ROLE BY ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterUserRoleListForCurrentUser(RoleAccessControl.UPDATE_ROLE_PERMISSION,userRoleList);
	}

	@Override
	public List<UserRole> findUserRoleByUserForAdd(AccountUser user) {
		List<UserRole> userRoleList = null;

		try {
			userRoleList = userRoleRepository.findByUser(user);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH USR ROLE BY ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterUserRoleListForCurrentUser(RoleAccessControl.ADD_ROLE_PERMISSION,userRoleList);
	}

	@Override
	public List<String> findUserRoleNames(AccountUser user) {
		List<String> roleNames = null;

		try {
			roleNames = userRoleRepository.findUserRoleNames(user.getId());
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FIND USR ROLE NAMES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return roleNames;
	}

	@Override
	public List<String> findUserRoleNamesForAdd(AccountUser user) {
		List<String> roleNames = null;

		try {
			roleNames = userRoleRepository.findUserRoleNames(user.getId());
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FIND USR ROLE NAMES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterRolesForCurrentUser(RoleAccessControl.ADD_ROLE_PERMISSION,roleNames);
	}

	@Override
	public List<String> findUserRoleNamesForUpdate(AccountUser user) {
		List<String> roleNames = null;

		try {
			roleNames = userRoleRepository.findUserRoleNames(user.getId());
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FIND USR ROLE NAMES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterRolesForCurrentUser(RoleAccessControl.UPDATE_ROLE_PERMISSION,roleNames);
	}

	@Override
	@Transactional
	public boolean removeUserRoleFromUser(UserRole userRole){
		String errMsg = null;
		boolean allowSave = false;
		
		try {
			if(userRole.getRoleFlag() == RoleService.ROLE_FLAG_DEFAULT){
				LOGGER.error("Default role can not be removed");
				throw new RuntimeException("Default role can not be removed");
			}
			else{
				// If this role is not a default role, then this user is an existing user and being added a role
				if(!accessControl.canAddRole(userRole.getRole().getName())){
					errMsg = "Permission denied, current user with role:"+accessControl.getCurrentUserRoleName()+
							" does not have sufficient privileges to remove a Role:"+userRole.getRole().getName();
					LOGGER.error(errMsg);
					throw new RuntimeException(errMsg);
				}
				allowSave = true;
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO SAVE USR ROLE: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		if(allowSave){
			userRole.setIsActive('N');
			userRoleRepository.saveAndFlush(userRole);
		}
		return true;
	}
	
	@Override
	@Transactional
	public void changeDefaultUserRole(AccountUser user, String newDefaultRole, boolean multipleRolesAllowed){
		try {
			Role requiredRole = this.roleService.findRoleByName(newDefaultRole);
			if(requiredRole == null) {
				throw new GIRuntimeException("Required role ("+newDefaultRole+") does not exist");
			}
			List<UserRole> roles = this.findByUser(user);
			boolean roleSet = false;
			for(UserRole tmp: roles) {
				if(tmp.isDefaultRole() && tmp.getRole().getName().equalsIgnoreCase(newDefaultRole)) {
					roleSet = true; // Nothing to do
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("User already as the default role set as desired role, nothing to do");
					}
					break;
				}
				if(tmp.isDefaultRole()) {
					if(!multipleRolesAllowed) {
						// this is the only role user has, just make it inactive
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("User has role as "+tmp.getRole().getName()+" disabling it");
						}
						tmp.setIsActive('N');
						tmp.setRoleFlag('N');
					}
					else {
						// User may have multiple, this one is default, make it non default
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("Found the default role as "+tmp.getRole().getName()+" making it non default it");
						}
						tmp.setRoleFlag('N');
					}
					// Done with existing default role, save it
					userRoleRepository.saveAndFlush(tmp);
				}
				if(tmp.getRole().getName().equalsIgnoreCase(newDefaultRole)) {
					// the new default role is available for user, make is default and active if it is not
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("User already have role as "+tmp.getRole().getName()+" enabling it as default role it");
					}
					tmp.setRoleFlag('Y');
					tmp.setIsActive('Y');
					userRoleRepository.saveAndFlush(tmp);
					roleSet = true;
				}
			}
			if(!roleSet) {
				//Existing role clean up is done but the new one is not available yet
				UserRole ur = new UserRole();
				ur.setUser(user);
				ur.setIsActive('Y'); // Make is active
				ur.setRoleFlag('Y'); // Set to default
				ur.setRole(requiredRole); //Set the role
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Provisioning role "+requiredRole.getName()+" as default");
				}
				userRoleRepository.saveAndFlush(ur);
				roles.add(ur);
			}
			// Finally update the user object
			user.getUserRole().clear();
			user.getUserRole().addAll(roles);
			user.setDefRole(requiredRole);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO SAVE USR ROLE: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
	}
	
	@Override
	//@GiAudit(transactionName = "Enable/Disable Duo Association", eventType = EventTypeEnum.USER_ACCOUNTS, eventName = EventNameEnum.MODIFY_ACCOUNTS)
	public List<GiAuditParameter> toggleDuoAssociation (AccountUser user) {
		String toggleDuoAssociation = null;
		List <GiAuditParameter> auditParams = new ArrayList<GiAuditParameter>();
		DuoAdminApiReturnStatus duoAdminApiReturnStatus = null;
		
		if(null == user){ return auditParams; }
		
		List<UserRole> userRoles = this.findUserRoleByUserId( user);
		if ( ( ! "active".equalsIgnoreCase(user.getStatus())  ) || user.getConfirmed() == 0   ){
			for (UserRole userRole : userRoles) {
				 if (userRole.isActiveUserRole() && "Y".equalsIgnoreCase( userRole.getRole().getMfaEnabled() ) ){
					 toggleDuoAssociation = "DISABLE";
					 break;
				 }
			 }
		}
		else {
			for (UserRole userRole : userRoles) {
				 if (userRole.isActiveUserRole() && "Y".equalsIgnoreCase( userRole.getRole().getMfaEnabled() ) ){
					 toggleDuoAssociation = "ENABLE";
					 break;
				 }
			 }
		}	
			
		if("DISABLE".equalsIgnoreCase(toggleDuoAssociation) ){
			duoAdminApiReturnStatus = DuoAdminApi.removeDuoCheck(user.getUserName()); 
		}else if("ENABLE".equalsIgnoreCase(toggleDuoAssociation) ){
			duoAdminApiReturnStatus = DuoAdminApi.addDuoCheck(user.getUserName());
		}
		if(null != duoAdminApiReturnStatus) {
			switch (duoAdminApiReturnStatus) {
				case AccountAdded:
					auditParams.add(new GiAuditParameter("userId ",   user.getId()));
					auditParams.add(new GiAuditParameter("userName ",   user.getUserName()));
					auditParams.add( new GiAuditParameter("Duo ac has been enabled ", user.getFullName() ) );
					/*GiAuditParameterUtil.add(new GiAuditParameter("userId ",   user.getId()),
							 new GiAuditParameter("userName ", user.getUserName()),
							 new GiAuditParameter("Duo ac has been enabled ", user.getFullName() )
							 );*/
					break;
				case AccountRemoved:
					auditParams.add(new GiAuditParameter("userId ",   user.getId()));
					auditParams.add(new GiAuditParameter("userName ",   user.getUserName()));
					auditParams.add( new GiAuditParameter("Duo ac has been disabled ", user.getFullName() ) );
					
					/*GiAuditParameterUtil.add(new GiAuditParameter("userId ",   user.getId()),
							 new GiAuditParameter("userName ", user.getUserName()),
							 new GiAuditParameter("Duo ac has been disabled ", user.getFullName() )
							 );*/
					break;
				case APIError:
				case SetUPError:
					auditParams.add(new GiAuditParameter("userId ",   user.getId()));
					auditParams.add(new GiAuditParameter("userName ",   user.getUserName()));
					auditParams.add( new GiAuditParameter("Failed to execute Duo request ", duoAdminApiReturnStatus.name() ));
					/*GiAuditParameterUtil.add(new GiAuditParameter("userId ",   user.getId()),
							 new GiAuditParameter("userName ", user.getUserName()),
							 new GiAuditParameter("Failed to execute Duo request ", duoAdminApiReturnStatus.name() )
							 );*/
					break;	
				default:
					break;
			}
		}
		return auditParams;
		
	}
}