package com.getinsured.hix.platform.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.GHIXApplicationContext;

/*
 * Enhanced to support HIX-46480 : Configuration to set the number of SecurityQuestion Sets 
 * -Venkata Taddepalli
 */

public class SecurityQuestionsHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityQuestionsHelper.class);
	private static final String LABEL_SECURITYQUESTION_SET = "label.securityquestion.set";
	
	private MessageSource messageSource;

	/**
	 * Objects are returned of type QuestionSetOne
	 */
	public class QuestionSet{
		private String code;
		private String name;

		protected QuestionSet(String code, String name) {
			this.code = code;
			this.name = name;
		}
		
		public QuestionSet(String code){
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
	}
	
	/**
	 * Objects are returned of type QuestionSetOne
	 */
	public class QuestionSetOne{
		private String code;
		private String name;

		protected QuestionSetOne(String code, String name) {
			this.code = code;
			this.name = name;
		}
		
		public QuestionSetOne(String code){
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
	}
//Security Question 2
	public class QuestionSetTwo {
		private String code;
		private String name;

		protected QuestionSetTwo(String code, String name) {
			this.code = code;
			this.name = name;
		}
		
		public QuestionSetTwo(String code){
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

	}
	
	//security question set three
	
	//Security Question 2
		public class QuestionSetThree {
			private String code;
			private String name;

			protected QuestionSetThree(String code, String name) {
				this.code = code;
				this.name = name;
			}
			
			public QuestionSetThree(String code){
				this.code = code;
			}
			
			public String getCode() {
				return code;
			}

			public String getName() {
				return name;
			}

		}
	
	private List<QuestionSetOne> questionSetOne = null;
	private List<QuestionSetTwo> questionSetTwo = null;
	private List<QuestionSetThree> questionSetThree = null;
	
	private Hashtable<String,List<QuestionSet>> htQuestionSet = new Hashtable<String,List<QuestionSet>>();

	public SecurityQuestionsHelper() {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("ADDING QSTNS FRM CONFIG");
		}
		
		try {
			this.messageSource = (MessageSource)GHIXApplicationContext.getBean("messageSource");
			if(null != this.messageSource) {
				htQuestionSet.put("QuestionSet1", fetchQuestionSet(1));
				htQuestionSet.put("QuestionSet2", fetchQuestionSet(2));
				htQuestionSet.put("QuestionSet3", fetchQuestionSet(3));
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE INIT OF SEC QSTNS: ",ex);
		}
	    
	}
	
	/**
	 * Method which fetches the question set from the security_question.properties label file
	 * 
	 * @param setNo int indicating the question set no
	 * @return List of question sets
	 */
	private List<QuestionSet> fetchQuestionSet(int setNo) {
		List<QuestionSet> questionSet = null;
		String securityQuestionsStr = null;
		String[] securityQuestionsArr = null;
		
		try {
			questionSet = new ArrayList<QuestionSet>();
			securityQuestionsStr = messageSource.getMessage(LABEL_SECURITYQUESTION_SET+setNo, null, LocaleContextHolder.getLocale());
			if(null != securityQuestionsStr && !securityQuestionsStr.isEmpty()) {
				securityQuestionsArr = securityQuestionsStr.split(",");
				for(int index=0;index < securityQuestionsArr.length;index++){
					questionSet.add(new QuestionSet(String.valueOf(index+1), securityQuestionsArr[index]));
				}
			}
		} catch(Exception ex) {
			LOGGER.warn("ERR: WHILE INIT OF SEC QSTNS: ",ex);
		}
		
		return questionSet;
	}

	public List<QuestionSetOne> getQuestionSetOne() {
		return questionSetOne;
	}
	public List<QuestionSetTwo> getQuestionSetTwo() {
		return questionSetTwo;
	}
	public List<QuestionSetThree> getQuestionSetThree() {
		return questionSetThree;
	}
	
	public List<QuestionSet> getQuestionSet(int Idx) {
		
		List<QuestionSet> questionSet = null;
		if(Idx>0 && Idx<4){
			questionSet = htQuestionSet.get("QuestionSet"+Idx);
		} 
		return questionSet;
	}
	
	
	public Hashtable<String, List<QuestionSet>> getHtQuestionSet() {
		return htQuestionSet;
	}

	public void setHtQuestionSet(Hashtable<String, List<QuestionSet>> htQuestionSet) {
		this.htQuestionSet = htQuestionSet;
	}
	
	public boolean isValidQuestion(int setIdx,String question){
		boolean isValid=false;
		
		List<QuestionSet> qstnSet = this.getQuestionSet(setIdx);

		for(QuestionSet currQstn:qstnSet){
			String currentQuestion=XssHelper.stripXSS(currQstn.getName());
			if(currentQuestion.equals(question)){
				isValid=true;
				break;
			}
		}
		
		return isValid;
		
	}

	public String getQuestionSetOneName(String code){
		QuestionSetOne st = new QuestionSetOne(code);
		int ind = this.questionSetOne.indexOf(st);
		if( ind >= 0) {
			return this.questionSetOne.get(ind).getName();
		}
		return null;
	}
}