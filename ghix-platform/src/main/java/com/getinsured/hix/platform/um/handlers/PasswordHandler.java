package com.getinsured.hix.platform.um.handlers;

import java.io.IOException;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import java.io.InputStream;
import com.atlassian.httpclient.api.HttpStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.handler.HandlerConstants;
import com.getinsured.hix.platform.handler.RequestHandler;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.hix.platform.security.service.PasswordService;

@Component
@DependsOn("configProp")
public class PasswordHandler implements RequestHandler {
	public static final String NAME = "PASSWORD_HANDLER";
	public static final String PASSWORD_OPRATION_KEY = "PASSWORD_OPERATION";
	public static final String CURRENT_PASSWORD = "CURRENT_PASSWORD";
	public static final String NEW_PASSWORD = "NEW_PASSWORD";
	public static final int VALIDATE_REGEX = 0;
	public static final int MATCH = 1;
	public static final int GENERATE_NEW = 2; // Not supported for now
	public static final int CHANGE_SELF = 3;
	public static final int CHANGE_ADMIN = 4;
	
	
	public static final int PASSWORD_MATCH = 1;
	public static final int PASSWORD_VALIDATE = 2;
	
	@Autowired
	private SCIMUserManager scimUserManager;
	@Autowired 
	private GhixEncryptorUtil ghixEncryptorUtil;
	@Autowired 
	private PasswordService passwordService;  
	@Autowired 
	private IUserRepository userRepository;
	@Autowired
	private HttpClient restHttpClient;
	
	private Logger LOGGER = LoggerFactory.getLogger(PasswordHandler.class);
	
	@Value("#{configProp['platform.im.wso2environment'] != null ? configProp['platform.im.wso2environment'] : '5.0'}")
	private String wso2Environment;
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return NAME;
	}

	@Override
	public List<String> getHandlerPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean handleRequest(HashMap<String, Object> context, ServletRequest request, boolean async) {
		int operation  = (int)context.get(PASSWORD_OPRATION_KEY);
		boolean wso2Env = (Boolean)context.get(HandlerConstants.WSO2_ENABLED_KEY);
		boolean success = false;
		switch(operation){
			case MATCH:
				if(wso2Env){
					success = matchPasswordWithWso2(context);
				}else{
					success = matchPasswordWithDb(context);
				}
				break;
			case VALIDATE_REGEX:
				context.put(HandlerConstants.HANDLER_ERROR, "Operation not yet supported");
				success = false;
				break;
			case CHANGE_ADMIN:
				if(wso2Env){
					success = updatePasswordOnWso2(context,true);
				}else{
					LOGGER.info("Change password called in admin mode, existing password must have been validated or its a forced password change");
					success = updatePasswordOnDB(context);
				}
				if(success && wso2Env){
					LOGGER.info("Cleaning up user profile after successful password change");
					updateDBForRemotePasswordUpdate(context);
				}
				break;
			case CHANGE_SELF:
				LOGGER.info("Initiating self password change");
				if(wso2Env){
					success = updatePasswordOnWso2(context,false);
				}else{
					success = updatePasswordOnDB(context);
				}
				if(success && wso2Env){
					LOGGER.info("Cleaning up user profile after successful password change");
					updateDBForRemotePasswordUpdate(context);
				}
				break;
			default:
				context.put(HandlerConstants.HANDLER_ERROR, "Unknown operation "+operation);
				success = false;
					
		}
		return success;
		
	}

	private boolean matchPasswordWithDb(HashMap<String, Object> context) {
		LOGGER.info("Initiating password match with database");
		AccountUser user = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		String userUuid=user.getUuid();
		String userProvidedPassword = (String)context.get(PASSWORD_KEY);
		userProvidedPassword = userProvidedPassword+userUuid;
		boolean result=ghixEncryptorUtil.verifyEncryptedPassword(userProvidedPassword, user.getPassword());
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private void put(String attrName, Object val, boolean mandatory, JSONObject payload){
		if(val == null && mandatory){
			throw new RuntimeException("Mandatory attribute "+attrName+" Not provided");
		}
		if(val != null){
			payload.put(attrName, val);
		}
	}

	@SuppressWarnings("unchecked")
	private String createChangePasswordRequest(AccountUser accountUser, HashMap<String,Object> context, boolean admin) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		TenantDTO tenantDTO = (TenantDTO)context.get(TENANT_DTO_KEY);
		if(tenantDTO == null){
			throw new RuntimeException("No tenant context available, can not proceed with password reset");
		}
		reqJsonObject = new JSONObject();
		put("tenantAdmin".intern(), tenantDTO.getTenantProvisionigUser(),true,reqJsonObject);
		put("tenantPassword".intern(), tenantDTO.getTenantProvisionigUserPassword(),true,reqJsonObject);
		put("tenantDomain".intern(), tenantDTO.getTenantDomain(),true,reqJsonObject);
		
		payloadJsonObject = new JSONObject();
		put("userName".intern(), accountUser.getUsername(),true,payloadJsonObject);
		if(!admin){
			// Current password is required only if its a self password reset
			put("oldPassword".intern(), StringEscapeUtils.unescapeHtml((String)context.get(CURRENT_PASSWORD)).trim(),true,payloadJsonObject); 
		}
		put("password".intern(), StringEscapeUtils.unescapeHtml((String)context.get(NEW_PASSWORD)).trim(),true,payloadJsonObject);
		reqJsonObject.put("payload", payloadJsonObject);
		if (null != reqJsonObject) {
			request = reqJsonObject.toJSONString();
		}
		if(LOGGER.isTraceEnabled()){
			// PII LOG
			LOGGER.trace("Password reset payload, isAdminReset:["+admin+"]\n"+request);
		}
		return request;
	}
	
	@SuppressWarnings("unchecked")
	private String createAuthenticateUserRequest(AccountUser accountUser, HashMap<String,Object> context) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		String userProvidedPass = (String)context.get(HandlerConstants.PASSWORD_KEY);
		
		TenantDTO tenantDTO = (TenantDTO)context.get(TENANT_DTO_KEY);
		
		reqJsonObject = new JSONObject();
		put("tenantAdmin".intern(), tenantDTO.getTenantProvisionigUser(),true,reqJsonObject);
		put("tenantPassword".intern(), tenantDTO.getTenantProvisionigUserPassword(),true,reqJsonObject);
		put("tenantDomain".intern(), tenantDTO.getTenantDomain(),true,reqJsonObject);

		payloadJsonObject = new JSONObject();
		put("userName".intern(), accountUser.getUsername(),true,payloadJsonObject);
		put("password".intern(), StringEscapeUtils.unescapeHtml(userProvidedPass).trim(),true,payloadJsonObject);

		reqJsonObject.put("payload", payloadJsonObject);
		if (null != reqJsonObject) {
			request = reqJsonObject.toJSONString();
		}
		return request;
	}
	private boolean matchPasswordWithWso2(HashMap<String, Object> context) {
		boolean success = false;
		LOGGER.info("Updating user password for remotely managed user");
		AccountUser user = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		switch(wso2Environment){
		case WSO2_ENV_5_3:
			String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"5.3/authenticate";
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Using WSO2 Environment 5.3 for URI:"+uri);
			}
			HttpPost post = new HttpPost(uri);
			HttpEntity payload = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(createAuthenticateUserRequest(user, context)).build();
			post.setEntity(payload);
			try {
				CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
				int status = response.getStatusLine().getStatusCode();
				if(HttpStatus.OK.code != status){
					LOGGER.error("Error response received with status:"+status);
					context.put(HandlerConstants.HANDLER_ERROR, Utils.getResponseString(response.getEntity()));
				}else{
					String updateUserResponse = Utils.getResponseString(response.getEntity());
					success= updateUserResponse != null && updateUserResponse.compareToIgnoreCase("true") == 0;
				}
				response.close();
			} catch (IOException e) {
				LOGGER.error("Error authenticating user",e);
				success = false;
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
			} 
			return success;
		case WSO2_ENV_5_0:
			String userProvidedPass = (String)context.get(HandlerConstants.PASSWORD_KEY);
			try {
				return scimUserManager.authenticate(user.getUsername(), userProvidedPass);
			} catch (GIException e) {
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
				return false;
			}
		default:
			context.put(HandlerConstants.HANDLER_ERROR, "Un supported environment for WSO2 "+wso2Environment);
			return false;
		}
	}
	
	private boolean updatePasswordOnWso2(HashMap<String, Object> context, boolean isAdminChange) {
		boolean success = false;
		LOGGER.info("Using SCIM Manager to update the password for remote user");
		AccountUser user = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		switch(wso2Environment){
		case WSO2_ENV_5_3: 
			String uri = null;
			if(isAdminChange){
				uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"5.3/changepswdByAdmin"; 
			}else{
				uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"5.3/changepswd";
			}
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Using WSO2 Environment 5.3 for URI:".intern()+uri);
			}
			HttpPost post = new HttpPost(uri);
			HttpEntity payload = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(createChangePasswordRequest(user, context, isAdminChange)).build();
			post.setEntity(payload);
			try {
				CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
				int status = response.getStatusLine().getStatusCode();
				if(HttpStatus.OK.code != status){
					LOGGER.error("Error response received with status:"+status);
					context.put(HandlerConstants.HANDLER_ERROR, Utils.getResponseString(response.getEntity()));
				}else{
					HttpEntity responseEntity = response.getEntity();
					InputStream is = responseEntity.getContent();
					String scimResponse = IOUtils.toString(is);
					if(!StringUtils.contains(scimResponse,"EXCEPTION")){
						success = true;
					}
				}
				response.close();
			} catch (IOException e) {
				LOGGER.error("Error changing user password".intern(),e);
				success = false;
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
			} 
			return success;
		case WSO2_ENV_5_0:
			String userProvidedPass = (String)context.get(PasswordHandler.NEW_PASSWORD);
			try {
				String  currentPassword = ((String)context.get(CURRENT_PASSWORD)).trim();
				user.setPassword(currentPassword);
				return scimUserManager.changePassword(user, userProvidedPass);
			} catch (GIException e) {
				context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
				context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
				return false;
			}
		default:
			context.put(HandlerConstants.HANDLER_ERROR, "Un supported environment for WSO2 ".intern()+wso2Environment);
			return false;
		}
	}
	private boolean updateDBForRemotePasswordUpdate(HashMap<String, Object> context){
		LOGGER.info("Updating DB for post password update".intern());
		AccountUser domainUser = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		AccountUser currentUser = (AccountUser)context.get(HandlerConstants.ACTING_ACCOUNT_USER_KEY);
		int updatedBy = domainUser.getId();
		if(currentUser != null){
			updatedBy = currentUser.getId();
		}
		if(domainUser != null){
			domainUser.setPassword("PASSWORD_NOT_IN_USE".intern());
			domainUser.setPasswordLastUpdatedTimeStamp(new TSDate());
			domainUser.setRetryCount(0);
			domainUser.setSecQueRetryCount(0);
			domainUser.setConfirmed(1);
			domainUser.setStatus("Active");
			domainUser.setPasswordRecoveryToken(null);
			domainUser.setPasswordRecoveryTokenExpiration(null);
			domainUser.setLastUpdatedBy(updatedBy);
		}
		return null != userRepository.save(domainUser);
	}
	
	private boolean updatePasswordOnDB(HashMap<String, Object> context)  {
		boolean success = false;
		AccountUser domainUser = (AccountUser)context.get(HandlerConstants.ACCOUNT_USER_KEY);
		String newPassword = (String)context.get(PasswordHandler.NEW_PASSWORD);
		try {
			passwordService.updatePassword(domainUser, newPassword, true);
			success = true;
			LOGGER.info("Updated password in DB".intern());
		} catch (GIException e) {
			LOGGER.error("Password Change failed, User :: ".intern() + domainUser.getUsername(), e);
		}
		return success;
	}
}
