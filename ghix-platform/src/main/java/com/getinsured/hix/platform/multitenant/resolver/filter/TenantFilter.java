/**
 *
 */
package com.getinsured.hix.platform.multitenant.resolver.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.enums.AffiliateFlowStatus;
import com.getinsured.affiliate.enums.AffiliateStatus;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.affiliate.service.AffiliateFlowConfigService;
import com.getinsured.hix.platform.multitenant.resolver.exception.TenantRuntimeException;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * Tenant HTTP Request filter.
 *
 * <p>
 *     Tries to figure out what tenant is configured for requested domain and sets information
 *     about it in {@link TenantContextHolder} object which is in turn using {@link ThreadLocal}
 *     to store tenant information.
 * </p>
 *
 * @author jgolubenko
 */
@Component("tenantFilter")
public class TenantFilter implements Filter
{
  private static final Logger log = LoggerFactory.getLogger(TenantFilter.class);

  public static boolean isTenantEnabled = "on".equals(System.getProperty("tenant.enabled"));
  private static final String YES = "Y";

  private static final String GETINSURED_TENANT_CODE = "GINS";
  private static final String[] LOCAL_HOST_NAMES = {"localhost", "127.0.0.1"};

  private static String defaultHostName;

  @Autowired
  private TenantService tenantService;

  @Autowired
  private AffiliateFlowConfigService affiliateFlowConfigService;

  @Autowired
  private GIExceptionHandler giExceptionHandler;

  @Autowired
  private TenantUtil tenantUtil;

  @Value("#{configProp['appUrl'] != null ? configProp['appUrl'] : null}")
  private String appUrl;

  /**
   * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
   */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {
        /* nothing to initialize */
  }

  /**
   * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
   *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException
  {
    final HttpServletRequest httpRequest = (HttpServletRequest) request;
    final HttpServletResponse httpResponse = (HttpServletResponse) response;

    if (isTenantEnabled)
    {
      if (!GhixUtils.isStaticResourceRequest(httpRequest))
      {
        boolean retrievedFromCookie = tenantUtil.setTenantFromCookies(httpRequest);

        if (retrievedFromCookie && log.isDebugEnabled())
        {
          log.debug("Set tenant in ThreadLocal from received cookie for url: {}{}", httpRequest.getRequestURL(), (httpRequest.getQueryString() != null) ? "?" + httpRequest.getQueryString() : "");
          log.debug("TenantContext has: tenantId: {}, affiliateId: {}, flowId: {}", TenantContextHolder.getTenant().getId(), TenantContextHolder.getAffiliateId(), TenantContextHolder.getFlowId());
        }

        // If cookies didn't contain anything interesting, try to get from parameters and fallback to hostname match.
        if (!retrievedFromCookie)
        {
          Long affiliateId = null;
          Integer flowId = null;

          String affiliateIdStr = httpRequest.getParameter("affiliateId");
          String flowIdStr = httpRequest.getParameter("flowId");

          if (affiliateIdStr != null && !"".equals(affiliateIdStr))
          {
            try
            {
              affiliateId = Long.parseLong(affiliateIdStr);
            } catch (NumberFormatException e)
            {
              if (log.isErrorEnabled())
              {
                log.error("Cannot parse affiliate id from parameter value: '{}', error: {}", affiliateIdStr, e.getMessage());
              }
            }
          }

          if (flowIdStr != null && !"".equals(flowIdStr))
          {
            try
            {
              flowId = Integer.parseInt(flowIdStr);
            } catch (NumberFormatException e)
            {
              if (log.isErrorEnabled())
              {
                log.error("Cannot parse flow id from parameter value: '{}', error: {}", flowIdStr, e.getMessage());
              }
            }
          }

          boolean updatedThreadLocalAndCookie = tenantUtil.initTenantByAffiliateIdAndFlowId(httpRequest, httpResponse, affiliateId, flowId);

          if (!updatedThreadLocalAndCookie)
          {
            setTenantInThreadLocal(httpRequest, null);
            tenantUtil.setAffiliateIdFromHeaderIfPresent(httpRequest);

            if (log.isDebugEnabled())
            {
              log.debug("Set tenant in ThreadLocal from based on domain: {} for url: {}{}", getHostNameFromRequest(httpRequest), httpRequest.getRequestURL(), (httpRequest.getQueryString() != null) ? "?" + httpRequest.getQueryString() : "");
              log.debug("TenantContext has: tenantId: {}, affiliateId: {}, flowId: {}", TenantContextHolder.getTenant().getId(), TenantContextHolder.getAffiliateId(), TenantContextHolder.getFlowId());
            }
          }
        }
      }
    }

    chain.doFilter(httpRequest, httpResponse);
  }

  /**
   * Clears tenant cache.
   * <p>
   *   Call this any time you update/create Tenant stuff.
   * </p>
   * @deprecated You no longer need to call this method, cache will be cleared periodically or automatically.
   */
  @Deprecated
  public static void clearHostToTenantCache()
  {
		/* Ignore for now until we use redis/couch */
  }

  private void setTenantInThreadLocal(final HttpServletRequest httpRequest, String hostName)
  {
    TenantContextHolder.clear();
    TenantContextHolder.setRequesturl(httpRequest.getRequestURL().toString());
		
    /*
     * Set Client IP Address
     */
    String ipAddress = httpRequest.getHeader("X-FORWARDED-FOR");
    if (ipAddress == null)
    {
      ipAddress = httpRequest.getRemoteAddr();
    }
    TenantContextHolder.setIPAddress(ipAddress);

    TenantDTO tenant = null;

    try
    {
      if (hostName == null)
      {
        hostName = getHostNameFromRequest(httpRequest);
      }

      tenant = getTenantForUrl(httpRequest, hostName);
    } catch (Exception e)
    {
      log.error("Error initializing the tenant", e);
    }

    if (tenant != null && YES.equals(tenant.getIsActive()))
    {
      TenantContextHolder.setTenant(tenant);
      httpRequest.getSession().setAttribute(TenantUtil.TENANT_ATTR_NAME, tenant);
      httpRequest.getSession().setAttribute(TenantUtil.TENANT_KEY_HEADER,tenant.getUrl());
      httpRequest.getSession().setAttribute(TenantUtil.TENANT_CODE_ATTR,tenant.getCode());
      tenantUtil.setAncillaryConfigIfNeeded(httpRequest, tenant);
    } else
    {
      String active = "false";

      if (tenant != null)
      {
        active = tenant.getIsActive();
      }

      if (log.isErrorEnabled())
      {
        log.warn("Could not retrieve tenant information for hostname: [{}], tenant = [{}], active = [{}]", hostName, tenant, active);
      }

      giExceptionHandler.recordCriticalException(GIRuntimeException.Component.PLATFORM, null, "Invalid tenant - " + hostName + " or it's not ACTIVE");
      throw new TenantRuntimeException("Invalid hostname: " + hostName);
    }
  }

  /**
   * @see javax.servlet.Filter#destroy()
   */
  @Override
  public void destroy()
  {
    TenantContextHolder.clear();
  }

  private TenantDTO getTenantForUrl(final HttpServletRequest httpRequest, final String hostName)
  {
    TenantDTO tenant = tenantService.getTenantForUrl(hostName);
    Affiliate affiliate = null;
    AffiliateFlow affiliateFlow = null;
    if (tenant == null)
    {
      affiliate = tenantService.getAffiliateForUrl(hostName);
      if (affiliate != null)
      {
        if (AffiliateStatus.ACTIVE.name().equals(affiliate.getAffiliateStatus()))
        {
          affiliateFlowConfigService.setAffiliateConfigurationInSession(httpRequest, affiliate.getAffiliateId(),
              affiliate.getHasEmployer(), affiliate.getAffiliateConfig());
          tenant = tenantService.getTenant(affiliate.getTenantId());
        }
      } else
      {

    	  affiliateFlow = tenantService.getAffiliateFlowForUrl(hostName);
    	  if (affiliateFlow != null)
    	  {
    		  if (AffiliateFlowStatus.ACTIVE.name().equals(affiliateFlow.getFlowStatus().name())){
    			  affiliateFlowConfigService.setAffiliateConfigurationInSession(httpRequest,
    					  affiliateFlow.getAffiliate().getAffiliateId(), affiliateFlow.getAffiliate().getHasEmployer(),
    					  affiliateFlow.getAffiliate().getAffiliateConfig());
    			  affiliateFlowConfigService.setFlowConfigurationInSession(httpRequest,
    					  affiliateFlow.getAffiliate().getAffiliateId(), affiliateFlow, null, null, affiliateFlow.getLogoURL(),
    					  null, affiliateFlow.getAffiliate().getHasEmployer());

    			  tenant = tenantService.getTenant(affiliateFlow.getTenantId());
    		  }
    	  }
      }
    }

    // If we failed to retrieve hostname based on tenant/affiliate/flow urls,
    // try to get configuration.properties->appUrl and figure out tenant based on that.
    if(affiliate == null && affiliateFlow == null && tenant == null && appUrl != null)
    {
      String appUrlHostname = TenantUtil.getHostnameFromUrl(appUrl);
      log.debug("Trying to use appUrl[{}] to get Tenant by hostname: {}", appUrl, appUrlHostname);

      // If we have http://localhost in appUrl, use default hostname for GI tenant from DB.
      for(String u : LOCAL_HOST_NAMES)
      {
        if(u.contains(appUrlHostname))
        {
          appUrlHostname = getDefaultHostName();
          break;
        }
      }

      tenant = tenantService.getTenantForUrl(appUrlHostname);
    }

    return tenant;
  }

  /**
   * Tries to get Hostname from request by checking the HEADER first, then if that fails,
   * queries DB for default GINS tenant hostname.
   *
   * @param httpRequest HTTP Servlet Request.
   * @return default hostname.
   */
  public String getHostNameFromRequest(HttpServletRequest httpRequest)
  {
    String hostName = "";

    // If non-static resource request, check if the http request already has tenant key
    // which means that this request is coming from the web to svc layer
    // and use that key instead to get the appropriate tenant
    if (httpRequest.getHeader(TenantUtil.TENANT_KEY_HEADER) != null)
    {
      hostName = httpRequest.getHeader(TenantUtil.TENANT_KEY_HEADER);

      if (log.isDebugEnabled())
      {
        log.debug("Tenant key from request HEADER is: {}", hostName);
      }
    } else
    {
      StringBuffer sb = httpRequest.getRequestURL();

      for (String host : LOCAL_HOST_NAMES)
      {
        if (sb.indexOf(host) != -1)
        {
          hostName = getDefaultHostName();
          break;
        }
      }

      if (hostName == null || "".equals(hostName))
      {
        hostName = TenantUtil.getHostnameFromUrl(httpRequest.getRequestURL().toString());
      }
    }

    return hostName;
  }


  /**
   * Get default hostname for GINS (GetInsured) Tenant and save it in static variable.
   * TODO: Change to: Not just get default hostname, but get default tenant right away...
   *
   * @return default hostname.
   */
  private String getDefaultHostName()
  {
    if (defaultHostName == null)
    {
      try
      {
        final TenantDTO tenant = tenantService.getTenant(GETINSURED_TENANT_CODE);

        if (tenant != null)
        {
          defaultHostName = tenant.getUrl();
        }
      } catch (Exception e)
      {
        log.error("Error getting default tenant info", e);
      } finally
      {
        if (defaultHostName == null)
        {
          throw new GIRuntimeException("Unable to determine default hostname");
        }
      }
    }

    if (log.isDebugEnabled())
    {
      log.debug("=> Returning default hostname: {} ", defaultHostName);
    }

    return defaultHostName;
  }

  public void setTenantService(TenantService tenantService)
  {
    this.tenantService = tenantService;
  }

  public void setAffiliateFlowConfigService(AffiliateFlowConfigService affiliateFlowConfigService)
  {
    this.affiliateFlowConfigService = affiliateFlowConfigService;
  }

  public void setGiExceptionHandler(GIExceptionHandler giExceptionHandler)
  {
    this.giExceptionHandler = giExceptionHandler;
  }

  public void setTenantUtil(TenantUtil tenantUtil)
  {
    this.tenantUtil = tenantUtil;
  }

  public void setAppUrl(String appUrl)
  {
    this.appUrl = appUrl;
  }

  public String getAppUrl()
  {
    return this.appUrl;
  }
}
