package com.getinsured.hix.platform.config;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.getinsured.hix.model.Tenant2TenantDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.service.GIAppConfigService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

@EnableScheduling
public class DynamicPropertiesUtil {
	private static final boolean isTenantEnabled = "on".equals(System.getProperty("tenant.enabled"));
	
	private static Logger logger = LoggerFactory.getLogger(DynamicPropertiesUtil.class);
    @Autowired
    private GIAppConfigService giAppConfigService;

	@Autowired
	private TenantRepository tenantRepository;

	private static TenantDTO defaultTenant;
	
	private static Map<Long, LocalCacheProperties> systemCache = new ConcurrentHashMap<Long, LocalCacheProperties>();
	
	@PostConstruct
	private void initDefaultTenant() {
		defaultTenant = Tenant2TenantDTO.Current.convert(tenantRepository.findByCode("GINS"));
		GiAppConfigSourceFactory.init(giAppConfigService);
		
		//load variables which change infrequently or never , to system cache
		//refresh this every 3hrs from GiAppConfigSourceFactory, which controls underlying caching mechanism.
		reloadLocalCache();
		
	}
	
	private static List<String> getLocalCachedProperties(){
		String properties = (null != GhixPlatformConstants.PLATFORM_LOCAL_CACHED_GIAPPCONFIGS) ? GhixPlatformConstants.PLATFORM_LOCAL_CACHED_GIAPPCONFIGS :"";
		List<String> localCachedProperties = Arrays.asList(properties.trim().split("\\s*#\\s*"));
		//List<String> localCachedProperties =  Splitter.on("#").omitEmptyStrings().trimResults().splitToList(GhixPlatformConstants.PLATFORM_LOCAL_CACHED_GIAPPCONFIGS);
		return localCachedProperties;
	}

	//every 5 hrs (in milliseconds hr(1000 * 3600secs) * 5hr )  this would be refreshed 
	@Scheduled(fixedDelay=18000000)
	//@Scheduled(fixedDelay=300000) // 5 mints for testing
	public  void reloadLocalCache() {
		List<String> propertiesName = DynamicPropertiesUtil.getLocalCachedProperties();
		systemCache.clear();
		logger.debug("DynamicPropertiesUtil Reload Local Cache : Initiating Refresh, Found : {} properties, creating local cache", propertiesName.size());

		LocalCacheProperties localProp = null;
		
		for (String propertyName : propertiesName) {
			if(propertyName != null) {
				localProp = systemCache.get(getTenantId());
				if (localProp != null) {
					localProp.wrap(propertyName, getPropertyValue(propertyName) );
				} else {
					localProp = new LocalCacheProperties();
					localProp.wrap(propertyName, getPropertyValue(propertyName) );
					systemCache.put(getTenantId(), localProp);
				}
			}
		}
	}

    public static String getPropertyValue(final String propertyName)
    {
        StringBuilder propertyValue = new StringBuilder();
        if (propertyName != null)
        {
        	//if property exist in local cache pass it from there
        	//or else let the existing implementation take care
        	if ( passFromLocalCache( propertyName, propertyValue ) ) {
        		logger.trace("DynamicPropertiesUtil Local cache exist for {} returning from local cache", propertyName);
        		return propertyValue.toString();
        	}
        	
        	logger.info("DynamicPropertiesUtil Local cache not exist for {} returning from backend implementation", propertyName);
        	
        	Object propertyValueAsObject = null;
    		if(isTenantEnabled) {
    			propertyValueAsObject = GiAppConfigSourceFactory.getPropertyValue(propertyName, getTenantId());
    		} else {
    			propertyValueAsObject = GiAppConfigSourceFactory.getPropertyValue(propertyName);
    		}
			
			if(null != propertyValueAsObject) {
				propertyValue.append(propertyValueAsObject.toString());
			}
        }
        return propertyValue.toString();
    }

    private static boolean passFromLocalCache(String propertyName , StringBuilder propertyValue) {
    	LocalCacheProperties localProp = systemCache.get(getTenantId());
    	if(localProp != null) {
    		if(localProp.get(propertyName) != null) {
    			propertyValue.append(localProp.get(propertyName));
    			return true;
    		}
    	}
		return false;
	}

	public static String getPropertyValue(final PropertiesEnumMarker propertiesEnumMarker)
    {
    	return getPropertyValue(propertiesEnumMarker.getValue());
    }
    
    public static Map<String, String> getPropertyValueList(final PropertiesEnumMarker propertiesEnumMarker)
    {
		if(isTenantEnabled) {
			return GiAppConfigSourceFactory.getPropertyValues(propertiesEnumMarker.getValue(), getTenantId());
		}
		return GiAppConfigSourceFactory.getPropertyValues(propertiesEnumMarker.getValue());
    }

	public void customInit() {

    }

	public GIAppConfigService getGiAppConfigService() {
		return giAppConfigService;
	}

	public void setGiAppConfigService(GIAppConfigService giAppConfigService) {
		this.giAppConfigService = giAppConfigService;
	}

	private static Long getTenantId()
	{
		Long tenantId = null;

		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null)
		{
			tenantId = TenantContextHolder.getTenant().getId();
		}
		else if(defaultTenant != null && defaultTenant.getId() != null)
		{
			tenantId = defaultTenant.getId();
		}

		return tenantId;
	}
	
	private static class LocalCacheProperties {
		Map<String, String> properties = new ConcurrentHashMap<String, String>();

		public void wrap(String property, String value) {
			if(property == null || value == null) {
				if(logger.isWarnEnabled()) {
					logger.warn("Unable to put property {} with value {}", property, value);
				}
			} else {
				this.properties.put(property, value);
			}
		}

		public String get(String property) {
			String value = null;

			// If property passed is null, .get() will throw NPE on ConcurrentHashMap, so we check
			// if it's there before getting it.
			if(this.properties.containsKey(property)) {
				value = this.properties.get(property);
			}

			if (value == null) {
				return null;
			}

			return value;
		}

	}
}
