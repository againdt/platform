package com.getinsured.hix.platform.util.exception;

/**
 * GetInsured generic exception.
 */
public class GIException extends Exception {
	
	/**
	 * Auto generated serial version id.
	 */
	
	private static final long	serialVersionUID	= 4278335833692506316L;
	
	private int errorCode;
	private String errorMsg;
	private String errorSeverity;
	
	public enum GhixErrors {
		/**
		 * 101, "Invalid coverage start date."
		 */
		INVALID_COVERAGE_DATE(101, "Invalid coverage start date."),
		/**
		 * 102, "Invalid date of birth."
		 */
		INVALID_BIRTH_DATE(102, "Invalid date of birth."),
		/**
		 * 103, "Zip Code is either missing or invalid."
		 */
		INVALID_ZIPCODE(103, "Zip Code is either missing or invalid."),
		/**
		 * 104, "Specified tobacco value is invalid."
		 */
		INVALID_TOBACCO_VALUE(104, "Specified tobacco value is invalid."),
		/**
		 * 105, "Member of type Self not specified. Can not process the request"
		 */
		MEMBER_SELF_NOT_SPECIFIED(105, "Member of type Self not specified. Can not process the request."),
		/**
		 * 106, "Child Age can not be higher than self/parent. Can not process the request."
		 */
		INVALID_CHILD_AGE(106, "Child Age can not be higher than self/parent. Can not process the request."),
		/**
		 * 106, "Invalid data for eligible members."
		 */
		INVALID_ELIGIBLE_MEMBERS(106, "Invalid data for eligible members."),
		/**
		 * 106, "Invalid data for shop dependents."
		 */
		INVALID_SHOP_DEPENDENTS(106, "Invalid data for shop dependents."),
		/**
		 * 106, "Invalid data for spouse id."
		 */
		INVALID_SPOUSE_ID(106, "Invalid data for spouse id."),
		/**
		* 106, "Invalid data for Employee ID."
		 */
		INVALID_EMPLOYEE_ID(106, "Invalid data for employee id."),
		/**
		 * 106, "Missing Agent ID/Assister ID."
		 */
		INVALID_AGENT_ID(106, "Missing Agent ID/Assister ID"),
		/**
		 * 106, "keeponlyflag is required."
		 */
		INVALID_KeepOnlyFlag(106, "KeepOnlyFlag is required"),
		/**
		 * 107, "Invalid County Specified. Can not process the request."
		 */
		INVALID_COUNTY(107, "Invalid County Specified. Can not process the request."),
		/**
		 * 108, "Invalid Zip Specified. Can not process the request."
		 */
		INVALID_ZIP(108, "Invalid Zip Specified. Can not process the request."),
		/**
		 * 108, "Missing Primary Phone number."
		 */
		INVALID_PRIMARY_PHONE(108, "Missing Primary Phone number."),
		/**
		 * 109, "Age can not be negative. Can not process the request."
		 */
		INVALID_AGE(109, "Invalid Age."),
		/**
		 * 109, "Missing Responsible Person Primary Phone Number."
		 */		
		INVALID_RESPONSIBLE_PERSON_PRIMARY_PHONE(109,"Missing Responsible Person Primary Phone Number."),
		/**
		 * 110, "Missing Household Contact Person Primary Phone Number"
		 */
		INVALID_HOUSEHOLD_CONTACT_PRIMARY_PHONE(110,"Missing Household Contact Person Primary Phone Number."),		
		/**
		 * 110, Household ID is missing
		 */		
		INVALID_HOUSEHOLD_ID(110, "Invalid Household Case Id."),
		/**
		 * 110, Household ID is missing
		 */		
		INVALID_DEATH_DATE(110, "Death date is missing or invalid"),
		/**
		 * 115, "Invalid County code."
		 */
		INVALID_COUNTY_CODE(115, "Invalid County code."),
		/**
		 * 113, "Invalid ExistingSADPEnrollmentID for ProductType H."
		 */
		INVALID_EXISTING_SADP_ENROLLMENTID_FOR_PRODUCTTYPE(113, "Invalid ExistingSADPEnrollmentID for ProductType H."),
		/**
		 * 113, "Invalid ExistingMedicalEnrollmentID for ProductType D."
		 */
		INVALID_EXISTING_MEDICAL_ENROLLMENTID_FOR_PRODUCTTYPE(113, "Invalid ExistingMedicalEnrollmentID for ProductType D."),/**
		 * 113, "Invalid ProductType A for AutoRenewal."
		 */
		INVALID_PRODUCTTYPE_FOR_AUTORENEWAL(113, "Invalid ProductType A for AutoRenewal."),
		/**
		 * 114, Invalid Date of Birth
		 */		
		INVALID_DATE_OF_BIRTH(114, "Invalid Date of Birth"),
		/**
		 * 114, Invalid Coverage Date
		 */		
		INVALID_COVERAGE_START_DATE(114, "Invalid Coverage Date"),
		/**
		 * 201, Benchmark plan not found
		 */
		NO_DATA_FOUND(201, "Benchmark plan not found."),
		/**
		 * 202, "Error while processing."
		 */
		ERROR_WHILE_PROCESSING(202, "Error while processing."),
		/**
		 * 200, "Service ran successfully."
		 */
		SUCCESS(200, "Service ran successfully."),
		/**
		 * 300, "Invalid request. One of the required field is missing in the request."
		 */
		INVALID_REQUEST(300, "Invalid request. One of the required field is missing in the request."),
		
		/**
		 * 111, "Relation Self should be specified only once."
		 */
		MULTIPLE_SELF_PRESENT(111, "Relation Self should be specified only once. Can not process the request."),
		
		/**
		 * 112, "Relation Spouse should be specified only once."
		 */
		MULTIPLE_SPOUSE_PRESENT(112, "Relation Spouse should be specified only once. Can not process the request."),
		/**
		 * 107, "Missing one of the Custodial Parent Element."
		 */
		MISSING_CUSTODIAL_ELEMENT(107, "Missing one of the Custodial Parent Element."),
		/**
		 * 101, "Invalid Employer Case ID or Order"
		 */
		INVALID_EMPLOYER_CASE_ID(101, "Invalid Employer Case ID or Order"),
		/**
		 * 102, "Invalid Employer ID or Order"
		 */
		INVALID_EMPLOYER_ID(102, "Invalid Employer ID or Order"),
		
		/**
		 * 103, "Table or cache region [gi_app_config] doesn't contain record with a given key"
		 */
		APP_CONFIG_KEY_NOT_FOUND(103, "Table or Cache region [gi_app_config] doesn't contain record with a given key"),
		
		ERROR_WHILE_GENERATING_SHOPPING_ID(127, "Error while saving Auto Renewal Information in Pd_Household and Pd_Person"),
		
		ERROR_WHILE_COPY(128, "Error while copying bean properties"),
		
		ERROR_WHILE_CALLING_CROSS_WALK_API(129, "Call to Plan Cross Walk Api is failed or No response from Api"),
		
		ERROR_WHILE_APPENDING_CSR_TO_HIOSID(130, "Error while appending CSR value to Hios Plan Id"),
		
		PLAN_NOT_RETURNED(131, "Plan is not returned"),
		
		ERROR_WHILE_CALLING_CREATE_AUTO_RENEWAL(132, "Exception occured while calling Enrollment createautorenewal api"),
		
		INVALID_PLAN_ID(133, "Invalid Plan Id"),
		
		INVALID_ENROLLMENT_ID(134, "Invalid Enrollment Id"),
		
		PLAN_LEVEL_NOT_FOUND(135, "Error while getting plan level for given renewal plan"),
		
		/**
		 * 521, "DRX AUthentication not configured."
		 */
		DRX_AUTH_CONFIG_ERROR(521, "DRX Authentication not configured"),
		/**
		 * 521, "DRX AUthentication Failed."
		 */
		DRX_AUTH_FAILED(521, "DRX AUthentication Failed."),;
		
		
		//... add more cases here ...

		private final int id;
		private final String message;

		GhixErrors(int id, String message) {
			this.id = id;
			this.message = message;
		}

		public int getId() { return id; }
		public String getMessage() { return message; }
	}
	
	public GIException() {
		super();
	}
	public GIException(String message, Throwable cause) {
		super(message, cause);
	}
	public GIException(String message) {
		super(message);
	}
	public GIException(Throwable cause) {
		super(cause);
	}
	
	public GIException(int errorCode, String errorMsg, String errorSeverity) {
		super(errorMsg);
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
		this.errorSeverity = errorSeverity;
	}
	
	public GIException(GhixErrors ghixError) 
	{
		this.errorCode = ghixError.getId();
		this.errorMsg = ghixError.getMessage();
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getErrorSeverity() {
		return errorSeverity;
	}
	public void setErrorSeverity(String errorSeverity) {
		this.errorSeverity = errorSeverity;
	}
	
}
