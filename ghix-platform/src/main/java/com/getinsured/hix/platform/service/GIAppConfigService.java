package com.getinsured.hix.platform.service;

import java.util.Map;

public interface GIAppConfigService {
   Object getPropertyValue(String propertyName);
   Map<String, String> getPropertyValues(String propertyName);
   void addTenantAppConfigurations(Long tenantId);
   Object getPropertyValue(String propertyName, Long tenantId);
   Map<String, String> getPropertyValues(String propertyName, Long tenantId);
   void refresh();
}
