package com.getinsured.hix.platform.notify;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notify.NotificationException;

public interface NotificationService {
	
	public void dispatch(Notice notice) throws NotificationException;
}
