package com.getinsured.hix.platform.util;


import java.lang.reflect.Field;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.context.MessageContext;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.util.payload.GIWSPayloadHandlerFactory;

/**
 * 
 * SoapClientLoggingInterceptor to intercept 
 * request/response for outgoing AHBX SOAP Web Services.
 * 
 * This should be used along with WebClient only. @see SoapWebClient
 * 
 * @author EkramAli Kazi
 *
 */
public class SoapClientLoggingInterceptor implements MethodInterceptor{

	private static final String UNKNOWN = "unknown";

	private static final Logger LOGGER = LoggerFactory.getLogger(SoapClientLoggingInterceptor.class);
	
	@Autowired private GIWSPayloadService giwsPayloadService;

	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		
		Object result = null;
		String correlationId = null;
		String globalId = null;
		String requestPayload = null;
		String responsePayload = null;
		String exceptionTrace = null;
		String status = "SUCCESS";
		HttpServletRequest request = null;
		MessageContext responseMessageContext = null;
		Object[] arg = methodInvocation.getArguments();
		
		try {
			requestPayload = SoapHelper.marshal(arg[0]);
			result = methodInvocation.proceed();
			responsePayload = SoapHelper.marshal(result);
		} catch (Exception e) {
			status = "FAILURE";
			exceptionTrace = ExceptionUtils.getFullStackTrace(e);
			throw e;
		} finally {
			try{
				if( RequestContextHolder.getRequestAttributes()!= null){
					request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
					correlationId = (request.getAttribute("LOG_CORRELATION_ID")!=null)?request.getAttribute("LOG_CORRELATION_ID").toString():null;
					globalId = (request.getAttribute("LOG_GLOBAL_ID")!=null)?request.getAttribute("LOG_GLOBAL_ID").toString():null;
					LOGGER.info("Global Id from client request  : " + globalId);
					Object obj = request.getAttribute("responseMessageContext");
					if (obj instanceof MessageContext){
						responseMessageContext = (request.getAttribute("responseMessageContext")!=null)?(MessageContext)request.getAttribute("responseMessageContext"):null;
					}else{
						responseMessageContext = null;
					}
				}
				
				/**
				 * Reading LOG_GLOBAL_ID from WebServiceMessageCallback
				 * Following code snippet is required for Enrollment IND20 
				 */
				if(StringUtils.isEmpty(globalId) && arg != null && arg.length >= 5 && arg[4] != null){
					Object webServiceMessageCallbackObject = arg[4];
					if(webServiceMessageCallbackObject instanceof WebServiceMessageCallback){
						WebServiceMessageCallback webServiceMessageCallback = (WebServiceMessageCallback)arg[4];
						Field field  = webServiceMessageCallback.getClass().getField("headerMap");
						globalId = getLogGlobalId(field, webServiceMessageCallback);
						
						LOGGER.info("Global Id from saajSoapMessage  : " + globalId);
					}
				}
				/*Ekram : HIX-33045 - Added for Eligibility RP module */
				String responseCode = SoapHelper.extractResponseCode(responsePayload);
				if (!responseCode.equalsIgnoreCase("200")) {
					status = "FAILURE";
				}
				String endpointUrl = (String) (arg != null ? (arg.length > 2 ? arg[1] : UNKNOWN) : UNKNOWN);
				String endpointFunction =  (String) (arg != null ? (arg.length > 3 ? arg[2] : UNKNOWN) : UNKNOWN);
				LOGGER.info("Soap Client Logging Interceptor ");
				GIWSPayload giwsPayload = new GIWSPayload();
				//giwsPayload.setCorrelationId(null);
				giwsPayload.setCorrelationId(correlationId);
				giwsPayload.setCreatedTimestamp(new TSDate());
				giwsPayload.setCreatedUserId(null);
				giwsPayload.setEndpointFunction(endpointFunction);
				giwsPayload.setEndpointOperationName(null);
				giwsPayload.setEndpointUrl(endpointUrl);
				giwsPayload.setExceptionMessage(exceptionTrace);
				giwsPayload.setRequestPayload(requestPayload);
				giwsPayload.setResponseCode(responseCode);
				giwsPayload.setResponsePayload(responsePayload);
				giwsPayload.setStatus(status);
				giwsPayload.setGlobalId(globalId);
				
				GIWSPayloadHandlerFactory.getHandler(endpointFunction).handlePayload(giwsPayload, result);
				
				saveSeviceLog(giwsPayload);
				
			} catch(Exception e){
				/** log and eat exception to give chance to get response back to the caller. Something gone wrong with SoapClientLoggingInterceptor recording*/
				LOGGER.error("Error recording request/response in SoapClientLoggingInterceptor --> " + ExceptionUtils.getFullStackTrace(e));
			}
		}
		return result;
	}
	
	private GIWSPayload saveSeviceLog(GIWSPayload giwsPayload){
		return giwsPayloadService.save(giwsPayload);
	}

	/**
	 * code added to retrieve GlobalId from Constructor arguments
	 * @param field
	 * @return
	 */
	private String getLogGlobalId(Field field, WebServiceMessageCallback webServiceMessageCallback)
	{
		String requestGlobalId = null;
		try
		{
			if(field != null)
			{
				if(!field.isAccessible())
				{
					field.setAccessible(true);
					@SuppressWarnings("unchecked")
					Map<String, String> headerMap = (Map<String, String>)field.get(webServiceMessageCallback);
					field.setAccessible(false);
					for(String key : headerMap.keySet())
					{
						if(key.equalsIgnoreCase("LOG_GLOBAL_ID"))
						{
							requestGlobalId = headerMap.get(key);
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception occurred in method getLogGlobalId(-, -): "+ex);
		}
		return requestGlobalId;
	}
}
