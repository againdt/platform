package com.getinsured.hix.platform.notify;

import java.io.Reader;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.timeshift.TimeShifterUtil;


public class TemplateSourceWrapper{
	private Object templateSource = null;
	private Reader sourceReader = null;
	private long initTime = -1l;
	private String path;
	
	private boolean expired=false;
	
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
	public TemplateSourceWrapper(){
		this.initTime = TimeShifterUtil.currentTimeMillis();
	}
	public Reader getSourceReader() {
		return sourceReader;
	}
	public void setSourceReader(Reader noticeReader) {
		this.sourceReader = noticeReader;
	}
	public TemplateSourceWrapper(Object noticeType) {
		this.initTime = TimeShifterUtil.currentTimeMillis();
		this.templateSource = noticeType;
	}
	public Object getTemplateSource() {
		return templateSource;
	}
	public void setNoticeType(Object noticeType) {
		this.templateSource = noticeType;
	}
	public boolean shouldRefreshTheSource() {
		if(this.templateSource instanceof Content){
			Content tmp = (Content)templateSource;
			if(tmp.getContentSource() ==  Content.COUCH){
				this.setExpired(true);
				return true;
			}
		}
		return (TimeShifterUtil.currentTimeMillis() - this.initTime) > (60*60*1000);
	}
	
	public void updateInitTime(){
		this.initTime = TimeShifterUtil.currentTimeMillis();
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
