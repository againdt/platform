package com.getinsured.hix.platform.location.service.jpa;

import java.util.List;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;

/**
 * This implements Address Validator with USPS. Future development if required.
 * 
 * @author polimetla_b
 * @since 11/1/2012
 */
public class AddressValidatorUSPS implements AddressValidatorComponent {

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.location.service.LocationValidatorService#
	 * validateLocation(com.getinsured.hix.platform.location.model.Location)
	 */
	public List<Location> validateAddress(Location address) throws Exception {

		throw new Exception("Not Implemented");
	}

	@Override
	public AddressValidationResponse validateAddress(LocationDTO address)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
