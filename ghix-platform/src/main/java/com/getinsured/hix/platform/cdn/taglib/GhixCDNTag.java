package com.getinsured.hix.platform.cdn.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import org.apache.taglibs.standard.tag.common.core.UrlSupport;

public class GhixCDNTag extends UrlSupport {
	private static final long serialVersionUID = 1;

	// ignore CDN logic and always do default behavior
	@Override
	public int doEndTag() throws JspException {
		return super.doEndTag();
	}

	// for tag attribute
	public void setValue(String value) throws JspTagException {
		this.value = value;
	}

	// for tag attribute
	public void setContext(String context) throws JspTagException {
		this.context = context;
	}

}
