package com.getinsured.hix.platform.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.InboxMsgClob;

public interface IInboxMsgClobRepository extends JpaRepository<InboxMsgClob, Long> {

	InboxMsgClob findById(long messageID);
	
}
