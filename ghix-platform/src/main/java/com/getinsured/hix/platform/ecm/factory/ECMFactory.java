package com.getinsured.hix.platform.ecm.factory;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.oracle.OracleContentManagementService;

/**
 * ECM Method Factory.
 *
 * <P>factory class to encapsulate ECM Service object creation logic based
 * on ECM TYPE configured in .properties files.
 *
 * <P>Currently, we have COUCH and ORACLE as two possible values.
 * Of Which COUCH is supported by the GHIX Application.
 *
 * @author Ekram Ali Kazi
 */
public final class ECMFactory
{
  public enum ECM
  {
     ORACLE, COUCH, MSCONTENT
  }

  private ECMFactory() {}

  /**
   * static method to return concrete ALFRESCO or ORACLE instance based on ecmType.
   *
   * @param ecmType - ENUM Type
   * @return concrete instance of ContentManagementService
   * @throws IllegalArgumentException
   */
  public static ContentManagementService createECMService(final String ecmType)
  {
    ContentManagementService ecmService;

    if (ECM.ORACLE.toString().equals(ecmType)) {
      // do more for setting session once OracleContentManagement is implemented...
      ecmService = new OracleContentManagementService();
    } else if (ECM.COUCH.toString().equals(ecmType)) {
      ecmService = (ContentManagementService) GHIXApplicationContext.getBean("couchEcmService");
    } else if (ECM.MSCONTENT.toString().equals(ecmType)) {
      ecmService = (ContentManagementService) GHIXApplicationContext.getBean("contentService");
    } else {
      throw new IllegalArgumentException(
          "\n\n***********************************************************\n"
              + "* Unknown ECM Type configured [" + ecmType + "] !!\n"
              + "* -DGHIX_HOME param or environment variable should point\n"
              + "* to <STATE-MAIN-BRANCH>/ because applicationPlatform.xml\n"
              + "* uses this value while bootstrapping context:\n"
              + "* file:///${GHIX_HOME}/ghix-setup/conf/configuration.properties\n"
              + "*\n"
              + "* NOTE: ALFRESCO is no longer supported.\n"
              + "*\n"
              + "****************************************************************\n\n");
    }

    return ecmService;
  }
}
