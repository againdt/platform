package com.getinsured.hix.platform.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapMessage;

/**
 * SoapClientResponseInterceptor to handle Response.
 * for SOAP Web Service clients.
 * 
 * @author nayak_b
 */

public class SoapClientResponseInterceptor implements ClientInterceptor {

	private static final Logger LOGGER = LoggerFactory.getLogger(SoapClientResponseInterceptor.class); 
	
	@Override
	public boolean handleRequest(MessageContext messageContext)
			throws WebServiceClientException {
	    return true;
	}

	@Override
	public boolean handleResponse(MessageContext messageContext) 
			throws WebServiceClientException {
		HttpServletRequest request = null;
		SoapMessage soapMessage = (SoapMessage) messageContext.getResponse();
		LOGGER.info("Soap Client Response Interceptor");
		try {
			if( RequestContextHolder.getRequestAttributes()!= null){
				request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
				SoapHelper.extractHeader(soapMessage,request);
				request.setAttribute("responseMessageContext", soapMessage); // HIX-45155 Enhance Payload Handler factory
			}
		} catch (Exception e) {
			LOGGER.error("Error handeling response in SoapClientResponseInterceptor --> " + ExceptionUtils.getFullStackTrace(e));
		}
	    return true;
	}

	@Override
	public boolean handleFault(MessageContext messageContext)
			throws WebServiceClientException {
		return true;
	}

	@Override
	public void afterCompletion(MessageContext arg0, Exception arg1) throws WebServiceClientException {
		// TODO Auto-generated method stub
		
	}

}
