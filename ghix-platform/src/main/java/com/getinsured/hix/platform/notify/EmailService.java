/*
 * =============================================================================
 * 
 *   Copyright (c) 2011-2012, The THYMELEAF team (http://www.thymeleaf.org)
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * 
 * =============================================================================
 */
package com.getinsured.hix.platform.notify;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.emailstat.service.IEmailStatService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.GhixUtils.EMAIL_STATS;
import com.getinsured.hix.platform.util.Utils;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGrid.Email;
import com.sendgrid.SendGridException;

@Service("emailServiceNew")
public class EmailService implements NotificationService{

	private static final Logger LOGGER = Logger.getLogger(EmailService.class);
    @Autowired private JavaMailSender mailSender;
	@Autowired private SendGrid sendGridAPI;
	@Autowired private IEmailStatService emialStatService;
	@Autowired private CloseableHttpClient restHttpClient;
	
	private JSONParser parser;
	
    /* 
     * Send HTML mail (simple) via notice object
     */
	
    public void dispatch(Notice noticeObj) throws NotificationException {
        
    	boolean hasAttachments = false; 
    	boolean isHtml = true; 
    	
		if(noticeObj.getAttachment() != null){
			hasAttachments = true;
		}
		
		if(noticeObj.getNoticeType().getType() .equals("PLAIN_EMAIL")){
			isHtml = false;
		}
		try{
			
	    	final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
	    	final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, hasAttachments, "UTF-8");
	    	message.setSubject(noticeObj.getSubject());
	        message.setFrom(noticeObj.getFromAddress());
	        message.setTo(noticeObj.getToAddress());
	        message.setText(noticeObj.getEmailBody(), isHtml);
	    	
	    	if(hasAttachments){
		    	List<String> fileLocations = getLocations(noticeObj.getAttachment());
				List <EmailAttachments> emailAttachments = new ArrayList<EmailAttachments>();
				for(String filepath: fileLocations){
					emailAttachments.add(getAttachments(filepath)); 
				}
				
				// Add the attachment
		        for(EmailAttachments emailAttachment: emailAttachments){
		        	  final InputStreamSource attachmentSource = new ByteArrayResource(emailAttachment.getAttachmentBytes());
		              message.addAttachment(emailAttachment.getAttachmentName(), attachmentSource, emailAttachment.getContentType());
		        }
	    	}
	        
	        // Send mail
	        this.mailSender.send(mimeMessage);
		}catch(MessagingException excp){
			LOGGER.error("Error while sending mail "+excp.getMessage());
			throw new NotificationException(excp.getMessage(), excp);
		}
    }
    
    /* 
     * Send HTML mail  via sendgrid java api
     */
    public void dispatch(Notice noticeObj, Map<GhixUtils.EMAIL_STATS, String> emailStatData) throws NotificationException {
        
    	boolean hasAttachments = false; 
    	boolean isHtml = true; 
    	
		if(noticeObj.getAttachment() != null){
			hasAttachments = true;
		}
		
		if(noticeObj.getNoticeType().getType() .equals("PLAIN_EMAIL")){
			isHtml = false;
		}
		try{
			ArrayList<String> params = new ArrayList<String>();
			params.add(String.valueOf(noticeObj.getId()));
			params.add(noticeObj.getNoticeType().getEmailClass());
			
			Email email = new Email();
			email.addUniqueArg("NoticeId", String.valueOf(noticeObj.getId()));
			email.addUniqueArg("NoticeType", noticeObj.getNoticeType().getEmailClass());
			email.addUniqueArg("AUTHKEY", GhixUtils.getGhixSecureCheckSum(params) );
			email.addUniqueArg(EMAIL_STATS.STATE_CODE.name(), DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE) );
			email.addUniqueArg(EMAIL_STATS.EXCHANGE_URL.name(), DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL) );
			
			emailStatData.put(GhixUtils.EMAIL_STATS.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			emailStatData.put(GhixUtils.EMAIL_STATS.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			
			email.setSubject(noticeObj.getSubject());
			email.setFrom(noticeObj.getFromAddress());
			email.addTo(noticeObj.getToAddress());
			if(isHtml){
				email.setHtml(noticeObj.getEmailBody());
			}else{
				email.setText(noticeObj.getEmailBody());
			}
			
			if(hasAttachments){
				List<String> fileLocations = getLocations(noticeObj.getAttachment());
				List <EmailAttachments> emailAttachments = new ArrayList<EmailAttachments>();
				for(String filepath: fileLocations){
					emailAttachments.add(getAttachments(filepath)); 
				}
				
				// Add the attachment
		        for(EmailAttachments emailAttachment: emailAttachments){
		        	  email.addAttachment(emailAttachment.getAttachmentName(), new ByteArrayInputStream(emailAttachment.getAttachmentBytes() ) );
		        }
			}
			
			sendGridAPI.send(email);
	        this.saveEmailStat(noticeObj.getId(), emailStatData);
		}catch(SendGridException | IOException | NoSuchAlgorithmException excp){
			LOGGER.error("Error while sending mail "+excp.getMessage());
			throw new NotificationException(excp.getMessage(), excp);
		}
    }
    
    private void saveEmailStat(int noticeId, Map<GhixUtils.EMAIL_STATS, String> emailStatData) {
		if(null == emailStatData){
			return;
		}
		
		try {
			emialStatService.saveEmailStat(emailStatData, noticeId);
		} catch (Exception e) {
			LOGGER.error("Error while recording email meta data ", e);
		}
	}

	
    public EmailAttachments getAttachments(String filePath) throws NotificationException{
    	 EmailAttachments emailAttachment  = new EmailAttachments();
    	if(GhixUtils.isGhixValidPath(filePath)){
    		File file = new File(filePath);
    		byte fileContent[] = null;
    		String contentType = null;
    		FileInputStream fin = null;
    	    try
    	    {
    	      //create FileInputStream object
    	      fin = new FileInputStream(file);
              fileContent = new byte[(int)file.length()];
              fin.read(fileContent);
    	     
              URI uri = file.toURI();
              URL u = uri.toURL();
              URLConnection uc = u.openConnection();
              contentType = uc.getContentType();	

              emailAttachment.setAttachmentBytes(fileContent);
              emailAttachment.setAttachmentName(file.getName());
              emailAttachment.setContentType(contentType);       
    	    }
    	    catch(FileNotFoundException e)
    	    {
    	      //System.out.println("File not found" + e);
    	    	LOGGER.error("File not found",e);
    	    }
    	    catch(IOException ioe)
    	    {
    	      //System.out.println("Exception while reading the file " + ioe);
    	    	LOGGER.error("Exception while reading the file ",ioe);
    	    }finally{
    			IOUtils.closeQuietly(fin);
    			
    		}
    	}else{
    		throw new NotificationException("Application trying to reach a blacklisted folder or filename or extension type.");
    	}
		
		return emailAttachment;
	}
    
    private List<String> getLocations(String attachmentPath)
	{
		List<String> locations = new ArrayList<String>();
		StringTokenizer tokens = new StringTokenizer(attachmentPath, ";");
		while(tokens.hasMoreTokens())
		{
			locations.add(tokens.nextToken());
		}
		return locations;
	}


    public void dispatchToAll(Notification noticeObj) throws NotificationException{
        
    	boolean hasAttachments = false; 
    	boolean isHtml = true; 
    	
		if(noticeObj.getAttachment() != null){
			hasAttachments = true;
		}
		if(noticeObj.getNoticeType().getType() .equals("PLAIN_EMAIL".intern())){
			isHtml = false;
		}
		try{
			
	    	final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
	    	final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, hasAttachments, "UTF-8");
	    	message.setSubject(noticeObj.getSubject());
	        message.setFrom(noticeObj.getFromAddress());
	        List<String> toRecipients = noticeObj.getToRecipients();
	        if(toRecipients != null){
	        for(String toRecipient: toRecipients){
	        	message.addTo(toRecipient);
	        }
	        }
	       
	        List<String> bCCRecipients = noticeObj.getbCCRecipients();
	        if(bCCRecipients != null){
	        for(String bCCRecipient: bCCRecipients){
	        	message.addBcc(bCCRecipient);
	        }
	        }
	        message.setText(noticeObj.getEmailBody(), isHtml);
	    	
	    	if(hasAttachments){
		    	List<String> fileLocations = getLocations(noticeObj.getAttachment());
				List <EmailAttachments> emailAttachments = new ArrayList<EmailAttachments>();
				for(String filepath: fileLocations){
					emailAttachments.add(getAttachments(filepath)); 
				}
				// Add the attachment
		        for(EmailAttachments emailAttachment: emailAttachments){
		        	  final InputStreamSource attachmentSource = new ByteArrayResource(emailAttachment.getAttachmentBytes());
		              message.addAttachment(emailAttachment.getAttachmentName(), attachmentSource, emailAttachment.getContentType());
		        }
	    	}
	        // Send mail
	        this.mailSender.send(mimeMessage);
		}catch(MessagingException excp){
			LOGGER.error("Error while sending mail "+excp.getMessage());
			throw new NotificationException(excp.getMessage(), excp);
		}
    }
    
    public boolean sendEmailRequest(Notice notice) {
    	boolean success = false;
    	notice.setStatus(STATUS.PENDING);
    	HttpPut post = null;
		try {
			post = new HttpPut(new URI(GhixPlatformConstants.REMOTE_NOTIFICATION_URL));
			HttpEntity payload = EntityBuilder.create()
					.setContentType(ContentType.TEXT_PLAIN)
					.setText(GhixAESCipherPool.encrypt(notice.getEmailBody())).build();
			post.setEntity(payload);
			CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
			int status = response.getStatusLine().getStatusCode();
			String responseMsg = Utils.getResponseString(response.getEntity());
			if(HttpStatus.SC_OK != status){
				notice.setStatus(STATUS.FAILED);
			}else{
				notice.setStatus(STATUS.EMAIL_SENT);
				success= true;
			}
			this.updateResponseInNoticeBody(notice, responseMsg);
			response.close();
		} catch (Exception e) {
			this.updateResponseInNoticeBody(notice, e.getMessage());
			notice.setStatus(STATUS.FAILED);
			success = false;
		}finally {
			if(post != null) {
				post.releaseConnection();
			}
		}
    return success;
	}
    
    private void updateResponseInNoticeBody(Notice noticeObj,String message) {
    	String emailBody = noticeObj.getEmailBody();
    	if(this.parser == null) {
    		this.parser = new JSONParser();
    	}
    	if(emailBody != null) {
    		try {
				JSONObject obj = (JSONObject) parser.parse(emailBody);
				obj.put("BASE64_RESPONSE".intern(), Base64.getEncoder().encodeToString(message.getBytes()));
			} catch (Exception e) {
				LOGGER.error("Email body is not is json format and remote send attempted, please check the configuration if remote send should be enabled");
			}
    		
    	}
		
	}


    /*
    
     
     * Send HTML mail with inline image
     
    public void sendMailWithInline(
            final String recipientName, final String recipientEmail, final String imageResourceName, 
            final byte[] imageBytes, final String imageContentType, final Locale locale)
            throws MessagingException {
        
        // Prepare the evaluation context
        final Context ctx = new Context(locale);
        ctx.setVariable("name", recipientName);
        ctx.setVariable("subscriptionDate", new TSDate());
        ctx.setVariable("hobbies", Arrays.asList("Cinema", "Sports", "Music"));
        ctx.setVariable("imageResourceName", imageResourceName); // so that we can reference it from HTML
        
        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = 
                new MimeMessageHelper(mimeMessage, true  multipart , "UTF-8");
        message.setSubject("Example HTML email with inline image");
        message.setFrom("thymeleaf@example.com");
        message.setTo(recipientEmail);

        // Create the HTML body using Thymeleaf
        final String htmlContent = this.templateEngine.process("email-inlineimage.html", ctx);
        message.setText(htmlContent, true  isHtml );
        
        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
        message.addInline(imageResourceName, imageSource, imageContentType);
        
        // Send mail
        this.mailSender.send(mimeMessage);
        
    }*/
}
