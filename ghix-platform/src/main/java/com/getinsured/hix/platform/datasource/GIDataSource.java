package com.getinsured.hix.platform.datasource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.datasource.AbstractDataSource;
import org.springframework.jdbc.datasource.lookup.DataSourceLookup;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.util.Assert;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class GIDataSource extends AbstractDataSource implements InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(GIDataSource.class);

	private static final ThreadLocal<String> TENANT_CONTEXT_HOLDER = new ThreadLocal<String>();
	
	private static final String JNDI_JDBC = "java:comp/env/jdbc";

	private Map<Object, Object> targetDataSources;

	private Object defaultTargetDataSource;

	private DataSourceLookup dataSourceLookup = new JndiDataSourceLookup();

	private Map<Object, DataSource> resolvedDataSources;

	private DataSource resolvedDefaultDataSource;
	
	public GIDataSource(){
		Map<Object, Object> targetDataSourcesLookup = new HashMap<Object, Object>();
		Context ctx;
		try {
			ctx = new InitialContext();
			NamingEnumeration<NameClassPair> list2 = ctx.list(JNDI_JDBC);
			while (list2.hasMore()) {
				String dataSourceJndiName = list2.next().getName();
				LOGGER.debug("Initializing "+dataSourceJndiName);
				String dataSourceJndiLookupKey = JNDI_JDBC.concat("/").concat(dataSourceJndiName);
				DataSource ds = (DataSource)ctx.lookup(dataSourceJndiLookupKey);
				String dataSourceLookupKey = dataSourceJndiName.substring(0, dataSourceJndiName.length()-2);
				LOGGER.debug("Initialized the entry with key:"+dataSourceLookupKey+" with the datasource whose Jndi Name is "+dataSourceJndiLookupKey);
				targetDataSourcesLookup.put(dataSourceLookupKey, ds);
				if(dataSourceJndiName.equalsIgnoreCase(GhixPlatformConstants.MULTITENANT_DEFAULT_DATASOURCE_JNDI_NAME)){
					/**
					 * TODO Consider externalization of the jndi name which should be considered as default datasource instead of ghixDS
					 */
					LOGGER.debug("Initialized the default datasource with the jndi resource having name: "+dataSourceJndiName);
					this.setDefaultTargetDataSource(ds);
				}
			}
			GIDataSource.this.setTargetDataSources(targetDataSourcesLookup);
			LOGGER.debug("Target DataSources initialization complete "+targetDataSourcesLookup);
			LOGGER.debug("Total target DataSources are "+targetDataSourcesLookup.size());
		} catch (Exception ex) {
			LOGGER.error("Error initializing the GIDataSource:", ex);
			System.exit(1);
		}

	}

	public void setTargetDataSources(Map<Object, Object> targetDataSources) {
		this.targetDataSources = targetDataSources;
	}

	public void setDefaultTargetDataSource(Object defaultTargetDataSource) {
		this.defaultTargetDataSource = defaultTargetDataSource;
	}

	public void setDataSourceLookup(DataSourceLookup dataSourceLookup) {
		this.dataSourceLookup = (dataSourceLookup != null ? dataSourceLookup : new JndiDataSourceLookup());
	}

	@Override
	public void afterPropertiesSet() {
		if (this.targetDataSources == null) {
			throw new IllegalArgumentException("Property 'targetDataSources' is required");
		}
		this.resolvedDataSources = new HashMap<Object, DataSource>(this.targetDataSources.size());
		for (Map.Entry<Object, Object> entry : this.targetDataSources.entrySet()) {
			Object lookupKey = resolveSpecifiedLookupKey(entry.getKey());
			DataSource dataSource = resolveSpecifiedDataSource(entry.getValue());
			this.resolvedDataSources.put(lookupKey, dataSource);
		}
		if (this.defaultTargetDataSource != null) {
			this.resolvedDefaultDataSource = resolveSpecifiedDataSource(this.defaultTargetDataSource);
		}
	}

	protected Object resolveSpecifiedLookupKey(Object lookupKey) {
		return lookupKey;
	}

	protected DataSource resolveSpecifiedDataSource(Object dataSource)
			throws IllegalArgumentException {
		if (dataSource instanceof DataSource) {
			return (DataSource) dataSource;
		} else if (dataSource instanceof String) {
			return this.dataSourceLookup.getDataSource((String) dataSource);
		} else {
			throw new IllegalArgumentException("Illegal data source value - only [javax.sql.DataSource] and String supported: "	+ dataSource);
		}
	}

	@Override
	public Connection getConnection() throws SQLException {
		return determineTargetDataSource().getConnection();
	}

	@Override
	public Connection getConnection(String username, String password)
			throws SQLException {
		return determineTargetDataSource().getConnection(username, password);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T unwrap(Class<T> iface) throws SQLException {
		if (iface.isInstance(this)) {
			return (T) this;
		}
		return determineTargetDataSource().unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return (iface.isInstance(this) || determineTargetDataSource().isWrapperFor(iface));
	}

	protected DataSource determineTargetDataSource() {
		Assert.notNull(this.resolvedDataSources, "DataSource router not initialized");
		Object lookupKey = determineCurrentLookupKey();
		DataSource dataSource = this.resolvedDataSources.get(lookupKey);
		if (dataSource == null && (GhixPlatformConstants.MULTITENANT_DEFAULT_FALLBACK && lookupKey == null)) {
			dataSource = this.resolvedDefaultDataSource;
		}
		if (dataSource == null) {
			throw new IllegalStateException("Cannot determine target DataSource for lookup key ["+ lookupKey + "]");
		}
		return dataSource;
	}

	protected Object determineCurrentLookupKey(){
		LOGGER.debug("Lookup is being made for a datasource with key: "+TENANT_CONTEXT_HOLDER.get());
		return TENANT_CONTEXT_HOLDER.get();
	}

	public static void setTenantKey(String tenantKey) {
		TENANT_CONTEXT_HOLDER.set(tenantKey);		
	}

}
