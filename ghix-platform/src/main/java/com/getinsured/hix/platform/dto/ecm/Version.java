package com.getinsured.hix.platform.dto.ecm;

import org.apache.commons.lang.StringUtils;


public class Version {

	private String name;

	private String versionLabel;

	private String versionSeriesId;

	private String versionSeriesCheckedOutBy;

	private String versionSeriesCheckedOutId;

	private boolean isMajorVersion;

	private boolean isLatestVersion;

	private boolean isLatestMajorVersion;

	private String checkinComment;

	private long contentStreamLength;

	public Version(String name, String versionLabel, String versionSeriesId, String versionSeriesCheckedOutBy,
			String versionSeriesCheckedOutId,boolean  isMajorVersion,boolean  isLatestVersion,boolean  isLatestMajorVersion,
			String checkinComment, long contentStreamLength) {
		this.name = name;
		this.versionLabel = versionLabel != null ? versionLabel : StringUtils.EMPTY;
		this.versionSeriesId = versionSeriesId != null ? versionSeriesId : StringUtils.EMPTY;
		this.versionSeriesCheckedOutBy = versionSeriesCheckedOutBy != null ? versionSeriesCheckedOutBy : StringUtils.EMPTY;
		this.versionSeriesCheckedOutId = versionSeriesCheckedOutId != null ? versionSeriesCheckedOutId : StringUtils.EMPTY;
		this.isMajorVersion = isMajorVersion;
		this.isLatestVersion = isLatestVersion;
		this.isLatestMajorVersion = isLatestMajorVersion;
		this.checkinComment = checkinComment != null ? checkinComment : StringUtils.EMPTY;
		this.contentStreamLength = contentStreamLength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersionLabel() {
		return versionLabel;
	}

	public void setVersionLabel(String versionLabel) {
		this.versionLabel = versionLabel;
	}

	public String getVersionSeriesId() {
		return versionSeriesId;
	}

	public void setVersionSeriesId(String versionSeriesId) {
		this.versionSeriesId = versionSeriesId;
	}

	public String getVersionSeriesCheckedOutBy() {
		return versionSeriesCheckedOutBy;
	}

	public void setVersionSeriesCheckedOutBy(String versionSeriesCheckedOutBy) {
		this.versionSeriesCheckedOutBy = versionSeriesCheckedOutBy;
	}

	public String getVersionSeriesCheckedOutId() {
		return versionSeriesCheckedOutId;
	}

	public void setVersionSeriesCheckedOutId(String versionSeriesCheckedOutId) {
		this.versionSeriesCheckedOutId = versionSeriesCheckedOutId;
	}

	public boolean isMajorVersion() {
		return isMajorVersion;
	}

	public void setMajorVersion(boolean isMajorVersion) {
		this.isMajorVersion = isMajorVersion;
	}

	public boolean isLatestVersion() {
		return isLatestVersion;
	}

	public void setLatestVersion(boolean isLatestVersion) {
		this.isLatestVersion = isLatestVersion;
	}

	public boolean isLatestMajorVersion() {
		return isLatestMajorVersion;
	}

	public void setLatestMajorVersion(boolean isLatestMajorVersion) {
		this.isLatestMajorVersion = isLatestMajorVersion;
	}

	public String getCheckinComment() {
		return checkinComment;
	}

	public void setCheckinComment(String checkinComment) {
		this.checkinComment = checkinComment;
	}

	public long getContentStreamLength() {
		return contentStreamLength;
	}

	public void setContentStreamLength(long contentStreamLength) {
		this.contentStreamLength = contentStreamLength;
	}

	@Override
	public String toString() {
		return "Version [name=" + name + ", versionLabel=" + versionLabel
				+ ", versionSeriesId=" + versionSeriesId
				+ ", versionSeriesCheckedOutBy=" + versionSeriesCheckedOutBy
				+ ", versionSeriesCheckedOutId=" + versionSeriesCheckedOutId
				+ ", isMajorVersion=" + isMajorVersion + ", isLatestVersion="
				+ isLatestVersion + ", isLatestMajorVersion="
				+ isLatestMajorVersion + ", checkinComment=" + checkinComment
				+ ", contentStreamLength=" + contentStreamLength + "]";
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
	
	


}
