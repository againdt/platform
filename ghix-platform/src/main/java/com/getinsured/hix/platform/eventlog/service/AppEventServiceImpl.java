package com.getinsured.hix.platform.eventlog.service;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.eventlog.BrowserInfo;
import com.getinsured.hix.platform.eventlog.ControllerInfo;
import com.getinsured.hix.platform.eventlog.EventDto;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.EventUserInfo;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

@Service("appEventService")
public class AppEventServiceImpl implements AppEventService {

	@Autowired UserService userService;
	@Autowired AppEventLogService appEventLogService;
	@Autowired RoleService roleService;
	private static final Logger LOGGER = LoggerFactory.getLogger(AppEventService.class);
	private static final String HTTP_HEADER_X_FORWARDED_FOR = "X-FORWARDED-FOR";
	private static final String HTTP_HEADER_USER_AGENT = "User-Agent";
	private static final String HTTP_HEADER_REFERER = "referer";
	private static final String SYSTEM_USER = "System User";
	/**
	 * 
	 * @param eventInfoDto
	 * @param mapParameters
	 */
	@Override
	public void record(EventInfoDto eventInfoDto,	Map<String, String> mapParameters) {
		EventDto eventDto = populateEventDto(eventInfoDto, mapParameters, getLoggedInUser());
		if(eventDto != null) {	
			appEventLogService.record(eventDto, eventInfoDto.getComments());
		} else {
			LOGGER.warn("Unable to save the event.Event details object is not valid");
		}
	}
	
	@Override
	public void record(EventInfoDto eventInfoDto,	Map<String, String> mapParameters, AccountUser accountUser) {
		EventDto eventDto = populateEventDto(eventInfoDto, mapParameters, accountUser);
		if(eventDto != null) {	
			appEventLogService.record(eventDto, eventInfoDto.getComments());
		} else {
			LOGGER.warn("Unable to save the event.Event details object is not valid");
		}
	}
	

	/**
	 * Fill the Dto with the values passed from the producer of the event
	 * @param eventInfoDto
	 * @param mapParameters
	 * @return
	 */
	private EventDto populateEventDto(EventInfoDto eventInfoDto, Map<String, String> mapParameters, final AccountUser accountUser) {
		// Event info : event type and event name lookup value, module user details  
		EventInfoDto eventInfo = eventInfoDto;
		EventUserInfo eventUserInfo = populateEventUserInfo(accountUser);
		if(eventInfo == null)
		{
			LOGGER.info("AppEventServiceImpl:: EventInfo is found to be null");
		}
		if(eventInfo != null && eventInfo.getEventLookupId() == null)
		{
			LOGGER.info("AppEventServiceImpl:: eventInfo.getEventLookupId() is null");
		}
		if(eventUserInfo == null)
		{
			LOGGER.info("AppEventServiceImpl:: eventUserInfo is null");
		}
		
		if((eventInfo == null) || (eventInfo.getEventLookupId() == null) || (eventUserInfo == null)) {
			LOGGER.warn("Invalid event detail object");
			return null;
		}
		
		EventDto giEventDto = EventDto.create();
		giEventDto.setEventInfoDto(eventInfo);
		
		// User info - logged in user and active module user info
		giEventDto.setLoggedInUserInfo(eventUserInfo);
		
		ServletRequestAttributes reqAttrs =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes(); 
		HttpServletRequest request =(reqAttrs != null) ? reqAttrs.getRequest() : null;
		
		// Controller info : referrer URL. This will be NULL when the event is called from a service and not the WEB-Controller 
		ControllerInfo controllerInfo = getControllerInfo(request);
		giEventDto.setControllerInfo(controllerInfo);
		
		// Browser Info : IP address etc
		BrowserInfo browserInfo = getBrowserInfo(request);
		giEventDto.setBrowserInfo(browserInfo);
		
		// Parameter list to be added to the event dto
		if(mapParameters != null && !mapParameters.isEmpty()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			Set<String> keys = mapParameters.keySet();
			for (String key : keys) {
				parameters.put(key, mapParameters.get(key));
			}

			giEventDto.setMapParameters(parameters);			
		}
				
		// module id to be set. 
		// Precedence order : 1) Producer set it in EventInfoDto 2) Active module 3) logged in user
		setModuleUserInfo(giEventDto);
		
		if(StringUtils.isEmpty(eventInfoDto.getTimeStamp())){
		giEventDto.setEventCreationTimeStamp(new TSTimestamp());
		}
		else{
			String strDt = eventInfoDto.getTimeStamp();
			DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm",Locale.ENGLISH);
				Date dt;
				try {
					dt = sdf.parse(strDt);
					giEventDto.setEventCreationTimeStamp(new TSTimestamp(dt.getTime(),false));
				} catch (ParseException e) {
					LOGGER.error("Event parameter for timestamp was not in correct format: "+eventInfoDto.getTimeStamp());
					giEventDto.setEventCreationTimeStamp(new TSTimestamp());
					eventInfoDto.setTimeStamp(null);
				}
		}
		return giEventDto;
	}

	/**
	 * Set the appropriate Module id and module user
	 * Use Case: If producer of the event has provided the user info, this should be consider for the created-for of the event.
	 * 			 If the active module id is available consider this info.
	 * 			 else consider logged-in ad default.
	 * @param giEventDto
	 */
	private void setModuleUserInfo(EventDto giEventDto) {
		EventUserInfo userInfo = giEventDto.getLoggedInUserInfo();		
		giEventDto.setModuleId(String.valueOf(userInfo.getUserId()));
		giEventDto.setModuleName(userInfo.getUserRole());
		
		EventInfoDto eventInfo = giEventDto.getEventInfoDto();		
		// If producer has explicitly provide the user information
		if (eventInfo.getModuleId() != null) {
			giEventDto.setModuleId(String.valueOf(eventInfo.getModuleId()));
			giEventDto.setModuleName(eventInfo.getModuleName());
		} else if (userInfo.getActiveModuleId() > 0) {
			// If the logged-in user has done the role-switching
			giEventDto.setModuleId(String.valueOf(userInfo.getActiveModuleId()));
			giEventDto.setModuleName(userInfo.getActiveModuleName());
		}

	}

	private BrowserInfo getBrowserInfo(HttpServletRequest request) {
		BrowserInfo browserInfo = null;
		if(request == null) {
			return browserInfo;
		}
		
		String userAgent = request.getHeader(HTTP_HEADER_USER_AGENT);
		String ipAddress = request.getHeader(HTTP_HEADER_X_FORWARDED_FOR);
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		browserInfo = new BrowserInfo();
		browserInfo.setUserAgent(userAgent);
		browserInfo.setIpAddress(ipAddress);
		
		return browserInfo;	
	}

	private ControllerInfo getControllerInfo(HttpServletRequest request) {
		ControllerInfo info = null;
		if(request != null) {
			info = new ControllerInfo();
			String referrer = request.getHeader(HTTP_HEADER_REFERER);
			info.setRequestUrl(request.getRequestURL().toString());
			info.setReferrerUrl(referrer);
			info.setjSessionId(request.getRequestedSessionId());
			info.setJSessionActive(request.isRequestedSessionIdValid());
		}
		return info;
	}
	
	private EventUserInfo populateEventUserInfo(final AccountUser user) {
		EventUserInfo eventUserInfo = null;
		if (user != null) {
			eventUserInfo = new EventUserInfo();
			eventUserInfo.setUserId(user.getId());
			eventUserInfo.setUserName(user.getFullName());
			eventUserInfo.setActiveModuleId(user.getActiveModuleId());
			eventUserInfo.setActiveModuleName(user.getActiveModuleName());
			ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
			if (sra == null) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("No servlet request context available for thread [" + Thread.currentThread().getName()
							+ "] Node profile " + GhixPlatformConstants.PLATFORM_NODE_PROFILE + " With IP:"
							+ GhixPlatformConstants.LOCAL_NODE_IP
							+ " Can not use the session to check for the current role");
				}
				getDefaultRole(eventUserInfo, user);
			} else {
				// We have the request context available, this means we have the
				// request and the session
				HttpServletRequest req = sra.getRequest();
				HttpSession session = req.getSession(false); // We are
																// interested in
																// existing
																// sessions only
				if (session == null) {
					getDefaultRole(eventUserInfo, user);
				} else {
					String currentRoleName = (String) session.getAttribute("userActiveRoleName");
					Role currentRole = roleService.findRoleByName(currentRoleName);
					if (null != currentRole) {
						eventUserInfo.setUserRole(currentRole.getLabel());
					} else {
						getDefaultRole(eventUserInfo, user);
					}
				}
			}
		} else {
			LOGGER.info("AppEventServiceImpl:: No Logged in User Found");

			eventUserInfo = new EventUserInfo();
			eventUserInfo.setUserId(0);
			eventUserInfo.setUserName(SYSTEM_USER);
			eventUserInfo.setActiveModuleId(0);
			eventUserInfo.setActiveModuleName(null);
			eventUserInfo.setUserRole(SYSTEM_USER);
		}
		return eventUserInfo;
	}
	
	private AccountUser getLoggedInUser(){
		AccountUser user = null;
		try {
			Object userObj = userService.getLoggedInUser();
			LOGGER.info("AppEventServiceImpl:: Fetching User Info"+userObj);
			if (userObj instanceof AccountUser) {
				user = (AccountUser) userObj;
			}
		} catch (InvalidUserException e) {
			LOGGER.warn("Unable to find the logged in user");
		}
		return user;
	}

	private void getDefaultRole(EventUserInfo eventUserInfo, AccountUser user) {
		Role defRole = user.getDefRole();
		if (defRole == null) {
			defRole = userService.getDefaultRole(user);
		}
		if(defRole != null){
			eventUserInfo.setUserRole(defRole.getLabel());
		}
	}
}
