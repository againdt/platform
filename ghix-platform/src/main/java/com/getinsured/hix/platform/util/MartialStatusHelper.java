package com.getinsured.hix.platform.util;

import java.util.ArrayList;
import java.util.List;

public class MartialStatusHelper {

	/**
	 * Objects are returned of type State
	 */
	public class MartialStatus {
		private String code;
		private String name;

		protected MartialStatus(String code, String name) {
			this.code = code;
			this.name = name;
		}
		
		public MartialStatus(String code){
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj){
				return true;
			}
			if (obj == null){
				return false;
			}
			if (getClass() != obj.getClass()){
				return false;
			}
			MartialStatus other = (MartialStatus) obj;
			if (!getOuterType().equals(other.getOuterType())){
				return false;
			}
			if (code == null) {
				if (other.code != null){
					return false;
				}
			} else if (!code.equals(other.code)){
				return false;
			}
			return true;
		}

		private MartialStatusHelper getOuterType() {
			return MartialStatusHelper.this;
		}
	}

	private List<MartialStatus> martialStatus = null;

	public MartialStatusHelper() {
		martialStatus = new ArrayList<MartialStatus>(51);
		martialStatus.add(new MartialStatus("B", "Registered Domestic Partner"));
		martialStatus.add(new MartialStatus("D", "Divorced"));
		martialStatus.add(new MartialStatus("I", "Single"));
		martialStatus.add(new MartialStatus("M", "Married"));
		martialStatus.add(new MartialStatus("R", "Unreported"));
		martialStatus.add(new MartialStatus("S", "Separated"));
		martialStatus.add(new MartialStatus("U", "Unmarried"));
		martialStatus.add(new MartialStatus("W", "Widowed"));
		martialStatus.add(new MartialStatus("X", "Legally Separated"));
		
	}

	public List<MartialStatus> getAllMartialStatus() {
		return martialStatus;
	}
	
	public String getMartialStatusName(String code){
		MartialStatus st = new MartialStatus(code);
		int ind = this.martialStatus.indexOf(st);
		if( ind >= 0) {
			return this.martialStatus.get(ind).getName();
		}
		return null;
	}
}