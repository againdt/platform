package com.getinsured.hix.platform.security.service;

import java.util.List;
import java.util.Set;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.auditor.GiAuditParameter;

public interface UserRoleService {

	Set<UserRole> setUserRole(String roleName, AccountUser user);

	UserRole saveUserRole(UserRole userrole);

	List<UserRole> getUserListByRoleId(String roleId);
	
	List<String> getAllUsersRoles();

	List<UserRole> findUserRoleByUserId(AccountUser user);
	
	List<String> findUserRoleNames(AccountUser user);

	List<UserRole> getUpdateUseRoleListForRole(String roleId);

	List<String> getAllUsersRolesForUpdate();

	List<String> getAllUsersRolesForAdd();

	List<UserRole> findUserRoleByUserForUpdate(AccountUser user);

	List<UserRole> findUserRoleByUserForAdd(AccountUser user);

	List<String> findUserRoleNamesForAdd(AccountUser user);

	List<String> findUserRoleNamesForUpdate(AccountUser user);

	List<UserRole> findByUserForUpdate(AccountUser user);

	List<UserRole> findByUserForAdd(AccountUser user);

	UserRole addUserRole(UserRole userrole);

	List<UserRole> findByUser(AccountUser user);

	boolean removeUserRoleFromUser(UserRole userRole);

	List<GiAuditParameter> toggleDuoAssociation(AccountUser user);

	void changeDefaultUserRole(AccountUser user, String newDefaultRole, boolean multipleRolesAllowed);
}