package com.getinsured.hix.platform.security.tokens;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.RequestContextUtils;
import com.getinsured.hix.platform.security.tokens.CSRFToken;

/**
 * Taglib that will output generated CSRF token to the JSP page.
 *
 * <p>
 * 	If <code>plainToken (default: false)</code> attribute set to true, it will output plain CSRF token,
 * 	that can be used in JavaScript for example,
 * 	otherwise it will output HTML hidden input type with CSRF token value.
 * </p>
 *
 * @author michael.simons, 2011-09-20
 */
public class CSRFTokenTag extends TagSupport {
	private static final Logger log = LoggerFactory.getLogger(CSRFTokenTag.class);

	public static final String CSRF_PARAMETER_NAME = "csrftoken";
	private static final long serialVersionUID = 745177955805541350L;
	private boolean plainToken = false;

	@Override
	public int doStartTag() throws JspException {
		String token = (String) pageContext.getSession().getAttribute(CSRF_PARAMETER_NAME);

		if(StringUtils.isBlank(token))
		{
			token = CSRFToken.getNewToken();
			pageContext.getSession().setAttribute(CSRF_PARAMETER_NAME, token);
		}

		if(!StringUtils.isBlank(token))
		{
			try
			{
				if(plainToken)
				{
					pageContext.getOut().write(token);
				}
				else
				{
					pageContext.getOut().write(String.format("<input type=\"hidden\" name=\"%1$s\" id=\"%1$s\" value=\"%2$s\" />", CSRF_PARAMETER_NAME, token));
				}
			}
			catch(IOException e)
			{
				if(log.isErrorEnabled())
				{
					log.error("Unable to write CSRF Token from custom taglib", e);
				}
			}
		}

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
 
	public boolean isPlainToken() {
		return plainToken;
	}
 
	public void setPlainToken(boolean plainToken) {
		this.plainToken = plainToken;
	}

	public static String getTokenParameterName() {
		return CSRF_PARAMETER_NAME;
	}
}
