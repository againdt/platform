package com.getinsured.hix.platform.util;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONValue;
import org.apache.log4j.Logger;

/**
 * Created by arielvernaza on 06/09/14.
 */
public class IgnoreCaseStringList {

    private static final Logger logger = Logger.getLogger(IgnoreCaseStringList.class);
    JSONArray convertedList;

    public IgnoreCaseStringList(String listOfInputsValue) {
        convertedList = (JSONArray) JSONValue.parse(listOfInputsValue);
    }

    public boolean contains(Object o) {
        String paramStr = (String) o;
        for (Object s : convertedList) {
            if (paramStr.equalsIgnoreCase((String) s))
                return true;
        }
        return false;
    }
}