package com.getinsured.hix.platform.util;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
public class GhixAssymmetricEncryption {

	@Autowired private GhixPlatformKeystore keyStore;
	
	/**
	 * If you have series of multiple encryption activity, obtain the context first then use this context
	 * for encryption
	 * @param alias
	 * @param password
	 * @return
	 */
	public CipherContext getPrivateKeyEncryptionCipherContext(String alias, char[] password){
		try{
			PrivateKey key = this.keyStore.getPrivateKey(alias, password);
			if(key == null){
				throw new GIRuntimeException("Failed to find the private key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return new CipherContext(cipher, Cipher.ENCRYPT_MODE);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using private key",e);
		}
	}
	
	public CipherContext getPrivateKeyDecryptionCipherContext(String alias, char[] password){
		try{
			PrivateKey key = this.keyStore.getPrivateKey(alias, password);
			if(key == null){
				throw new GIRuntimeException("Failed to find the private key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new CipherContext(cipher, Cipher.DECRYPT_MODE);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using private key",e);
		}
	}
	
	public CipherContext getPublicKeyEncryptionCipherContext(String alias){
		try{
			PublicKey key = this.keyStore.getPublicKey(alias);
			if(key == null){
				throw new GIRuntimeException("Failed to find the public key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return new CipherContext(cipher, Cipher.ENCRYPT_MODE);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using private key",e);
		}
	}
	
	public CipherContext getPublicKeyDecryptionCipherContext(String alias){
		try{
			PublicKey key = this.keyStore.getPublicKey(alias);
			if(key == null){
				throw new GIRuntimeException("Failed to find the public key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new CipherContext(cipher, Cipher.DECRYPT_MODE);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using private key",e);
		}
	}
	
	public String encryptWithProvidedContext(CipherContext context, String data){
		String encryptedAndEncodedStr = null;
		if(context.getCipherMode() != Cipher.ENCRYPT_MODE){
			throw new GIRuntimeException("Invalid cipher mode, needs ENCRYPTION MODE, context not initialized correctly");
		}
		try{
			Cipher cipher = context.getCipher();
			byte[] encryptedDataBytes = cipher.doFinal(data.getBytes());
			encryptedAndEncodedStr = Base64.encodeBase64String(encryptedDataBytes);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using public key",e);
		}
		return encryptedAndEncodedStr;
	}
	
	public String decryptWithProvidedContext(CipherContext context, String data){
		byte[] plainDataBytes = null;
		if(context.getCipherMode() != Cipher.DECRYPT_MODE){
			throw new GIRuntimeException("Invalid cipher mode, needs ENCRYPTION MODE, context not initialized correctly");
		}
		try{
			Cipher cipher = context.getCipher();
			plainDataBytes = cipher.doFinal(Base64.decodeBase64(data.getBytes()));
		}catch(Exception e){
			throw new GIRuntimeException("Failed to decrypt data using public key",e);
		}
		return new String(plainDataBytes);
	}
	
	
	public String encryptWithPublicKey(String alias, String data){
		String encryptedAndEncodedStr = null;
		try{
			PublicKey key = this.keyStore.getPublicKey(alias);
			if(key == null){
				throw new GIRuntimeException("Failed to find the public key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encryptedDataBytes = cipher.doFinal(data.getBytes());
			encryptedAndEncodedStr = Base64.encodeBase64String(encryptedDataBytes);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using public key",e);
		}
		return encryptedAndEncodedStr;
	}
	
	public String decryptWithPublicKey(String alias, String data){
		byte[] plainDataBytes = null;
		try{
			PublicKey key = this.keyStore.getPublicKey(alias);
			if(key == null){
				throw new GIRuntimeException("Failed to find the public key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			plainDataBytes = cipher.doFinal(Base64.decodeBase64(data.getBytes()));
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using public key",e);
		}
		return new String(plainDataBytes);
	}
	
	public String encryptWithPrivateKey(String alias, String data, char[] password){
		String encryptedAndEncodedStr = null;
		try{
			PrivateKey key = this.keyStore.getPrivateKey(alias, password);
			if(key == null){
				throw new GIRuntimeException("Failed to find the public key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encryptedDataBytes = cipher.doFinal(data.getBytes());
			encryptedAndEncodedStr = Base64.encodeBase64String(encryptedDataBytes);
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using private key",e);
		}
		return encryptedAndEncodedStr;
	}
	
	public String decryptWithPrivateKey(String alias, String data, char[] password){
		byte[] plainDataBytes = null;
		try{
			PrivateKey key = this.keyStore.getPrivateKey(alias, password);
			if(key == null){
				throw new GIRuntimeException("Failed to find the public key for alias:"+alias);
			}
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			plainDataBytes = cipher.doFinal(Base64.decodeBase64(data.getBytes()));
		}catch(Exception e){
			throw new GIRuntimeException("Failed to encrypt data using private key",e);
		}
		return new String(plainDataBytes);
	}
}
