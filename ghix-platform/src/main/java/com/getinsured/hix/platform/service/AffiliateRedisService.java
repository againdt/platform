package com.getinsured.hix.platform.service;

import com.getinsured.affiliate.model.Affiliate;

public interface AffiliateRedisService {
	
	Affiliate get(String url);
	void set(String url, Affiliate affiliate);

}
