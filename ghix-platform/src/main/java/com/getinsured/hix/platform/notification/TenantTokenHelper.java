package com.getinsured.hix.platform.notification;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.model.EmailConfigurationDTO;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.affiliate.service.AffiliateConfigService;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.google.gson.Gson;

@Component
public class TenantTokenHelper 
{	
	private static final String ON = "ON";
	private static final String OFF = "OFF";
	private static final String IS_EMAIL_SEND = "isMailSend";
	private static final String LOGO_URL = "logoUrl";
	private static final String REDIRECT_URL = "redirectUrl";
	private static final String APPSERVER_URL = "appserverUrl";
	private static final String FOOTER_ADDRESS = "footerAddress";
	private static final String CUSTOMER_CARE_NUM = "customerCareNum";
	private static final String COMPANY_NAME = "companyName";
	private static final String BASE_URL = "baseUrl";
	private static final String CALL_CENTER_HOURS = "callCenterHours";
	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	public static final String DEFAULT_CALL_CENTER_HOURS = "5 days a week, Monday to Friday 9am - 8:30pm ET";
	private static final List<String> HEADER_FOOTER_TOKENS = Arrays.asList(LOGO_URL, REDIRECT_URL, APPSERVER_URL, FOOTER_ADDRESS, CUSTOMER_CARE_NUM, COMPANY_NAME, BASE_URL, CALL_CENTER_HOURS, DISCLAIMER_CONTENT);
	
	@Autowired private TenantService tenantService;
	@Autowired private ConfigurationService configurationService;
	@Autowired private AffiliateConfigService affiliateConfigService;

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantTokenHelper.class);
	
	public void populateTenantSpecificTokens(Map<String,String> messageTokens) 
	{
		LOGGER.info("<----Inside TenantTokenHelper.populateTenantSpecificTokens method---->");
		Integer flowId = null;
      Long affiliateId = null;
      Long tenantId = null;
      if(messageTokens != null && !messageTokens.isEmpty()) {
			String tempToken = messageTokens.get("flowId");
			if(tempToken != null && NumberUtils.isNumber(tempToken)) {
				flowId = Integer.valueOf(tempToken);
			}
			tempToken = messageTokens.get("affiliateId");
			if(tempToken != null && NumberUtils.isNumber(tempToken)) {
				affiliateId = Long.valueOf(tempToken);
			}
			tempToken = messageTokens.get("tenantId");
			if(tempToken != null && NumberUtils.isNumber(tempToken)) {
				tenantId = Long.valueOf(tempToken);
			}else{
				tenantId = tenantService.getTenant("GINS").getId();
			}
			// Populate email configuration token
			String emailTemplate = messageTokens.get("emailClass");
			String isEmailEnabled = checkEmailEnabled(emailTemplate, affiliateId, tenantId);
			messageTokens.put(IS_EMAIL_SEND, isEmailEnabled);
			if(OFF.equalsIgnoreCase(isEmailEnabled)){
				return;
			}
			
			for (String token : HEADER_FOOTER_TOKENS) {
				String tokenPassed = messageTokens.get(token);
				if(StringUtils.isBlank(tokenPassed)) {
					switch(token) {
					case LOGO_URL : 
						if(flowId != null || affiliateId != null || tenantId != null) {
							String logoUrl = configurationService.logoUrl(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(logoUrl)) {
								logoUrl = StringUtils.EMPTY;
							}
							messageTokens.put(LOGO_URL, logoUrl);
						}
						break;
					case REDIRECT_URL : 
						if(flowId != null || affiliateId != null || tenantId != null) {
							String redirectUrl = configurationService.baseUrl(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(redirectUrl)) {
								redirectUrl = StringUtils.EMPTY;
							} else {
								redirectUrl = redirectUrl + GhixPlatformEndPoints.LOGIN_PAGE;
							}
							messageTokens.put(REDIRECT_URL, redirectUrl);
						}
						break;	
					case BASE_URL : 
						if(flowId != null || affiliateId != null || tenantId != null) {
							String baseUrl = configurationService.baseUrl(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(baseUrl)) {
								baseUrl = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL;
							} else {
								baseUrl = baseUrl + "/";
							}
							messageTokens.put(BASE_URL, baseUrl);
						}
						break;
					case FOOTER_ADDRESS : 
						if(affiliateId != null || tenantId != null) {
							String footerAddress = configurationService.postalAddress(affiliateId, tenantId);
							if(StringUtils.isBlank(footerAddress)) {
								footerAddress = StringUtils.EMPTY;
							}
							messageTokens.put(FOOTER_ADDRESS, footerAddress);
						}
						break;
					case CUSTOMER_CARE_NUM : 
						if(flowId != null || affiliateId != null || tenantId != null) {
							String customerCareNumber = configurationService.customerCareNum(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(customerCareNumber)) {
								customerCareNumber = StringUtils.EMPTY;
							}
							messageTokens.put(CUSTOMER_CARE_NUM, customerCareNumber);
						}
						break;
					case APPSERVER_URL : 
						messageTokens.put(APPSERVER_URL, GhixPlatformEndPoints.APPSERVER_URL);
						break;
					case COMPANY_NAME : 
						if(affiliateId != null || tenantId != null) {
							String companyName = configurationService.getCompanyName(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(companyName)) {
								companyName = StringUtils.EMPTY;
							}
							messageTokens.put(COMPANY_NAME, companyName);
						}
						break;
					case CALL_CENTER_HOURS : 
					      String callCenterHours = null;
					      if(affiliateId != null) {
					    	  Affiliate affiliate = affiliateConfigService.getAffiliate(affiliateId);
					    	  if(affiliate != null && affiliate.getAffiliateConfig() != null && affiliate.getAffiliateConfig().getSupport() != null){
					    		  callCenterHours = affiliate.getAffiliateConfig().getSupport().getCallCenterHours();
					    	  }
					      }
					      if(tenantId != null && StringUtils.isBlank(callCenterHours)) {
						   TenantDTO tenant = tenantService.getTenant(tenantId);
						   if(tenant !=null && tenant.getConfiguration() != null && tenant.getConfiguration().getSupport() != null){
							   callCenterHours = tenant.getConfiguration().getSupport().getCallCenterHours();
						   }
					      }
					      if(StringUtils.isBlank(callCenterHours)){
					       callCenterHours = DEFAULT_CALL_CENTER_HOURS;
					      }
					      messageTokens.put(CALL_CENTER_HOURS, callCenterHours);
					      break; 
					case DISCLAIMER_CONTENT: 
						String disclaimerContent = StringUtils.EMPTY;
						if(flowId != null || affiliateId != null || tenantId != null) {
							disclaimerContent = configurationService.getDisclaimerTextForEmailFooter(flowId, affiliateId, tenantId);
						}
						messageTokens.put(DISCLAIMER_CONTENT, disclaimerContent);
						break; 
					}
				}
			}
		}
	}
	
	public String checkEmailEnabled(String emailTemplate, Long affiliateId, Long tenantId){
		if(StringUtils.isBlank(emailTemplate)){
			return ON;
		}
		// Default email configuration for configurable email's is OFF
		String isEmailEnabled = OFF;
		
		TenantDTO tenantDTO = null;
		Affiliate affiliate = null;
				
		if(tenantId!=null){
			tenantDTO = tenantService.getTenant(tenantId);
		}
		if(affiliateId!=null){
			affiliate = affiliateConfigService.getAffiliate(affiliateId);
		}							
			
		if(tenantDTO!=null 
				&& tenantDTO.getConfiguration()!=null 
				&& tenantDTO.getConfiguration().getEmailConfiguration()!=null){
			String tanentKeyVal = getEmailConfiguration(tenantDTO.getConfiguration().getEmailConfiguration(), emailTemplate);
			if(ON.equals(tanentKeyVal)){
				// If tenant level configuration is ON
				isEmailEnabled = ON;
				if(affiliate!=null 
						&& affiliate.getAffiliateConfig()!=null 
						&& affiliate.getAffiliateConfig().getEmailConfiguration()!=null ){
					String affiliateKeyVal = getEmailConfiguration(affiliate.getAffiliateConfig().getEmailConfiguration(), emailTemplate);
					// Overwrite tenant configuration if affiliate level configuration is not ON
					if(OFF.equals(affiliateKeyVal)){
						isEmailEnabled = OFF;
						return isEmailEnabled;
					}
				}
			}
		}
		return isEmailEnabled;
	}
	
	@SuppressWarnings("unchecked")
	private String getEmailConfiguration(EmailConfigurationDTO emailConfiguration, String emailTemplate) {
		String emailConfigurationVal = OFF;
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		if(emailConfiguration != null && StringUtils.isNotBlank(emailConfiguration.getEmailConfiguration())){
			Map<String, String> emailConfigurationMap = new HashMap<String, String>();
			emailConfigurationMap = gson.fromJson(emailConfiguration.getEmailConfiguration(), emailConfigurationMap.getClass());
			if(null != emailConfigurationMap){
				for(String emailClasskey : emailConfigurationMap.keySet()){
					if(emailTemplate.equalsIgnoreCase(emailClasskey)){
						emailConfigurationVal = emailConfigurationMap.get(emailClasskey);
						if(ON.equals(emailConfigurationVal)){
							emailConfigurationVal = ON;
						}
						break;
					}
				}
			}
		}
		return emailConfigurationVal;
	}
}