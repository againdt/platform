package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Tenant;
@Repository
@Transactional
public interface TenantRepository extends JpaRepository<Tenant, Long> {
	
	Page<Tenant> findAll(Pageable pageable);
	
	Tenant findByCode(@Param("code") String code);
	
	Tenant findByUrl(@Param("url") String url);

	Tenant findById(Long tenantId);

	@Query("FROM Tenant WHERE isActive = :active")
	List<Tenant> findAllByActive(@Param("active") Tenant.IS_ACTIVE active);
}

