package com.getinsured.hix.platform.dto.ecm;

import java.util.Date;
import java.util.Map;




public class MetadataElement {

	private String objectId;
	private String contentId;
	private String contentPath;
	private String author;
	private String description;
	private String url;
	private String title;
	private String snippet;
	private Date lastModified;
	private String mimeType;
	private String language;
	private int contentLength;
	private Map<String,String> customAttributes;
	private Datasource datasource;

	public MetadataElement() {
		super();
	}

	public MetadataElement(String objectId,String contentId, String author, String description,
			String url, String title, String snippet, Date lastModified,
			String mimeType, String language, int contentLength,
			Map<String, String> customAttributes, Datasource datasource) {
		super();
		this.objectId = objectId;
		this.contentId = contentId;
		this.author = author;
		this.description = description;
		this.url = url;
		this.title = title;
		this.snippet = snippet;
		this.lastModified = lastModified;
		this.mimeType = mimeType;
		this.language = language;
		this.contentLength = contentLength;
		this.customAttributes = customAttributes;
		this.datasource = datasource;
	}

	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSnippet() {
		return snippet;
	}
	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public int getContentLength() {
		return contentLength;
	}
	public void setContentLength(int contentLength) {
		this.contentLength = contentLength;
	}
	public Map<String, String> getCustomAttributes() {
		return customAttributes;
	}
	public void setCustomAttributes(Map<String, String> customAttributes) {
		this.customAttributes = customAttributes;
	}
	public Datasource getDatasource() {
		return datasource;
	}
	public void setDatasource(Datasource datasource) {
		this.datasource = datasource;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	public String getContentPath() {
		return contentPath;
	}

	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}

	@Override
	public String toString() {
		return "MetadataElement [objectId=" + objectId + ", contentId="
				+ contentId + ", contentPath=" + contentPath + ", author="
				+ author + ", description=" + description + ", url=" + url
				+ ", title=" + title + ", snippet=" + snippet
				+ ", lastModified=" + lastModified + ", mimeType=" + mimeType
				+ ", language=" + language + ", contentLength=" + contentLength
				+ ", customAttributes=" + customAttributes + ", datasource="
				+ datasource + "]";
	}
	
}
