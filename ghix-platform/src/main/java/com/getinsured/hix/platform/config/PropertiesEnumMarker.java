package com.getinsured.hix.platform.config;

public interface PropertiesEnumMarker {
	public String getValue();
}
