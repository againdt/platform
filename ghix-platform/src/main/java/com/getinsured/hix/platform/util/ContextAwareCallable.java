package com.getinsured.hix.platform.util;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.timeshift.TimeshiftContext;

public class ContextAwareCallable<T> implements Callable<T> {
    private Callable<T> task;
    private RequestAttributes context;
    private Logger logger  = LoggerFactory.getLogger(ContextAwareCallable.class);

    public ContextAwareCallable(Callable<T> task, RequestAttributes context) {
        this.task = task;
        this.context = context;
    }

    @Override
    public T call() throws Exception {
        if (context != null) {
        	if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
        		populateTSContext(context);
        	}
            RequestContextHolder.setRequestAttributes(context);
        }

        try {
        	logger.info("Initiating the processing");
            return task.call();
        } finally {
        	if(logger.isInfoEnabled()) {
        		logger.info("Clearing the thread context".intern());
        	}
        	TimeshiftContext.clearCurrent();
       //     RequestContextHolder.resetRequestAttributes();
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			if(ra != null) {
				ra.removeAttribute(ContextAwarePoolExecutor.X_USER, RequestAttributes.SCOPE_SESSION);
				ra.removeAttribute(ContextAwarePoolExecutor.X_USER_OFFSET, RequestAttributes.SCOPE_SESSION);
			}
			RequestContextHolder.setRequestAttributes(ra);
        }
    }
    
    private void populateTSContext(RequestAttributes ra) {
    	ServletRequestAttributes sra  = (ServletRequestAttributes)ra;
		String user = sra.getRequest().getHeader("X_USER".intern());
		String offset = sra.getRequest().getHeader("X_USER_OFFSET".intern());
		if(user != null && offset != null) {
			logger.info("Initialized the TS Context for user {} with offset {}", user, offset);
			TimeshiftContext.getCurrent().setUserName(user);
			TimeshiftContext.getCurrent().setTimeOffset(offset);
		}
	}
}
