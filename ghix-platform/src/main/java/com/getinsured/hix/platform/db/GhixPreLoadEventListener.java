package com.getinsured.hix.platform.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.hibernate.event.spi.PreLoadEvent;
import org.hibernate.event.spi.PreLoadEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.getinsured.hix.model.AccountUser;

public class GhixPreLoadEventListener implements PreLoadEventListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5954885176449993468L;

	private static final Logger LOGGER = LoggerFactory.getLogger(GhixPreLoadEventListener.class);

	@Override
	public void onPreLoad(PreLoadEvent event) {
		
		try {
			Connection connection = event.getSession().connection();
			try
			{
				final String userId = getLoggedInUser();
				if (userId != null && !userId.equalsIgnoreCase("-1"))
				{
					final String prepSql = "{ call DBMS_SESSION.SET_IDENTIFIER(?) }";
					final CallableStatement cs = connection.prepareCall(prepSql);
					cs.setString(1, userId);
					cs.execute();
					cs.close();
					LOGGER.debug("Set CLIENT_IDENTIFIER [" + userId + "]");
				}
			}
			catch (final SQLException e)
			{
				LOGGER.error("Error while fetching connection for Pre-Select Event", e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while setting client id for Pre-Select Event", e);
		}
	}
	
	protected String getLoggedInUser()
	{
		final SecurityContext ctx = SecurityContextHolder.getContext();

		if(ctx != null)
		{
			final Authentication auth = ctx.getAuthentication();

			if(auth != null)
			{
				final Object principal = auth.getPrincipal();
				LOGGER.debug("GHIXJpaDialect::getLoggedInUser() - UserAccount: " + principal);
				if(principal instanceof AccountUser)
				{
					final AccountUser accountUser = (AccountUser)principal;
					return String.valueOf(accountUser.getUserName());
				}
			}
		}

		return "-1";
	}
}
