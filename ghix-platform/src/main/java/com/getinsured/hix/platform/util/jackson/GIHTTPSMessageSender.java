package com.getinsured.hix.platform.util.jackson;

import java.net.URI;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.CommonsHttpMessageSender;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * GIHTTPSMessageSender used as message sender for {@link WebServiceTemplate} to handle SSL 
 * certificate webservice call with provided certificate.
 * 
 * <p>
 * GIHTTPSMessageSender can be used to extend the default {@link CommonsHttpMessageSender} and  provide custom implementation.
 * </p>
 * 
 * @since 29 Nov 2013
 * @author Biswakalyan
 * 
 *
 */
@SuppressWarnings("deprecation")
public class GIHTTPSMessageSender extends CommonsHttpMessageSender{
	
	
	public GIHTTPSMessageSender( HttpClient httpClient) throws GIRuntimeException{
		 super(httpClient);
	}
	
	@Override
	public boolean supports(URI uri) {
		HostConfiguration hostConfig = getHttpClient().getHostConfiguration();
        if (hostConfig.getProtocol() != null && hostConfig.getHost() != null){
            return true;
        }
        return super.supports(uri);
	}
}
