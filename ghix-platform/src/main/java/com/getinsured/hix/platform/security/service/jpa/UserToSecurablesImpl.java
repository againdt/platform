package com.getinsured.hix.platform.security.service.jpa;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.SecurableTarget;
import com.getinsured.hix.model.Securables;
import com.getinsured.hix.platform.security.repository.ISecurableTargetRepository;
import com.getinsured.hix.platform.security.repository.ISecurablesRepository;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserToSecurables;

@Service("userToSecurables")
public class UserToSecurablesImpl implements UserToSecurables{

	@Autowired
	private IUserRepository iUserRepository;

	@Autowired
	private ISecurablesRepository iSecurablesRepository;

	@Autowired
	ISecurableTargetRepository iSecurablesTargetRepository;

	public void saveUsersInSecurable() {
		SecurableTarget securableTarget;
		List<AccountUser> userListDetails = iUserRepository
				.getListOfUsersToMigrateIn();

		securableTarget = null;

		for (AccountUser user : userListDetails) {
			securableTarget = new SecurableTarget();
			securableTarget.setTargetId(user.getId());
			securableTarget.setTargetName(SecurableTarget.TargetName.USER);
			securableTarget.setCreated(new TSDate());
			securableTarget.setUpdated(new TSDate());
			addUserToSecurable(user, securableTarget);

		}

	}

	private List<Securables> addUserToSecurable(AccountUser user,
			SecurableTarget securableTarget) {
		List<Securables> securableList = null;

		if (securableTarget.getSecurables() == null
				|| securableTarget.getSecurables().size() == 0) {
			securableList = new ArrayList<Securables>();
		} else {
			securableList = securableTarget.getSecurables();
		}

		Securables securables = new Securables();

		// Setting logged in user's id
		if (StringUtils.isNotEmpty(user.getFfmUserId())) {
			securables.setUserName(user.getFfmUserId());
		}

		// Commenting as per comments on HIX-54659 		
//		if (StringUtils.isNotEmpty(user.getUserNPN())) {
//			securables.setPassword(user.getUserNPN());
//		}
		
		securables.setApplicationData("N");
		securables.setSecurableTarget(securableTarget);

		securableList.add(securables);
		securableTarget.setSecurables(securableList);

		iSecurablesTargetRepository.save(securableTarget);
		return securableTarget.getSecurables();
	}

}
