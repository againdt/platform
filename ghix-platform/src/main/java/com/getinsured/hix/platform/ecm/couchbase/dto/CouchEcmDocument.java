package com.getinsured.hix.platform.ecm.couchbase.dto;

import java.util.Map;

import com.getinsured.hix.platform.couchbase.dto.CouchDocument;
import com.google.gson.annotations.Expose;

public class CouchEcmDocument extends CouchDocument {
	@Expose
	private String relativePath;
	@Expose
	private String description;
	@Expose
	private String originalFileName;
	@Expose
	private Map<String, String> customMetaData;

	public CouchEcmDocument() {
		set_class(this.getClass().getName());
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public Map<String, String> getCustomMetaData() {
		return customMetaData;
	}

	public void setCustomMetaData(Map<String, String> customMetaData) {
		this.customMetaData = customMetaData;
	}

	
}
