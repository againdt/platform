package com.getinsured.hix.platform.handler;

import java.util.HashMap;

import javax.servlet.ServletRequest;

public interface IndexedRequestHandler extends HandlerConstants{
	
	/**
	 * Returns a positive (including zero) the index of this handler, This handler will be called to handle the request 
	 * in the order of its index
	 * @return
	 */
	public int getHandlerIndex();
	
	/**
	 * Returns the path handled by this handler
	 * @return
	 */
	public String getHandlerPath();
	/**
	 * This function is usually called by the controller, Implementation can be done
	 * in async or sync mode depending upon the use case
	 * Any inputs required for processing the request can be passed using the context
	 * Handler
	 * 
	 * Handler implementation can chose to populate the context with newly created objects it may want 
	 * to return the caller/controller
	 * Caller/Controller should check the Keys @HANDLER_ERROR, @HANDLER_EXCEPTION and @SHOULD_CONTINUE after the handler
	 * is done with handling the request to take any appropriate action if needed
	 * 
	 * Implementation should be careful to not to allow any exception to be thrown out from
	 * this method and handle every thing internally and set the KEYS @HANDLER_ERROR, @HANDLER_EXCEPTION and @SHOULD_CONTINUE 
	 * appropriately to communicate if there is any error while processing the request
	 *  
	 * @param context
	 * @param request
	 * @return true or false based on how the request is processed, 
	 */
	public boolean handleRequest(HashMap<String, Object> context, ServletRequest request);
}
