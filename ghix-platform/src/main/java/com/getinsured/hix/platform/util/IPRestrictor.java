package com.getinsured.hix.platform.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.net.util.SubnetUtils.SubnetInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;

@Component
public class IPRestrictor {

	private static final Logger LOGGER = LoggerFactory.getLogger(IPRestrictor.class);
	private String allowedIprange = null;
	private HashMap<String, IpAddressMatcher> ipMatchers = new HashMap<>();

	public boolean hasRoleAccess(HttpServletRequest request,ServletRequest servletRequest) {
		String userIpAddr = getIpAddr(request, servletRequest);
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("USR IP ADDR: "+userIpAddr);
		}
    	
		String isRoleAccessEnabled = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ROLE_ACCESS_ENABLED);
		String allowedIpRanges = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ROLE_ACCESS_ALLOWED_IP_RANGES);
    	//Enable CAP Access Check ONLY if the configuration file has the flag turned ON
    	if(StringUtils.isEmpty(isRoleAccessEnabled) || isRoleAccessEnabled.equalsIgnoreCase("N"))
    	{
    		return true;
    	}
       	if(checkIPExistsInRange(allowedIpRanges,userIpAddr)){
	    		return true;
       	}    	
    	return false;
	}
	
	public boolean checkIpAccess(){
		String isRoleAccessEnabled = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ROLE_ACCESS_ENABLED);
		String allowedIpRanges = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ROLE_ACCESS_ALLOWED_IP_RANGES);
		if(StringUtils.isEmpty(isRoleAccessEnabled) || isRoleAccessEnabled.equalsIgnoreCase("N")){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Role access Ip restrictions not enabled, allowing access");
			}
    		return true;
    	}
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra != null) {
			HttpServletRequest req = sra.getRequest();
			String userIpAddr = getIpAddr(req, (ServletRequest) req);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Role access Ip restrictions enabled, validating request for " + userIpAddr);
			}
			if (StringUtils.isEmpty(userIpAddr)) {
				LOGGER.error("No user IP address received with the request, not allowing access");
				return false;
			}
			if (checkIPExistsInRange(allowedIpRanges, userIpAddr)) {
				return true;
			}
		}else{
			LOGGER.error("No request context available, not allowing access Thread ID:"+Thread.currentThread().getId());
		}
		return false;
	}

	private String getIpAddr(HttpServletRequest request, ServletRequest servletRequest) {      
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("X-FORWARDED IP ADDR: " + ipAddress);
		}
		
		if (StringUtils.isEmpty(ipAddress)) {
			ipAddress = servletRequest.getRemoteAddr();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("X-FORWARED EMPTY, SETTING IP FROM REMOTE ADDR: " + ipAddress);
			}
			
		}
		
		return ipAddress;
	}   
	
	private boolean checkIPExistsInRange(String allowedIpRanges,
			String ipToCheck) {
		if((StringUtils.isEmpty(allowedIpRanges) ||  StringUtils.isEmpty(ipToCheck))){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Attempted evaluating:\""+ipToCheck+"\" against allowed Ip Range:\""+allowedIpRanges+"\" Check failed");
			}
			return false;
		}
		if(this.allowedIprange == null || !this.allowedIprange.equals(allowedIpRanges)){
			cleanupCache(allowedIpRanges);
		}
		
		boolean isValidIpAddress = false;
		Iterator<IpAddressMatcher> cursor  = this.ipMatchers.values().iterator();
		while(cursor.hasNext()){
			if(cursor.next().matches(ipToCheck)){
				isValidIpAddress = true;
				break;
			}
		}
		if(!isValidIpAddress){
			LOGGER.info("IP Address match failed for:"+ipToCheck+" Allowed Ranges are "+this.allowedIprange);
		}
		return isValidIpAddress;
	}

	private synchronized void cleanupCache(String allowedIpRanges) {
		if(this.allowedIprange != null && this.allowedIprange.equalsIgnoreCase(allowedIpRanges)){
			return;
		}
		this.ipMatchers.clear();
		for (String rangeOrAddress : allowedIpRanges.split("\\s*,\\s*")) {
			this.ipMatchers.put(rangeOrAddress, new IpAddressMatcher(rangeOrAddress));
		}
		this.allowedIprange = allowedIpRanges;
	}
}