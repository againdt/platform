package com.getinsured.hix.platform.logging.filter;

public class RequesterNameHolder {

	private static final ThreadLocal<RequesterNameHolder> CONTEXT = new ThreadLocal<RequesterNameHolder>();

	private String contextName;

	protected RequesterNameHolder() {
	}

	public static RequesterNameHolder getCurrent() {
		RequesterNameHolder context = CONTEXT.get();
		if (context == null) {
			context = new RequesterNameHolder();
			CONTEXT.set(context);
		}

		return context;
	}

	public static void clearCurrent() {
		CONTEXT.remove();
	}

	public String getContextName() {
		return contextName;
	}

	public void setContextName(String contextName) {
		this.contextName = contextName;
	}

}
