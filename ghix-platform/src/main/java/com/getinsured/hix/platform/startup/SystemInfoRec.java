/**
 * 
 */
package com.getinsured.hix.platform.startup;

import java.io.Serializable;

/**
 * Record structure for System Information records
 * @author Nikhil Talreja
 * @since 20 June, 2013
 */
public class SystemInfoRec implements Serializable
{
	private static final long serialVersionUID = -8766502343215536280L;
	private String group;
	private String attribute;
	private String value;
	private String errorMessage="NA";
	private String threshold="NA";

	public String getGroup() {
		return group;
	}
	public void setGroup(final String group) {
		this.group = group;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(final String attribute) {
		this.attribute = attribute;
	}
	public String getValue() {
		return value;
	}
	public void setValue(final String value) {
		this.value = value;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(final String threshold) {
		this.threshold = threshold;
	}

}
