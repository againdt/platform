/**
 * 
 */
package com.getinsured.hix.platform.comment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Comment;

/**
 * @author panda_p
 * 
 */
@Transactional(readOnly = true)
public interface ICommentRepository extends JpaRepository<Comment, Integer> {

	@Query("SELECT comment FROM Comment comment where comment.commentTarget.id = :commentTargetId")
	List<Comment> findByCommentTargetId(
			@Param("commentTargetId") Integer commentTargetId);

	@Query("SELECT comment.comment FROM Comment comment where comment.id = :commentId")
	String getCommentTextById(@Param("commentId") Integer commentId);

	@Modifying
	@Transactional
	@Query("DELETE FROM Comment comment where comment.id = :commentId")
	void deleteCommentTextById(@Param("commentId") Integer commentId);
	
	@Query("SELECT comment FROM Comment comment where comment.commentTarget.targetId = :commentTargetId order by comment.updated desc")
	List<Comment> findByCommentTargetUserId(
			@Param("commentTargetId") String commentTargetId);

	Comment findById(Integer id);
}
