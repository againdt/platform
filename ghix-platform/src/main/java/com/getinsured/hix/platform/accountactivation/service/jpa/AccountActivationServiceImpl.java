package com.getinsured.hix.platform.accountactivation.service.jpa;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.VimoEncryptor;
import com.getinsured.hix.platform.accountactivation.AccountActivationEmail;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.repository.IAccountActivationRepository;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.sms.SmsResponse;
import com.getinsured.hix.platform.sms.service.SmsService;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;

@Service("accountActivationService")
@Repository
public class AccountActivationServiceImpl implements AccountActivationService, ApplicationContextAware {
	
	protected static final int THIRTY_TWO = 32;

	protected static final int PASSCODE_EXPIRATION_HOURS = 24;
	
	protected static final String PHONECODE_ERRORMSG="label.account.phoneverification.sendingcodeerror";
	
	private ApplicationContext applicationContext;

	/** to store activation flow */
	protected static Map<String, Integer> activationFlow = new HashMap<String, Integer>();

	static{
		//Modified as per HIX-19331 : Darshan Hardas :  To change the order of registration pages.
		activationFlow.put(ACCOUNT_PHONEVERIFICATION_SENDCODE, 1);
		activationFlow.put(ACCOUNT_SIGNUP, 2);
		//activationFlow.put(ACCOUNT_SECURITYQUESTIONS, 3);
	}

	protected static final Logger LOGGER = LoggerFactory.getLogger(AccountActivationServiceImpl.class);

	/** Sms text - This needs to be improved to store in db and read and process the information based on some parameter in a separate class hierarchy. */
	protected static final String ACTIVATION_TEXT = "One Time Activation Code to access your account is <pin>";

	/** handle to created object repository */
	protected IGHIXCustomRepository<?, Integer> ghixCustomRepository;

	//protected AccountActivationEmail activationEmailType;

	@Autowired protected IAccountActivationRepository iAccountActivationRepository;
	
	@Autowired private UserService userService;
	/** to encrypt token for email */
	@Autowired protected VimoEncryptor vimoEncryptor;

	/** to send email */
	@Autowired protected AccountActivationEmail accountActivationEmail;

	/** to send sms. this can be improved to design a separate AccountActivation to send sms based on some parameter */
	@Autowired protected SmsService smsService;

	/** To form object from json and json from object */
	protected final Gson gson = new Gson();
	
	/** to encrypt token to hexadecimal for email*/
	@Autowired protected GhixEncryptorUtil ghixEncryptorUtil; //HIX-30257
	
	@Autowired private MessageSource messageSource;
	
	

	@Override
	public String sendCode(String toPhone, Integer id,String codeType){

		String message = StringUtils.EMPTY;

		//1. generate random code
		/**
		 * Some logic to generate 8-digit activation code to be used by call/sms service
		 *
		 * example:- id = 123 timestamp = 12345678901234 --> code = first char in id + last 6 chars from timestamp + last char in id =  1 + 901234 + 3 = 19012343
		 *
		 */
		String id_str = String.valueOf(id);
		String currentTimeStamp = String.valueOf(TimeShifterUtil.currentTimeMillis());
		String activationCode = StringUtils.substring(id_str, id_str.length()-1) + StringUtils.substring(currentTimeStamp, currentTimeStamp.length()-6, currentTimeStamp.length())
				+	StringUtils.substring(id_str, 0, 1) ;
		LOGGER.info("activationCode - " + activationCode);
		//2. send sms
		SmsResponse smsResponse;
		if(codeType.equals("call")){
			smsResponse = smsService.call(toPhone, ACTIVATION_TEXT, activationCode);
		}
		else{
		smsResponse = smsService.send(toPhone, ACTIVATION_TEXT, activationCode);
		}

		//3. if successfully sent sms, save code in db
		if (!StringUtils.equals(smsResponse.getStatus(), "FAILURE")){
			AccountActivation accountActivation = iAccountActivationRepository.findById(id);
			accountActivation.setActivationCode(activationCode);
			Date codeExpiryDate = TSDateTime.getInstance().plusHours(PASSCODE_EXPIRATION_HOURS).toDate();
			accountActivation.setActivationCodeExpirationDate(codeExpiryDate);
			iAccountActivationRepository.save(accountActivation);
			message = "SUCCESS";
		}else{
			message = smsResponse.getFailureReason();
		}

		return message;
	}

	@Override
	public String sendCode(String toPhone, Integer id,String codeType, HttpServletRequest request) {

		String message = StringUtils.EMPTY;

		//1. generate random code
		/**
		 * Some logic to generate 8-digit activation code to be used by call/sms service
		 *
		 * example:- id = 123 timestamp = 12345678901234 --> code = first char in id + last 6 chars from timestamp + last char in id =  1 + 901234 + 3 = 19012343
		 *
		 */
		String id_str = String.valueOf(id);
		String currentTimeStamp = String.valueOf(TimeShifterUtil.currentTimeMillis());
		String activationCode = StringUtils.substring(id_str, id_str.length()-1) + StringUtils.substring(currentTimeStamp, currentTimeStamp.length()-6, currentTimeStamp.length())
				+	StringUtils.substring(id_str, 0, 1) ;
		// Activation code is set into session for verification
		HttpSession session = request.getSession();
		session.setAttribute("activationCode", activationCode);
		LOGGER.info("activationCode - " + activationCode);
		//2. send sms
		SmsResponse smsResponse;
		if(codeType.equals("call")){
			smsResponse = smsService.call(toPhone, ACTIVATION_TEXT, activationCode);
		}
		else{
			smsResponse = smsService.send(toPhone, ACTIVATION_TEXT, activationCode);
		}

		//3. if successfully sent sms, return status accordingly
		if (!StringUtils.equals(smsResponse.getStatus(), "FAILURE")){
			message = "SUCCESS";
		}else{
			message = smsResponse.getFailureReason();
		}
		return message;
	}

	@Override
	public String verifyCode(String codeToBeVerified, Integer id){
		String error = null;
		AccountActivation accountActivation = iAccountActivationRepository.findById(id);
		String code = accountActivation.getActivationCode();
		if(code == null){
			return "You have tried to use a one-time activation code that has already been used. Please request a new activation code.".intern();
		}
		if (!StringUtils.equals(code, codeToBeVerified)){
			error = messageSource.getMessage(PHONECODE_ERRORMSG, null, LocaleContextHolder.getLocale());
		}else {
			Date currentDate = new TSDate();
			if (currentDate.after(accountActivation.getActivationCodeExpirationDate())){
				error= "You have tried to use a one-time activation code that has expired. Please request a new activation code.".intern();
			}
		}
		accountActivation.setActivationCode(null);
		accountActivation.setActivationCodeExpirationDate(null);
		iAccountActivationRepository.save(accountActivation);
		
		if(error != null) {
			return error;
		}
		return "SUCCESS".intern();

	}

	@Override
	public void updateUserInCreatedObject(AccountUser accountUser, Integer createdId, ActivationJson jsonObject){
		setRepository(jsonObject.getRepositoryName());
		ghixCustomRepository.updateUser(accountUser, createdId);
	}

	@Override
	public void linkUserTo(AccountUser newAccountUser, CreatedObject createdObject) throws GIException{
		
		if(null != createdObject.getCustomeFields()  && createdObject.getCustomeFields().containsKey("linkUserto")){
			
			String linkUserto = createdObject.getCustomeFields().get("linkUserto");
			switch (linkUserto) {
			case UserService.LINK_AFFILIATE_ROLE:
				try {
					int moduleId = Integer.parseInt( createdObject.getCustomeFields().get("affiliateId") );
					userService.createModuleUser(moduleId , UserService.LINK_AFFILIATE_ROLE,  newAccountUser, true);
				} catch (NumberFormatException e) {
					throw new GIException("Wrong or invalid moduleid passed",e);
				}
				
			break;

			case RoleService.EMPLOYER_ROLE :
				try {
					int moduleId = Integer.parseInt( createdObject.getCustomeFields().get("employerId") );
					userService.createModuleUser(moduleId , linkUserto,  newAccountUser, true);
				} catch (NumberFormatException e) {
					throw new GIException("Wrong or invalid moduleid passed",e);
				}
			break;
				
			default:
				break;
			}
			
		}
		
	}
	
	@Override
	public AccountActivation updateLastVisitedUrlAndUsername(String lastVisitedUrl, String username, Integer id){
		AccountActivation accountActivation = iAccountActivationRepository.findById(id);
		accountActivation.setLastVisitedUrl(lastVisitedUrl);
		accountActivation.setUsername(username);

		//Modified for HIX-21680 : Darshan Hardas: Username is updated in signup page submit. Hence, invalidate the account activation link
		if(!StringUtils.isEmpty(username) && accountActivation.getStatus() == STATUS.NOTPROCESSED) {
			accountActivation.setStatus(STATUS.PROCESSED);
		}

		return iAccountActivationRepository.save(accountActivation);
	}

	@Override
	public String getLandingPage(String token){

		AccountActivation accountActivation = iAccountActivationRepository.findByActivationToken(token);
		return getNextLandingUrl(accountActivation);
	}


	@Override
	public String getLandingPage(Integer id){

		AccountActivation accountActivation = iAccountActivationRepository.findById(id);
		return getNextLandingUrl(accountActivation);
	}

	private String getNextLandingUrl(AccountActivation accountActivation) {
		String nextURL = StringUtils.EMPTY;
		if (!StringUtils.isEmpty(accountActivation.getLastVisitedUrl()) && activationFlow.containsKey(accountActivation.getLastVisitedUrl())){
			int index = activationFlow.get(accountActivation.getLastVisitedUrl());
			nextURL = getKeyFromValue(index + 1);
		}
		return nextURL;
	}

	private static String getKeyFromValue(Integer value) {
		for (String o : activationFlow.keySet()) {
			if (activationFlow.get(o).equals(value)) {
				return o;
			}
		}
		return null;
	}

	@Override
	public AccountActivation findByActivationToken(String activationToken){
		return iAccountActivationRepository.findByActivationToken(activationToken);
	}

	@Override
	public AccountActivation findByActivationId(Integer activationId){
		return iAccountActivationRepository.findById(activationId);
	}

	@Override
	public ActivationJson getActivationJson(String jsonbString){
		return gson.fromJson(jsonbString, ActivationJson.class);
	}
	
	@Transactional
	public void processPreviousActivation(CreatedObject createdObject){
		
		if(createdObject != null){
			
			if(createdObject.getObjectId() == 0){ // Ignore this check for admin roles
				return;
			}
			List<AccountActivation> prevActivations = getAccountActivationsByCreatedObjectIdAndType(createdObject.getObjectId(), 
					createdObject.getRoleName() );
		
			if(prevActivations != null){
				for( AccountActivation prevActivation : prevActivations){
					if(prevActivation != null &&  prevActivation.getStatus() == STATUS.NOTPROCESSED){
						prevActivation.setStatus(STATUS.PROCESSED);
						update(prevActivation);
						
					}
				}
			}
		}
	}
	
	@Override
	@Transactional
	public AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject,Integer expirationDays) throws GIException {

		NotificationAgent activationEmailType;

		//1. perform basic incoming data validation
		validateIncomingData(createdObject, creatorObject);

		//2. form ActivationJson object
		ActivationJson jsonObject = createActivationJson(createdObject, creatorObject);

		//3a. get actual repository by invoking
		setRepository(jsonObject.getRepositoryName());

		//3b. check whether created.ObjectId exist in db for respective  objectType
		boolean isExists = ghixCustomRepository.recordExists(createdObject.getObjectId());

		if (!isExists){
			throw new IllegalArgumentException("No record found for createdObjectId - " + createdObject.getObjectId());
		}
		// Set the previous activation record statuses to PROCESSED
		processPreviousActivation(createdObject);
		
		//4. convert it to JSON string
		String jsonString = gson.toJson(jsonObject);

		//5. generate secure random token
		//HIX-30523
		String randomToken = generateRandomToken();
		/*String randomToken1 = createdObject.getRoleName() + createdObject.getObjectId()+ TimeShifterUtil.currentTimeMillis();
		String encryptToken1 = vimoEncryptor.encrypt(randomToken);*/
		
		//6. create new AccountActivation object & populate details & persist record with status as NOT_PROCESSED
		AccountActivation accountActivation = formAccountActivationEntity(jsonString, randomToken, createdObject, creatorObject,expirationDays);//HIX-30523
		accountActivation = iAccountActivationRepository.save(accountActivation);

		Location location = null;
		//7. initiate email sending process
		LOGGER.info("Sending Account Activation e-mail...");
		try {
			if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("emailType")){
				String emailClassName = createdObject.getCustomeFields().get("emailType");
				if(StringUtils.isEmpty(emailClassName)){
					throw new GIException("Valid Email Type is missing");
				}
				LOGGER.info("Email Type:..."+emailClassName);
				activationEmailType = (NotificationAgent) applicationContext.getBean(emailClassName);
				
			}else{
				LOGGER.info("Using default email activation...");
				activationEmailType = accountActivationEmail;
			}
			
			location = this.getLocationObject(createdObject);
			sendAccountActivationMail(activationEmailType, accountActivation,jsonObject, location);
		} catch (Exception e) {
			LOGGER.error("Account Activation e-mail cannot be sent - " + e.getMessage(),e);
			throw new GIException("Account Activation e-mail cannot be sent. Please contact the administrator.", e);
		}
		return accountActivation;
	}
	
	@Override
	@Transactional
	public AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject,Integer expirationDays,Map<GhixUtils.EMAIL_STATS,String> notificationTrackingAttributes) throws GIException {

		NotificationAgent activationEmailType;

		//1. perform basic incoming data validation
		validateIncomingData(createdObject, creatorObject);

		//2. form ActivationJson object
		ActivationJson jsonObject = createActivationJson(createdObject, creatorObject);

		//3a. get actual repository by invoking
		setRepository(jsonObject.getRepositoryName());

		//3b. check whether created.ObjectId exist in db for respective  objectType
		boolean isExists = ghixCustomRepository.recordExists(createdObject.getObjectId());

		if (!isExists){
			throw new IllegalArgumentException("No record found for createdObjectId - " + createdObject.getObjectId());
		}

		//4. convert it to JSON string
		String jsonString = gson.toJson(jsonObject);

		//5. generate secure random token
		//HIX-30523
		String randomToken = generateRandomToken();
		/*String randomToken1 = createdObject.getRoleName() + createdObject.getObjectId()+ TimeShifterUtil.currentTimeMillis();
		String encryptToken1 = vimoEncryptor.encrypt(randomToken);*/

		//6. create new AccountActivation object & populate details & persist record
		AccountActivation accountActivation = formAccountActivationEntity(jsonString, randomToken, createdObject, creatorObject,expirationDays);//HIX-30523
		accountActivation = iAccountActivationRepository.save(accountActivation);

		Location location = null;
		//7. initiate email sending process
		LOGGER.info("Sending Account Activation e-mail...");
		try {
			if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("emailType")){
				String emailClassName = createdObject.getCustomeFields().get("emailType");
				if(StringUtils.isEmpty(emailClassName)){
					throw new GIException("Valid Email Type is missing");
				}
				LOGGER.info("Email Type:..."+emailClassName);
				activationEmailType = (NotificationAgent) applicationContext.getBean(emailClassName);
				
			}else{
				LOGGER.info("Using default email activation...");
				activationEmailType = accountActivationEmail;
			}
			
			location = this.getLocationObject(createdObject);
			sendAccountActivationMail(activationEmailType, accountActivation,jsonObject, location, notificationTrackingAttributes);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("Account Activation e-mail cannot be sent - " + e.getMessage());
			throw new GIException("Account Activation e-mail cannot be sent. Please contact the administrator.", e);
		}
		return accountActivation;
	}
	
	protected String generateRandomToken() {
		return GhixEncryptorUtil.generateUUIDHexString(THIRTY_TWO);
	}

	@Override
	@Transactional
	public AccountActivation resendActivationMail(CreatedObject createdObject, CreatorObject creatorObject, Integer expirationDays,
			AccountActivation accountActivation) throws GIException {
		NotificationAgent activationEmailType = null;
		accountActivation.setCreatedObjectId(createdObject.getObjectId());
		accountActivation.setCreatedObjectType(createdObject.getRoleName());

		accountActivation.setCreatorObjectId(creatorObject.getObjectId());
		accountActivation.setCreatorObjectType(creatorObject.getRoleName());

		Date previousTokenExpiryDate = accountActivation.getActivationTokenExpirationDate();
		//Check if the expiry date exceeds current date
		if((new TSDate()).after(previousTokenExpiryDate)){
			// if yes then set new activation token
			// generate secure random token

			String randomToken = createdObject.getRoleName() + createdObject.getObjectId()+ TimeShifterUtil.currentTimeMillis();
		    String encryptToken = vimoEncryptor.encrypt(randomToken);
			accountActivation.setActivationToken(encryptToken);

		}

	    // To set the newTokenExpiryDate every time the send activaton link is clicked
	    //Date newTokenExpiryDate = TSDateTime.getInstance().plusHours(expirationHours).toDate();
		//accountActivation.setActivationTokenExpirationDate(newTokenExpiryDate);

		int expirationHours=PASSCODE_EXPIRATION_HOURS;
		if(expirationDays==null ){
			expirationHours =24;
			LOGGER.info("Default expiration hours is set to 24 hours as expirationDays is null ");
			}
		else{
			expirationHours= 24 * expirationDays;
		}

		processPreviousActivation(createdObject);
		
		ActivationJson jsonObject = createActivationJson(createdObject, creatorObject);

		//3a. get actual repository by invoking
		setRepository(jsonObject.getRepositoryName());
		String jsonString = gson.toJson(jsonObject);

		String randomToken = generateRandomToken();
		accountActivation.setCreatedObjectId(createdObject.getObjectId());
		accountActivation.setCreatedObjectType(createdObject.getRoleName());

		accountActivation.setCreatorObjectId(creatorObject.getObjectId());
		accountActivation.setCreatorObjectType(creatorObject.getRoleName());

		accountActivation.setActivationToken(randomToken);
		Date tokenExpiryDate = TSDateTime.getInstance().plusHours(expirationHours).toDate();
		accountActivation.setActivationTokenExpirationDate(tokenExpiryDate);
		accountActivation.setJsonString(jsonString);
		accountActivation.setStatus(STATUS.NOTPROCESSED);

		accountActivation = iAccountActivationRepository.save(accountActivation);
		
		Location location = null;
		try {

			LOGGER.info("Sending Account Activation e-mail...");
			if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("emailType")){
				String emailClassName = createdObject.getCustomeFields().get("emailType");
				if(StringUtils.isEmpty(emailClassName)){
					throw new GIException("Valid Email Type is missing");
				}
				activationEmailType = (NotificationAgent) applicationContext.getBean(emailClassName);
			}else{
				activationEmailType = accountActivationEmail;
			}
			location = this.getLocationObject(createdObject);
			sendAccountActivationMail(activationEmailType,accountActivation,jsonObject,location);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("Account Activation e-mail cannot be sent - " + e.getMessage());
			throw new GIException("Account Activation e-mail cannot be sent. Please contact the administrator.", e);
		}
		return accountActivation;
	}

	@Override
	@Deprecated
	@Transactional
	public AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject) throws GIException {
		NotificationAgent activationEmailType = null;
		//1. perform basic incoming data validation
		validateIncomingData(createdObject, creatorObject);

		//2. form ActivationJson object
		ActivationJson jsonObject = createActivationJson(createdObject, creatorObject);

		//3a. get actual repository by invoking
		setRepository(jsonObject.getRepositoryName());

		//3b. check whether created.ObjectId exist in db for respective  objectType
		boolean isExists = ghixCustomRepository.recordExists(createdObject.getObjectId());

		if (!isExists){
			throw new IllegalArgumentException("No record found for createdObjectId - " + createdObject.getObjectId());
		}

		//4. convert it to JSON string
		String jsonString = gson.toJson(jsonObject);

		//5. generate secure random token
		String randomToken = createdObject.getRoleName() + createdObject.getObjectId()+ TimeShifterUtil.currentTimeMillis();
		String encryptToken = vimoEncryptor.encrypt(randomToken);

		//6. create new AccountActivation object & populate details & persist record
		AccountActivation accountActivation = formAccountActivationEntity(jsonString, encryptToken, createdObject, creatorObject,null);
		accountActivation = iAccountActivationRepository.save(accountActivation);


		//7. initiate email sending process
		LOGGER.info("Sending Account Activation e-mail...");
		try {
			if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("emailType")){
				String emailClassName = createdObject.getCustomeFields().get("emailType");
				if(StringUtils.isEmpty(emailClassName)){
					throw new GIException("Valid Email Type is missing");
				}
				activationEmailType = (NotificationAgent) applicationContext.getBean(emailClassName);
			}else{
				activationEmailType = accountActivationEmail;
			}
			Location location = this.getLocationObject(createdObject);
			sendAccountActivationMail(activationEmailType,accountActivation,jsonObject,location);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("Account Activation e-mail cannot be sent - " + e.getMessage());
			throw new GIException("Account Activation e-mail cannot be sent. Please contact the administrator.", e);
		}
		return accountActivation;
	}
	
	private void sendAccountActivationMail(NotificationAgent activationEmailType, AccountActivation accountActivation, ActivationJson jsonObject, Location location) throws NotificationTypeNotFound  {
		//Notice noticeObj = activationEmailType.generateEmail(jsonObject, accountActivation,location);
		
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("ACCOUNT_ACTIVATION", accountActivation);
		notificationContext.put("ACTIVATION_JSON", jsonObject);
		notificationContext.put("LOCATION", location);
		Map<String, String> tokens = activationEmailType.getTokens(notificationContext);
		if(!tokens.containsKey("isMailSend") || ("ON".equalsIgnoreCase(tokens.get("isMailSend")))) {
			Map<String, String> emailData = activationEmailType.getEmailData(notificationContext);
			
			Notice noticeObj = activationEmailType.generateEmail(activationEmailType.getClass().getSimpleName(),location, emailData, tokens);
			if(noticeObj == null){
				throw new NotificationTypeNotFound("Failed to find the notification for location:"+activationEmailType.getClass().getSimpleName());
			}
			
			activationEmailType.sendEmail(noticeObj);
		}		
	}
	
	private void sendAccountActivationMail(NotificationAgent activationEmailType, AccountActivation accountActivation, ActivationJson jsonObject, Location location, Map<GhixUtils.EMAIL_STATS,String>notificationTrackingAttributes) throws NotificationTypeNotFound  {
		//Notice noticeObj = activationEmailType.generateEmail(jsonObject, accountActivation,location);
		
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("ACCOUNT_ACTIVATION", accountActivation);
		notificationContext.put("ACTIVATION_JSON", jsonObject);
		notificationContext.put("LOCATION", location);
		Map<String, String> tokens = activationEmailType.getTokens(notificationContext);
		if(!tokens.containsKey("isMailSend") || ("ON".equalsIgnoreCase(tokens.get("isMailSend")))) {
			Map<String, String> emailData = activationEmailType.getEmailData(notificationContext);
			
			Notice noticeObj = activationEmailType.generateEmail(activationEmailType.getClass().getSimpleName(),location, emailData, tokens);
			if(noticeObj == null){
				throw new NotificationTypeNotFound("Failed to find the notification for location:"+activationEmailType.getClass().getSimpleName());
			}
			activationEmailType.sendEmail(noticeObj,notificationTrackingAttributes);
		}		
	}

	protected AccountActivation formAccountActivationEntity(String jsonString, String encryptToken, CreatedObject createdObject, CreatorObject creatorObject,Integer expirationDays ) {

		int expirationHours=PASSCODE_EXPIRATION_HOURS;
		if(expirationDays==null ){
			expirationHours =24;
			LOGGER.info("Default expiration hours is set to 24 hours as expirationDays is null ");
			}
		else{
			expirationHours= 24 * expirationDays;
		}

		AccountActivation accountActivation = new AccountActivation();
		accountActivation.setCreatedObjectId(createdObject.getObjectId());
		accountActivation.setCreatedObjectType(createdObject.getRoleName());

		accountActivation.setCreatorObjectId(creatorObject.getObjectId());
		accountActivation.setCreatorObjectType(creatorObject.getRoleName());

		accountActivation.setActivationToken(encryptToken);
		Date tokenExpiryDate = TSDateTime.getInstance().plusHours(expirationHours).toDate();
		accountActivation.setActivationTokenExpirationDate(tokenExpiryDate);
		accountActivation.setJsonString(jsonString);
		accountActivation.setStatus(STATUS.NOTPROCESSED);
		accountActivation.setSentDate(new TSDate());
		return accountActivation;
	}

	protected ActivationJson createActivationJson( CreatedObject createdObject, CreatorObject creatorObject) {
		ActivationJson jsonObject = new ActivationJson();
		jsonObject.setCreatorObject(creatorObject);
		jsonObject.setCreatedObject(createdObject);
		return jsonObject;
	}

	protected void validateIncomingData(CreatedObject createdObject, CreatorObject creatorObject) {

		validateCreatedObjectData(createdObject);
		validateCreatorObjectData(creatorObject);
	}

	protected void validateCreatorObjectData(CreatorObject creatorObject) {
		if (creatorObject == null){
			throw new IllegalArgumentException("creatorObject cannot be blank");
		}

		if (StringUtils.isEmpty(creatorObject.getFullName())){
			throw new IllegalArgumentException("creatorObject.FullName cannot be blank");
		}

		if (creatorObject.getObjectId() == null){
			throw new IllegalArgumentException("creatorObject.ObjectId cannot be blank");
		}

		if (StringUtils.isEmpty(creatorObject.getRoleName())){
			throw new IllegalArgumentException("creatorObject.RoleName cannot be blank");
		}
	}

	protected void validateCreatedObjectData(CreatedObject createdObject) {

		if (createdObject == null){
			throw new IllegalArgumentException("createdObject cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getEmailId())){
			throw new IllegalArgumentException("createdObject.EmailId cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getFullName())){
			throw new IllegalArgumentException("createdObject.FullName cannot be blank");
		}

		if (createdObject.getObjectId() == null){
			throw new IllegalArgumentException("createdObject.ObjectId cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getRoleName())){
			throw new IllegalArgumentException("createdObject.RoleName cannot be blank");
		}

		if (createdObject.getPhoneNumbers() == null || createdObject.getPhoneNumbers().size() <= 0){
			throw new IllegalArgumentException("createdObject.PhoneNumbers cannot be blank");
		}

		for (String phoneNumber : createdObject.getPhoneNumbers()) {
			if (StringUtils.isEmpty(phoneNumber)){
				throw new IllegalArgumentException("createdObject.PhoneNumbers cannot be blank or null");
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void setRepository(String repositoryName) {
		ghixCustomRepository = (IGHIXCustomRepository<?, Integer>) applicationContext.getBean(repositoryName);
	}

	@Override
	public List<AccountActivation> getAccountActivationAdminUsers() {

		return iAccountActivationRepository.findAllAdminUsers();
	}

	/* Find the AccountActivation object based on the username (login)
	 * @see com.getinsured.hix.platform.accountactivation.service.AccountActivationService#getAccountActivationByUserName(java.lang.String)
	 */
	@Override
	public AccountActivation getAccountActivationByUserName(String userName){
		return iAccountActivationRepository.findByUsername(userName);
	}
	
	/**
	 * Find only Un processed records
	 */
	@Override
	public AccountActivation getUnProcessedAccountActivationByUserName(String userName){
		List<AccountActivation> activationecords = iAccountActivationRepository.findUnProcessedByUsername(userName);
		if(activationecords == null || activationecords.size() == 0){
			return null;
		}
		if(activationecords.size() > 1){
			throw new GIRuntimeException("Multiple un processed activation records found for user:"+userName);
		}
		return activationecords.get(0);
		
	}

	/*
	* To support phix_main implementation
	*/
	@Override
	public AccountActivation getAccountActivationByCreatedObjectId(int createdObjectId, String createdObjectType){
		return iAccountActivationRepository.findByCreatedObjectId(createdObjectId,createdObjectType);
	}
	
	@Override
	public List<AccountActivation> getAccountActivationsByCreatedObjectIdAndType(int createdObjectId, String createdObjectType){
		return iAccountActivationRepository.findByCreatedObjectTypeAndCreatorObjectId(createdObjectType,createdObjectId);
	}

	@Override
	public AccountActivation getAccountActivationByCreatedObjectId(int createdObjectId, String createdObjectType,int creatorObjectId, String creatorObjectType){
		return iAccountActivationRepository.findByCreatedObjectId(createdObjectId,createdObjectType,creatorObjectId,creatorObjectType);
	}

	@Override
	public AccountActivation getAccountActivationByCreatedObjectIdAndCreatorObjectId(int createdObjectId, String createdObjectType,
			int creatorObjectId,  String creatorObjectType, STATUS status) {
		return iAccountActivationRepository.findByActivationObjCreatedObjectIdAndCreatorObjectId(createdObjectType,createdObjectId,creatorObjectId, creatorObjectType,status);
	}

	@Override
	public int sendActivationReminders(AccountActivation accountActivation) {
		throw new GIRuntimeException("sendActivationReminders not implemented for base class AccountActivationServiceImpl. Use PrintEmailAccountActivationServiceImpl object.");
	}
	@Override
	public AccountActivation update(AccountActivation accountActivation) {
		return iAccountActivationRepository.save(accountActivation);
	}
	
	@Override
	public void expireAccountActivation(STATUS statusTo, STATUS statusFrom,
			String createdObjectType, Integer createdObjectId) {
		iAccountActivationRepository.updateStatusByCreatedObjectTypeAndCreatorObjectId(statusTo, statusFrom, createdObjectType, createdObjectId);
	}
	
	private Location getLocationObject(CreatedObject createdObject) {
		Location location = null;
		if(createdObject.getCustomeFields() != null && createdObject.getCustomeFields().containsKey("location")){
			String locationJson = createdObject.getCustomeFields().get("location");
			location = gson.fromJson(locationJson, Location.class);
		}
		return location;
	}
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
}
