package com.getinsured.hix.platform.dto.smartystreet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Sunil Desu
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SmartyStreetResponse {

	private String delivery_line_1;
	private String delivery_line_2;
	private String last_line;
	private Components components;
	private Metadata metadata;
	private Analysis analysis;

	public String getDelivery_line_1() {
		return delivery_line_1;
	}

	public void setDelivery_line_1(String delivery_line_1) {
		this.delivery_line_1 = delivery_line_1;
	}

	public String getDelivery_line_2() {
		return delivery_line_2;
	}

	public void setDelivery_line_2(String delivery_line_2) {
		this.delivery_line_2 = delivery_line_2;
	}

	public String getLast_line() {
		return last_line;
	}

	public void setLast_line(String last_line) {
		this.last_line = last_line;
	}

	public Components getComponents() {
		return components;
	}

	public void setComponents(Components components) {
		this.components = components;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public Analysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Analysis analysis) {
		this.analysis = analysis;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SmartyStreetResponse [delivery_line_1=");
		builder.append(delivery_line_1);
		builder.append(", delivery_line_2=");
		builder.append(delivery_line_2);
		builder.append(", last_line=");
		builder.append(last_line);
		builder.append(", components=");
		builder.append(components);
		builder.append(", metadata=");
		builder.append(metadata);
		builder.append(", analysis=");
		builder.append(analysis);
		builder.append("]");
		return builder.toString();
	}

}
