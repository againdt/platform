package com.getinsured.hix.platform.util;

import java.io.IOException;
import java.util.Set;

import org.apache.http.HttpClientConnection;
import org.apache.http.config.Registry;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PoolingConnectionManagerProxy extends PoolingHttpClientConnectionManager {
	Logger logger = LoggerFactory.getLogger(PoolingConnectionManagerProxy.class);
	
	
	private void infoTrace() {
		try {
			throw new RuntimeException("info Trace");
		}catch(Exception e) {
			logger.error("Info Trace:",e);
		}
	}
	public PoolingConnectionManagerProxy(Registry<ConnectionSocketFactory> registry) {
		super(registry);
	}
	public void close() {
		logger.error("Possible Illegal close invoked on pool, allowed only if system is shutting down, please check");
		infoTrace();
		PoolStats stats = super.getTotalStats();
		logger.info(stats.toString());
		super.close();
	}
	
	public void connect(HttpClientConnection managedConn, HttpRoute route, int connectTimeout, HttpContext context)
			throws IOException {
		if(logger.isInfoEnabled()) {
			logger.info("Connection Request : {}",super.getStats(route));
			//infoTrace();
		}
		super.connect(managedConn, route, connectTimeout, context);
	}
	
	@Override
    protected void finalize() throws Throwable {
		infoTrace();
		logger.info("Object being finalized, pool is now shutting down");
        PoolStats stats = super.getTotalStats();
    	logger.warn(stats.toString());
        super.finalize();
    }
	
	public void shutdown() {
		logger.error("Shutdown called on pool, Illegal unless system is shutting down");
		printRouteStats();
		infoTrace();
		super.shutdown();
		
	}
	
	private void printRouteStats() {
		Set<HttpRoute> routes = super.getRoutes();
		for(HttpRoute route: routes) {
			PoolStats stats = super.getStats(route);
			if(logger.isInfoEnabled()) {
				logger.info("Route {}, Stats {}",route.toString(),stats.toString());
			}
		}
	}
}
