package com.getinsured.hix.platform.notices;

import com.getinsured.hix.model.Event;

public interface EventService {
	public Event findByEventName(String eventName);
}
