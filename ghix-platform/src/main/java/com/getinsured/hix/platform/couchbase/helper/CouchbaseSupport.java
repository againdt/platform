package com.getinsured.hix.platform.couchbase.helper;

import java.util.List;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.json.JsonObject;
import com.getinsured.hix.platform.couchbase.dto.CouchBinaryDocument;

public interface CouchbaseSupport {
	void setBucket(Bucket bucket);

	Bucket getBucket();

	<T> T findById(String id, Class<? extends T> type);

	<T> String create(T entity, String id);

	<T> String update(T entity, String id);

	void delete(String id);

	String createBinary(CouchBinaryDocument entity, String id);

	String updateBinary(CouchBinaryDocument entity, String id);
	
	byte[] findBinaryByIdNative(String id);

	String createBinaryNative(byte[] data, String id);

	<T> String updateBinaryNative(byte[] data, String id);

	<T> List<T> query(String statement, Class<? extends T> type);
}
