package com.getinsured.hix.platform.enums;

import org.apache.commons.lang3.StringUtils;

public enum OnOffEnum {
	ON, OFF;

	public static OnOffEnum forString(String enumString) {
		OnOffEnum retval;
		if (StringUtils.isBlank(enumString)) {
			retval = null;
		} else {
			switch (enumString) {
			case "ON":
				retval = ON;
				break;
			case "OFF":
				retval = OFF;
				break;
			default:
				retval = OFF;
			}
		}
		return retval;
	}
}
