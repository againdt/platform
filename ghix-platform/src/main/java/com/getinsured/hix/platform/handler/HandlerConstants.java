package com.getinsured.hix.platform.handler;

public interface HandlerConstants {
	public static final String HANDLER_ERROR_CODE_KEY="HANDLER_ERROR_CODE";
	public static final String HANDLER_ERROR="HANDLER_ERROR";
	public static final String HANDLER_EXCEPTION="HANDLER_EXCEPTION";
	public static final String SHOULD_CONTINUE="SHOULD_CONTINUE";
	public static final int ERROR_CODE_ABORT = 1;
	public static final String WSO2_ENABLED_KEY="WSO2_ENABLED";
	public static final String ACCOUNT_USER_KEY="ACCOUNT_USER";
	public static final String ACTING_ACCOUNT_USER_KEY = "ACTING_ACCOUNT_USER";
	public static final String PROVISIONIG_ROLE_KEY = "PROVISIONIG_ROLE";
	public static final String TENANT_DTO_KEY = "TENANT_DTO";
	public static final String PASSWORD_KEY = "PASSWORD";
	public static final String WSO2_ENV_5_0 = "5.0";
	public static final String WSO2_ENV_5_3 = "5.3";
	public static final String HASH_ANSWER_1="HASH_ANSWER_1";
	public static final String HASH_ANSWER_2="HASH_ANSWER_2";
	

}
