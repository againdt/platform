package com.getinsured.hix.platform.security.tokens;

import java.util.Map;
import org.apache.commons.codec.binary.Base64;

public class GiBinaryToken {

	/**
	 * Initializtion vector, a unique new IV for every token
	 */
	private byte[] iv = null;
	/**
	 * Encrypted data containing a map of key value pairs
	 */
	private byte[] data = null;	
	/**
	 * Timestamp associated with this token
	 */
	private long timestamp = -1;
	
	/**
	 * once decrypted, this variable will hold the key value pairs
	 */
	private Map<String, String> parameters;
	
	public GiBinaryToken() {
		
	}
	
	/**
	 * Re-creates a token using a Base64 Encoded token data
	 * @param base64EncodedTocken
	 */
	public GiBinaryToken(String base64EncodedTocken) {
		byte[] in = Base64.decodeBase64(base64EncodedTocken);
		int len = in.length;
		this.iv = new byte[16];
		this.data = new byte[len-16];
		System.arraycopy(in, 0, this.iv, 0, 16);
		System.arraycopy(in, 16, this.data, 0, len-16);
	}
	
	/**
	 * Should be used to get the string representation of this token
	 * @return the URL safe base 64 encoded token
	 */
	public String toString() {
		int ivSize = this.iv.length;
		int dataSize = this.data.length;
		byte[] packaged = new byte[ivSize+dataSize];
		System.arraycopy(this.iv, 0, packaged, 0, ivSize);
		System.arraycopy(this.data, 0, packaged, ivSize, dataSize);
		return Base64.encodeBase64URLSafeString(packaged);
	}

	/**
	 * Sets the Initialization vector for this token
	 * @param iv2
	 */
	public void setIv(byte[] iv2) {
		this.iv = iv2;
	}
	
	/**
	 * Returns the Initialization vector for this token
	 * @return
	 */
	public byte[] getIv() {
		return this.iv;
	}
	
	/**
	 * 
	 * @return the byte[] representing the encrypted data
	 */
	public byte[] getCipherData() {
		
		return this.data;
	}

	/**
	 * Sets the cipher data of this token
	 * @param ciphertext
	 */
	public void setCipherData(byte[] ciphertext) {
		this.data = ciphertext;
	}
	
	/**
	 * Sets the timestamp associated with this token
	 * @param timestamp
	 */
	public void setTimeSTamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public long getTimestamp() {
		return this.timestamp;
	}
	
	/**
	 * Checks if the token is valid against a validity period requested as a function parameter
	 * @param validityInSec
	 * @return
	 */
	public boolean isValid(long validityInSec) {
		if(validityInSec == -1) {
			return true;
		}
		long validDiff = validityInSec*1000;
		long currentTime = System.currentTimeMillis();
		if(currentTime-this.timestamp <= validDiff) {
			return true;
		}
		return false;
	}

	public void setTokenParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
	
	public String getParameter(String name) {
		return this.parameters.get(name);
	}
	
	public Map<String, String> getAllParameters() {
		return this.parameters;
	}

}
