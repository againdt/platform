package com.getinsured.hix.platform.couchbase.dto.gimonitor;

import com.getinsured.hix.platform.couchbase.dto.CouchDocument;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author Sunil Desu, Sharad
 *
 */
public class GIMonitorDocument extends CouchDocument {

	@Expose
	private String component;

	@Expose
	private String errorCode;

	@Expose
	private String exception;

	@Expose
	private String exceptionStackTrace;

	@Expose
	private String url;

	public GIMonitorDocument() {
		set_class(this.getClass().getName());
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getExceptionStackTrace() {
		return exceptionStackTrace;
	}

	public void setExceptionStackTrace(String exceptionStackTrace) {
		this.exceptionStackTrace = exceptionStackTrace;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}