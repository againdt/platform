/**
 *
 */
package com.getinsured.hix.platform.security;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.timeshift.TimeshiftContext;

/**
 * @author tadepalli_v
 *
 */
@Component()
public class GhixRestTemplate
{
  private static final Logger logger = LoggerFactory.getLogger(GhixRestTemplate.class);

  private boolean isTenantContext = false;
  private String tenantUserName = "";
  @Autowired
  private UserService userService;
  @Autowired
  RestTemplate restTemplate;

  public GhixRestTemplate()
  {
    // Default Constructor ; used (mainly in non-tenant usage)
  }

  @Deprecated
  public GhixRestTemplate(boolean isTenantContext, String tenantUserName)
  {

    // Constructor with fields: used (mainly in tenant usage)
    this.isTenantContext = isTenantContext;
    this.tenantUserName = tenantUserName;
  }

  @Deprecated
  public String getTenantUserName()
  {
    return tenantUserName;
  }

  //
//
//	/**
//	 * @param tenantUserName the tenantUserName to set
//	 */
  @Deprecated
  public void setTenantUserName(String tenantUserName)
  {
    this.tenantUserName = tenantUserName;
  }

  //
//
//	/**
//	 * @return the isTenantContext
//	 */
  @Deprecated
  public boolean isTenantContext()
  {
    return isTenantContext;
  }

  //	/**
//	 * @param isTenantContext the isTenantContext to set
//	 */
  @Deprecated
  public void setTenantContext(boolean isTenantContext)
  {
    this.isTenantContext = isTenantContext;
  }

  public <T> ResponseEntity<T> exchangeParameterized(String url, String username, HttpMethod method, ParameterizedTypeReference<T> responseType)
      throws RestClientException
  {
    return exchangeWithParameterizedType(url, username, method, MediaType.APPLICATION_JSON, responseType, null, false, null);
  }

  public <T> T getForObject(String url, ParameterizedTypeReference<T> responseType)
      throws RestClientException
  {
    logger.info("getForObject: {}, expects return type: {}", url, responseType.getType().getTypeName());
    ResponseEntity<T> responseEntity = null;

    try
    {
      responseEntity = exchangeParameterized(url, getUsername(), HttpMethod.GET, responseType);
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate exception", e);
      throw new RestClientException(e.getMessage(), e);
    }

    return responseEntity.getBody();
  }

  public <T> T getForObject(String url, Class<T> responseType) throws RestClientException
  {
    logger.info("Start request for getForObject ...");
    ResponseEntity<T> responseObject = null;
    try
    {
      String userName = getUsername();
      responseObject = exchange(url, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, responseType, null);
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject.getBody();
  }

  // @Override
  public <T> T getForObject(String url, Class<T> responseType, Map<String, ?> urlVariables)
      throws RestClientException
  {
    logger.info("Start request for getForObject ...");
    ResponseEntity<T> responseObject = null;
    try
    {
      String userName = getUsername();
      responseObject = exchange(url, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, responseType,
          urlVariables);
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }

    return responseObject.getBody();
  }

  public <T> T postForObject(String url, Class<T> responseType, Map<String, ?> uriVariables)
      throws RestClientException
  {
    logger.info("Start request for postForObject ...");
    ResponseEntity<T> responseObject = null;
    try
    {
      String userName = getUsername();
      responseObject = exchange(url, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, responseType,
          uriVariables);
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject.getBody();
  }

  private String getUsername() throws InvalidUserException
  {
    AccountUser user = userService.getLoggedInUser();
    if (user != null)
    {
      return user.getUserName();
    }
    return null;
  }

  public <T> T postForObject(String url, Object request, Class<T> responseType) throws RestClientException
  {
    logger.trace("Start request for postForObject ...");
    ResponseEntity<T> responseObject = null;
    try
    {
      String userName = getUsername();
      responseObject = exchange(url, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, responseType,
          request);
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate postForObject: URL: {} Exception Message: {}, [debug log (if enabled) below]", url, e.getMessage());
      logger.debug("Exception details", e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject.getBody();
  }

  public <T> T putForObject(String url, Object request, Class<T> responseType) throws RestClientException
  {
    logger.trace("Start request for postForObject ...");
    ResponseEntity<T> responseObject = null;
    try
    {
      String userName = getUsername();

      responseObject = exchange(url, userName, HttpMethod.PUT, MediaType.APPLICATION_JSON, responseType, request);
    } catch (Exception e)
    {

      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject.getBody();
  }

  public <T> ResponseEntity<T> exchange(String url, String userName, HttpMethod httpMethod, MediaType contentType,
                                        Class<T> responseType, Map<String, ?> inputVariables) throws RestClientException
  {
    ResponseEntity<T> responseObject = null;
    responseObject = exchange(url, userName, httpMethod, contentType, responseType, inputVariables, false, null);
    return responseObject;
  }

  /**
   *
   * @param url
   * @param userName
   * @param httpMethod
   * @param contentType
   * @param responseType
   * @param request
   * @return
   * @throws RestClientException
   */
  public <T> ResponseEntity<T> exchange(String url, String userName, HttpMethod httpMethod, MediaType contentType,
                                        Class<T> responseType, Object request) throws RestClientException
  {
    logger.trace("GHIX SSO :: Start request exchange ...");
    ResponseEntity<T> responseObject = null;
    responseObject = exchange(url, userName, httpMethod, contentType, responseType, request, false, null);
    return responseObject;
  }

  /**
   *
   * @param url
   * @param userName
   * @param httpMethod
   * @param contentType
   * @param responseType
   * @param inputVariables
   * @param isTenantContext
   * @param tenantUserName
   * @return
   * @throws RestClientException
   */
  public <T> ResponseEntity<T> exchange(String url, String userName, HttpMethod httpMethod, MediaType contentType,
                                        Class<T> responseType, Map<String, ?> inputVariables, boolean isTenantContext, String tenantUserName)
      throws RestClientException
  {

    logger.info("GHIX SSO :: Start request exchange ...");
    String principalRequestHeader = "GHIX_SSO_USER";
    ResponseEntity<T> responseObject = null;
    HttpHeaders headers = new HttpHeaders();
    addTSContext(headers, url);
    T response = null;
    try
    {
      if (StringUtils.isNotBlank(userName))
      {
        userName = userName.toLowerCase();
        if (logger.isDebugEnabled())
        {
          logger.debug("Request <header,value>          ::<" + principalRequestHeader + ","
              + getUserName(userName, isTenantContext, tenantUserName) + ">");
          logger.debug("Request contentType             ::" + contentType);
        }
        headers.add(principalRequestHeader, userName);
      }
      headers.setContentType(contentType);
      if (inputVariables != null)
      {
        logger.debug("Request input variable map size ::" + inputVariables.size());
      } else
      {
        inputVariables = new HashMap<>(); // To avoid null pointer exception
      }
      HttpEntity<Map<String, ?>> requestEntity = new HttpEntity<>(inputVariables, headers);
      responseObject = restTemplate.exchange(url, httpMethod, requestEntity, responseType, inputVariables);

      if (responseObject != null)
      {
        response = responseObject.getBody();
        HttpStatus responseStatus = responseObject.getStatusCode();
        if (!responseStatus.is2xxSuccessful())
        {
          logger.error("Call to URL:" + url + " failed with status:" + responseStatus.toString());
          if (response != null)
          {
            logger.error("Server responded with " + response);
          }
          throw new GIRuntimeException(null, null, Severity.HIGH, "HIX",
              responseObject.getStatusCode().value());
        }
      }
    } catch (GIRuntimeException giexception)
    {
      throw giexception;
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject;
  }

  public <T> ResponseEntity<T> exchangeWithParameterizedType(String url, String userName, HttpMethod httpMethod, MediaType contentType,
                                         ParameterizedTypeReference<T> responseType, Map<String, ?> inputVariables, boolean isTenantContext, String tenantUserName)
      throws RestClientException
  {

    logger.info("GHIX SSO :: Start request exchange ...");
    String principalRequestHeader = "GHIX_SSO_USER";
    ResponseEntity<T> responseObject = null;
    HttpHeaders headers = new HttpHeaders();
    addTSContext(headers, url);
    T response = null;
    try
    {
      if (StringUtils.isNotBlank(userName))
      {
        userName = userName.toLowerCase();
        if (logger.isDebugEnabled())
        {
          logger.debug("Request <header,value>          ::<" + principalRequestHeader + ","
              + getUserName(userName, isTenantContext, tenantUserName) + ">");
          logger.debug("Request contentType             ::" + contentType);
        }
        headers.add(principalRequestHeader, userName);
      }
      headers.setContentType(contentType);
      if (inputVariables != null)
      {
        logger.debug("Request input variable map size ::" + inputVariables.size());
      } else
      {
        inputVariables = new HashMap<>(); // To avoid null pointer exception
      }
      HttpEntity<Map<String, ?>> requestEntity = new HttpEntity<>(inputVariables, headers);
      responseObject = restTemplate.exchange(url, httpMethod, requestEntity, responseType, inputVariables);

      if (responseObject != null)
      {
        response = responseObject.getBody();
        HttpStatus responseStatus = responseObject.getStatusCode();
        if (!responseStatus.is2xxSuccessful())
        {
          logger.error("Call to URL:" + url + " failed with status:" + responseStatus.toString());
          if (response != null)
          {
            logger.error("Server responded with " + response);
          }
          throw new GIRuntimeException(null, null, Severity.HIGH, "HIX",
              responseObject.getStatusCode().value());
        }
      }
    } catch (GIRuntimeException giexception)
    {
      throw giexception;
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject;
  }

  /**
   *
   * @param url
   * @param userName
   * @param httpMethod
   * @param contentType
   * @param responseType
   * @param isTenantContext
   * @param tenantUserName
   * @return
   * @throws RestClientException
   */
  public <T> ResponseEntity<T> exchange(String url, String userName, HttpMethod httpMethod, MediaType contentType,
                                        Class<T> responseType, Object request, boolean isTenantContext, String tenantUserName)
      throws RestClientException
  {
    String principalRequestHeader = "GHIX_SSO_USER";
    ResponseEntity<T> responseObject = null;
    HttpHeaders headers = new HttpHeaders();
    addTSContext(headers, url);
    T response = null;
    try
    {
      if (StringUtils.isNotBlank(userName))
      {
        userName = userName.toLowerCase();
        headers.add(principalRequestHeader, userName);
      }
      headers.setContentType(contentType);
      HttpEntity<Object> requestEntity = new HttpEntity<>(request, headers);
      responseObject = restTemplate.exchange(url, httpMethod, requestEntity, responseType);
      if (responseObject != null)
      {
        response = responseObject.getBody();
        HttpStatus responseStatus = responseObject.getStatusCode();
        if (!(responseStatus.is2xxSuccessful()))
        {
          logger.error("Call to URL {} failed with status: {}", url, responseStatus.toString());
          if (response != null)
          {
            logger.error("Server responded with {}", response);
          }
          throw new GIRuntimeException(null, null, Severity.HIGH, "HIX",
              responseObject.getStatusCode().value());
        }
      }
    } catch (GIRuntimeException giexception)
    {
      throw giexception;
    } catch (Exception e)
    {
      logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
      throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
    }
    return responseObject;
  }

  private String getUserName(String userName, boolean isTenantContext, String tenantUserName)
  {
    logger.info("Get User Name for the GhixRestTemplate Request (for TenanatContext ::" + isTenantContext);
    String userNm = "";
    try
    {
      if (!isTenantContext)
      {
        if (userName != null)
        {
          userNm = userName;
        } else
        {
          userNm = getUsername();
        }
      } else
      {
        userNm = tenantUserName;
      }
      if (StringUtils.isBlank(userName) || StringUtils.containsIgnoreCase(userName, "anonymous"))
      {
        logger.warn("User object is not an instance of AccountUser / anonymous user access ::(User :: {})", userName);
      }
    } catch (Exception e)
    {
      String errorMsg = "Exception ::" + e.getMessage();
      logger.warn(errorMsg);
    }
    return userNm;
  }

  private void addTSContext(HttpHeaders headers, String url)
  {
    if (GhixPlatformConstants.TIMESHIFT_ENABLED)
    {
      if (!url.startsWith(GhixPlatformConstants.TIMESHIFT_URL))
      {
        logger.debug("Adding TS Context for URL");
        headers.add("X-USER", TimeshiftContext.getCurrent().getUserName());
        headers.add("X-USER-OFFSET", TimeshiftContext.getCurrent().getTimeOffset());
      }
    }

  }
  
  /**
	 * 
	 * @param url
	 * @param request
	 * @param responseType
	 * @param headers
	 * @return
	 * @throws RestClientException
	 */
	public <T> T postForObject(String url, Object request, Class<T> responseType, HttpHeaders headers) throws RestClientException {
		logger.trace("Start request for postForObject ...");
		ResponseEntity<T> responseObject = null;
		try {
			String userName = getUsername();
			responseObject = exchange(url, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, responseType,
					request, headers);
		} catch (Exception e) {
			logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
			throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
		}
		return responseObject.getBody();
	}
	
	/**
	 * 
	 * @param url
	 * @param userName
	 * @param httpMethod
	 * @param contentType
	 * @param responseType
	 * @param request
	 * @param headers
	 * @return
	 * @throws RestClientException
	 */
	public <T> ResponseEntity<T> exchange(String url, String userName, HttpMethod httpMethod, MediaType contentType,
			Class<T> responseType, Object request, HttpHeaders headers)
			throws RestClientException {
		String principalRequestHeader = "GHIX_SSO_USER";
		ResponseEntity<T> responseObject = null;
		addTSContext(headers, url);
		T response = null;
		try {
			if (StringUtils.isNotBlank(userName)) {
				userName = userName.toLowerCase();
				headers.add(principalRequestHeader, userName);
			}
			headers.setContentType(contentType);
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(request, headers);
			responseObject = restTemplate.exchange(url, httpMethod, requestEntity, responseType);
			if (responseObject != null) {
				response = responseObject.getBody();
				HttpStatus responseStatus = responseObject.getStatusCode();
				if (!(responseStatus.equals(HttpStatus.OK) || responseStatus.equals(HttpStatus.CREATED))) {
					logger.error("Call to URL:" + url + " failed with status:" + responseStatus.toString());
					if (response != null) {
						logger.error("Server responded with " + response);
					}
					throw new GIRuntimeException(null, null, Severity.HIGH, "HIX",
							responseObject.getStatusCode().value());
				}
			}
		} catch (GIRuntimeException giexception) {
			throw giexception;
		} catch (Exception e) {
			logger.error("GhixRestTemplate Exception ::" + e.getMessage(), e);
			throw new RestClientException("GhixRestTemplate Exception ::" + e.getMessage(), e);
		} 
		return responseObject;
	}

}
