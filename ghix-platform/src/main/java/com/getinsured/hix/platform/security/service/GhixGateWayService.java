/**
 * Declares the Ghix GateWay Service operations to uri traffic with ghix applicaitons
 * @author venkata_tadepalli
 * @since 02/01/2014
 */
package com.getinsured.hix.platform.security.service;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;


public interface GhixGateWayService {

	/**
	 * method to encrypt the url.
	 * accepts HttpServletRequest and url
	 * @param request 
	 * @param url
	 * @return conversationId
	 * @throws UnsupportedEncodingException 
	 */
	public String setConversationUrl(HttpServletRequest request,String url) throws UnsupportedEncodingException;

	/**
	 * method to decrypt the url.
	 * accepts HttpServletRequest
	 * @param request
	 * @return url
	 * @throws UnsupportedEncodingException 
	 */
	public String getConversationUrl(HttpServletRequest request) throws UnsupportedEncodingException;
}