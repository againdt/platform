package com.getinsured.hix.platform.couchbase.converters;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component("gsonConverter")
public class GsonConverter implements JsonConverter
{
	private static final Logger log = LoggerFactory.getLogger(GsonConverter.class);
	private static Gson gson = null;

	@PostConstruct
	public void postConstruct()
	{
		if(gson == null)
		{
			gson = new GsonBuilder()
					.registerTypeAdapterFactory(new CouchbaseAdapterFactory())
					.excludeFieldsWithoutExposeAnnotation()
					.create();

			log.info("Registered new Gson converter for Couchbase");
		}
	}

	public <T> T fromJson(String source, Class<T> type) {
		return gson.fromJson(source, type);
	}

	public <T> String toJson(T source) {
		return gson.toJson(source);
	}
}
