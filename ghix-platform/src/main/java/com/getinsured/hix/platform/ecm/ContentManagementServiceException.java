package com.getinsured.hix.platform.ecm;

/**
 * Thrown to indicate that a method has failed while processing ECM request.
 *
 */
public class ContentManagementServiceException extends Exception {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1053471180051791761L;
	
	private String errorCode;
	
	public String getErrorCode(){
		return this.errorCode;
	}
	
	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}

	public ContentManagementServiceException() {
		super();
	}

	public ContentManagementServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContentManagementServiceException(String message) {
		super(message);
	}
	
	public ContentManagementServiceException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public ContentManagementServiceException(Throwable cause) {
		super(cause);
	}



}
