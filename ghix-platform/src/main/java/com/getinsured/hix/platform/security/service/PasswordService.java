/**
 * HIX-32110 : Modify Change/Reset Password Process to accept salted password
 * Declares the CRUD operations to manage User Password
 * @author venkata_tadepalli
 * @since 03/26/2014
 */
package com.getinsured.hix.platform.security.service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.exception.GIException;

public interface PasswordService {
	//READ Operations
	//READ- Attribute level operations

	//Update Operations

	//Delete Operations

	//Service Operations
	/**
	 * Updates new password for the given User
	 * @param newPassword
	 * @param userId
	 */
	AccountUser updatePassword(AccountUser user, String newPassword,boolean autoSave) throws GIException; //HIX-32110
	/**
	 * If there are multiple roles, lowest will be considered
	 * @param user
	 * @return
	 */
	int getPasswordExpiryPeriod(AccountUser user);
	
}