package com.getinsured.hix.platform.accountactivation;

import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Place holder to store info about Creator object.
 * 
 * @author Ekram Ali Kazi
 *
 */
public class CreatorObject {

	/** PK of the creator object. This is useful to perform db operation */
	private Integer objectId;

	/** To draw JSP */
	private String fullName;

	/** To draw JSP */
	private String roleName;

	/** For Future use, if we need more parameters then simply add those as key-value pair */
	private Map<String, String> customeFields;

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Map<String, String> getCustomeFields() {
		return customeFields;
	}
	public void setCustomeFields(Map<String, String> customeFields) {
		this.customeFields = customeFields;
	}
	public Integer getObjectId() {
		return objectId;
	}
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	@Override
	public String toString() {
				return "CreatorObject [objectId=" + objectId + ", fullName=" + fullName
												+ ", roleName=" + roleName + ", customeFields=" + customeFields
												+ "]";
	}


}
