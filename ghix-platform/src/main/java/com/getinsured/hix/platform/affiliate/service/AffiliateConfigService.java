package com.getinsured.hix.platform.affiliate.service;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.model.ConfigurationDTO;

public interface AffiliateConfigService {
	
	ConfigurationDTO getAffiliateConfig(Long affiliateId);
	Affiliate getAffiliate(Long affiliateId);
	boolean isProductForCrossSell(ConfigurationDTO configuration, String productCode);
}
