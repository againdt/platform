package com.getinsured.hix.platform.multitenant.resolver.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.enums.AffiliateAncillary;
import com.getinsured.affiliate.enums.AffiliateAncillaryStatus;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.model.ProductConfigurationDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.affiliate.service.AffiliateFlowConfigService;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

/**
 * Provides utility methods related to the tenant.
 */
@Component
public class TenantUtil
{
  private static final Logger log = LoggerFactory.getLogger(TenantUtil.class);

  public  static final String COOKIE_AFFILIATE_AND_FLOW_ID = "AF";

  public static final String TENANT_KEY_HEADER = "TENANT_KEY";
  public static final String TENANT_CODE_ATTR = "TENANT_CODE";
  public static final String TENANT_ATTR_NAME = "TENANT";

  public static final String AFFILIATE_ID_HEADER = "AFFILIATE_ID";
  public static final String AFFILIATE_CONFIG = "affiliateConfig";
  public static final String ANCILLARY_CONFIG_CODE = "ancillaryConfig";
  public static final String AFFILIATE_ID = "ClickTrackAffiliateId";

  public static final String FLOW_ID = "ClickTrackAffiliateFlowId";
  public static final String FLOW_ID_PARAM = "flowId";

  private static final String STM_CODE = "STM";
  private static final String HEALTH_CODE = "HLT";
  private static final String DENTAL_CODE = "DEN";
  private static final String ACCIDENT_CODE = "AME";
  private static final String VISION_CODE = "VSN";
  private static final String MEDICARE_CODE = "MDR";

  @Autowired
  private TenantService tenantService;

  @Autowired
  private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

  @Autowired
  private AffiliateFlowConfigService affiliateFlowConfigService;

  public static void setAncillaryConfigInSession(
      final HttpServletRequest httpRequest,
      ConfigurationDTO tenantConfiguration,
      ConfigurationDTO affiliateConfiguration)
  {

    Map<Enum<AffiliateAncillary>, Enum<AffiliateAncillaryStatus>> ancillaryConfig = new HashMap<>();
    String medicareConfig = AffiliateAncillaryStatus.OFF.getDescription();
    ancillaryConfig.put(AffiliateAncillary.HEALTH, AffiliateAncillaryStatus.OFF);
    ancillaryConfig.put(AffiliateAncillary.STM, AffiliateAncillaryStatus.OFF);
    ancillaryConfig.put(AffiliateAncillary.DENTAL, AffiliateAncillaryStatus.OFF);
    ancillaryConfig.put(AffiliateAncillary.VISION, AffiliateAncillaryStatus.OFF);
    ancillaryConfig.put(AffiliateAncillary.AME, AffiliateAncillaryStatus.OFF);
    Set<String> selectedProductSet = new HashSet<>();
    if (null != tenantConfiguration &&
        null != tenantConfiguration.getProductConfigurations() &&
        !tenantConfiguration.getProductConfigurations().isEmpty())
    {
      for (ProductConfigurationDTO product : tenantConfiguration.getProductConfigurations())
      {
        if (BooleanUtils.isTrue(product.getSelected()))
        {
          selectedProductSet.add(product.getCode());
        }
      }
    }

    if (affiliateConfiguration != null)
    {
      List<ProductConfigurationDTO> productConfigurations = affiliateConfiguration.getProductConfigurations();
      if (productConfigurations != null && !productConfigurations.isEmpty())
      {
        for (ProductConfigurationDTO product : productConfigurations)
        {
          if (BooleanUtils.isFalse(product.getSelected()) && selectedProductSet.contains(product.getCode()))
          {
            selectedProductSet.remove(product.getCode());
          }
        }
      }
    }

    if(log.isDebugEnabled())
    {
      log.debug("Tenant has the following products enabled: {}", selectedProductSet);
    }

    if (selectedProductSet.contains(HEALTH_CODE))
    {
      ancillaryConfig.put(AffiliateAncillary.HEALTH, AffiliateAncillaryStatus.ON);
    }
    if (selectedProductSet.contains(STM_CODE))
    {
      ancillaryConfig.put(AffiliateAncillary.STM, AffiliateAncillaryStatus.ON);
    }
    if (selectedProductSet.contains(MEDICARE_CODE))
    {
      medicareConfig = AffiliateAncillaryStatus.ON.getDescription();
    }
    if (selectedProductSet.contains(DENTAL_CODE))
    {
      ancillaryConfig.put(AffiliateAncillary.DENTAL, AffiliateAncillaryStatus.ON);
    }
    if (selectedProductSet.contains(VISION_CODE))
    {
      ancillaryConfig.put(AffiliateAncillary.VISION, AffiliateAncillaryStatus.ON);
    }
    if (selectedProductSet.contains(ACCIDENT_CODE))
    {
      ancillaryConfig.put(AffiliateAncillary.AME, AffiliateAncillaryStatus.ON);
    }

    httpRequest.getSession().setAttribute("ancillaryConfig", ancillaryConfig);
    httpRequest.getSession().setAttribute("medicareConfig", medicareConfig);
  }

  /**
   * If you have affiliate id and flow id, pass it here to store and initialize a {@link com.getinsured.hix.model.Tenant}
   * object in {@link ThreadLocal} and create {@code AF} cookie with that information for the
   * future.
   *
   * @param httpRequest HTTP Request.
   * @param httpResponse HTTP Response.
   * @param affiliateId id of the {@link Affiliate}
   * @param flowId id of the {@link AffiliateFlow}
   * @return true if information was stored in {@link ThreadLocal} and in the cookie.
   */
  public boolean initTenantByAffiliateIdAndFlowId(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                  Long affiliateId, Integer flowId)
  {
    boolean storedTenant = false;
    boolean storedCookie = false;

    if ((affiliateId != null && affiliateId > 0) || (flowId != null && flowId > 0))
    {
      storedTenant = updateTenantInThreadLocal(httpRequest, null, affiliateId, flowId);

      if (storedTenant)
      {
        storedCookie = storeAffiliateIdFlowIdInCookie(httpRequest, httpResponse, affiliateId, flowId);
      }
    }

    return storedTenant && storedCookie;
  }

  public boolean setTenantFromCookies(HttpServletRequest request)
  {
    final Cookie[] cookies = request.getCookies();

    String affiliateIdFlowIdStr = null;
    Long affiliateId = null;
    Integer flowId = null;

    if (cookies != null && cookies.length > 0)
    {
      for (Cookie c : cookies)
      {
        if (COOKIE_AFFILIATE_AND_FLOW_ID.equals(c.getName()))
        {
          affiliateIdFlowIdStr = c.getValue();
          break;
        }
      }
    }

    if (affiliateIdFlowIdStr != null)
    {
      String decoded = unhashAffiliateFlowIds(affiliateIdFlowIdStr);
      String[] parts = null;

      if (decoded != null)
      {
        parts = decoded.split(",");
      }

      if (parts != null && parts.length == 2)
      {
        // If cookie has different affiliate/flow id in comparison to parameters,
        // perhaps they are coming through new affiliate/flow id, so don't use cookies
        // to set tenant/affiliate/flow in thread local.
        String affiliateIdStr = request.getParameter("affiliateId");
        String flowIdStr = request.getParameter("flowId");

        if (affiliateIdStr != null && !parts[0].equals(affiliateIdStr))
        {
          return false;
        }

        if (flowIdStr != null && !parts[1].equals(flowIdStr))
        {
          return false;
        }

        try
        {
          affiliateId = Long.parseLong(parts[0]);
        } catch (NumberFormatException nfe)
        {
          if (log.isWarnEnabled())
          {
            log.warn("Unable to get Long value for affiliateId: '{}'", parts[0]);
          }
        }

        try
        {
          flowId = Integer.parseInt(parts[1]);
        } catch (NumberFormatException nfe)
        {
          if (log.isWarnEnabled())
          {
            log.warn("Unable to get Integer value for flowId: '{}'", parts[1]);
          }
        }
      }

      return updateTenantInThreadLocal(request, null, affiliateId, flowId);
    }

    return false;
  }

  private boolean updateTenantInThreadLocal(final HttpServletRequest request, final Long tenantId, final Long affiliateId, final Integer flowId)
  {
    TenantDTO tenantDTO = null;
    Affiliate affiliate = null;
    AffiliateFlow affiliateFlow = null;

    if (tenantId != null)
    {
      tenantDTO = tenantService.getTenant(tenantId);
    }

    if (affiliateId != null)
    {
      affiliate = tenantService.getAffiliateById(affiliateId);

      // Override Tenant if not found, based on the affiliate
      if (affiliate != null && tenantDTO == null)
      {
        tenantDTO = tenantService.getTenant(affiliate.getTenantId());
      }
    }

    if (flowId != null && flowId != 0)
    {
      // TODO: Check if affiliateFlow belongs to affiliate/tenant?
      affiliateFlow = tenantService.getAffiliateFlowById(flowId);
    } else if (affiliate != null)
    {
      affiliateFlow = tenantService.getDefaultFlowForAffiliate(affiliate.getAffiliateId());
    }

    // TODO: Should flowId override affiliateId?
    if (tenantDTO == null && affiliateFlow != null)
    {
      tenantDTO = tenantService.getTenant(affiliateFlow.getTenantId());
    }
    // If we just got flowId and no affiliateId, find it and set.
    if (affiliate == null && affiliateFlow != null)
    {
      affiliate = affiliateFlow.getAffiliate();
    }

    // If we unable to get tenant based on the:
    // 1. passed tenant id
    // 2. passed affiliate id
    // 3. passed flow id
    // return false.
    if (tenantDTO == null)
    {
      return false;
    }

    return setTenantInThreadLocal(request, tenantDTO, affiliate, affiliateFlow);
  }

  private boolean setTenantInThreadLocal(final HttpServletRequest request, final TenantDTO tenant, final Affiliate affiliate, final AffiliateFlow affiliateFlow)
  {
    boolean ret = false;

    // TODO: Do we care if tenant/affiliate/flow is active?
    if (tenant != null)
    {
      TenantContextHolder.clear();
      TenantContextHolder.setTenant(tenant);
      TenantContextHolder.setRequesturl(request.getRequestURL().toString());
      request.getSession().setAttribute(TENANT_ATTR_NAME, tenant);
      request.getSession().setAttribute(TENANT_KEY_HEADER,tenant.getUrl());
      request.getSession().setAttribute(TENANT_CODE_ATTR,tenant.getCode());
      setAncillaryConfigIfNeeded(request, tenant);

      ret = true;
    }

    if (affiliate != null)
    {
      TenantContextHolder.setAffiliateId(affiliate.getAffiliateId());
      request.getSession().setAttribute(AFFILIATE_ID, affiliate.getAffiliateId());

      affiliateFlowConfigService.setAffiliateConfigurationInSession(request, affiliate.getAffiliateId(), affiliate.getHasEmployer(),
          affiliate.getAffiliateConfig());
      ret = true;
    }

    if (affiliateFlow != null)
    {
      TenantContextHolder.setFlowId(affiliateFlow.getAffiliateflowId());
      request.getSession().setAttribute(FLOW_ID, affiliateFlow.getAffiliateflowId());
      affiliateFlowConfigService.setAffiliateConfigurationInSession(request, affiliateFlow.getAffiliate().getAffiliateId(),
          affiliateFlow.getAffiliate().getHasEmployer(), affiliateFlow.getAffiliate().getAffiliateConfig());
      affiliateFlowConfigService.setFlowConfigurationInSession(request, affiliateFlow.getAffiliate().getAffiliateId(),
          affiliateFlow, null, null, affiliateFlow.getLogoURL(), null, affiliateFlow.getAffiliate().getHasEmployer());

      ret = true;
    }

    if (affiliate != null)
    {
      // Set default flow configuration in session if flowId not passed, based on affiliate's default flow setting
      affiliateFlowConfigService.setFlowIdIfNotSetAlready(request, affiliate.getAffiliateId());
    }

    return ret;
  }

  private boolean storeAffiliateIdFlowIdInCookie(HttpServletRequest request, HttpServletResponse response, Long affiliateId, Integer affiliateFlowId)
  {
    if (affiliateId == null)
    {
      affiliateId = TenantContextHolder.getAffiliateId();
    }

    if (affiliateId != null)
    {
      if (affiliateFlowId == null)
      {
        affiliateFlowId = TenantContextHolder.getFlowId();

        if (affiliateFlowId == null)
        {
          AffiliateFlow affiliateFlow = tenantService.getDefaultFlowForAffiliate(affiliateId);

          if (affiliateFlow != null)
          {
            affiliateFlowId = affiliateFlow.getAffiliateflowId();
          } else
          {
            affiliateFlowId = 0;
          }
        }
      }

      String hashed = hashAffiliateFlowIds(affiliateId, affiliateFlowId);

      final Cookie affiliateIdFlowIdCookie = new Cookie(COOKIE_AFFILIATE_AND_FLOW_ID, hashed);

      affiliateIdFlowIdCookie.setPath("/");
      affiliateIdFlowIdCookie.setMaxAge(315_360_000); // 10 years
      affiliateIdFlowIdCookie.setSecure(request.isSecure());
      affiliateIdFlowIdCookie.setHttpOnly(true);

      response.addCookie(affiliateIdFlowIdCookie);

      if (log.isInfoEnabled())
      {
        log.info("Added cookie: {} => {}; affiliateId: {}, flowId: {}", COOKIE_AFFILIATE_AND_FLOW_ID, hashed, affiliateId, affiliateFlowId);
      }

      return true;
    }

    return false;
  }

  public void setAncillaryConfigIfNeeded(final HttpServletRequest httpRequest, final TenantDTO tenant)
  {
    if (httpRequest.getSession().getAttribute(ANCILLARY_CONFIG_CODE) == null
        && httpRequest.getParameter(FLOW_ID_PARAM) == null)
    {
      ConfigurationDTO tenantConfiguration = tenant.getConfiguration();
      ConfigurationDTO affiliateConfiguration = null;

      if (httpRequest.getSession().getAttribute(AFFILIATE_CONFIG) != null)
      {
        affiliateConfiguration = (ConfigurationDTO) httpRequest.getSession().getAttribute(AFFILIATE_CONFIG);
      }

      affiliateFlowConfigService.setAncillaryConfigInSession(httpRequest, tenantConfiguration, affiliateConfiguration);
    }
  }

  public void setAffiliateIdFromHeaderIfPresent(final HttpServletRequest httpRequest)
  {
    if (httpRequest.getHeader(AFFILIATE_ID_HEADER) != null)
    {
      final String affiliateIdHeaderValue = httpRequest.getHeader(AFFILIATE_ID_HEADER);

      if (affiliateIdHeaderValue != null && !affiliateIdHeaderValue.trim().equals(""))
      {
        final Long affiliateId = Long.valueOf(affiliateIdHeaderValue);
        TenantContextHolder.setAffiliateId(affiliateId);
        httpRequest.getSession().setAttribute(AFFILIATE_ID, affiliateId);
        affiliateFlowConfigService.setFlowIdIfNotSetAlready(httpRequest, affiliateId);
      }
    }
  }

  /**
   * Returns hostname portion of the given URL.
   *
   * @param urlString
   * @return
   */
  public static String getHostnameFromUrl(String urlString)
  {
    String hostName = null;

    if(urlString == null)
    {
      return hostName;
    }

    urlString = urlString.trim();

    try
    {
      URL url = new URL(urlString);
      hostName = url.getHost();
    }
    catch (MalformedURLException e)
    {
      if (log.isErrorEnabled())
      {
        log.error("Could not get hostname from the URL: {}", urlString);
        log.error("Exception while getting hostName from requestURL", e);
      }
    }

    return hostName;
  }

  private String hashAffiliateFlowIds(Long affiliateId, Integer affiliateFlowId)
  {
    return ghixJasyptEncrytorUtil.encryptStringByJasypt(affiliateId + "," + affiliateFlowId);
  }

  private String unhashAffiliateFlowIds(String hash)
  {
    return ghixJasyptEncrytorUtil.decryptStringByJasypt(hash);
  }

  public void setGhixJasyptEncrytorUtil(GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil)
  {
    this.ghixJasyptEncrytorUtil = ghixJasyptEncrytorUtil;
  }

  public void setAffiliateFlowConfigService(AffiliateFlowConfigService affiliateFlowConfigService)
  {
    this.affiliateFlowConfigService = affiliateFlowConfigService;
  }

  public void setTenantService(TenantService tenantService)
  {
    this.tenantService = tenantService;
  }
}
