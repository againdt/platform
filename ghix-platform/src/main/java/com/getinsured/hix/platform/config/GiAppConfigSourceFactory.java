package com.getinsured.hix.platform.config;

import java.util.Map;

import com.getinsured.hix.platform.service.GIAppConfigService;
/**
 * This class is to stick with the existing convention of using DynamicPropertiesUtil methods statically.
 * To do this, we  
 *
 */
public class GiAppConfigSourceFactory {
	
	private static GIAppConfigService giAppConfigService;

	public static void init(GIAppConfigService giAppConfigService) {
		GiAppConfigSourceFactory.giAppConfigService = giAppConfigService;
	}

	public static Object getPropertyValue(String propertyName) {
		return giAppConfigService.getPropertyValue(propertyName);
	}

	public static Map<String, String> getPropertyValues(String value) {
		return giAppConfigService.getPropertyValues(value);
	}
	
	public static Object getPropertyValue(String propertyName, Long tenantId) {
		return giAppConfigService.getPropertyValue(propertyName, tenantId);
	}
	
	public static Map<String, String> getPropertyValues(String value, Long tenantId) {
		return giAppConfigService.getPropertyValues(value, tenantId);
	}

}
