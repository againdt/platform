package com.getinsured.hix.platform.service;

import java.net.URL;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.TenantDTO;

public interface TenantService {

	Page<Tenant> getAllTenant(Pageable pageable);
	List<Tenant> getAllTenant();
	TenantDTO getTenant(String code);
	TenantDTO getTenantForUrl(String url);
	Affiliate getAffiliateForUrl(String url);
	Affiliate getAffiliateById(Long affiliateId);
	TenantDTO getTenant(Long id);
	Tenant saveTenant(String code, TenantDTO tenantDTO);
	void addTenant(TenantDTO tenantDTO, int noOfExpirationDays, String activationUrl);
	String addTenantConfigurations(Tenant tenant);
	String addTenantPermissions(Tenant tenant);
	String addTenantRoles(Tenant tenant);
	AffiliateFlow getDefaultFlowForAffiliate(Long affiliateId);
	AffiliateFlow getAffiliateFlowById(Integer affiliateFlowId);
	URL upload(String tenantName, String propertyKey, String fileName, byte[] dataBytes);
	AffiliateFlow getAffiliateFlowForUrl(String hostName);
}
