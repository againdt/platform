package com.getinsured.hix.platform.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Component
public class GICachingService<T extends CacheableObject> {
	public static final boolean IS_REDIS_ENABLED = "on".equalsIgnoreCase(System.getProperty("redis.enabled"));
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GICachingService.class);
	
	@Autowired(required = false)
	@Qualifier("redisTemplate")
	private RedisTemplate<String, T> redisTemplate;
	
	@Autowired (required = false)
	@Qualifier("redisTemplate")
	private RedisTemplate<String, Collection<T>> redisTemplateForList;
	
	public void set(String objectKey, String hashKey, T obj){
		if(obj == null){
			return;
		}

		try
		{
			if(IS_REDIS_ENABLED)
			{
			if(!(obj instanceof Serializable)){
				throw new RuntimeException("Invalid object used for caching:"+obj.getClass().getSimpleName());
			}

				redisTemplate.opsForHash().put(obj.getObjectKey(), obj.getKey(), obj);
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Failed to put data in the cache", e);
		}
	}
	
	
	public void setAll(String objectKey, String hashKey, Collection<T> values) {
		try {
			if(values == null || values.isEmpty()){
				return;
			}

			if (IS_REDIS_ENABLED)
			{
			Iterator<T> itr = values.iterator();

			if(itr.hasNext()){
			Object obj = values.iterator().next();
			if(!(obj instanceof Serializable)){
				throw new RuntimeException("Invalid object used for caching:"+obj.getClass().getSimpleName());
			}
			}

				redisTemplateForList.opsForHash().put(objectKey, hashKey, values);
			}
		} catch (Exception e) {
			LOGGER.error("Failed to put data in the cache", e);
		}
	}

	@SuppressWarnings("unchecked")
	public T get(String objectKey, String hashKey,CachedObjectDataProvider<T> cachedDataProvider) {
		T cachedObject = null;
		try {
			if (IS_REDIS_ENABLED) {
				cachedObject = (T) redisTemplate.opsForHash().get(objectKey, hashKey);
			}
			if (cachedDataProvider != null && cachedObject == null) {
				List<T> cachedObjects = cachedDataProvider.fetchFromSource(objectKey, hashKey);
				if(cachedObjects !=null && !cachedObjects.isEmpty()){
					cachedObject = cachedObjects.get(0);
				}
				set(objectKey, hashKey, cachedObject);
			}
		} catch (Exception e) {
			LOGGER.error("Failed to fetch data from the cache", e);
		}
		return cachedObject;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<T> getAll(String objectKey, String hashKey, CachedObjectDataProvider<T> cachedDataProvider) {
		Collection<T> cachedObjects = null;
		try {
			if (IS_REDIS_ENABLED) {
				cachedObjects = (Collection<T>) redisTemplateForList.opsForHash().get(objectKey, hashKey);
			}
			if (cachedDataProvider != null && ( cachedObjects == null || cachedObjects.isEmpty())) {
				cachedObjects = cachedDataProvider.fetchFromSource(objectKey, hashKey);
				setAll(objectKey, hashKey, cachedObjects);
			}
		} catch (Exception e) {
			LOGGER.error("Failed to fetch data collection from the cache", e);
		}
		return cachedObjects;
	}

	public void delete(String objectKey, Object hashKey) {
		try {
			if (IS_REDIS_ENABLED) {
				if (hashKey != null) {
					redisTemplateForList.opsForHash().delete(objectKey, hashKey);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Failed to delete data from the cache", e);
		}

	}
	
	public void deleteAll(String objectKey,List<String> hashKey){
		try {
			if (IS_REDIS_ENABLED) {
				if (hashKey != null && !hashKey.isEmpty()) {
					redisTemplateForList.opsForHash().delete(objectKey,hashKey.toArray());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Failed to delete data collection from the cache", e);
		}
		
	}
	
}
