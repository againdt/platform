package com.getinsured.hix.platform.handler;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletRequest;

public interface RequestHandler extends HandlerConstants {
	
	public String getName();
	public List<String> getHandlerPath();
	public boolean handleRequest(HashMap<String, Object> context, ServletRequest request, boolean async);

}
