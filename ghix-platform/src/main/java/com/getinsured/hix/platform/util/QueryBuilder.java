package com.getinsured.hix.platform.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.Subquery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.timeshift.util.TSDate;

@Component
@Scope("prototype")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class QueryBuilder<E> {
	private static int aliasCount = 0;
	private Logger logger = LoggerFactory.getLogger(QueryBuilder.class);
	private static final String UPPER_CASE = "UPPER";
	private static final String LOWER_CASE = "LOWER";

	/**
	 * Entity Manager instance for DB communication
	 */
	@PersistenceContext
	private EntityManager entityManager;

	public enum DataType {
		CHAR, STRING, NUMERIC, DATE, ENUM, LIST, LONG
	}

	public enum ComparisonType {
		LIKE, EQUALS, GT, LT, EQ, GE, LE, STARTWITH, NE, ISNULL, EQUALS_CASE_IGNORE, STARTWITH_EQUALS_CASE_IGNORE, ISNOTNULL
	}

	public enum SortOrder {
		ASC, DESC
	}

	public enum ConjuctionType {
		AND, OR
	}

	private CriteriaBuilder builder;
	private CriteriaQuery criteriaQuery;
	private Subquery subQuery;
	private Root<E> root;
	private boolean isSelectQuery = false;
	private boolean fetchDistinct;
	private Class entityClass;

	private List<String> selectColumns;

	private Map<String, Object> map = new HashMap<String, Object>();
	private Map<Class<?>,Set<String>> sortableColumnsForClass = new HashMap<Class<?>, Set<String>>();

	private boolean isColumnSortable(Class<?> clz, String columnName) {
		if(logger.isInfoEnabled()) {
			logger.info("Checking column name \"{}\" for class \"{}\" for sorting", columnName, clz.getName());
		}
		if(!SortableEntity.class.isAssignableFrom(this.entityClass)) {
			logger.error("Insecure Sort order, {} for column {}. have this class implement the SortableEntity interface and provide information on Sortable columns for validation",this.entityClass.getName(), columnName);
			return true;
		}
		Set<String> sColumns = this.sortableColumnsForClass.get(clz);
		if(sColumns == null) {
			try {
				SortableEntity entity = (SortableEntity) clz.newInstance();
				Method m = clz.getMethod("getSortableColumnNamesSet");
				sColumns = (Set<String>) m.invoke(entity);
				this.sortableColumnsForClass.put(clz, sColumns);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
				logger.error("Failed to find sort information from the entity {}, Exception encountered {} with message {}", clz.getName(), e.getClass().getName(), e.getMessage());
				return false;
			}
		}
		boolean sortable = sColumns.contains(columnName);
		if(!sortable && logger.isDebugEnabled()) {
			logger.debug("Column \"{}\" not sortable, supported columns are {}",columnName,sColumns);
		}
		return sortable;
	}
	/**
	 * Initialize the QueryBuilder with class for which the Object Fetch Query
	 * should be prepared.
	 */
	private void init(Class clazz) {
		this.entityClass = clazz;
		builder = entityManager.getCriteriaBuilder();
		if (!isSelectQuery) {
			criteriaQuery = builder.createQuery(clazz);
		} else {
			criteriaQuery = builder.createQuery(Object[].class);
		}
		root = criteriaQuery.from(clazz);
	}

	/**
	 * Initialize the QueryBuilder with class for which the Object Fetch Query
	 * should be prepared. Along with appropriate names of required join tables.
	 */
	private void init(Class clazz, List<String> joinTables) {
		init(clazz);
		Join a = null;
		for (String joinTable : joinTables) {
			if (!isSelectQuery) {
				a = root.join(joinTable, JoinType.LEFT);
				// root.fetch(joinTable);
			} else {
				a = root.join(joinTable, JoinType.LEFT);
				// root.fetch(joinTable);
			}
			// Join a = countRoot.join(joinTable,JoinType.LEFT);
			map.put(joinTable, a);
		}
	}

	/**
	 * Initialize the QueryBuilder with class for which the Object Fetch Query
	 * should be prepared.
	 */
	private void initSubQuery(Class clazz) {
		builder = entityManager.getCriteriaBuilder();
		criteriaQuery = builder.createQuery(clazz);
		subQuery = criteriaQuery.subquery(clazz);
		root = subQuery.from(clazz);
		subQuery.select(root);
	}

	/**
	 * Initialize the QueryBuilder with class for which the Object Query should
	 * be prepared.
	 */
	public void buildObjectQuery(Class clazz) {
		init(clazz);
	}

	/**
	 * Initialize the QueryBuilder with class for which the Object Query should
	 * be prepared. Along with appropriate names of required join tables.
	 */
	public void buildObjectQuery(Class clazz, List<String> joinTables) {
		init(clazz, joinTables);
	}

	/**
	 * Initialize the QueryBuilder with class for which the SELECT Query should
	 * be prepared. Along with appropriate names of required join tables.
	 */
	public void buildSelectQuery(Class clazz, List<String> joinTables) {
		isSelectQuery = true;
		init(clazz, joinTables);
	}

	/**
	 * Initialize the QueryBuilder with class for which the SELECT Query should
	 * be prepared. Along with appropriate names of required join tables.
	 *
	 * <b> This time as INNER join and not LEFT join </b>
	 *
	 * @param clazz
	 * @param joinTables
	 */
	public void buildInnerSelectQuery(Class clazz, List<String> joinTables) {
		isSelectQuery = true;
		init(clazz, joinTables, false);
	}

	private void init(Class clazz, List<String> joinTables,boolean isSelectQuery2) {
		init(clazz);
		Join a = null;
		for (String joinTable : joinTables) {
			if (!isSelectQuery2) {
				a = root.join(joinTable, JoinType.INNER);
			}
			map.put(joinTable, a);
		}

	}

	/**
	 * Initialize the QueryBuilder with class for which the SELECT Query should
	 * be prepared.
	 */
	public void buildSelectQuery(Class clazz) {
		isSelectQuery = true;
		init(clazz);
	}

	/**
	 * Initialize the QueryBuilder with class for which the SELECT Query should
	 * be prepared.
	 */
	public void buildSubQuery(Class clazz) {
		initSubQuery(clazz);
	}

	/**
	 * Applies specified where clause to the query.
	 *
	 * @param columnName
	 *            {@Code String} the columnName which should be part of
	 *            whereClause
	 * @param value
	 *            {@Code Object} the Value which should be part of
	 *            whereClause for {@code columnName}
	 * @param dataType
	 *            {@Code DataType} the data type of column
	 *            {@code columnName}
	 * @param cmpType
	 *            {@Code ComparisonType} Specify the kind of Comparison
	 *            needs to be done with specified where clause.
	 *
	 *            <pre>
	 * 		For Example :
	 * 			For Like comparison on any String/varchar column values will be as below
	 * 			columnName = 'firstName'
	 * 			value = 'abc'
	 * 			dataType =  DataType.STRING
	 * 			cmpType = ComparisonType.LIKE
	 * </pre>
	 */
	public void applyWhere(String columnName, Object value, DataType dataType,
			ComparisonType cmpType) {
		if (DataType.CHAR.equals(dataType)) {
			charWhereClause(columnName, value, cmpType);
		} else if (DataType.STRING.equals(dataType)) {
			stringWhereClause(columnName, value, cmpType);
		} else if (DataType.DATE.equals(dataType)) {
			dateWhereClause(columnName, value, cmpType);
		} else if (DataType.NUMERIC.equals(dataType)) {
			numericWhereClause(columnName, value, cmpType);
		} else if (DataType.ENUM.equals(dataType)) {
			enumWhereClause(columnName, value, cmpType);
		} else if (DataType.LIST.equals(dataType)) {
			inWhereClause(columnName, value, cmpType);
		}
		else if(DataType.LONG.equals(dataType))
		{
			longWhereClause(columnName, value, cmpType);
		}
	}

	/**
	 * forms where clause for IN queries
	 * */
	private void inWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formInPredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Forms where clause for IN queries
	 */
	private Predicate formInPredicate(String columnName, Object value,
			ComparisonType cmpType) {
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		predicate = colName.in(((ArrayList) value).toArray());
		return predicate;
	}

	public void applyWhereClauses(List<String> columnNames, List values,
			List<DataType> dataTypes, List<ComparisonType> cmpTypes,
			List<ConjuctionType> conjunctionTypes) {
		/*
		 * iterate on coumnNames based on data type get the predicate store the
		 * conjuctiontype if the iteration is 2nd onwards.. get the previous
		 * conjuction type AND it or OR it
		 */
		int size = columnNames.size();
		String columnName;
		Object value;
		DataType dataType;
		ComparisonType cmpType;
		ConjuctionType conjunctionType;
		List<Predicate> predicates = new ArrayList<Predicate>();
		Predicate predicate = null;

		for (int index = 0; index < size; index++) {
			columnName = columnNames.get(index);
			value = values.get(index);
			dataType = dataTypes.get(index);
			cmpType = cmpTypes.get(index);
			if(DataType.CHAR.equals(dataType)) {
				predicate = formCharPredicate(columnName, value, cmpType);
			} else if (DataType.STRING.equals(dataType)) {
				predicate = formStringPredicate(columnName, value, cmpType);
			} else if (DataType.DATE.equals(dataType)) {
				predicate = formDatePredicate(columnName, value, cmpType);
			} else if (DataType.NUMERIC.equals(dataType)) {
				predicate = formNumericPredicate(columnName, value, cmpType);
			}
			predicates.add(predicate);
		}

		Predicate oldPredicate;
		Predicate currentPredicate;
		size = conjunctionTypes.size();
		oldPredicate = predicates.get(0);

		for (int index = 1; index < size; index++) {
			conjunctionType = conjunctionTypes.get(index - 1);
			currentPredicate = predicates.get(index);
			if (ConjuctionType.AND.equals(conjunctionType)) {
				oldPredicate = builder.and(oldPredicate, currentPredicate);
			} else if (ConjuctionType.OR.equals(conjunctionType)) {
				oldPredicate = builder.or(oldPredicate, currentPredicate);
			}
		}
		updateCriteriaPredicate(oldPredicate);
	}

	/**
	 * Forms where clause for Date columns.
	 */
	private Predicate formDatePredicate(String columnName, Object value,
			ComparisonType cmpType) {
		if (value instanceof TSDate)
		{
			Calendar myCalendar = new GregorianCalendar();
			myCalendar.setTime((Date)value);
			value = myCalendar.getTime();
		}
		Expression<Date> literal = builder.literal((Date) value);
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		if (ComparisonType.GT.equals(cmpType)) {
			predicate = builder.greaterThan(colName, literal);
		} else if (ComparisonType.GE.equals(cmpType)) {
			predicate = builder.greaterThanOrEqualTo(colName, literal);
		} else if (ComparisonType.LT.equals(cmpType)) {
			predicate = builder.lessThan(colName, literal);
		} else if (ComparisonType.LE.equals(cmpType)) {
			predicate = builder.lessThanOrEqualTo(colName, literal);
		} else if (ComparisonType.EQ.equals(cmpType)) {
			predicate = builder.equal(colName, literal);
		} else if (ComparisonType.NE.equals(cmpType)) {
			predicate = builder.notEqual(colName, literal);
		}
		return predicate;
	}

	/**
	 * Forms where clause for Date columns.
	 */
	private void dateWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formDatePredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Forms where clause for Numeric columns.
	 */
	private Predicate formNumericPredicate(String columnName, Object value,
			ComparisonType cmpType) {
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		if (ComparisonType.ISNULL.equals(cmpType)) {
			predicate = builder.isNull(getPath(columnName));
		} else if (ComparisonType.GT.equals(cmpType)) {
			Expression<Integer> literal = builder.literal((Integer) value);
			predicate = builder.greaterThan(colName, literal);
		} else if (ComparisonType.GE.equals(cmpType)) {
			Expression<Integer> literal = builder.literal((Integer) value);
			predicate = builder.greaterThanOrEqualTo(colName, literal);
		} else if (ComparisonType.LT.equals(cmpType)) {
			Expression<Integer> literal = builder.literal((Integer) value);
			predicate = builder.lessThan(colName, literal);
		} else if (ComparisonType.LE.equals(cmpType)) {
			Expression<Integer> literal = builder.literal((Integer) value);
			predicate = builder.lessThanOrEqualTo(colName, literal);
		} else if (ComparisonType.EQ.equals(cmpType)) {
			Expression<Integer> literal = builder.literal((Integer) value);
			predicate = builder.equal(colName, literal);
		} else if (ComparisonType.NE.equals(cmpType)) {
			Expression<Integer> literal = builder.literal((Integer) value);
			predicate = builder.notEqual(colName, literal);
		}
		/*
		 *adding not null for numeric value
		 */
		else if (ComparisonType.ISNOTNULL.equals(cmpType)) {
			predicate = builder.isNotNull(getPath(columnName));
		}
		return predicate;
	}

	/**
	 * Forms where clause for Long columns.
	 */
	private Predicate formLongPredicate(String columnName, Object value,
			ComparisonType cmpType) {
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		if (ComparisonType.ISNULL.equals(cmpType)) {
			predicate = builder.isNull(getPath(columnName));
		} else if (ComparisonType.GT.equals(cmpType)) {
			Expression<Long> literal = builder.literal((Long) value);
			predicate = builder.greaterThan(colName, literal);
		} else if (ComparisonType.GE.equals(cmpType)) {
			Expression<Long> literal = builder.literal((Long) value);
			predicate = builder.greaterThanOrEqualTo(colName, literal);
		} else if (ComparisonType.LT.equals(cmpType)) {
			Expression<Long> literal = builder.literal((Long) value);
			predicate = builder.lessThan(colName, literal);
		} else if (ComparisonType.LE.equals(cmpType)) {
			Expression<Long> literal = builder.literal((Long) value);
			predicate = builder.lessThanOrEqualTo(colName, literal);
		} else if (ComparisonType.EQ.equals(cmpType)) {
			Expression<Long> literal = builder.literal((Long) value);
			predicate = builder.equal(colName, literal);
		} else if (ComparisonType.NE.equals(cmpType)) {
			Expression<Long> literal = builder.literal((Long) value);
			predicate = builder.notEqual(colName, literal);
		}
		return predicate;
	}


	/**
	 * Forms where clause for Numeric columns.
	 */
	private void numericWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formNumericPredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Forms where clause for Long columns.
	 */
	private void longWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formLongPredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Forms where clause for String columns.
	 */
	private Predicate formEnumPredicate(String columnName, Object value,
			ComparisonType cmpType) {
		// Expression<String> colName = root.get(columnName);
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		predicate = builder.equal(colName, value);

		if (ComparisonType.NE.equals(cmpType)) {
			predicate = builder.notEqual(colName, value);
		}

		return predicate;
	}

	/**
	 * Forms where clause for String columns.
	 */
	private Predicate formStringPredicate(String columnName, Object value,
			ComparisonType cmpType) {
		// Expression<String> colName = root.get(columnName);
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		if (ComparisonType.ISNULL.equals(cmpType)) {
			predicate = builder.isNull(getPath(columnName));
		} else if (ComparisonType.LIKE.equals(cmpType)) {
			Expression<String> literal = builder.literal("%" + value + "%");
			predicate = builder.like(builder.upper(colName), literal);
		} else if (ComparisonType.EQUALS.equals(cmpType)) {
			Expression<String> literal = builder.literal((String) value);
			predicate = builder.equal(colName, literal);
		} else if (ComparisonType.EQUALS_CASE_IGNORE.equals(cmpType)) {
			// @author : Ashok : Added for changing the character case of column
			// value for searching
			Expression<String> literal = builder.literal(((String) value)
					.toUpperCase());
			predicate = builder.equal(builder.upper(colName), literal);
		} else if (ComparisonType.STARTWITH.equals(cmpType)) {
			Expression<String> literal = builder.literal(value + "%");
			predicate = builder.like(builder.upper(colName), literal);
		} else if (ComparisonType.STARTWITH_EQUALS_CASE_IGNORE.equals(cmpType)) {
			// @author : Ashok : Added for changing the character case of column
			// value for searching
			Expression<String> literal = builder.literal(((String) value)
					.toUpperCase() + "%");
			predicate = builder.like(builder.upper(colName), literal);
		} else if (ComparisonType.NE.equals(cmpType)) {
			Expression<String> literal = builder.literal((String) value);
			predicate = builder.notEqual(builder.upper(colName), literal);
		} else if (ComparisonType.ISNOTNULL.equals(cmpType)) {
			predicate = builder.isNotNull(getPath(columnName));
		}

		return predicate;
	}

	/**
	 * Forms where clause for Char columns.
	 */
	private Predicate formCharPredicate(String columnName, Object value,
			ComparisonType cmpType) {
		Expression colName = getPath(columnName);
		Predicate predicate = null;
		if (ComparisonType.ISNULL.equals(cmpType)) {
			predicate = builder.isNull(getPath(columnName));
		} else if (ComparisonType.LIKE.equals(cmpType)) {
			Expression<String> literal = builder.literal("%" + value + "%");
			predicate = builder.like(builder.upper(colName), literal);
		} else if (ComparisonType.EQUALS.equals(cmpType)) {
			Expression<Character> literal = builder.literal((Character) value);
			predicate = builder.equal(colName, literal);
		} else if (ComparisonType.NE.equals(cmpType)) {
			Expression<Character> literal = builder.literal((Character) value);
			predicate = builder.notEqual(builder.upper(colName), literal);
		} else if (ComparisonType.ISNOTNULL.equals(cmpType)) {
			predicate = builder.isNotNull(getPath(columnName));
		}
		return predicate;
	}

	/**
	 * Forms where clause for String columns.
	 */
	private void enumWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formEnumPredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Forms where clause for String columns.
	 */
	private void stringWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formStringPredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Forms where clause for Char columns.
	 */
	private void charWhereClause(String columnName, Object value,
			ComparisonType cmpType) {
		Predicate predicate = formCharPredicate(columnName, value, cmpType);
		updateCriteriaPredicate(predicate);
	}

	/**
	 * Updates the existing criteria with ANDing the newly specified criteria.
	 */
	public void updateCriteriaPredicate(Predicate predicate) {
		Predicate existingPredicate = criteriaQuery.getRestriction();
		if (existingPredicate != null) {
			criteriaQuery.where(existingPredicate, predicate);
		} else {
			criteriaQuery.where(predicate);
		}
	}

	/**
	 * Updates the existing criteria with ANDing the newly specified criteria.
	 */
	public void updateSubqueryPredicate(Predicate predicate) {
		Predicate existingPredicate = subQuery.getRestriction();
		if (existingPredicate != null) {
			subQuery.where(existingPredicate, predicate);
		} else {
			subQuery.where(predicate);
		}
	}
	/**
	 * Applies specified Order by clause to the query.
	 *
	 * @param columnName
	 *            {@Code String} the columnName on which sort should be
	 *            applied.
	 * @param order
	 *            {@Code SortOrder} the Sort Order of column
	 *            {@code columnName}
	 */
	public void applySort(String columnName, SortOrder order) {
		
		if(this.isColumnSortable(entityClass, columnName)) {
			if (SortOrder.ASC.equals(order)) {
				criteriaQuery.orderBy(builder.asc(getPath(columnName)));
			} else if (SortOrder.DESC.equals(order)) {
				criteriaQuery.orderBy(builder.desc(getPath(columnName)));
			}
			if(logger.isDebugEnabled()) {
				logger.debug("Applying sort to column \"{}\"",columnName);
			}
		}else {
			logger.warn("Ignoring {} for sorting, not registered",columnName);
		}
	}

	/**
	 * Applies sort for list of given columns
	 *
	 * @param columnNames
	 * @param sortOrder
	 */
	// @author : Biswaranjan : Added for applying multiple column sort
	public void applySortList(List<String> columnNames, SortOrder sortOrder) {
		List<Order> orders = new ArrayList<>();

		if(columnNames!=null && columnNames.size()>0){
			for(String columnName:columnNames){
				Expression colName = getPath(columnName);
				if(this.isColumnSortable(entityClass, columnName)) {
					if (SortOrder.ASC.equals(sortOrder)) {
						orders.add(builder.asc(colName));
					} else if (SortOrder.DESC.equals(sortOrder)) {
						orders.add(builder.desc(colName));
					}
				}else {
					logger.warn("Ignoring {} for sorting, not registered",columnName);
				}
}
			criteriaQuery.orderBy(orders);
		}
	}

	/**
	 * Applies sort for list of given columns by Case Name (UPPER / LOWER / NULL)
	 */
	public void applySortList(List<String> columnNames, SortOrder sortOrder, String caseName) {

		List<Order> orders = new ArrayList<>();

		if (columnNames != null && columnNames.size() > 0) {

			Expression<Object> expressionList = null;
			Expression colName = null;

			for (String columnName : columnNames) {

				colName = getPath(columnName);

				if (this.isColumnSortable(entityClass, columnName)) {
					expressionList = null;

					if (SortOrder.ASC.equals(sortOrder)) {

						if (UPPER_CASE.equalsIgnoreCase(caseName)) {
							expressionList = builder.upper(colName);
						}
						else if (LOWER_CASE.equalsIgnoreCase(caseName)) {
							expressionList = builder.lower(colName);
						}
						else {
							expressionList = getPath(columnName);
						}
						orders.add(builder.asc(expressionList));
					}
					else if (SortOrder.DESC.equals(sortOrder)) {

						if (UPPER_CASE.equalsIgnoreCase(caseName)) {
							expressionList = builder.upper(colName);
						}
						else if (LOWER_CASE.equalsIgnoreCase(caseName)) {
							expressionList = builder.lower(colName);
						}
						else {
							expressionList = getPath(columnName);
						}
						orders.add(builder.desc(expressionList));
					}
				}
				else {
					logger.warn("Ignoring {} for sorting, not registered", columnName);
				}
			}
			criteriaQuery.orderBy(orders);
		}
	}

	// @author : Ashok : Added for changing the character case of column value
	// for sorting
	public void applySort(String columnName, SortOrder order, String caseName) {
		if(!this.isColumnSortable(entityClass, columnName)) {
			logger.warn("Ignoring {} for sorting, not registered",columnName);
			return;
		}
		Expression colName = getPath(columnName);
		if (SortOrder.ASC.equals(order)) {
			if (caseName.equalsIgnoreCase(UPPER_CASE)) {
				criteriaQuery.orderBy(builder.asc(builder.upper(colName)));
			} else if (caseName.equalsIgnoreCase(LOWER_CASE)) {
				criteriaQuery.orderBy(builder.asc(builder.lower(colName)));
			}
		} else if (SortOrder.DESC.equals(order)) {
			if (caseName.equalsIgnoreCase(UPPER_CASE)) {
				criteriaQuery.orderBy(builder.desc(builder.upper(colName)));
			} else if (caseName.equalsIgnoreCase(LOWER_CASE)) {
				criteriaQuery.orderBy(builder.desc(builder.lower(colName)));
			}
		}
	}

	private boolean isSubObject(String columnName) {
		return columnName.contains(".");
	}

	private boolean isEmbededObject(String columnName) {
		return columnName.contains("#");
	}
	
	public Expression<Object> getPath(String columnName) {
		Path<Object> path = null;
		if (isSubObject(columnName)) {
			String[] strings = columnName.split("\\.");
			Join j = null;
			List<String> a = Arrays.asList(strings);
			int i = 0;
			for (String parent : a) {
				if (i == 0) {
					j = (Join) map.get(parent);
					if (j == null) {
						j = root.join(parent);
						map.put(parent, j);
					}
				} else if (i == 1) {
					if(j != null){
						path = j.get(parent);
					}
				} else if (path != null){
					path = path.get(parent);
				}
				i++;
			}
		} 
		else if (isEmbededObject(columnName)) {
			String[] strings = columnName.split("\\#");
			List<String> a = Arrays.asList(strings);
			for (String parent : a) {
				path = (path == null) ? root.get(parent) : path.get(parent);
			}
		}
		else {
			path = root.get(columnName);
		}
		return path;
	}


	public Subquery getSubQuery() {
		return subQuery;
	}

	public void setSubQuery(Subquery subQuery) {
		this.subQuery = subQuery;
	}

	public CriteriaBuilder getBuilder() {
		return builder;
	}

	public void setBuilder(CriteriaBuilder builder) {
		this.builder = builder;
	}

	/**
	 * If the query is being prepared as a Select query instead of Object fetch
	 * query, then List<String> of Select columns should be provided with this
	 * method.
	 *
	 * @param columnNames
	 *            {@Code List<String>} columns which should be part of
	 *            select columns.
	 */
	public void applySelectColumns(List<String> columnNames) {
		List<Selection<?>> selections = new ArrayList<Selection<?>>();
		for (String column : columnNames) {
			selections.add(getPath(column));
			criteriaQuery.multiselect(selections);
		}
		selectColumns = columnNames;
	}

	/**
	 * Returns List<E> of objects E. Note : This should be used when an object
	 * fetch query is performed.
	 *
	 * @param startRecord
	 *            {@code int} startRecord number (for paginated record fetch)
	 * @param pageSize
	 *            {@code int} to indicate how many records to be fetched from
	 *            specified {@code int} startRecord
	 */
	public List<E> getRecords(int startRecord, int pageSize) {
		if (fetchDistinct) {
			criteriaQuery.distinct(true);
		}
		setTenantIdIfApplicable(criteriaQuery);
		Query query = entityManager.createQuery(criteriaQuery);
		if (pageSize != -1) {
			query.setFirstResult(startRecord);
			query.setMaxResults(pageSize);
		}
		return query.getResultList();
	}

	private void setTenantIdIfApplicable(CriteriaQuery criteriaQuery) {
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
			if(isSetTenantIdMethodDefined(this.entityClass)) {
				longWhereClause("tenantId", TenantContextHolder.getTenant().getId(), ComparisonType.EQ);
			}
		}
	}

	private boolean isSetTenantIdMethodDefined(Class clazz) {
		boolean retVal = false;
		try {
			if(this.entityClass.getMethod("setTenantId", Long.class) != null) {
				retVal = true;
			}
		} catch (NoSuchMethodException | SecurityException e) {
			//TODO : Check if we need to log this exception
		}
		return retVal;
	}

	/**
	 * Returns the total number of records present for specified clauses.
	 */
	public Long getRecordCount() {
		CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);
		copyCriteria(criteriaQuery, countQuery);
		if (fetchDistinct) {
			countQuery.select(builder.countDistinct(countQuery.getRoots()
					.iterator().next()));
		} else {
			countQuery.select(builder.count(countQuery.getRoots().iterator()
					.next()));
		}
		Query query = entityManager.createQuery(countQuery);
		return (Long) query.getSingleResult();
	}

	/**
	 * Returns List<Map<String,Object>> of objects for specified select query.
	 * Note : This should be used when an select fetch query is performed (Query
	 * with limited number of columns).
	 *
	 * @param startRecord
	 *            {@code int} startRecord number (for paginated record fetch)
	 * @param pageSize
	 *            {@code int} to indicate how many records to be fetched from
	 *            specified {@code int} startRecord
	 */
	public List<Map<String, Object>> getData(int startRecord, int pageSize) {
		if (fetchDistinct) {
			criteriaQuery.distinct(true);
		}
		setTenantIdIfApplicable(criteriaQuery);
		Query query = entityManager.createQuery(criteriaQuery);
		if(pageSize != -1){
			query.setFirstResult(startRecord);
			query.setMaxResults(pageSize);
		}
		List<Object[]> data = query.getResultList();
		List<Map<String, Object>> dataset = new ArrayList<Map<String, Object>>();
		String columnName = null;
		for (Object[] tuple : data) {
			Map<String, Object> test = new HashMap<String, Object>();
			for (int i = 0; i < selectColumns.size(); i++) {
				columnName = selectColumns.get(i);
				if (columnName.contains(".")) {
					columnName = columnName.replace(".", "");
				}
				if (columnName.contains("#")) {
					columnName = columnName.replace("#", "");
				}
				test.put(columnName, tuple[i]);
			}
			dataset.add(test);
		}
		return dataset;
	}

	public void setFetchDistinct(boolean fetchDistinct) {
		this.fetchDistinct = fetchDistinct;
	}

	public static <T> void copyCriteria(CriteriaQuery<T> from,
			CriteriaQuery<T> to) {
		copyCriteriaNoSelection(from, to);
		to.select(from.getSelection());
	}

	/**
	 * Copy Criteria without Selection
	 *
	 * @param from
	 *            source Criteria
	 * @param to
	 *            destination Criteria
	 */
	public static void copyCriteriaNoSelection(CriteriaQuery<?> from,
			CriteriaQuery<?> to) {
		// Copy Roots
		for (Root<?> root : from.getRoots()) {
			Root<?> dest = to.from(root.getJavaType());
			dest.alias(getOrCreateAlias(root));
			copyJoins(root, dest);
		}
		if (from.getRestriction() != null) {
			to.where(from.getRestriction());
		}
	}

	/**
	 * Copy Joins
	 *
	 * @param from
	 *            source Join
	 * @param to
	 *            destination Join
	 */
	public static void copyJoins(From<?, ?> from, From<?, ?> to) {
		for (Join<?, ?> j : from.getJoins()) {
			Join<?, ?> toJoin = to.join(j.getAttribute().getName(),
					j.getJoinType());
			toJoin.alias(getOrCreateAlias(j));

			copyJoins(j, toJoin);
		}

		for (Fetch<?, ?> f : from.getFetches()) {
			Fetch<?, ?> toFetch = to.fetch(f.getAttribute().getName());
			copyFetches(f, toFetch);

		}
	}

	/**
	 * Copy Fetches
	 *
	 * @param from
	 *            source Fetch
	 * @param toFetch
	 *            dest Fetch
	 */
	public static void copyFetches(Fetch<?, ?> from, Fetch<?, ?> to) {
		for (Fetch<?, ?> f : from.getFetches()) {
			Fetch<?, ?> toFetch = to.fetch(f.getAttribute().getName());
			// recursively copy fetches
			copyFetches(f, toFetch);
		}
	}

	/**
	 * Gets The result alias, if none set a default one and return it
	 *
	 * @param criteria
	 *            criteria
	 * @return root alias or generated one
	 */
	public static <T> String getOrCreateAlias(Selection<T> selection) {
		String alias = selection.getAlias();
		if (alias == null) {
			alias = "Custom_generatedAlias" + aliasCount++;
			selection.alias(alias);
		}
		return alias;
	}
}
