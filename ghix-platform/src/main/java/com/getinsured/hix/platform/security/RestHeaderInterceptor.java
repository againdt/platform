package com.getinsured.hix.platform.security;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import com.getinsured.hix.platform.logging.filter.RequestCorrelationContext;
import com.getinsured.hix.platform.logging.filter.RequesterNameHolder;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class RestHeaderInterceptor implements HttpRequestInterceptor {

	private static final String AFFILIATE_ID = "AFFILIATE_ID";
	private static final String TENANT_KEY = "TENANT_KEY";

	@Override
	public void process(HttpRequest request, HttpContext context)
			throws HttpException, IOException {
		Header host = request.getFirstHeader(HTTP.TARGET_HOST);
		if(host != null && host.getValue().contains(GhixPlatformConstants.HUB_INTEGRATION_HOST)){
			return;
		}
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getUrl() != null) {
			Header[] tenantHeader = request.getHeaders(TENANT_KEY);
			if(tenantHeader != null && tenantHeader.length == 0) {
				request.addHeader(TENANT_KEY, TenantContextHolder.getTenant().getUrl());
			}
		}
		
		if(TenantContextHolder.getAffiliateId() != null) {
			Header[] affiliateIdHeader = request.getHeaders(AFFILIATE_ID);
			if(affiliateIdHeader != null && affiliateIdHeader.length == 0) {
				request.addHeader( AFFILIATE_ID, TenantContextHolder.getAffiliateId().toString());
			}
		}
		try {
			if (null != RequestCorrelationContext.getCurrent() && null != RequestCorrelationContext.getCurrent().getCorrelationId()) {
				String requestCorrelationId =  RequestCorrelationContext.getCurrent().getCorrelationId();
				if (null != RequesterNameHolder.getCurrent() && null != RequesterNameHolder.getCurrent().getContextName()) {
					String[] params = requestCorrelationId.split(",requester=");
					requestCorrelationId = params[0].concat(",requester="+ RequesterNameHolder.getCurrent().getContextName());
				}
				request.addHeader("X-requestCorrelationId", requestCorrelationId);
			}
		} catch (Exception ex) {
			//
		}
	}
}
