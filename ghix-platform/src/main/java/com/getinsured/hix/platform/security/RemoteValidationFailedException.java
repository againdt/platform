package com.getinsured.hix.platform.security;

public class RemoteValidationFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RemoteValidationFailedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemoteValidationFailedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RemoteValidationFailedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RemoteValidationFailedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RemoteValidationFailedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
