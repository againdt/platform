package com.getinsured.hix.platform.ftp;


public class GHIXFTPFile {
	
	private String name;
	private long size;
	private String timeStamp;
	
	public GHIXFTPFile() {
		super();
	}

	public GHIXFTPFile(String name, long size, String timeStamp) {
		super();
		this.name = name;
		this.size = size;
		this.timeStamp = timeStamp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
}
