package com.getinsured.hix.platform.enums;

import org.apache.commons.lang3.StringUtils;

public enum YesNoEnum {
	YES, NO;

	public static YesNoEnum forString(String enumString) {
		YesNoEnum retval;
		if (StringUtils.isBlank(enumString)) {
			retval = null;
		} else {
			switch (enumString) {
			case "YES":
				retval = YES;
				break;
			case "NO":
				retval = NO;
				break;
			default:
				retval = NO;
			}
		}
		return retval;
	}
}
