package com.getinsured.hix.platform.couchbase.helper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;

public final class BucketMetaHelper {

	private static final String BUILD_SNAPSHOT = "-BUILD-SNAPSHOT";
	private static final String ARTIFACT_VERSION = "version";
	private static final String PHIX = "PHIX";
	private static final String MAVEN_POM_PROPS = "/META-INF/maven/com.getinsured.platform/ghix-platform/pom.properties";

	private static String BUILD_VERSION = null;
	private static String ENV = null;

	private BucketMetaHelper() {}

	/**
	 * Information read from current request.
	 * 
	 */
	public static String getEnv() {

		if (!StringUtils.isBlank(ENV)){
			return ENV;
		}

		String env = StringUtils.EMPTY;
		try {
			ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
			if (servletRequestAttributes != null){
				HttpServletRequest request = servletRequestAttributes.getRequest();
				StringBuffer requestURL = request.getRequestURL();
				if (requestURL != null){
					env = new URL(requestURL.toString()).getHost();
				}
			}

			if (StringUtils.isBlank(env) && StringUtils.isNotBlank(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL)){ /* safety check */
				env = new URL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL).getHost();
			}


		} catch (Exception e) {
			/* will this happen */
		} 

		if (!StringUtils.equalsIgnoreCase(PHIX, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))){
			/* this would handle multi-tenant */
			ENV = env;
		}

		return env;

	}

	/**
	 * Information read from jar.
	 * 
	 */
	public static String getBuildArtifactVersion(){

		if (StringUtils.isBlank(BUILD_VERSION)){
			String version = null;
			InputStream is = null;
			try {
				Properties p = new Properties();
				is = BucketMetaHelper.class.getResourceAsStream(MAVEN_POM_PROPS);
				if (is != null) {
					p.load(is);
					version = p.getProperty(ARTIFACT_VERSION, StringUtils.EMPTY);
				}

			} catch(Exception e){
				/* will this happen? */
			} finally{
				if (is != null){
					try {
						is.close();
					} catch (IOException e) {
						/* will this happen? */
					}
				}
			}

			BUILD_VERSION = StringUtils.EMPTY;
			if (version != null){
				String value = StringUtils.substringBefore(version, BUILD_SNAPSHOT);
				BUILD_VERSION = StringUtils.isNotBlank(value) ? value : StringUtils.EMPTY;
			}
		}

		return BUILD_VERSION;

	}

	public static String getTenantCode() {
		TenantDTO tenantDto = TenantContextHolder.getTenant();
		String tenantCode = StringUtils.EMPTY;

		if (tenantDto != null && !StringUtils.isEmpty(tenantDto.getName())){
			tenantCode = tenantDto.getCode();
		} else {
			tenantCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		}
		return tenantCode;
	}
}
