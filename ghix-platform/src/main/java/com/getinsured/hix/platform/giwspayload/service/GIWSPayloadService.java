package com.getinsured.hix.platform.giwspayload.service;

import com.getinsured.hix.model.GIWSPayload;

import java.util.List;

public interface GIWSPayloadService {
	
	GIWSPayload save(GIWSPayload giwsPayload);
	
	GIWSPayload findById(Integer id);
	
	List<GIWSPayload> findBySSAPID(Long ssapId);

	List<GIWSPayload> findByCustomKeyId1(String customKey1);
	
	List<GIWSPayload> findByEndpointFunctionAndSSAPID(String endpointFunction, Long ssapId);
}
