package com.getinsured.hix.platform.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class RestServiceExecutionContext {
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServiceExecutionContext.class);
	private static final String PRINCIPLE_REQUEST_HEADER_KEY="GHIX_SSO_USER";
	private MediaType contentType = MediaType.APPLICATION_JSON;
	private HttpHeaders headers = null;
	private Map<String,Object> requestPrams = null;
	private HttpMethod httpMethod = null;
	private HttpEntity<?> httpEntity = null;
	private Object requestObject = null;
	private String URL;
	private String principleUser = null;
	
	public RestServiceExecutionContext(String url, String method) {
		this.URL = url;
		if(method.equalsIgnoreCase("post")){
			this.httpMethod = HttpMethod.POST;
		}else if(method.equalsIgnoreCase("GET")){
			this.httpMethod = HttpMethod.GET;
		}else{
			throw new GIRuntimeException("Method not supported:"+method+" Expecting GET or POST");
		}
		this.headers = new HttpHeaders();
	}
	
	public HttpMethod getHttpMethod(){
		return this.httpMethod;
	}
	
	public HttpEntity<?> getHttpEntity(){
		if(this.httpEntity == null){
			if(this.requestObject != null){
				this.httpEntity = new HttpEntity<Object>(this.requestObject);
			}else{
				this.httpEntity = new HttpEntity<Map<String, ?>>(this.requestPrams, headers);
			}
		}
		return this.httpEntity;
	}
	
	public void setRequestObject(Object obj){
		if(this.requestPrams != null){
			throw new GIRuntimeException("Execution context is set to process parameters set, can not handle the object, use a fresh context");
		}
		this.requestObject = obj;
	}
	
	public Object getRequestObject(){
		return this.requestObject;
	}
	
	
	public void addRequestParams(String key, Object val){
		if(this.requestObject != null){
			throw new GIRuntimeException("Execution context is already set to process an object, can not handle more request parameters, use a fresh context");
		}
		if(this.requestPrams == null){
			this.requestPrams = new HashMap<String, Object>();
		}
		this.requestPrams.put(key, val);
	}
	
	public Map<String, Object> getRequestParams(){
		if(this.requestPrams == null){
			return new HashMap<String, Object>();
		}
		return this.requestPrams;
	}
	
	public void setPrincipleRequestHeader(String principle){
		List<String> existingPrincipleHeader = this.headers.get(PRINCIPLE_REQUEST_HEADER_KEY);
		if(existingPrincipleHeader.size() > 0){
			throw new GIRuntimeException("Principle already set ["+existingPrincipleHeader.get(0)+"], not expecting more than one principle");
		}		
		this.principleUser = principle.toLowerCase();
		this.headers.add(PRINCIPLE_REQUEST_HEADER_KEY, this.principleUser);
	}
	
	public String getPrincipleRequestHeader(){
		if(this.headers == null || this.headers.size() == 0){
			LOGGER.warn("Call to read Principle from request headers returned NULL");
			return null;
		}
		return this.headers.get(PRINCIPLE_REQUEST_HEADER_KEY).get(0);
	}

	public MediaType getContentType() {
		return contentType;
	}

	public void setContentType(MediaType contentType) {
		this.contentType = contentType;
		this.headers.setContentType(contentType);
	}

	public String getURL() {
		return this.URL;
	}

	public void setURL(String uRL) {
		this.URL = uRL;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("User:"+this.principleUser+"\n");
		sb.append("Method:"+this.httpMethod.toString()+"\n");
		sb.append("URL:"+this.URL+"\n");
		sb.append("Headers"+this.headers.toSingleValueMap()+"\n");
		sb.append("Parameters (if Any:"+this.requestPrams+"\n");
		sb.append("Request Object (if any)"+this.requestObject+"\n");
		return sb.toString();
	}
}
