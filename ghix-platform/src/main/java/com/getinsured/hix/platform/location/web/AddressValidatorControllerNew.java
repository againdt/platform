package com.getinsured.hix.platform.location.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.model.AddressValidatorResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.location.service.AddressValidatorService;

/**
 * This is AddressValidatorControllerNew. It will be used by VIMO viewValidAddressListNew function to perform AJAX call. *
 * 
 * From Spec:-
 * 	There are a variety of situations we need to address:
 *
 *
 *		User enters an address that is missing required fields.
 *		User enters an address that is an exact match to an address in the database.
 *		User enters an address that is close to an address in the database, but not an exact match.
 *		User enters an address for which no close match is found in the database.
 *
 *		Display up to 2 most likely matches from the database
 * 
 * @author Ekram Ali Kazi
 */
@Controller
public class AddressValidatorControllerNew {

	@Autowired private AddressValidatorService addressValidatorService;

	private static final Logger LOGGER = Logger.getLogger(AddressValidatorControllerNew.class);

	@RequestMapping(value = "/address/validate", method = RequestMethod.POST)
	@ResponseBody
	public AddressValidationResponse validateAddress(@RequestBody LocationDTO address) {
		
		AddressValidationResponse validatorResponse = new com.getinsured.hix.platform.dto.address.AddressValidationResponse();
		
		try{
			validatorResponse = addressValidatorService.validateAddress(address);			
		}catch(Exception ex){
			LOGGER.error("Exception while validating address ", ex);
		}
		
		return validatorResponse;

	}
	
	@RequestMapping(value = "/platform/validateaddressnew", method = RequestMethod.GET)
	@ResponseBody
	public String validateAddress(Model model, HttpServletRequest request,
			@RequestParam("enteredAddress") String enteredAddress,
			@RequestParam("ids") String ids) {


		LOGGER.debug("Address Validation - Version 2....");

		/* performed initial validation on ids, this will be configured by the developer */
		if (ids == null || !ids.contains("~")){
			LOGGER.error("Invalid configuration for ids. ids cannot be null and should be ~ separated. Returning from AddressValidatorController.validateAddress without hitting address web srvice.");
			return "IGNORE";
		}

		Location location = null;
		if (!StringUtils.isEmpty(enteredAddress) && enteredAddress.contains(",")) {
			String[] items = enteredAddress.split("\\s*,\\s*");
			if (items.length > 0 && items.length < 6) {

				String addressLine1 = items[0];
				String addressLine2 = items[1].equals("") ? null : items[1];
				String city = items[2];
				String state = items[3];
				Integer zip = StringUtils.isNumeric(items[4]) ? Integer.parseInt(items[4]) : 0;

				//perform incoming data validations
				if(StringUtils.isEmpty(addressLine1) || StringUtils.isEmpty(state) || StringUtils.isEmpty(city) || zip == 0){
					// this should never happen... as this is supposed to be handle at client-side
					// Spec2.1.1:- User enters an address that is missing required fields
					LOGGER.warn("******************Invalid input. Either addressLine1, city, state or zip is missing. Returning from AddressValidatorControllerNew.validateAddress without hitting address web srvice.");
					return "IGNORE";
				}

				location = formLocation(addressLine1, addressLine2, city, state, zip);
			}

		}

		if (location == null){
			// this should never happen... as this is supposed to be handle at client-side
			LOGGER.info("Location object not found. Returning from AddressValidatorControllerNew.validateAddress without hitting address web srvice.");
			return "IGNORE";
		}


		AddressValidatorResponse response = invokeAddressWS(location);

		if (StringUtils.equals(response.getStatus(), "failed")){
			// 3rd Party WS call failed due to some exception....
			LOGGER.warn(response.getValidatorName() + " - " + response.getErrMsg());
			return "FAILURE~Unknown error! Please try again setting zipcode... If error perists then please contact helpdesk.";
		}

		List<Location> validAddressList = response.getList();


		if (validAddressList.size() > 0) {
			// check for Matching address...
			for (Location validAddress : validAddressList) {
				if (StringUtils.equalsIgnoreCase(validAddress.getAddress1(), location.getAddress1())
						&& StringUtils.equalsIgnoreCase(validAddress.getAddress2(), location.getAddress2())
						&& StringUtils.equalsIgnoreCase(validAddress.getCity(), location.getCity())
						&& StringUtils.equalsIgnoreCase(validAddress.getState(), location.getState())
						&& (validAddress.getZip().equalsIgnoreCase(location.getZip()))){
					// Spec2.1.2:- User enters an address that is an exact match to an address in the database
					LOGGER.info("Matching adress found from web service. Skipping opening Address Selection iFrame (NEW).");
					return "MATCH_FOUND~" + validAddress.getLat() + "~" +validAddress.getLon() +  "~" +validAddress.getRdi() + "~" +validAddress.getCounty();
				}
			}

			//LOGGER.info(Arrays.toString(validAddressList.toArray()));

			// Spec2.1.3:- User enters an address that is close to an address in the database, but not an exact match
			// take first two valid locations from list (this is as per spec, take only first two values)...
			List<Location> finalValidAddressList = new ArrayList<Location>(2);
			finalValidAddressList.add(validAddressList.get(0));
			//HIX-34546 if we get similar address(i.e Address1,City,State and Zip are same ) need to consider the address is similar.
			//As we are considering first two address for all the case 
			if (validAddressList.size() > 1 ) {
				Location firstLocation = validAddressList.get(0);
				Location secondLocation = validAddressList.get(1);
				if(StringUtils.equalsIgnoreCase(firstLocation.getAddress1(), secondLocation.getAddress1())
						&& StringUtils.equalsIgnoreCase(firstLocation.getAddress2(), secondLocation.getAddress2())
						&& StringUtils.equalsIgnoreCase(firstLocation.getCity(), secondLocation.getCity())
						&& StringUtils.equalsIgnoreCase(firstLocation.getState(), secondLocation.getState())
						&& (firstLocation.getZip().equalsIgnoreCase(secondLocation.getZip()))){
					// Skip adding the duplicate.
				}else{
					finalValidAddressList.add(secondLocation);
				}
				
				
			}
			
			request.getSession().setAttribute("validateAddress", location);
			request.getSession().setAttribute("validAddressListS", finalValidAddressList);
			request.getSession().setAttribute("ids", ids);
			return "SUCCESS";
		}else{
			LOGGER.info("No Matching adress found from web service. Skipping opening Address Selection iFrame (NEW).");
		}

		// Spec2.1.4:- User enters an address for which no close match is found in the database
		return "FAILURE~The address you entered is not in the postal database.\nPlease check it for accuracy.";
	}


	private AddressValidatorResponse invokeAddressWS(Location location) {
		return addressValidatorService.validateAddress(location);
	}
	
	private Location formLocation(String addressLine1, String addressLine2, String city, String state, Integer zip) {
		Location location = new  Location();
		location.setAddress1(addressLine1);
		location.setAddress2(addressLine2);
		location.setCity(city);
		location.setState(state);
		location.setZip(String.valueOf(zip));
		return location;
	}

	@RequestMapping(value = "/platform/address/viewvalidaddressnew", method = RequestMethod.GET)
	public String getAddressList(Model model, HttpServletRequest request) {

		Location location = (Location) request.getSession().getAttribute("validateAddress");
		String ids = (String) request.getSession().getAttribute("ids");
		@SuppressWarnings("unchecked")
		List<Location> validAddressListS = (List<Location>) request.getSession().getAttribute("validAddressListS");

		model.addAttribute("validAddressList", validAddressListS);
		model.addAttribute("enteredLocation", location);
		model.addAttribute("ids", ids);

		return "platform/address/viewvalidaddressnew";
	}

	@RequestMapping(value = "/platform/address/viewvalidaddressnew", method = RequestMethod.POST)
	@ResponseBody public String viewvalidaddress(Model model, HttpServletRequest request) {

		model.addAttribute("page_title", "GetInsured Health Exchange: Address Verification page version 2");

		return "SUCCESS";
	}
	
	public String getCaAddressList(Model model, HttpServletRequest request) {

		Location location = (Location) request.getSession().getAttribute("validateAddress");
		String ids = (String) request.getSession().getAttribute("ids");
		@SuppressWarnings("unchecked")
		List<Location> validAddressListS = (List<Location>) request.getSession().getAttribute("validAddressListS");

		model.addAttribute("validAddressList", validAddressListS);
		model.addAttribute("enteredLocation", location);
		model.addAttribute("ids", ids);

		return "ssap/addressmatch";
	}

}
