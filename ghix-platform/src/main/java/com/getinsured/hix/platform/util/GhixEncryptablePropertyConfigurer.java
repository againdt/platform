package com.getinsured.hix.platform.util;

import java.util.Properties;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.model.ConfigPageData;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
@DependsOn("ghixSecretKey" )
public class GhixEncryptablePropertyConfigurer extends PropertyPlaceholderConfigurer {
	public static final String ENC_START_TAG = "ENC(";
	public static final String ENC_END_TAG = ")";
	public static Logger logger = LoggerFactory.getLogger(GhixEncryptablePropertyConfigurer.class);
	private int springSystemPropertiesMode = SYSTEM_PROPERTIES_MODE_OVERRIDE;
	
	public GhixEncryptablePropertyConfigurer(){

	}

	@Override
	protected String convertPropertyValue(final String originalValue) {
		if(originalValue == null){
			return null;
		}
		if(originalValue.startsWith(ENC_START_TAG) && originalValue.endsWith(ENC_END_TAG)){
			try {
				return GhixAESCipherPool.decrypt(getInnerEncryptedValue(originalValue));
			} catch (Exception e) {
				throw new GIRuntimeException("Failed to decrypt the property ["+originalValue+"]"+e.getMessage(),e);
			}
		}
		return originalValue;
	}


	@Override
	protected String resolveSystemProperty(final String key) {
		return convertPropertyValue(super.resolveSystemProperty(key));
	}

	private static String getInnerEncryptedValue(String value) {
		return value.substring(ENC_START_TAG.length(),
				(value.length() - ENC_END_TAG.length()));
}
	@Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) throws BeansException {
        super.processProperties(beanFactory, props);
      
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String valueStr = resolvePlaceholder(keyStr, props, springSystemPropertiesMode);
            
            try {
				ConfigPageData.validateConfigProperty(keyStr, valueStr);
			} catch (InvalidOperationException | ConfigValidationException e) {
				throw new GIRuntimeException("Failed to validate the property ["+keyStr+"] "+e.getMessage(),e);
			}
        }
        logger.info("Validation successful for all configuration properties");
		
    }


}
