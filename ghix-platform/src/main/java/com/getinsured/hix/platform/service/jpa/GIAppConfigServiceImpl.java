package com.getinsured.hix.platform.service.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.model.ConfigPageData;
import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.model.Tenant2TenantDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.repository.GIAppConfigRepository;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.service.GIAppConfigService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Service("giAppConfigService")
public class GIAppConfigServiceImpl implements GIAppConfigService {
    
    @Autowired
    private GIAppConfigRepository gIAppConfigRepository;
    
    @Autowired
    private TenantRepository tenantRepository;  
    
    private TenantDTO defaultTenant;
    
    private Logger logger  = LoggerFactory.getLogger(GIAppConfigServiceImpl.class);
    
    private static  boolean tenantAware = "on".equalsIgnoreCase(System.getProperty("tenant.enabled"));
    
    @PersistenceUnit private EntityManagerFactory emf;
   
    private static final Logger LOGGER = LoggerFactory.getLogger(GIAppConfigServiceImpl.class);
    
    @PostConstruct
    private void initDefaultTenant() {
    	try{
    		defaultTenant = Tenant2TenantDTO.Current.convert(tenantRepository.findByCode("GINS"));
    	}catch(Exception e){
    		logger.error("Exception ",e);
    	}
    }

    @Override
    @Cacheable(value="giappconfig", key="#propertyName", unless="#result == null")
    public Object getPropertyValue(String propertyName) {
    	try{
    		return validateProperty(StringUtils.EMPTY, gIAppConfigRepository.findByPropertyKey(propertyName));
    	}catch(Exception e){
    		logger.error("Error retreiving property:"+propertyName, e);
    	}
    	return null;
    }

    @Override																				
    @Cacheable(value="giappconfig", key="#propertyName + '_' + #tenantId", condition="#tenantId != null", unless="#result == null")
    public Object getPropertyValue(String propertyName, Long tenantId) {
    	// To handle scenarios where getPropertyValues is called for tenant.enabled = on but there is no tenant id in the context (and it is not needed? HIX-73324)
    	Long actualTenantId = tenantId == null ? defaultTenant.getId() : tenantId;
    	try{
    	return validateProperty(StringUtils.EMPTY, gIAppConfigRepository.findByPropertyKeyAndTenantId(propertyName, actualTenantId));
    	}catch(Exception e){
    		logger.error("Error retreiving property:"+propertyName, e);
    	}
    	return null;
    }
    
    @Override
	@Cacheable(value="giappconfig", key="#propertyName + '_map'", unless="#result == null")
    public Map<String, String> getPropertyValues(String propertyName) {
    	try{
    	return convertToMap(gIAppConfigRepository.findAllByPropertyKeyStartingWith(propertyName));
    	}catch(Exception e){
    		logger.error("Error retreiving property:"+propertyName, e);
    	}
    	return null;
    }
    
    @Override
    @Cacheable(value="giappconfig", key="#propertyName + '_' + #tenantId+ '_map'", condition="#tenantId != null", unless="#result == null")
    public Map<String, String> getPropertyValues(String propertyName, Long tenantId) {
    	// To handle scenarios where getPropertyValues is called for tenant.enabled = on but there is no tenant id in the context (and it is not needed? HIX-73324)
    	Long actualTenantId = tenantId == null ? defaultTenant.getId() : tenantId;
    	try{
    	return convertToMap(gIAppConfigRepository.findAllByPropertyKeyStartingWithAndTenantId(propertyName, actualTenantId));
    	}catch(Exception e){
    		logger.error("Error retreiving property:"+propertyName, e);
    	}
    	return null;
    }

    public GIAppConfigRepository getGIAppConfigRepository() {
        return gIAppConfigRepository;
    }
    
    public void setGIAppConfigRepository(GIAppConfigRepository gIAppConfigRepository) {
        this.gIAppConfigRepository = gIAppConfigRepository;
    }

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GIRuntimeException.class)
	public void addTenantAppConfigurations(Long tenantId) {
		if (tenantId != null) {
			String queryStr = "INSERT"+
					" INTO GI_APP_CONFIG"+
					" ("+
					" ID,"+
					" PROPERTY_KEY,"+
					" PROPERTY_VALUE,"+
					" DESCRIPTION,"+
					" CREATED_BY,"+
					" CREATION_TIMESTAMP,"+
					" LAST_UPDATED_BY,"+
					" LAST_UPDATE_TIMESTAMP,"+
					" TENANT_ID"+
					" )"+
					" ("+
					" SELECT GI_APP_CONFIG_SEQ.nextval,"+
					" gi.PROPERTY_KEY,"+
					" gi.PROPERTY_VALUE,"+
					" gi.DESCRIPTION,"+
					" (SELECT id from users where username = 'exadmin@ghix.com' and tenant_id = (select id from tenant where code = 'GINS')),"+
					" CURRENT_TIMESTAMP,"+
					" (SELECT id from users where username = 'exadmin@ghix.com' and tenant_id = (select id from tenant where code = 'GINS')), "+
					" CURRENT_TIMESTAMP, :tenantId"+
					" FROM GI_APP_CONFIG gi, tenant t"+
					" where t.name = 'GetInsured'"+
					" AND gi.TENANT_ID = t.ID )";
			EntityManager em = null;
			Query query = null;
			try{
				em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}
				query = em.createNativeQuery(queryStr);
				query.setParameter("tenantId", tenantId);
				int count = query.executeUpdate();
				LOGGER.info("Number of records inserted in GI_APP_CONFIG: "+count);
				em.getTransaction().commit();
			}catch(Exception e){
				LOGGER.error("Failed to insert records in GI_APP_CONFIG for a tenant",e);
				if(em.getTransaction().isActive()){
					em.getTransaction().rollback();
				}
				throw new GIRuntimeException(e);
			}finally{
				if(em !=null && em.isOpen()){
					em.clear();
					em.close();
				}
			}
		}

	}

	private String validateProperty(String propertyValue, GIAppProperties giAppProperty) {
		if(null != giAppProperty) {
    		propertyValue = giAppProperty.getPropertyValue();
            try {
				ConfigPageData.validateConfigProperty(giAppProperty.getPropertyKey(), giAppProperty.getPropertyValue());
			} catch (InvalidOperationException | ConfigValidationException e) {
				throw new GIRuntimeException("Failed to validate the property ["+ giAppProperty.getPropertyKey() +"] ", e);
			}
    	}
		return propertyValue;
	}

	private Map<String, String> convertToMap(List<GIAppProperties> giAppProperties) {
		Map<String, String> propertiesMap = new HashMap<>();
		if(giAppProperties != null) {
			for (GIAppProperties giAppProperty : giAppProperties) {
				propertiesMap.put(giAppProperty.getPropertyKey(), giAppProperty.getPropertyValue());
			}
		}
    	return propertiesMap;
	}
	
	public void refresh(){
		// Not doing anything, refresh is implemented by underlying caching framework
	}
}
