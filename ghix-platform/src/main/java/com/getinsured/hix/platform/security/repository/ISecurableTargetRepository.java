package com.getinsured.hix.platform.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.SecurableTarget;

@Transactional(readOnly = true)
public interface ISecurableTargetRepository extends JpaRepository<SecurableTarget, Integer> {

	@Query("SELECT scblTarget FROM SecurableTarget scblTarget where targetId = :targetId and targetName= :targetName")
	public SecurableTarget findByTargetIdAndTargetType(@Param("targetId") Integer targetId,@Param("targetName") SecurableTarget.TargetName targetName);
	
}
