/**
 * 
 */
package com.getinsured.hix.platform.comment.service;

import java.util.List;

import com.getinsured.hix.model.Comment;

/**
 * @author panda_p
 *
 */
public interface CommentService {
	
	Comment saveComment(Comment comment);
	
	List<Comment> findByCommentTargetId(Integer commentTargetId);

	Comment findCommentById(Integer id);
	
	void deleteComment(Integer commentId);

	String getCommentTextById(Integer commentId);
}
