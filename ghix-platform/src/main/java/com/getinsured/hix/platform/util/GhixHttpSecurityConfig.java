package com.getinsured.hix.platform.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import com.getinsured.platform.config.bootstrap.UrlSecurityConfiguration;

/**
 * Provides utility method to check if URL is part of Unsecure list or not.
 */
@Component
public class GhixHttpSecurityConfig
{
  private static Logger log = LoggerFactory.getLogger(GhixHttpSecurityConfig.class);
  private Set<String> unsecuredPaaths = new HashSet<String>();

  /**
   * Accepts {@code /path/page?params} {@code /path} or {@code http[s]://domain[:port]/path/page?params} or {@code http[s]://domain[:port]/page}
   * and returns true if parsed path is not added to UNSERCURED url list.
   *
   * @param urlString path without protocol, port and domain or something like {@code http[s]://domain[:port]/path}
   * @return true if URL is protected, false if URL added to unsecured list {@link UrlSecurityConfiguration#UNSECURED_URL_LIST}
   */
  public boolean isUrlProtected(String urlString)
  {
	  
    if(urlString == null)
    {
      if(log.isWarnEnabled())
      {
        log.warn("NULL url given to isUrlProtected({}) method, returning TRUE", urlString);
      }

      return true;
    }
    
    try
    {
      String path = null;

      // If we have full url starts with 'http' or 'https', get only path
      if(urlString.startsWith("http"))
      {
        URL url = new URL(urlString);
        path = url.getPath();
      }
      // If we don't start with /, add it
      else if(!urlString.startsWith("/"))
      {
        path = "/" + urlString;
      }
      // Otherwise assume that /path was only passed.
      else
      {
        path = urlString;
      }
      if(unsecuredPaaths.contains(path)) {
    	  return false;
      }
      PathMatcher pathMatcher = new AntPathMatcher();
      
      if(log.isTraceEnabled())
      {
        log.trace("Checking if url path [{}] is secure or not", path);
      }

      for(String permitAllPath : UrlSecurityConfiguration.UNSECURED_URL_LIST)
      {
        // if match found, it means URL is not secure because it was added to UNSECURED_URL_LIST.
        if(pathMatcher.match(permitAllPath, path) || pathMatcher.match("/hix" + permitAllPath, path))
        {
         this.unsecuredPaaths.add(path);
          return false;
        }
      }
    }
    catch(MalformedURLException e)
    {
      if(log.isErrorEnabled())
      {
        log.error("Given URL had protocol (http|https) but we were not able to parse it because it's invalid, marking URL as secure! url: [{}]", urlString);
        log.error("Could not construct java.net.URL from given URL string", e);
      }
      // If exception occurred, it means URL had http or https protocol, but was not valid.
      // Let's be on safe side and return true meaning that this URL is protected. Let developers handle it.
      return true;
    }

    // If path didn't match unsecure URL list, and exception didn't happen, it means URL must be protected.
    return true;
  }
}
