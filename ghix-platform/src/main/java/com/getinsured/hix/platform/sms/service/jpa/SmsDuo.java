package com.getinsured.hix.platform.sms.service.jpa;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.duosecurity.client.Http;
import com.getinsured.hix.platform.sms.SmsResponse;
import com.getinsured.hix.platform.sms.service.SmsService;
import com.getinsured.hix.platform.util.DuoSecurityUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;

public class SmsDuo implements SmsService {

	private static final String OUTBOUND = "outbound";
	private static final int BODY_MAX_LIMIT = 160;
	private static final String PROVIDER = "DUOSECURITY";

	private static final Logger LOGGER = LoggerFactory.getLogger(SmsDuo.class);

	private final String host;
	private final String integrationKey;
	private final String secretKey;
	private final String fromPhone;

	/**
	 * Constructor to get handle for Duo Security Sms and call Provider.
	 * 
	 * @param host
	 * @param integrationKey
	 * @param secretKey
	 */

	public SmsDuo(String host, String integrationKey, String secretKey, String fromPhone) {
		this.host=host;
		this.integrationKey= integrationKey;
		this.secretKey = secretKey;
		this.fromPhone = fromPhone;
	}

	@Override
	public SmsResponse call(String toPhone, String message,String pin) {
		String phoneNo;

		if (StringUtils.isEmpty(toPhone)) {
			throw new IllegalArgumentException("To Phone number is required");
		}
		
		if(!toPhone.contains("+")){
			 phoneNo="+1"+toPhone;
		}
		else{
			phoneNo=toPhone;
		}

		if (StringUtils.isEmpty(message)){
			throw new IllegalArgumentException("Message body is required");
		}

		if (StringUtils.isEmpty(pin)){
			throw new IllegalArgumentException("Pin is required");
		}

		if (message.length() > BODY_MAX_LIMIT){
			throw new IllegalArgumentException("The message body exceeds 160 character limit");
		}
		
		/** put pauses in the pin */
		char[] pinArray = pin.toCharArray();
		StringBuilder pausedPin = new StringBuilder();
		for (char c : pinArray) {
			pausedPin.append(c).append("..");
		}
		SmsResponse smsResponse;
		Date date = new TSDate();
		// Make API call for phone verification
		try{
/*			Http request = new Http("POST",host, "/verify/v1/call.json");
			request.addParam("phone",phoneNo);
			request.addParam("message", message);
			request.addParam("pin",pausedPin.toString());
			request.signRequest(integrationKey,secretKey,1);

			request.executeRequest();
			request = null; // cleanup the request object
*/
			HashMap<String, String> postParams = new HashMap<String, String>();
			postParams.put("phone",phoneNo);
			postParams.put("message",message);
			postParams.put("pin",pausedPin.toString());
			HashMap<String, String> duoAuthHeaders = DuoSecurityUtils. getAuthorizationHeader("POST",host,"/verify/v1/call.json",integrationKey,secretKey,postParams);
			HIXHTTPClient request1 = new HIXHTTPClient();
			request1.postFormData("https://api-053e5501.duosecurity.com/verify/v1/call.json", duoAuthHeaders,postParams);

			smsResponse = new SmsResponse(PROVIDER, toPhone, fromPhone, message, "SENDING", "", "messageSid", OUTBOUND, date, date, null);
		}
		catch(Exception e) {
			LOGGER.error("Error making call - ", e);
			smsResponse = new SmsResponse(PROVIDER, toPhone, fromPhone, message, "FAILURE", "Unknown error occured. Please try re-sending...", "", OUTBOUND, date, date, null);
		}

		return smsResponse;
	}


	@Override
	public SmsResponse send(String toPhone, String body,String pin) {
		String phoneNo;

		if (StringUtils.isEmpty(toPhone)) {
			throw new IllegalArgumentException("To Phone number is required");
		}
		
		if(!toPhone.contains("+")){
			 phoneNo="+1"+toPhone;
		}
		else{
			phoneNo=toPhone;
		}

		if (StringUtils.isEmpty(body)){
			throw new IllegalArgumentException("Message body is required");
		}

		if (StringUtils.isEmpty(pin)){
			throw new IllegalArgumentException("Pin is required");
		}

		if (body.length() > BODY_MAX_LIMIT){
			throw new IllegalArgumentException("The message body exceeds 160 character limit");
		}


		SmsResponse smsResponse;
		Date date = new TSDate();
		// Make API call for sms verification
		try{
/*			Http request = new Http("POST",host, "/verify/v1/sms.json");
			request.addParam("phone",phoneNo);
			request.addParam("message", body);
			request.addParam("pin",pin);
			request.signRequest(integrationKey,secretKey,1);

			request.executeRequest();*/
			
			HashMap<String, String> postParams = new HashMap<String, String>();
			postParams.put("phone",phoneNo);
			postParams.put("message",body);
			postParams.put("pin",pin);
			HashMap<String, String> duoAuthHeaders = DuoSecurityUtils. getAuthorizationHeader("POST",host,"/verify/v1/sms.json",integrationKey,secretKey,postParams);
			HIXHTTPClient request1 = new HIXHTTPClient();
			request1.postFormData("https://api-053e5501.duosecurity.com/verify/v1/sms.json", duoAuthHeaders,postParams);

			
			smsResponse = new SmsResponse(PROVIDER, toPhone, fromPhone, body, "SENDING", "", "messageSid", OUTBOUND, date, date, null);
		}
		catch(Exception e) {
			LOGGER.error("Error sending sms - ", e);
			smsResponse = new SmsResponse(PROVIDER, toPhone, fromPhone, body, "FAILURE", "Unknown error occured. Please try re-sending...", "", OUTBOUND, date, date, null);
		}

		return smsResponse;
	}

}
