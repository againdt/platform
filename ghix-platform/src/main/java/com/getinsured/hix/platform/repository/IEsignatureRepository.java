package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Esignature;

public interface IEsignatureRepository extends JpaRepository<Esignature, Integer> {
	// Find esign by AccountUser
	public List<Esignature> findByUser(AccountUser user);
	
	// Find esign by module name and reference id. This will return only one record (ROWNUM = 1).
	@Query("SELECT esign FROM Esignature esign WHERE (esign.moduleName = :moduleName) AND (esign.refId = :refId) AND ROWNUM = 1")
    public Esignature findByModuleRefId(@Param("moduleName") String moduleName, @Param("refId") Integer refId);
	
	@Query("SELECT esign FROM Esignature esign WHERE (esign.firstName = :fName) AND (esign.lastName = :lName) ORDER BY esign.esignDate DESC")
    public List<Esignature> findByFnameLname(@Param("fName") String fName, @Param("lName") String lName);
	

	@Query("SELECT esign FROM Esignature esign WHERE (esign.id = :id)")
    public Esignature findById(@Param("id") Integer id);
}
