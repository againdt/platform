package com.getinsured.hix.platform.repository;

import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Repository
@Transactional
public class PropertiesRepository {
    private static final Logger log = LoggerFactory.getLogger(PropertiesRepository.class);

    @Autowired
    private IPropertiesRepository propertiesRepository;
    public static final String PAGE_NUMBER = "pageNumber";
    public static final Integer PAGE_SIZE = 10;

    public void flush()
    {
        propertiesRepository.flush();
    }

    @Cacheable(value = "giappconfig", key = "#root.methodName + '_all'", unless = "#result == null")
    public List<GIAppProperties> findAll()
    {
        return propertiesRepository.findAll();
    }

    @Cacheable(value = "giappconfig", key = "#root.methodName + '_' + #propertyKey", unless = "#result == null")
    public GIAppProperties findBypropertyKey(final String propertyKey)
    {
        if(log.isDebugEnabled()) {
            log.debug("findBypropertyKey: {}", propertyKey);
        }

        return propertiesRepository.findBypropertyKey(propertyKey);
    }

    @CacheEvict(value = "giappconfig", allEntries =  true)
    @Transactional
    public GIAppProperties saveAndFlush(final GIAppProperties giAppProperties)
    {
        GIAppProperties dbProperty = propertiesRepository.findById(giAppProperties.getId());

        if(dbProperty == null)
        {
            if(log.isErrorEnabled())
            {
                log.error("Tried save GIAppProperties object with invalid id: {}; tenantId: {}, it was NOT found in database. TenantContext: tenantId: {}",
                    giAppProperties.getId(), giAppProperties.getTenantId(), ((TenantContextHolder.getTenant() != null) ? TenantContextHolder.getTenant().getId() : TenantContextHolder.getTenant()));
            }
        }
        else
        {
        giAppProperties.setTenantId(dbProperty.getTenantId());
        giAppProperties.setCreatedBy(dbProperty.getCreatedBy());
        giAppProperties.setCreationTimestamp(dbProperty.getCreationTimestamp());
        }

        if(log.isDebugEnabled())
        {
            log.debug("saveAndFlush property by key: {} => {}", giAppProperties.getPropertyKey(), giAppProperties.getPropertyValue());
        }

        return propertiesRepository.saveAndFlush(giAppProperties);
    }


    /* @CacheEvict(
            value = "giappconfig",
            key = "#giAppProperties.propertyKey + '_' + #giAppProperties.tenantId",
            condition = "#giAppProperties != null",
            beforeInvocation = true
    )
    @CachePut(
            value = "giappconfig",
            key = "#giAppProperties.propertyKey + '_' + #giAppProperties.tenantId",
            condition = "#giAppProperties != null && #giAppProperties.propertyKey != null",
            unless = "#result == null"
    )
    /*
    @Cacheable(value = "giappconfig",
            key = "#giAppProperties.propertyKey + '_' + #giAppProperties.tenantId",
            condition = "#giAppProperties != null && #giAppProperties.propertyKey != null",
            unless = "#result == null")
    */

    /**
     * Deprecated, use {{@link #saveAndFlush(GIAppProperties)}}.
     *
     * @param giAppProperties
     * @return
     */
    @Deprecated
    @CacheEvict(value = "giappconfig", allEntries =  true)
    @Transactional
    public GIAppProperties save(final GIAppProperties giAppProperties)
    {
        if (log.isDebugEnabled()) {
            log.debug("save property by key: {} => {}", giAppProperties.getPropertyKey(), giAppProperties.getPropertyValue());
        }

        return propertiesRepository.save(giAppProperties);
    }

    /**
     * Returns list of properties by search term.
     *
     * @param propertyKey property key
     * @return {@link List} of {@link GIAppProperties} objects.
     * @deprecated Use {@link #getPropertyKeyList(String, Long)}.
     */
    public List<GIAppProperties> getPropertyKeyList(final String propertyKey)
    {
        return propertiesRepository.getPropertyKeyList(propertyKey);
    }

    /**
     * Used by property search.
     *
     * @param propertyKey property key to search for
     * @param tenantId tenant id.
     * @return list of properties that matched search string.
     */
    public List<GIAppProperties> searchByPropertyKey(final String section, final String propertyKey, final Long tenantId)
    {
        return propertiesRepository.searchByPropertyKey(section, propertyKey, tenantId);
    }

    /**
     * Used by property search in ID/SHOP.
     * <p>
     *   For PHIX environment, use {@link #searchByPropertyKey(String, String, Long)} which takes
     *   tenant id.
     * </p>
     *
     * @param propertyKey property key to search for
     * @return list of properties that matched search string.
     * @see {@link #searchByPropertyKey(String, String, Long)} that takes tenant id information as well.
     */
    public List<GIAppProperties> searchByPropertyKey(final String section, final String propertyKey)
    {
        return propertiesRepository.searchByPropertyKey(section, propertyKey);
    }

    /**
     * Returns list of properties by key.
     *
     * @param propertyKey property key
     * @return {@link List} of {@link GIAppProperties} objects.
     */
    @Cacheable(value = "giappconfig",
        key = "'tenant_' + #tenantId + '_' + #root.methodName + '_' + #propertyKey",
            condition = "#propertyKey != null",
            unless = "#result == null")
    public List<GIAppProperties> getPropertyKeyList(final String propertyKey, final Long tenantId)
    {
        return propertiesRepository.getPropertyKeyList(propertyKey, tenantId);
    }

    /**
     * Gets property list with {@link Pageable} argument.
     *
     * @param propertyKey property key
     * @param pageable {@link Pageble} interface.
     * @return {@link Page} object with list of {@link GIAppProperties}
     * @deprecated use {@link #getPropertyKeyList(String, Pageable, Long)}
     */
    @Deprecated
    public Page<GIAppProperties> getPropertyKeyList(final String propertyKey, final Pageable pageable)
    {
        return propertiesRepository.getPropertyKeyList(propertyKey, pageable);
    }


    /**
     * Gets property list for given property key and page.
     *
     * @param propertyKey will find properties starting with this string.
     * @param pageable {@link Pageable} object.
     * @param tenantId tenant id.
     * @return {@link Page<GIAppProperties>} page with found {@link GIAppProperties} objects.
     */
    @Cacheable(value = "giappconfig",
        key = "'tenant_' + #tenantId + '_key_' + #propertyKey + '_page_' + #pageable.pageNumber",
            condition = "#propertyKey != null",
            unless = "#result == null")
    public Page<GIAppProperties> getPropertyKeyList(final String propertyKey, final Pageable pageable, Long tenantId)
    {
        return propertiesRepository.getPropertyKeyList(propertyKey, pageable, tenantId);
    }

    public Pageable getPagingAndSorting(final Model model, final HttpServletRequest request) {
        // get the page number if exists else set default page number to 1 for fetching the data.
        int pageNumber = 0;

        if (StringUtils.isNumeric(request.getParameter(PAGE_NUMBER)))
        {
            try
            {
            pageNumber = Integer.parseInt(request.getParameter(PAGE_NUMBER));
        }
            catch(NumberFormatException e)
            {
                /* ignore */
            }
        }

        if (pageNumber < 1)
        {
            pageNumber = 1;
        }

        // create a PageRequest object for the specific page number and no.of.records to display on the page (page size).
        final Pageable pageable = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return pageable;
    }
}

