package com.getinsured.hix.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;


@Component
@DependsOn("platformConstants")
public class GhixPlatformKeystore {
	private static Logger LOGGER = LoggerFactory.getLogger(GhixPlatformKeystore.class);
	public static final String COMPONENT_NAME = "platformConstants";
	private static HashMap<String, String> aliasMapping = new HashMap<>();
	private static String defaultAlias = null;
	private static KeyStore platformKeyStore = null;
	private static String keyStorePassword = null;
	private static String keyStoreFileName = null;
	private static String keyStoreLocation = null;
	private static String ghixHome = null;
	private static String aliasMappingFile = null;
	private static HashMap<String, JKSKeyManager> samlKeyManagers;
	
	static {
		ghixHome  = System.getProperty("GHIX_HOME");
		keyStorePassword = GhixPlatformConstants.PLATFORM_KEYSTORE_PASS;
		keyStoreFileName = GhixPlatformConstants.PLATFORM_KEY_STORE_FILE;
		keyStoreLocation = GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION;
		Assert.notNull(keyStoreFileName,"Required property, Keystore file name not availale, please check the configuration, check for password and location as well");
		Assert.notNull(keyStorePassword,"Required property, Keystore password not availale, please check the configuration, check for file name and location as well");
		Assert.notNull(keyStoreLocation,"Required property, Keystore location name not availale, please check the configuration, check for password and file name as well");
		aliasMappingFile  = ghixHome+File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+"sslConf.xml";
		File f = new File(aliasMappingFile);
		boolean aliasMapExists = f.exists() && f.canRead();
		Assert.isTrue(aliasMapExists,"File:"+aliasMappingFile+" doesn't exists or not readable, please check");
		Assert.isTrue(loadAliasMapping(),"Failed to buold the alias map");
	}
	
	
	public String getDefaultAlias() {
		return defaultAlias;
	}
	
	private static synchronized void initKeyStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException{
		if(platformKeyStore != null){
			return;
		}
			File keyStoreFile = getKeyStoreFile();
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Reading keystore file:"+keyStoreFile.getAbsolutePath());
			}
			platformKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			
			FileInputStream instream = new FileInputStream(keyStoreFile);
			platformKeyStore.load(instream, GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray());
			LOGGER.info("Done creating trust store...");
			validateKeyStore(platformKeyStore);
		
	}

	/**
	 * Returns the private key from this key store for a given alias if exists.
	 * If no password is provided, keystore password will be used to retrieve the key
	 * @param alias
	 * @param password
	 * @return PrivateKey for a given alias
	 */
	public PrivateKey getPrivateKey(String alias, char[] password){
		char[] keyPass = null;
		PrivateKey aliasPrivateKey = null;
		if(alias == null || alias.equalsIgnoreCase("Default".intern())) {
			alias = this.getDefaultAlias();
		}
		if(password == null){
			keyPass = keyStorePassword.toCharArray();
		}else{
			keyPass = password;
		}
		InputStream instream = null;
		try{
			if(platformKeyStore == null){
				initKeyStore();
			}
			KeyStore.ProtectionParameter protParam =
			        new KeyStore.PasswordProtection(keyPass);
				// get private key
				KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry)
					platformKeyStore.getEntry(alias, protParam);
				if(pkEntry == null) {
					LOGGER.error("Failed to find the private key for alias:"+alias);
					return null;
				}
				aliasPrivateKey = pkEntry.getPrivateKey();
		}catch(Exception e){
			LOGGER.error("Failed to find the private key for alias:"+alias+" failed with:"+e.getMessage(),e);
			return null;
		}finally{
			IOUtils.closeQuietly(instream);
		}
		return aliasPrivateKey;
	}
	
	public PublicKey getPublicKey(String alias){
		Certificate cert = this.getCertificates(alias);
		if(cert != null){
			return cert.getPublicKey();
		}
		return null;
	}
	
	public String signData(byte[] data, String alias, char[] password) {
		PrivateKey signingKey = this.getPrivateKey(alias, password);
		if(signingKey == null){
			throw new GIRuntimeException("Signing failed, no private key available for alias \""+alias+"\"");
		}
		try {
			Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initSign(signingKey);
			signature.update(data);
			byte[] signedData = signature.sign();
			return Base64.encodeBase64String(signedData);
		} catch (NoSuchAlgorithmException | SignatureException| InvalidKeyException e) {
			LOGGER.error("Failed to Sign data with Error:"+e.getMessage());
			throw new GIRuntimeException("Failed to Sign the data",e);
		}
	}
	
	public String signData(byte[] data, String alias, char[] password, String algorithm) {
		PrivateKey signingKey = this.getPrivateKey(alias, password);
		if(signingKey == null){
			throw new GIRuntimeException("Signing failed, no private key available for alias \""+alias+"\"");
		}
		try {
			Signature signature = Signature.getInstance(algorithm);
            signature.initSign(signingKey);
			signature.update(data);
			byte[] signedData = signature.sign();
			return Base64.encodeBase64String(signedData);
		} catch (NoSuchAlgorithmException | SignatureException| InvalidKeyException e) {
			LOGGER.error("Failed to Sign data with Error:"+e.getMessage());
			throw new GIRuntimeException("Failed to Sign the data",e);
		}
	}
	
	public boolean verifySignature(byte[] dataToBeVerified, byte[] signature, String alias){
		PublicKey pubKey = this.getPublicKey(alias);
		if(pubKey == null){
			throw new GIRuntimeException("Can not verify signature, failed to find the public Key for alias \""+alias+"\"");
		}
		try {
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(pubKey);
			sig.update(dataToBeVerified);
			return sig.verify(signature);
		} catch (SignatureException | NoSuchAlgorithmException | InvalidKeyException e) {
			LOGGER.error("Error to verifying signature, failed with Error:"+e.getMessage());
			throw new GIRuntimeException("Error to verifying signature", e);
		}
	}
	
	public boolean verifySignature(byte[] dataToBeVerified, byte[] signature, PublicKey pubKey){
		if(pubKey == null){
			throw new GIRuntimeException("Can not verify signature, public Key not provided");
		}
		try {
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(pubKey);
			sig.update(dataToBeVerified);
			return sig.verify(signature);
		} catch (SignatureException | NoSuchAlgorithmException | InvalidKeyException e) {
			LOGGER.error("Error to verifying signature, failed with Error:"+e.getMessage());
			throw new GIRuntimeException("Error to verifying signature", e);
		}
	}
	
	public boolean decodeAndVerifySignature(byte[] dataToBeVerified, String base64EncodedSignature, String alias){
		PublicKey pubKey = this.getPublicKey(alias);
		if(pubKey == null){
			throw new GIRuntimeException("Can not verify signature, failed to find the public Key for alias \""+alias+"\"");
		}
		
		byte[] rawSignature = Base64.decodeBase64(base64EncodedSignature);
		try {
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(pubKey);
			sig.update(dataToBeVerified);
			return sig.verify(rawSignature);
		} catch (SignatureException | NoSuchAlgorithmException | InvalidKeyException e) {
			LOGGER.error("Error to verifying signature, failed with Error:"+e.getMessage());
			throw new GIRuntimeException("Error to verifying signature", e);
		}
	}
	
	public Certificate getCertificates(String aliasName) {
		Certificate certificate = null;
		try {
			if(platformKeyStore == null){
				initKeyStore();
			}
			Enumeration<String> aliases = platformKeyStore.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				if(!alias.equalsIgnoreCase(aliasName)){
					continue;
				}
				certificate = platformKeyStore.getCertificate(alias);
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected Exception reading key store", e);
			return null;
		}
		return certificate;
	}
	
	public String getEncodedCertificates(String aliasName) {
		Certificate certificate = null;
		try {
			if(platformKeyStore == null){
				initKeyStore();
			}
			Enumeration<String> aliases = platformKeyStore.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				if(!alias.equalsIgnoreCase(aliasName)){
					continue;
				}
				certificate = platformKeyStore.getCertificate(alias);
				byte[] encodedCert = certificate.getEncoded();
				return Base64.encodeBase64String(encodedCert);
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected Exception reading key store", e);
		}
		return null;
	}
	
	public String encyptEncodeUsingPublicKey(String alias, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher pkCipher = Cipher.getInstance("RSA");
		PublicKey key = this.getPublicKey(alias);
		if(key == null){
			throw new GIRuntimeException("Failed to encrypt using public Key, alias \""+alias+"\" not found");
		}
		pkCipher.init(Cipher.ENCRYPT_MODE, key);
		pkCipher.update(data);
		byte[] out = pkCipher.doFinal();
		return Base64.encodeBase64String(out); // this will be a binary data, return as Base64 encoded
	}
	
	public byte[] encyptUsingPublicKey(String alias, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher pkCipher = Cipher.getInstance("RSA");
		PublicKey key = this.getPublicKey(alias);
		if(key == null){
			throw new GIRuntimeException("Failed to encrypt using public Key, alias \""+alias+"\" not found");
		}
		pkCipher.init(Cipher.ENCRYPT_MODE, key);
		pkCipher.update(data);
		return pkCipher.doFinal();
	}
	
	public byte[] decryptUsingPublicKey(String alias, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher pkCipher = Cipher.getInstance("RSA");
		PublicKey key = this.getPublicKey(alias);
		if(key == null){
			throw new GIRuntimeException("Failed to decrypt using public Key, alias \""+alias+"\" not found");
		}
		pkCipher.init(Cipher.DECRYPT_MODE, key);
		pkCipher.update(data);
		return pkCipher.doFinal();
	}
	
	public byte[] decryptBase64EncodedUsingPublicKey(String alias, String encryptedEncoded) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		byte[] input = Base64.decodeBase64(encryptedEncoded);
		Cipher pkCipher = Cipher.getInstance("RSA");
		PublicKey key = this.getPublicKey(alias);
		if(key == null){
			throw new GIRuntimeException("Failed to decrypt using public Key, alias \""+alias+"\" not found");
		}
		pkCipher.init(Cipher.DECRYPT_MODE, key);
		pkCipher.update(input);
		return pkCipher.doFinal();
	}
	
	public byte[] decryptBase64EncodedUsingPrivateKey(String alias,char[] password, String encryptEncoded) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		byte[] input = Base64.decodeBase64(encryptEncoded); // Assuming input is Base64 Encoded
		Cipher prCipher = Cipher.getInstance("RSA");
		PrivateKey key = this.getPrivateKey(alias,password);
		if(key == null){
			throw new GIRuntimeException("Failed to decrypt using private Key, alias \""+alias+"\" not found");
		}
		prCipher.init(Cipher.DECRYPT_MODE, key);
		prCipher.update(input);
		return prCipher.doFinal();
	}
	
	public byte[] decryptUsingPrivateKey(String alias,char[] password, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher prCipher = Cipher.getInstance("RSA");
		PrivateKey key = this.getPrivateKey(alias,password);
		if(key == null){
			throw new GIRuntimeException("Failed to decrypt using private Key, alias \""+alias+"\" not found");
		}
		prCipher.init(Cipher.DECRYPT_MODE, key);
		prCipher.update(data);
		return prCipher.doFinal();
	}
	
	public byte[] encryptUsingPrivateKey(String alias,char[] password, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher prCipher = Cipher.getInstance("RSA");
		PrivateKey key = this.getPrivateKey(alias,password);
		if(key == null){
			throw new GIRuntimeException("Failed to encrypt using private Key, alias \""+alias+"\" not found");
		}
		prCipher.init(Cipher.ENCRYPT_MODE, key);
		prCipher.update(data);
		return prCipher.doFinal();
	}
	
	public byte[] encryptEncodeUsingPrivateKey(String alias,char[] password, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher prCipher = Cipher.getInstance("RSA");
		PrivateKey key = this.getPrivateKey(alias,password);
		if(key == null){
			throw new GIRuntimeException("Failed to encrypt using private Key, alias \""+alias+"\" not found");
		}
		prCipher.init(Cipher.ENCRYPT_MODE, key);
		prCipher.update(data);
		return Base64.encodeBase64(prCipher.doFinal())	;
	}
	
	public Map<String,Certificate> getAvailableCertificates() {
		Certificate certificate = null;
		HashMap<String,Certificate> list = new HashMap<String,Certificate>();
		try {
			if(platformKeyStore == null){
				initKeyStore();
			}
			Enumeration<String> aliases = platformKeyStore.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				certificate = platformKeyStore.getCertificate(alias);
				if(certificate != null){
					list.put(alias, certificate);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected Exception reading key store", e);
			return null;
		}
		return list;
	}
	
	private static File getKeyStoreFile(){
		LOGGER.info("Keystore Location:"+keyStoreLocation);
		File keyStoreDir = new File(keyStoreLocation);
		File keyStore = new File(keyStoreDir,keyStoreFileName);
		if(!keyStore.exists() || !keyStore.isFile()){
			throw new GIRuntimeException("Key store ["+keyStore.getAbsolutePath()+"] does not exist Or its not a file");
		}
		return keyStore;
	}
	
	/**
	 * Checks if configured aliases have a coressponding private key available in the supplied keystore
	 * @param trustStore
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws UnrecoverableEntryException
	 */
	private static void validateKeyStore(KeyStore trustStore) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException{
		Collection<String> requiredAliases = aliasMapping.values();
		Enumeration<String> aliases = trustStore.aliases();
		String tmp = null;
		while(aliases.hasMoreElements()){
			tmp = aliases.nextElement();
			if(requiredAliases.contains(tmp)){
				
				//Try loading the key
				KeyStore.ProtectionParameter protParam =
			        new KeyStore.PasswordProtection(GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray());
				// get private key
				KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry)
					trustStore.getEntry(tmp, protParam);
				PrivateKey aliasPrivateKey = pkEntry.getPrivateKey();
				if(aliasPrivateKey == null){
					LOGGER.error("Alias:"+tmp+" is expected to have a coressponding private key entry, ....... false");
					throw new RuntimeException("No private key entry found for alias:"+tmp);
				}
				LOGGER.info("Alias:"+tmp+" is expected to have a coressponding private key entry, ....... true");
			}
		}
		// finally check if Keystore has all the aliases we have configured
		for(String requiredOne:requiredAliases){
			if(!trustStore.containsAlias(requiredOne)){
				LOGGER.error("Alias:"+requiredOne+" is expected to have a coressponding private key entry, ....... false, Ignoring");
			}
		}
		
	}
	
	private static boolean loadAliasMapping(){
		LOGGER.info("Loading alias mapping from file:"+aliasMappingFile);
		Document doc = null;
		InputStream is = null;
		try {
			DocumentBuilderFactory factory = Utils.getDocumentBuilderFactoryInstance();
			is = new FileInputStream(aliasMappingFile);
			doc = factory.newDocumentBuilder().parse(is);
			buildAliasMap(doc);
			return true;
		} catch (SAXException | IOException | ParserConfigurationException e) {
			LOGGER.error("Failed to load the attribute map", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
		return false;
	}
	
	public BasicX509Credential getSigningCredentials(String alias, String password){
		PrivateKeyEntry pk;
		try {
			if(platformKeyStore == null){
				initKeyStore();
			}
			pk = (PrivateKeyEntry) platformKeyStore.getEntry(alias, new KeyStore.PasswordProtection(password.toCharArray()));
			X509Certificate certificate = (X509Certificate) pk.getCertificate();
			BasicX509Credential credential = new BasicX509Credential();
			credential.setEntityCertificate(certificate);
			credential.setPrivateKey(pk.getPrivateKey());
			return credential;
		} catch (Exception e) {
			LOGGER.error("Failed to retrieve the X509 Signing credentials for alias:"+alias+" Password provided:"+((password == null || password.isEmpty())? "Null or empty":"Yes"));
		}
		return null;
	}
	
	/**
	 * This function assumes provate key password is same as the keystore password
	 * @param alias
	 * @return
	 */
	public static JKSKeyManager getSamlKeyManager(String alias){
		if(samlKeyManagers != null){
			samlKeyManagers = new HashMap<String,JKSKeyManager>();
		}
		if(samlKeyManagers.containsKey(alias)){
			return samlKeyManagers.get(alias);
		}
		try {
			if(platformKeyStore == null){
				initKeyStore();
			}
			if(platformKeyStore.containsAlias(alias) && platformKeyStore.isKeyEntry(alias)){
				Map<String, String> passwords = new HashMap<String, String>();
				passwords.put(alias, keyStorePassword);
				JKSKeyManager samlManager = new JKSKeyManager(platformKeyStore, passwords, alias);
				samlKeyManagers.put(alias, samlManager);
				return samlManager;
			}
		} catch (Exception e) {
			LOGGER.error("Failed with error:"+e.getMessage()+" while building the JKS KeyManager for alias:"+alias);
		}
		LOGGER.error("Failed to initialized SAML Key manager for alias:"+alias);
		return null;
	}
	
	/**
	 * Returns Spring framework wrapper for KeyManager
	 * @param alias -> Alias of the private key
	 * @param password -> password of the key entry if different than the keystore password
	 * @return
	 */
	public static JKSKeyManager getSamlKeyManager(String alias, String password){
		if(samlKeyManagers == null){
			samlKeyManagers = new HashMap<String,JKSKeyManager>();
		}
		if(samlKeyManagers.containsKey(alias)){
			return samlKeyManagers.get(alias);
		}
		try {
			if(platformKeyStore == null){
				initKeyStore();
			}
			if(platformKeyStore.containsAlias(alias) && platformKeyStore.isKeyEntry(alias)){
				Map<String, String> passwords = new HashMap<String, String>();
				passwords.put(alias, password);
				JKSKeyManager samlManager = new JKSKeyManager(platformKeyStore, passwords, alias);
				samlKeyManagers.put(alias, samlManager);
				return samlManager;
			}
		} catch (Exception e) {
			LOGGER.error("Failed with error:"+e.getMessage()+" while building the JKS KeyManager for alias:"+alias+" Password provided: "+((password == null || password.isEmpty()) ? "Empty or null":"Yes"));
		}
		LOGGER.error("Failed to initialized SAML Key manager for alias:"+alias);
		return null;
	}
	
	private static void buildAliasMap(Document doc) {
		NodeList aliasMaps = doc.getElementsByTagName("aliasMap");
		Node aliasMapNode = null;
		String alias = null;
		String server = null;
		String tmpVal = null;
		Node tmp = null;
		NamedNodeMap attributesList = null;
		int len = aliasMaps.getLength();
		for(int i = 0; i < len; i++){
			aliasMapNode = aliasMaps.item(i);
			attributesList = aliasMapNode.getAttributes();
			tmp = attributesList.getNamedItem("alias");
			if(tmp != null){
				alias = tmp.getNodeValue();
			}else{
				LOGGER.warn("No alias found for alias entry, skipping");
				continue;
			}
			tmp = attributesList.getNamedItem("server");
			if(tmp != null){
				server = tmp.getNodeValue();
			}else{
				LOGGER.warn("No server entry found for alias:"+alias+", skipping");
				continue;
			}
			
			tmp = attributesList.getNamedItem("default");
			if(tmp != null){
				tmpVal = tmp.getNodeValue();
				if(Boolean.valueOf(tmpVal)){// This alias is set to default
					if(defaultAlias != null && !alias.equals(defaultAlias)){
						throw new RuntimeException("Alias:"+defaultAlias+" is already set to be a default alias, \""+alias+"\" can not be set to be default alias, please provide only one");
					}
					defaultAlias  = alias;
					if(LOGGER.isInfoEnabled()){
						LOGGER.info("Marking \""+alias+"\" entry as default alias");
					}
				}
			}
			
			if(alias.length() > 0 && server.length() > 0){
				aliasMapping.put(server, alias);
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Added alias:"+alias+" for server:"+server);
				}
			}else{
				LOGGER.error("Invalid alias mapping entry:[alias="+alias+" -> server="+server+"], Ignoring");
			}
		}
	}
	/**
	 * Returns the private key from this key store for a given alias if exists.
	 * If no password is provided, keystore password will be used to retrieve the key
	 * @param alias
	 * @param password
	 * @return PrivateKey for a given alias
	 */
	public ECPrivateKey getECPrivateKey(String alias, char[] password){
		char[] keyPass = null;
		ECPrivateKey aliasPrivateKey = null;
		if(alias == null || alias.equalsIgnoreCase("Default")) {
			alias = this.getDefaultAlias();
		}
		if(password == null){
			keyPass = keyStorePassword.toCharArray();
		}else{
			keyPass = password;
		}
		try{
			if(platformKeyStore == null){
				initKeyStore();
			}
			KeyStore.ProtectionParameter protParam =
			        new KeyStore.PasswordProtection(keyPass);
			
			// get private key
			KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry)
				platformKeyStore.getEntry(alias, protParam);
			aliasPrivateKey = (ECPrivateKey)pkEntry.getPrivateKey();
		}catch(Exception e){
			LOGGER.error("Failed to find the private key for alias:"+alias+" failed with:"+e.getMessage(),e);
			throw new GIRuntimeException("Failed to find the private key for alias:"+alias+" failed with:", e);
		}
		return aliasPrivateKey;
	}
	
	public ECPublicKey getECPublicKey(String alias){
		Certificate cert = this.getCertificates(alias);
		if(cert != null){
			return (ECPublicKey)cert.getPublicKey();
		}
		return null;
	}

	public Certificate[] getCertificateChain(String alias) {
		try {
			return platformKeyStore.getCertificateChain(alias);
		} catch (KeyStoreException e) {
			LOGGER.error("Error retrieving certificate chain for alias {}",alias);
		}
		return null;
	}
	
	public static PrivateKeyEntry getPrivateKeyEntry(String alias, char[] password){
		char[] keyPass = null;
		PrivateKeyEntry aliasPrivateKeyEntry = null;
		if(password == null){
			keyPass = keyStorePassword.toCharArray();
		}else{
			keyPass = password;
		}
		try{
			if(platformKeyStore == null){
				initKeyStore();
			}
			KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(keyPass);
			aliasPrivateKeyEntry = (KeyStore.PrivateKeyEntry)platformKeyStore.getEntry(alias, protParam);
		}catch(Exception e){
			LOGGER.error("Failed to find the private key for alias:"+alias+" faled with:"+e.getMessage(),e);
			return null;
		}
		return aliasPrivateKeyEntry;
	}
}
