package com.getinsured.hix.platform.couchbase.helper;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.couchbase.client.deps.io.netty.buffer.ByteBuf;
import com.couchbase.client.deps.io.netty.buffer.Unpooled;
import com.couchbase.client.deps.io.netty.util.ReferenceCountUtil;
import com.couchbase.client.java.document.BinaryDocument;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.transcoder.JsonTranscoder;
import com.getinsured.hix.platform.couchbase.converters.JsonConverter;
import com.getinsured.hix.platform.couchbase.helper.DocumentHelperException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

@Component("couchDocumentHelper")
public class DocumentHelper {
	@Autowired
	@Qualifier("gsonConverter")
	private JsonConverter converter;

	private final JsonTranscoder transcoder = new JsonTranscoder();

	public <T> T fromJsonDocument(JsonDocument doc, Class<T> type) {
		if (doc == null) {
			throw new DocumentHelperException("document is null");
		}

		JsonObject content = doc.content();
		return fromJsonDocument(content, type);
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> T fromJsonDocument(JsonObject content, Class<T> type) {
		if (content == null) {
			throw new DocumentHelperException("document has no content");
		}
		if (type == null) {
			throw new DocumentHelperException("type is null");
		}
		if (type == String.class) {
			return (T) content.toString();
		}
		T result = converter.fromJson(content.toString(), type);
		return result;
	}

	public <T> JsonDocument toJsonDocument(T source, String id) {
		if (source == null) {
			throw new DocumentHelperException("entity is null");
		}
		if (id == null) {
			throw new DocumentHelperException("entity ID is null");
		}
		try {
			JsonObject content;
			if (source instanceof String) {
				content = transcoder.stringToJsonObject((String) source);
			} else if (source instanceof Byte[]) {
				throw new DocumentHelperException("API does not support this operation");
			} else {
				content = transcoder.stringToJsonObject(converter.toJson(source));
			}

			JsonDocument doc = JsonDocument.create(id, content);
			return doc;
		} catch (Exception e) {
			throw new DocumentHelperException(e);
		}
	}

	public static String encodeBase64String(byte[] dataBytes) {
		return Base64.getEncoder().encodeToString(dataBytes);
	}

	public static byte[] decodeBase64(String base64String) {
		return Base64.getDecoder().decode(base64String);
	}

	public static boolean isValidJson(String jsonString) {
		boolean isValid = false;
		try {
			JsonParser parser = new JsonParser();
			parser.parse(jsonString);
			isValid = true;
		} catch (JsonSyntaxException e) {
			/* eat exception */
		}
		return isValid;
	}

	public BinaryDocument toBinaryDocument(byte[] data, String id) {
		if (data == null) {
			throw new DocumentHelperException("data is null");
		}
		if (id == null) {
			throw new DocumentHelperException("ID is null");
		}
		try {
			BinaryDocument doc = BinaryDocument.create(id, Unpooled.copiedBuffer(data));
			return doc;
		} catch (Exception e) {
			throw new DocumentHelperException(e);
		}
	}

	public byte[] fromBinaryDocument(BinaryDocument doc) {
		if (doc == null) {
			throw new DocumentHelperException("document is null");
		}

		ByteBuf content = null;

		try
		{
			content = doc.content();

			if (content == null)
			{
				throw new DocumentHelperException("document has no content");
			}

			byte[] res = new byte[content.readableBytes()];
			content.readBytes(res);
			return res; // content.array();
		}
		finally
		{
			if(content != null)
			{
				ReferenceCountUtil.release(content);
			}
		}
	}
}
