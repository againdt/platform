package com.getinsured.hix.platform.accountactivation;

import com.getinsured.hix.platform.config.IEXConfiguration;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;

/**
*
* @author pravin wadkar
*/
@Component("ReferralActivationWithoutAutoLinkingEmail")
@DependsOn("dynamicPropertiesUtil")
@Scope("prototype")
public class ReferralActivationWithoutAutoLinkingEmail extends ReferralAccountActivationEmail {
    private String oeStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE);
    private String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE);
    
	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		Map<String,String> bean = super.getTokens(notificationContext);
		bean.put("OEStartDate", DateUtil.dateToString(DateUtil.addToDate(DateUtil.StringToDate(oeStartDate,"MM/dd/yyyy"),365),"MMMM dd, YYYY"));
		bean.put("OEEndDate", DateUtil.dateToString(DateUtil.addToDate(DateUtil.StringToDate(oeEndDate,"MM/dd/yyyy"),365),"MMMM dd, YYYY"));
		
		return bean;
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
