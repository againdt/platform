package com.getinsured.hix.platform.notices.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Event;
import com.getinsured.hix.model.TriggeredEvent;
import com.getinsured.hix.platform.notices.EventService;
import com.getinsured.hix.platform.notices.TriggeredEventAsyncService;
import com.getinsured.hix.platform.notices.TriggeredEventService;
import com.getinsured.hix.platform.notify.EmailNotificationService;
import com.getinsured.hix.platform.repository.ITriggeredEventRepository;
import com.google.gson.Gson;

@Service("TriggeredEventService")
public class TriggeredEventServiceImpl implements TriggeredEventService {

	@Autowired ITriggeredEventRepository triggeredEventRepository;
	@Autowired EmailNotificationService emailNotificationService;
	@Autowired EventService eventService;
	@Autowired TriggeredEventAsyncService triggeredEventAsynService;
	
	
	@Override
	public String createTriggeredEvent(String eventName, String jsonEventData) {
		String result = "";
		/*if(StringUtils.isNotEmpty(eventName)){
			Event event = new  Event();
			event =	eventService.findByEventName(eventName);
			if(Event.STATUS.ENABLED == event.getStatus()){
				try {
					this.asyncTriggeredEvent(event, jsonEventData);
					result = "SUCCESS";
				} catch (Exception e) {
					result = "FAILURE";
				}
			}else{
				result = "FAILURE";
			}
		}*/
		return result;
	}
	
	private void asyncTriggeredEvent(Event event, String jsonEventData){
		Gson gson = new Gson();
		
		List<Map<String, String>> eventDataList = new ArrayList<Map<String, String>>();
		eventDataList = gson.fromJson(jsonEventData, List.class);
		
		for (Map<String, String> eventJsonData : eventDataList) {
			TriggeredEvent triggeredEvent = new TriggeredEvent();
			triggeredEvent.setEvent(event);
			triggeredEvent.setJsonMergeTags(gson.toJson(eventJsonData));
			triggeredEventAsynService.save(triggeredEvent); 
		}
	}
}
