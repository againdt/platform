package com.getinsured.hix.platform.file.manager.factory;

import com.getinsured.hix.platform.file.manager.FileManagerService;
import com.getinsured.hix.platform.file.manager.service.FileSystemManagerServiceImpl;

/**
 * @author nayak_b
 *
 */
public class FileManagerFactory {
	
	public enum FileManager{HTTP,FTP,SFTP,FILE};

	public FileManagerFactory(){}
	
	/**
	 * static method to return concrete HTTP or FTP or SFTP or FILE instance based on fileManagerType.
	 * 
	 * @param fileManagerType - ENUM Type
	 * @return concrete instance of FileManagerService
	 * @throws IllegalArgumentException
	 */
	public static FileManagerService createFileManagerService(final String fileManagerType) {
		FileManagerService fileManagerService;
		if (FileManager.FILE.toString().equals(fileManagerType)){
			// return file system client
			fileManagerService = new FileSystemManagerServiceImpl();
		}else{
			throw new IllegalArgumentException(
					"\n\n***********************************************************\n"
							+ "* Unknown file manager Type configured [" + fileManagerType + "] !!\n"
			                + "* -DGHIX_HOME param or environment variable should point\n"
			                + "* to <MAIN-BRANCH setup module>/ because applicationPlatform.xml\n"
			                + "* uses this value while bootstrapping context:\n"
			                + "* file:///${GHIX_HOME}/ghix-setup/conf/configuration.properties\n"
							+ "****************************************************************\n\n");
		}

		return fileManagerService;

	}
	
}
