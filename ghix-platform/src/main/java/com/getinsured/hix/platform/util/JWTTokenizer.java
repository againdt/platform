
package com.getinsured.hix.platform.util;

import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

@Component
public class JWTTokenizer {
	private static Logger LOGGER = LoggerFactory.getLogger(JWTTokenizer.class);
	
	@Value("#{configProp['hms.jwt.private.key.password']}")
	public String jwtPrivateKeyPass;
	
	@Value("#{configProp['hms.jwt.private.key.alias']}")
	public String privateKeyAlias;
	
	@Autowired
	private GhixPlatformKeystore keyStore;

	public String getJWTECToken(String id, String issuer, String subject, long ttlMillis) throws Exception {
		String token = null;
		try {
			ECPrivateKey privateKey = this.keyStore.getECPrivateKey(privateKeyAlias, jwtPrivateKeyPass.toCharArray());

			// Create the EC signer
			JWSSigner signer = new ECDSASigner(privateKey);

			long nowMillis = System.currentTimeMillis();
			long expMillis = 0;
			Date d = null;
			if (ttlMillis >= 0) {
				expMillis = nowMillis + ttlMillis;
				d = new Date(expMillis);
			}
			// Prepare JWT with claims set
			JWTClaimsSet claimsSet = new JWTClaimsSet.Builder().jwtID(id).subject(subject).issuer(issuer).expirationTime(d)
					.build();

			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.ES256), claimsSet);

			// Compute the EC signature
			signedJWT.sign(signer);

			// Serialize the JWS to compact form
			token = signedJWT.serialize();
		} catch(Exception e){
			LOGGER.error("Failed to generate token ",e);
			throw new GIRuntimeException("Failed to generate token", e);
		}

		return token;
	}

	public void verifyJWT(String jwtToken) throws Exception {

		// On the consumer side, parse the JWS and verify its EC signature
		SignedJWT signedJWT = SignedJWT.parse(jwtToken);

		ECPublicKey publicKey = this.keyStore.getECPublicKey(privateKeyAlias);

		JWSVerifier verifier = new ECDSAVerifier(publicKey);
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("JWT Verified::::::" + signedJWT.verify(verifier));
		}

		if (signedJWT.verify(verifier)) {
			long sec = (signedJWT.getJWTClaimsSet().getExpirationTime().getTime() - System.currentTimeMillis()) / 1000;

			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Verify JWTId:" + signedJWT.getJWTClaimsSet().getJWTID());
				LOGGER.debug("Verify Subject:" + signedJWT.getJWTClaimsSet().getSubject());
				LOGGER.debug("Verify Issuer: " + signedJWT.getJWTClaimsSet().getIssuer());
				LOGGER.debug("JWT Token Reaching Expiration In ::  " + sec + " Seconds");
			}
			
			if (signedJWT.getJWTClaimsSet().getSubject().equals("HMS")
					&& signedJWT.getJWTClaimsSet().getIssuer().equals("GI") && sec >= 0)
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Verified");
				}
			else {
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("NotVerified : JWT Token Is EXPIRED. Get New Token!!");
				}
			}
		}
	}
}
