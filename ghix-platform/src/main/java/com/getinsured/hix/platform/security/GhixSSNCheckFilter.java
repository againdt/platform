package com.getinsured.hix.platform.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GhixSSNCheckFilter implements Filter{
	private static Logger log = LoggerFactory.getLogger(GhixSSNCheckFilter.class);

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		 	boolean shouldContinue = true;
		 	
		 	HttpServletRequest req= (HttpServletRequest) request;
		 	final String url = req.getRequestURL().toString();
		 	String path = req.getRequestURI().substring(req.getContextPath().length());
		 	if(	!path.startsWith("/resources/") 
						 			&& !path.contains("/account/user/logout") 
						 			&& !path.contains("/saml/logout") 
						 			&& !path.contains("j_spring_security_logout")
						 			&& !url.contains("ssnDetails") 
						 			&& !url.contains("validateSSNDetails")  
						 			&& !url.contains("loginSuccess")) {
		 		if(req.getSession().getAttribute("ssnRequired")  != null) {
					Boolean ssnRequired= (Boolean)req.getSession().getAttribute("ssnRequired");	
					
					if(ssnRequired == true) {
						HttpServletResponse httpResp = (HttpServletResponse)response;
			            
						httpResp.sendRedirect(req.getContextPath() + "/account/user/ssnDetails");
			            shouldContinue = false;
					}
				}
		 	}
		if(shouldContinue) {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		if(log.isDebugEnabled()) {
			log.debug("Initializing GhixSSNCheckFilter");
		}
	}

}
