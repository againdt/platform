package com.getinsured.hix.platform.config;

/**
 * 
 * @author Sharma_k
 * @since 17th December 2013
 * Dynamic configuration for Finance module HIX-24649
 */
public class FinanceConfiguration
{
	  public enum FinanceConfigurationEnum implements PropertiesEnumMarker
	  {
		  
		  IS_BINDER_INVOICE ("finance.IsBinderInvoice"),
		  ELECTRONIC_PAYMENT_ONLY ("finance.ElectronicPaymentOnly"),
		  BINDER_GRACE_DAYS ("finance.BinderGraceDays"),
		  PRO_RATIO_SUPPORTED ("finance.ProRationSupported"),
		  PARTIAL_PAYMENT_SUPPORTED ("finance.PartialPaymentSupported"),
		  MINIMUM_PAYMENT_THRESHOLD ("finance.MinimumPaymentThreshold"),
		  PARITIAL_PAYMENT_ALLOCATION_TYPE ("finance.PartialPaymentAllocationType"),
		  MAXIMUM_PAYMENT_THRESHOLD ("finance.MaximumPaymentThreshold"),
		  EXCHANGE_DEDUCT_REMITTENCE ("finance.ExchangeDeductRemittence"),
		  ACCEPT_CREDIT_CARD ("finance.AcceptCreditCard"),
		  PAYMENT_GRACE_PERIOD ("finance.PaymentGracePeriod"),
		  IS_PAYMENT_GATEWAY ("finance.IsPaymentGateway"),
		  EXCHANGE_FEE ("finance.ExchangeFee"),
		  MERCHANT_ID ("finance.merchantID"),
		  TARGET_API_VERSION ("finance.targetAPIVersion"),
		  SEND_TO_PRODUCTION ("finance.sendToProduction"),
		  UPLOAD_PATH ("finance.uploadPath"),
		  PAYMENT_RECONCILE_BUSINESSDAYS ("finance.paymentReconcileBusinessDays"),
		  BUSINESSDAYS_RECONLICE_REQUIRED ("finance.businessDaysReconciliationRequired"),
		  IS_STANDALONE_CREDIT_SUPPORTED("finance.isStandaloneCreditSupported"),
		  ESCALATION_EMAIL("finance.escalationEmail"),
		  FINANCE_SYSTEM_USER("finance.financeSystemUser"),
		  PAYMENT_EVENT_REPORT_URL("finance.paymentEventReportURL"),		
		  CARRIER_REDIRECT_FLAG("finance.carrierRedirect"),
		  IS_MANUAL_ADJUSTMENT_REQUIRED("finance.isManualAdjustmentRequired"),
		  IS_BINDER_REMINDER_EMAIL_REQUIRED("finance.isBinderReminderRequired"),
		  IS_AGENT_EMPDUE_NOTIFICATION_REQUIRED("finance.isAgentEmpDueNotificationRequired"),
		  IS_NSF_FEES_REQUIRED("finance.isNSFFeesRequired"),
		  NSF_FLAT_FEE("finance.NSFFlatFee"),
		  IS_EMPLOYEE_INVOICE_DUE_NOTIFICATION_REQUIRED("finance.isEmployeeInvoiceDueNoticeRequired"),
		  IS_EMPLOYER_FEE_REQUIRED("finance.isEmployerFeeRequired"),
		  EMPLOYER_FLAT_FEE_PER_ENROLLMENT("finance.EmployerFlatFeePerEnrollment"),
		  IS_AUTOPAY_APPROACHING_NOTIFICATION_REQUIRED("finance.isAutopayApproachingNotificationRequired"),
		  IS_AUTOPAY_MADE_NOTIFICATION_REQUIRED("finance.isAutopayMadeNotificationRequired"),
		  AUTOPAY_DAY_OF_MONTH("finance.autoPayDayOfMonth"),
		  PRE_BILLING_NOTIFICATION_DAYS_FOR_AUTOPAY("finance.preBillingNotificationDaysForAutoPay"),
		  BINDER_INVOICE_GENERATION_DAY("finance.BinderInvoiceGenerationDay");
		  
		  private final String value;
		  
		  public String getValue(){return this.value;}
		  FinanceConfigurationEnum(String value)
		  {
			  this.value = value;
		  }
	  };
}
