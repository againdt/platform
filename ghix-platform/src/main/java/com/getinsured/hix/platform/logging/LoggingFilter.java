package com.getinsured.hix.platform.logging;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.ThreadContext;

/**
 * This class will help to create and set correlation id and user name to each thread of operation.
 * Scope if improvement:
 * For rest call we need to upgrade our rest template/service invoker to pass the correlation id and username/userid
 *  or else it will set new id and passed username/userid.
 * 
 * @author nayak_b
 *
 */
public class LoggingFilter implements Filter
{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		boolean clear = false;
		if(!ThreadContext.containsKey("id")) {
			clear = true;
			ThreadContext.put("id", UUID.randomUUID().toString());
			HttpSession session = ((HttpServletRequest)request).getSession(false);
			if(session != null){
				ThreadContext.put("username", (String)session.getAttribute("username"));
			}

			// Only for demo..
			//ThreadContext.put("username", "getinsured@ghix.com");
		}

		try	{
			chain.doFilter(request, response);
		}
		finally	{
			if(clear) {
				ThreadContext.clearAll();
			}
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}
