package com.getinsured.hix.platform.security.service.jpa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ActiveFlag;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.MenuItemDTO;
import com.getinsured.hix.model.TenantPermission;
import com.getinsured.hix.model.TenantPermissionRoleMappingDTO;
import com.getinsured.hix.platform.cache.GICachingService;
import com.getinsured.hix.platform.security.repository.ITenantPermissionRepository;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.TenantPermissionService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Service(value="tenantPermissionService")
public class TenantPermissionServiceImpl implements TenantPermissionService{
	
	@Autowired
	private ITenantPermissionRepository iTenantPermissionRepository;
	
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil; 
	
	@Autowired
	private GICachingService<MenuItemDTO> giCachingService;
	
	@Autowired RoleService roleService; 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TenantPermissionServiceImpl.class);
	
	@Override
	public List<TenantPermission> findByTenantIdAndPermissionName(long tenantId,String permission) {
		List<TenantPermission> tenantPermissions = null;
		try{
			tenantPermissions = iTenantPermissionRepository.findByTenantIdAndPermissionName(tenantId,permission,ActiveFlag.Y);
		}catch(Exception e){
			LOGGER.error("Failed to pull tenant permissions",e);
		}
		return tenantPermissions;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GIRuntimeException.class)
	public void addTenantPermissions(String tenantName){
		EntityManager em = null;
		StringBuilder queryString = new StringBuilder("INSERT"+
		" INTO TENANT_PERMISSIONS"+
		" ("+
			" ID,"+
			" PERMISSION_ID,"+
			" TENANT_ID,"+
			" CREATION_TIMESTAMP,"+
			" LAST_UPDATE_TIMESTAMP,"+
			" IS_ACTIVE"+
		" )"+
		" ("+
			" SELECT"+
			" TENANT_PERMISSIONS_SEQ.NEXTVAL,"+
			" p.id,t.id,systimestamp,systimestamp,COALESCE(p.IS_DEFAULT,'N') FROM tenant t, permissions p"+
			" where t.name= :tenantName"+
		" )");
		
		Query query = null;
		
		try{
			em = emf.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			query = em.createNativeQuery(queryString.toString());
			
			query.setParameter("tenantName", tenantName);
			int count = query.executeUpdate();
			
			LOGGER.info("Number of records inserted: "+count);
			em.getTransaction().commit();
			
		}catch(Exception e){
			LOGGER.error("Failed to insert records in  tenant permissions",e);
			if(em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}
			throw new GIRuntimeException(e);
		}finally{
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
			
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getTenantPermissionMapping(String tenantName) {

		List<String> allRoleNames = roleService.findAllRoleName();
		EntityManager em = null;
		

		String queryStr = 	" SELECT "+
				" p.name, "+
				" COALESCE(p.description,'-'), "+
				" tp.IS_ACTIVE, "+
				" r.name as ROLE_NAME, "+
				" tp.ID AS tenant_permission_id "+
				" FROM permissions p "+
				" LEFT JOIN role_permissions rp "+
				" ON p.id = rp.PERMISSION_ID "+
				" LEFT JOIN roles r "+
				" ON rp.ROLE_ID = r.id "+
				" LEFT JOIN tenant_permissions tp "+
				" ON p.id = tp.PERMISSION_ID "+
				" JOIN tenant t "+
				" ON tp.TENANT_ID = t.id "+
				" AND t.name = :tenantName "
				+ " order by case when p.description is null then 1 else 0 end,p.name";
		

		//		List<RolePermission> rolePermissions = iRolePermissionRepository.findAll();
		List<Object[]> resultList =  new ArrayList<Object[]>();
		List<Map<String,Object>> finalPermissionList  = new ArrayList<Map<String,Object>>();
		try{
			em = emf.createEntityManager();
			Query query = em.createNativeQuery(queryStr);
			query.setParameter("tenantName", tenantName);
			resultList = query.getResultList();


			List<String> permissionRoles = null;
			TenantPermissionRoleMappingDTO tenantPermissionRoleMappingDTO = new TenantPermissionRoleMappingDTO();
			Map<String,TenantPermissionRoleMappingDTO> permissionMap = new HashMap<String,TenantPermissionRoleMappingDTO>();


			//		TODO - cleanup this logic 
			for(Object[] resultArr : resultList){
				tenantPermissionRoleMappingDTO = new TenantPermissionRoleMappingDTO();
				tenantPermissionRoleMappingDTO.setPermissionName(resultArr[0] != null ? resultArr[0].toString() : null);
				tenantPermissionRoleMappingDTO.setPermissionDescription(resultArr[1] != null ? resultArr[1].toString() : null);
				String isActive = resultArr[2] != null ? resultArr[2].toString() : null;
				tenantPermissionRoleMappingDTO.setIsActiveForTenant(ActiveFlag.Y.toString().equalsIgnoreCase(isActive) ? true : false );
				tenantPermissionRoleMappingDTO.setTenantPermissionId(ghixJasyptEncrytorUtil.encryptStringByJasypt(resultArr[4] != null ? resultArr[4].toString() : null));

				if(permissionMap.get(tenantPermissionRoleMappingDTO.getPermissionName()) != null){
					permissionRoles = permissionMap.get(tenantPermissionRoleMappingDTO.getPermissionName()).getRoleNames();
				}else{ 
					permissionRoles = new ArrayList<String>();
				}
				permissionRoles.add(resultArr[3] != null ? resultArr[3].toString() : null);
				tenantPermissionRoleMappingDTO.setRoleNames(permissionRoles);
				permissionMap.put(tenantPermissionRoleMappingDTO.getPermissionName(), tenantPermissionRoleMappingDTO);
			}

			Map<String,Object> finalMap = new TreeMap<String,Object>();

			for(Map.Entry<String,TenantPermissionRoleMappingDTO> entrySet: permissionMap.entrySet()){
				finalMap = new HashMap<String,Object>();
				finalMap.put("permissionName", entrySet.getValue().getPermissionName());
				finalMap.put("permissionDescription", entrySet.getValue().getPermissionDescription());
				finalMap.put("isActiveForTenant", entrySet.getValue().getIsActiveForTenant());
				finalMap.put("tenantPermissionId",  entrySet.getValue().getTenantPermissionId());
				for(String role : allRoleNames){
					String roleKey = WordUtils.capitalize(role).replaceAll(" ", "_");
					if(entrySet.getValue().getRoleNames().contains(role)){
						finalMap.put(roleKey, true);
					}else{
						finalMap.put(roleKey, false);
					}
				}
				finalPermissionList.add(finalMap);
			}

		}catch(Exception e){
			LOGGER.error("Failed to fetch tenant permission mapping records",e);
		}finally{
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}

		}

		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		String output = gson.toJson(finalPermissionList);

//		LOGGER.info("Permission Role mapping JSON: "+output);

		return output;
	}

	@Override
	public String updateTenantPermissionMapping(
			List<TenantPermissionRoleMappingDTO> tenantPermissionRoleMappingDTOs) {

		try{
			List<Long> tenantPermissionIds = new ArrayList<Long>();
			Map<String,Boolean> tenantPermissionMap = new HashMap<String,Boolean> ();

			for(TenantPermissionRoleMappingDTO tenantPermissionRoleMappingDTO : tenantPermissionRoleMappingDTOs){
				if(tenantPermissionRoleMappingDTO.getTenantPermissionId() != null) {
					String tenantPermissionId = ghixJasyptEncrytorUtil.decryptStringByJasypt(tenantPermissionRoleMappingDTO.getTenantPermissionId());
					LOGGER.info("tenantPermissionId: "+tenantPermissionId);
					tenantPermissionIds.add(Long.valueOf(tenantPermissionId));
					tenantPermissionMap.put(tenantPermissionId, tenantPermissionRoleMappingDTO.getIsActiveForTenant());
				}

			}

			List<TenantPermission> tenantPermissions = iTenantPermissionRepository.findByIdIn(tenantPermissionIds);

			if(tenantPermissions != null && !tenantPermissions.isEmpty()){
				LOGGER.info("Size of tenantPermissions: "+tenantPermissions.size());
				for(TenantPermission tenantPermission : tenantPermissions){
					ActiveFlag isPermissionActive = tenantPermissionMap.get(
							String.valueOf(tenantPermission.getId())) 
							? ActiveFlag.Y : ActiveFlag.N;
					LOGGER.info("isPermissionActive: "+isPermissionActive.toString());
					tenantPermission.setIsActive(isPermissionActive);
				}
				iTenantPermissionRepository.save(tenantPermissions);
			}
			LOGGER.info("Clearing Menu cache for the permission");
			clearMenuCacheForTenantPermission(tenantPermissionRoleMappingDTOs);
			LOGGER.info("Returning data");
			return "SUCCESS";
		}catch(Exception e){
			LOGGER.error("Failed to update Tenant Permissions ",e);
			return "FAILURE";
		}
	}
	
	private void clearMenuCacheForTenantPermission(List<TenantPermissionRoleMappingDTO> tenantPermissionRoleMappingDTOs){
		try{
			for(TenantPermissionRoleMappingDTO tenantPermissionRoleMappingDTO : tenantPermissionRoleMappingDTOs){
				if (StringUtils.isNotEmpty(tenantPermissionRoleMappingDTO.getTenantName())
						&& tenantPermissionRoleMappingDTO.getRoleNames() != null) {
					List<String> keyList = new ArrayList<String>();
					String tenantName = tenantPermissionRoleMappingDTO.getTenantName().toUpperCase();
					for(String roleName :tenantPermissionRoleMappingDTO.getRoleNames()){
						String key = roleName.toUpperCase() + tenantName;
						keyList.add(key);
					}
					giCachingService.deleteAll(new MenuItemDTO().getObjectKey(), keyList);
				}
			}
		}catch(Exception e){
			LOGGER.error("Failed to clear the Menu Item cache: ",e);
		}
	}
	
}
