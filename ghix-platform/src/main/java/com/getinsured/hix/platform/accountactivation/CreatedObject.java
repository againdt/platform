package com.getinsured.hix.platform.accountactivation;

import java.util.List;
import java.util.Map;

/**
 * Place holder to store info about created object.
 * 
 * @author Ekram Ali Kazi
 *
 */
public class CreatedObject {

	/** PK of the created object. This is useful to perform db operation */
	private Integer objectId;

	/** To send email */
	private String fullName;

	/** email id */
	private String emailId;

	/** Multiple numbers to send code via sms/call*/
	private List<String> phoneNumbers;

	/** Role to identify type of created object and creation of user with the ROLE */
	private String roleName;

	/** For Future use, if we need more parameters then simply add those as key-value pair */
	private Map<String, String> customeFields;
	
	/** first name of user */
	private String firstName;
	/** last name of user */
	private String lastName;
	
	private String accountActivationUrl;
	

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
	

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/


	@Override
	public String toString() {
					return "CreatedObject [objectId=" + objectId + ", fullName=" + fullName
													+ ", emailId=" + emailId + ", phoneNumbers=" + phoneNumbers
													+ ", roleName=" + roleName + ", customeFields=" + customeFields
													+ ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}


	public Map<String, String> getCustomeFields() {
		return customeFields;
	}

	public void setCustomeFields(Map<String, String> customeFields) {
		this.customeFields = customeFields;
	}

	public String getAccountActivationUrl() {
		return accountActivationUrl;
	}

	public void setAccountActivationUrl(String accountActivationUrl) {
		this.accountActivationUrl = accountActivationUrl;
	}

}
