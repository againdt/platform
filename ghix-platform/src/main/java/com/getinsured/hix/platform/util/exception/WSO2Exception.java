package com.getinsured.hix.platform.util.exception;

/**
 * Generic Exception for WSO2 related errors
 */
public class WSO2Exception extends GIException {
	
	private static final long serialVersionUID = 1L;
	
	public WSO2Exception() {
		super();
	}
	public WSO2Exception(String message, Throwable cause) {
		super(message, cause);
	}
	public WSO2Exception(String message) {
		super(message);
	}
	public WSO2Exception(Throwable cause) {
		super(cause);
	}
	
}
