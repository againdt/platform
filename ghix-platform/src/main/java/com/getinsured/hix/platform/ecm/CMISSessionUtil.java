package com.getinsured.hix.platform.ecm;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

public class CMISSessionUtil
{
  private static final Map<String, String> ECM_MIMES = new HashMap<String, String>();

  private static final Map<String, String> TYPES = new HashMap<String, String>();

  private static final Map<String, String> AHBX_ECM_MIMES = new HashMap<String, String>();

  private static String ecmUsername;
  private String username;

  static
  {
    TYPES.put("txt", "TXT");
    TYPES.put("css", "CSS");
    TYPES.put("xml", "XML");
    TYPES.put("csv", "CSV");
    TYPES.put("png", "IMG");
    TYPES.put("jpg", "IMG");
    TYPES.put("jpeg", "IMG");
    TYPES.put("bmp", "IMG");
    TYPES.put("gif", "IMG");
    TYPES.put("doc", "DOC");
    TYPES.put("docx", "DOC");
    TYPES.put("dot", "DOC");
    TYPES.put("dotx", "DOC");
    TYPES.put("xls", "XLS");
    TYPES.put("xlsx", "XLS");
    TYPES.put("ppt", "PPT");
    TYPES.put("pptx", "PPT");
    TYPES.put("pdf", "PDF");
    TYPES.put("zip", "ZIP");
    TYPES.put("tar", "TAR");
    TYPES.put("gz", "GTAR");
    TYPES.put("js", "JS");
    TYPES.put("xlsm", "XLS");
  }

  static
  {
    AHBX_ECM_MIMES.put("txt", "text/plain");
    AHBX_ECM_MIMES.put("xml", "application/xml");
    AHBX_ECM_MIMES.put("csv", "text/csv");
    AHBX_ECM_MIMES.put("png", "image/png");
    AHBX_ECM_MIMES.put("jpg", "image/jpeg");
    AHBX_ECM_MIMES.put("jpeg", "image/jpeg");
    AHBX_ECM_MIMES.put("bmp", "image/bmp");
    AHBX_ECM_MIMES.put("gif", "image/gif");
    AHBX_ECM_MIMES.put("doc", "application/msword");
    AHBX_ECM_MIMES.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    AHBX_ECM_MIMES.put("xls", "application/vnd.ms-excel");
    AHBX_ECM_MIMES.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    AHBX_ECM_MIMES.put("pdf", "application/pdf");
    AHBX_ECM_MIMES.put("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12");
    AHBX_ECM_MIMES.put("ppt", "application/vnd.ms-powerpointtd");
    AHBX_ECM_MIMES.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
    AHBX_ECM_MIMES.put("zip", "application/zip, application/x-compressed-zip");
  }

  static
  {
    ECM_MIMES.put("txt", "text/plain");
    ECM_MIMES.put("css", "text/css");
    ECM_MIMES.put("xml", "text/xml");
    ECM_MIMES.put("csv", "text/csv");
    ECM_MIMES.put("png", "image/png");
    ECM_MIMES.put("jpg", "image/jpeg");
    ECM_MIMES.put("jpeg", "image/jpeg");
    ECM_MIMES.put("bmp", "image/bmp");
    ECM_MIMES.put("gif", "image/gif");
    ECM_MIMES.put("doc", "application/msword");
    ECM_MIMES.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    ECM_MIMES.put("dot", "application/msword");
    ECM_MIMES.put("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
    ECM_MIMES.put("xls", "application/vnd.ms-excel");
    ECM_MIMES.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    ECM_MIMES.put("ppt", "application/vnd.ms-powerpoint");
    ECM_MIMES.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
    ECM_MIMES.put("pdf", "application/pdf");
    ECM_MIMES.put("zip", "application/x-zip-compressed");
    ECM_MIMES.put("tar", "application/x-tar");
    ECM_MIMES.put("gz", "application/x-gzip");
    ECM_MIMES.put("js", "text/javascript");
    ECM_MIMES.put("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12");
    ECM_MIMES.put("ppt", "application/vnd.ms-powerpointtd");
    ECM_MIMES.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
  }

  public static String getMime(String fileName)
  {
    String extension = FilenameUtils.getExtension(fileName);
    if (StringUtils.isNotEmpty(extension) && ECM_MIMES.containsKey(extension.toLowerCase()))
    {
      return ECM_MIMES.get(extension.toLowerCase());
    }
    return StringUtils.EMPTY;
  }

  public static String getType(String fileName)
  {
    String extension = FilenameUtils.getExtension(fileName);
    if (StringUtils.isNotEmpty(extension) && TYPES.containsKey(extension.toLowerCase()))
    {
      return TYPES.get(extension.toLowerCase());
    }
    return StringUtils.EMPTY;
  }

  public static String getAHBXMime(String fileName)
  {
    String extension = FilenameUtils.getExtension(fileName);
    if (StringUtils.isNotEmpty(extension) && AHBX_ECM_MIMES.containsKey(extension.toLowerCase()))
    {
      return AHBX_ECM_MIMES.get(extension.toLowerCase());
    }
    return StringUtils.EMPTY;
  }

  public static String getEcmUsername()
  {
    return ecmUsername;
  }

  public static void setEcmUsername(String ecmUsername)
  {
    CMISSessionUtil.ecmUsername = ecmUsername;
  }

  public void setUsername(String username) {
    this.username = username;
    CMISSessionUtil.ecmUsername = username;
  }

  public String getUsername() {
    return this.username;
  }
}
