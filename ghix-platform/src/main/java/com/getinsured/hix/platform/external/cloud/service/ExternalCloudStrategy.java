package com.getinsured.hix.platform.external.cloud.service;

import java.net.URL;

interface ExternalCloudStrategy {

	URL upload(String relativePath, String fileName, byte[] dataBytes);

}
