package com.getinsured.hix.platform.security.service;

import java.util.List;

import com.getinsured.hix.model.TenantRoleDTO;


/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface TenantRoleService {
	
	public void addTenantRoles(Long tenantId);
	
	public String getTenantRoles(String tenantName);
	
	public String updateTenantRolesMapping(List<TenantRoleDTO> tenantRolesDTOs);
	
	public void addOpsadminUserForTenant(Long tenantId);

}
