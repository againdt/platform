package com.getinsured.hix.platform.startup;


/**
 * This class has method that pings the database and returns the timezone and
 * query duration.
 *
 * @author Sunil Desu
 * @since April 02 2013
 */
//@Component
public class DbPing {

	// @Autowired	private OracleDataSource dataSource;

	//private static final String DATEDUAL = "datedual";
	//private static final String DB_TIMEZONE = "DBTIMEZONE";
	//private static final String SESSION_TIMEZONE = "SESSIONTIMEZONE";
	//private static final String DB_PING_SQL = "select DBTIMEZONE, SESSIONTIMEZONE, to_char(sysdate, 'Dy DD-Mon-YYYY HH24:MI:SS') as \"datedual\" from dual";

	public String dbPing(String type) {
		//String LINE_DELIMETER = "";
		StringBuffer response = new StringBuffer();
		/*try {
			if ("html".equalsIgnoreCase(type)){
				LINE_DELIMETER = "<BR>";
			}
			else{
				LINE_DELIMETER = "\n";
			}
			String DBTIMEZONE = "";
			String SESSIONTIMEZONE = "";
			String dateDual = "";
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			long startTime;
			long endTime;

			try {
				conn = dataSource.getConnection();
				ps = conn.prepareStatement(DB_PING_SQL);
				startTime = System.currentTimeMillis();
				rs = ps.executeQuery();
				endTime = System.currentTimeMillis();
				while (rs.next()) {
					dateDual = rs.getString(DATEDUAL);
					DBTIMEZONE = rs.getString(DB_TIMEZONE);
					SESSIONTIMEZONE = rs.getString(SESSION_TIMEZONE);
				}
				response.append("<table>\n");
				
				response.append("<tr><td>");
				response.append("DB Ping: URL: ");
				response.append(dataSource.getURL());
				response.append(LINE_DELIMETER);
				response.append("</td></tr>");
				
				response.append("<tr><td>");
				response.append("DB Ping: USER: ");
				response.append(dataSource.getUser());
				response.append(LINE_DELIMETER);
				response.append("</td></tr>");
				
				response.append("<tr><td>");
				response.append("DB Ping: Date: ");
				response.append(dateDual);
				response.append(LINE_DELIMETER);
				response.append("</td></tr>");
				
				response.append("<tr><td>");
				response.append("DB Ping: DB Timezone: ");
				response.append(DBTIMEZONE);
				response.append(LINE_DELIMETER);
				response.append("</td></tr>");
				
				response.append("<tr><td>");
				response.append("DB Ping: Session Timezone: ");
				response.append(SESSIONTIMEZONE);
				response.append(LINE_DELIMETER);
				response.append("</td></tr>");
				
				response.append("<tr><td>");
				response.append("DB Ping: Time taken to query (ms):");
				response.append(String.valueOf(endTime - startTime));
				response.append("</td></tr>");
				
				response.append("\n</table>");

			} catch (SQLException sqlException) {
				response.append("DB Ping: ERROR: SQL Exception: ");
				response.append(sqlException.getMessage());
			} catch (Exception exception) {
				response.append("DB Ping: ERROR: Exception: ");
				response.append(exception.getMessage());
			} finally {
				if (null != rs) {
					try {
						rs.close();
						rs = null;
					} catch (Exception sqlException) {
						response.append("DB Ping: ERROR: SQL Exception: ");
						response.append(sqlException.getMessage());
					}
				}
				if (ps != null) {
					try {
						ps.close();
					} catch (Exception sqlException) {
						response.append("DB Ping: ERROR: SQL Exception: ");
						response.append(sqlException.getMessage());
					}
				}
				if (conn != null) {
					try {
						conn.close();
					} catch (Exception sqlException) {
						response.append("DB Ping: ERROR: SQL Exception: ");
						response.append(sqlException.getMessage());
					}
				}
			}
		} catch (Exception exception) {
			response.append("DB Ping: ERROR: Exception: ");
			response.append(exception.getMessage());
		}*/
		return response.toString();
	}

}
