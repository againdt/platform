package com.getinsured.hix.platform.config;

public class MedicareConfiguration {

	public enum MedicareConfig implements PropertiesEnumMarker
	{  
		MEDICARE_AEP_START_DATE("global.medicare.aep.startDate"),
		MEDICARE_AEP_END_DATE("global.medicare.aep.endDate"),
		MEDICARE_AEP_CURRENT_YEAR("global.medicare.currentYear"),
		MEDICARE_DRX_URL("global.medicare.drx.url"),
		MEDICARE_DRX_YEAR1("global.medicare.year1"),
		MEDICARE_DRX_YEAR2("global.medicare.year2");
		
		private final String value;	  

		@Override
		public String getValue(){return this.value;}

		MedicareConfig(String value){
	        this.value = value;
	    }
	};

}
