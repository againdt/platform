package com.getinsured.hix.platform.external.cloud.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

public class ExternalCloudHelper {
	
	private static final String FILE_NAME_CANNOT_BE_NULL = "File Name cannot be null";
	private static final String UNSUPPORTED_DOCUMENT_TYPE = "Unknown file type. Please upload one of these file types: pdf, png, gif, jpg/jpeg, bmp";
	private static final String DATA_BYTES_CANNOT_BE_NULL = "dataBytes cannot be null";

	private static final Map<String, String> MIMES = new HashMap<String, String>();
	static {
		MIMES.put("txt", "text/plain");
		MIMES.put("css", "text/css");
		MIMES.put("xml", "text/xml");
		MIMES.put("csv", "text/csv");
		MIMES.put("png", "image/png");
		MIMES.put("jpg", "image/jpeg");
		MIMES.put("jpeg", "image/jpeg");
		MIMES.put("bmp", "image/bmp");
		MIMES.put("gif", "image/gif");
		MIMES.put("doc",  "application/msword");
		MIMES.put("pdf", "application/pdf");
		MIMES.put("js", "text/javascript");
		MIMES.put("json", "application/json");
		MIMES.put("ico", "image/x-icon");
	}
	
	
	private static String getMime(String fileName) {
		String extension = FilenameUtils.getExtension(fileName);
		if (StringUtils.isNotEmpty(extension) && MIMES.containsKey(extension.toLowerCase())){
			return MIMES.get(extension.toLowerCase());
		}
		return StringUtils.EMPTY;
	}
	
	public static String validateIncomingContent(String fileName, byte[] dataBytes) {
		if (StringUtils.isEmpty(fileName)){
			throw new IllegalArgumentException(FILE_NAME_CANNOT_BE_NULL);
		}

		String mimeType = getMime(fileName);
		if (StringUtils.isEmpty(mimeType)){
			throw new IllegalArgumentException(UNSUPPORTED_DOCUMENT_TYPE);
		}

		if (dataBytes == null || dataBytes.length <= 0){
			throw new IllegalArgumentException(DATA_BYTES_CANNOT_BE_NULL);
		}
		return mimeType;
	}

}
