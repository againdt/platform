package com.getinsured.hix.platform.external.cloud.service;

import java.net.URL;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExternalCloudServiceImpl implements ExternalCloudService {
	
	private static final String RACKSPACE = "RACKSPACE";
	private static final String UNKNOWN_CLOUD_TYPE = "Unknown cloud.type set in configuration.properties.";

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalCloudServiceImpl.class);
	
	@Value("#{configProp['cloud.type'] != null ? configProp['cloud.type'] : 'RACKSPACE'}")
	private String type;
	
	@Autowired private RackspaceCloud rackspaceCloud;
	
	private ExternalCloudStrategy externalCloudStrategy;
	
	@PostConstruct
	public void createContextInitilizer() {
		if (RACKSPACE.equals(type)) {
			externalCloudStrategy = rackspaceCloud;
		}else{
			LOGGER.error(UNKNOWN_CLOUD_TYPE);
			throw new IllegalArgumentException(UNKNOWN_CLOUD_TYPE);
		}
	}

	@Override
	public URL upload(String relativePath, String fileName, byte[] dataBytes) {
		return externalCloudStrategy.upload(relativePath, fileName, dataBytes);
	}
	
}
