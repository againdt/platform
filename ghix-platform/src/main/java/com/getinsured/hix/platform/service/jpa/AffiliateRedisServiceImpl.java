package com.getinsured.hix.platform.service.jpa;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.platform.service.AffiliateRedisService;

@Component("affiliateRedisService")
public class AffiliateRedisServiceImpl implements AffiliateRedisService
{
  @Autowired(required = false)
  @Qualifier("redisTemplate")
  private RedisTemplate<String, Affiliate> redisAffiliateTemplate;

  private static final Logger log = LoggerFactory.getLogger(AffiliateRedisServiceImpl.class);

  private ValueOperations<String, Affiliate> affiliateOperations;

  @Override
  public Affiliate get(String url)
  {
    Affiliate affiliate = null;
    if (null != affiliateOperations)
    {
      try
      {
        affiliate = affiliateOperations.get(url);
      } catch (Exception e)
      {
        log.error("Exception saving affiliate to redis", e);
      }
    }
    return affiliate;
  }

  @Override
  public void set(String url, Affiliate affiliate)
  {
    if (null != affiliateOperations)
    {
      try
      {
        affiliateOperations.set("Affiliate:" + url, affiliate);
      } catch (Exception e)
      {
        log.error("Exception saving affiliate to redis", e);
      }
    }
  }

  @PostConstruct
  public void initAffililateOperations()
  {
    if ("on".equals(System.getProperty("redis.enabled")))
    {
      if (redisAffiliateTemplate != null)
      {
        this.affiliateOperations = redisAffiliateTemplate.opsForValue();
      } else
      {
        log.error("Unable to get @Autowired redisAffiliateTemplate: {}", redisAffiliateTemplate);
      }
    }
  }
}
