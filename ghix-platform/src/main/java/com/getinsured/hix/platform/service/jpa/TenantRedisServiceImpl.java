package com.getinsured.hix.platform.service.jpa;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.service.TenantRedisService;

@Component("tenantRedisService")
public class TenantRedisServiceImpl implements TenantRedisService {
	private static final Logger log = LoggerFactory.getLogger(TenantRedisServiceImpl.class);

	@Qualifier("redisTemplate")
	@Autowired(required = false) 
	private RedisTemplate<String, TenantDTO> redisTenantTemplate;

	private ValueOperations<String, TenantDTO> tenantOperations;

	
	@Override
	public TenantDTO get(String url) {
		TenantDTO tenant = null;
		if(null != tenantOperations){
			try {
				tenant = tenantOperations.get(url);
			} catch (Exception e) {

				if(log.isErrorEnabled())
				{
					log.error("Exception reading tenant from redis", e);
				}
			}
		}
		return tenant;
	}

	@Override
	public void set(String url, TenantDTO tenant) {
		if(null != tenantOperations){
			try {
				tenantOperations.set("Tenant:" + url, tenant);
			} catch (Exception e) {
				if(log.isErrorEnabled())
				{
					log.error("Exception saving tenant to redis", e);
				}
			}
		}
	}
	
	@PostConstruct
	public void initTenantOperations() {
		if ("on".equals(System.getProperty("redis.enabled")))
		{
			if (redisTenantTemplate != null)
			{
				this.tenantOperations = redisTenantTemplate.opsForValue();
			}
			else
			{
				log.error("Unable to get @Autowired redisTenantTemplate: {}", redisTenantTemplate);
			}
		}
	}
}
