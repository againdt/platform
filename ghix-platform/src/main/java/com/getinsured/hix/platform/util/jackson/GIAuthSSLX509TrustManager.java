package com.getinsured.hix.platform.util.jackson;
/*
*
* $Header: /home/cvs/jakarta-commons/httpclient/src/contrib/org/apache/commons/httpclient/contrib/ssl/AuthSSLX509TrustManager.java,v 1.1.2.1 2004/06/09 21:07:41 olegk Exp $
* $Revision: 1.1.2.1 $
* $Date: 2004/06/09 21:07:41 $
*
* ====================================================================
*
*  Copyright 2002-2004 The Apache Software Foundation
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
* ====================================================================
*
* This software consists of voluntary contributions made by many
* individuals on behalf of the Apache Software Foundation.  For more
* information on the Apache Software Foundation, please see
* .
*
*/


import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* 
*
* GIAuthSSLX509TrustManager can be used to extend the default {@link X509TrustManager} 
* with additional trust decisions.
* @since  29 Nov 2013
* @author Oleg Kalnichevski
* @Modify Biswakalyan
*
*<p>
* DISCLAIMER: HttpClient developers DO NOT actively support this component.
* The component is provided as a reference material, which may be inappropriate
* for use without additional customization.
* </p>
*/

public class GIAuthSSLX509TrustManager  implements X509TrustManager 
{
   private X509TrustManager securityTrustManager = null;

   /** Log object for this class. */
   private static final Log LOG = LogFactory.getLog(GIAuthSSLX509TrustManager.class);

   /**
    * Constructor for AuthSSLX509TrustManager.
    */
   public GIAuthSSLX509TrustManager(final X509TrustManager defaultTrustManager) {
       super();
       if (defaultTrustManager == null) {
           throw new IllegalArgumentException("Trust manager may not be null");
       }
       this.securityTrustManager =  defaultTrustManager;
   }
 
   /**
    * @see X509TrustManager#getAcceptedIssuers()
    */
   public X509Certificate[] getAcceptedIssuers() {
       return this.securityTrustManager.getAcceptedIssuers();
   }
	
	@Override
	public void checkClientTrusted(X509Certificate[] certificates, String authType)
			throws CertificateException {
		if (LOG.isInfoEnabled() && certificates != null) {
	           for (int c = 0; c < certificates.length; c++) {
	               X509Certificate cert = certificates[c];
	               LOG.info(" Client certificate " + (c + 1) + ":");
	               LOG.info("  Subject DN: " + cert.getSubjectDN());
	               LOG.info("  Signature Algorithm: " + cert.getSigAlgName());
	               LOG.info("  Valid from: " + cert.getNotBefore() );
	               LOG.info("  Valid until: " + cert.getNotAfter());
	               LOG.info("  Issuer: " + cert.getIssuerDN());
	           }
	       }
		securityTrustManager.checkClientTrusted(certificates, authType);
	}
	
	@Override
	public void checkServerTrusted(X509Certificate[] certificates, String authType)
			throws CertificateException {
	       if (LOG.isInfoEnabled() && certificates != null) {
	           for (int c = 0; c < certificates.length; c++) {
	               X509Certificate cert = certificates[c];
	               LOG.info(" Server certificate " + (c + 1) + ":");
	               LOG.info("  Subject DN: " + cert.getSubjectDN());
	               LOG.info("  Signature Algorithm: " + cert.getSigAlgName());
	               LOG.info("  Valid from: " + cert.getNotBefore() );
	               LOG.info("  Valid until: " + cert.getNotAfter());
	               LOG.info("  Issuer: " + cert.getIssuerDN());
	           }
	       }
		securityTrustManager.checkServerTrusted(certificates, authType);
		
	}
}