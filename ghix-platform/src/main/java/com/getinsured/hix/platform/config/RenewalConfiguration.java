package com.getinsured.hix.platform.config;

public class RenewalConfiguration {
	public enum RenewalConfigurationEnum implements PropertiesEnumMarker {

		RENEW_NEW_MEMBERS_HEALTH("renewal.renew_new_members_health"), 
		RENEW_INCONSISTENT_GROUP_HEALTH("renewal.renew_inconsistent_group_health"),
		CONSIDER_REMAINING_ELIG_MEMBER_FOR_DENTAL("renewal.consider.remaining.elig.member.for.dental"),
		CONSIDER_ALL_FROM_AT_OR_ENROLLMENT("renewal.consider.all.from.at.or.enrollment"),
		RENEW_DENTAL("renewal.renew.dental"),
		PERSIST_TOCONSIDER_PAYLOAD("renewal.persistToConsiderPayload"),
		ALLOW_MEDICAID_MEMBERS_IN_DENTAL("renewal.allow.medicaid.members.in.dental");
		 
		private final String value;

		@Override
		public String getValue() {
			return this.value;
		}

		private RenewalConfigurationEnum(String value) {
			this.value = value;
		}
	};
}
