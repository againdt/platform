package com.getinsured.hix.platform.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

@Component
class ApplicationHealthIndicator implements HealthIndicator {
	public ApplicationHealthIndicator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Health health() {
		Map<String, String> map = new HashMap<>();
		map.put("result", "ok");
		
		Health h = new Health.Builder(Status.UP, map).build();
		return h;
		//return Health.up().build();
	}
}
