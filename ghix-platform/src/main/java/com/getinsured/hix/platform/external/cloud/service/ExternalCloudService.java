package com.getinsured.hix.platform.external.cloud.service;

import java.net.URL;

public interface ExternalCloudService {
	
	URL upload(String relativePath, String fileName, byte[] dataBytes);

}
