/**
 * 
 */
package com.getinsured.hix.platform.security.tokens;

import java.util.List;

import com.getinsured.hix.model.UserTokens;


/**
 * @author Biswakesh
 *
 */
public interface UserTokenService {
	
	List<UserTokens> findDeprecatedTokens(String userIdentifier,String sourceName, String eventName);
	
	UserTokens findUserToken(String token);
	
	void deprecateTokens(List<UserTokens> userTokens);
	
	void createUserToken(UserTokens userTokens);
	
}
