package com.getinsured.hix.platform.giwspayload.service.jpa;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("giwsPayloadService")
public class GIWSPayloadServiceImpl implements GIWSPayloadService {


	@Autowired
	private GIWSPayloadRepository giwsPayloadRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(GIWSPayloadServiceImpl.class);
	
	@Override
	@Transactional
	public GIWSPayload save(GIWSPayload giwsPayload) {
		if (LOGGER.isDebugEnabled()){
			LOGGER.debug("Saving/Updating GIWSPayload object - "+ giwsPayload);
		}
		return giwsPayloadRepository.save(giwsPayload);
	}
	
	@Override
	public GIWSPayload findById(Integer id){
		return giwsPayloadRepository.findById(id);
	}

	@Override
	public List<GIWSPayload> findBySSAPID(Long ssapId) {
		return giwsPayloadRepository.findBySsapApplicationId(ssapId);
	}

	@Override
	public List<GIWSPayload> findByCustomKeyId1(String customKey1){
		return giwsPayloadRepository.findByCustomKeyId1(customKey1);
	}

	@Override
	public List<GIWSPayload> findByEndpointFunctionAndSSAPID(String endpointFunction, Long ssapId) {
		return giwsPayloadRepository.findBySsapApplicationId(ssapId, endpointFunction);
	}

	
}
