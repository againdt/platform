package com.getinsured.hix.platform.sms.service.jpa;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.sms.SmsResponse;
import com.getinsured.hix.platform.sms.service.SmsService;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Sms;


public class SmsTwilio implements SmsService {

	private static final int BODY_MAX_LIMIT = 160;
	private static final int TWILIO_PARAM_SIZE = 3;
	private static final String PROVIDER = "TWILIO";

	private static final Logger LOGGER = LoggerFactory.getLogger(SmsTwilio.class);

	private final TwilioRestClient client;
	private final String fromPhone;

	/**
	 * Constructor to get handle for Twilio Sms Provider.
	 * 
	 * @param client
	 * @param fromPhone
	 */
	public SmsTwilio(TwilioRestClient client, String fromPhone) {
		this.client = client;
		this.fromPhone = fromPhone;
	}

	@Override
	public SmsResponse send(String toPhone, String body, String pin) {

		if (StringUtils.isEmpty(toPhone)) {
			throw new IllegalArgumentException("To Phone number is required");
		}

		if (StringUtils.isEmpty(body)){
			throw new IllegalArgumentException("Message body is required");
		}
		
		if (StringUtils.isEmpty(pin)){
			throw new IllegalArgumentException("Pin is required");
		}

		if (body.length() > BODY_MAX_LIMIT){
			throw new IllegalArgumentException("The message body exceeds 160 character limit");
		}

		final Account mainAccount = client.getAccount();
		final SmsFactory smsFactory = mainAccount.getSmsFactory();
		final Map<String, String> smsParams = new HashMap<String, String>(TWILIO_PARAM_SIZE);

		smsParams.put("To", toPhone);
		smsParams.put("From", this.fromPhone);
		smsParams.put("Body", body+" "+pin);

		/** status for successful API call will always be 'queued'.
		 * TWILIO takes our request & returns queued status.
		 * TWILIO processes the request in an ASYNC manner and later updates the status to Sent.*/
		SmsResponse smsResponse;
		try {
			Sms sms = smsFactory.create(smsParams);
			smsResponse = new SmsResponse(PROVIDER, sms.getTo(), sms.getFrom(), sms.getBody(), sms.getStatus(), "", sms.getSid(), sms.getDirection(), sms.getDateCreated(), sms.getDateUpdated(), null);
		} catch (TwilioRestException tre) {
			LOGGER.info("Exception from Twilio ", tre);
			Date date = new TSDate();
			smsResponse = new SmsResponse(PROVIDER, toPhone, fromPhone, body, "FAILURE", tre.getMessage(), "", "", date, date, date);
		} catch (Exception e) { //some unknown exception handled..
			LOGGER.error("Error sending sms - ", e);
			Date date = new TSDate();
			smsResponse = new SmsResponse(PROVIDER, toPhone, fromPhone, body, "FAILURE", "Unknown error occured. Please try re-sending...", "", "", date, date, date);
		}

		return smsResponse;
	}

	@Override
	public SmsResponse call(String toPhone, String body, String pin) {
		LOGGER.warn("Call not implemented for Twilio provider");
		return null;
	}



}
