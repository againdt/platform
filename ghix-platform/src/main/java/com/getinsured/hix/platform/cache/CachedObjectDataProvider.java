package com.getinsured.hix.platform.cache;

import java.util.List;

public interface CachedObjectDataProvider<T extends CacheableObject> {

	List<T> fetchFromSource(String objectKey, String key );
}
