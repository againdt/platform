package com.getinsured.hix.platform.emailstat.service;


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.emailstat.model.EmailStats;
import com.getinsured.hix.platform.emailstat.repository.IEmailStatRepository;
import com.getinsured.hix.platform.util.GhixUtils;


@Service
public class EmailStatService implements IEmailStatService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailStatService.class);
	
	private @Autowired IEmailStatRepository emailStatRepo;
	
	@Override
	public void saveEmailStat(Map<GhixUtils.EMAIL_STATS, String> emailStatData, int noticeId) {
		EmailStats emailStat = new EmailStats();
		StringBuilder sb = new  StringBuilder();
		String delim = "";
		
		emailStat.setNoticeId(noticeId)	;
		for (GhixUtils.EMAIL_STATS stat : GhixUtils.EMAIL_STATS .values()) {
			String key = stat.name();
			String value = ( emailStatData.get(stat) == null ) ? "null" :  emailStatData.get(stat);
			sb.append(delim);
			delim = ",";
			sb.append( key ) ;
			sb.append( " = " ) ;
			sb.append(value);
		}
		
		LOGGER.debug("Notice ID :" + noticeId +" : Stats => " + sb.toString());
		emailStat.setMetaData(sb.toString());
		emailStatRepo.save(emailStat);	
	}
}
