package com.getinsured.hix.platform.util;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

public class ExpUtil {

	 public static String getExpByYear(String gradYear) {  
		 Calendar gradYear1 = TSCalendar.getInstance();
		 gradYear1.set(Integer.parseInt(gradYear), 1, 1);
		 Calendar today = TSCalendar.getInstance();
		 Integer exp = today.get(Calendar.YEAR)- gradYear1.get(Calendar.YEAR);
		return exp.toString();
	 }  
}
