/**
 * 
 */
package com.getinsured.hix.platform.security.scim.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.wso2.charon.core.client.SCIMClient;
import org.wso2.charon.core.exceptions.AbstractCharonException;
import org.wso2.charon.core.exceptions.BadRequestException;
import org.wso2.charon.core.exceptions.CharonException;
import org.wso2.charon.core.objects.Group;
import org.wso2.charon.core.objects.ListedResource;
import org.wso2.charon.core.objects.SCIMObject;
import org.wso2.charon.core.schema.SCIMConstants;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.scim.SCIMJSONParser;
import com.getinsured.hix.platform.security.scim.service.SCIMGroupManager;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.hub.platform.security.HubSecureHttpClient;

/**
 * @author Biswakesh.Praharaj
 * 
 */
@Service("scimGroupManager")
public class SCIMGroupManagerImpl implements SCIMGroupManager {
	private static final String ERR_WHIE_FETCHING_GRP_VIA_SCIM = "ERR: WHIE FETCHING GRP VIA SCIM: ";

	private static final Logger LOGGER = Logger
			.getLogger(SCIMGroupManager.class);

	private CloseableHttpClient httpClient = null;

	@Override
	public Group findGroup(String filter) throws GIException {
		HttpGet getMethod = null;
		String authHeader = null;
		CloseableHttpResponse responseStatus = null;
		HttpEntity response = null;
		String responseStr = null;
		Group scimGroup = null;
		SCIMClient scimClient = null;
		GIException ex = null;
		ListedResource listedResource = null;
		List<SCIMObject> groups = null;
		AbstractCharonException exception = null;
		SCIMJSONParser scimjsonParser = null;
		try {
			scimjsonParser = new SCIMJSONParser();
			getMethod = new HttpGet(
					GhixPlatformConstants.SCIM_USER_GROUP_ENDPOINT + "?"
							+ filter);
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,
					GhixPlatformConstants.SCIM_ADMIN_USER,
					GhixPlatformConstants.SCIM_ADMIN_PASS);
			getMethod.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);

			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			responseStatus = httpClient.execute(getMethod);
			response = responseStatus.getEntity();
			responseStr = scimjsonParser.getJsonResponseFromEntity(response)
					.toJSONString();
			scimClient = new SCIMClient();
			if (scimClient.evaluateResponseStatus(responseStatus
					.getStatusLine().getStatusCode())) {
				listedResource = scimClient
						.decodeSCIMResponseWithListedResource(
								responseStr,
								SCIMConstants
										.identifyFormat(SCIMClientConstants.CONTENT_TYPE),
								SCIMConstants.GROUP_INT);
				if (null != listedResource) {
					groups = listedResource.getScimObjects();
					//Commenting out this block of code as we are not using it to fetch groups
					/*if (null != groups && !groups.isEmpty()) {
						// we expect only one user in the list
						for (SCIMObject scimObject : groups) {
							scimGroup = (Group) scimObject;
							if (null != scimGroup) {
								if (LOGGER.isDebugEnabled()) {
									LOGGER.debug("GRP NAME: "
											+ scimGroup.getDisplayName());
								}
							}
						}
					}*/

				}
			} else {
				// decode scim exception and extract the specific error message.
				exception = scimClient
						.decodeSCIMException(
								responseStr,
								SCIMConstants
										.identifyFormat(SCIMClientConstants.CONTENT_TYPE));
				ex = new GIException(exception.getCode(),exception.getDescription(),"CRITICAL");
				LOGGER.error(ERR_WHIE_FETCHING_GRP_VIA_SCIM, exception);
			}
		} catch (CharonException | BadRequestException | IOException e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error(ERR_WHIE_FETCHING_GRP_VIA_SCIM, e);
		} catch (Exception e) {
			LOGGER.error(ERR_WHIE_FETCHING_GRP_VIA_SCIM, e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (responseStatus != null) {
				try {
					responseStatus.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		if (ex != null) {
			throw ex;
		}
		return scimGroup;
	}

	@Override
	public List<String> findGroups() throws GIException {
		HttpGet getMethod = null;
		String authHeader = null;
		CloseableHttpResponse responseStatus = null;
		HttpEntity response = null;
		String responseStr = null;
		SCIMClient scimClient = null;
		GIException ex = null;
		AbstractCharonException exception = null;
		List<String> groups = null;
		SCIMJSONParser scimjsonParser = null;
		try {
			scimjsonParser = new SCIMJSONParser();
			getMethod = new HttpGet(
					GhixPlatformConstants.SCIM_USER_GROUP_ENDPOINT);
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,
					GhixPlatformConstants.SCIM_ADMIN_USER,
					GhixPlatformConstants.SCIM_ADMIN_PASS);
			getMethod.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);

			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			responseStatus = httpClient.execute(getMethod);
			response = responseStatus.getEntity();
			responseStr = scimjsonParser.getJsonResponseFromEntity(response)
					.toJSONString();
			scimClient = new SCIMClient();
			if (scimClient.evaluateResponseStatus(responseStatus
					.getStatusLine().getStatusCode())) {
				groups = scimjsonParser.processListGroupsResponse(responseStr);
			} else {
				// decode scim exception and extract the specific error message.
				exception = scimClient
						.decodeSCIMException(
								responseStr,
								SCIMConstants
										.identifyFormat(SCIMClientConstants.CONTENT_TYPE));
				ex = new GIException(exception.getCode(),exception.getDescription(),"CRITICAL");
				LOGGER.error(ERR_WHIE_FETCHING_GRP_VIA_SCIM, exception);
			}
		} catch (CharonException | IOException e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error(ERR_WHIE_FETCHING_GRP_VIA_SCIM, e);
		} catch (Exception e) {
			LOGGER.error(ERR_WHIE_FETCHING_GRP_VIA_SCIM, e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (responseStatus != null) {
				try {
					responseStatus.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		if (ex != null) {
			throw ex;
		}
		return groups;
	}

	@Override
	public void addUserToGroup(String emailId, AccountUser accountUser,
			String groupName) throws GIException {
		String encodedGroup = null;
		String authHeader = null;
		Group scimGroup = null;
		SCIMClient scimClient = null;
		HttpEntity requestEntity = null;
		HttpPut httpPut = null;
		CloseableHttpResponse responseStatus = null;
		Map<String, Object> userMap = null;
		GIException ex = null;
		AbstractCharonException ace = null;
		SCIMJSONParser scimjsonParser = null;
		try {
			scimjsonParser = new SCIMJSONParser();
			if(null != groupName && groupName.length() > 0 && groupName.indexOf("YHI_") < 0) {
				groupName = "YHI_"+groupName;
			}
			scimGroup = findGroup(SCIMClientConstants.GROUP_FILTER + groupName);
			
			if(scimGroup == null){
				LOGGER.error("ERR: WHIE ADDING USR TO GRP: Group " + groupName + " not found");
				ex = new GIException("ERR: WHIE ADDING USR TO GRP: Group " + groupName + " not found");
			}else{
				userMap = new HashMap<String, Object>();
				userMap.put(SCIMClientConstants.VALUE_KEY, accountUser.getExtnAppUserId());
				userMap.put(SCIMClientConstants.DISPLAY_KEY, accountUser.getUsername());
	
				scimGroup.setMember(userMap);
	
				if (this.httpClient == null) {
					this.httpClient = HubSecureHttpClient.getHttpClient();
				}
				httpPut = new HttpPut(
						GhixPlatformConstants.SCIM_USER_GROUP_ENDPOINT + "/"
								+ scimGroup.getId());
				authHeader = Utils.getAuthorizationHeader(
						false,
						null,
						GhixPlatformConstants.SCIM_ADMIN_USER,
						GhixPlatformConstants.SCIM_ADMIN_PASS);
				httpPut.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
	
				scimClient = new SCIMClient();
				encodedGroup = scimClient.encodeSCIMObject(scimGroup,
						SCIMConstants.JSON);
	
				requestEntity = EntityBuilder.create()
						.setContentType(ContentType.APPLICATION_JSON)
						.setText(encodedGroup).build();
	
				httpPut.setEntity(requestEntity);
	
				responseStatus = httpClient.execute(httpPut);
	
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("USER GRP ADDITION RESPONSE RECEIVED FROM WSO2: "
							+ responseStatus.getStatusLine());
				}
	
				if (scimClient.evaluateResponseStatus(responseStatus
						.getStatusLine().getStatusCode())) {
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("USR: ADDED TO GRP SUCCESSFULLY");
					}
				} else {
					ace = scimClient.decodeSCIMException(scimjsonParser
							.getJsonResponseFromEntity(responseStatus.getEntity())
							.toJSONString(), SCIMConstants
							.identifyFormat(SCIMClientConstants.CONTENT_TYPE));
					LOGGER.error("ERR: WHIE ADDING USR TO GRP: ",
							ace);
					ex = new GIException(ace.getCode(),ace.getDescription(),"CRITICAL");
				}
			}
		} catch (CharonException | IOException
				| GIException e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHIE ADDING USR TO GRP: ", e);
		} catch (Exception e) {
			LOGGER.error("ERR: WHIE ADDING USR TO GRP: ", e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (responseStatus != null) {
				try {
					responseStatus.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		if (ex != null) {
			throw ex;
		}

	}

}
