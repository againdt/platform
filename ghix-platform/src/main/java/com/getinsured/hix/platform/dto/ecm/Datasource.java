package com.getinsured.hix.platform.dto.ecm;

public class Datasource {

	private String datasourceId;
	private String name;
	private String description;

	public Datasource(String datasourceId, String name, String description) {
		super();
		this.datasourceId = datasourceId;
		this.name = name;
		this.description = description;
	}

	public Datasource() {
		super();
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
