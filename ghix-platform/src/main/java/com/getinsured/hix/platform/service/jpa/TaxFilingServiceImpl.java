package com.getinsured.hix.platform.service.jpa;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.TaxYear;
import com.getinsured.hix.platform.repository.ITaxFilingRepository;
import com.getinsured.hix.platform.service.TaxFilingService;

@Service("taxFilingService")
@Transactional
public class TaxFilingServiceImpl implements TaxFilingService{
	
	private static final Logger logger = LoggerFactory.getLogger(TaxFilingServiceImpl.class);
	
	@Autowired private ITaxFilingRepository iTaxFilingRepository;	
	
	@Override
	public TaxYear findTaxFilingDateByYear(String yearOfEffectiveDate) {
		List<TaxYear> taxYearList = iTaxFilingRepository.findByYearOfEffectiveDate(yearOfEffectiveDate);	
		if(taxYearList!=null && !taxYearList.isEmpty()){
			return 	taxYearList.get(0);
		}
		return null;
	}
	
}