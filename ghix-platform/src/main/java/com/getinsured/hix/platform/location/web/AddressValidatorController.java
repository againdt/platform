package com.getinsured.hix.platform.location.web;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.model.AddressValidatorResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.location.service.AddressValidatorService;

/**
 * This is AddressValidatorController. It will be used by VIMO viewValidAddressList function to perform AJAX call.
 * 
 * @author Ekram Ali Kazi
 */
@Deprecated
@Controller
public class AddressValidatorController {

	@Autowired private AddressValidatorService addressValidatorService;

	private static final Logger LOGGER = Logger.getLogger(AddressValidatorController.class);

	@RequestMapping(value = "/platform/validateaddress", method = RequestMethod.GET)
	@ResponseBody public String validateAddress(Model model, HttpServletRequest request,
			@RequestParam("enteredAddress") String enteredAddress,
			@RequestParam("ids") String ids) {

		/* performed assert for ids, as this will be configured by the developer */
		if (ids == null || !ids.contains("~")){
			LOGGER.info("Invalid configuration for ids. ids cannot be null and should be ~ separated. Returning from AddressValidatorController.validateAddress without hitting address web srvice.");
			return "FAILURE";
		}

		Location location = null;
		if (!StringUtils.isEmpty(enteredAddress) && enteredAddress.contains(",")) {
			String[] items = enteredAddress.split("\\s*,\\s*");
			if (items.length > 0 && items.length < 6) {
				if(StringUtils.isEmpty(items[0])){ //handle input validations....
					LOGGER.info("Invalid input. Returning from AddressValidatorController.validateAddress without hitting address web srvice.");
					return "FAILURE";
				}

				location = new  Location();
				location.setAddress1(items[0]);
				location.setAddress2(items[1].equals("") ? null : items[1]);
				location.setCity(items[2]);
				location.setState(items[3]);
				location.setZip(StringUtils.isNumeric(items[4]) ? String.valueOf(Integer.parseInt(items[4])) : "0");
			}

		}

		if (location == null){
			LOGGER.info("Location object not found. Returning from AddressValidatorController.validateAddress without hitting address web srvice.");
			return "FAILURE";
		}

		List<Location> validList = getValidAddress(location, request);

		if (validList.size() > 0) {
			request.getSession().setAttribute("validateAddress", location);
			request.getSession().setAttribute("validAddressListS", validList);
			request.getSession().setAttribute("ids", ids);
			return "SUCCESS";
		}
		return "FAILURE";
	}

	@SuppressWarnings("unchecked")
	private List<Location> getValidAddress(Location address, HttpServletRequest request) {
		AddressValidatorResponse response = addressValidatorService.validateAddress(address);

		List<Location> validList = response.getList();

		validList = (List<Location>) ((validList != null) ? validList : Collections.emptyList());
		if (validList.size() > 0) {
			//LOGGER.info(Arrays.toString(validList.toArray()));
			if (StringUtils.equals(validList.get(0).getAddress1(), address.getAddress1())
					&& StringUtils.equals(validList.get(0).getAddress2(), address.getAddress2())
					&& StringUtils.equals(validList.get(0).getCity(), address.getCity())
					&& StringUtils.equals(validList.get(0).getState(), address.getState())
					&& (validList.get(0).getZip().equalsIgnoreCase(address.getZip()))
					){
				LOGGER.info("Matching adress found from web service. Skipping opening Address Selection iFrame.");
				request.getSession().setAttribute("validAddress", validList.get(0));
				return Collections.emptyList();
			}
		}else{
			LOGGER.info("No Matching adress found from web service. Skipping opening Address Selection iFrame.");
		}
		return validList;
	}

	@RequestMapping(value = "/platform/address/viewvalidaddress", method = RequestMethod.GET)
	public String getAddressList(Model model, HttpServletRequest request) {

		Location location = (Location) request.getSession().getAttribute("validateAddress");
		String ids = (String) request.getSession().getAttribute("ids");
		@SuppressWarnings("unchecked")
		List<Location> validAddressListS = (List<Location>) request.getSession().getAttribute("validAddressListS");

		model.addAttribute("validAddressList", validAddressListS);
		model.addAttribute("enteredLocation", location);
		model.addAttribute("ids", ids);

		return "platform/address/viewvalidaddress";
	}

	@RequestMapping(value = "/platform/address/viewvalidaddress", method = RequestMethod.POST)
	@ResponseBody public String viewvalidaddress(Model model, HttpServletRequest request) {

		model.addAttribute("page_title", "GetInsured Health Exchange: Address Verification page");

		return "SUCCESS";
	}

}
