package com.getinsured.hix.platform.startup;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

/**
 * SystemInfo class provides required information for developers.
 * <p>
 * Prints data during startup
 * </p>
 * <p>
 * Provides data through JSP, Web Services and JMX
 * </p>
 * <p>
 * This is platform level class and not supposed to copy to other modules.
 * </p>
 * HIX-6234 - Startup Java Code
 * 
 * @author Sunil D, Bhavani, Romin
 * @since 03/29/2013
 * 
 */
@Component
public class SystemInfo implements ApplicationListener<ContextRefreshedEvent> {

	private static final String MB_TXT = " MB";
	private static final int UNIT_SIZE = 1024;
	private static final Logger LOGGER = LoggerFactory.getLogger(SystemInfo.class);
	@Value("#{configProp['address_validator_source']}")
	private String addressValidatorComponent;

	@Value("#{configProp['ecm.type']}")
	private String ecmType;

	@Value("#{configProp['sms.provider']}")
	private String smsProvider;

	@Value("#{configProp['driver']}")
	private String dbDriver;

	@Value("#{configProp['url']}")
	private String dbURL;

	private static Map<String, Map<String, String>> sysInfoMap = new HashMap<String, Map<String, String>>();

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		// This is already called inside getSystemInfo
		// loadSystemInfo();
		if(LOGGER.isTraceEnabled()) {	
			LOGGER.trace(getSystemInfo("txt"));
		}
	}

	/**
	 * This is for Quick Testing Purpose
	 * 
	 * @param str
	 */
	public static void main(String str[]) {
		SystemInfo sysInfo = new SystemInfo();
		sysInfo.loadSystemInfo();
		if(LOGGER.isTraceEnabled()) {	
			LOGGER.trace("==>" + sysInfo.getSystemInfo("txt"));
		}
		//System.out.println(sysInfo.getSystemInfo("txt"));
	}

	/**
	 * <p>
	 * This method calls all the individual methods. Every method loads specific
	 * details as indicated in each method.
	 * </p>
	 * 
	 * @author Sunil Desu
	 */
	public void loadSystemInfo() {
		// User details
		loadUserDetails();
		// OS details
		loadOSDetails();
		// Java details
		loadJavaDetails();
		// TimeZone, Date information
		loadTimeInformation();
		// JVM Parameters
		loadJVMParameters();
		// Memory Information
		loadMemoryInformation();
		// Disk Space Information
		loadDiskSpaceInformation();
		// Application Build Number
		loadApplicationBuildNumber();
		// Application Details
		loadApplicationConfigDetails();
		// Load Database Information
		loadDatabaseInfo();
		// Load Database Information
		loadLogFileInfo();
	}

	/**
	 * <p>
	 * This method returns the SYSINFO as a string
	 * </p>
	 * 
	 * @param type
	 *            txt - for Text Output, html - for HTML Output
	 * 
	 * @return the SYSINFO information as a string.
	 */
	public String getSystemInfo(String type) {
		
		try{
			loadSystemInfo();
	
			String lineDelimiter = "";
			String sectionDelimiter = "";
			if ("html".equalsIgnoreCase(type)){
				lineDelimiter = "<br>";
				sectionDelimiter="<tr><td><hr></td></tr>";
			}else{
				lineDelimiter = "\n";
				sectionDelimiter="\n";
			}
			StringBuffer systemInfo = new StringBuffer();
			List<String> systemInfoList = new ArrayList<String>();
			// systemInfo.append("System information at ").append(new
			// Date().toString()).append(".").append(LINE_DELIMETER);
			Object obj = null;
			for (String sysInfoMapEntryKey : sysInfoMap.keySet()) {
				// systemInfo.append("Information for : ").append(sysInfoMapEntryKey).append(" starts").append(LINE_DELIMETER);
				for (String key : sysInfoMap.get(sysInfoMapEntryKey).keySet()) {
					systemInfo = new StringBuffer();
					systemInfo.append("<tr><td>");
					systemInfo.append(sysInfoMapEntryKey).append("::");
					systemInfo.append(key).append("::");
					obj = sysInfoMap.get(sysInfoMapEntryKey).get(key);
					if (null != obj){
						systemInfo.append(obj.toString());
					}else{
						systemInfo.append("");
					}
					systemInfo.append("</td></tr>");
					systemInfo.append(lineDelimiter);
					systemInfoList.add(systemInfo.toString());
				}
				systemInfoList.add(sectionDelimiter);
				//systemInfo.append(("Information for : " + sysInfoMapEntryKey + " ends.") + lineDelimiter);
				//systemInfo.append(lineDelimiter+lineDelimiter);
			}
			systemInfo.append(lineDelimiter);
			
			//Collections.sort(systemInfoList);
	
			// Sort the final list and return...this helps down the line to compare
			// result of two different systems.
			systemInfo = new StringBuffer();
			systemInfo.append("<table>\n");
			for (String temp : systemInfoList) {
				systemInfo.append(temp);
			}
			systemInfo.append("\n</table>");
			
			return systemInfo.toString();
		}
		catch (Exception e){
			LOGGER.error(e.getMessage(),e);
			return null;
		}
	}

	private void loadApplicationConfigDetails() {

		if (sysInfoMap.get("AppConfig") != null){
			return;
		}

		HashMap<String, String> hm = new HashMap<String, String>();

		hm.put("ECM Type", ecmType);
		hm.put("SMS Provider", smsProvider);
		hm.put("Address Validator Type", addressValidatorComponent);
		sysInfoMap.put("AppConfig", hm);
	}

	/**
	 * Load Java Information
	 */
	private void loadJavaDetails() {

		if (sysInfoMap.get("JavaInfo") != null){
			return;
		}

		HashMap<String, String> temp = new HashMap<String, String>();
		temp.put("java.version", System.getProperty("java.version"));
		
		String jvmModel = System.getProperty("sun.arch.data.model");
		
		if(StringUtils.equalsIgnoreCase(jvmModel, "32")){
			temp.put("JVM Bit size", jvmModel + "<b style='color:red'> Warning ! Your system is running a 32-bit JVM</b>");
		}
		else{
			temp.put("JVM Bit size", jvmModel);
		}
		temp.put("java.vendor", System.getProperty("java.vendor"));
		temp.put("java.home", System.getProperty("java.home"));

		temp.put("Boot Classpath", ManagementFactory.getRuntimeMXBean()
				.getBootClassPath());
		temp.put("Classpath", ManagementFactory.getRuntimeMXBean()
				.getClassPath());
		temp.put("Library Path", ManagementFactory.getRuntimeMXBean()
				.getLibraryPath());
		temp.put("Virtual Machine Details", ManagementFactory
				.getRuntimeMXBean().getVmName()
				+ ","
				+ ManagementFactory.getRuntimeMXBean().getVmVendor()
				+ ","
				+ ManagementFactory.getRuntimeMXBean().getVmVersion());
		sysInfoMap.put("JavaInfo", temp);
	}

	/**
	 * FIXME - Add Java Docs
	 */
	private void loadUserDetails() {
		if (sysInfoMap.get("UserInfo") != null){
			return;
		}

		HashMap<String, String> temp = new HashMap<String, String>();

		temp.put("user.name", System.getProperty("user.name"));
		temp.put("user.dir", System.getProperty("user.dir"));
		temp.put("user.home", System.getProperty("user.home"));

		sysInfoMap.put("UserInfo", temp);
	}

	private void loadOSDetails() {
		if (sysInfoMap.get("OSInfo") != null){
			return;
		}

		HashMap<String, String> temp = new HashMap<String, String>();

		temp.put("OS Architecture", ManagementFactory
				.getOperatingSystemMXBean().getArch());
		temp.put("Number of processors", "" + ManagementFactory
				.getOperatingSystemMXBean().getAvailableProcessors());
		temp.put("OS Name", ManagementFactory.getOperatingSystemMXBean()
				.getName());
		temp.put("OS Version", ManagementFactory.getOperatingSystemMXBean()
				.getVersion());

		sysInfoMap.put("OSInfo", temp);
	}

	private void loadTimeInformation() {
		if (sysInfoMap.get("TimeInfo") != null){
			return;
		}

		HashMap<String, String> temp = new HashMap<String, String>();

		temp.put("user.timezone", System.getProperty("user.timezone"));
		temp.put("user.language", System.getProperty("user.language"));
		TimeZone tz = TimeZone.getDefault();
		temp.put("Default TimeZone", tz.getDisplayName());
		temp.put("System Date/Time", new TSDate().toString());

		sysInfoMap.put("TimeInfo", temp);
	}

	/**
	 * This method loads the JVM parameters into the SYSINFO map.
	 * 
	 * @author Sunil Desu
	 */
	private void loadJVMParameters() {

		if (sysInfoMap.get("JVMArgs") != null) {
			return;
		}

		Map<String, String> jvmArgs = new HashMap<String, String>();
		try {
			LOGGER.info("Loading JVMArgs.");
			RuntimeMXBean runtimemxBean = ManagementFactory.getRuntimeMXBean();
			List<String> arguments = runtimemxBean.getInputArguments();

			StringBuffer jvmArgsAsString = new StringBuffer();

			for (String jvmParam : arguments) {
				jvmArgsAsString.append(jvmParam);
				jvmArgsAsString.append("\n");
			}
			jvmArgs.put("ARGS", jvmArgsAsString.toString());
		} catch (Exception ex) {
			LOGGER.error(""+ex);
		}
		sysInfoMap.put("JVMArgs", jvmArgs);
	}

	/**
	 * It won't cache this information.
	 */
	private void loadMemoryInformation() {
		Map<String, String> memoryInformation = new HashMap<String, String>();

		LOGGER.info("Loading memory information.");
		int mb = UNIT_SIZE * UNIT_SIZE;
		MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
		MemoryUsage muHeap = mxBean.getHeapMemoryUsage();
		MemoryUsage muNonHeap = mxBean.getNonHeapMemoryUsage();
		
		memoryInformation.clear();
		
		double spaceRatio = (double)muHeap.getUsed()/muHeap.getMax();
		spaceRatio = Math.round(spaceRatio * 100.0) / 100.0;
		
		memoryInformation.put("heapMaxMemory", muHeap.getMax() / mb + MB_TXT);
		memoryInformation.put("heapInitMemory", muHeap.getInit() / mb + MB_TXT + " (" + (spaceRatio*100) +"%)");
		memoryInformation.put("heapUsedMemory", muHeap.getUsed() / mb + MB_TXT + " (" + (spaceRatio*100) +"%)");
		
		if(spaceRatio <= 0.1){
			memoryInformation.put("heapCommittedMemory", "<b style='color:red'>"+muHeap.getCommitted() / mb	+ MB_TXT + " ("+ (spaceRatio*100) +"%) (Warning ! Low Heap Space)</b> ");
		}
		else{
			memoryInformation.put("heapCommittedMemory", muHeap.getCommitted() / mb	+ MB_TXT + " (" + (spaceRatio*100) +"%)");
		}
		
		spaceRatio = (double)muNonHeap.getUsed()/muNonHeap.getMax();
		spaceRatio = Math.round(spaceRatio * 100.0) / 100.0;
		
		memoryInformation.put("nonHeapMaxMemory", muNonHeap.getMax() / mb + MB_TXT);
		memoryInformation.put("nonHeapInitMemory", muNonHeap.getInit() / mb	+ MB_TXT + " (" + (spaceRatio*100) +"%)");
		memoryInformation.put("nonHeapUsedMemory", muNonHeap.getUsed() / mb + MB_TXT + " (" + (spaceRatio*100) +"%)");
		if(spaceRatio <= 0.1){
			memoryInformation.put("nonHeapCommittedMemory", "<b style='color:red'>"+muNonHeap.getCommitted() / mb + MB_TXT + " (" + (spaceRatio*100) +"%) (Warning ! Low Non-Heap Space)</b> ");
		}
		else{
			memoryInformation.put("nonHeapCommittedMemory", muNonHeap.getCommitted() / mb	+ MB_TXT + " (" + (spaceRatio*100) +"%)");
		}
		
		sysInfoMap.put("MemoryInfo", memoryInformation);

	}

	private void loadDiskSpaceInformation() {

		HashMap<String, String> temp = new HashMap<String, String>();
		final String dir = System.getProperty("user.dir");
		File file = new File(dir);
		// total disk space in bytes.
		long totalSpace = file.getTotalSpace();
		long usableSpace = file.getUsableSpace();
		long freeSpace = file.getFreeSpace();
		
		double spaceRatio = (double)freeSpace/totalSpace;
		spaceRatio = Math.round(spaceRatio * 100.0)/100.0;
		
		temp.put("Total size", totalSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT);
		if(spaceRatio <= 0.1){
			temp.put("Usable Space free", "<b style='color:red'>"+usableSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT + " (" + (spaceRatio*100) +"%) (Warning ! Low Disk Space)</b> ");
			temp.put("Space free", "<b style='color:red'>"+freeSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT + " (" + (spaceRatio*100) +"%) (Warning ! Low Disk Space)</b> ");
		}
		else{
			temp.put("Usable Space free", usableSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT + " (" + (spaceRatio*100) +"%)");
			temp.put("Space free", freeSpace / UNIT_SIZE / UNIT_SIZE + MB_TXT + " (" + (spaceRatio*100) +"%)");
		}
		

		sysInfoMap.put("DISKInfo", temp);
	}

	private void loadApplicationBuildNumber() {
		if (sysInfoMap.get("BuildInfo") != null){
			return;
		}

		HashMap<String, String> temp = new HashMap<String, String>();
		temp.put("Build Number", GhixPlatformConstants.BUILD_NUMBER);
		temp.put("Build Date", GhixPlatformConstants.BUILD_DATE);
		temp.put("Branch Name ", GhixPlatformConstants.BRANCH_NAME);
		sysInfoMap.put("BuildInfo", temp);
	}

	private void loadDatabaseInfo() {
		if (sysInfoMap.get("DatabaseInfo") != null){
			return;
		}

		HashMap<String, String> temp = new HashMap<String, String>();
		temp.put("Database Driver", dbDriver);
		temp.put("Database URL", dbURL);

		sysInfoMap.put("DatabaseInfo", temp);
	}

	private void loadLogFileInfo() {
		if (sysInfoMap.get("LogFileInfo") != null){
			return;
		}

		// We could probably put in a level based on isDebugEnabled(),
		// isErrorEnabled(), etc.

		HashMap<String, String> temp = new HashMap<String, String>();
		String ghixHome = System.getenv("GHIX_HOME");
		if(null == ghixHome || ghixHome.isEmpty())
		{
			//This helps in default situations and while running from STS.
			ghixHome="C:/workfolder/ghix";
		}
	    File ghixHomeDir = new File(ghixHome);
	    //We are looking for all log4j files in GHIX_HOME (except target folders)
	    StringBuffer logFileNames = new StringBuffer();
	    findLogConfigFiles(ghixHomeDir, logFileNames);
	    temp.put("LogFileInfo", logFileNames.toString());
	    
		sysInfoMap.put("LogFileInfo", temp);
	}
	
	/**
	 * Finds all log4j.xml files in a directory
	 * @author Nikhil Talreja
	 * @since Apr 5, 2013
	 * @param File file, HashMap<String, String> temp
	 * @return void
	 */
	public void findLogConfigFiles(File file, StringBuffer logFileNames) {

		if (file.isFile()
				&& file.getName().matches("^[A-Za-z0-9_\\-]*log4j.xml")
				&& !file.getAbsolutePath().toLowerCase().contains("target")) {
			logFileNames.append(file.getAbsolutePath());
			logFileNames.append(";");
		} else if (file.isDirectory()) {
			File[] listOfFiles = file.listFiles();
			if (listOfFiles != null) {
				for (int i = 0; i < listOfFiles.length; i++) {
					findLogConfigFiles(listOfFiles[i], logFileNames);
				}
			}
		}
		
	}
	
}
