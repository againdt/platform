package com.getinsured.hix.platform.dto.ecm;

public final class CMISErrors {

	public static final String NOT_YET_IMPLEMENTED = "Not Yet Implemented";
	public static final String UNSUPPORTED_DOCUMENT_TYPE = "Un-supported document type. Supported extension - txt, css, xml, csv, png, jpg, jpeg, bmp, gif, doc, docx, dot, dotx, xls, xlsx, ppt, pptx, pdf, zip, tar, gz, js";
	public static final String FILE_NAME_CANNOT_BE_NULL = "File Name cannot be null";
	public static final String BTYPEMAP_CANNOT_BE_NULL = "byteMap cannot be blank";
	public static final String DATA_BYTES_CANNOT_BE_NULL = "dataBytes cannot be null";
	public static final String FOLDER_ID_CANNOT_BE_NULL = "folderId cannot be null";
	public static final String QUERY_CANNOT_BE_NULL = "query cannot be null";
	public static final String INPUT_PARAMETERS_CANNOT_BE_NULL = "input parameters cannot be null";
	public static final String METADATA_ELEMENT_CANNOT_BE_NULL = "Metadata Element cannot be null";
	public static final String NO_CONTENT_FOUND_FOR_CONTENT_NAME = "No Content found for contentName = ";
	public static final String DOCUMENT_TYPE_CANNOT_BE_NULL = "documentType cannot be null";
	public static final String CONTENT_NAME_CANNOT_BE_NULL = "contentName cannot be null";
	public static final String CONTENT_ID_IS_NOT_A_PRIVATE_WORKING_COPY = "Content Id is not a private working copy (pwc)";
	public static final String CHECK_IN_COMMENTS_MISSING = "Check In Comments missing";
	public static final String FOLDER_NAME_CANNOT_BE_NULL = "Folder Name cannot be null";
	public static final String CONTENT_CANNOT_BE_NULL = "Content cannot be null";
	public static final String CONTENT_ID_CANNOT_BE_BLANK = "Content Id cannot be blank";
	public static final String OBJECT_ABSOLUTE_PATH_SHOULD_START_WITH_SLASH = "Object Absolute Path should start with / (slash) - ";
	public static final String OBJECT_ABSOLUTE_PATH_CANNOT_BE_BLANK = "Object Absolute Path cannot be blank";
	public static final String FOLDER_NAME_CANNOT_BE_BLANK = "Folder Name cannot be blank";
	public static final String RELATIVE_PATH_CANNOT_START_WITH_SLASH = "Relative Path cannot start with / (slash)";
	public static final String RELATIVE_PATH_CANNOT_BE_BLANK = "Relative Path cannot be blank";
	public static final String ABSOLUTE_PATH_DOESNOT_EXIST = "Absolute Path doesnot exist - ";
	public static final String INVALID_ECM_BASE_PATH = "ECM Base Path not properly configured. It should start and end with / (slash). Please contact the administrator.";
	public static final String ECM_BASE_PATH_NOT_FOUND = "ECM Base Path not found. Please contact the administrator.";
	public static final String INVALID_ECM_STATIC_CONTENT_PATH = "ECM Static Content Path not properly configured. It should start and end with / (slash). Please contact the administrator.";
	public static final String ECM_STATIC_CONTENT_PATH_NOT_FOUND = "ECM Static Content Path not found. Please contact the administrator.";
	public static final String INVALID_FILE_NAME = "Invalid file name";

	public static final String ECM_SESSION_NOT_FOUND = "ECM Session not found. Please contact the administrator.";
	public static final String ERROR_CREATING_CMIS_CONNECTION = "Error creating cmis connection.";
	public static final String ONLY_APPLICABLE_FOR_COUCH = "Method not implemented. Only applicable for COUCH configuration.";

	private CMISErrors(){}
}
