package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Event;

public interface IEventRepository extends JpaRepository<Event, Integer> {
	public Event findByEventName(String emailClass);
}
