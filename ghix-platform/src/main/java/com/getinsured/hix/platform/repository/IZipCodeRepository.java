package  com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ZipCode;

public interface IZipCodeRepository extends JpaRepository<ZipCode, Integer> {

	public List<ZipCode> findByZip(String zip);

	@Query("SELECT DISTINCT z.city FROM ZipCode z where  (LOWER(z.city) like lower(:city) || '%')")
	public List<ZipCode> getCityList(@Param("city") String city);
	
	@Query("SELECT z FROM ZipCode z where z.zip = :zip and z.state = :stateCode")
	List<ZipCode> findByZipAndStateCode(@Param("zip") String zip, @Param("stateCode") String stateCode);

	@Query("SELECT DISTINCT z.zip FROM ZipCode z where  (LOWER(z.zip) like lower(:zip) || '%')")
	public List<ZipCode> getZipCodeList(@Param("zip") String zip);

	@Query("SELECT DISTINCT z.zip FROM ZipCode z where  (LOWER(z.zip) = lower(:zip)) and (CONCAT(LOWER(z.stateFips), LOWER(z.countyFips)) = lower(:countyCode))")
	public List<ZipCode> findByZipAndCountyCode(@Param("zip") String zip, @Param("countyCode") String countyCode);

	@Query("SELECT DISTINCT z.countyFips FROM ZipCode z where  (LOWER(z.state) like lower(:state) || '%')")
	public List<String> findCountiesByState(@Param("state") String state);

	@Query("FROM ZipCode z where  (LOWER(z.state) like lower(:state) || '%') and z.countyFips=:countyFips")
	public List<ZipCode> findByCountyAndState(@Param("state") String state, @Param("countyFips") String countyFips);

	@Query("SELECT DISTINCT z.zip FROM ZipCode z where  (LOWER(z.state) like lower(:state) || '%') and z.countyFips=:countyFips")
	public List<String> findZipByCountyAndState(@Param("countyFips") String countyFips,@Param("state") String state);

	@Query("SELECT DISTINCT z.zip FROM ZipCode z where  (LOWER(z.state) like lower(:stateCode) || '%')")
	public List<String> findZipByState(@Param("stateCode") String stateCode);
	
	List<ZipCode> findByState(String state);

	List<ZipCode> findByCounty(String county);
	
	@Query("SELECT z.zip FROM ZipCode z where  z.zip in (:zips) and (CONCAT(LOWER(z.stateFips), LOWER(z.countyFips)) = lower(:countyCode))")
	public List<String> getZipByCountyCodeAndZips(@Param("zips") List<String> zips, @Param("countyCode") String countyCode);

	@Query("FROM ZipCode z where  (LOWER(z.zip) = lower(:zip)) and (CONCAT(LOWER(z.stateFips), LOWER(z.countyFips)) = lower(:countyCode))")
	public List<ZipCode> getZipByZipAndCountyCode(@Param("zip") String zip, @Param("countyCode") String countyCode);
	
	@Query("SELECT DISTINCT z.county FROM ZipCode z where  (CONCAT(LOWER(z.stateFips), LOWER(z.countyFips)) = lower(:countyCode))")
	public List<String> findCountyNameByCountyCode( @Param("countyCode") String countyCode);

	@Query("SELECT DISTINCT z.countyFips FROM ZipCode z where  (LOWER(z.state) = lower(:stateCode)) and (LOWER(z.county) = lower(:county))")
	public List<String> findCountyNameByStateAndCounty(@Param("stateCode") String stateCode, @Param("county") String county);

	@Query("SELECT z.county FROM ZipCode z where  z.zip = :zip and (lower(z.state) = lower(:state)) and z.countyFips = :countyCode")
	public List<String> findCountyNameByZipStateAndCountyCD(@Param("zip") String zip, @Param("state") String state, @Param("countyCode") String countyCode );
	
	@Query("SELECT z.stateFips FROM ZipCode z where  z.zip = :zip and (lower(z.state) = lower(:state)) and z.countyFips = :countyCode")
	public List<String> findStateFIPSByZipStateAndCountyCD(@Param("zip") String zip, @Param("state") String state, @Param("countyCode") String countyCode );
	
	@Query("FROM ZipCode z where  (LOWER(z.zip) = lower(:zip)) and (LOWER(z.state) like lower(:stateCode) || '%')")
	public List<ZipCode> findByZipAndState(@Param("zip") String zip, @Param("stateCode") String stateCode);
	
	@Query("FROM ZipCode z where  z.zip = :zip and z.countyFips = :countyCode and z.state = :stateCode ")
	public List<ZipCode> findByZipAndCountyCodeAndStateCode(@Param("zip") String zip, @Param("countyCode") String countyCode, @Param("stateCode") String stateCode);
	
	 @Query("SELECT z FROM ZipCode z where  z.zip in (:zips) and (LOWER(z.state) = lower(:stateCode))")
	 public List<ZipCode> getZipListByZipandState(@Param("zips") List<String> zips ,  @Param("stateCode") String stateCode );
	
	 @Query("SELECT z FROM ZipCode z where (LOWER(z.county) = lower(:county)) and (LOWER(z.state) = lower(:stateCode))")
	 public List<ZipCode> getZipListByCountyNameandState(@Param("county") String countyFips ,  @Param("stateCode") String stateCode );
	 
	@Query("SELECT DISTINCT z.county FROM ZipCode z where  (LOWER(z.state) like lower(:state) || '%')")
	public List<String> findCountyNameByState(@Param("state") String state);

	@Query("SELECT DISTINCT new ZipCode(z.county, z.stateFips, z.countyFips) FROM ZipCode z where z.state = :stateCode order by county")
	public List<ZipCode> findCountyNameStateFipsAndCountyFipsByState(@Param("stateCode") String stateCode);
	
	@Query("SELECT DISTINCT z.state FROM ZipCode z where (LOWER(z.areaCode) like '%' || lower(:areaCode) || '%')")
	public List<String> findStateByAreaCode(@Param("areaCode") String areaCode);
	 
	
}
