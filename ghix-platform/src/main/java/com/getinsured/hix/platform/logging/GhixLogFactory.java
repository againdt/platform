package com.getinsured.hix.platform.logging;

/**
 * This class is for wrapper factory logger for ghix application. We need to
 * call this manager for generating logger. Manager will class the
 * IGhixLogFactory which in deed return the configured logger object.
 * 
 * Now this class only return log4j2 logger. It's now tightly coupled, whenever
 * we plan to use other API will upgrade the implementation.
 * 
 * @author Biswakalyan(BK)
 * 
 */
public abstract class GhixLogFactory {

	public static GhixLogger getLogger(Class<?> clazz) {
		//TODO IGhixLogFactory should be initiated to get the logger. Now we are directly instantiating 
		IGhixLogFactory ghixLogFactory = new Log4j2LoggerFactory();
		
		return ghixLogFactory.getGhixLogger(clazz);
	}

	
}
