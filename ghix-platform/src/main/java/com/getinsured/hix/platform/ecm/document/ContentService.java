package com.getinsured.hix.platform.ecm.document;

import static com.getinsured.hix.dto.platform.ecm.CMISConstants.CONTENT_SIZE;
import static com.getinsured.hix.dto.platform.ecm.CMISConstants.MAP_SIZE;
import static com.getinsured.hix.dto.platform.ecm.CMISConstants.ZERO;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.AWS_DOCUMENT_NOT_FOUND;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.CATEGORY_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.DATA_BYTES_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.FILE_NAME_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.NOT_YET_IMPLEMENTED;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.SUB_CATEGORY_CANNOT_BE_NULL;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.content.dto.Document;
import com.getinsured.content.dto.Document.PropertyKey;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.dto.platform.ecm.MetadataElement;
import com.getinsured.hix.platform.couchbase.helper.BucketMetaHelper;
import com.getinsured.hix.platform.document.service.ContentMicroServiceDocumentClient;
import com.getinsured.hix.platform.ecm.CMISSessionUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.MediaTypeNotSupported;
import com.getinsured.hix.platform.ecm.MimeConfig;

@Service
@DependsOn("contentMicroServiceDocumentClient")
public class ContentService implements ContentManagementService {

	private static final String CHARSET_UTF_8 = "; charset=UTF-8";
	private static Logger logger = LoggerFactory.getLogger(ContentService.class);

	@Autowired private ContentMicroServiceDocumentClient contentMicroServiceDocumentClient;

	private String tenantCode;

	@Value("#{configProp['ecm.username']}")
	private String ecmUser;

	@PostConstruct
	private void post() {
		this.tenantCode = BucketMetaHelper.getTenantCode();
	}

	private static final String UPLOAD_PATH = "%s/%s/%s";

	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, boolean skipAntiVirusCheck) throws ContentManagementServiceException {
		return uploadDocument(relativePath, fileName, dataBytes, category, subCategory, type, skipAntiVirusCheck);
	}

	private String uploadDocument(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, boolean skipAntiVirusCheck) throws ContentManagementServiceException {
		String mimeType=null;
		try {
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if(sra == null) {
				mimeType = MimeConfig.getMimeConfig().validateIncomingContent(dataBytes, fileName);
			}else {
				mimeType = MimeConfig.getMimeConfig().validateMimeTypeFromSession(dataBytes,fileName) ;
			}
		} catch (MediaTypeNotSupported e) {
			throw new ContentManagementServiceException(e.getMessage(),e);
		}
		validateCategories(category, subCategory);

		if (StringUtils.isEmpty(type)) {
			type = CMISSessionUtil.getType(fileName);
		}

		Document s3Doc = formDocument(relativePath, fileName, dataBytes,
									category, subCategory, type,
									mimeType);

		return contentMicroServiceDocumentClient.createObject(String.format(UPLOAD_PATH, tenantCode, category, subCategory), s3Doc, skipAntiVirusCheck);
	}

	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type) throws ContentManagementServiceException {

		return uploadDocument(relativePath, fileName, dataBytes, category, subCategory, type, false);
	}




	private Document formDocument(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, String mimeType) {

		HashMap<PropertyKey, String> propertyMap = new HashMap<>();
		propertyMap.put(PropertyKey.CATEGORY, category);
		propertyMap.put(PropertyKey.SUB_CATEGORY, subCategory);
		propertyMap.put(PropertyKey.RELATIVE_PATH, relativePath);
		propertyMap.put(PropertyKey.FILE_NAME, fileName);
		propertyMap.put(PropertyKey.DOC_TYPE, type);
		propertyMap.put(PropertyKey.MODIFIED_BY, ecmUser);
		propertyMap.put(PropertyKey.CREATED_BY, ecmUser);


		Document s3Document = new Document();
		s3Document.setMimeType(mimeType + CHARSET_UTF_8);
		s3Document.setFileSize(dataBytes.length);
		s3Document.setData(dataBytes);
		s3Document.getProperties().putAll(propertyMap);

		return s3Document;
	}

	private void validateCategories(String category, String subCategory) {

		if (StringUtils.isEmpty(category)) {
			throw new IllegalArgumentException(CATEGORY_CANNOT_BE_NULL);
		}

		if (StringUtils.isEmpty(subCategory)) {
			throw new IllegalArgumentException(SUB_CATEGORY_CANNOT_BE_NULL);
		}

	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, boolean skipAntiVirusCheck)
			throws ContentManagementServiceException {

		return updateDocument(contentId, mimeType, newContent, skipAntiVirusCheck);
	}

	private String updateDocument(String contentId, String mimeType, byte[] newContent, boolean skipAntiVirusCheck)
			throws ContentManagementServiceException {
		String mimeTypeFromContent = null;
		if (newContent == null || newContent.length <= ZERO) {
			throw new IllegalArgumentException(DATA_BYTES_CANNOT_BE_NULL);
		}
		try {
			mimeTypeFromContent = MimeConfig.getMimeConfig().detectMimeType(new ByteArrayInputStream(newContent));
			if(!mimeTypeFromContent.equalsIgnoreCase(mimeType)) {
				logger.error("Update document content type type does not match with the content being updated, found {}, expected {}",mimeTypeFromContent,mimeType);
			}
		} catch (Exception e) {
			throw new ContentManagementServiceException(e.getMessage(),e);
		}

		Document s3Document = contentMicroServiceDocumentClient.getObject(contentId);
		if (null == s3Document) {
			throw new ContentManagementServiceException(AWS_DOCUMENT_NOT_FOUND);
		}
		s3Document.setMimeType(mimeType != null ? mimeType + CHARSET_UTF_8 : s3Document.getMimeType());
		s3Document.setData(newContent);
		s3Document.setFileSize(newContent.length);
		s3Document.setCreationDate(null);
		s3Document.setModifiedDate(null);
		return contentMicroServiceDocumentClient.updateObject(s3Document, skipAntiVirusCheck);
	}


	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent)
			throws ContentManagementServiceException {
		return updateDocument(contentId, mimeType, newContent, false);
	}



	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, String checkinNotes, String user)
			throws ContentManagementServiceException {
		return updateDocument(contentId, mimeType, newContent, false);
	}

	@Override
	public String updateContent(String contentId, String mimeType, byte[] newContent, String checkinNotes, String user, boolean skipAntiVirusCheck)
			throws ContentManagementServiceException {
		return updateDocument(contentId, mimeType, newContent, skipAntiVirusCheck);
	}


	private void updateMetaData(MetadataElement metadataElement) throws ContentManagementServiceException {
		if (StringUtils.isEmpty(metadataElement.getContentId())) {
			throw new IllegalArgumentException(FILE_NAME_CANNOT_BE_NULL);
		}
		Document s3Document = contentMicroServiceDocumentClient.getObjectMetaData(metadataElement.getContentId());
		if (null == s3Document) {
			throw new ContentManagementServiceException(AWS_DOCUMENT_NOT_FOUND);
		}
		s3Document.setMimeType(metadataElement.getMimeType());
		s3Document.getProperties().put(PropertyKey.DESCRIPTION, metadataElement.getDescription());
		s3Document.setCreationDate(null);
		s3Document.setModifiedDate(null);
		contentMicroServiceDocumentClient.updateObject(s3Document, true);
	}

	@Override
	public String updateContentMetadata(MetadataElement metadataElement) throws ContentManagementServiceException {
		updateMetaData(metadataElement);
		return metadataElement.getContentId();
	}



	@Override
	public String updateContentMetadata(MetadataElement metadataElement, List<String> tags)
			throws ContentManagementServiceException {
		updateMetaData(metadataElement);
		return metadataElement.getContentId();
	}



	@Override
	public Content getContentByPath(String relativePath) throws ContentManagementServiceException {
		return getContentByPath(relativePath, false);
	}



	@Override
	public Content getContentById(String contentId) throws ContentManagementServiceException {
		Document s3Document = contentMicroServiceDocumentClient.getObjectMetaData(contentId);
		return formContent(contentId, s3Document);
	}



	private Content formContent(String contentId, Document s3Document) {

		Content content1 = new Content();
		content1.setContentSource(Content.MSCONTENT);
		if (s3Document != null) {
			content1.setContentId(contentId);

			content1.setCreationDate(s3Document.getCreationDate());
			content1.setModifiedDate(s3Document.getModifiedDate());
			content1.setDescription(s3Document.getProperties().get(PropertyKey.DESCRIPTION));
			content1.setMimeType(s3Document.getMimeType());
			content1.setOriginalFileName(s3Document.getProperties().get(PropertyKey.FILE_NAME));
			content1.setTitle(s3Document.getProperties().get(PropertyKey.FILE_NAME));

			Map<String, String> customAttributeList = new HashMap<>(MAP_SIZE);
			customAttributeList.put(CONTENT_SIZE, s3Document.getFileSize() != ZERO
					? String.valueOf(s3Document.getFileSize())  : StringUtils.EMPTY);
			content1.setCustomAttributeList(customAttributeList);

			//content1.setContent(document.getContent()); ?? need to check
		}
		return content1;
	}



	@Override
	public byte[] getContentDataByPath(String relativePath) throws ContentManagementServiceException {
		return getContentDataById(relativePath);
	}



	@Override
	public byte[] getContentDataById(String contentId) throws ContentManagementServiceException {
		Document s3Document = contentMicroServiceDocumentClient.getObject(contentId);
		return s3Document.getData();
	}



	@Override
	public Content getContentByPath(String relativePath, boolean latestVer) throws ContentManagementServiceException {
		Content content = getContentById(relativePath);
		content.setContentId(relativePath);
		return content;
	}



	@Override
	public String createContent(String relativePath, String fileName, String dataString, String category,
			String subCategory, String type) throws ContentManagementServiceException {
		throw new ContentManagementServiceException(NOT_YET_IMPLEMENTED);
	}



	@Override
	public String deleteContent(String contentId) throws ContentManagementServiceException {
		contentMicroServiceDocumentClient.deleteObject(contentId);
		return contentId;
	}



	@Override
	public String createContent(String relativePath, String fileName, byte[] dataBytes)
			throws ContentManagementServiceException {
		throw new ContentManagementServiceException(NOT_YET_IMPLEMENTED);
	}

}
