package com.getinsured.hix.platform.config;

public class TenantConfiguration {
	
	public enum TenantConfigurationEnum implements PropertiesEnumMarker
	{  
		BOOTSTRAP_LESS_FILE ("tenant.BootstrapLessFile"),
		THEME ("tenant.themedata"),
		LOGO ("tenant.logo"),
		FOOTER_DATA ("tenant.footerdata"),
		LOGO_ALT_TEXT("ui.tenant.logo.alt.text");
		
		private final String value;	  
		
		@Override
		public String getValue(){return this.value;}
		TenantConfigurationEnum(String value){
	        this.value = value;
	    }
	};

}
