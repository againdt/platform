//package org.springframework.security.web.authentication.preauth;
package com.getinsured.hix.platform.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.owasp.esapi.AccessReferenceMap;
import org.owasp.esapi.reference.RandomAccessReferenceMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import com.getinsured.hix.platform.security.service.GhixGateWayService;


/**
 * Base class for processing filters that handle pre-authenticated authentication requests, where it is assumed
 * that the principal has already been authenticated by an external system.
 * <p>
 * The purpose is then only to extract the necessary information on the principal from the incoming request, rather
 * than to authenticate them. External authentication systems may provide this information via request data such as
 * headers or cookies which the pre-authentication system can extract. It is assumed that the external system is
 * responsible for the accuracy of the data and preventing the submission of forged values.
 *
 * <p>
 * If the security context already contains an {@code Authentication} object (either from a invocation of the
 * filter or because of some other authentication mechanism), the filter will do nothing by default. You can force
 * it to check for a change in the principal by setting the {@link #setCheckForPrincipalChanges(boolean)
 * checkForPrincipalChanges} property.
 * <p>
 * By default, the filter chain will proceed when an authentication attempt fails in order to allow other
 * authentication mechanisms to process the request. To reject the credentials immediately, set the
 * <tt>continueFilterChainOnUnsuccessfulAuthentication</tt> flag to false. The exception raised by the
 * <tt>AuthenticationManager</tt> will the be re-thrown. Note that this will not affect cases where the principal
 * returned by {@link #getPreAuthenticatedPrincipal} is null, when the chain will still proceed as normal.
 *
 * @author Luke Taylor
 * @author Ruud Senden
 * @since 2.0
 */
public class CustomAbstractPreAuthenticatedProcessingFilter extends GenericFilterBean implements
        ApplicationEventPublisherAware {
	private static final Logger logger = Logger.getLogger(CustomAbstractPreAuthenticatedProcessingFilter.class);
	protected String principalRequestHeader = "OAM_REMOTE_USER";
	protected String credentialsRequestHeader;
	protected boolean exceptionIfHeaderMissing = true;
	protected ApplicationEventPublisher eventPublisher = null;
    protected AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource
            = new WebAuthenticationDetailsSource();
    protected AuthenticationManager authenticationManager = null;
    protected boolean continueFilterChainOnUnsuccessfulAuthentication = true;
    protected boolean checkForPrincipalChanges;
    protected boolean invalidateSessionOnPrincipalChange = true;
	protected String loginSuccessUrl="/";
	protected String baseGhixAuthContext;
	
	@Autowired private GhixGateWayService ghixGateWayService;

    /**
     * Check whether all required properties have been set.
     */
    @Override
    public void afterPropertiesSet() {
        Assert.notNull(authenticationManager, "An AuthenticationManager must be set");
    }

    /**
     * Try to authenticate a pre-authenticated user with Spring Security if the user has not yet been authenticated.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String requestURL=((HttpServletRequest)request).getRequestURL().toString();
        String contextPath=((HttpServletRequest)request).getContextPath();
        String redirectUri=StringUtils.substringAfter(requestURL, contextPath);
       // String baseContext = "/hix";
      //@suneel to construct the url which includes the query string parameters like iframe=yes
        String requestHeaders = getRequestParameters((HttpServletRequest)request);
        if(logger.isDebugEnabled()) {
	        logger.debug("Checking secure context token: " + SecurityContextHolder.getContext().getAuthentication());
	        logger.debug("Request Url ::"+requestURL);
	        logger.debug("Base Ghix Auth Context ::"+baseGhixAuthContext);
	        logger.debug("requestHeaders ::"+requestHeaders);
        }
        if(StringUtils.isBlank(baseGhixAuthContext)){
        	String invalidSecurityConfigException="Invalid Ghix Security Configuration:: Missing value for baseGhixAuthContext ::"+baseGhixAuthContext;
        	logger.debug(invalidSecurityConfigException);
        	throw new ServletException(invalidSecurityConfigException);
        }


        //System.out.println("-**-Checking secure context token: " + SecurityContextHolder.getContext().getAuthentication());
        //System.out.println("-**-Request Url ::"+requestURL);


        if (requiresAuthentication((HttpServletRequest) request)) {
        	if( doAuthenticate((HttpServletRequest) request, (HttpServletResponse) response)){
 	           String ghixLoginSuccessRedirectUrl="/";
 	          	//if(contextPath.contains(baseGhixAuthContext) && !redirectUri.equals(loginSuccessUrl))
 	          if(contextPath.equals(baseGhixAuthContext) && !redirectUri.equals(loginSuccessUrl))
             	{
					Set set = new HashSet();
					set.add(redirectUri);
					AccessReferenceMap<String> map = (RandomAccessReferenceMap)((HttpServletRequest)request).getSession().getAttribute("accessReferenceMap");
					if(null == map) {
						 map = new RandomAccessReferenceMap(set);
					}
					((HttpServletRequest)request).getSession().setAttribute("acessReferenceMap", map);
					String indirectRedirectUri = map.getIndirectReference(redirectUri);
 	        	  	// Suneel: Added the request headers like iframe=yes to the uri
					ghixGateWayService.setConversationUrl((HttpServletRequest)request, redirectUri);
					
             		ghixLoginSuccessRedirectUrl=contextPath+loginSuccessUrl+"?redirectUri="+indirectRedirectUri + requestHeaders;
             		
             		logger.debug("User had athenticated, redirecting to ::"+ghixLoginSuccessRedirectUrl);
             		
     	            ((HttpServletResponse) response).sendRedirect(ghixLoginSuccessRedirectUrl);
     	            return;
             	}

             }
        }


        chain.doFilter(request, response);

    }

    /**
     * Do the actual authentication for a pre-authenticated user.
     * @throws IOException
     */
    private boolean doAuthenticate(HttpServletRequest request, HttpServletResponse response) {
        Authentication authResult;

        Object principal = getPreAuthenticatedPrincipal(request);
        Object credentials = getPreAuthenticatedCredentials(request);

        if (principal == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("No pre-authenticated principal found in request");
            }

            return false;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("preAuthenticatedPrincipal = " + principal + ", trying to authenticate");
        }

        try {
            PreAuthenticatedAuthenticationToken authRequest = new PreAuthenticatedAuthenticationToken(principal, credentials);
            authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
            authResult = authenticationManager.authenticate(authRequest);
            successfulAuthentication(request, response, authResult);

        } catch (AuthenticationException failed) {
            unsuccessfulAuthentication(request, response, failed);

            if (!continueFilterChainOnUnsuccessfulAuthentication) {
                throw failed;
            }
        }
        return true;
    }

    private boolean requiresAuthentication(HttpServletRequest request) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

        if (currentUser == null) {
        	 logger.debug("Required Authentication currentUser == null" );
             //System.out.println("-**-Required Authentication currentUser == null" );
            return true;
        }

        logger.debug("NOT required Authentication");
        //System.out.println("-**-NOT required Authentication");

        if (!checkForPrincipalChanges) {
            return false;
        }

        Object principal = getPreAuthenticatedPrincipal(request);

        if (principal == null) {
        	 logger.debug("NOT required Authentication; header check not required");
             //System.out.println("-**-NOT required Authentication; header check not required");
        	//Either no change in the HEADER or NO HEADER exists
            return false;
        }

        if (currentUser.getName().equalsIgnoreCase((String)principal)) {
            return false;
        }

        logger.debug("Pre-authenticated principal has changed to " + principal + " and will be reauthenticated");

        if (invalidateSessionOnPrincipalChange) {
            HttpSession session = request.getSession(false);

            if (session != null) {
                logger.debug("Invalidating existing session");
                session.invalidate();
                request.getSession();
            }
        }

        return true;
    }

    /**
     * Puts the <code>Authentication</code> instance returned by the
     * authentication manager into the secure context.
     */

    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {
        if (logger.isDebugEnabled()) {
            logger.debug("Authentication success: " + authResult);
        }
        SecurityContextHolder.getContext().setAuthentication(authResult);
        // Fire event
        if (this.eventPublisher != null) {
            eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
        }
    }

    /**
     * Ensures the authentication object in the secure context is set to null when authentication fails.
     * <p>
     * Caches the failure exception as a request attribute
     */
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) {
        SecurityContextHolder.clearContext();

        if (logger.isDebugEnabled()) {
            logger.debug("Cleared security context due to exception", failed);
        }
        request.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, failed);
    }

    /**
     * @param anApplicationEventPublisher
     *            The ApplicationEventPublisher to use
     */
    public void setApplicationEventPublisher(ApplicationEventPublisher anApplicationEventPublisher) {
        this.eventPublisher = anApplicationEventPublisher;
    }

    /**
     * @param authenticationDetailsSource
     *            The AuthenticationDetailsSource to use
     */
    public void setAuthenticationDetailsSource(AuthenticationDetailsSource<HttpServletRequest,?> authenticationDetailsSource) {
        Assert.notNull(authenticationDetailsSource, "AuthenticationDetailsSource required");
        this.authenticationDetailsSource = authenticationDetailsSource;
    }

    protected AuthenticationDetailsSource<HttpServletRequest, ?> getAuthenticationDetailsSource() {
        return authenticationDetailsSource;
    }

    /**
     * @param authenticationManager
     *            The AuthenticationManager to use
     */
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * If set to {@code true}, any {@code AuthenticationException} raised by the {@code AuthenticationManager} will be
     * swallowed, and the request will be allowed to proceed, potentially using alternative authentication mechanisms.
     * If {@code false} (the default), authentication failure will result in an immediate exception.
     *
     * @param shouldContinue set to {@code true} to allow the request to proceed after a failed authentication.
     */
    public void setContinueFilterChainOnUnsuccessfulAuthentication(boolean shouldContinue) {
        continueFilterChainOnUnsuccessfulAuthentication = shouldContinue;
    }

    /**
     * If set, the pre-authenticated principal will be checked on each request and compared
     * against the name of the current <tt>Authentication</tt> object. If a change is detected,
     * the user will be reauthenticated.
     *
     * @param checkForPrincipalChanges
     */
    public void setCheckForPrincipalChanges(boolean checkForPrincipalChanges) {
        this.checkForPrincipalChanges = checkForPrincipalChanges;
    }

    /**
     * If <tt>checkForPrincipalChanges</tt> is set, and a change of principal is detected, determines whether
     * any existing session should be invalidated before proceeding to authenticate the new principal.
     *
     * @param invalidateSessionOnPrincipalChange <tt>false</tt> to retain the existing session. Defaults to <tt>true</tt>.
     */
    public void setInvalidateSessionOnPrincipalChange(boolean invalidateSessionOnPrincipalChange) {
        this.invalidateSessionOnPrincipalChange = invalidateSessionOnPrincipalChange;
    }


	public String getLoginSuccessUrl() {
		return loginSuccessUrl;
	}

	public void setLoginSuccessUrl(String loginSuccessUrl) {
		this.loginSuccessUrl = loginSuccessUrl;
	}

	public String getBaseGhixAuthContext() {
		return baseGhixAuthContext;
	}

	public void setBaseGhixAuthContext(String baseGhixAuthContext) {
		this.baseGhixAuthContext = baseGhixAuthContext;
	}


	   /**
     * Read and returns the header named by {@code principalRequestHeader} from the request.
     *
     * @throws PreAuthenticatedCredentialsNotFoundException if the header is missing and {@code exceptionIfHeaderMissing}
     *          is set to {@code true}.
     */
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {

        String principal = request.getHeader(principalRequestHeader);

        if (principal == null && exceptionIfHeaderMissing) {
            throw new PreAuthenticatedCredentialsNotFoundException(principalRequestHeader
                    + " header not found in request.");
        }

        return principal;
    }

    /**
     * Credentials aren't usually applicable, but if a {@code credentialsRequestHeader} is set, this
     * will be read and used as the credentials value. Otherwise a dummy value will be used.
     */
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        if (credentialsRequestHeader != null) {
            return request.getHeader(credentialsRequestHeader);
        }

        return "N/A";
    }

    public void setPrincipalRequestHeader(String principalRequestHeader) {
        Assert.hasText(principalRequestHeader, "principalRequestHeader must not be empty or null");
        this.principalRequestHeader = principalRequestHeader;
    }

    public void setCredentialsRequestHeader(String credentialsRequestHeader) {
        Assert.hasText(credentialsRequestHeader, "credentialsRequestHeader must not be empty or null");
        this.credentialsRequestHeader = credentialsRequestHeader;
    }

    /**
     * Defines whether an exception should be raised if the principal header is missing. Defaults to {@code true}.
     *
     * @param exceptionIfHeaderMissing set to {@code false} to override the default behaviour and allow
     *          the request to proceed if no header is found.
     */
    public void setExceptionIfHeaderMissing(boolean exceptionIfHeaderMissing) {
        this.exceptionIfHeaderMissing = exceptionIfHeaderMissing;
    }

    /**
     * @param request
     * @return String in the form of encoded url with all the query string parameters
     *
     */
	private String getRequestParameters(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		int i=0;
		Enumeration e = request.getParameterNames();
		try {
			while( e.hasMoreElements() ){
				 String key = (String) e.nextElement();
				 if(key.equalsIgnoreCase("j_username".intern()) || key.equalsIgnoreCase("j_password".intern()) || key.equalsIgnoreCase("csrftoken".intern())){
					 logger.debug("Skipping sensitive username/password info from logs and header");
					 continue;
				 }
				 String[] val = request.getParameterValues( key );
				 /*if(i==0) {
					//sb.append(URLEncoder.encode("?","UTF-8"));
					sb.append("&");
					i++;
				 }
				 else {
					 //sb.append(URLEncoder.encode("&","UTF-8"));
					 sb.append("&");
				 }*/
				 sb.append("&");// we just need ampersand for forming proper url
				 sb.append(URLEncoder.encode(key,"UTF-8"));
				 //sb.append(URLEncoder.encode("=","UTF-8"));
				 sb.append("=");
				 sb.append(URLEncoder.encode(val[0],"UTF-8"));
			}
		}
		catch ( UnsupportedEncodingException usee) {
			logger.error("CustomAbstractPreAuthenticatedProcessingFilter:getRequestParameters:UnsupportedEncodingException", usee);
		}
		catch ( Exception ex) {
			logger.error("CustomAbstractPreAuthenticatedProcessingFilter:getRequestParameters:Exception", ex);
		}

		return sb.toString();

	}

	/*private void setGhixSession(AccountUser user,String roleName,HttpServletRequest request) {

    	if(StringUtils.isBlank((roleName))){
			// if the moduleName is blank (empty , whitespace or null) then moduleName=user's default role name (in lowercase)
			roleName=StringUtils.lowerCase(userService.getDefaultRole(user).getName());
		}
    	roleName = roleName.toLowerCase();
    	request.getSession().setAttribute("module",	roleName);
		request.getSession().setAttribute("switchToModuleName",roleName);
		request.getSession().setAttribute("switchToThemeName",roleName);
	}*/
}