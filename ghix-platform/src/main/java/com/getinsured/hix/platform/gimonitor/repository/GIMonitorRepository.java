package com.getinsured.hix.platform.gimonitor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.GIMonitor;

@Repository("giMonitorRepository")
public interface GIMonitorRepository extends JpaRepository<GIMonitor, Integer>{

}
