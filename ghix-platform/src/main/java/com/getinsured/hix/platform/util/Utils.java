/**
 * 
 */
package com.getinsured.hix.platform.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.apache.commons.lang.StringUtils;
/**
 * @author Biswakesh.Praharaj
 * 
 */
public class Utils {
	private static final String XML_ENTITY_PROP_PARAM = "http://xml.org/sax/features/external-parameter-entities";
	private static final String XML_ENTITY_PROP_GENERAL = "http://xml.org/sax/features/external-general-entities";
	private static final String XML_ENTITY_PROP_SECURE = "http://javax.xml.XMLConstants/feature/secure-processing";
	
	/**
     * Generates a unique Id for Authentication Requests
     *
     * @return generated unique ID
     */
    public static String createID() {

        byte[] bytes = new byte[20]; // 160 bit
        
        new SecureRandom().nextBytes(bytes);
        
        char[] charMapping = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'};

        char[] chars = new char[40];

        for (int i = 0; i < bytes.length; i++) {
            int left = (bytes[i] >> 4) & 0x0f;
            int right = bytes[i] & 0x0f;
            chars[i * 2] = charMapping[left];
            chars[i * 2 + 1] = charMapping[right];
        }

        return String.valueOf(chars);
    }

	/**
	 * Check if the given parameter is null or empty.
	 * 
	 * This method does not trim the input. So if the input is a blank space,
	 * this method will return false.
	 * 
	 * @param input
	 *            String to be validated for null and empty check
	 * @return true if the given parameter is null or empty. false otherwise.
	 */
	public static boolean isNullOrEmpty(String input) {
		return (null == input || input.length() == 0);
	}

	public static String getBase64EncodedBasicAuthHeader(String userName,
			String password) {
		String concatenatedCredential = userName + ":" + password;
		byte[] byteValue = concatenatedCredential.getBytes();
		String encodedAuthHeader = Base64.getEncoder().encodeToString(byteValue);
		encodedAuthHeader = "Basic " + encodedAuthHeader;
		return encodedAuthHeader;
	}

	public static String getAuthorizationHeader(boolean enableOAuth,
			String oauthAccessToken, String userName, String password) {
		if (enableOAuth) {
			return "Bearer " + oauthAccessToken;
		}
		return getBase64EncodedBasicAuthHeader(userName, password);
	}
	
	public static ResponseEntity<String> makeResponse(String responseStr,
			HttpStatus requestStatus, Map<String,String> responseHeaders, MediaType mediaType) {
		 HttpHeaders x = new HttpHeaders();
	     x.setContentType(mediaType);
		if(responseHeaders != null && responseHeaders.size() > 0){
			Set<Entry<String, String>> headerEntrySet = responseHeaders.entrySet();
			Iterator<Entry<String, String>> cursor = headerEntrySet.iterator();
			Entry<String, String> entry = null;
			while(cursor.hasNext()){
				entry = cursor.next();
				x.add(entry.getKey(), entry.getValue());
			}
			
		}
		ResponseEntity<String> response = new ResponseEntity<String>(responseStr, x, requestStatus);
		return response;
				
	}
	
	public static String getResponseString(HttpEntity entity){
		try {
			StringBuilder builder = new StringBuilder();
			BufferedReader sb = new BufferedReader(new InputStreamReader(entity.getContent()));
			String str = null;
			while((str = sb.readLine()) != null){
				builder.append(str);
			}
			sb.close();
			return builder.toString();
		} catch (UnsupportedOperationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static DocumentBuilderFactory getDocumentBuilderFactoryInstance()
			throws ParserConfigurationException {
		DocumentBuilderFactory factory = null;
		factory = DocumentBuilderFactory.newInstance();
		factory.setExpandEntityReferences(false);
		return factory;
	}
	
	public static void debugTrace(String caller, Logger logger, String message){
		if(logger.isDebugEnabled()){
			try{
				throw new Exception("Debug Trace initiated for "+caller);
			}catch(Exception e){
				logger.debug(message, e);
			}
		}
	}
	
	 public static boolean validateSSN(String ssn){
		 if(ssn == null || ssn.isEmpty()) {
			 return false;
		 }
		 ssn = ssn.trim().replaceAll("-", "");		   
	     //check length first
	     if (ssn.length() != 9) return false;
	      
	      if(ssn.startsWith("9") || ssn.startsWith("000") || ssn.endsWith("0000") || ssn.substring(4, 5).equalsIgnoreCase("00")) {
	    	  return false;
	      }
	      
	     return StringUtils.isNumeric(ssn);
	    }

	 static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

	 public static boolean isPureAscii(String v) {
	    return asciiEncoder.canEncode(v);
	  }

}
