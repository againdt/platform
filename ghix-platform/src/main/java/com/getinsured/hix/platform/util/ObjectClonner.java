/**
 * 
 */
package com.getinsured.hix.platform.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.exception.GIException;


/**
 * This is a utility class to create deep copies of Objects The object to be
 * cloned must implement the java.io.Serializable interface.
 * 
 * @author Nikhil Talreja
 * @since 23 November, 2012
 * 
 */

public class ObjectClonner {

	/**
	 * Clones an object using serialization
	 * 
	 * @author Nikhil Talreja
	 * @param obj
	 *            - Object to be clonned
	 * @return Object - Clonned object
	 * @throws GIException
	 *             if clonning fails
	 * 
	 */
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectClonner.class);
	
	//Creating private constructor to fix SONAR violation
	public ObjectClonner(){
		
	}
	
	public Object clone(Object obj) {

		Object clonedObj = null;

		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;

		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;

		try {
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(obj);
			
			bais = new ByteArrayInputStream(baos.toByteArray());
			ois = new ObjectInputStream(bais);
			clonedObj = ois.readObject();
			

		} catch (Exception e) {
			LOGGER.error("Object cloning failed - ", e);
		} finally {
			try {
				if (oos != null) { 
					oos.close();
				}
			} catch (IOException e) {
				LOGGER.error("Error closing Byte Stream - ", e);
			}try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				LOGGER.error("Error closing Byte Stream - ", e);
			}
			baos = null;
			oos = null;
			bais = null;
			ois = null;
			
		}
		return clonedObj;
	}

}
