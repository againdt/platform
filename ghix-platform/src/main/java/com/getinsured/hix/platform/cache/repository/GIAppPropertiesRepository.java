package com.getinsured.hix.platform.cache.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.getinsured.hix.model.GIAppProperties;

/**
 * Defines available methods to interact with repository.
 *
 * so that you don't reinvent the wheel.
 * 
 * @author golubenko
 */
public interface GIAppPropertiesRepository extends CrudRepository<GIAppProperties, Integer>
{
	GIAppProperties getByPropertyKey(String propertyKey);
	
	List<GIAppProperties> findByPropertyKeyLike(String  propertyKey);

	Iterable<GIAppProperties> findByPropertyKeyIn(Collection<String> keys);
}
