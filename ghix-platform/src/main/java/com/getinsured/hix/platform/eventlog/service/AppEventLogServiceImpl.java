package com.getinsured.hix.platform.eventlog.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AppEvent;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.eventlog.AppEventLogMapper;
import com.getinsured.hix.platform.eventlog.EventDto;
import com.getinsured.hix.platform.eventlog.repository.IAppEventRepository;

@Service("appEventLogService")
public class AppEventLogServiceImpl implements AppEventLogService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppEventLogServiceImpl.class);
	@Autowired IAppEventRepository iAppEventRepository; 
	@Autowired CommentTargetService commentTargetService;
	
	@Override
	@Async
	public void record(EventDto eventDto, final String comments) {
		LOGGER.info("Logging the event");
		
		try {
			AppEvent appEvent = new AppEventLogMapper().dtoToEntity(eventDto);
			AppEvent savedAppEvent = iAppEventRepository.save(appEvent);
			
			if(!StringUtils.isEmpty(comments)) {
				boolean bStatus = saveComments(comments, savedAppEvent);
				LOGGER.info("Comment for the event status: ", bStatus);
			}
		} catch (Exception e) {
			LOGGER.info("Error auditing the event. DB saving failed", e);
		}
		
	}

	/**
	 * Save the comment to the comment target table with TARGET_ID as AppEvent Id and TARGET_NAME as APP_EVENT
	 * @param strComment
	 * @param appEvent
	 * @return
	 */
	@Transactional
	public boolean saveComments(final String strComment, AppEvent appEvent) {
		LOGGER.info("======================== in saveComments =======================");
		boolean bStatus = false;
		if(StringUtils.isEmpty(strComment) ){
			return bStatus;
		}
		
		Comment comment = new Comment();
		comment.setComment(strComment);
		comment.setComentedBy(appEvent.getCreatedBy());
		comment.setCommenterName(appEvent.getCreatedByUserName());
		
		CommentTarget commentTarget = new CommentTarget();
		commentTarget.setTargetId(appEvent.getId().longValue());
		commentTarget.setTargetName(TargetName.APP_EVENT);
		
		List<Comment> comments = new ArrayList<Comment>();	
		comment.setCommentTarget(commentTarget);
		comments.add(comment);
		commentTarget.setComments(comments);
		
		commentTarget = commentTargetService.saveCommentTarget(commentTarget);
		
		bStatus = (commentTarget != null);
		return bStatus;
	}
}
