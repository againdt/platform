package com.getinsured.hix.platform.notify;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.ExitFlowConfigurationDTO;
import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.model.SelfServiceRegistrationConfigurationDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.UIConfigurationDTO;
import com.getinsured.hix.platform.repository.AffiliateFlowRepository;
import com.getinsured.hix.platform.repository.AffiliateRepository;
import com.getinsured.hix.platform.repository.GIAppConfigRepository;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.enums.YesNoEnum;

@Component("configurationServiceImplNew")
public class ConfigurationServiceImpl implements ConfigurationService {
	
	private static final String HTTPS = "https://";

	private static final String GLOBAL_EXCHANGE_PHONE = "global.ExchangePhone";

	private static final Logger LOGGER  = LoggerFactory.getLogger(ConfigurationServiceImpl.class);
	
	@Autowired
	private AffiliateFlowRepository affiliateFlowRepository;
	
	@Autowired
	private AffiliateRepository affiliateRepository;
	
	@Autowired
	private TenantService tenantService;
	
	@Autowired
	private GIAppConfigRepository gIAppConfigRepository;

	@Override
	public String logoUrl(Long flowId, Long affiliateId, Long tenantId) {
		String returnLogoUrl = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && StringUtils.isNotBlank(affiliateFlow.getLogoURL())) {
				returnLogoUrl = affiliateFlow.getLogoURL();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnLogoUrl)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && 
			   affiliate.getAffiliateConfig() != null && 
			   affiliate.getAffiliateConfig().getUiConfiguration() != null &&
			   affiliate.getAffiliateConfig().getUiConfiguration().getBrandingConfiguration() != null &&
			   StringUtils.isNotBlank(affiliate.getAffiliateConfig().getUiConfiguration().getBrandingConfiguration().getLogoUrl())) {
				returnLogoUrl = affiliate.getAffiliateConfig().getUiConfiguration().getBrandingConfiguration().getLogoUrl();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnLogoUrl)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && 
				tenant.getConfiguration() != null && 
				tenant.getConfiguration().getUiConfiguration() != null &&
				tenant.getConfiguration().getUiConfiguration().getBrandingConfiguration() != null &&
				StringUtils.isNotBlank(tenant.getConfiguration().getUiConfiguration().getBrandingConfiguration().getLogoUrl())) {
				returnLogoUrl = tenant.getConfiguration().getUiConfiguration().getBrandingConfiguration().getLogoUrl();
			}
		}
		return returnLogoUrl;
	}

	@Override
	public String baseUrl(Long affiliateId, Long tenantId) {
		String returnBaseUrl = null;
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && StringUtils.isNotBlank(affiliate.getUrl())) {
				returnBaseUrl = affiliate.getUrl();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnBaseUrl)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && StringUtils.isNotBlank(tenant.getUrl())) {
				returnBaseUrl = tenant.getUrl();
			}
		}
		if(StringUtils.isNotBlank(returnBaseUrl)) {
			returnBaseUrl = HTTPS + returnBaseUrl;
		}
		return returnBaseUrl;
	}

	@Override
	public String fromEmailAddress(Long affiliateId, Long tenantId) {
		String returnFromEmailAddress = null;
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && 
			   affiliate.getAffiliateConfig() != null && 
			   affiliate.getAffiliateConfig().getEmailConfiguration() != null &&
			   StringUtils.isNotBlank(affiliate.getAffiliateConfig().getEmailConfiguration().getFromAddress())) {
				returnFromEmailAddress = affiliate.getAffiliateConfig().getEmailConfiguration().getFromAddress();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnFromEmailAddress)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && 
				tenant.getConfiguration() != null && 
				tenant.getConfiguration().getEmailConfiguration() != null &&
				StringUtils.isNotBlank(tenant.getConfiguration().getEmailConfiguration().getFromAddress())) {
				returnFromEmailAddress = tenant.getConfiguration().getEmailConfiguration().getFromAddress();
			}
		}
		return returnFromEmailAddress;
	}

	@Override
	public String postalAddress(Long affiliateId, Long tenantId) {
		String returnPostalAddress = null;
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && 
			   affiliate.getAffiliateConfig() != null && 
			   affiliate.getAffiliateConfig().getEmailConfiguration() != null &&
			   StringUtils.isNotBlank(affiliate.getAffiliateConfig().getEmailConfiguration().getFooterAddress())) {
				returnPostalAddress = affiliate.getAffiliateConfig().getEmailConfiguration().getFooterAddress();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnPostalAddress)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && 
				tenant.getConfiguration() != null && 
				tenant.getConfiguration().getEmailConfiguration() != null &&
				StringUtils.isNotBlank(tenant.getConfiguration().getEmailConfiguration().getFooterAddress())) {
				returnPostalAddress = tenant.getConfiguration().getEmailConfiguration().getFooterAddress();
			}
		}
		return returnPostalAddress;
	}

	@Override
	public String customerCareNum(Long flowId, Long affiliateId, Long tenantId) {
		String returnCustomerCareNum = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && affiliateFlow.getIvrNumber() != null) {
				returnCustomerCareNum = affiliateFlow.getIvrNumber().toString();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnCustomerCareNum)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && StringUtils.isNotBlank(affiliate.getPhone())) {
				returnCustomerCareNum = affiliate.getPhone();
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnCustomerCareNum)) {
			GIAppProperties exchangePhone = gIAppConfigRepository.findByPropertyKeyAndTenantId(GLOBAL_EXCHANGE_PHONE, tenantId);
			if(exchangePhone != null) {
				returnCustomerCareNum = exchangePhone.getPropertyValue();
			}
		}
		if(StringUtils.isNotBlank(returnCustomerCareNum)) {
			returnCustomerCareNum = returnCustomerCareNum.replaceAll("\\D+","").replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		}
		return returnCustomerCareNum;
	}
	
	@Override
	public String medicareUrl(Long flowId, Long affiliateId, Long tenantId) {
		String returnMedicareUrl = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && affiliateFlow.getMedicareRedirectionUrl() != null) {
				returnMedicareUrl = affiliateFlow.getMedicareRedirectionUrl();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnMedicareUrl)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig() != null){
				returnMedicareUrl = affiliate.getAffiliateConfig().getProductUrl("MDR");
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnMedicareUrl)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && tenant.getConfiguration() != null) {
				returnMedicareUrl = tenant.getConfiguration().getProductUrl("MDR");
			}
		}
		return returnMedicareUrl;
	}

	@Override
	public ExitFlowConfigurationDTO getExitOfferConfigurations(Integer flowId, Long affiliateId, Long tenantId){
		
		ExitFlowConfigurationDTO exitFlowConfigurationDTO = null;
		/**
		 * First try to pull the exitFlowConfigurationDTO for the flowId
		 */
		if (flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId);
			if (affiliateFlow != null) {
				exitFlowConfigurationDTO = affiliateFlow.getAffiliateFlowConfig().getExitFlowConfigurations();
			}
		}  
		/**
		 * If exitFlowConfigurationDTO is still null , try to pull it for the affiliate
		 */
		if (affiliateId != null && exitFlowConfigurationDTO == null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if (affiliate != null && affiliate.getAffiliateConfig() != null) {
				exitFlowConfigurationDTO = affiliate.getAffiliateConfig().getExitFlowConfigurations();
			}
		}  
		/**
		 * If exitFlowConfigurationDTO is still null , try to pull it for the tenant
		 */
		if (tenantId != null && exitFlowConfigurationDTO == null) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if (tenant != null && tenant.getConfiguration() != null) {
				exitFlowConfigurationDTO = tenant.getConfiguration().getExitFlowConfigurations();
			}
		}
		return exitFlowConfigurationDTO;
	}

	/**
	 * The method is used to show the privacy text to the consumer on signup page
	 * @param affiliateFlowId
	 * @return
	 */
	@Override
	public String getPrivacyTextForConsumer(Integer affiliateFlowId) {
		String privacyText = null;
		
		if(affiliateFlowId == null) {
			return privacyText;
		}
		
		try {
			AffiliateFlow flow = affiliateFlowRepository.findOne(affiliateFlowId);
			UIConfigurationDTO uiConfig = flow.getAffiliateFlowConfig().getUiConfiguration();
			privacyText = (uiConfig != null) ? uiConfig.getPrivacyNotice() : privacyText;
		} catch (Exception e) {
			LOGGER.error("Invalid affiliate flow id");
		}
		
		return privacyText;
	}
	

	@Override
	public String getDisclaimerTextForEmailFooter(Long flowId, Long affiliateId, Long tenantId) {
		String returnDisclaimerContent = null;
		if(flowId != null) {
			AffiliateFlow affiliateFlow = affiliateFlowRepository.findOne(flowId.intValue());
			if(affiliateFlow != null && affiliateFlow.getAffiliateFlowConfig().getEmailConfiguration() != null) {
				returnDisclaimerContent = affiliateFlow.getAffiliateFlowConfig().getEmailConfiguration().getDisclaimerContent();
			} 
		}
		if(affiliateId != null && StringUtils.isBlank(returnDisclaimerContent)) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig().getEmailConfiguration() != null){
				returnDisclaimerContent = affiliate.getAffiliateConfig().getEmailConfiguration().getDisclaimerContent();
						//affiliate.getAffiliateConfig().getProductUrl("MDR");
			}
		}
		if(tenantId != null && StringUtils.isBlank(returnDisclaimerContent)) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && tenant.getConfiguration() != null) {
				returnDisclaimerContent = tenant.getConfiguration().getEmailConfiguration().getDisclaimerContent();
				//returnMedicareUrl = tenant.getConfiguration().getProductUrl("MDR");
			}
		}
		return GhixUtils.parseForHtmlContent(returnDisclaimerContent);
	}
	
	@Override
	public SelfServiceRegistrationConfigurationDTO getSelfServiceRegistrationConfiguration(Long affiliateId, Long tenantId) {
		SelfServiceRegistrationConfigurationDTO returnSelfServiceRegistrationConfiguration = new SelfServiceRegistrationConfigurationDTO();
		returnSelfServiceRegistrationConfiguration.setBypassD2CRegistration(SelfServiceRegistrationConfigurationDTO.BypassTypeConfig.OFF);
		returnSelfServiceRegistrationConfiguration.setBypassFFMRegistration(SelfServiceRegistrationConfigurationDTO.BypassTypeConfig.OFF);
		
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig() != null && affiliate.getAffiliateConfig().getSelfServiceRegistrationConfiguration() != null) {
				returnSelfServiceRegistrationConfiguration = affiliate.getAffiliateConfig().getSelfServiceRegistrationConfiguration();
			}
		} else if(tenantId != null) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(tenant != null && tenant.getConfiguration() != null && tenant.getConfiguration().getSelfServiceRegistrationConfiguration() != null) {
				returnSelfServiceRegistrationConfiguration = tenant.getConfiguration().getSelfServiceRegistrationConfiguration();
			}
		}
		return returnSelfServiceRegistrationConfiguration;
	}
	
	@Override
	public YesNoEnum getShowPufPlansConfiguration(Long affiliateId, Long tenantId) {
		YesNoEnum showPufPlans = YesNoEnum.NO;
		if(tenantId != null) {
			TenantDTO tenant = tenantService.getTenant(tenantId);
			if(null != tenant.getConfiguration().getPlansConfiguration() 
					&& null != tenant.getConfiguration().getPlansConfiguration().getShowPufPlans() 
					&& YesNoEnum.YES.name().equals(tenant.getConfiguration().getPlansConfiguration().getShowPufPlans().toString()) ){
				showPufPlans = YesNoEnum.YES;
			}
		}
		
		if(affiliateId != null) {
			Affiliate affiliate = affiliateRepository.findOne(affiliateId);
			if(affiliate != null && affiliate.getAffiliateConfig() != null 
					&&  affiliate.getAffiliateConfig().getUiConfiguration() != null
					&& affiliate.getAffiliateConfig().getUiConfiguration().getShowPufPlans() != null) {
				showPufPlans =  affiliate.getAffiliateConfig().getUiConfiguration().getShowPufPlans();
			}
		}
		
		return showPufPlans;
	}
}
