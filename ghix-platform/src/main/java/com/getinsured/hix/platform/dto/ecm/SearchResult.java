package com.getinsured.hix.platform.dto.ecm;

import java.util.List;




public class SearchResult {

	private Integer estimatedHitCount;
	private List<MetadataElement> resultElements;
	private String query;

	public SearchResult() {}
	public SearchResult(Integer estimatedHitCount, List<MetadataElement> resultElements) {
		this.estimatedHitCount = estimatedHitCount;
		this.resultElements = resultElements;
	}
	public Integer getEstimatedHitCount() {
		return estimatedHitCount;
	}
	public void setEstimatedHitCount(Integer estimatedHitCount) {
		this.estimatedHitCount = estimatedHitCount;
	}
	public List<MetadataElement> getResultElements() {
		return resultElements;
	}
	public void setResultElements(List<MetadataElement> resultElements) {
		this.resultElements = resultElements;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	@Override
	public String toString() {
		return "SearchResult [estimatedHitCount=" + estimatedHitCount
				+ ", resultElements=" + resultElements + ", query=" + query
				+ "]";
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
	
}
