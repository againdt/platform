package com.getinsured.hix.platform.events;

import java.util.Map;

public interface PartnerEventService {
	void publishEvent(Map<PartnerEventDataFieldEnum, String> eventData);
}
