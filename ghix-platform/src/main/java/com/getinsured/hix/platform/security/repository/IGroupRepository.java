package com.getinsured.hix.platform.security.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CapTeam;
import com.getinsured.hix.platform.repository.TenantAwareRepository;




public interface IGroupRepository extends TenantAwareRepository<CapTeam, Integer>{
	
	List<CapTeam> findAll();
	
	@Query("FROM CapTeam order by groupName")
	List<CapTeam> findSortedGroupsByName();
	
	@Query("FROM CapTeam where endDate is NULL order by groupName ")
	List<CapTeam> findActiveGroupsSortedByName();
	
	@Query("FROM CapTeam group WHERE group.groupLeadUserId IN ?1")
	List<CapTeam> findGroupByUser(List<Integer>  UserId);
	
	@Query("SELECT COUNT(*) FROM CapTeam g WHERE LOWER(g.groupName) = LOWER(:groupName) and g.endDate IS NULL")
	Long doesGroupExist(@Param("groupName")   String groupName);
	
	@Query("SELECT COUNT(*) FROM CapTeam g WHERE LOWER(g.groupName) = LOWER(:groupName) AND g.id != :groupId")
	Long doesGroupExist(@Param("groupName")   String groupName, @Param("groupId") Integer groupId);
	
	CapTeam findById(int groupId);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM CapTeam g WHERE g.id= :groupId ")
	void deleteByGroupId(@Param("groupId") Integer groupId);
	
	
	@Query("select u from AccountUser u where u.id = (select g.groupLeadUserId from  CapTeam g, CapTeamMembers ug where ug.groupId=g.id and ug.userId=:userId and ug.endDate is NULL)")
	AccountUser getUserGroupLead(@Param("userId")Integer userId);

	@Transactional
	@Modifying
	@Query("Update CapTeam ct set ct.endDate= :endDate WHERE ct.id= :groupId and ct.endDate IS NULL")
	void deleteGroupByGroupIdAndEndDate(@Param("groupId") Integer groupId,@Param("endDate") Date endDate);
}
