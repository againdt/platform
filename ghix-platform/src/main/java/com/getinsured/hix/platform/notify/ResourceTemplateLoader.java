package com.getinsured.hix.platform.notify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

import freemarker.cache.TemplateLoader;

@Component
public class ResourceTemplateLoader implements TemplateLoader{
	Logger LOGGER = LoggerFactory.getLogger(ResourceTemplateLoader.class);

	@Autowired private ApplicationContext appContext;
	
	public ResourceTemplateLoader() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void closeTemplateSource(Object source) throws IOException {
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper) source;
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Closing resource:"+((Resource)wrapper.getTemplateSource()).getURI());
		}
		Reader reader = wrapper.getSourceReader();
		if(reader != null){
			reader.close();
		}
	}

	@Override
	public Object findTemplateSource(String resourcePath) throws IOException {
		if("Y".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))){
			LOGGER.info("ECM Enabled, not using local resources for path: {}", resourcePath);
			return null;
		}

    final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

    String newPath = resourcePath;

    // If we are not on PHIX, and stateCode is two-letter state code of some sort.
    if(!stateCode.toLowerCase().contains("phix") && stateCode.length() == 2) {
      newPath = resourcePath.replace("notificationTemplate/", "notificationTemplate/" + stateCode.toLowerCase() + "/");
    }

    Resource resource = appContext.getResource("classpath:" + newPath);

		if(!resource.exists()) {
      LOGGER.warn("Template file Resource: {} does not exist", resourcePath);
      return null;
		}

		if(!resource.isReadable()){
			throw new IOException("Template file Resource:"+resourcePath+" is not readable");
		}

		LOGGER.info("Resource found: {}", resourcePath);
		return new TemplateSourceWrapper(resource);
	}

	@Override
	public long getLastModified(Object source) {
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper) source;
		Resource resource = (Resource)wrapper.getTemplateSource();
		try {
			return resource.lastModified();
		} catch (IOException e) {
			return -1l;
		}
	}

	@Override
	public Reader getReader(Object source, String encoding) throws IOException {
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper) source;
		Resource resource = (Resource)wrapper.getTemplateSource();
		LOGGER.info("Resource Reader made available for:"+resource.getURI());
		BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
		wrapper.setSourceReader(reader);
		return reader;
	}

}
