/**
 * 
 */
package com.getinsured.hix.platform.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;



/**
 * @author panda_p
 *
 */
public final class DateUtil {

	private static final long TWENTYFOUR = 24L;
	private static final long SIXTY = 60L;
	private static final long SIXTYL = 60L;
	private static final long THOUSAND = 1000L;
	private static final PeriodType YEAR_MONTH_DAY = PeriodType.yearMonthDay();
	private static final String DATE_SPLITTER  ="/";
	public enum StartEndYearEnum {START,END};
		
	/**
	 * 
	 */
	private DateUtil() {
		
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);
	/**
	 * Converts the java.util.Date to String with a user specified format
	 * 
	 * @param date (java.util.Date)
	 * @param requiredFormat
	 * @return
	 */
	public static String dateToString(java.util.Date date, String requiredFormat) {
		DateFormat formatter = new SimpleDateFormat(requiredFormat);
		return  formatter.format(date);
	}
	
	/**
	 * Converts the string to java.util.Date
	 * 
	 * @param dateStr
	 * @param format
	 * @return java.util.Date
	 */
	public static Date StringToDate(String dateStr, String format) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    Date convertedDate = null;
		try {
			convertedDate = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			if(LOGGER.isErrorEnabled())
			{
				LOGGER.error("ParseException", e);
			}
		}
	    return convertedDate;
	}
	
	/**
     * Converts the java.sql.Date to String with a user specified format
     * 
     * @param sqlDate (java.sql.Date)
     * @param requiredFormat
     * @return
     */
    public static String sqlDateToString(java.sql.Date sqlDate, String requiredFormat){
    	DateFormat df = new SimpleDateFormat(requiredFormat);
        return df.format(sqlDate);
    }
    
    /**
     * Converts the date String of one format to date String of another format
     * 
     * @param dateStr
	 * @param currentformat
     * @param requiredFormat
     * @return
     */
    public static String changeFormat(String dateStr, String currentformat, String requiredFormat){
    	String coverageStartDate = "";
    	try{
    		SimpleDateFormat sdfSource = new SimpleDateFormat(currentformat);
    		Date date = sdfSource.parse(dateStr);
    		SimpleDateFormat sdfDestination = new SimpleDateFormat(requiredFormat);
    		coverageStartDate = sdfDestination.format(date);
    	}catch(ParseException e){
			if(LOGGER.isErrorEnabled())
			{
				LOGGER.error("ParseException", e);
			}
	    }
    	return coverageStartDate;
    }
    
    /**
     * @author parhi_s
     * checks  if a date is valid or not
     * 
     * @param dateToValidate
     * @param dateFromat
     * @return boolean
     */
    public static boolean isValidDate(String dateToValidate, String dateFromat){
		if(dateToValidate == null){
			return false;
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
			sdf.setLenient(false);
			sdf.parse(dateToValidate);
		} catch (Exception e) {
			return false;
		}
 
		return true;
	}
    /**
     * @author parhi_s
     * Method to compare if two dates of same format
     * @param firstDate
     * @param secondDate
     * @return
     */
    public static boolean isEqual(Date firstDate, Date secondDate){
    	boolean isequal=false;
    	if(firstDate==null && secondDate==null){
    		isequal=true;
    	}else if(firstDate!=null && secondDate!=null){
    		isequal=firstDate.equals(secondDate);
    	}
    	return isequal;
    }
    
    /**
     * 
     * @param date
     * @param returnDateFromat
     * @param diffDay
     * @return
     */
    public static String addToDate(Date date,String returnDateFromat,int diffDay){
    	
    	Calendar cal = TSCalendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.DATE, diffDay);
    	Date newDate = cal.getTime();
    	return dateToString(newDate, returnDateFromat);
    }
    
    /**
     * 
     * @param date
     * @param returnDateFromat
     * @param diffDay
     * @return
     */
    public static Date addToDate(Date date,int diffDay){
    	
    	Calendar cal = TSCalendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.DATE, diffDay);
    	Date newDate = cal.getTime();
    	return newDate;
    }
    
    /**
     * 
     * @param startDate
     * @param endDate
     * @return true if start date is before end date.
     */
    public static boolean isValidDateInterval(Date startDate, Date endDate){
    	
    	Calendar startcal = TSCalendar.getInstance();
    	startcal.setTime(startDate);
    	
    	Calendar endcal = TSCalendar.getInstance();
    	endcal.setTime(endDate);
    	
    	return startcal.before(endcal);
    	
    }

    /**
     * @param xmlCalender
     * @return date string in dd-MMM-yyyy
     */
    public static String getStringDate(XMLGregorianCalendar xmlCalender){
    	String date=null;
    	if(xmlCalender!= null){
    		date = getDateAsString(xmlCalender.toGregorianCalendar());
    	}
    	return date;
	}

 /**
     * @param xmlCalender
     * @param format
     * @return date string in specified format
     */
    public static String getStringDate(XMLGregorianCalendar xmlCalender,String format){
    	String date=null;
    	if(xmlCalender!= null){
    		date = getDateAsString(xmlCalender.toGregorianCalendar(),format);
    	}
    	return date;
    }


   /**
     * @param calender
     * @return date string in dd-MMM-yyyy
     */
    public static String getDateAsString(Calendar calender){
    	if(calender!= null){
    		return getDateAsString(calender,"dd-MMM-yyyy");
    	}
    	return null;
    }
    
    /**
     * @param calender
     * @param format
     * @return date string in specified format
     */
    public static String getDateAsString(Calendar calender,String format){
    	if(calender!= null && format!=null){
    		return dateToString(calender.getTime(),format);
    	}
    	return null;
    }
 
    
    /**
     * 
     * @param compareDate
     * @return long Difference between todays date and inout date.
     * @throws ParseException 
     */
    public static long getDateIntervalInDaysInputFormatyyyyMMddHHmmssS(Date compareDate) { 

    	Date date = new TSDate(); 
    	long diff =  date.getTime() -  compareDate.getTime();
    	return  diff / THOUSAND / SIXTYL / SIXTY / TWENTYFOUR ;
    
    }
    
    /**
     * @author shanbhag_a
     * @param date
     * @return
     */
    public static Date getMonthStartDate(Date date) {
		Date startDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DAY_OF_MONTH, 1);  
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 000);
			startDay = cal.getTime();
		}
		return startDay;
	}
    
    /**
     * 
     * @param compareDate
     * @return long Difference between todays date and inout date.
     * @throws ParseException 
     */
    public static long getDateIntervalInDaysInputFormatyyyyMMddHHmmssS(String compareDate) { 
    	SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    	Date tempDate=null;
		try {
			tempDate = simpledateformat.parse(compareDate);
		} catch (ParseException e) {
			if(LOGGER.isErrorEnabled())
			{
				LOGGER.error("ParseException", e);
			}
		}
    	Date date = new TSDate(); 
    	long diff =  date.getTime() -  tempDate.getTime();
    	return  diff / THOUSAND / SIXTYL / SIXTY / TWENTYFOUR ;
    
    }
    
    /**
     * Remove the time part from date. Set default HHmmss as 000000
     * @param date
     * @return date
     */
    public static Date removeTime(Date date) {
        Calendar cal = TSCalendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    public static Date getLastPossibleTimeOfTheDay(Date date) {
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE,      calendar.getMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND,      calendar.getMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
    
    public static boolean equateDate(Date dateOne, Date dateTwo){
    	Calendar calOne = TSCalendar.getInstance();
    	calOne.setTime(dateOne);
    	
    	Calendar calTwo = TSCalendar.getInstance();
    	calTwo.setTime(dateTwo);
    	
    	if( ( calOne.get(Calendar.YEAR)== calTwo.get(Calendar.YEAR) ) 
    			&& ( calOne.get(Calendar.MONTH)== calTwo.get(Calendar.MONTH) ) 
    			  && ( calOne.get(Calendar.DATE)== calTwo.get(Calendar.DATE) ) ){
    		return true;
    	}else{
    		return false;
    	}
    	}
    
    public static boolean equateDateYear(Date dateOne, Date dateTwo){
    	Calendar calOne = TSCalendar.getInstance();
    	calOne.setTime(dateOne);
    	
    	Calendar calTwo = TSCalendar.getInstance();
    	calTwo.setTime(dateTwo);
    	
    	if(calOne.get(Calendar.YEAR) == calTwo.get(Calendar.YEAR)){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public static Date getYearStartDate(Date date,StartEndYearEnum startEnd){
    	Calendar calOne = TSCalendar.getInstance();
    	calOne.setTime(date);
    	if(startEnd.equals(StartEndYearEnum.START)){
    	calOne.set(Calendar.DATE, calOne.getMinimum(Calendar.DATE));
    	calOne.set(Calendar.HOUR_OF_DAY,calOne.getMinimum(Calendar.HOUR_OF_DAY));
    	calOne.set(Calendar.MINUTE, calOne.getMinimum(Calendar.MINUTE));
    	calOne.set(Calendar.SECOND, calOne.getMinimum(Calendar.SECOND));
    	calOne.set(Calendar.MONTH, calOne.getMinimum(Calendar.MONTH));
    	}else{
        	calOne.set(Calendar.DATE, calOne.getMaximum(Calendar.DATE));
        	calOne.set(Calendar.HOUR_OF_DAY,calOne.getMaximum(Calendar.HOUR_OF_DAY));
        	calOne.set(Calendar.MINUTE, calOne.getMaximum(Calendar.MINUTE));
        	calOne.set(Calendar.SECOND, calOne.getMaximum(Calendar.SECOND));
        	calOne.set(Calendar.MONTH, calOne.getMaximum(Calendar.MONTH));
    	}
    	return calOne.getTime();
    }
    
    /**
     * 
     * @param date
     * @return year
     */
    public static Integer getYearFromDate(Date date){
    	
    	if(date == null)
    	   return null;
    	
    	Calendar cal = TSCalendar.getInstance();
    	cal.setTime(date);
    	return cal.get(Calendar.YEAR);
    	
    }
    
    /**
     * 
     * @param String
     * @return year
     */
    public static Integer getYearFromDate(String dateStr, String format){
    	
    	Date date = StringToDate(dateStr, format);
    	
    	if(date == null)
    	   return null;
    	
    	Calendar cal = TSCalendar.getInstance();
    	cal.setTime(date);
    	return cal.get(Calendar.YEAR);
    	
    }
    
    public static int getAgeFromtodaysDate(Date date)
    {
    	LocalDate personBirthdate = new LocalDate(date);
    	LocalDate todaysDate = new LocalDate(new TSDate());
    	Period period = new Period(personBirthdate, todaysDate, YEAR_MONTH_DAY);
	    return period!=null ? period.getYears():0;
    }
    
    
    
    /*
     * 
     * PHIX : getEffectiveDate() returns the effective date based on the following logic , 
     *        effectiveDateStr is optional (mm/dd/YYYY format if passed) and will be used in issuers flow,
     *        where effective dates can be set to a custom specific date.
     * 
     * 1. During the OEP (if OEP flag is set to ON), Effective date is computed based on current day of month 
     * i.e : if current date is Nov-10-2014 then effective date is Dec-1-2014
     *     : if current date is Nov-16-2014 then effective date is Jan-1-2015 
     * 2. If the OEP is OFF then we use the 1st of next month logic.
     * 	i.e. if the if current date is Nov-16-2014 then effective date is Dec-1-2014 .
     * 
     * 3. IF OEP ON and current Date < OEP start Date then effective Date is Jan 01 <Year from OEP start Date> +1
     * 	 ie. if current Date is Nov 03 2014 then effective date is Jan 01 2015
     *        if current Date is Nov 16 2014 then  Effective Date is computed based on the logic described on step 1.
     * 4. IF OEP is turned OFF then Effective Date computation is based on 1st of next month Rule.
     * 		i.e.  if current date is Nov-16-2014 then effective date is Dec-1-2014      
     *			  if current date is Nov-10-2014 then effective date is Dec-1-2014
     * 
     * */
	public static Calendar getEffectiveDate(String effectiveDateStr)
	{

		String oepFlag = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OEP_FLAG);
		String oepDate = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OEP_START_DATE);
		Calendar calendarObj = null;
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();
		Calendar Nov15calendarObj = TSCalendar.getInstance();

		//FFM Extended date for Jan 01 coverage till Dec 17 th- so  Hard reseting the effective date to Jan 01 2016
		Nov15calendarObj.set(Calendar.MONTH, Calendar.DECEMBER);//Nov15calendarObj.set(Calendar.MONTH,Calendar.NOVEMBER );
		Nov15calendarObj.set(Calendar.DAY_OF_MONTH, 19);//Nov15calendarObj.set(Calendar.DAY_OF_MONTH, 15);
		Nov15calendarObj.set(Calendar.HOUR_OF_DAY, 0);
		Nov15calendarObj.set(Calendar.MINUTE, 0);
		Nov15calendarObj.set(Calendar.SECOND, 0);
		Nov15calendarObj.set(Calendar.MILLISECOND, 0);

		boolean isOepStartsBeforeNov15 = false;
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);

		// For testing if current date is less than Nov 15 set the effective date to Jan 1 2015.
		Calendar oepStartDate = TSCalendar.getInstance();
		if (StringUtils.isNotEmpty(oepDate))
		{
			String[] dateArr = oepDate.split(DATE_SPLITTER);
			oepStartDate.set(Calendar.MONTH, Integer.valueOf(dateArr[0]) - 1);
			oepStartDate.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateArr[1]));
			oepStartDate.set(Calendar.YEAR, Integer.valueOf(dateArr[2]));
			Nov15calendarObj.set(Calendar.YEAR, Integer.valueOf(dateArr[2]));
			if (Integer.valueOf(dateArr[0]) == 11 && Integer.valueOf(dateArr[1]) < 15)
			{
				isOepStartsBeforeNov15 = true;
			}
		}
		// Check if there effectiveDateStr is present
		if (StringUtils.isNotEmpty(effectiveDateStr))
		{
			calendarObj = TSCalendar.getInstance();
			String[] dateArr = effectiveDateStr.split(DATE_SPLITTER);

			calendarObj.set(Calendar.HOUR_OF_DAY, 0);
			calendarObj.set(Calendar.MINUTE, 0);
			calendarObj.set(Calendar.SECOND, 0);
			calendarObj.set(Calendar.MILLISECOND, 0);

			calendarObj.set(Calendar.MONTH, Integer.valueOf(dateArr[0]) - 1);
			calendarObj.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateArr[1]));
			calendarObj.set(Calendar.YEAR, Integer.valueOf(dateArr[2]));

			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("EffectiveDate for issuer flow is: {}", new TSDate(calendarObj.getTimeInMillis()));
			}
		} else if (null != oepFlag && oepFlag.equalsIgnoreCase("ON") && (currentDate.compareTo(oepStartDate) < 0 || (isOepStartsBeforeNov15 && currentDate.compareTo(Nov15calendarObj) <= 0)))
		{
			effectiveDate.set(Calendar.MONTH, 0);
			effectiveDate.set(Calendar.YEAR, currentDate.get(Calendar.YEAR) + 1);
			effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
			effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
			effectiveDate.set(Calendar.MINUTE, 0);
			effectiveDate.set(Calendar.SECOND, 0);
			effectiveDate.set(Calendar.MILLISECOND, 0);

			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("EffectiveDate is {}", new TSDate(effectiveDate.getTimeInMillis()));
			}

			return effectiveDate;
		} else
		{
			if (null != oepFlag && oepFlag.equalsIgnoreCase("ON"))
			{
				//Current Date is on or before 15 of month
				if (currentDate.get(Calendar.DAY_OF_MONTH) <= 15)
				{
					effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);
				}
				//Current Date is after 15 of month
				else
				{
					effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 2);
				}

			} else
			{
				// Next MONTH 1st Rule.
				effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);
			}
			effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
			effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
			effectiveDate.set(Calendar.MINUTE, 0);
			effectiveDate.set(Calendar.SECOND, 0);
			effectiveDate.set(Calendar.MILLISECOND, 0);

			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("EffectiveDate is {}", new TSDate(effectiveDate.getTimeInMillis()));
			}

			return effectiveDate;
		}

		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("EffectiveDate is " + new TSDate(calendarObj.getTimeInMillis()));
		}

		return calendarObj;

	}


	/**
	 * Adds days and time in provided date. Add negative values to change date and time 
	 * in the past from provided date.
	 * 
	 * @param date - Date to be changed.
	 * @param diffDay - Difference in days from provided date.
	 * @param diffHour - Difference in hours from provided date.
	 * @param diffMin - Difference in minutes from provided date.
	 * @param diffSec - Difference in seconds from provided date.
	 * @return Changed date.
	 */
	public static Date addToDate(Date date, int diffDay, int diffHour, int diffMin, int diffSec){
	     Calendar cal = TSCalendar.getInstance();
	     cal.setTime(date);
	     cal.add(Calendar.DATE, diffDay);
	     cal.add(Calendar.HOUR, diffHour);
	     cal.add(Calendar.MINUTE, diffMin);
	     cal.add(Calendar.SECOND, diffSec);
	     return cal.getTime();
	}
	
	/**
	 * Check if " dateToCheck " is inbetween min and max date
	 * @param min
	 * @param max
	 * @param dateToCheck
	 * @return if " dateToCheck " is inbetween min and max date
	 */
	public static boolean checkIfInBetween(Date min, Date max, Date dateToCheck)
	{
		return dateToCheck.after(min) && dateToCheck.before(max);
	}
	
	/**
	 * 
	 * @param requestDate
	 * @return
	 */
	public static Integer getDateOfMonth(Date requestDate){
		if(requestDate != null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(requestDate);
			return cal.get(Calendar.DAY_OF_MONTH);
		}else{
			return null;
		}
	}
}
