package com.getinsured.hix.platform.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class  Log4j2LoggerFactory implements IGhixLogFactory {
	  
	public Log4j2LoggerFactory() {
		
	 }

	@Override
	public GhixLogger getGhixLogger(Class<?> clazz) {
		GhixLogger logger = loggerMap.get(clazz.getName());
	    if (logger != null) {
	      return logger;
	    } else {
	      Logger log4j2Logger = LogManager.getLogger(clazz);
	      
	      GhixLogger newInstance = new Log4j2LoggerAdaptor(log4j2Logger);
	      GhixLogger oldInstance = loggerMap.putIfAbsent(clazz.getName(), newInstance);
	      return oldInstance == null ? newInstance : oldInstance;
	    }
	}

}
