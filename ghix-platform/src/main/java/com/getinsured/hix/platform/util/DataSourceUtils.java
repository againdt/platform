package com.getinsured.hix.platform.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import java.sql.Connection;

import com.getinsured.hix.platform.util.exception.GIException;

public class DataSourceUtils {
	
	public static boolean isConnectionRecoverable(String sqlState){
		if(sqlState.startsWith("0800".intern())){
			return true;
		}
		return false;
	}
	
	public static PreparedStatement getPreparedStatement(JdbcTemplate jdbcTemplate, Connection connection, String queryTemplate) throws GIException{
		int connectionAttempt = 0;
		SQLException sqe  = null;
		String errMsg = null;
		PreparedStatement stmt = null;
		while(connectionAttempt < 3){
			if(sqe != null){
				if(!isConnectionRecoverable(sqe.getSQLState())){
					break;
				}
			}
			try{
				// Check if existing connection is there and its valid, wait for 1 sec for the validity check, if not close and get a new one
				if(connection != null && !connection.isValid(1000)){
					connection.close();
					connection = null;
				}else{
					connection = jdbcTemplate.getDataSource().getConnection();
				}
				stmt = connection.prepareStatement(queryTemplate);
				connectionAttempt = 0;
			}catch(SQLException se){
				sqe = se;
				connectionAttempt++;
			}
		}
		if(stmt == null){
			if(sqe != null){
				errMsg = "SQL State:"+sqe.getSQLState()+" SQL Code"+sqe.getErrorCode()+ " SQL Exception Message:"+sqe.getMessage();
			}
			throw new GIException("Non recoverable error, Failed to initize the prepared statement, Err Message"+errMsg);
		}
		return stmt;
	}

}
