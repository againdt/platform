package com.getinsured.hix.platform.lookup.taglib;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * @author ajinkya m
 * 
 */

public class DropdownList extends TagSupport {
	private static final long serialVersionUID = 1L;

	private LookupService lookupService;

	private String name;
	private String value;
	private String lookuptype;
	private String cssclass;
	private String cssstyle;
	private String id;
	private String onchange;
	
	private List<LookupValue> dropdownList;

	private JspWriter out;

	private void setProperties() {
		this.out = pageContext.getOut();
	}

	private String createDropdown() {

		dropdownList = lookupService.getLookupValueList(this.lookuptype);

		StringBuilder dropdown = new StringBuilder("<select");
		
		if (name != null && !name.equals("")){
			dropdown.append(" name=" + this.name);
		}
		if (onchange != null && !onchange.equals("")){
			dropdown.append(" onchange='" + this.onchange + ";'");
		}
		if (id != null && !id.equals("")){
			dropdown.append(" id=" + this.id);
		}
		
		if (cssclass != null && !cssclass.equals("")){
			dropdown.append(" class=" + this.cssclass);
		}
		if (cssstyle != null && !cssstyle.equals("")){
			dropdown.append(" style=" + this.cssstyle);
		}
		
		dropdown.append( ">");
		for (LookupValue lookupValue : dropdownList) {

			dropdown.append("<option value=" + SecurityUtil.encodeForHTML(lookupValue.getLookupValueCode()));
			if (value != null && value.equals(lookupValue.getLookupValueCode())){
				dropdown.append(" selected");
			}
			dropdown.append( ">"+ SecurityUtil.encodeForHTML(lookupValue.getLookupValueLabel()) + "</option>");
		}

		dropdown.append("</select>");

		return dropdown.toString();
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		autowireDependencies();
		try {
			out.write(this.createDropdown());
		} catch (Exception ex) {
			throw new JspException("Error in DropDown tag", ex);
		}
		return SKIP_BODY;
	}
	
	private void autowireDependencies() {
		ApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext(pageContext.getRequest(), pageContext.getServletContext());
		lookupService = (LookupService) applicationContext.getBean("lookupService");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLookuptype() {
		return lookuptype;
	}

	public void setLookuptype(String lookupType) {
		this.lookuptype = lookupType;
	}

	public String getCssclass() {
		return cssclass;
	}

	public void setCssclass(String cssclass) {
		this.cssclass = cssclass;
	}

	public String getCssstyle() {
		return cssstyle;
	}

	public void setCssstyle(String cssstyle) {
		this.cssstyle = cssstyle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
