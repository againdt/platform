package com.getinsured.hix.platform.config;

public class GlobalConfiguration {

	public enum GlobalConfigurationEnum implements PropertiesEnumMarker
	{
		
		EXCHANGE_TYPE ("global.ExchangeType"),

		STATE_NAME ("global.StateName"),

		STATE_CODE ("global.StateCode"),

		EXCHANGE_NAME ("global.ExchangeName"),
		
		MEDICAID_NAME ("global.MedicaidName"),

		EXCHANGE_PHONE ("global.ExchangePhone"),
		
		EXCHANGE_FAX("global.ExchangeFax"),

		EXCHANGE_URL ("global.ExchangeURL"),

		EXCHANGE_SIGNATURE ("global.ExchangeSignature"),

		CITY_NAME ("global.CityName"),

		PIN_CODE ("global.PinCode"),

		EXCHANGE_ADDRESS_1 ("global.ExchangeAddress1"),

		EXCHANGE_ADDRESS_2 ("global.ExchangeAddress2"),

		EXCHANGE_FULL_NAME ("global.ExchangeFullName"),
		//
		TODAYS_DATE ("global.TodaysDate"),

		@Deprecated
		EXCHANGE_ADDRESS_LINE_ONE ("global.ExchangeAddressLineOne"),

		COUNTRY_NAME ("global.CountryName"),

		DEFAULT_TENANT ("global.DefaultTenant"),
		FFM_DOWN ("global.ffmDown"),

		CALLCENTER_ADDRESS_1 ("global.CallcenterAddress1"),

		CALLCENTER_ADDRESS_2 ("global.CallcenterAddress2"),

		CALLCENTER_CITY ("global.CallcenterCity"),

		CALLCENTER_STATE ("global.CallcenterState"),

		EXCHANGE_ADDRESS_EMAIL ("global.ExchangeAddressEmail"),

		CALLCENTER_PINCODE ("global.CallcenterPincode"),

		EXCHANGE_USERS ("global.ExchangeUsers"),
		FFM_ENABLED_FOR_CONSUMERS ("global.ffmEnabledForConsumers"),
		ECMTEMPLATEFOLDERPATH("global.EcmTemplateFolderPath"),
		SSAP_APPLICATION_TYPE("global.SSAPApplicationType"),

		USEECMTEMPLATE("global.UseEcmTemplate"),

		STATE_EXCHANGE_TYPE("global.StateExchangeType"),

		SECURE_WEB_SVC_URLS("global.secureWebSvcUrls"),

		PRIVACY_URL("global.privacyURL"),
		
		PRIVACY_URL_ESP("global.privacyURLEsp"),
		
		EXCHANGE_BASED_APTC_CSR_CALCULATION("global.isExchangeBasedAptcCsrCalculation"),

		USEPRINTMAIL("global.UsePrintMail"),
		PRINTMAILPATH("global.PrintMailPath"),
		
		RIDPCHECK("global.ridp.check.required"),
		
		MAX_BCC_LIMIT("global.maxListInBccLimit"),
		
		OPEN_ENROLLMENT_START_DATE("global.open.enrollment.start.date"),
		
		OPEN_ENROLLMENT_END_DATE("global.open.enrollment.end.date"),
		
		SSO_ENABLED("global.SsoEnabled"),
		
		LCE_REFERRAL_EXPIRATION_DAYS("global.referral.lce.expiration.days"),
		
		REFERRAL_AUTOMATIC_LINKING_EFFECTIVE_DATE("referral.AutomaticLinkingEffectiveDate"),
		
		TIME_ZONE("global.timezone"),
		CDNURL("global.CdnUrl"),
		CDN_ACTIVE_CONTAINER("global.CdnActiveContianer"),
		CDN_RESOURCE_TYPE("global.Cdn.Resource.Type"),   /* possible values - STATIC, DYNAMIC, BOTH */
		
		OEP_START_DATE("global.OEP.StartDate"),
		
		OEP_END_DATE("global.OEP.EndDate"),
		
		PRE_OEP_START_DATE("global.PREOEP.Startdate"),
		
		PENALTY_PERCHILD_AMOUNT("global.Penalty.PerChild.Amount"),
		
		PENALTY_PERPERSON_AMOUNT("global.Penalty.PerPerson.Amount"),
		
		PENALTY_PERCENTAGE_HOUSEHOLD("global.Penalty.Percentage.HouseHold"),
				
		OEP_FLAG("global.OEP"),
		
		ENABLEMULTILANGUAGE("global.EnableMultiLanguage"),
		
		ROLE_LIST("ticket.RoleList"),
		
		LCE_ENROLLMENT_DAYS_GRACE_PERIOD("iex.lce.enrollmentEndDate.gracePeriod"),
		
		EXCHANGE_LOGIN_URL("global.exhangeLoginURL"),
		
		ALLOW_FUTURE_EFFECTIVE_DATES("global.AllowFutureEffectiveDates"),
		
		PASSIVE_ENROLLMENT_NOTIFICATION_ROLES("iex.passive.enrollment.notification.roles"),

		QE_REFERRAL_EXPIRATION_DAYS("iex.referral.qe.expiration.days"),
		
		LCE_RECURRINGNOTICE_DENIED_FREQUENCY("iex.lce.reccuringnotice.denied.frequency"),
		
		LCE_RECURRINGNOTICE_DENIED_BUFFER("iex.lce.reccuringnotice.denied.buffer"), 
		
		LCE_RECURRINGNOTICE_PENDING_FREQUENCY("iex.lce.reccuringnotice.pending.frequency"),
		
		LCE_RECURRINGNOTICE_PENDING_BUFFER("iex.lce.reccuringnotice.pending.buffer"),
		
		LCE_RECURRINGNOTICE_SEPGRANTED_FREQUENCY("iex.lce.reccuringnotice.sepgranted.frequency"),
		
		LCE_RECURRINGNOTICE_SEPGRANTED_BUFFER("iex.lce.reccuringnotice.sepgranted.buffer"),
		
		ONLINE1_DENIAL_REASON_ENGLISH("iex.english.denialreason.online1"),
		
		ONLINE1_DENIAL_REASON_SPANISH("iex.spanish.denialreason.online1"),
		
		SEP_NUMBER_OF_DAYS_TO_SUBMIT_AN_APPEAL("iex.sep.denied.days.to.appeal"),
		
		LCE_PERF_MODE("iex.lce.perfmode"),

		LCE_SEP_DENIAL_DAYS("iex.lce.sep.denial.days"),
		
		EXIT_SURVEY_EMAIL("global.Email.ExitSurveyEmail"),
		
		CART_ABANDONMENT_FIRST_INTERVAL_EMAIL("global.Email.CartAbandFirstEmail"),
		
		CART_ABANDONMENT_SECOND_INTERVAL_EMAIL("global.Email.CartAbandSecondEmail"),
		
		CART_ABANDONMENT_THIRD_INTERVAL_EMAIL("global.Email.CartAbandThirdEmail"),
		
		DEFAULT_MEDICARE_URL("global.medicareURL"),
		
		DEFAULT_NPN("global.DefaultNPN"), 
		ENVIRONMENT("global.environment"),
/*		
		SSAP_AUTO_RENEWAL_PARENTAPP_YEAR("iex.ssap.autorenewal.parentappyear"),
		
		SSAP_AUTO_RENEWAL_RENEWAL_YEAR("iex.ssap.autorenewal.renewalyear"),*/
		
		SSAP_AUTO_RENEWAL_BATCHSIZE("iex.ssap.autorenewal.batchsize"), 
		
		SSAP_AUTO_RENEWAL_CHILDRELATIONCODE("iex.ssap.autorenewal.childrelationcode"), 
		SSAP_AUTO_RENEWAL_ACTIVEENROLLMENTSTATUSLIST("iex.ssap.autorenewal.activeenrollmentstatuslist"),
		PRINTPAPER_NOTICE_COMMIT_INTERVAL("global.PrintNoticeCommitInterval"),
		PRINTPAPER_NOTICE_SKIP_FILEPATH("global.PrintNoticeFailedPath"),
		
		EXTERNAL_NOTICE_COMMIT_INTERVAL("iex.external.notices.batch.commit.interval"),
		EXTERNAL_NOTICE_DAYS_TO_CONSIDER("iex.external.notices.batch.days.toconsider"),
		
		SCREENER_FLAG("global.screener"),
		SCREENER_URL("global.screener.url"),
		GLOBAL_JIRAUSER("global.JiraUser"),
		GLOBAL_JIRAPASS("global.JiraPass"), PRIVACY_STATEMENT("global.notices.privacy_statement"), CONTACT_INFORMATION("global.notices.contact_information"),

		//HIX-101432 
		GLOBAL_MEDICARE_FLAG("global.medicareFlag"),
		EXCHANGE_ELIGIBILITY_ENABLE("iex.AT.exchange.eligibility.enable"),
		
		EXCHANGE_AT_GENERATE_MEMBER_IDS("iex.AT.generateMemberIds"),

		AT_CAPTURE_COVERAGE_YEAR("iex.at.capture.coverage.year"),
		GLOBAL_PREVENT_ADD_CONSUMER("global.preventAddConsumer"),
		EXCHANGE_START_YEAR("global.exchangeStartYear"),
		STATE_SUBSIDY("global.enableStateSubsidy"),

		IEX_BROKER_CONNECT_ENABLED("iex.brokerConnectEnabled"),

		//HIX-115045 Add configuration for age out job
		SSAP_AGE_OUT_INSURANCE_TYPE("iex.ssap.ageout.insurance.type"),
		SSAP_AGE_OUT_APPLICATION_TYPE("iex.ssap.ageout.application.type"),

		APPLY_APTC_TO_DENTAL("global.applyAptcToDental"),
		GLOBAL_INBOX_NOTICE_COUNT_INTERVAL("global.inboxNoticeCount.interval");
		
		private final String value;

		@Override
		public String getValue(){return this.value;}

		GlobalConfigurationEnum(String value){
	        this.value = value;
	    }
	};

}
