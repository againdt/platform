package com.getinsured.hix.platform.util;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PreemptiveAuthInterceptor implements HttpRequestInterceptor {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
		logger.debug("Processing Preemptive authentication");
        HttpClientContext finalContext = (HttpClientContext) context;
        AuthState authState = finalContext.getTargetAuthState();
        // If no auth scheme available yet, try to initialize it preemptively
        if (authState.getAuthScheme() == null) {
            CredentialsProvider credsProvider = finalContext.getCredentialsProvider();
            HttpHost targetHost = finalContext.getTargetHost();
            Credentials creds = credsProvider.getCredentials(
                    new AuthScope(targetHost.getHostName(), targetHost.getPort()));
            if (creds != null) {
            	BasicScheme authScheme = new BasicScheme();
                authState.update(authScheme, creds); 
            }else {
            	logger.debug("No credetials available for {} at port {}",targetHost.getHostName(), targetHost.getPort());
            }
            
        }else {
        	logger.debug("Auth state already exists, nothing to do");
        }
    }
}