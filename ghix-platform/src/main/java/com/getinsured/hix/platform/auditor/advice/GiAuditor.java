package com.getinsured.hix.platform.auditor.advice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditDto;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;


public class GiAuditor {
	public static final String STATUS_ATTRIBUTE_KEY = "auditStatus";
	private static final String UNKNOWN_FAILURE_REASON = "Unknown failure reason";
	private static final String ERROR_RECORDING_ADUIT_INFO_IN_GI_AUDITOR = "Error recording aduit info in GiAuditor --> ";
	private static final String UNABLE_TO_FIND_LOGGED_IN_USER = "unable to find logged in user";
	private static final String ANONYMOUS_USER = "Anonymous";
	private static final String HTTP_HEADER_REFERER = "referer";
	private static final String HTTP_HEADER_X_FORWARDED_FOR = "X-FORWARDED-FOR";
	private static final String HTTP_HEADER_USER_AGENT = "User-Agent";
	public static final String FAILURE = "FAILURE";
	public static final String SUCCESS = "SUCCESS";
	public static final String STATUS_ATTRIBUTE_MESSAGE="Reason ";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GiAuditor.class);

	@Autowired private UserService userService;

	@Autowired private GiAuditorService giAuditorService;

	public Object doAround(ProceedingJoinPoint point, GiAudit giAudit) throws Throwable {

		String status = SUCCESS;
		Object result = null;
		String exceptionMessage = null;

		try {
			GiAuditContextHolder.clear();
			result = point.proceed();
		} catch (Throwable e) {
			status = FAILURE;
			exceptionMessage = e != null ? e.toString() : e.getMessage() != null ? e.getMessage() : UNKNOWN_FAILURE_REASON;
			throw e;
		} finally {
			recordInfo(point, giAudit, status, exceptionMessage);
		}

		return result;
	}

	private void recordInfo(ProceedingJoinPoint point, GiAudit giAudit, String status, String exceptionMessage) {
		try {

			GiAuditDto giAuditDto = GiAuditDto.create();
			/* User Info */
			setUserInfo(giAuditDto);

			/* Controller Info */
			if(RequestContextHolder.getRequestAttributes() != null){
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			String controllerMethodName = MethodSignature.class.cast(point.getSignature()).getMethod().getName();

			setControllerInfo(giAuditDto, status, request, controllerMethodName);

			/* Browser Info */
			setBrowserInfo(giAuditDto, request);
			}
			

			/* Method Audit Info */
			setAuditEventInfo(giAuditDto, giAudit);
			
			/* Parameter Info */
/*
 			List<String> argumentsTolog = Arrays.asList(giAudit.argumentsName());
			List<String> parameters = new ArrayList<String>();
			for (Object parameter : point.getArgs()) {

				if (parameter instanceof org.springframework.ui.Model){
					org.springframework.ui.Model modelParameters = (org.springframework.ui.Model) parameter;
					Map<String, Object> modelMap = modelParameters.asMap();
					for (Entry<String, Object> e : modelMap.entrySet()) {
						if (argumentsTolog.contains(e.getKey())){
							parameters.add(e.getKey() + "=" + e.getValue());
						}
					}
				}
			}

*/			
			List<String> parameters = new ArrayList<String>();
			Map<String,Object> mp = GiAuditContextHolder.get();
			Set<Entry<String, Object>> values =  mp.entrySet();
			Entry<String, Object> entry;
			for (Iterator<Entry<String, Object>> iterator = values.iterator(); iterator.hasNext();) {
				entry = iterator.next();
				parameters.add(entry.getKey() + "=" + entry.getValue());				
			}
			setParameters(giAuditDto, parameters);

			/* Exception Info */
			setException(giAuditDto, exceptionMessage);

			giAuditorService.doAudit(giAuditDto);
		} catch(Exception e) {
			/** log and eat exception to give chance to get response back to the caller. Something went wrong with GiAuditor recording */
			LOGGER.error(ERROR_RECORDING_ADUIT_INFO_IN_GI_AUDITOR + ExceptionUtils.getFullStackTrace(e));
		}
	}

	private void setException(GiAuditDto giAuditDto, String exceptionMessage) {
		giAuditDto.setExceptionInfo(exceptionMessage);

	}

	private void setParameters(GiAuditDto giAuditDto, List<String> parameters) {
		giAuditDto.setParameterInfo(parameters);

	}

	private GiAuditDto setAuditEventInfo(GiAuditDto giAuditDto, GiAudit giAudit) {
		giAuditDto.setAuditEventInfo(giAudit);
		return giAuditDto;
	}

	private GiAuditDto setBrowserInfo(GiAuditDto giAuditDto, HttpServletRequest request) {
		String userAgent = request.getHeader(HTTP_HEADER_USER_AGENT);


		String ipAddress = request.getHeader(HTTP_HEADER_X_FORWARDED_FOR);
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		giAuditDto.setBrowserInfo(ipAddress, userAgent);

		return giAuditDto;
	}

	private GiAuditDto setControllerInfo(GiAuditDto giAuditDto, String status, HttpServletRequest request, String controllerMethodName) {
		String referrer = request.getHeader(HTTP_HEADER_REFERER);
		giAuditDto.setControllerInfo(status, controllerMethodName, request.getRequestURL().toString(), referrer, request.getRequestedSessionId(), request.isRequestedSessionIdValid() );
		return giAuditDto;
	}

	private GiAuditDto setUserInfo(GiAuditDto giAuditDto) {
		AccountUser user = null;
		try {
			Object userObj = userService.getLoggedInUser();
			if (userObj instanceof AccountUser) {
				user = (AccountUser) userObj;
			}
		} catch (InvalidUserException e) {
			LOGGER.warn(UNABLE_TO_FIND_LOGGED_IN_USER);
		}

		if (user != null){
			String fname = user.getFirstName();
			String lName = user.getLastName();
			String userFqn = lName+","+fname+"<"+user.getUsername()+">";
			giAuditDto.setLoggedInUserInfo(user.getId(), userFqn, user.getActiveModuleId(), user.getActiveModuleName(), user.getAuthorities());
		} else {
			giAuditDto.setAnonymousUserInfo(ANONYMOUS_USER);
		}
		return giAuditDto;
	}

}