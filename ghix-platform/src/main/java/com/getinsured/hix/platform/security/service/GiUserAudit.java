package com.getinsured.hix.platform.security.service;

import com.getinsured.hix.platform.auditor.GiAuditParameter;

public interface  GiUserAudit {
	public void createDeActivateAudit(GiAuditParameter...auditParameters );
}
