package com.getinsured.hix.platform.security.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.CapTeamLeaders;
import com.getinsured.hix.platform.repository.TenantAwareRepository;



public interface ITeamLeaderRepository  extends TenantAwareRepository<CapTeamLeaders, Integer>{

	CapTeamLeaders findById(int groupId);	
	
	@Query("select tl from CapTeamLeaders tl where tl.groupId=:groupId and tl.userId = :userId and tl.endDate IS  NULL")
	CapTeamLeaders findByGroupIdAndUserIdAndEndDate(@Param("groupId") Integer groupId, @Param("userId") Integer userId);
	
	/**
	 * @author kaul_s
	 * @return void
	 * @param groupId
	 */
	@Transactional
	@Modifying
	@Query("Update CapTeamLeaders ctl set ctl.endDate= :endDate WHERE ctl.groupId= :groupId and ctl.endDate IS NULL")
	void deleteByGroupAndEndDate(@Param("groupId") Integer groupId, @Param("endDate") Date endDate);
}
