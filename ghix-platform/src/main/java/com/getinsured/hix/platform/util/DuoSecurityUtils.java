package com.getinsured.hix.platform.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.http.MediaType;

import com.duosecurity.client.Base64;
import com.duosecurity.client.Util;

public class DuoSecurityUtils {

	public static SimpleDateFormat RFC_2822_DATE_FORMAT = new SimpleDateFormat(
			"EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);
	
	//public static final int SIG_VERSION = 2;
	public static final int SIG_VERSION = 1;

	public static MediaType FORM_ENCODED = MediaType.APPLICATION_FORM_URLENCODED;

	public static HashMap<String, String> getAuthorizationHeader(String method, String host, String uri, String ikey, String skey,HashMap<String, String> params)
			throws UnsupportedEncodingException {
		HashMap<String, String> headers = new HashMap<String, String>();
		String date = RFC_2822_DATE_FORMAT.format(TSDate.getNoOffsetTSDate(System.currentTimeMillis()));
		String canon = canonRequest(method, host, uri, date, SIG_VERSION, params);
		String sig = signHMAC(skey, canon);

		String auth = ikey + ":" + sig;
		String header = "Basic " + Base64.encodeBytes(auth.getBytes());
		headers.put("Authorization", header);
		headers.put("Host", host);
		//headers.put("Date", date);
		return headers;
	}

	public static String signHMAC(String skey, String msg) {
		try {
			byte[] sig_bytes = Util.hmacSha1(skey.getBytes(), msg.getBytes());
			String sig = Util.bytes_to_hex(sig_bytes);
			return sig;
		} catch (Exception e) {
			return "";
		}
	}

	private static String canonRequest(String method, String host,
			String uri, String date, int sig_version,
			HashMap<String, String> params) throws UnsupportedEncodingException {
		String canon = "";
		if (sig_version == 2) {
			canon += date + "\n";
		}
		canon += method.toUpperCase() + "\n";
		canon += host.toLowerCase() + "\n";
		canon += uri + "\n";
		canon += createQueryString(params);

		return canon;
	}

	private static String createQueryString(HashMap<String, String> params)
			throws UnsupportedEncodingException {
		ArrayList<String> args = new ArrayList<String>();
		ArrayList<String> keys = new ArrayList<String>();

		for (String key : params.keySet()) {
			keys.add(key);
		}

		Collections.sort(keys);

		for (String key : keys) {
			String name = URLEncoder.encode(key, "UTF-8").replace("+", "%20")
					.replace("*", "%2A").replace("%7E", "~");
			String value = URLEncoder.encode(params.get(key), "UTF-8")
					.replace("+", "%20").replace("*", "%2A")
					.replace("%7E", "~");
			args.add(name + "=" + value);
		}

		return join(args.toArray(), "&");
	}

	public static byte[] hmacSha1(byte[] key_bytes, byte[] text_bytes)
			throws NoSuchAlgorithmException, InvalidKeyException {
		Mac hmacSha1;

		try {
			hmacSha1 = Mac.getInstance("HmacSHA1");
		} catch (NoSuchAlgorithmException nsae) {
			hmacSha1 = Mac.getInstance("HMAC-SHA-1");
		}

		SecretKeySpec macKey = new SecretKeySpec(key_bytes, "RAW");
		hmacSha1.init(macKey);
		return hmacSha1.doFinal(text_bytes);
	}


	// Taken from:
	// http://stackoverflow.com/questions/1515437/java-function-for-arrays-like-phps-join/1515548#1515548
	private static String join(Object[] s, String glue) {
		int k = s.length;
		if (k == 0)
			return "";
		StringBuilder out = new StringBuilder();
		out.append(s[0]);
		for (int x = 1; x < k; ++x)
			out.append(glue).append(s[x]);
		return out.toString();
	}

}
