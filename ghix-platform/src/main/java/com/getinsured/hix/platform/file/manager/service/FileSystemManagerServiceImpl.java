package com.getinsured.hix.platform.file.manager.service;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.util.GhixUtils;

@Service
public class FileSystemManagerServiceImpl extends AbstractFileManagerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemManagerServiceImpl.class);
	
	@Override
	public boolean uploadFile(String fileName, byte[] dataBytes, String floderName)  {
		boolean status = false;
		LOGGER.info("Uploading file to file system on local server");
		Path fileDirectory;
		try {
			//0. This is important to validate the path before communication
			if(GhixUtils.isGhixValidPath(getBasePath()+File.separator+ floderName+File.separator+fileName)){
				//1.Create the directory structure
				fileDirectory = Files.createDirectories(Paths.get(getBasePath(), floderName));
				//2. Create the file absolute path
				Path filePath = Paths.get(fileDirectory.toString(), fileName);
				
				//3. Delete if already present
				//Files.deleteIfExists(filePath);
				//Change in step 3, Create a copy if file is already present
				if(Files.exists(filePath)){
					LOGGER.info("File with same name exists: "+fileName);
					filePath = Paths.get(fileDirectory.toString(), renameFile(fileName, fileDirectory.toString()));
				}
				
				//4. Create the file content
				Path file = Files.write(Files.createFile(Paths.get(filePath.toString())), dataBytes);
				//5. Set status on successfully file creation 
				status = Files.exists(file);
			}else{
				//We can't stop over here Just Print a error log and continue with the process
				LOGGER.error("Application trying to reach a blacklisted folder or filename or extension type.");
			}
			
		} catch (IOException exp) {
			status = false;
			LOGGER.error("Error occured while creating the file. IOException: "+exp.getMessage(),exp);
		}
		return status;
	}
	
	
	/**
	 * 
	 * @param fileName
	 * @param directory
	 * @return
	 */
	private String renameFile(String fileName, String directory){
		String fileExtension = null;
		String searchFileName = null;
		if(fileName.contains(".")){
			searchFileName = fileName.substring(0, fileName.lastIndexOf("."));
			fileExtension = fileName.substring(fileName.lastIndexOf("."), fileName.length());
			if(fileName.contains("~")){
				searchFileName = fileName.substring(0, fileName.lastIndexOf("~") -1);
			}
		}
		File[] fileArray = getFilesInAFolderByName(directory, searchFileName);
		if(fileArray != null){
			int file_count = fileArray.length;
			searchFileName = searchFileName + "~" + file_count +fileExtension;
		}
		return searchFileName;
	}
	
	/**
	 * 
	 * @param commonLocation
	 * @param responseType
	 * @return
	 */
	private File[] getFilesInAFolderByName(String commonLocation, String responseType) {
		File[] files = null;
		try{
			File folder= new File(commonLocation);
			final String finalResponseType=responseType;
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if(pathname.isFile() && pathname.getName().contains(finalResponseType)){
						return true;
					}
					return false;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location", e);
		}
		return files;
	}

}
