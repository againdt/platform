package com.getinsured.hix.platform.util.ui;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class IFrameGlobalInterceptor extends HandlerInterceptorAdapter {

	private static Log LOG = LogFactory.getLog(IFrameGlobalInterceptor.class);
	private static final Matcher URL_MATCHER = Pattern.compile("(/hix/admin.*|/hix/broker.*|/hix/assister.*|/hix/entity.*|/hix/issuer.*)",
			Pattern.CASE_INSENSITIVE).matcher("");
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		if(LOG.isTraceEnabled()){
			LOG.trace("Received the request in IFrameGlobalInterceptor");
		}
		// Set the iFrame variable over here
		// First determine if it is present or not
		// All values other than "yes" are considered to be "no"
		String iframe = request.getParameter("iframe");
		if (null != iframe) {
			if (iframe.equalsIgnoreCase("yes")) {
				iframe = "yes";
			} else {
				iframe = "no";
			}
		} else {
			//Check for specific module if iframe is in Session scope.
			String requestUrl = request.getRequestURI();
			try{
				if (requestUrl!=null && URL_MATCHER.reset(requestUrl).matches()){
					iframe = "no";
				}else{
					// Check in session if it is there
					String iframefromsession = (String) request.getSession()
							.getAttribute("iframe");
					if (null != iframefromsession) {
						if (iframefromsession.equalsIgnoreCase("yes")) {
							iframe = "yes";
						} else {
							iframe = "no";
						}
					} else {
						iframe = "no";
					}
				}
			}catch(Exception e){
				LOG.warn("Error encountered while checking if iFrame is available for URl "+requestUrl);
				iframe = "no";
			}
		}
		
		request.getSession().setAttribute("iframe", iframe);
		return super.preHandle(request, response, handler);
	}
}
