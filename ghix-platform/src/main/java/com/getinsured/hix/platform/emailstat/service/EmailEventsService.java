package com.getinsured.hix.platform.emailstat.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.emailstat.model.EmailEvents;
import com.getinsured.hix.platform.emailstat.repository.IEmailEventsRepository;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.GhixUtils.EMAIL_STATS;

@Service
public class EmailEventsService implements IEmailEventsService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailEventsService.class);
	private @Autowired IEmailEventsRepository emailEventsRepo;

	@Override
	public void saveEmailEvent(Map<String, Object> emailEventData) {
		Integer noticeId = null;
		String event = null;
		try {
			noticeId = (emailEventData.get("NoticeId") != null && !emailEventData.get("NoticeId").toString().isEmpty() ) ? Integer.valueOf(emailEventData.get("NoticeId").toString()) : null ;
			event = (emailEventData.get("event") != null && !emailEventData.get("event").toString().isEmpty() ) ? emailEventData.get("event").toString() : null;
		} catch (Exception e) {
			LOGGER.error("Notice ID passed from sendgrid not a number "+emailEventData.toString());
			return;
		}
		
		
		if(noticeId == null || event == null){
			LOGGER.error("Notice ID / event not passed from sendgrid. Skipping the execution. "+emailEventData.toString());
			return;
		}
	
		EmailEvents emailEventDBObj = new EmailEvents();
		emailEventDBObj.setNoticeId(noticeId);
		emailEventDBObj.setEvent(event);
		emailEventDBObj.setEventDate(new TSDate());
		emailEventDBObj.setStateCode(emailEventData.get(EMAIL_STATS.STATE_CODE.name()).toString());
		emailEventDBObj.setExchangeUrl(emailEventData.get(EMAIL_STATS.EXCHANGE_URL.name()).toString());
		emailEventsRepo.save(emailEventDBObj);
	}

	@Override
	public boolean validateEmailEvent(Map<String, Object> emailStat) {
		boolean validRequest = false;
		
		if(null == emailStat.get("NoticeId") || null == emailStat.get("NoticeType")  || null == emailStat.get("AUTHKEY") ){
			return validRequest;
		}
		ArrayList<String> params = new ArrayList<String>();
		String noticeId = emailStat.get("NoticeId").toString();
		String noticeType = emailStat.get("NoticeType").toString();
		String authkey =  emailStat.get("AUTHKEY").toString();
		params.add(noticeId);
		params.add(noticeType);
		
		try {
			if (GhixUtils.validateGhixSecureCheckSum(params, authkey) ){
				validRequest = true;
			}
		} catch (NoSuchAlgorithmException e) {
			// logging the error and allowing the flow to continue as valid request
			LOGGER.error("Error while validating send grid request, continuing the flow as a valid request:", e);
			validRequest = true;
		}
		return validRequest;
	}

}
