package com.getinsured.hix.platform.notices.jpa;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.krysalis.barcode4j.ChecksumMode;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ExternalNotice;
import com.getinsured.hix.model.ExternalNotice.ExternalNoticesStatus;
import com.getinsured.hix.model.ExternalNoticeDto;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.model.InboxMsg.CONTENT_TYPE;
import com.getinsured.hix.model.InboxMsg.TYPE;
import com.getinsured.hix.model.InboxMsgDoc;
import com.getinsured.hix.model.InboxMsgResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.PrintableFlag;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.account.inboxnotification.SecureInboxNotificationEmailType;
import com.getinsured.hix.platform.account.service.InboxMsgService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.notify.EmailNotificationService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.TemplateNotFoundException;
import com.getinsured.hix.platform.repository.IExternalNoticeRepository;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Template Method Pattern implementation to generate PDF.
 *
 * @author EkramAli Kazi
 *
 */
public abstract class AbstractNotice implements INoticeStrategy {

    private static final String ADDRESS_TEMPLATE_HEADER = "notificationTemplate/addressTemplateHeader.html";
    private static final String ADDRESS_TEMPLATE_HEADER_SPANISH = "notificationTemplate/addressTemplateHeaderSpanish.html";
    public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
    public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
    public static final String EMAIL_FOOTERESP_LOCATION = "notificationTemplate/emailTemplateFooterEsp.html";

    private static final String EMPTY = "";
    @Autowired private NoticeTypeRepository noticeTypeRepository;
    @Autowired private NoticeRepository noticeRepository;
    @Autowired private InboxMsgService inboxMsgService;
    @Autowired private ContentManagementService ecmService;
    /*@Autowired private SecureInboxNotificationEmail secureInboxNotificationEmail;*/
    @Autowired private EmailNotificationService emailNotificationService;
    @Autowired private  ApplicationContext appContext;
    @Autowired private GhixDBSequenceUtil ghixDBSequenceUtil;
    @Autowired NoticeTemplateFactory noticeTemlatesFactory;
    @Autowired private IExternalNoticeRepository externalNoticeRepository;

    @Value("#{configProp['security.myInboxUrl']}")
    private String myInboxUrl;
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractNotice.class);
    protected final Gson gson = new Gson();

    public AbstractNotice() {
        super();
    }

    public abstract String createPDFDocument(NoticeType noticeType, Map<String, Object> tokens, String ecmFilePath, String ecmFileName) throws NoticeServiceException;

    /**
     * Template method to generate PDF using FLYING SAUCER or ADOBE LiveCycle.
     *
     * Steps of an algorithm:
     * 		1. validate incoming noticeTemplateName
     * 		2. delegate PDF document creation and uploading to ECM (Alfresco) to FLYING SAUCER or ADOBE LiveCycle
     * 		3. persist notice object into notices table
     * 		4. return back notice object to the client
     */
    @Override
    public final Notice createNotice(final String noticeTemplateName, final GhixLanguage language, final Map<String, Object> tokens,
                                     final String ecmFilePath, final String ecmFileName, final AccountUser user, final Location location, final GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException {
        //1. validate incoming noticeTemplateName
        final Notice notice = create(noticeTemplateName, language, tokens,
                ecmFilePath, ecmFileName, user, location, communicationPref);
        // 1.1 modifying the notice type information if required.
        if(tokens.containsKey("updateSubject")){
            notice.setSubject(tokens.get("updateSubject").toString());
        }

        //HIX-11660 Integrate PDF Notifications into Secure Inbox changes - start
        try {
            postToInbox(user,null, "null", -99999, notice, ecmFileName, "Exchange Admin", user.getFullName(), communicationPref);
        } catch (final ContentManagementServiceException e) {
            LOGGER.error("ContentManagementServiceException==>",e);
            throw new NoticeServiceException(e);
        } catch (final GIException e) {
            LOGGER.error("GIException==>",e);
            throw new NoticeServiceException(e);
        }
        //HIX-11660 Integrate PDF Notifications into Secure Inbox changes - end

        return notice;

    }


    @Override
    public final Notice createNoticeWOInbox(final String noticeTemplateName, final GhixLanguage language, final Map<String, Object> tokens, final String ecmFilePath, final String ecmFileName, final AccountUser user) throws NoticeServiceException {
        //1. validate incoming noticeTemplateName
        final Notice notice = create(noticeTemplateName, language, tokens,
                ecmFilePath, ecmFileName, user, null, null);
        return notice;

    }


    private Notice create(final String noticeTemplateName, final GhixLanguage language,
                          final Map<String, Object> tokens, final String ecmFilePath, final String ecmFileName,
                          final AccountUser user, final Location location, final GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException {


        final NoticeType noticeType = setNoticeType(noticeTemplateName, language);

        //1.2 Added to support address template by Biswakalyan.User is mandatory as per previous implementation
        setAddressTemplate(tokens, user.getFirstName()+StringUtils.SPACE+user.getLastName(), location);

        //1.3 Add the unique ifd for ntice
        String noticeId = ghixDBSequenceUtil.getNextSequenceFromDB(Notice.NOTICESEQUENCE.notices_seq.toString());
        tokens.put(TemplateTokens.NOTICE_UNIQUE_ID, StringUtils.leftPad(noticeId, GhixPlatformConstants.TEN, GhixPlatformConstants.ZERO));
        getHeaderFooterTokens(tokens);

        //2. delegate PDF document creation and uploading to ECM (Alfresco) to FLYING SAUCER or ADOBE LIFE CYCLE
        final String ecmDocumentId = createPDFDocument(noticeType, tokens, ecmFilePath, ecmFileName);

        //3. persist notice object into notices table
        final Notice notice = saveNoticeObject(noticeType, ecmDocumentId, user,noticeId, communicationPref, null, 0);
        return notice;
    }

    private NoticeType setNoticeType(final String noticeTemplateName, final GhixLanguage language) throws NoticeServiceException
    {
        NoticeType noticeType;
        try {
            noticeType = noticeTypeRepository.findByEmailClassAndLanguage(noticeTemplateName, language);
        } catch (final NotificationTypeNotFound e) {
            LOGGER.warn(e.getMessage());
            throw new NoticeServiceException(e.getMessage(), e);
        }
        if (noticeType == null)
        {
            LOGGER.error("No template found for noticeTemplateName - " + noticeTemplateName + " and language - " + language);
            throw new NoticeServiceException("No template found for noticeTemplateName - " + noticeTemplateName + " and language - " + language);
        }
        return noticeType;
    }

    private Notice saveNoticeObject(final NoticeType noticeType, final String ecmFileId, final AccountUser user, String noticeId, GhixNoticeCommunicationMethod communicationPref, String moduleName, long moduleId) {
        Notice noticeObj = new Notice();
        try {
            noticeObj.setId(Integer.parseInt(noticeId));
        } catch (Exception e) {
            LOGGER.error("Notice can not created without Unique ID.noticeSeq id-"+noticeId +e);
            throw new GIRuntimeException("Notice created without Unique ID");
        }
        noticeObj.setNoticeType(noticeType);
        noticeObj.setSubject(noticeType.getEmailSubject());
        noticeObj.setSentDate(new TSDate());
        noticeObj.setStatus(STATUS.PDF_GENERATED);
        noticeObj.setEcmId(ecmFileId);
        noticeObj.setUser(user);
        noticeObj.setKeyId((int) moduleId);
        noticeObj.setKeyName(StringUtils.upperCase(moduleName));
        noticeObj.setPrintable(getPrintableFlag(noticeType, communicationPref).toString());
        noticeObj = noticeRepository.save(noticeObj);
        return noticeObj;
    }

    private void recordExternalNotice(final Notice notice,final String documentName,final Map<String, Object> tokens)
    {
        if(tokens.containsKey(GhixConstants.SSAP_APPLICATION_ID))
        {
            ExternalNotice extNotice=new ExternalNotice();
            ExternalNoticeDto externalNoticeDto=new ExternalNoticeDto();
            extNotice.setStatusType(ExternalNoticesStatus.INIT);
            extNotice.setSsapId(((Long)tokens.get(GhixConstants.SSAP_APPLICATION_ID)).intValue());
            extNotice.setNoticeId(notice.getId());
            externalNoticeDto.setNoticeId(String.valueOf(notice.getId()));
            externalNoticeDto.setDocumentName(documentName);
            externalNoticeDto.setDocument(notice.getEcmId());
            externalNoticeDto.setTransactionId(String.valueOf(notice.getId()));
            extNotice.setRequestPayload(gson.toJson(externalNoticeDto));
            externalNoticeRepository.save(extNotice);
            LOGGER.info("External Notice recorded with INIT status");
        }
        else
        {
            LOGGER.info("Required Token 'ssapApplicationId' is missing, External Notice wont be sent");
        }
    }

    public void postToInbox(final AccountUser userObj,final List<String> sendToEmailList, final String moduleName, final long moduleId, final Notice notice, final String docName, final String fromFullName, final String toFullName, final GhixNoticeCommunicationMethod communicationPref)throws ContentManagementServiceException, GIException{
        GhixNoticeCommunicationMethod commPref = communicationPref;
        final InboxMsg msg = new InboxMsg();

        msg.setType(TYPE.N);
        msg.setContentType(CONTENT_TYPE.H);
        msg.setToUserNameList(toFullName);
        if(userObj!=null){
            msg.setToUserIdList(Integer.toString(userObj.getId()));
            msg.setOwnerUserId((userObj.getId()));
        }
        msg.setModuleId(moduleId);
        msg.setModuleName(moduleName);
        msg.setMsgSub(notice.getSubject());

        msg.setFromUserId(0);
        msg.setFromUserName(fromFullName);
        msg.setPriority(InboxMsg.PRIORITY.H);
        msg.setStatus(InboxMsg.STATUS.C);
        //msg.setOwnerUserId(0);
        msg.setOwnerUserName(fromFullName);

        final InboxMsgDoc inboxMsgDoc = new InboxMsgDoc();
        inboxMsgDoc.setCreatedOn(new TSDate());
        inboxMsgDoc.setDocType(InboxMsgDoc.TYPE.BINARY);
        inboxMsgDoc.setDocName(docName);

        final long size = ecmService.getContentDataById(notice.getEcmId()).length;
        inboxMsgDoc.setDocSize(size);
        final List<InboxMsgDoc> list = new ArrayList<InboxMsgDoc>();
        inboxMsgDoc.setDocId(notice.getEcmId());
        inboxMsgDoc.setMessage(msg);
        list.add(inboxMsgDoc);
        msg.setMessageDocs(list);

        InboxMsgResponse response = inboxMsgService.saveMessage(msg);
        InboxMsg newMsg = null;

        newMsg = inboxMsgService.findMessageById(response.getMessageList().get(0).getId());
        newMsg.setStatus(InboxMsg.STATUS.N);
        newMsg.setToUserNameList(toFullName);
        //newMsg.setToUserIdList(Integer.toString(userObj.getId()));
        newMsg.setMsgSub(newMsg.getMsgSub());
        newMsg.setMsgBody(notice.getAttachment());

        response = inboxMsgService.saveMessage(newMsg);

        if(response.getErrCode() == 0){
            if(commPref == null){
                commPref = GhixNoticeCommunicationMethod.Mail;
            }
            switch(commPref){
                case Email:
                case EmailAndMail:
                case Mail:
                    notifySecureEmailStatus(userObj, sendToEmailList, moduleName, moduleId, toFullName );
                    break;
                default:
                    LOGGER.info("Communication preference {} no eligible for Email",communicationPref);
            }
        }else{
            LOGGER.error("Failed to save inbox message. error code {} from moduleName {}",response.getErrCode(), moduleName);
        }

    }

    private void notifySecureEmailStatus(final AccountUser userObj,final List<String> sendToEmailList, final String moduleName, final long moduleId, final String toFullName){
        try{
            final Map<String, String> emailData = new HashMap<String, String>();
            emailData.put("exchangename", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
            emailData.put("exchangeurl", GhixPlatformEndPoints.APPSERVER_URL);
            emailData.put("inboxurl", myInboxUrl);
            emailData.put("exchangephone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
            emailData.put("host", GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
            emailData.put("KeyId", String.valueOf(moduleId));
            emailData.put("KeyName", StringUtils.upperCase(moduleName));
            String privacyStatement = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_STATEMENT);
            if(privacyStatement != null){
                emailData.put("privacy_statement",privacyStatement);
            }
            String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
            if(contactInformation != null){
                emailData.put("contact_information",contactInformation);
            }
            String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
            if(!"NV".equalsIgnoreCase(stateCode)) {
                emailData.put("Subject", "Important Notice from "+ DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
            }

            //If user object is present, send email to the email address present in the user object
            if(userObj!=null){
                emailData.put("To", userObj.getEmail());
				/*secureInboxNotificationEmail.setUserObj(userObj);
				secureInboxNotificationEmail.setEmailData(emailData);
				secureInboxNotificationEmail.sendEmail(secureInboxNotificationEmail.generateEmail());*/

                Map<String, String> tokens = SecureInboxNotificationEmailType.setTokens(emailData, userObj);

                Notice noticeObj = emailNotificationService.generateEmail(emailData, tokens, SecureInboxNotificationEmailType.EMAIL_CLAZZ, null);
                emailNotificationService.sendEmail(noticeObj);

            }
            //Use sendToEmailList list to send emails to everyone in the list
            else if(sendToEmailList != null){

                for(final String emailId : sendToEmailList){
                    final AccountUser user = new AccountUser();
                    user.setEmail(emailId);
                    user.setFirstName(toFullName);
                    emailData.put("To", emailId);
					/*secureInboxNotificationEmail.setUserObj(user);
					secureInboxNotificationEmail.setEmailData(emailData);
					secureInboxNotificationEmail.sendEmail(secureInboxNotificationEmail.generateEmail());*/

                    Map<String, String> tokens = SecureInboxNotificationEmailType.setTokens(emailData, user);

                    Notice noticeObj = emailNotificationService.generateEmail(emailData, tokens, SecureInboxNotificationEmailType.EMAIL_CLAZZ, null);
                    emailNotificationService.sendEmail(noticeObj);
                }
            }
            else{
                LOGGER.info("No user object or email list found in request");
            }
            LOGGER.info("Secure inbox notification email sent successfully");
        }
        catch (final Exception e){
            LOGGER.error("Unable to send email notification to user",e);
        }
    }

    public void postToInbox(final AccountUser userObj,final List<String> sendToEmailList, final String moduleName, final long moduleId, final Notice notice, final String docName, final String fromFullName, final String toFullName)throws ContentManagementServiceException, GIException{

        final InboxMsg msg = new InboxMsg();
        /**
         * "TO" : details of the message
         */
        msg.setType(TYPE.N);
        msg.setContentType(CONTENT_TYPE.H);
        msg.setToUserNameList(toFullName);
        //TODO
        //FIXME Remove userId assignment after module based read is implemented
        if(userObj!=null){
            msg.setToUserIdList(Integer.toString(userObj.getId()));
            msg.setOwnerUserId((userObj.getId()));
        }
        msg.setModuleId(moduleId);
        msg.setModuleName(moduleName);
        msg.setMsgSub(notice.getSubject());

        /**
         * "FROM": details of the message
         * From user Id is set to 0 for system generate messages
         */
        //AccountUser user = iUserRepository.findByUserName("exadmin@ghix.com");
        msg.setFromUserId(0);
        msg.setFromUserName(fromFullName);
        msg.setPriority(InboxMsg.PRIORITY.H);
        msg.setStatus(InboxMsg.STATUS.C);
        //msg.setOwnerUserId(0);
        msg.setOwnerUserName(fromFullName);

        /**
         * Document details
         */
        final InboxMsgDoc inboxMsgDoc = new InboxMsgDoc();
        inboxMsgDoc.setCreatedOn(new TSDate());
        inboxMsgDoc.setDocType(InboxMsgDoc.TYPE.BINARY);
        inboxMsgDoc.setDocName(docName);

        final long size = ecmService.getContentDataById(notice.getEcmId()).length;
        inboxMsgDoc.setDocSize(size);
        final List<InboxMsgDoc> list = new ArrayList<InboxMsgDoc>();
        inboxMsgDoc.setDocId(notice.getEcmId());
        inboxMsgDoc.setMessage(msg);
        list.add(inboxMsgDoc);
        msg.setMessageDocs(list);

        InboxMsgResponse response = inboxMsgService.saveMessage(msg);
        InboxMsg newMsg = null;

        newMsg = inboxMsgService.findMessageById(response.getMessageList().get(0).getId());
        newMsg.setStatus(InboxMsg.STATUS.N);
        newMsg.setToUserNameList(toFullName);
        //newMsg.setToUserIdList(Integer.toString(userObj.getId()));
        newMsg.setMsgSub(newMsg.getMsgSub());
        newMsg.setMsgBody(notice.getAttachment());

        response = inboxMsgService.saveMessage(newMsg);

        /*
         * Sending Email notification to user
         * HIX-14899
         * Author - Nikhil Talreja
         * since - 05 September 2013
         *
         * 1. Get email from the user object
         * 2. Construct the email by reading the template based on the resource location from Notice_types table
         * 3. Use EmailService to send the email
         * 4. Enclose the whole functionality in try catch block.
         * 5. In catch, print the stacktrace and do nothing. (I assume that failure related to email about a notification should not impact the flow)
         */

        try{
            final Map<String, String> emailData = new HashMap<String, String>();
            emailData.put("exchangename", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
            emailData.put("exchangeurl", GhixPlatformEndPoints.APPSERVER_URL);
            emailData.put("inboxurl", myInboxUrl);
            emailData.put("exchangephone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
            emailData.put("host", GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
            emailData.put("KeyId", String.valueOf(moduleId));
            emailData.put("KeyName", StringUtils.upperCase(moduleName));
            String privacyStatement = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_STATEMENT);
            if(privacyStatement != null){
                emailData.put("privacy_statement",privacyStatement);
            }
            String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
            if(contactInformation != null){
                emailData.put("contact_information",contactInformation);
            }
            String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
            if(!"NV".equalsIgnoreCase(stateCode)) {
                emailData.put("Subject", "Important Notice from "+ DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
            }


            //If user object is present, send email to the email address present in the user object
            if(userObj!=null){
                emailData.put("To", userObj.getEmail());
				/*secureInboxNotificationEmail.setUserObj(userObj);
				secureInboxNotificationEmail.setEmailData(emailData);
				secureInboxNotificationEmail.sendEmail(secureInboxNotificationEmail.generateEmail());*/

                Map<String, String> tokens = SecureInboxNotificationEmailType.setTokens(emailData, userObj);

                Notice noticeObj = emailNotificationService.generateEmail(emailData, tokens, SecureInboxNotificationEmailType.EMAIL_CLAZZ, null);
                emailNotificationService.sendEmail(noticeObj);

            }
            //Use sendToEmailList list to send emails to everyone in the list
            else if(sendToEmailList != null){

                for(final String emailId : sendToEmailList){
                    final AccountUser user = new AccountUser();
                    user.setEmail(emailId);
                    user.setFirstName(toFullName);
                    emailData.put("To", emailId);
					/*secureInboxNotificationEmail.setUserObj(user);
					secureInboxNotificationEmail.setEmailData(emailData);
					secureInboxNotificationEmail.sendEmail(secureInboxNotificationEmail.generateEmail());*/

                    Map<String, String> tokens = SecureInboxNotificationEmailType.setTokens(emailData, user);

                    Notice noticeObj = emailNotificationService.generateEmail(emailData, tokens, SecureInboxNotificationEmailType.EMAIL_CLAZZ, null);
                    emailNotificationService.sendEmail(noticeObj);
                }
            }
            else{
                LOGGER.info("No user object or email list found in request");
            }
            LOGGER.info("Secure inbox notification email sent successfully");
        }
        catch (final Exception e){
            LOGGER.error("Unable to send email notification to user",e);
        }
    }

    @SuppressWarnings("unchecked")
    private String populateNoticeJson(Map<String,Object> tokens, NoticeType noticeType) {
        JSONObject payloadObj = new JSONObject();
        JSONObject requestObj = new JSONObject();

        JSONObject dataObj = new JSONObject();
        Iterator<Entry<String, Object>> cursor = tokens.entrySet().iterator();
        Map.Entry<String,Object> entry = null;
        String key = null;
        while(cursor.hasNext()) {
            entry = cursor.next();
            key = entry.getKey();

            if(key.equals("referralLCENotificationDTO")) {
                JSONAware jsonAware = (JSONAware) entry.getValue();


                if(jsonAware != null) {
                    String jsonData = jsonAware.toJSONString();

                    if(LOGGER.isDebugEnabled() ) {
                        LOGGER.info("jsonData = {}", jsonData);
                    }

                    JSONObject requestJson = null;
                    JSONParser parser = null;
                    try {
                        parser = new JSONParser();
                        requestJson = (JSONObject) parser.parse(jsonData);
                    } catch (ParseException parseException) {
                        LOGGER.error("Error in parsing referralLCENotificationDTO json".intern() , parseException);
                        throw new GIRuntimeException("Error in parsing referralLCENotificationDTO json".intern());
                    }

                    if(requestJson != null) {
                        for (Object jsonKey : requestJson.keySet()) {
                            //based on you key types
                            String keyStr = (String)jsonKey;
                            if(keyStr.toLowerCase().startsWith("meta_".intern())) {
                                requestObj.put(keyStr.substring(5),  requestJson.get(jsonKey));

                            }else {
                                dataObj.put(keyStr, requestJson.get(jsonKey));
                            }
                        }
                    }
                }
                cursor.remove();
            }else {
                dataObj.put(key, entry.getValue());
                cursor.remove();
            }

        }
        requestObj.put("templateName".intern(), noticeType.getNotificationName());
        requestObj.put("data", dataObj);

        payloadObj.put("request", requestObj);

        if(LOGGER.isDebugEnabled()) {
            LOGGER.info("email request json body: " + payloadObj.toJSONString());
        }
        return payloadObj.toJSONString();

    }
    /**
     * Template method to generate PDF using FLYING SAUCER or ADOBE LiveCycle.
     *
     * Steps of an algorithm:
     * 		1. validate incoming noticeTemplateName
     * 		2. delegate PDF document creation and uploading to ECM (Alfresco) to FLYING SAUCER or ADOBE LiveCycle
     * 		3. persist notice object into notices table
     * 		4. return back notice object to the client
     */
    @Override
    public final Notice createModuleNotice(final String noticeTemplateName, final GhixLanguage language, final Map<String, Object> tokens,
                                           final String ecmFilePath,  String ecmFileName, final String moduleName, final long moduleId,
                                           final List<String> sendToEmailList, final String fromFullName, final String toFullName, final Location location, final GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException {
        Notice notice = null;
        final String ecmDocumentId;
        final NoticeType noticeType;
        String emailClass = null;
        try {
            //1. validate incoming noticeTemplateName
            noticeType = setNoticeType(noticeTemplateName, language);

            // 1.1 modifying the notice type information if required.
            updateNoticeType(noticeType,tokens);

            // 1.2 Added to support address template.
            setAddressTemplate(tokens,toFullName, location);

            // 1.3 Add the unique id for notice
            String noticeId = ghixDBSequenceUtil.getNextSequenceFromDB(Notice.NOTICESEQUENCE.notices_seq.toString());
            tokens.put(TemplateTokens.NOTICE_UNIQUE_ID, StringUtils.leftPad(noticeId, GhixPlatformConstants.TEN, GhixPlatformConstants.ZERO));

            // 1.4 Generate Barcode for document
            String documentBarcode = "barcode";
            String documentBarcodeBase64 = "barcode64";
            if(StringUtils.isNotBlank(noticeId)) {
                LOGGER.debug("createModuleNotice::module id padded: {}", StringUtils.leftPad(noticeId, 12, GhixPlatformConstants.ZERO));
                String barcodeMessage = StringUtils.leftPad(noticeId, 12, GhixPlatformConstants.ZERO);
                byte[] barcodeImage = generateBarcodeForDefaults(true, ChecksumMode.CP_ADD, barcodeMessage);
                if(barcodeImage != null) {
                    if(LOGGER.isDebugEnabled()) {
                        LOGGER.debug("createModuleNotice::base64 Image: {}", Base64.getEncoder().encodeToString(barcodeImage));
                    }
                    tokens.put(documentBarcode, "<img alt=\"barcode\" src=\"data:image/png;base64," + Base64.getEncoder().encodeToString(barcodeImage) + "\" style=\"width: 150px; height: 75px;\"/>");
                    tokens.put(documentBarcodeBase64, "data:image/png;base64," + Base64.getEncoder().encodeToString(barcodeImage));

                    if(LOGGER.isDebugEnabled()) {
                        LOGGER.debug("createModuleNotice::barcode img tag = {}", tokens.get(documentBarcode));
                    }
                } else {
                    LOGGER.error("createModuleNotice::barcode image array was null");
                }
            }

            getHeaderFooterTokens(tokens);

            //1.5 Add privacy statement and contact information
            String privacyStatement = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_STATEMENT);
            if(privacyStatement != null){
                tokens.put("privacy_statement",privacyStatement);
            }
            String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
            if(contactInformation != null){
                tokens.put("contact_information",contactInformation);
            }

            //HIX-114906 fix for fetching host in mn notice template
            tokens.put("host", GhixPlatformEndPoints.GHIXHIX_SERVICE_URL);
            
            // Construct document name in format : {template_name}_{notice id}_{timeinMilliSec}.pdf
            ecmFileName = getDocumentName(noticeType,noticeId);
            //2. delegate PDF document creation and uploading to ECM (Alfresco) to FLYING SAUCER or ADOBE LIFE CYCLE
            ecmDocumentId = createPDFDocument(noticeType, tokens, ecmFilePath, ecmFileName);

            //3. persist notice object into notices table
            notice = saveNoticeObject(noticeType, ecmDocumentId, null, noticeId, communicationPref, moduleName, moduleId);
            emailClass = notice.getNoticeType().getEmailClass();
            if(GhixPlatformConstants.REMOTE_EMAIL_ENABLED) {
                if(noticeType.getExternalSend() == NoticeType.ExternalSendEmail.REMOTE) {
                    LOGGER.info("populating notice json");
                    notice.setEmailBody(populateNoticeJson(tokens,noticeType));
                    this.emailNotificationService.sendEmailRequest(notice);
                }
            }else if(noticeType.getExternalSend() == NoticeType.ExternalSendEmail.NATIVE) {
                //HIX-11660 Integrate PDF Notifications into Secure Inbox changes - start
                postToInbox(null,sendToEmailList, moduleName, moduleId, notice, ecmFileName, fromFullName, toFullName, communicationPref );
            }
            else if(noticeType.getExternalSend() == NoticeType.ExternalSendEmail.NATIVE_REMOTE) {

                // Post Notice to Inbox and Record the Notice to External Notices table (batch will pick the entry and process it further)
                postToInbox(null,sendToEmailList, moduleName, moduleId, notice, ecmFileName, fromFullName, toFullName, GhixNoticeCommunicationMethod.None );
                recordExternalNotice(notice,ecmFileName,tokens);
            }
        } catch (final ContentManagementServiceException e) {
            LOGGER.error("ContentManagementServiceException==>",e);
            throw new NoticeServiceException(e);
        }
        catch (final GIException e) {
            LOGGER.error("GIException==>",e);
            throw new NoticeServiceException(e);
        } catch (Exception e) {
            if("DemographicChange".equalsIgnoreCase(emailClass)){
                LOGGER.error("Notice Type is not active as of today for DemographicChange");
            }
            else {
                LOGGER.error("Exception==>", e);
                throw new NoticeServiceException(e);
            }
        }
        //HIX-11660 Integrate PDF Notifications into Secure Inbox changes - end
        return notice;
    }

    private byte[] generateBarcodeForDefaults(boolean quietZone, ChecksumMode checksumMode, String barcodeMessage) {
        return generateBarcode(quietZone, checksumMode, barcodeMessage, "image/png", 300, BufferedImage.TYPE_BYTE_BINARY, false, 0);
    }

    private byte[] generateBarcode(boolean quietZone, ChecksumMode checksumMode, String barcodeMessage, String mimeType, int resolution, int imageType, boolean antiAlias, int orientation) {
        EAN13Bean ean13Bean = new EAN13Bean();

        // Change this section for different DPI and module width based on DPI
        //final int dpi = 300;
        //ean13Bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi));
        //ean13Bean.setFontSize(0.5);
        ean13Bean.doQuietZone(quietZone);
        ean13Bean.setChecksumMode(checksumMode);

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, mimeType, resolution, imageType, antiAlias, orientation);
                LOGGER.debug("generateBarcode::generating barcode message: {}", barcodeMessage);
                ean13Bean.generateBarcode(canvas, barcodeMessage);
                canvas.finish();
            } catch (IOException e) {
                LOGGER.error("generateBarcode::Error writing barcode", e);
            } finally {
                out.close();
            }

            return out.toByteArray();
        } catch(Exception e) {
            LOGGER.error("error barcode", e);
        }

        return null;
    }

    private NoticeType updateNoticeType(final NoticeType noticeType, final Map<String, Object> tokens){

        // UPDATE THE SUBJECT OF THE EXISTING NOTICE TYPE RECORD FETCHED FROM THE NOTICE_TYPES TABLE AS PER THE CONFIGURABLE EXCHANGE NAME
        if(tokens!=null && tokens.containsKey("updateSubject")){
            noticeType.setEmailSubject(tokens.get("updateSubject").toString());
        }

        return noticeType;
    }

    private PrintableFlag getPrintableFlag(NoticeType noticeType, GhixNoticeCommunicationMethod communicationPref) {
        PrintableFlag flag = PrintableFlag.N;
        if(communicationPref!=null){
            if(GhixNoticeCommunicationMethod.Mail.equals(communicationPref) || GhixNoticeCommunicationMethod.EmailAndMail.equals(communicationPref)){
                flag = PrintableFlag.Y;
            }
        }else if(GhixNoticeCommunicationMethod.Mail.equals(noticeType.getMethod())){
            flag = PrintableFlag.Y;
        }
        return flag;
    }

    private void setAddressTemplate(final Map<String, Object> tokens,String toFullName, Location location) throws NoticeServiceException {
        if(location!= null ){
            tokens.put(TemplateTokens.ADDRESS_CONTENT, populatreAddressHeaderTemplate(toFullName, location, ADDRESS_TEMPLATE_HEADER));
            tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, populatreAddressHeaderTemplate(toFullName, location, ADDRESS_TEMPLATE_HEADER_SPANISH));
        }else{
            //set empty
            tokens.put(TemplateTokens.ADDRESS_CONTENT, EMPTY);
            tokens.put(TemplateTokens.ADDRESS_CONTENT_SPANISH, EMPTY);
        }
    }

    private String populatreAddressHeaderTemplate(String userFullName,Location location, String addressTemplateType) throws NoticeServiceException
    {
    	LocalDate localdate = LocalDate.now();
    	String stringdate = DateTimeFormatter.ofPattern("MMMM d, y", Locale.ENGLISH).format(localdate).toString();
        
        Map<String, String> replaceableObj = new HashMap<String, String> ();
        //place empty if no detail available
        replaceableObj.put(TemplateTokens.USER_FULL_NAME, (null==userFullName)?EMPTY:userFullName);
        replaceableObj.put(TemplateTokens.ADDRESS_LINE_1, (null== location.getAddress1())?EMPTY:location.getAddress1());
        replaceableObj.put(TemplateTokens.ADDRESS_LINE_2, (null== location.getAddress2())?EMPTY:location.getAddress2());
        replaceableObj.put(TemplateTokens.CITY_NAME, (null== location.getCity())?EMPTY:location.getCity());
        replaceableObj.put(TemplateTokens.STATE_CODE, (null== location.getState())?EMPTY:location.getState());
        replaceableObj.put(TemplateTokens.PIN_CODE, (null == location.getZip())?EMPTY:location.getZip());
        
        replaceableObj.put(TemplateTokens.TODAYS_DATE, stringdate);
        
        if(GhixPlatformConstants.REMOTE_EMAIL_ENABLED) {
            return JSONObject.toJSONString(replaceableObj);
        }

        return getTemplateContentWithTokensReplaced(replaceableObj, addressTemplateType);
    }


    public Map<String, Object> getHeaderFooterTokens(Map<String, Object> tokens) throws NoticeServiceException {
        if(GhixPlatformConstants.REMOTE_EMAIL_ENABLED) {
            tokens.put(TemplateTokens.HEADER_CONTENT,"not_applicable");
            tokens.put(TemplateTokens.FOOTER_CONTENT,"not_applicable");
            tokens.put(TemplateTokens.SPAINISH_FOOTER_CONTENT,"not_applicable");
            return tokens;
        }
        
		
        Map<String, String> templateTokens = new HashMap<String, String>();
        templateTokens.put(TemplateTokens.HOST, GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
        templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
        templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
        templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
        templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
        templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
        templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
        templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
        templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
        templateTokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
        templateTokens.put(TemplateTokens.NOTICE_UNIQUE_ID, (String) tokens.get(TemplateTokens.NOTICE_UNIQUE_ID));
        templateTokens.put(TemplateTokens.FOOTER_YEAR, Integer.toString(TSCalendar.getInstance().get(Calendar.YEAR)));
        
        String privacyStatement = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_STATEMENT);
        if (privacyStatement != null) {
            templateTokens.put(TemplateTokens.PRIVACY_STATEMENT, privacyStatement);
        }
        String contactInformation = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CONTACT_INFORMATION);
        if (contactInformation != null) {
            templateTokens.put(TemplateTokens.CONTACT_INFORMATION, contactInformation);
        }

        if (tokens.containsKey("barcode")) {
            templateTokens.put("barcode", (String) tokens.get("barcode"));
        }

        if (tokens.containsKey("barcode64")) {
            templateTokens.put("barcode64", (String) tokens.get("barcode64"));
        }

        tokens.put(TemplateTokens.HEADER_CONTENT, this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_HEADER_LOCATION));
        templateTokens.put(TemplateTokens.PRIVACY_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));
        templateTokens.put(TemplateTokens.PRIVACY_URL_ESP, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL_ESP));
        tokens.put(TemplateTokens.FOOTER_CONTENT, this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_FOOTER_LOCATION));
        tokens.put(TemplateTokens.SPAINISH_FOOTER_CONTENT, this.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_FOOTERESP_LOCATION));

        return tokens;
    }

    private String getTemplateContentWithTokensReplaced(Map<String, String> tokens, String location) throws NoticeServiceException {
        if(GhixPlatformConstants.REMOTE_EMAIL_ENABLED) {
            return "not_applicvable";
        }
        StringWriter sw = new StringWriter();
        try {
            Template tmpl = noticeTemlatesFactory.getTemplate(location);
            tmpl.process(tokens, sw);
        } catch (TemplateNotFoundException | TemplateException | IOException e) {
            throw new NoticeServiceException(e);
        }
        finally{
            IOUtils.closeQuietly(sw);
        }
        return sw.toString();
    }

    private  String getDocumentName(NoticeType noticetype,String noticeId) {
        String templateLocationStr=noticetype.getTemplateLocation();
        StringBuilder documentName= new StringBuilder();
        if(templateLocationStr.lastIndexOf('/')>0 &&templateLocationStr.lastIndexOf('.')>0 )
        {
            documentName.append(templateLocationStr.substring(templateLocationStr.lastIndexOf('/')+1,templateLocationStr.lastIndexOf('.')));
        }
        else
        {
            documentName.append(templateLocationStr);
        }
        documentName.append("_").append(noticeId).append("_").append(System.currentTimeMillis()).append(".pdf");
        return documentName.toString();
    }


}
