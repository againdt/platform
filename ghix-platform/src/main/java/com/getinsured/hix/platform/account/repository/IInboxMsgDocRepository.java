package com.getinsured.hix.platform.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import com.getinsured.hix.model.InboxMsgDoc;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IInboxMsgDocRepository extends JpaRepository<InboxMsgDoc, Long> {
	
	 public final static String FIND_BY_DOCUMENT_ID = "SELECT inboxMsgDoc " + 
             "FROM InboxMsgDoc inboxMsgDoc where inboxMsgDoc.docId=:docId" ;
	  
	 /*
	  *find InboxMsgDoc by docID
	  */
	  @Query(FIND_BY_DOCUMENT_ID)
	  public List<InboxMsgDoc> findInboxMsgDocbyDocID(@Param ("docId") String docId);

}
