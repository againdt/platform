package com.getinsured.hix.platform.security.duo;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.duosecurity.client.Http;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class DuoAdminApi {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DuoAdminApi.class);
	public static enum DuoAdminApiReturnStatus{
		SetUPError,APIError,AccountNotExist,AccountRemoved,AccountUnchanged,AccountAdded
	}
	public static JSONArray retrieveUsers(){
		JSONArray duoUserList = null; 
		if (! DuoAdminApi.validateDuoSetup() ){
			LOGGER.error("Duo setup Invalid.Falied to remove the user duo account");
			return null;
		}
		
		try {
			Http request = new Http("GET", GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS, "/admin/v1/users");
			request.signRequest(GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS , GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS);
			duoUserList = (JSONArray)request.executeRequest();
			return duoUserList;
		} catch (Exception e) {
			LOGGER.error("Failed to retrieve user from duo account, Exception :",e);
			return null;
		}
	}
	
	public static JSONObject retrieveUserByUsername(String username){
		JSONObject userDuo =  null;
		if (! DuoAdminApi.validateDuoSetup() ){
			LOGGER.error("Duo setup Invalid.Falied to remove the user duo account");
			return null;
		}
		
		try {
			Http request = new Http("GET", GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS, "/admin/v1/users");
			request.addParam("username", username);
			request.signRequest(GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS , GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS);
			JSONArray result = (JSONArray)request.executeRequest();
			if(result.length() >= 1){
				userDuo = result.getJSONObject(0);
			}
			return userDuo;
		} catch (Exception e) {
			LOGGER.error("Failed to retrieve user from duo account, Exception :",e);
			return null;
		}
	}
	
	public static DuoAdminApiReturnStatus addDuoCheck(String username){
		if (! DuoAdminApi.validateDuoSetup() ){
			LOGGER.error("Duo setup Invalid.Falied to remove the user duo account");
			return DuoAdminApiReturnStatus.SetUPError;
		}
		
		
		try {
			JSONObject userDuo = retrieveUserByUsername(username);
	       
			if(null != userDuo ){
				Http request = new Http("POST", GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS, "/admin/v1/users/"+userDuo.getString("user_id"));
				request.addParam("status", "active");
				request.signRequest(GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS , GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS);
				request.executeRequest();
				return DuoAdminApiReturnStatus.AccountAdded;
			}
			return DuoAdminApiReturnStatus.AccountUnchanged;
		} catch (Exception e) {
			LOGGER.error("Failed to remove the user from duo account, Exception :",e);
			return DuoAdminApiReturnStatus.APIError;
		}
	}
	
	public static DuoAdminApiReturnStatus removeDuoCheck(String username){
		if (! DuoAdminApi.validateDuoSetup() ){
			LOGGER.error("Duo setup Invalid.Falied to remove the user duo account");
			return DuoAdminApiReturnStatus.SetUPError;
		}
		
		
		try {
			JSONObject userDuo = retrieveUserByUsername(username);
			if(null != userDuo ){
				Http request = new Http("POST", GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS, "/admin/v1/users/"+userDuo.getString("user_id"));
				request.addParam("status", "disabled");
				request.signRequest(GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS , GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS);
				request.executeRequest();
				return DuoAdminApiReturnStatus.AccountRemoved;
			}
			return DuoAdminApiReturnStatus.AccountUnchanged;
		} catch (Exception e) {
			LOGGER.error("Failed to remove the user from duo account, Exception :",e);
			return DuoAdminApiReturnStatus.APIError;
		}
	}
	
	/*public static void authenticationAttempts(){
		JSONObject result = null;
        try {
            // Prepare request.
            Http request = new Http("GET", GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS, "/admin/v1/info/authentication_attempts");
            request.signRequest(GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS , GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS);
            
            // Send the request to Duo and parse the response.
            result = (JSONObject)request.executeRequest();
            
            System.out.println("==========================AUTHENTICATION ATTEMPTS START=============================");
            System.out.println("mintime = " + result.getInt("mintime"));
            System.out.println("maxtime = " + result.getInt("maxtime"));

            JSONObject attempts
                = result.getJSONObject("authentication_attempts");
            Iterator<?> keys = attempts.keys();
            while (keys.hasNext()) {
                String key = (String)keys.next();
                System.out.println(key + " count = " + attempts.getInt(key));
            }
            System.out.println("==========================AUTHENTICATION ATTEMPTS END=============================");
        }
        catch (Exception e) {
            System.out.println("error making request");
            System.out.println(e.toString());
        }

	}*/
	
	
	private static boolean validateDuoSetup() {
		boolean isDuoSetupValid = true;
		if(GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS == null){
			LOGGER.error("Duo setup: hostname missing.");
			isDuoSetupValid = false;
		}if(GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS == null){
			LOGGER.error("Duo setup: Integration key missing.");
			isDuoSetupValid = false;
		}if(GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY == null){
			LOGGER.error("Duo setup: application secret key missing.");
			isDuoSetupValid = false;
		}if(GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS == null){
			LOGGER.error("Duo setup: secret key missing.");
			isDuoSetupValid = false;
		}
		return isDuoSetupValid;
	}

	public static void main(String[] args) {
	        System.out.println("Duo Admin Demo");
	        //hanee a/c
	        /*GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS = "api-6715d265.duosecurity.com";
	        GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS = "DIW8KRTZEZ5XVKMFKX9R";
	        GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY = "baaf2efb955c3c9e9b7580c5b2d36a5ad16e22ea";
	        GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS = "4B2XuY0LUcafLIt6Ix1forqMTGXrysHwV7oZdt2y";*/
	        
	        //new a/c by victor
	        GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS = "api-00c6150c.duosecurity.com";
	        GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS = "DINYAR24GU59HGHY3ZIB";
	        GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY = "baaf2efb955c3c9e9b7580c5b2d36a5ad16e22ea";
	        GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS = "2p1r8tiVb0fEEsePwSvdN7aFjTIAYQKFLTkf10xm";
	        
	        
	        //idmain
	       /* GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS = "api-00c6150c.duosecurity.com";
	        GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS = "DIZLMSIAUW75F1117NXO";
	        GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY = "baaf2efb955c3c9e9b7580c5b2d36a5ad16e22ea";
	        GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS = "v4sj0q2A7FDGiXfHUoAaVLPIHTJTfdcP44zKZVWD";*/
	        
	        //ide2e
	        /*GhixPlatformConstants.DUO_HOSTNAME_ADMINAPIS = "api-de42aa95.duosecurity.com";
	        GhixPlatformConstants.DUO_INTEGRATION_KEY_ADMINAPIS = "DINBKBCN7JAZHTR1P47A";
	        GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY = "baaf2efb955c3c9e9b7580c5b2d36a5ad16e22ea";
	        GhixPlatformConstants.DUO_SECRET_KEY_ADMINAPIS = "yGcve6lcRTJ7yAm6E2Ntm90zHHdLdQP6Iebi23k2";*/
	       	        
	      
	        JSONArray duoUserList =  retrieveUsers();
	        System.out.println(duoUserList.toString());
	       
	       
	       JSONObject userDuo;
	        userDuo = retrieveUserByUsername("jasmin.p@yopmail.com");
	        if(null != userDuo ){
	        	System.out.println(userDuo.toString() );
	        }
	        
	        removeDuoCheck("jasmin.p@yopmail.com");
	        System.out.println("marking the user disabled" );
	        
	        userDuo = retrieveUserByUsername("jasmin.p@yopmail.com");
	        if(null != userDuo ){
	        	System.out.println(userDuo.toString() );
	        }
	        
	        addDuoCheck("jasmin.p@yopmail.com");
	        
	        System.out.println("marking the user enabled" );
	        userDuo = retrieveUserByUsername("jasmin.p@yopmail.com");
	        if(null != userDuo ){
	        	System.out.println(userDuo.toString() );
	        }
	        
	        System.out.println("Duo Admin Demo Ended");
	        System.exit(0);

	}

}
