package com.getinsured.hix.platform.util;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * Note: This class in fact does NOT use Jasypt encryption!
 * 
 * @author 
 */
@Component
@Scope("singleton")
public class GhixJasyptEncrytorUtil {

	public static final String GENC_TAG_START = "GENCS";
	public static final String GENC_TAG_END = "GENCE";

	public GhixJasyptEncrytorUtil() {
		// Left it for backward compatibility for configuration
	}

	/**
	 * This will throw exception if the configuration used for encryption was
	 * not configured to decrypt the string. Caller need to handle the Exception
	 * 
	 * @author Biswakalyan
	 * @since 24-Feb-2014
	 * @throws Exception
	 * @param encryptedValue
	 * @return string after decrypting the given value.
	 */
	public String decryptStringByJasypt(String encryptedValue) {
		GIRuntimeException ex = null;
		String decVal = null;
		try {
			decVal = GhixAESCipherPool.decrypt(encryptedValue);
		} catch (Exception e) {
			ex = new GIRuntimeException(e);
		}
		if (ex != null) {
			throw ex;
		}
		return decVal;
	}

	/**
	 * Use the method decryptGencStringByJasypt(..) to decrypt the data that
	 * encapsulated between '$GNC[' and ']'. This method is useful if the data
	 * can not be decrypted at filter level, for example the 'name' attribute of
	 * any form element can not be decrypted at filter lavel. If the data did
	 * not encapsulated by '$GNC[' and ']' throws the exception. Caller need to
	 * handle the Exception
	 * 
	 * @author Venkata Tadepalli
	 * @since 16-Sept-2014
	 * @throws Exception
	 * @param gencEncryptedValue
	 * @return string after decrypting the given value.
	 */
	public String decryptGencStringByJasypt(String gencEncryptedValue) {
		String encInput = stripGencEnclosure(gencEncryptedValue);
		return decryptStringByJasypt(encInput);
	}

	/**
	 * This will throw exception if the configuration used for encryption was
	 * not configured properly.
	 * 
	 * @author Biswakalyan
	 * @since 24-Feb-2014
	 * @throws Exception
	 * @param textValue
	 *            to encrypt.
	 * @return string after encrypting the given value.
	 */
	public String encryptStringByJasypt(String textValue) {
		GIRuntimeException ex = null;
		String encVal = null;

		try {
			encVal = GhixAESCipherPool.encrypt(textValue);
		} catch (Exception e) {
			ex = new GIRuntimeException(e);

		}
		if (ex != null) {
			throw ex;
		}
		return encVal;
	}

	public static String stripGencEnclosure(String str) {
		if (str == null) {
			throw new GIRuntimeException("Null string provided , expected GENC enclosed encrypted string");
		}
		if (!str.startsWith(GENC_TAG_START) && str.endsWith(GENC_TAG_END)) {
			throw new GIRuntimeException("Invalid Input String, expected $GENC[<encrypted data>]");
		}
		return str.substring(GENC_TAG_START.length(), str.length() - GENC_TAG_END.length());
	}

	public static void main(String[] args) {
		GhixJasyptEncrytorUtil util = new GhixJasyptEncrytorUtil();
		String str = util.encryptStringByJasypt("Abhai Chaudhary");
		str = GENC_TAG_START + str + GENC_TAG_END;
		System.out.println("Encrypted String:" + str);
		str = util.decryptGencStringByJasypt(str);
		System.out.println(str);
	}
}
