//package org.springframework.security.web.authentication.preauth;
package com.getinsured.hix.platform.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.timeshift.TimeShifterUtil;

public class GhixModuleSsoProcessingFilter extends CustomAbstractPreAuthenticatedProcessingFilter
{
  private static final Logger logger = LoggerFactory.getLogger(GhixModuleSsoProcessingFilter.class);

  /**
   * Try to authenticate a pre-authenticated user with Spring Security if the user has not yet been authenticated.
   */
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException
  {
    String requestURL = ((HttpServletRequest) request).getRequestURL().toString();

    if (logger.isTraceEnabled())
    {
      logger.trace("Checking secure context token: " + SecurityContextHolder.getContext().getAuthentication());
      logger.trace("Request Url ::" + requestURL);
    }

    if (!requestURL.contains("actuator"))
    {
      //System.out.println("-**-Checking secure context token: " + SecurityContextHolder.getContext().getAuthentication());
      //System.out.println("-**-Request Url ::"+requestURL);


      if (requiresAuthentication((HttpServletRequest) request))
      {
        doAuthenticate((HttpServletRequest) request, (HttpServletResponse) response);
      }
      HttpServletRequest req = (HttpServletRequest) request;
      String user = req.getHeader("GHIX_SSO_USER".intern());
      String tsUser = req.getHeader("X-USER".intern());
      String activeRoleName = req.getHeader("X-USER-ROLE".intern());
      String requesterId = req.getHeader("X-REQUESTER-ID".intern());
      String userOffset = req.getHeader("X-USER-OFFSET".intern());
      if (logger.isInfoEnabled())
      {
        logger.info("Inbound TS Context received for URL {} for sso-user {}, ts-user{}, with offset {}", requestURL, user, tsUser, userOffset);
      }
      TimeShifterUtil.setUserOffset(user, userOffset);

      if (logger.isTraceEnabled())
      {
        logger.trace("User:" + user + " with userOffset = " + userOffset + " with role:" + activeRoleName + " from:" + requesterId + " Requested Resource:" + req.getRequestURI());
      }
      if (activeRoleName != null)
      {
        HttpSession session = ((HttpServletRequest) request).getSession(false);
        if (session != null)
        {
          session.setAttribute("userActiveRoleName", activeRoleName);
          session.setAttribute("requesterId", requesterId);
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null)
        {
          Object currentUser = auth.getPrincipal();
          if (currentUser instanceof AccountUser)
          {
            ((AccountUser) currentUser).setCurrentUserRole(activeRoleName);
          }
        }
      }

    }
    chain.doFilter(request, response);

  }

  /**
   * Do the actual authentication for a pre-authenticated user.
   */
  private boolean doAuthenticate(HttpServletRequest request, HttpServletResponse response)
  {
    Authentication authResult;
    Object principal = getPreAuthenticatedPrincipal(request);
    Object credentials = "N/A";
    //String activeRoleName = request.getHeader("X-USER-ROLE");
    if (principal == null)
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("No pre-authenticated principal found in request");
      }

      return false;
    }

    if (logger.isDebugEnabled())
    {
      logger.debug("preAuthenticatedPrincipal = " + principal + ", trying to authenticate");
    }

    try
    {
      PreAuthenticatedAuthenticationToken authRequest = new PreAuthenticatedAuthenticationToken(principal, credentials);
      authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
      authResult = authenticationManager.authenticate(authRequest);
      successfulAuthentication(request, response, authResult);
    }
    catch (AuthenticationException failed)
    {
      unsuccessfulAuthentication(request, response, failed);

      if (!continueFilterChainOnUnsuccessfulAuthentication)
      {
        throw failed;
      }
    }
    return true;
  }

  private boolean requiresAuthentication(HttpServletRequest request)
  {
    Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

    if (currentUser == null)
    {
      logger.debug("Required Authentication currentUser == null");
      return true;
    }

    logger.debug("NOT required Authentication");

    if (!checkForPrincipalChanges)
    {
      return false;
    }

    Object principal = getPreAuthenticatedPrincipal(request);

    if (principal == null)
    {
      logger.debug("NOT required Authentication; header check not required");
      return false;
    }

    if (currentUser.getName().equalsIgnoreCase((String) principal))
    {
      return false;
    }
    if (logger.isDebugEnabled())
    {
      logger.debug("Pre-authenticated principal has changed to " + principal + " and will be reauthenticated");
    }

    if (invalidateSessionOnPrincipalChange)
    {
      HttpSession session = request.getSession(false);

      if (session != null)
      {
        logger.debug("Invalidating existing session");
        session.invalidate();
        request.getSession();
      }
    }

    return true;
  }

  /**
   * Puts the <code>Authentication</code> instance returned by the
   * authentication manager into the secure context.
   */

  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult)
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Authentication success: " + authResult);
    }
    SecurityContextHolder.getContext().setAuthentication(authResult);

  }

  /**
   * Ensures the authentication object in the secure context is set to null when authentication fails.
   * <p>
   * Caches the failure exception as a request attribute
   */
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
  {
    SecurityContextHolder.clearContext();

    if (logger.isDebugEnabled())
    {
      logger.debug("Cleared security context due to exception", failed);
    }
    request.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, failed);
  }


  /**
   * If set to {@code true}, any {@code AuthenticationException} raised by the {@code AuthenticationManager} will be
   * swallowed, and the request will be allowed to proceed, potentially using alternative authentication mechanisms.
   * If {@code false} (the default), authentication failure will result in an immediate exception.
   *
   * @param shouldContinue set to {@code true} to allow the request to proceed after a failed authentication.
   */
  public void setContinueFilterChainOnUnsuccessfulAuthentication(boolean shouldContinue)
  {
    continueFilterChainOnUnsuccessfulAuthentication = shouldContinue;
  }

  /**
   * If set, the pre-authenticated principal will be checked on each request and compared
   * against the name of the current <tt>Authentication</tt> object. If a change is detected,
   * the user will be reauthenticated.
   *
   * @param checkForPrincipalChanges
   */
  public void setCheckForPrincipalChanges(boolean checkForPrincipalChanges)
  {
    this.checkForPrincipalChanges = checkForPrincipalChanges;
  }

  /**
   * If <tt>checkForPrincipalChanges</tt> is set, and a change of principal is detected, determines whether
   * any existing session should be invalidated before proceeding to authenticate the new principal.
   *
   * @param invalidateSessionOnPrincipalChange <tt>false</tt> to retain the existing session. Defaults to <tt>true</tt>.
   */
  public void setInvalidateSessionOnPrincipalChange(boolean invalidateSessionOnPrincipalChange)
  {
    this.invalidateSessionOnPrincipalChange = invalidateSessionOnPrincipalChange;
  }

  /**
   * Read and returns the header named by {@code principalRequestHeader} from the request.
   *
   * @throws PreAuthenticatedCredentialsNotFoundException if the header is missing and {@code exceptionIfHeaderMissing}
   *          is set to {@code true}.
   */
  protected Object getPreAuthenticatedPrincipal(HttpServletRequest request)
  {

    String principal = request.getHeader(principalRequestHeader);
    if (principal == null)
    {
      principal = request.getHeader("GHIX_SSO_USER".intern());
    }
    if (principal == null)
    {
      principal = request.getHeader("X-USER".intern());
    }

    if (principal == null || "".equals(principal))
    {
      return "anonymous";
    }

    return principal;
  }

  @Override
  protected Object getPreAuthenticatedCredentials(HttpServletRequest request)
  {
    // TODO Auto-generated method stub
    return null;
  }


}
