package com.getinsured.hix.platform.dto.smartystreet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Sunil Desu
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Analysis {

	private String dpv_match_code;
	private String dpv_footnotes;
	private String dpv_cmra;
	private String dpv_vacant;
	private String active;
	private String ewa_match;
	private String footnotes;

	public String getDpv_match_code() {
		return dpv_match_code;
	}

	public void setDpv_match_code(String dpv_match_code) {
		this.dpv_match_code = dpv_match_code;
	}

	public String getDpv_footnotes() {
		return dpv_footnotes;
	}

	public void setDpv_footnotes(String dpv_footnotes) {
		this.dpv_footnotes = dpv_footnotes;
	}

	public String getDpv_cmra() {
		return dpv_cmra;
	}

	public void setDpv_cmra(String dpv_cmra) {
		this.dpv_cmra = dpv_cmra;
	}

	public String getDpv_vacant() {
		return dpv_vacant;
	}

	public void setDpv_vacant(String dpv_vacant) {
		this.dpv_vacant = dpv_vacant;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getEwa_match() {
		return ewa_match;
	}

	public void setEwa_match(String ewa_match) {
		this.ewa_match = ewa_match;
	}

	public String getFootnotes() {
		return footnotes;
	}

	public void setFootnotes(String footnotes) {
		this.footnotes = footnotes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Analysis [dpv_match_code=");
		builder.append(dpv_match_code);
		builder.append(", dpv_footnotes=");
		builder.append(dpv_footnotes);
		builder.append(", dpv_cmra=");
		builder.append(dpv_cmra);
		builder.append(", dpv_vacant=");
		builder.append(dpv_vacant);
		builder.append(", active=");
		builder.append(active);
		builder.append(", ewa_match=");
		builder.append(ewa_match);
		builder.append(", footnotes=");
		builder.append(footnotes);
		builder.append("]");
		return builder.toString();
	}

}
