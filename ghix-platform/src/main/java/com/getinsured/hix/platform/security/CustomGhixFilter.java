package com.getinsured.hix.platform.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CustomGhixFilter implements Filter {
	private static Log LOG = LogFactory.getLog(CustomGhixFilter.class);

 
     
    public void destroy() {
    }
 
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
 
        HttpServletRequest request = (HttpServletRequest) req;
       // String url = request.getServletPath();
        LOG.debug("Received the request in IFrameGlobalInterceptor");
		// Set the iFrame variable over here
		// First determine if it is present or not
		// All values other than "yes" are considered to be "no"
		String iframe = request.getParameter("iframe");
		if (null != iframe) {
			if (iframe.equalsIgnoreCase("yes")) {
				iframe = "yes";
			} else {
				iframe = "no";
			}
		} else {
			// Check in session if it is there
			String iframefromsession = (String) request.getSession()
					.getAttribute("iframe");
			if (null != iframefromsession) {
				if (iframefromsession.equalsIgnoreCase("yes")) {
					iframe = "yes";
				} else {
					iframe = "no";
				}
			} else {
				iframe = "no";
			}
		}
		request.getSession().setAttribute("iframe", iframe);
         
        chain.doFilter(req, res);
    }
 
    public void init(FilterConfig config) throws ServletException {
       
    }
}