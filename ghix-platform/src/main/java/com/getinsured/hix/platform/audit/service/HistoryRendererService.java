package com.getinsured.hix.platform.audit.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * This class is used to populate the List<Map<String, Object>> base on the data got from REV_history or AUD tables.
 * Popultaed List<Map<String, Object>> can be used to display the corresponding data as History pages in UI
 * TODO: The data currently taken is whole data.This should have been paginated to give optimum results.
 * @author save_h
 */
@Service
public interface HistoryRendererService {
	/**
	 * Populates the List<Map<String, Object>> which can be used to display the corresponding data as History pages in UI
	 * @param data
	 * 			List<Map<String, String>> input of the entire revision history data.
	 * @param compareColumns
	 * 			List<String> of column names based on which the filteration of @param data should be done.
	 * @param columnsToDisplay
	 * 			List<Integer> of column ids specified in corresponding HistoryService, which will indicate newly prepared map should contain what all keys.
	 * @return
	 * 			List<Map<String, Object>> Filtered data based on @param data, @param compareColumns and @param columnsToDisplay.
	 */
	List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns, List<Integer> columnsToDisplay);
	

}
