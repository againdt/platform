package com.getinsured.hix.platform.config;

import java.util.EnumSet;

public class CAPConfiguration {
	
	public enum ProductEnum {
		HLT("HLT","Health"), DEN("DEN","Dental"), AME("AME","AME"), STM("STM","STM"), VSN("VSN","Vision"), MDR("MDR","Medicare"), LI("LI","Life Insurance")
		,MC_RX("MC_RX","Medicare Part D"),MC_ADV("MC_ADV","Medicare Advantage"),MC_SUP("MC_SUP","Medicare Supplement"),MC_DEN("MC_DEN","Medicare Dental"),
		MC_VISION("MC_VISION", "Medicare Vision");
		
		private final String key;
		private final String value;
		
		public String getKey(){return this.key;}
		public String getValue(){return this.value;}
		
		ProductEnum(String key, String value){
			this.key = key;
			this.value = value;
		}
	}

	public enum CAPManualEnrollmentConfigEnum {
		
		CAP_MANUAL_ENROLLMENT_ON_EXCHANGE_HEALTH ("ON","cap.manualEnrollment.onExchange.health",ProductEnum.HLT),
		CAP_MANUAL_ENROLLMENT_ON_EXCHANGE_DENTAL ("ON","cap.manualEnrollment.onExchange.dental",ProductEnum.DEN),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_HEALTH ("OFF","cap.manualEnrollment.offExchange.health",ProductEnum.HLT),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_DENTAL ("OFF","cap.manualEnrollment.offExchange.dental",ProductEnum.DEN),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_VISION ("OFF","cap.manualEnrollment.offExchange.vision",ProductEnum.VSN),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_LIFE_INSURANCE ("OFF","cap.manualEnrollment.offExchange.life",ProductEnum.LI),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_AME ("OFF","cap.manualEnrollment.offExchange.ame",ProductEnum.AME),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_STM ("OFF","cap.manualEnrollment.offExchange.stm",ProductEnum.STM),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_MC_ADV("OFF","cap.manualEnrollment.offExchange.mc_adv",ProductEnum.MC_ADV),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_MC_RX("OFF","cap.manualEnrollment.offExchange.mc_rx",ProductEnum.MC_RX),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_MC_SUP("OFF","cap.manualEnrollment.offExchange.mc_sup",ProductEnum.MC_SUP),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_MC_DEN("OFF","cap.manualEnrollment.offExchange.mc_den",ProductEnum.MC_DEN),
		CAP_MANUAL_ENROLLMENT_OFF_EXCHANGE_MC_VISION("OFF","cap.manualEnrollment.offExchange.mc_vision",ProductEnum.MC_VISION);
		

		private final String exchangeType;
		private final String configName;	
		private final ProductEnum product;	

		public String getExchangeType(){return this.exchangeType;}
		
		public String getConfigName(){return this.configName;}
		
		public ProductEnum getProduct() {return this.product;}
		
		CAPManualEnrollmentConfigEnum(String exchangeType,String configName,ProductEnum product){
			this.exchangeType = exchangeType;
			this.configName = configName;
			this.product = product;
		}
	};
	
	public static final EnumSet<CAPManualEnrollmentConfigEnum> CAP_MANUAL_ENROLLMENT_CONFIGS = EnumSet.allOf(CAPManualEnrollmentConfigEnum.class);

	
	public enum CAPConfigurationEnum implements PropertiesEnumMarker
	{  
		SHOW_APPVIEWS_V2 ("cap.ShowAppsView"),
		SHOW_LEADS_V2 ("cap.ShowLeadsView"),
		SHOW_SCREENPOP_V2 ("cap.ShowScreenPopView"),
		SHOW_D2C_SUBMIT("cap.ShowD2CSubmit"),
		CAP_CAPPROXY_AGENT_ID("cap.caproxy.agent.id"),
		CAP_CAPPROXY_AGENT_PASSWORD("cap.caproxy.agent.password"),
		CAP_AGENT_DASHBOARD_V2("cap.AgentDashboard"),
		CAP_LEAD_PARTNER_ID("cap.leadPartner.id"),
		CAP_LEAD_PARTNER_API_KEY("cap.leadPartner.apiKey"),
		CAP_APPOINTMENT_SCHEDULER("cap.appointmentScheduler"),
		CAP_LIVEOPS_OUTBOUND_API_USERNAME("cap.liveOps.outbound.api.username"),
		CAP_LIVEOPS_OUTBOUND_API_PASSWORD("cap.liveOps.outbound.api.password"),
		CAP_LIVEOPS_OUTBOUND_API_MEDICARE_LEAD_BATCH_ID("cap.liveOps.outbound.api.medicare.batchId"),
		CAP_APPOINTMENT_EMAILS("cap.appointmentEmails"),
		CAP_CSR_PASSWORD_RESET_RETRIES_LIMIT("security.CSR.PasswordResetRetryLimit"),
		CAP_CSR__SUPERVISOR_OVERRIDE_EMAIL("security.Supervisor.EnableOverrideEmail"),
		CAP_EDIT_AFFILIATE_FLOW("cap.edit.affiliate.flow");
		
		private final String value;	  

		@Override
		public String getValue(){return this.value;}

		CAPConfigurationEnum(String value){
	        this.value = value;
	    }
	};
	
	public enum CAPAgentsConfigurationEnum implements PropertiesEnumMarker
	{  
		MANAGE_AGENTS_GI ("cap.ManageAgentsGI");
		
		private final String value;	  

		@Override
		public String getValue(){return this.value;}

		CAPAgentsConfigurationEnum(String value){
	        this.value = value;
	    }
	};
}
