package com.getinsured.hix.platform.config;

/**
 * Configuration class for accessing AEE configuration. 
 * @ version 1.0
 */
public class AEEConfiguration {

	public enum AEEConfigurationEnum implements PropertiesEnumMarker
	{  	
		/** Property to verify whether Payment flow is enabled for Agent module or not.	*/		
		AGENT_PAYMENT_IS_ENABLED ("agent.payment.isEnabled"),
		/** Property to declare status of an Agent	*/
		AGENT_STATUS ("agent.status"),
		AGENT_ALLOWMAILNOTICES("agent.allowMailNotices"),
		AGENT_TO_EMPLOYER_DEDESIGNATION_NOTICE("agent.agentDedesignationByEmployerNotice"),
		ENROLLMENTENTITY_CEC_DEDESIGNATIONSTATUS("enrollmententities.counselors.dedesignationStatus"),		
		ENROLLMENTENTITY_CEC_ALLOWMAILNOTICES("enrollmententities.counselors.allowMailNotices"),
		
		AGENT_EMPLOYER_DESIGNATION_ACCEPTANCE_NOTICE("agent.employer.designationAcceptanceNotice"),
		AGENT_EMPLOYER_PENDING_TO_DECLINE_NOTICE("agent.employer.designationDeclineNotice"),
		AGENT_EMPLOYER_DEDESIGNATION_NOTICE("agent.EmployerInactivationByAgentNotice"),
		ADMIN_AGENT_INACTIVATION_NOTICE_TO_EMPLOYER("agent.employer.employerInactivationByAdminNotice"),
		ENROLLMENT_COUNSELOR_INDIVIDUAL_DEDESIGNATION_NOTICE("enrollmentCounselor.enrollmentCounselorDedesignationNoticebyIndividual"),
		INDIVIDUAL_AGENT_DE_DESIGNATION_NOTICE("agent.agentDedesignationByIndividualNotice"),
		AGENT_ACTIONABLENOTICESTOINDIVIDUALS_CONFIGURED("agent.actionableNoticesToIndividuals.configured"),
		AGENT_BOOKOFBUSINESSSUMMARYNOTICES_IS_ENABLED("agent.BookOfBusinessSummaryNotices.isEnabled"),
		AGENCY_PORTAL_ENABLE("agency.portal.enable"),
		//IND35 change start
		AEE_IND_IS_ENABLED("aee.ind.enabled"),
		AGENCY_PAYMENT_IS_ENABLED ("agency.payment.isEnabled");
		//IND35 change end
		
		private final String value;	  
		
		@Override
		public String getValue(){return this.value;}
		
		AEEConfigurationEnum(String value){
	        this.value = value;
	    }
	};
}