package com.getinsured.hix.platform.security.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.UserSession;

public interface IUserSessionRepository extends JpaRepository<UserSession, Long> {
	
	@Query("select us from UserSession as us where us.sessionId=:sessionId")
	public List<UserSession> findUserSession(@Param("sessionId") String sessionId);
	
	@Query("select us from UserSession as us where us.sessionId=:sessionId and us.status='IN_PROGRESS'")
	public List<UserSession> findActiveSessionBySessionId(@Param("sessionId") String sessionId);

	@Query("select us from UserSession as us where us.user.id=:userId and us.status='IN_PROGRESS'")
	public List<UserSession> findActiveUserSession(@Param("userId") int userId);
	
	@Query("select us from UserSession as us where us.nodeId=:localNodeAddress and us.status='IN_PROGRESS'")
	public List<UserSession> findLocalActiveSessions(@Param("localNodeAddress")String localNodeAddress);
	
	@Query("select us from UserSession as us where us.sessionId=:sessionId and us.nodeId=:localNodeAddress and us.status='IN_PROGRESS'")
	public List<UserSession> findLocalActiveSessionsBySessionId(@Param("localNodeAddress")String localNodeAddress, @Param("sessionId") String sessionId);

	@Query("select us from UserSession as us where us.user.id=:userId and us.status='INIT'")
	public List<UserSession> findRegisteredUserSession(@Param("userId") int userId);
	
	@Query("select us from UserSession as us where us.user.id=:userId and us.status !='IN_PROGRESS' and us.logoutTime >=:duration and us.sessionVal='MFA_CHECK:CHECKED' order by us.Id desc")
	public List<UserSession> findLastMFACheckedUserSession(@Param("userId") int userId, @Param("duration") Date duration);
}
