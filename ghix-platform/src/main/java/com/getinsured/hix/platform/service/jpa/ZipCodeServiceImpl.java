package com.getinsured.hix.platform.service.jpa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.service.ZipCodeService;

@Service("zipCodeService")
@Repository
@Transactional
public class ZipCodeServiceImpl implements ZipCodeService{
	private static final Logger logger = LoggerFactory.getLogger(ZipCodeServiceImpl.class);
	@Autowired private IZipCodeRepository zipCodeRepository;	
	
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zip}", unless="#result == null")
	public ZipCode findByZipCode(String zip) {
		//logger.info("== got all messages, size={} in findzipcode", zip);
		List<ZipCode> zipcodes = zipCodeRepository.findByZip(zip);	
		if(zipcodes.size()>0){
			return zipcodes.get(0);
		}
		else{
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
    @Override
    @Cacheable(value="zip", key="{#root.methodName,#zip,#stateCode}", unless="#result == null")
	public List<ZipCode> findByZipAndStateCode(String zip, String stateCode) {
	    return (List<ZipCode>) (StringUtils.isNumeric(zip) ? zipCodeRepository.findByZipAndStateCode(zip, StringUtils.upperCase(stateCode)) : Collections.emptyList()); 
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#city}", unless="#result == null")
	public List<ZipCode> getCityList(String city) {
		return zipCodeRepository.getCityList(city);	
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zipCode}", unless="#result == null")
	public List<ZipCode> getZipList(String zipCode) {
		return zipCodeRepository.getZipCodeList(zipCode);	
	}
	
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zipCode,#countyCode}", unless="#result == null")
	public boolean validateZipAndCountyCode(String zipCode, String countyCode){
		//logger.info("Validating Zipcode:"+zipCode+" against CountyCode:"+countyCode);
		if(zipCode==null || countyCode == null || zipCode.equalsIgnoreCase("%") || countyCode.equalsIgnoreCase("%")){
			return false;
		}
		List<ZipCode> zipcodes = zipCodeRepository.findByZipAndCountyCode(zipCode, countyCode);	
		if(zipcodes.size()>0){
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zipCode,#countyCode}", unless="#result == null")
	public boolean validateZipAndCountyCodeAndState(String zipCode, String countyCode,String stateCode){
		if(zipCode==null || countyCode == null || stateCode == null){
			return false;
		}
		List<ZipCode> zipcodes = zipCodeRepository.findByZipAndCountyCodeAndStateCode(zipCode, countyCode,stateCode);
		if(zipcodes != null && zipcodes.size() > 0 ){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * Fetch all record based on zip code
	 *
	 * @author Nikhil Talreja
	 * @since May 21, 2013
	 */
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zip}", unless="#result == null")
	public List<ZipCode> findByZip(String zip) {
		return zipCodeRepository.findByZip(zip);
	}

	/**
	 * Validate ZipCode with state
	 * 
	 * @author Jitendra Chaudhari
	 * @since Sept 14, 2013
	 */
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#stateCode,#zipCode}", unless="#result == null")
	public boolean validateZipByState(String stateCode, String zipCode) {
		Boolean isValidZip = false;
		List<String> zipCodes =  zipCodeRepository.findZipByState(stateCode);
		for(String zipVal : zipCodes){
			if(zipVal!=null){
				if(zipVal.equalsIgnoreCase(zipCode)){
					isValidZip=true;
				}
			}
		}
		return isValidZip;
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zips,#countyCode}", unless="#result == null")
	public List<String> getZipByCountyCodeAndZips(List<String> zips,String countyCode) {
		return  zipCodeRepository.getZipByCountyCodeAndZips(zips, countyCode);
	}
	
	/**
	 * @author Sunil Desu
	 */
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zipCode,#countyCode}", unless="#result == null")
	public ZipCode getZipcodeByZipAndCountyCode(String zipCode, String countyCode){
		//logger.info("Getting Zipcode record for zip:"+zipCode+" and CountyCode:"+countyCode);
		if(zipCode==null || countyCode == null || zipCode.equalsIgnoreCase("%") || countyCode.equalsIgnoreCase("%")){
			return null;
		}
		/**
		 * TODO Ideally zipcode and fips countycode combination should be unique. Update this later after confirmation.
		 */
		List<ZipCode> zipcodes = zipCodeRepository.getZipByZipAndCountyCode(zipCode, countyCode);	
		if(zipcodes==null || zipcodes.size() == 0){
			return null;
		}
		return zipcodes.get(0);
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#countyCode}", unless="#result == null")
	public String findCountyNameByCountyCode(String countyCode) {
		
		List<String> zipcodes = zipCodeRepository.findCountyNameByCountyCode(countyCode);	
		if(zipcodes.size()>0)
		{
		return zipcodes.get(0);
		}
		return "";
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zips,#countyCode}", unless="#result == null")
	public List<String> getCountyCodes(List<String> zips, String countyCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Cacheable(value="zip", key="{#root.methodName,#stateCode}", unless="#result == null")
	public List<ZipCode> getCountyListForState(String stateCode) {
		// TODO Auto-generated method stub
		
		List<ZipCode> zipcodes = zipCodeRepository.findByState(stateCode);
		if(zipcodes==null ){
			return null;
		}
		return zipcodes;
	}

	
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#stateCode,#county}", unless="#result == null")
	public String getCountyCodeByStateAndCounty(String stateCode, String county) {

		List<String> zipcodes = zipCodeRepository.findCountyNameByStateAndCounty(stateCode,county);	
		if(zipcodes.size()>0)
		{
		return zipcodes.get(0);
		}
		return "";

	}
	
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zip,#state,#countyCode}", unless="#result == null")
	public String findCountyNameByZipStateAndCountyCD(String zip, String state, String countyCode) {
		
		List<String> countyNames = zipCodeRepository.findCountyNameByZipStateAndCountyCD(zip, state, countyCode);
		if(countyNames!=null && countyNames.size()>0)
		{
			return countyNames.get(0);
		}
		return "";
	}
	
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zip,#state,#countyCode}", unless="#result == null")
	public String findStateFIPSByZipStateAndCountyCD(String zip, String state,
			String countyCode) {
		
		List<String> StateFips = zipCodeRepository.findStateFIPSByZipStateAndCountyCD(zip, state, countyCode);
		if(StateFips!=null && StateFips.size()>0)
		{
			return StateFips.get(0);
		}
		return "";
	}
	
	@Override
	@Cacheable(value="zip", key="{#root.methodName,#zip,#stateCode}", unless="#result == null")
	public List<ZipCode> getCountyListForZipAndState(String zip,String stateCode) {
		// TODO Auto-generated method stub
		
		List<ZipCode> zipcodes = zipCodeRepository.findByZipAndState(zip, stateCode);
		if(zipcodes==null ){
			return null;
		}
		return zipcodes;
	}
	
	
	@Override
	public List<String> getStateListByAreaCode(String areaCode) {
		// TODO Auto-generated method stub
		List<String> stateCodes = new ArrayList<String>();
		stateCodes = zipCodeRepository.findStateByAreaCode(areaCode);
		
		return stateCodes;
	}
	
}