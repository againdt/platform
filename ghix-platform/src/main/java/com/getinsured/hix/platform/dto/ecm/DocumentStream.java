package com.getinsured.hix.platform.dto.ecm;

import java.util.Arrays;



public class DocumentStream {

	// instance variables

	private String encoding = null;

	private String isoCode = null;

	private byte[] content = null;

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public byte[] getContent() {
		return content.clone();
	}

	public void setContent(byte[] content) {
		this.content = content.clone();
	}

	@Override
	public String toString() {
		return "DocumentStream [encoding=" + encoding + ", isoCode=" + isoCode
				+ ", content=" + Arrays.toString(content) + "]";
	}


	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
	
	
}
