package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.affiliate.model.Affiliate;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface ITenantUnawareAffiliateRepository extends TenantUnawareRepository<Affiliate,Long> {
	
	@Query(value="select aff from Affiliate aff where aff.tenantId = :tenantId")
	public List<Affiliate> getAffiliates(@Param("tenantId") Long tenantId);

	public Affiliate findByAffiliateIdAndTenantId(Long affiliateId,Long tenantId);
}
