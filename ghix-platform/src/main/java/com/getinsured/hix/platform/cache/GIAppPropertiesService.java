package com.getinsured.hix.platform.cache;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.platform.cache.repository.GIAppPropertiesRepository;
import com.getinsured.hix.platform.config.PropertiesEnumMarker;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;

/**
 * Properties service layer. This service allows you to fetch, save, delete
 * properties defined in table mapped by {@link com.getinsured.hix.model.GIAppProperties} object.
 * 
 * @author Yevgen Golubenko
 */
public class GIAppPropertiesService 
{
	private static Log log = LogFactory.getLog(GIAppPropertiesService.class);
	
	@Autowired
	private GIAppPropertiesRepository gIAppPropertiesRepository;
	
	/**
	 * Local log method, because I dont want to modify log4j logging level but rather want to increase priority 
	 * right here.
	 * 
	 * @param message message string to log.
	 */
	private static void log(String message)
	{
		log.warn(message);
	}
	
	/**
	 * Local log method.
	 * 
	 * @param message message to log
	 * @param e exception.
	 */
	private static void log(String message, Exception e)
	{
		log.warn(message, e);
	}
	
	/**
	 * Returns list of all properties defined. 
	 * Consider looking at {{@link #findAllByIds(Collection)} and {{@link #findByPropertyKeyIn(Collection)} 
	 * methods, because they are giving you ability to fetch smaller sets of all records, especially if you 
	 * know property keys and ids that you need.
	 * 
	 * @return Iterable list of properties.
	 */
	public Iterable<GIAppProperties> findAllProperties()
	{
		log("GIAppPropertiesService.findAllProperties()");
		return gIAppPropertiesRepository.findAll();
	}
	
	/**
	 * Returns list of properties by given collection of ids.
	 * 
	 * @param ids Integer ids of the properties that you need.
	 * @return Iterable list of properties
	 * @see #findByPropertyKeyIn(Collection)
	 * @see #getByPropertyId(Integer)
	 */
	public Iterable<GIAppProperties> findAllByIds(Collection<Integer> ids)
	{
		log("GIAppPropertiesService.findAllByIds(" + ((ids != null) ? ids.size() : "<null>") + ")");
		return gIAppPropertiesRepository.findAll(ids);
	}
	
	/**
	 * Returns list of properties that have property key equals to given 
	 * collection of property keys. 
	 * 
	 * @param keys collection of property keys.
	 * @return Iterable list of properties
	 */
	public Iterable<GIAppProperties> findByPropertyKeyIn(Collection<String> keys)
	{
		log("GIAppPropertiesService.findByPropertyKeyIn(" + ((keys != null) ? keys.size() : "<null>") + ")");
		return gIAppPropertiesRepository.findByPropertyKeyIn(keys);
	}

	/**
	 * Returns unique property for given property id.
	 * 
	 * @param propertyId property id.
	 * @return unique property record.
	 * @see #findAllByIds(Collection)
	 */
	public GIAppProperties getByPropertyId(Integer propertyId)
	{
		log("GIAppPropertiesService.getByPropertyId(" + propertyId + ")");
		return gIAppPropertiesRepository.findOne(propertyId);
	}
	
	/**
	 * Returns single property for given property key.
	 * 
	 * @param propertyKey property key.
	 * @return unique property record.
	 */
	public GIAppProperties findByPropertyKey(String propertyKey) throws GIException
	{
		log("GIAppPropertiesService.findByPropertyKey(" + propertyKey + ")");
		GIAppProperties p = gIAppPropertiesRepository.getByPropertyKey(propertyKey);
		
		if(p == null)
		{
			log.error("No property defined in gi_app_config table for property key [" + propertyKey + "]");
			throw new GIException(GhixErrors.APP_CONFIG_KEY_NOT_FOUND);
		}
		
		return p;
	}
	
	/**
	 * Helper method to find property by given enumeration which implements {@link PropertiesEnumMaker}
	 * 
	 * @param enumMaker {@link PropertiesEnumMarker}
	 * @return property value.
	 * @see #findByPropertyKey(String)
	 */
	public GIAppProperties findByPropertyKey(PropertiesEnumMarker enumMaker) throws GIException
	{
		log("GIAppPropertiesService.findByPropertyKey <ENUM>(" + enumMaker.getValue() + ")");
		return findByPropertyKey(enumMaker.getValue());
	}
	
	/**
	 * Saves property record.
	 * 
	 * @param property property record to save.
	 * @return saved property record.
	 */
	public GIAppProperties save(GIAppProperties property)
	{
		log("GIAppPropertiesService.save(" + ((property!=null)? property.getPropertyKey() : "<null>") + ")");
		return gIAppPropertiesRepository.save(property);
	}
	
	/**
	 * Saves collection of property records.
	 * 
	 * @param props collection of property records.
	 * @return saved collection of property records.
	 */
	public Iterable<GIAppProperties> save(Collection<GIAppProperties> props)
	{
		log("GIAppPropertiesService.save(" + ((props != null) ? props.size() : "<null>") + ")");
		return gIAppPropertiesRepository.save(props);
	}
	
	/**
	 * Returns total number of property records stored.
	 * 
	 * @return total number of property records.
	 */
	public long count()
	{
		log("GIAppPropertiesService.count()");
		return gIAppPropertiesRepository.count();
	}
	
	/**
	 * Checks to see if property with given id exists.
	 * 
	 * @param propertyId property id to check.
	 * @return true if property with this id exists, false otherwise.
	 */
	public boolean exists(Integer propertyId)
	{
		log("GIAppPropertiesService.exists(" + propertyId + ")");
		return gIAppPropertiesRepository.exists(propertyId);
	}
	
	/**
	 * Returns true if property with given property key exists.
	 * 
	 * @param propertyKey property key.
	 * @return true if property exists, false otherwise.
	 */
	public boolean exists(String propertyKey)
	{
		log("GIAppPropertiesService.exists(" + propertyKey + ")");
		
		GIAppProperties property = null;
		try
		{
			property = findByPropertyKey(propertyKey);
		}
		catch (GIException e)
		{
			log("property does not exists: " + propertyKey, e);
		}
		
		if(property != null)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Deletes given property record.
	 * 
	 * @param property property record to delete.
	 */
	public void delete(GIAppProperties property)
	{
		log("GIAppPropertiesService.delete(" + property + ")");
		gIAppPropertiesRepository.delete(property);
	}
	
	/**
	 * Deletes property record for given property id.
	 * 
	 * @param propertyId property id of the record that needs to be deleted.
	 */
	public void delete(Integer propertyId)
	{
		log("GIAppPropertiesService.delete(" + propertyId + ")");
		gIAppPropertiesRepository.delete(propertyId);
	}
}
