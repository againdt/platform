package com.getinsured.hix.platform.multitenant.resolver.exception;

public class TenantRuntimeException extends RuntimeException {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1053471180051791761L;

	public TenantRuntimeException() {
		super();
	}

	public TenantRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public TenantRuntimeException(String message) {
		super(message);
	}

	public TenantRuntimeException(Throwable cause) {
		super(cause);
	}

}