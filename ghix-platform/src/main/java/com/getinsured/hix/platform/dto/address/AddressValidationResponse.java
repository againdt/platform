package com.getinsured.hix.platform.dto.address;

import java.util.List;

public class AddressValidationResponse {

	private LocationDTO input;
	private boolean valid;
	private boolean exactMatch;
	private List<LocationDTO> suggestions;

	public LocationDTO getInput() {
		return input;
	}

	public void setInput(LocationDTO input) {
		this.input = input;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isExactMatch() {
		return exactMatch;
	}

	public void setExactMatch(boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	public List<LocationDTO> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(List<LocationDTO> suggestions) {
		this.suggestions = suggestions;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AddressValidatorResponse [input=");
		builder.append(input);
		builder.append(", valid=");
		builder.append(valid);
		builder.append(", exactMatch=");
		builder.append(exactMatch);
		builder.append(", suggestions=");
		builder.append(suggestions);
		builder.append("]");
		return builder.toString();
	}

}
