package com.getinsured.hix.platform.notices;

import org.springframework.stereotype.Component;

/**
 * 
 * @author Sunil Desu
 * @since September 17 2013
 * 
 */
@Component
public class TemplateTokens {

	public static String EXCHANGE_TYPE = "exchangeType";
	public static String STATE_NAME = "stateName";
	public static String STATE_CODE = "stateCode";
	public static String EXCHANGE_NAME = "exchangeName";
	public static String EXCHANGE_PHONE = "exchangePhone";
	public static String EXCHANGE_URL = "exchangeURL";
	public static String CITY_NAME = "cityName";
	public static String PIN_CODE = "pinCode";
	public static String EXCHANGE_ADDRESS_1 = "exchangeAddress1";
	public static String EXCHANGE_ADDRESS_2 = "exchangeAddress2";
	public static String EXCHANGE_FULL_NAME = "exchangeFullName";
	public static String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	public static String COUNTRY_NAME = "countryName";
	public static String EXCHANGE_ADDRESS_EMAIL = "exchangeAddressEmail";
	public static String USER_FIRST_NAME = "firstName";
	public static String USER_LAST_NAME = "lastName";
	public static String USER_FULL_NAME = "fullName";
	public static String ADDRESS_LINE_1 = "addressLine1";
	public static String ADDRESS_LINE_2 = "addressLine2";
	public static String NOTICE_UNIQUE_ID = "noticeUniqueId";
	
	public static String TODAYS_DATE = "todaysDate";
	
	
	public static String HEADER_CONTENT = "headerContent";
	public static String FOOTER_CONTENT = "footerContent";
	public static String SPAINISH_FOOTER_CONTENT = "footerContentEsp";
	public static String ADDRESS_CONTENT = "addressContent";
	public static String ADDRESS_CONTENT_SPANISH = "addressContentSpanish";
	public static String HOST = "host";
	public static String APPSERVER_URL = "appserverUrl";
	public static String PRIVACY_STATEMENT = "privacy_statement";
	public static String PRIVACY_URL = "privacyUrl";
	public static String FOOTER_YEAR = "footerYear";
	public static String PRIVACY_URL_ESP = "privacyUrlEsp";
	public static String CONTACT_INFORMATION = "contact_information";
	public static String SHOP_DASHBOARD ="exchangeShopERDashboardURL";
	
	
}
