package com.getinsured.hix.platform.interceptors;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.aop.framework.Advised;

import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepositoryImpl;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

public class TenantInterceptor {

	private static final String TENANT_ID = "tenantId";
	private static final String TENANT_ID_FILTER = "tenantIdFilter";
	private static Logger LOGGER = Logger.getLogger(TenantInterceptor.class);

	@SuppressWarnings("rawtypes")
	public Object enableTenantFilter(ProceedingJoinPoint jp) throws Throwable {

		try {
			if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
				Object repositoryObject = ((Advised) jp.getTarget()).getTargetSource().getTarget();
				if(repositoryObject instanceof EnversRevisionRepositoryImpl) {
					EnversRevisionRepositoryImpl enversRevisionRepositoryImpl = (EnversRevisionRepositoryImpl) repositoryObject;
					Session session = (Session) enversRevisionRepositoryImpl.getEntityManager().getDelegate();
					if(session.isOpen()) {
						Filter filter = session.enableFilter(TENANT_ID_FILTER);
						filter.setParameter(TENANT_ID, TenantContextHolder.getTenant().getId());
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error enabling tenant filter", e);
		}
		return jp.proceed();
	}
	
	@SuppressWarnings("rawtypes")
	public Object disableTenantFilter(ProceedingJoinPoint jp) throws Throwable {
		
		try {
				Object repositoryObject = ((Advised) jp.getTarget()).getTargetSource().getTarget();
				if(repositoryObject instanceof EnversRevisionRepositoryImpl) {
					EnversRevisionRepositoryImpl enversRevisionRepositoryImpl = (EnversRevisionRepositoryImpl) repositoryObject;
					Session session = (Session) enversRevisionRepositoryImpl.getEntityManager().getDelegate();
					if(session.isOpen()) {
						session.disableFilter(TENANT_ID_FILTER);
					}
				}
		} catch (Exception e) {
			LOGGER.error("Error disabling tenant filter", e);
		}
		return jp.proceed();
	}
}