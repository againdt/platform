package com.getinsured.hix.platform.pdf.calculator.factory;


import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.platform.pdf.calculator.calculators.FieldCalculator;
import com.getinsured.hix.platform.pdf.calculator.calculators.Simple;
import com.getinsured.hix.platform.util.exception.FieldCalculatorNotFoundException;

public class FieldCalculatorFactory {

	private final Map<String, FieldCalculator> calculators = new HashMap<String, FieldCalculator>();

	private static FieldCalculatorFactory instance = new FieldCalculatorFactory();

	public static FieldCalculatorFactory getFactory() {
		return instance;
	}

	private FieldCalculatorFactory() {
		calculators.put(null, (FieldCalculator) new Simple());
	}

	public FieldCalculator create(String fieldTypeName) throws FieldCalculatorNotFoundException {
		FieldCalculator calculator = calculators.get(fieldTypeName);
		if (calculator == null) {
			throw new FieldCalculatorNotFoundException(fieldTypeName);
		}
		return calculator;
	}

	public boolean contains(String type) {
		return calculators.containsKey(type);
	}
}
