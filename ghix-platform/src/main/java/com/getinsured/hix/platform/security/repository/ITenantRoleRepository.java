package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.TenantRole;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public interface ITenantRoleRepository extends JpaRepository<TenantRole,Long>{
	
	@Query(name = "findTenantRoleMapping",nativeQuery=true, value = "SELECT r.name , "
			+ " r.IS_TENANT_DEFAULT,  tr.IS_ACTIVE, tr.id "
			+ " FROM tenant_roles tr  INNER JOIN Roles r "
			+ " ON tr.ROLE_ID = r.id  INNER JOIN Tenant t "
			+ " ON tr.TENANT_ID = t.id " 
			+ " WHERE t.name= :tenantName")
	public List<Object[]> findTenantRoleMapping(
			@Param("tenantName") String tenantName);
	
	public List<TenantRole> findByIdIn(List<Long> tenantRolesIdList);
	
	@Query(value="select tr from TenantRole tr where tr.tenant.name= :tenantName")
	public List<TenantRole> findByTenantName(@Param("tenantName") String tenantName);
	

}
