/**
 * Declares the CRUD operations to manage User Profile
 * @author venkata_tadepalli
 * @since 11/30/2012
 */
package com.getinsured.hix.platform.security.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.security.RemoteValidationFailedException;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.WSO2Exception;

public interface UserService {
	static int CONFIRMED = 1; // ???
	
	String LINK_AFFILIATE_ROLE = "affiliate";
	
	//CREAT Operations
	AccountUser createUser(AccountUser newUser,String userRoleName) throws GIException;
	
	@Deprecated
	ModuleUser createModuleUser(int moduleId, String moduleName, AccountUser user);//HIX-26903
	ModuleUser createModuleUser(int moduleId, String moduleName, AccountUser user,boolean isSelfSigned) throws GIException;//HIX-26903	

	//READ Operations
	
	AccountUser findByEmail(String email);
	
	AccountUser findByExtnAppUserId(String extAppUserId); // HIX-42022 - Implement hybrid authentication (sso and db login)
	
	AccountUser findById(int id);
	
	AccountUser findByUserName(String userName);
	
	AccountUser findByUserNameAndEmail(String userName, String email);
	
	boolean isPasswordPresentInHistory(AccountUser userObj, String newPassword, int passwordHistoryLimit);

	/**
	 * HIX-49151
	 * Returns the logged in user object.
	 * NOTE: This method will be deprecated after re-factoring the getLoggedInUser (after removal of InvalidUserException)
	 * -Venkata Tadepalli: Sept-10-2014
	 * 
	 * @return AccountUser
	 */
	AccountUser getPrincipalUser();

	AccountUser getLoggedInUser() throws InvalidUserException;

	

	AccountUser findUserByRecovery(String recovery);

	//READ- Attribute level operations

	List<String> getUserPermissions(AccountUser user);

	Role getDefaultRole(AccountUser user);
	
	Set<Role> getAllRolesOfUser(AccountUser user);
	
	int getUsersMaxRetryCount(AccountUser user);
	
	boolean hasUserRole(AccountUser user, String moduleName);
	
	boolean hasAuthorization(String roleName,Model model, HttpServletRequest request);
	
	boolean isEmployeeAdmin(AccountUser user);

	boolean isEmployee(AccountUser user);

	//boolean isEmployer(AccountUser user);

	//boolean isIssuer(AccountUser user);

	boolean isAdmin(AccountUser user);
	
	boolean isAffiliateAdmin(AccountUser user);

	boolean isIssuerRep(AccountUser user);

	boolean isBroker(AccountUser user);

	//READ-Module- Attribute level operations
	
	List<ModuleUser> getUserModules(int userId); //by UserId
	List<ModuleUser> getUserModules(int userId,String moduleName);//by UserId and moduleName
	List<ModuleUser> getModuleUsers(int moduleId); //by moduleId
	List<ModuleUser> getModuleUsers(int moduleId,String moduleName);//by moduleId and moduleName
	List<Integer> getModuleIdsByModuleName(String moduleName);
	
	ModuleUser findModuleUser(int moduleId,String moduleName,AccountUser user);
	ModuleUser getUserDefModule(String defModuleName,int userId);

	//HIX-28851 Application error is displayed on switching from Agent to Employer
	ModuleUser getUserSelfSignedModule(String defModuleName,int userId);
	//Update Operations

	AccountUser updateUser(AccountUser accountUser);

	void addUserRole(AccountUser user, Role userRole);
	
	
	void addUserRole(AccountUser user, String userNewRoleName) throws Exception; // HIX-26903	

	void updateUserRole(AccountUser user,Role userRole); // New

	void updateModuleUser(AccountUser user,ModuleUser userModules); // New

	//void savePassword(int id, String password);// HIX-32110 : use passwordService.updatePassword

	void updateConfirmed(AccountUser user,int confFlag); // Used to set the account as active=1 / inactive=0;

	void updateLastLogin(AccountUser user) throws Exception; //

	void invalidateConfirmed(AccountUser user) throws Exception; //
	
	//Delete Operations
	void deleteModuleUser(AccountUser user, ModuleUser userModules);

	//Service Operations
	/**
	 * Returns the User Name of specified Id
	 */
	String getUserName(int userId);

	AccountUser savePasswordRecoveryDetails(int id, String passwordRecoveryToken);

	AccountUser savePasswordRecoveryDetails(String email, String passwordRecoveryToken);

	AccountUser findByPasswordRecoveryToken(String passwordRecoveryToken);
	
	String getAutoLoginUrl(AccountUser accountUser);
	
	String getAutoLoginUrl(String userName, String password); // HIX-42022 - Implement hybrid authentication (sso and db login)
	
	void enableRoleMenu(HttpServletRequest request, String roleName);
	
	List<AccountUser> getAccountUsersByUserRole(int roleId);

	List<Integer> getAdminUsers();

	List<AccountUser> getAdminAndCSRRoleUsers();
	
	List<AccountUser> getUsersByRoleName(String roleName);
	
	List<AccountUser> getAgentList();
	
	List<String> migrateUsers(String userPattern);
	
	List<String> disableInactiveAccounts();
	boolean isEnrollmentEntity(AccountUser user);
	
	Map<String, Object>  searchUsers(Map<String, Object> searchCriteria);
	
	List<String>  findAllUsersRoles();
	
	void changeUserStatus(int confirmed,int id);
	
	void updateStatusCode(String status,int id);
	
	//public Role findRoleById(int  roleId);
	
	List<ModuleUser> getModuleUsersByModuleIds(List<Integer> moduleIdList, String moduleName);
	
	void saveFFMUserId(int id, String ffmUserId);
	boolean hasPrincipalChanged(String userName);
	boolean isPasswordMatch(AccountUser user,String pwd1, String pwd2);  // HIX-32110 :Modify Change/Reset Password Process to accept salted password
	boolean isPasswordSameInDatabase(AccountUser user, String pwd1, String pwd2);
	void setTenantContext(String userName);
	void signOutTenantContext(String userName);
	
	List<String> getIssuerRepresentativeEmailIds(int userRoleId);

	/**
	 * method to update user retry count
	 * @param user
	 * @param confFlag
	 */
	void updateRetryCount(AccountUser user,int retryCount);
	
	/**
	 * 
	 * method to update user confirmed flag & retry count
	 * 
	 * @param user
	 * @param confFlag
	 * @param retryCount
	 */
	void updateConfirmedAndRetryCount(AccountUser user,int confFlag, int retryCount);
	
	
	/**
	 * This will return a set of non default userRoles for specified account user.
	 * @param user
	 * @return set of non default userRoles
	 */
	Set<UserRole> getNonDefaultUserRole(AccountUser user);
	
	/**
	 * This method will update the UserRole with updated values and 
	 * set the updated list of userRoles to current user object. 
	 * @param user
	 * @param toUpdateUserRole
	 * @return {@link UserRole} updated userRole
	 */
	UserRole updateUserRole(AccountUser user,UserRole toUpdateUserRole);
	
	
	/**
	 * This will return the configured {@link UserRole} object depending on parameter roleName.
	 * @param user
	 * @param roleName
	 * @return {@link UserRole} userRole
	 */
	UserRole getUserRoleByRoleName(AccountUser user, String roleName);
	boolean hasActiveUserRole(AccountUser user, String roleName);
	
	/**
	 * method to update user security question retry count
	 * @param user
	 * @param secQueRetryCount
	 */
	void updateSecQueRetryCount(AccountUser user,int secQueRetryCount);
	
	/**
	 * 
	 * method to update user confirmed flag & security question retry count
	 * 
	 * @param user
	 * @param confFlag
	 * @param retryCount
	 */
	void updateConfirmedAndSecQueRetryCount(AccountUser user,int confFlag, int secQueRetryCount);
	
	/**
	 * method to update user login retry count & security question retry count
	 * @param user
	 * @param confFlag
	 */
	void updateRetryCounts(AccountUser user,int retryCount, int secQueRetryCount);
	
	/**
	 * Updates new password for the given User
	 * @param newPassword
	 * @param userId
	 * @return true if password changed successfully
	 */
	boolean changePassword(int userId, String newPassword);
	
	/**
	 * Load history for the given User
	 * @param userId
	 */
	List<Map<String, String>> loadUserHistory(String userName);
	
	/**
	 * Load history for the given User
	 * @param userId
	 */
	Set<String> getUserPasswordHistory(String userName);

	/**
	 * This will return a set of active userRoles for specified account user.
	 * @param user
	 * @return set of non default userRoles
	 */
	Set<UserRole> getActiveUserRole(AccountUser user); //HIX-30004
	
	void updateIndividualUserAsDangling(int userId) throws GIException;
	
	void saveFFMDetails(String ffmUsername,String ffmPassword,String proxy,int userId);
	
	void updateFFMDetails(String ffmUsername,String ffmPassword,String proxy,int userId);
	
	List<Object[]> getAccountUsersInfoByUserRole(int roleId);
	
	List<Object[]> getAccountUsersInfoByRoleAndName(int parseInt,String userName);
	
	int getSuperUserId(String emailAddress);

	List<AccountUser> findAccountUsersForRoleNames(List<String> roleNameList);
	
	List<Integer> getAccountUserIdsForRoleNames(List<String> roleNameList,Long tenantId);

	Map<Integer, String> getWorkgroupUsersToAdd(String username);

	List<AccountUser> findAll();
	
	AccountUser updateUserPHIX(AccountUser accountUser);

	void changeEmail(AccountUser accountUser);

	List<AccountUser> getUserNames(List<Integer> userIds);

	List<AccountUser> getUserNameAndPhoneByUserIds(List<Integer> userIds);

	void checkUpdatesAllowed(AccountUser user);

	void checkIfRoleUpdateAllowed(AccountUser user);
	
	void updateUserStatusAndConfirmedFields(List<Integer> userIds, String status,Integer confirmed);
	
	boolean hasAccessToUpdateUser(AccountUser user); 
	
	void updateUserAffiliate(ModuleUser moduleUser, String affilaiteId,	AccountUser user) throws GIException;

	AccountUser markAccountInactive(AccountUser user);
	
	boolean changePasswordOnWSO2(HttpServletRequest request, String newPassword,  String currentPassword) throws GIException, WSO2Exception, InvalidUserException;

	AccountUser findByUserName(String userName, boolean validateExternal) throws RemoteValidationFailedException;

	AccountUser getBasicInfoByUserName(String userName);

	boolean changePasswordSelf(int userId, String existingPassword, String newPassword );

	boolean changePasswordAdmin(int currentUserId, String parameter);

	AccountUser updateUserName(AccountUser accountUser, String password);

	AccountUser updateUserName(AccountUser user, String newEmail, String currentPassword);
	
	AccountUser findByPhoneAndUserName(String phone, String userName);
	
	List<AccountUser> findByPhone(String phone);

	void updateUserStatusWithRetries(AccountUser user, int confFlag, String status, int retryCount);
	AccountUser createLocalUser(AccountUser newUser, String roleName) throws GIException;

	AccountUser findByUserNpn(String userNPN);

	ModuleUser createOrUpdateModuleUser(int moduleId, String moduleName, AccountUser user, boolean isSelfSigned)
			throws GIException;

	List<ModuleUser> getUserSelfSignedModules(String defModuleName, int userId);
	
	List<Object[]> getAccountUsersInfoByName(String userName);
}
