/**
 * 
 */
package com.getinsured.hix.platform.test;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

//import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
//import org.apache.commons.dbcp.datasources.SharedPoolDataSource;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.xerces.parsers.DOMParser;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @Created 16-APR-2014 It is the base Test class for MVC Junit tests 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ 
"classpath:/applicationSecurity-db-unittests.xml",
"classpath:/applicationContext.xml"})
public abstract class GhixBaseMvcTest {

	private static final Logger log = LoggerFactory
			.getLogger(GhixBaseMvcTest.class);
	protected static InitialContext		ic	= null;
	protected static Map<String, String> connInfo = null;

	protected static String				DB_CONNECT_URL	= "jdbc:oracle:thin:@ghixdevdb.com:1521:ghixdb";
	protected static String				DB_USERNAME		= null;
	protected static String				DB_PASSWORD		= null;
	protected static String				DB_JNDI_NAME	= "jdbc/ghixDS";
	protected static String             DB_JDBC_DRIVER  = "oracle.jdbc.OracleDriver";
	
	@BeforeClass
	public static void setUp() {
		try{
		
		//This helps to load application context.
		//context = new ClassPathXmlApplicationContext("/applicationContext.xml");
		
		connInfo = getCatalinaContextXml();

		if(connInfo != null)
		{
			for (String key : connInfo.keySet())
			{
				System.out.format("%-30.25s%-40.90s%n", key, connInfo.get(key));
			}
		}
		else
		{
			System.out.println("Could not read connection configuration from context.xml");
		}

		
		log.info("Binding JNDI DataSource");

		bindJNDIDataSourceViaSpringContextBuilder();

		log.info("Finished binding JNDI data source");

		//This helps to load log4j file
		//String rootPath = System.getProperty("GHIX_HOME");
		String rootPath = System.getenv("GHIX_HOME");
		
		DOMConfigurator.configure(rootPath + "/ghix-setup/conf/ghix-log4j.xml");
		}catch(Exception ex)
		{
			
			log.error("",ex);
		}
	}
	
	

	/**
	 * Creates new JNDI Data Source and binds it to the system, so that
	 * inside of applicationContext.xml whenever we refer to jdbc/ghixDS
	 * it will be working.
	 */
	protected static void bindJNDIDataSourceViaSpringContextBuilder()
	{
		try
		{
			SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
			OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();

			ds.setURL(getDbConnectUrl());
			ds.setUser(getDbUsername());
			ds.setPassword(getDbPassword());

			builder.bind(DB_JNDI_NAME, ds);
			builder.activate();
		}
		catch (NamingException ne)
		{
			ne.printStackTrace();
			log.error("bindJNDIDataSourceViaSpringContextBuilder() " +
					"NamingException occurred while registring JNDI " + getDbJndiName(), ne);
		}
		catch (SQLException sex)
		{
			sex.printStackTrace();
			log.error("bindJNDIDataSourceViaSpringContextBuilder() " +
					"SQLException occurred while creating OracleConnectionPoolDataSource()", sex);
		}
	}
	
	/**
	 * Returns database connection url.
	 * 
	 * @return database connection url.
	 */
	protected static String getDbConnectUrl()
	{
		if(connInfo != null && connInfo.containsKey("url"))
		{
			return connInfo.get("url");
		}
		
		return DB_CONNECT_URL;
	}
	
	/**
	 * Returns database username.
	 * 
	 * @return username to connect to database.
	 */
	protected static String getDbUsername()
	{
		if(connInfo != null && connInfo.containsKey("username"))
		{
			return connInfo.get("username");
		}
		
		return DB_USERNAME;
	}
	
	/**
	 * Returns database password.
	 * 
	 * @return password to be used to connect to database.
	 */
	protected static String getDbPassword()
	{
		if(connInfo != null && connInfo.containsKey("password"))
		{
			return connInfo.get("password");
		}
		
		return DB_PASSWORD;
	}
	
	/**
	 * Returns JNDI name to bind to the system for database access.
	 * 
	 * @return JNDI name.
	 */
	protected static String getDbJndiName()
	{
		if(connInfo != null && connInfo.containsKey("name"))
		{
			return connInfo.get("name");
		}
		
		return DB_JNDI_NAME;
	}

	/**
	 * Returns JDBC driver used for database connection.
	 * 
	 * @return jdbc driver class name (as string) used for database connection.
	 */
	protected static String getDbJdbcDriver()
	{
		if(connInfo != null && connInfo.containsKey("driver"))
		{
			return connInfo.get("driver");
		}
		
		return DB_JDBC_DRIVER;
	}
	
	/**
	 * Returns key/value pairs from context.xml for jndi data bind
	 * 
	 * @return Map or null.
	 */
	private static Map<String, String> getCatalinaContextXml()
	{
		String catalinaHome = System.getenv("CATALINA_HOME");
		String vfabricHome = System.getenv("VFABRIC_HOME");

		// /home/jeneag/projects/workspaces/sts-3.8.2/Servers/VMware vFabric tc Server Developer Edition v2.9-config/context.xml

		String xmlFileName = "context.xml";
		String xmlFilePath = "";

		System.out.println("  Using CATALINA_HOME: " + catalinaHome);
		System.out.println("         VFABRIC_HOME: " + vfabricHome);

		// If we have VFABRIC_HOME, lets check it first...
		if(vfabricHome != null)
		{
			System.out.println("VFABRIC_HOME environment variable is set, checking for base-instance/conf/" + xmlFileName);
			xmlFilePath = vfabricHome + File.separatorChar + "base-instance" + 
							File.separatorChar + "conf" + File.separatorChar + xmlFileName;
			File f = new File(xmlFilePath);
			
			if(f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				Document doc = parseXmlFile(xmlFilePath);
				
				if(doc != null)
				{
					Map<String, String> kv = getMapFromDocument(doc);
					
					if(kv != null) 
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}

		// are we supplied vfabric path?
		if(catalinaHome != null && catalinaHome.toLowerCase().contains("vfabric"))
		{
			System.out.println("CATALINA_HOME is pointing to Eclipse's vFabric instance: " + catalinaHome);
			xmlFilePath = catalinaHome + File.separatorChar + xmlFileName;
			
			File f = new File(xmlFilePath);
			
			if(f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				Document doc = parseXmlFile(xmlFilePath);
				
				if(doc != null)
				{
					Map<String, String> kv = getMapFromDocument(doc);
					
					if(kv != null) 
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}
		else
		{
			System.out.println("CATALINA_HOME is pointing to Tomcat's instance: " + catalinaHome);
			xmlFilePath = catalinaHome + File.separatorChar + "conf" + File.separatorChar + xmlFileName;
			
			File f = new File(xmlFilePath);
			
			if(f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				Document doc = parseXmlFile(xmlFilePath);
				
				if(doc != null)
				{
					Map<String, String> kv = getMapFromDocument(doc);
					
					if(kv != null) 
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}
		

		return null;
	}

	/**
	 * Parses given XML file and returns Document object.
	 * 
	 * @param xmlFilePath absolute xml file path.
	 * @return Document object or null if unable to parse or exeption occurrs.
	 */
	private static Document parseXmlFile(String xmlFilePath)
	{
		DOMParser parser = new DOMParser();
		Document doc = null;
		
		try
		{
			parser.parse(xmlFilePath);

			doc = parser.getDocument();
		}
		catch (Exception e)
		{
			log.error("Could not get Document from given XML file: " + xmlFilePath, e);
		}
		

		return doc;
	}

	/**
	 * Gets Map of XML attribute names/values from context.xml file.
	 * 
	 * @return {@link Map} object or null if context.xml file not found or
	 *         doesn't have &lt;Resource/&gt;
	 *         child defined.
	 */
	protected static Map<String, String> getMapFromDocument(Document doc)
	{
		if (doc != null)
		{
			System.out.println("Reading context.xml file and parsing relavent data into Map<String,String> ...");

			NodeList root = doc.getChildNodes();
			Node context = getNode("Context", root);

			if (context == null)
			{
				System.out.println("No <Context> node found, ignoring this context.xml");
				return null;
			}
			
			Node resource = getNode("Resource", context.getChildNodes());

			if (resource == null)
			{
				System.out.println("No <Resource> node found, ignoring this context.xml");
				return null;
			}
			
			Map<String, String> connectionMap = new HashMap<String, String>();

			connectionMap.put("auth", getNodeAttr("auth", resource));
			connectionMap.put("type", getNodeAttr("type", resource));
			connectionMap.put("url", getNodeAttr("url", resource));
			connectionMap.put("driver", getNodeAttr("driverClassName", resource));
			connectionMap.put("name", getNodeAttr("name", resource));
			connectionMap.put("username", getNodeAttr("username", resource));
			connectionMap.put("password", getNodeAttr("password", resource));
			connectionMap.put("removeAbandoned", getNodeAttr("removeAbandoned", resource));
			connectionMap.put("removeAbandonedTimeout", getNodeAttr("removeAbandonedTimeout", resource));
			connectionMap.put("logAbandoned", getNodeAttr("logAbandoned", resource));
			connectionMap.put("maxActive", getNodeAttr("maxActive", resource));
			connectionMap.put("maxIdle", getNodeAttr("maxIdle", resource));
			connectionMap.put("minIdle", getNodeAttr("minIdle", resource));
			connectionMap.put("maxWait", getNodeAttr("maxWait", resource));

			if(connectionMap.containsKey("driver") && connectionMap.containsKey("type") &&
					connectionMap.containsKey("name") && connectionMap.containsKey("url"))
			{
				return connectionMap;
			}
		}

		return null;
	}

	
	/**
	 * Gets attribute from given node.
	 * 
	 * @param attrName name of the attribute.
	 * @param node node to read attribute from.
	 * @return node value or empty string.
	 */
	protected static String getNodeAttr(String attrName, Node node)
	{
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			NamedNodeMap attrs = node.getAttributes();

			for (int y = 0; y < attrs.getLength(); y++)
			{
				Node attr = attrs.item(y);

				if (attr.getNodeName().equalsIgnoreCase(attrName))
				{
					return attr.getNodeValue();
				}
			}
		}

		return "";
	}

	/**
	 * Gets attribute from given node list.
	 * 
	 * @param tagName name of the tag.
	 * @param attrName attribute name.
	 * @param nodes list of nodes.
	 * @return node value or empty string.
	 */
	protected static String getNodeAttr(String tagName, String attrName, NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			Node node = nodes.item(x);

			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				NodeList childNodes = node.getChildNodes();

				for (int y = 0; y < childNodes.getLength(); y++)
				{
					Node data = childNodes.item(y);

					if (data.getNodeType() == Node.ATTRIBUTE_NODE)
					{
						if (data.getNodeName().equalsIgnoreCase(attrName))
							return data.getNodeValue();
					}
				}
			}
		}

		return "";
	}

	/**
	 * Returns node from given node list.
	 * 
	 * @param tagName tag name of the node thats needed.
	 * @param nodes list of nodes where this node is placed
	 * @return Node or null.
	 */
	protected static Node getNode(String tagName, NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				return node;
			}
		}

		return null;
	}

	/**
	 * Returns node text value from given node.
	 * 
	 * @param node node that contains text, e.g. &lt;name&gt;Cousin
	 *            Vinny&lt;/name&gt;
	 * @return Cousin Vinny
	 */
	protected static String getNodeValue(Node node)
	{
		NodeList childNodes = node.getChildNodes();
		for (int x = 0; x < childNodes.getLength(); x++)
		{
			Node data = childNodes.item(x);
			if (data.getNodeType() == Node.TEXT_NODE)
				return data.getNodeValue();
		}
		return "";
	}

	/**
	 * Returns text node value.
	 * 
	 * @param tagName tag name of the node.
	 * @param nodes list of nodes.
	 * @return text node value or empty string.
	 */
	protected static String getNodeValue(String tagName, NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++)
				{
					Node data = childNodes.item(y);
					if (data.getNodeType() == Node.TEXT_NODE)
						return data.getNodeValue();
				}
			}
		}
		return "";
	}

	/**
	 * Builds and activates JNDI datasource namespace using catalina's url
	 * context factory.
	 * @deprecated Use {@link #bindJNDIDataSourceViaSpringContextBuilder()}
	 */
	protected static void bindJNDIDataSource()
	{
		try
		{
			System.out.println("GetinsuredSpringTest.setup() Binding JNDI DataSource");
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");

			ic = new InitialContext();
			ic.createSubcontext("jdbc");
			ic.createSubcontext("jdbc/ghixDS");

			OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
			ds.setURL(getDbConnectUrl());
			ds.setUser(getDbUsername());
			ds.setPassword(getDbPassword());

			//ic.bind(DB_JNDI_NAME, ds);
			ic.rebind(getDbJndiName(), ds);
			log.info("bindJNDIDataSource() Finished bying JNDI data source: " + ic.getNameInNamespace());
		}
		catch (NamingException ne)
		{
			ne.printStackTrace();
			log.error("NamingException occurred while registring JNDI jdbc/ghixDS", ne);
		}
		catch (SQLException sex)
		{
			sex.printStackTrace();
			log.error("SQLException occurred while creating OracleConnectionPoolDataSource()", sex);
		}
	}

	public Logger getLog()
	{
		return log;
	}

	/**
	 * Gets initial context.
	 * 
	 * @return InitialContext
	 */
	public InitialContext getInitialContext()
	{
		return ic;
	}

}
