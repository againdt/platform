package com.getinsured.hix.platform.lookup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.LookupValue;
/**
 * @author ajinkya m
 * 
 */

public interface ILookupRepository extends JpaRepository<LookupValue, Integer> {

	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name and UPPER(lookupValue.isobsolete) = 'N' AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH'")
	List<LookupValue> getLookupValueList(@Param("name")String name);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name and UPPER(lookupValue.isobsolete) = 'N'  AND lookupValue.lookupLocale.localeLanguage = :language ")
	List<LookupValue> getLookupValueList(@Param("name")String name,@Param("language")String language);

	@Query("SELECT lookupValue FROM LookupValue lookupValue WHERE lookupValue.lookupType.name='ENTITY_COUNTIESSERVED' " +
			" and UPPER (lookupValue.lookupValueLabel) LIKE '%'|| UPPER (:name) ||'%'  AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	List<LookupValue> getLookupValueListForCountiesServed(@Param("name")String name);
	
	@Query("SELECT lookupValue FROM LookupValue lookupValue WHERE lookupValue.lookupType.name='ENTITY_COUNTIESSERVED' " +
			" and UPPER (lookupValue.lookupValueLabel) LIKE '%'|| UPPER (:name) ||'%'  AND lookupValue.lookupLocale.localeLanguage = :language ")
	List<LookupValue> getLookupValueListForCountiesServed(@Param("name")String name, @Param("language")String language);

	@Query("SELECT lookupValue FROM LookupValue lookupValue WHERE lookupValue.lookupType.name='LANGUAGE' " +
			" and UPPER (lookupValue.lookupValueLabel) LIKE '%'|| UPPER (:term) ||'%' " +
			" and UPPER(lookupValue.isobsolete) = 'N'  AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ORDER BY lookupValue.lookupValueLabel " )
	List<LookupValue> getLookupValueListForLanguages(@Param("term") String query);
	
	@Query("SELECT lookupValue FROM LookupValue lookupValue WHERE lookupValue.lookupType.name='LANGUAGE' " +
			" and UPPER (lookupValue.lookupValueLabel) LIKE '%'|| UPPER (:term) ||'%' " +
			" and UPPER(lookupValue.isobsolete) = 'N' AND lookupValue.lookupLocale.localeLanguage = :language ORDER BY lookupValue.lookupValueLabel " )
	List<LookupValue> getLookupValueListForLanguages(@Param("term") String query, @Param("language")String language);

	@Query(" SELECT lookupValue.lookupValueLabel "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name "+
			" AND lookupValue.lookupValueCode = :lookupValueCode " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	String getLookupValueLabelByNameAndLookupValueCode(@Param("name") String name,@Param("lookupValueCode") String lookupValueCode);
	
	@Query(" SELECT lookupValue.lookupValueLabel "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name "+
			" AND lookupValue.lookupValueCode = :lookupValueCode " +
			" AND lookupValue.lookupLocale.localeLanguage = :language ")
	String getLookupValueLabelByNameAndLookupValueCode(@Param("name") String name,@Param("lookupValueCode") String lookupValueCode, @Param("language")String language);

	@Query(" SELECT lookupValue.lookupValueCode "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name " +
			//" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' "+
			" AND lookupValue.lookupValueLabel = :lookupValueLabel")
	String getLookupValueCodeByNameAndLookupValueLabel(@Param("name") String name,@Param("lookupValueLabel") String lookupValueLabel);

	@Query(" SELECT lookupValue.lookupValueId "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name "+
			" AND lookupValue.lookupValueCode = :lookupValueCode " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	Integer getlookupValueIdByTypeANDLookupValueCode(@Param("name") String name,@Param("lookupValueCode") String lookupValueCode);
	
	@Query(" SELECT lookupValue.lookupValueId "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name "+
			" AND lookupValue.lookupValueCode = :lookupValueCode " +
			" AND lookupValue.lookupLocale.localeLanguage = :language ")
	Integer getlookupValueIdByTypeANDLookupValueCode(@Param("name") String name,@Param("lookupValueCode") String lookupValueCode, @Param("language")String language);

	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.parentLookupValueId = :parentLookupValueId " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	List<LookupValue> getDependentLookupValueList(@Param("parentLookupValueId") String parentLookupValueId);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.parentLookupValueId = :parentLookupValueId " +
			" AND lookupValue.lookupLocale.localeLanguage = :language ")
	List<LookupValue> getDependentLookupValueList(@Param("parentLookupValueId") String parentLookupValueId, @Param("language")String language);

	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :type "+
			" AND lookupValue.lookupValueCode = :lookupValueCode " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	LookupValue getlookupValueByTypeANDLookupValueCode(@Param("type") String type,@Param("lookupValueCode") String lookupValueCode);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :type "+
			" AND lookupValue.lookupValueCode = :lookupValueCode " +
			" AND lookupValue.lookupLocale.localeLanguage = :language")
	LookupValue getlookupValueByTypeANDLookupValueCode(@Param("type") String type,@Param("lookupValueCode") String lookupValueCode, @Param("language")String language);

	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :type "+
			" AND LOWER(lookupValue.lookupValueLabel) = :lookupValueLabel " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	LookupValue getlookupValueByTypeANDLookupValueLabel(@Param("type") String type,@Param("lookupValueLabel") String lookupValueLabel);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :type "+
			" AND LOWER(lookupValue.lookupValueLabel) = :lookupValueLabel " +
			" AND lookupValue.lookupLocale.localeLanguage = :language ")
	LookupValue getlookupValueByTypeANDLookupValueLabel(@Param("type") String type,@Param("lookupValueLabel") String lookupValueLabel, @Param("language")String language);
	
/*	@Query(" SELECT LookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' " +
			" order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForAeeStatus(@Param("name")String name);
	
	@Query(" SELECT LookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name " +
			" AND lookupValue.lookupLocale.localeLanguage = :language " +
			" order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForAeeStatus(@Param("name")String name, @Param("language")String language);*/
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForEligibiltyStatus(@Param("name")String name);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name " +
			" AND lookupValue.lookupLocale.localeLanguage = :language order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForEligibiltyStatus(@Param("name")String name, @Param("language")String language);

	@Query("SELECT lookupValue FROM LookupValue lookupValue " +
			" WHERE lookupValue.lookupType.name=:name " +
			" and UPPER(lookupValue.isobsolete) = 'N' AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' " +
			" order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForHoursOfOperation(@Param("name") String name);
	
	@Query("SELECT lookupValue FROM LookupValue lookupValue " +
			" WHERE lookupValue.lookupType.name=:name " +
			" and UPPER(lookupValue.isobsolete) = 'N' AND lookupValue.lookupLocale.localeLanguage = :language " +
			" order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForHoursOfOperation(@Param("name") String name, @Param("language")String language);

	@Query(" SELECT lookupValue FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name AND lookupValue.lookupValueLabel != 'eCommitted' " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' " +
			" order by lookupValue.lookupValueId ")
	List<LookupValue> getLookupValueListForEnrollmentStatusEdit(@Param("name") String name);
	
	@Query(" SELECT lookupValue FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = :name AND lookupValue.lookupValueLabel != 'eCommitted' " +
			" AND lookupValue.lookupLocale.localeLanguage = :language " +
			" order by lookupValue.lookupValueId")
	List<LookupValue> getLookupValueListForEnrollmentStatusEdit(@Param("name") String name,@Param("language")String language);
	
	@Query("SELECT lookupValue.lookupValueLabel FROM LookupValue lookupValue " +
			" WHERE lookupValue.lookupType.name = UPPER (:name) " +
			" AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH' ")
	List<String> getLookupValueLabelList(@Param("name") String name);
	
	@Query("SELECT lookupValue.lookupValueLabel FROM LookupValue lookupValue " +
			" WHERE lookupValue.lookupType.name = UPPER (:name) " +
			" AND lookupValue.lookupLocale.localeLanguage = :language ")
	List<String> getLookupValueLabelList(@Param("name") String name,@Param("language")String language);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.lookupType.name = UPPER (:name) " +
			" AND lookupValue.lookupValueCode in (:lookupValueCode)")
	List<LookupValue> getLookupValueListForBOBFeed(@Param("name")String name, @Param("lookupValueCode") List<String> lookupValueCode);
	
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE UPPER(lookupValue.lookupType.name) LIKE UPPER (:name) ||'%' and UPPER(lookupValue.isobsolete) = 'N' AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH'")
	List<LookupValue> getLookupValueListForAppEvents(@Param("name")String name);
	
	/**
	 * Jira Id: HIX-79052
	 * @since 03rd November 2015
	 * @param lookupValueCode String
	 * @param parentLookupCode String
	 * @param parentLookupTypeName String
	 * @return Object<LookupValue>
	 */
	@Query(" SELECT lookupValue "+
			" FROM LookupValue lookupValue "+
			" WHERE lookupValue.parentLookupValueId = (SELECT innerLookup.lookupValueId FROM LookupValue innerLookup WHERE innerLookup.lookupType.name = :parentLookupTypeName AND innerLookup.lookupValueCode IN (:parentLookupCode) AND innerLookup.lookupLocale.localeLanguage = 'ENGLISH') " +
			" AND lookupValue.lookupValueCode IN (:lookupValueCode) AND lookupValue.lookupLocale.localeLanguage = 'ENGLISH'")
	LookupValue getlookupValueBylookupCodeAndParentLookupCode(@Param("lookupValueCode")String lookupValueCode, @Param("parentLookupCode")String parentLookupCode, @Param("parentLookupTypeName")String parentLookupTypeName);

	LookupValue findByLookupValueId(int id);
	
}