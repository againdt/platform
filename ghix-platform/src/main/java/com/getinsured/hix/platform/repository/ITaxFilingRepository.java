package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.getinsured.hix.model.TaxYear;

public interface ITaxFilingRepository extends JpaRepository<TaxYear, Integer> {
	
	public List<TaxYear> findByYearOfEffectiveDate(String yearOfEffectiveDate);
	
}
