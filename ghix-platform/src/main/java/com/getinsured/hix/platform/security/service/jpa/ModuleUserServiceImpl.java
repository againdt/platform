package com.getinsured.hix.platform.security.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.platform.security.repository.IModuleUserRepository;
import com.getinsured.hix.platform.security.service.ModuleUserService;

@Deprecated
class ModuleUserServiceImpl implements ModuleUserService {
	
	@Autowired	private IModuleUserRepository moduleUserRepository;	
	
	/*
	 *  NOTE :: DONOT UNCOMMENT THE FOLLOWING (all) methods - Venkata Tadepalli
	 *  	public ModuleUser saveModuleUser(ModuleUser moduleUser);
	 *  	public ModuleUser findModuleUserByUser(int userid, String moduleName);
	 *  	public List<ModuleUser> findByModuleIdModuleName(int moduleId,
	 *		String moduleName);
	 *		public void deleteModuleUser(ModuleUser moduleUser);
	 *		void deleteModuleUserByBrokerId(int brokerId, String moduleName);
	 *
	 *
	 *
	*/
	
	/*@Override	
	@Transactional
	public ModuleUser saveModuleUser(ModuleUser moduleUser){
		return moduleUserRepository.save(moduleUser);
	}
	
	@Override	
	@Transactional(readOnly=true)
	public ModuleUser findModuleUserByUser(int userid, String moduleName){
		return moduleUserRepository.findByUser(userid,moduleName);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<ModuleUser> findByModuleIdModuleName(int moduleId, String moduleName){
		return moduleUserRepository.findByModuleIdModuleName(moduleId, moduleName);
	}
	@Override	
	@Transactional
	 public void deleteModuleUser(ModuleUser moduleUser)
	{
		moduleUserRepository.delete(moduleUser);
		
	}
	
	@Override
	@Transactional
	public void deleteModuleUserByBrokerId(int brokerId, String moduleName) {
		moduleUserRepository.deleteModuleUserByBrokerId(brokerId, moduleName);
	}*/
}