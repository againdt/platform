/**
 * 
 */
package com.getinsured.hix.platform.comment.service;

import com.getinsured.hix.model.CommentTarget;

/**
 * @author panda_p
 *
 */
public interface CommentTargetService {
	
	CommentTarget saveCommentTarget(CommentTarget commentGroup);
	CommentTarget findByTargetIdAndTargetType(Long targetId, CommentTarget.TargetName targetName);
	
}
