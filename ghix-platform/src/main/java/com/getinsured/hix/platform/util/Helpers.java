package com.getinsured.hix.platform.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.getinsured.hix.platform.util.exception.ApplicationDataException;
import com.getinsured.hix.platform.util.exception.ConfigurationException;

public class Helpers {

	private static final Logger logger = Logger.getLogger(Helpers.class);

	public static String appendFormat(String fileIdentifier, String format) {
		return String.format("%s.%s", fileIdentifier, format);
	}

	//    public static String loadFile(String filePath, String encoding) throws ConfigurationException {
	//        StringBuilder sb = new StringBuilder();
	//        BufferedReader br = null;
	//        try {
	//            br = new BufferedReader(
	//                    new InputStreamReader(
	//                            new FileInputStream(filePath), encoding));
	//            while (br.ready()) {
	//                sb.append(br.readLine());
	//            }
	//        } catch (IOException e) {
	//            throw new ConfigurationException(e);
	//        } finally {
	//            if (br != null) {
	//                try {
	//                    br.close();
	//                } catch (IOException e) {
	//                    logger.debug(e.getMessage(), e);
	//                }
	//            }
	//        }
	//        return sb.toString().trim();
	//    }

	//	public static JsonNode getJsonNodeFormConfig(String fileIdentifier) throws ConfigurationException {
	//		try {
	//			return GlobalConfig.getInstance().getData(fileIdentifier);
	//		} catch (FileNotFoundException | RuntimeException | ExceptionInInitializerError e) {
	//			throw new ConfigurationException(String.format("Issue loading configuration file {%s}", fileIdentifier), e);
	//		}
	//	}

	public static JsonNode getFormString(String jsonConfigStr) throws ConfigurationException {
		try {
			JsonNode configNode = null;
			ByteArrayInputStream bis = new ByteArrayInputStream(jsonConfigStr.getBytes());
			ObjectMapper mapper = new ObjectMapper();
			try {
				configNode = mapper.readTree(bis);
			} catch (IOException e) {
				throw new ConfigurationException(e);
			} finally {
				try {
					bis.close();
				} catch (IOException e) {
					logger.debug(e.getMessage(), e);
				}
			}
			return configNode;
		} catch (RuntimeException | ExceptionInInitializerError e) {
			throw new ConfigurationException("Issue loading configuration");
		}
	}

	/**
	 * 
	 * @param fileIdentifier
	 * @return
	 * @throws ConfigurationException
	 */

	//	public static JsonNode getConfigFromClasspath(String fileIdentifier) throws ConfigurationException {
	//		try {
	//			return GlobalConfig.getInstance().getResourceData(fileIdentifier);
	//		} catch (FileNotFoundException | RuntimeException | ExceptionInInitializerError e) {
	//			throw new ConfigurationException(String.format("Issue loading configuration file {%s}", fileIdentifier), e);
	//		}
	//	}

	/**
	 * Reads json from filesystem. 
	 * This code will only be used for initial bootstrapping. 
	 * @return
	 * @throws ConfigurationException 
	 */
	//	public static JsonNode readApplicationJsonFromFile(String fileName) throws ConfigurationException{
	//		return getConfigFromClasspath(fileName);
	//	}

	public static JsonNode prepareApplicationJson(String responseObjectJson, String paymentInfo) throws ApplicationDataException {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode application;
		try {
			JsonNode responseObject = objectMapper.readTree(responseObjectJson);
			application = (ObjectNode) objectMapper.readTree(responseObject.get("uiData").asText());
			ObjectNode header = JsonNodeFactory.instance.objectNode();
			header.put("appstate", responseObject.get("state"));
			header.put("appversion", responseObject.get("year"));
			application.put("header", header);
			application.put("documents", responseObject.get("documents"));
			try {
				String issuerApplicationId = responseObject.get("issuerApplicationId").asText();
				if (!issuerApplicationId.equals("null")) {
					application.put("issuerApplicationId", issuerApplicationId);
				}
			} catch (NullPointerException e) {
				logger.debug("No 'issuerApplicationId' key found in incoming json application request");
			}
			try {
				JsonNode appYear = responseObject.get("year");
				application.put("year", appYear);
			} catch (NullPointerException e) {
				logger.debug("No 'family.appYear' key found in incoming json application request");
				application.put("year", "2014");
			}
			ObjectNode payment = JsonNodeFactory.instance.objectNode();
			try {
				String paymentString = application.get("payment").toString();
				if (!paymentString.equals("null") && !paymentString.isEmpty()) {
					payment.putAll((ObjectNode) objectMapper.readTree(paymentString));
				}
			} catch (NullPointerException e) {
				logger.debug("No 'payment' key found in incoming json application request");
			}
			try {
				String familyPaymentString = application.get("family").get("payment").toString();
				if (!familyPaymentString.equals("null") && !familyPaymentString.isEmpty()) {
					payment.putAll((ObjectNode) objectMapper.readTree(familyPaymentString));
				}
			} catch (NullPointerException e) {
				logger.debug("No 'family.payment' key found in incoming json application request");
			}
			if (paymentInfo != null) {
				try {
					ObjectNode paymentInfoNode = (ObjectNode) objectMapper.readTree(paymentInfo);
					payment.putAll(paymentInfoNode);
				} catch (Exception e) {
					logger.debug("The payment info Json is not valid");
				}
			}
			application.set("payment", payment);
		} catch (IOException e) {
			throw new ApplicationDataException(e);
		}
		logger.debug(application.toString());
		return application;
	}
}
