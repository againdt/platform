package com.getinsured.hix.platform.events;

public enum PartnerEventTypeEnum {
	
	ADD_TO_CART, 
	REMOVE_FROM_CART,
	ENROLL

}
