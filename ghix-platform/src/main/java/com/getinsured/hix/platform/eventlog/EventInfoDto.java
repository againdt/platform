package com.getinsured.hix.platform.eventlog;

import com.getinsured.hix.model.LookupValue;

public class EventInfoDto {
//	private LookupValue eventLookupValue;
	private Integer moduleId;
	private String moduleName;
	private String moduleUserName;
	
	//internally used
	private Integer eventLookupId;
	private String eventName;
	private String eventType;
	private String timeStamp;
	
	private String comments;

//	public LookupValue getEventLookupValue() {
//		return eventLookupValue;
//	}

	public void setEventLookupValue(LookupValue eventLkp) {
		if(eventLkp != null) {			
			this.setEventLookupId(eventLkp.getLookupValueId());
			this.setEventName(eventLkp.getLookupValueLabel());
			this.setEventType(eventLkp.getLookupType().getDescription());
		}
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleUserName() {
		return moduleUserName;
	}

	public void setModuleUserName(String moduleUserName) {
		this.moduleUserName = moduleUserName;
	}
	
	public String getEventName() {
		return eventName;
	}
	
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public String getEventType() {
		return eventType;
	}
	
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public Integer getEventLookupId() {
		return eventLookupId;
	}
	
	public void setEventLookupId(Integer eventLookupId) {
		this.eventLookupId = eventLookupId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String object) {
		timeStamp = object;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
}
