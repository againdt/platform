package com.getinsured.hix.platform.security;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.security.repository.CustomLdapUserRepository;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.exception.GIException;

@Service
@Transactional(readOnly = true)
public class CustomLdapUserDetailsService implements
		AuthenticationUserDetailsService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CustomLdapUserDetailsService.class);

	private CustomLdapUserRepository customLdapUserRepository;

	@Autowired
	private UserService userService;
	@Autowired
	private  UserRoleService userRoleService;
	@Autowired
	private RoleService roleService;
	
	public CustomLdapUserRepository getCustomLdapUserRepository() {
		return customLdapUserRepository;
	}

	public void setCustomLdapUserRepository(
			CustomLdapUserRepository customLdapUserRepository) {
		this.customLdapUserRepository = customLdapUserRepository;
	}

	
	@Override
	@Transactional
	@ExceptionHandler(UsernameNotFoundException.class)
	public UserDetails loadUserDetails(Authentication token)
			throws UsernameNotFoundException {
		AccountUser ldapUser = null;
		String username = token.getName();
		if(username.equalsIgnoreCase("Anonymous")){
			ldapUser = new AccountUser();
			ldapUser.setUserName("anonymous");
			ldapUser.setAuthenticated(true);
			ldapUser.setConfirmed(1);
			LOGGER.info("Received username as Anonymous");
			return ldapUser;
		}
		ldapUser = customLdapUserRepository.findLdapUser(username.toLowerCase());
		String ldapUserRoleName = customLdapUserRepository.searchRole(username);
		
		String defUserRoleName = customLdapUserRepository.getGhixRoleName(ldapUserRoleName);
		
		if (ldapUser == null) {
			LOGGER.error("LDAP Username Not Found :: " + username);
			throw new UsernameNotFoundException(username);
		}
		//LOGGER.debug("LDAP User name ::" + ldapUser.getUserName()+", LDAP Role ::"+ldapUserRoleName+", GHIX Role ::"+defUserRoleName);
		LOGGER.info("LDAP User name :: " + ldapUser.getUserName()+ ", LDAP Role ::"+ldapUserRoleName+", GHIX Role ::"+defUserRoleName);
		/*
		 * Check if the user had already provisioned in the GHIX domain
		 * if not then set necessary data into ldapUser and provision the user
		 * in UserController->loginSuccess method.
		 * 
		 */
		AccountUser ghixUser = this.userService.findByUserName(username.toLowerCase());
		if (ghixUser == null) {
			LOGGER.info("Ldap User "+ ldapUser.getUserName()+"does ,not exist in GHIX");
			if (!StringUtils.equals(defUserRoleName, "anonymous")) {
				
				setNewProfileDefaults(ldapUser,defUserRoleName);
				try {
					ldapUser=this.userService.createUser(ldapUser, defUserRoleName);
					ldapUser.setHasGhixProvisioned(true);
				} catch (GIException e) {
					LOGGER.error("Error creating the user record ",e);
					throw new UsernameNotFoundException("Error creating user frm LDAP data",e);
				}
			}
		} else{
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Ldap User "+ ldapUser.getUserName()+" exists in GHIX");
			}
			Role role = this.userService.getDefaultRole(ghixUser);
			String roleName = role.getName();
			if( role != null && !roleName.equalsIgnoreCase("individual".intern()) && !roleName.equalsIgnoreCase(defUserRoleName)) {
				this.userRoleService.changeDefaultUserRole(ghixUser, defUserRoleName, false);
			}
			ghixUser.setRecordId(ldapUser.getRecordId()); // Required only for Accenture IND-65
			ghixUser.setRecordType(ldapUser.getRecordType());// Required only for Accenture IND-65
			ghixUser.setExternPin(ldapUser.getExternPin());  // Refer: JIRA : HIX-9827 ;Expose PIN in the Account User object
			ldapUser = ghixUser;
		}
		return ldapUser;
	}

	/**
	 * Sets the default profile values for the user object required for
	 * registration.
	 * 
	 * @param accountUser
	 *            AccountUser object
	 * @return void
	 */

	private void setNewProfileDefaults(AccountUser accountUser, String defUserRoleName) {
		String defPassword = GhixEncryptorUtil.generateUUIDHexString(12);
		String defSecQuestion1 = "What was your childhood nickname?";
		String defSecQuestion2 = "What school did you attend for sixth grade?";
		//String defSecQuestion3 = "In what city does your nearest sibling live?";

		String defSecAnswer1 = "Defautl Security Answer 1";
		String defSecAnswer2 = "Defautl Security Answer 2";
		//String defSecAnswer3 = "Defautl Security Answer 3";
		accountUser.setUserName(accountUser.getUserName().toLowerCase());
		accountUser.setPassword(defPassword);
		accountUser.setSecurityQuestion1(defSecQuestion1);
		accountUser.setSecurityAnswer1(defSecAnswer1);
		accountUser.setHasGhixProvisioned(false);

		accountUser.setSecurityQuestion2(defSecQuestion2);
		accountUser.setSecurityAnswer2(defSecAnswer2);

		//accountUser.setSecurityQuestion3(defSecQuestion3);
		//accountUser.setSecurityAnswer3(defSecAnswer3);
		
		//accountUser.setHasGhixProvisioned(false);  // indicates that the user needs to provision in GHIX DB
		accountUser.setGhixProvisionRoleName(defUserRoleName.toUpperCase()); // new users default role
		accountUser.setConfirmed(1); // enables the user account
	}
	
	
}
