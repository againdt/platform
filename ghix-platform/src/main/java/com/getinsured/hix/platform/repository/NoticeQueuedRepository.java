package com.getinsured.hix.platform.repository;

import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.model.NoticeType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface NoticeQueuedRepository extends JpaRepository<NoticeQueued, Integer> {
    @Query("SELECT COUNT(*) FROM NoticeQueued as noticeQueued WHERE noticeQueued.status = :status AND noticeQueued.processingDate <= :processingDate")
    Integer getNoticeQueuedCount(@Param("status") NoticeQueued.QueuedStatus status, @Param("processingDate") Date processingDate);

    @Query("FROM NoticeQueued as noticeQueued WHERE noticeQueued.status = :status AND noticeQueued.processingDate <= :processingDate")
    List<NoticeQueued> findNoticeQueuedByStatusAndDate(@Param("status") NoticeQueued.QueuedStatus status, @Param("processingDate") Date processingDate, Pageable pageable);

    @Query("FROM NoticeQueued as noticeQueued WHERE noticeQueued.noticeType = :noticeType AND noticeQueued.tableName = :tableName AND noticeQueued.columnName = :columnName AND noticeQueued.columnValue = :columnValue AND noticeQueued.processingDate = :processingDate AND noticeQueued.status = :status")
    NoticeQueued findByUniqueConstraint(@Param("noticeType") NoticeType noticeType, @Param("tableName") NoticeQueued.TableNames tableName, @Param("columnName") String columnName, @Param("columnValue") Long columnValue, @Param("processingDate") Date processingDate, @Param("status") NoticeQueued.QueuedStatus status);
}
