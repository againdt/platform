package com.getinsured.hix.platform.payments.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Location;

public interface ILocRepository extends JpaRepository<Location, Integer> {
	
}
