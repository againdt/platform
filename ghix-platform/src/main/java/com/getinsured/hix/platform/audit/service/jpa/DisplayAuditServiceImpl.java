package com.getinsured.hix.platform.audit.service.jpa;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.platform.audit.service.DisplayAuditService;



/**
 * 
 * @author EkramAli Kazi
 * Implementation class for Display Audit Service
 *
 */
@Service("displayAuditService")
@Scope("prototype")
public class DisplayAuditServiceImpl implements DisplayAuditService{

	private RevisionRepository<?, Integer, Integer> repositoryObject;
	private Map<String, String> requiredFieldsMap;
	private ApplicationContext applicationContext;

	private static final class DomainClazz{
		private static Class<?> name;
		private static List<Field> fields;
		private static List<String> fieldNames;

		private DomainClazz() {}
	}

	private static final Logger LOGGER = Logger.getLogger(DisplayAuditServiceImpl.class);
	
	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	@Transactional(readOnly = true)
	@Override
	public List<Map<String, String>> findRevisions(String repositoryName, String domainName, Map<String, String> requiredFieldsMap, Integer entityId) {

		if (entityId == null){
			throw new IllegalArgumentException("entityId cannot be null.");
		}

		preProcess(repositoryName, domainName, requiredFieldsMap);

		Revisions<Integer, ?> revisions = repositoryObject.findRevisions(entityId);

		List<?> revisionData = revisions.getContent();
		List<Object> list = new ArrayList<Object>();
		for (Object object : revisionData) {
			@SuppressWarnings("unchecked")
			Object obj = ((Revision<Integer, ?>) object).getEntity();
			list.add(obj);
		}

		return postProcess(list);
	}

	@Override
	public <T> List<Map<String, String>> findRevisions(
			RevisionRepository<T, Integer, Integer> repository,
			String domainName, Map<String, String> requiredFieldsMap,
			Integer entityId) {
		if (entityId == null){
			throw new IllegalArgumentException("entityId cannot be null.");
		}

		preProcess(repository, domainName, requiredFieldsMap);

		Revisions<Integer, ?> revisions = repositoryObject.findRevisions(entityId);

		List<?> revisionData = revisions.getContent();
		List<Object> list = new ArrayList<Object>();
		for (Object object : revisionData) {
			@SuppressWarnings("unchecked")
			Object obj = ((Revision<Integer, ?>) object).getEntity();
			list.add(obj);
		}

		return postProcess(list);
	}

	private <T> void  preProcess(RevisionRepository<T, Integer, Integer> repository, String domainName, Map<String, String> requiredFieldsMap) {
		assertNotNull(repository);
		assertNotNull(domainName);
		assertNotNull(requiredFieldsMap);

		setRepository(repository);
		setDomain(domainName);
		setRequiredFieldsMap(requiredFieldsMap);

	}
	
	private void preProcess(String repositoryName, String domainName, Map<String, String> requiredFieldsMap) {
		assertNotNull(repositoryName);
		assertNotNull(domainName);
		assertNotNull(requiredFieldsMap);

		setRepository(repositoryName);
		setDomain(domainName);
		setRequiredFieldsMap(requiredFieldsMap);

	}

	private void setRequiredFieldsMap(Map<String, String> requiredFieldsMap) {
		//verify fields are present in the fields list
		for (@SuppressWarnings("rawtypes") Map.Entry entry : requiredFieldsMap.entrySet()) {
			assertTrue(DomainClazz.fieldNames.contains(entry.getKey()));
		}

		this.requiredFieldsMap = requiredFieldsMap;
	}

	private void setDomain(String domainName) {
		try {
			DomainClazz.name = Class.forName(domainName);
		} catch (ClassNotFoundException e) {
			assertFalse("Class Not Found - " + domainName, false);
		}

		Field[]  fields = DomainClazz.name.getDeclaredFields();

		DomainClazz.fields = Arrays.asList(fields);

		List<String> fieldNames = new ArrayList<String>(DomainClazz.fields.size());
		for (Field field : DomainClazz.fields) {
			fieldNames.add(field.getName());
		}

		DomainClazz.fieldNames = fieldNames;
	}

	@SuppressWarnings("unchecked")
	private void setRepository(String repositoryName) {
		repositoryObject = (RevisionRepository<?, Integer, Integer>) this.applicationContext.getBean(repositoryName);
	}

	private void setRepository(RevisionRepository<?, Integer, Integer> repository) {
		repositoryObject = repository;
	}


	private List<Map<String, String>> postProcess(List<Object> list) {

		if (list.size() == 0){
			return emptyList();
		}

		List<Map<String, String>> resultList = new LinkedList<Map<String,String>>();
		Map<String, String> result;

		for(Object object : list){
			result =  new LinkedHashMap<String, String>();
			for (@SuppressWarnings("rawtypes") Map.Entry entry : requiredFieldsMap.entrySet()) {
				int index = DomainClazz.fieldNames.indexOf(entry.getKey());
				if(index > 0){
					Field field = DomainClazz.fields.get(index);
					field.setAccessible(true);
					Object value = null;

					try {
						value = field.get(object);
					} catch (Exception e) {
						LOGGER.error("Error reading field - " + e);
						throw new IllegalStateException("Error reading field - ", e);
					}
					value = value != null ? value.toString() : StringUtils.EMPTY;
					result.put(field.getName(), value.toString());
				}
			}
			resultList.add(result);
		}
		return unmodifiableList(resultList);
	}
	@Transactional(readOnly = true)
	@Override
	public List<Map<String, Object>> findAudRevisions(String repositoryName, String domainName, Map<String, String> requiredFieldsMap, Integer entityId) {

		if (entityId == null){
			throw new IllegalArgumentException("entityId cannot be null.");
		}

		preProcess(repositoryName, domainName, requiredFieldsMap);

		Revisions<Integer, ?> revisions = repositoryObject.findRevisions(entityId);

		List<?> revisionData = revisions.getContent();
		List<Object> list = new ArrayList<Object>();
		for (Object object : revisionData) {
			@SuppressWarnings("unchecked")
			Object obj = ((Revision<Integer, ?>) object).getEntity();
			list.add(obj);
		}

		return postAudProcess(list);
	}


	private List<Map<String, Object>> postAudProcess(List<Object> list) {

		if (list.size() == 0){
			return emptyList();
		}

		List<Map<String, Object>> resultList = new LinkedList<Map<String,Object>>();
		Map<String, Object> result;

		for(Object object : list){
			result =  new LinkedHashMap<String, Object>();
			for (@SuppressWarnings("rawtypes") Map.Entry entry : requiredFieldsMap.entrySet()) {
				int index = DomainClazz.fieldNames.indexOf(entry.getKey());
				if(index > 0){
					Field field = DomainClazz.fields.get(index);
					field.setAccessible(true);
					Object value = null;

					try {
						value = field.get(object);
					} catch (Exception e) {
						LOGGER.error("Error reading field - " + e);
						throw new IllegalStateException("Error reading field - ", e);
					}
					value = value != null ? value : null;
					result.put(field.getName(), value);
				}
			}
			resultList.add(result);
		}
		return unmodifiableList(resultList);
	}
}
