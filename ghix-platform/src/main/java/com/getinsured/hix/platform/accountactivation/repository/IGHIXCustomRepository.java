package com.getinsured.hix.platform.accountactivation.repository;

import java.io.Serializable;

import com.getinsured.hix.model.AccountUser;

/**
 * Custom Repository Interface to directly autowire in generic classes for Creator-Created flow.
 * This interface needs to be implemented by the Created side class.
 * @author Ekram Ali Kazi
 *
 * @param <T> -- Entity
 * @param <ID> -- Primary Key type
 */
public interface IGHIXCustomRepository<T, ID extends Serializable> {

	/**
	 * To check whether record exists for the supplied primaryKey for type <T>
	 * This needs to be overridden in corresponding Repository which implements IGHIXCustomRepository.
	 * Please example refer {@link EmployeeRepository}
	 * 
	 * @param primaryKey
	 * @return true if exists else false
	 */
	boolean recordExists(ID primaryKey);

	/**
	 * updates userId in type <T>
	 * This needs to be overridden in corresponding Repository which implements IGHIXCustomRepository
	 * Please example refer {@link EmployeeRepository}
	 * 
	 * @param accountUser
	 * @param primaryKey
	 */
	void updateUser(AccountUser accountUser, ID primaryKey);

}
