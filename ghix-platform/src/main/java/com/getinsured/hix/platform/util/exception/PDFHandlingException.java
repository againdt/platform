package com.getinsured.hix.platform.util.exception;

/**
 * Created by ivanbastidas on 3/20/14.
 */
public class PDFHandlingException extends Exception {

    public PDFHandlingException(Throwable cause) {
        super(String.format("Error while accessing data from a document: %s", cause.getMessage()), cause);
    }

    public PDFHandlingException(String message, Throwable cause) {
        super(message, cause);
    }
}
