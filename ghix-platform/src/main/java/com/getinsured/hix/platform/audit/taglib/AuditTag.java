package com.getinsured.hix.platform.audit.taglib;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.platform.audit.service.DisplayAuditService;

/**
 * 
 * @author EkramAli Kazi
 * Audit class to Display Audit Information
 */
public class AuditTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AuditTag.class);

	private DisplayAuditService displayAuditService;
	private MessageSource messageSource;

	/**
	 * to capture header
	 *
	 */
	private enum HEADER{

		SIZE("{0}.size"),
		PROPERTYNAME("{0}.{1}.propertyname"), LABEL("{0}.{1}.label");

		private String key;

		HEADER(String k) { key = k; }
	}

	//Tag's arributes
	private String repoBeanName;
	private String className;
	private Map<String, String> requiredFieldsMap;
	private Integer entityId;

	public String getRepoBeanName() {
		return repoBeanName;
	}

	public void setRepoBeanName(String repoBeanName) {
		this.repoBeanName = repoBeanName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	/**
	 * Call back method invoked by taglib
	 * This methods does following:-
	 * 		1.	autowire dependencies
	 * 		2.  reads *_aud.properties file and form requiredFieldsMap
	 * 		3.  call DAO layer to fetch audit data
	 * 		4.  form result in tabular format
	 * 		5.  write output using JspWriter
	 * @throws RuntimeException
	 */
	@Override
	public int doStartTag() throws JspException {

		autowireDependencies();
		populateFieldsMap();

		List<Map<String, String>> auditDataList = fetchAuditData();
		String finalResult = formOutput(auditDataList);

		writeOutput(finalResult);

		return SKIP_BODY;
	}

	private void writeOutput(String finalResult) throws JspException {
		JspWriter out = pageContext.getOut();

		try {
			out.println(finalResult);
		} catch (Exception e) {
			throw new JspException("Error Processing JSP Output for audit - " + e);
		}
	}

	private String formOutput(List<Map<String, String>> auditDataList) {

		StringBuilder sb = new StringBuilder();
		sb.append("<table class='table table-border-none table-condensed'><thead><tr>");
		Set<String> keys = requiredFieldsMap.keySet();

		//form header...
		for (String key : keys) {
			String value = requiredFieldsMap.get(key);
			String cClass = value.replace(" ", ""); // creates a class for each column
			sb.append("<th class='" + cClass + "'>");
			sb.append(value);
			sb.append("</th>");
		}
		sb.append("</tr></thead><tbody>");
		//form value rows...
		for (Map<String, String> map : auditDataList) {
			sb.append("<tr>");
			for (@SuppressWarnings("rawtypes") Map.Entry entry : map.entrySet()) {
				sb.append("<td>");
				sb.append(entry.getValue());
				sb.append("</td>");
			}
			sb.append("</tr>");
		}
		sb.append("</tbody></table>");

		return sb.toString();
	}

	private List<Map<String, String>> fetchAuditData() {
		return displayAuditService.findRevisions(repoBeanName, className,requiredFieldsMap, entityId);
	}

	private void populateFieldsMap() {
		requiredFieldsMap = new LinkedHashMap<String, String>();

		String auditPropFile = StringUtils.lowerCase(StringUtils.substringAfterLast(className, ".")) + "_aud";
		Locale locale = LocaleContextHolder.getLocale();
		List<HeaderColumn> columns = buildHeader(auditPropFile, locale);

		for (HeaderColumn headerColumn : columns) {
			if (LOGGER.isDebugEnabled()){
				LOGGER.debug(headerColumn.getLabel() + " " + headerColumn.getPropertyName());
			}
			requiredFieldsMap.put(headerColumn.getPropertyName(), headerColumn.getLabel());
		}
	}

	private void autowireDependencies() {
		ApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext(pageContext.getRequest(), pageContext.getServletContext());
		displayAuditService = (DisplayAuditService) applicationContext.getBean("displayAuditService");
		displayAuditService.setApplicationContext(applicationContext);
		messageSource = (MessageSource) applicationContext.getBean("messageSource");
	}

	private List<HeaderColumn> buildHeader(String auditPropFile, Locale locale) {

		Integer headerColumnCount = getHeaderColumnCount(MessageFormat.format(HEADER.SIZE.key, auditPropFile), locale);
		List<HeaderColumn> headers = Collections.emptyList();

		if (headerColumnCount != null){
			headers = new LinkedList<HeaderColumn>();
			HeaderColumn column = null;
			for (int i = 0; i < headerColumnCount; i++) {

				String propertyName = readValue(MessageFormat.format(HEADER.PROPERTYNAME.key, auditPropFile, i), locale);
				String label = readValue(MessageFormat.format(HEADER.LABEL.key, auditPropFile, i), locale);

				column = new HeaderColumn(propertyName, label);
				headers.add(column);
			}
		}
		return headers;
	}

	private Integer getHeaderColumnCount(String key, Locale locale) {

		String size = readValue(key, locale);
		if(StringUtils.isEmpty(size) || !StringUtils.isNumeric(size)){
			LOGGER.error("Configuration Error - count is not defined in properties file.");
			throw new IllegalStateException("Configuration Error - count is not defined in properties file.");
		}

		return Integer.parseInt(size);
	}

	private String readValue(String key, Locale locale) {
		return messageSource.getMessage(key, null, locale);
	}

	/**
	 * 
	 * Static inner class to capture HeaderColumn.
	 *
	 */
	public static class HeaderColumn {

		private String propertyName;
		private String label;


		public HeaderColumn(String propertyName, String label) {
			this.propertyName = propertyName;
			this.label = label;
		}

		public String getPropertyName() {return propertyName;}
		public String getLabel() {return label;}

	}
}
