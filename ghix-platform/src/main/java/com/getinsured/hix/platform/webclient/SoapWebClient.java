package com.getinsured.hix.platform.webclient;

import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;

/**
 * 
 * Class to invoke AHBX SOAP Service.
 * 
 * @author Ekram Ali Kazi
 * @param <Response>
 *
 */
@Component
public class SoapWebClient<E> {
	
	
	/**
	 * 
	 * @param request
	 * @param wsdlUrl
	 * @param webServiceTemplate
	 * 
	 * @return response object
	 */
	@SuppressWarnings("unchecked")
	public <T>  E send(T request, String wsdlUrl, String endpointFunction, WebServiceTemplate webServiceTemplate) {
		webServiceTemplate.setDefaultUri(wsdlUrl);
		return (E) webServiceTemplate.marshalSendAndReceive(request);
	}

	/**
	 * WebServiceTemplate marshallSendAndReseive with WebServiceMessageCallback
	 * @since 23rd July 2015
	 * @param request <Object>
	 * @param wsdlUrl String
	 * @param endpointFunction String
	 * @param webServiceTemplate WebServiceTemplate
	 * @param webServiceMessageCallback WebServiceMessageCallback
	 * @return response <Object>
	 */
	@SuppressWarnings("unchecked")
	public <T> E send(T request, final String wsdlUrl, String endpointFunction,
			WebServiceTemplate webServiceTemplate,
			WebServiceMessageCallback webServiceMessageCallback) 
	{
		webServiceTemplate.setDefaultUri(wsdlUrl);
		return (E) webServiceTemplate.marshalSendAndReceive(request, webServiceMessageCallback); 
	}
}