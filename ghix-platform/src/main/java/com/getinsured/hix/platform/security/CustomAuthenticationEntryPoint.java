/**
 * 
 */
package com.getinsured.hix.platform.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
/**
 * @author tadepalli_v
 *
 */
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
	
	protected String loginPageUrl;
	private Logger logger = LoggerFactory.getLogger(CustomAuthenticationEntryPoint.class);
	
	
	 public String getLoginPageUrl() {
		return loginPageUrl;
	}



	public void setLoginPageUrl(String loginPageUrl) {
		this.loginPageUrl = loginPageUrl;
	}



	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
	            throws IOException, ServletException {
			if(loginPageUrl == null){
				if(logger.isInfoEnabled()){
					logger.info("No loginPage url available for request {}, referer {}",request.getRequestURI(),request.getHeader("Referer".intern()));
				}
				return;
			}
	        response.sendRedirect(loginPageUrl);
	    }

}
