package com.getinsured.hix.platform.dto.ecm;

import java.util.List;


public class AccessControlEntry {
	private String principalId;
	private List<String> permissionsList;
	private boolean isDirect;

	public AccessControlEntry() {
		super();
	}

	public AccessControlEntry(String principalId, List<String> permissionsList,	boolean isDirect) {
		super();
		this.principalId = principalId;
		this.permissionsList = permissionsList;
		this.isDirect = isDirect;
	}

	public String getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public List<String> getPermissionsList() {
		return permissionsList;
	}

	public void setPermissionsList(List<String> permissionsList) {
		this.permissionsList = permissionsList;
	}

	public boolean isDirect() {
		return isDirect;
	}

	public void setDirect(boolean isDirect) {
		this.isDirect = isDirect;
	}

	@Override
	public String toString() {
		return "AccessControlEntry [principalId=" + principalId
				+ ", permissionsList=" + permissionsList + ", isDirect="
				+ isDirect + "]";
	}

/*	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
}
