package com.getinsured.hix.platform.security.service.jpa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RoleProvisioningRule;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.security.RoleAccessControl;
import com.getinsured.hix.platform.security.repository.IRoleRepository;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.util.PlatformErrorCode;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

@Service("roleService")
@Repository
@Transactional
public class RoleServiceImpl implements RoleService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);
	@Autowired	private IRoleRepository roleRepository;	
	@Autowired private RoleAccessControl accessControl;
	
	
	@Override	
	@Transactional(readOnly=true)
	public Role findRoleByName(String roleName) {
		Role role=null;
		try {
			if(StringUtils.isNotBlank(roleName)){
				roleName=StringUtils.upperCase(roleName);
				
				role = roleRepository.findIdByName(roleName);	
			}else{
				LOGGER.warn("Invalid Role Name");
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH ROLE BY NAME: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		
		return role;
	}	

	@Override
	public 	List<Role> findRolesByName(List<String>  roleNames){
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findRolesByName(roleNames);
			rolesList = this.accessControl.filterRoleListForAddOrUpdate(rolesList);
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ROLES BY NAME: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return rolesList;
	}
	
	@Override
	public List<Role> findAll(){
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll();
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		
		return rolesList;
	}
	
	@Override
	public List<Role> findAllUserCanAdd(){
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll();
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		
		return accessControl.filterRoleListForCurrentUser(rolesList,RoleAccessControl.ADD_ROLE_PERMISSION);
	}
	
	@Override
	public List<Role> findAllUserCanUpdate(){
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll();
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		
		return accessControl.filterRoleListForCurrentUser(rolesList,RoleAccessControl.ADD_ROLE_PERMISSION);
	}
	
	@Override
	public List<Role> findAll(Sort sort) {
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll(sort); 
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL SORTED ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return rolesList;
	}
	
	@Override
	public List<Role> findAllUserCanUpdate(Sort sort) {
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll(sort); 
			rolesList = this.accessControl.filterRoleListForAddOrUpdate(rolesList);
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL SORTED ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterRoleListForCurrentUser(rolesList,RoleAccessControl.ADD_ROLE_PERMISSION);
	}
	
	@Override
	public List<Role> findAllUserCanAdd(Sort sort) {
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll(sort); 
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL SORTED ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return accessControl.filterRoleListForCurrentUser(rolesList,RoleAccessControl.ADD_ROLE_PERMISSION);
	}
	
	@Override
	public Role findById(int id){
		Role role = null;
		
		try {
			role = roleRepository.findById(id);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH ROLE BY ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		
		return role;
	}

	@Override
	public List<Role> findNonAdminUsers() {
		List<Role> rolesList = null;
		
		try {
			rolesList = roleRepository.findNonAdminUsers(); 
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL NON ADMIN USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return this.accessControl.filterRoleListForCurrentUser(rolesList, RoleAccessControl.ADD_ROLE_PERMISSION);
	}
	
	@Override
	public List<Role> findAdminRolesForUsers() {
		List<Role> rolesList = null;
		
		try {
			rolesList = roleRepository.findAdminRolesForUsers(); 
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ADMIN USR ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return this.accessControl.filterRoleListForCurrentUser(rolesList, RoleAccessControl.ADD_ROLE_PERMISSION);
	}

	@Override
	public List<Role> findPrivilegedRoles() {
		List<Role> rolesList = null;
		
		try {
			rolesList = roleRepository.findPrivilegedRoles(); 
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL PRIVILEGED ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return this.accessControl.filterRoleListForCurrentUser(rolesList, RoleAccessControl.ADD_ROLE_PERMISSION);
	}

	@Override
	public List<String> findAllRoleName() {
		List<Role> allRoles = null;
		List<String> allRoleName = null;
		
		try {
			//Added for HIX-27184 and HIX-27305
			allRoles = findAll();
			allRoles = this.accessControl.filterRoleListForCurrentUser(allRoles, RoleAccessControl.ADD_ROLE_PERMISSION);
			allRoleName = null;
			if(null != allRoles){
				allRoleName = new ArrayList<String>();
				for (Role role : allRoles) {
					allRoleName.add(role.getName());
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLE NAMES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return allRoleName;
	}

	@Override
	public List<String> findAllManagedRoleNames() {
		List<Role> allRoles = null;
		List<String> allRoleName = null;
		
		try {
			//Added for HIX-27184 and HIX-27305
			allRoles = findAll();
			allRoles = this.accessControl.filterRoleListForCurrentUser(allRoles, RoleAccessControl.UPDATE_USERDETAILS_PERMISSION);
			allRoleName = null;
			if(null != allRoles){
				allRoleName = new ArrayList<String>();
				for (Role role : allRoles) {
					allRoleName.add(role.getName());
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLE NAMES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return allRoleName;
	}

	
	@Override
	public Role findRoleByUserId(int id) {
        Role role = null;
		
		try {
			role = roleRepository.findRoleByUserId(id);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE TRYING TO FETCH ROLE BY USER ID: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		
		return role;
	}

	/**
	 * This method returns all the roles existing in the system without filtering for the current user
	 * Required because privileged user can modify all the user but may not assign
	 */
	@Override
	public List<Role> findAllRoleTypes(Sort sort) {
		List<Role> rolesList = null;
		try {
			rolesList = roleRepository.findAll();
			rolesList = this.accessControl.filterRoleListForCurrentUser(rolesList, RoleAccessControl.UPDATE_USERDETAILS_PERMISSION);
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return rolesList;
	}
	
	/**
	 * This method returns all the roles existing in the system without filtering for the current user
	 * Required because privileged user can modify all the user but may not assign
	 */
	@Override
	public List<String> findAllRoleTypesByName(Sort sort) {
		List<Role> rolesList = null;
		List<String> allRoleName = new ArrayList<>();
		try {
			rolesList = roleRepository.findAll();
			rolesList = this.accessControl.filterRoleListForCurrentUser(rolesList, RoleAccessControl.ADD_ROLE_PERMISSION);
			for (Role role : rolesList) {
				allRoleName.add(role.getName());
			}
		} catch(Exception e){
			LOGGER.error("ERR: WHILE TRYING TO FETCH ALL ROLES: ", e);
			throw new GIRuntimeException(PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null, Severity.HIGH);
		}
		return allRoleName;
	}
	
	@Override
	public List<UserRole> findProvisioningOptionsForExistingRoles(Set<UserRole> existingUserRoles) {
		HashSet<String> existingRoleNames = new HashSet<String>();
		if (existingUserRoles == null) {
			return new ArrayList<UserRole>();
		}
		List<UserRole> returnList = null;
		try {
			String currentRoleName = accessControl.getCurrentUserRoleName();
			if (currentRoleName == null) {
				return null;
			}
			String defaultRoleName = null;
			currentRoleName = currentRoleName.toUpperCase();
			returnList = new ArrayList<UserRole>();
			String rName = null;
			HashMap<String, RoleProvisioningRule> rolePermissionMap = accessControl.getCurrentRoleProvisioningMap();
			for (UserRole tmpRole : existingUserRoles) {
				rName = tmpRole.getRole().getName();
				if (tmpRole.isDefaultRole()){
					defaultRoleName = tmpRole.getRole().getName();
					continue;
				}
				if (accessControl.canAddRole(rName, rolePermissionMap) && "N".equalsIgnoreCase(tmpRole.getRole().getIsExclusive().toString() )) {
					LOGGER.info("Adding "+rName+", [Label:"+tmpRole.getRole().getLabel()+"] to list of existing roles which could be modified by "+ currentRoleName);
					returnList.add(tmpRole);
					existingRoleNames.add(tmpRole.getRole().getName());
				}
			}
			LOGGER.info("Checking additional roles provisionable by "+currentRoleName);
			if(rolePermissionMap != null && rolePermissionMap.size() > 0){
				Set<Entry<String,RoleProvisioningRule>> provisionEntries = rolePermissionMap.entrySet();
				Iterator<Entry<String,RoleProvisioningRule>> cursor = provisionEntries.iterator();
				RoleProvisioningRule e = null;
				UserRole tmpUserRole = null;
				Role managedRole = null;
				String managedRoleName = null;
				Entry<String,RoleProvisioningRule> tmpEntry = null;
				while(cursor.hasNext()){
					tmpEntry = cursor.next();
					managedRoleName = tmpEntry.getKey();
					e = tmpEntry.getValue();
					if(managedRoleName.equalsIgnoreCase(defaultRoleName) || !e.isActive() || !e.isAddAllowed()){
						continue;
					}
					//exclusive roles will not be added/updated by super roles
					if("Y".equalsIgnoreCase(e.getManagedRole().getIsExclusive().toString() )){
						continue;
					}
					if(!existingRoleNames.contains(managedRoleName)){
						tmpUserRole=new UserRole();
						managedRole = e.getManagedRole();
						tmpUserRole.setRole(managedRole);
						tmpUserRole.setIsActive('N');
						tmpUserRole.setRoleFlag('N');
						returnList.add(tmpUserRole);
						LOGGER.info("Adding "+managedRole.getName()+", [Label:"+managedRole.getLabel()+"] as additional provisionable role by "+currentRoleName);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING LIST OF PROVISIONABLE ROLES: ", e);
			throw new GIRuntimeException(
					PlatformErrorCode.PLATFORM_00002.getErrorCode(), e, null,
					Severity.HIGH);
		}
		return returnList;
	}
	
	public void bootstrapProvisioningRules(){
		List<Role> allRoles = this.roleRepository.findAll();
		// Create a map for faster lookups
		HashMap<String, Role> allRolesMap = new HashMap<>(allRoles.size());
		for(Role tmp: allRoles){
			allRolesMap.put(tmp.getName(), tmp);
		}
		boolean  bootstrapCompleted = this.accessControl.initiateBootstrap(allRolesMap);
		if(!bootstrapCompleted){
			throw new GIRuntimeException("Bootstrapping for provisioning rules failed!");
		}
	}
	
}