package com.getinsured.hix.platform.security;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.platform.util.GhixHttpSecurityConfig;

public class GhixCacheControlFilter implements Filter{
	
	private boolean filterEnabled = false;
	@Autowired
	private GhixHttpSecurityConfig config;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if(this.filterEnabled){
			if(config == null){
				config = new GhixHttpSecurityConfig();
			}
			HttpServletRequest req= (HttpServletRequest) request;
			HttpServletResponse httpResponse = (HttpServletResponse) response;

			if(config.isUrlProtected(req.getRequestURI())){
				httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
				httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
				httpResponse.setDateHeader("Expires", 0); // Proxies.
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		
		String filterEnabledStr = config.getInitParameter("cacheFilterEnabled");
		if(filterEnabledStr != null){
			this.filterEnabled = filterEnabledStr.equalsIgnoreCase("true");
		}
	}
}
