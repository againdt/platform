package com.getinsured.hix.platform.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;

public interface AffiliateFlowRepository extends TenantAwareRepository<AffiliateFlow, Integer> {
	//TODO: Think of reusing the the logic IAffiliateFlowRepository in ghix-affiliate-svc
	
	AffiliateFlow findByAffiliateflowId(Integer id);
	
	@Query("select flow from AffiliateFlow flow where flow.affiliate.affiliateId = :affiliateId and flow.flowName = :flowName and flow.affiliateflowId !=:flowId")
	AffiliateFlow findByAffiliateIdAndFlowname(@Param("affiliateId") Long affiliateId,@Param("flowName") String flowName,@Param("flowId") Integer flowId);
	
	@Query("select flow from AffiliateFlow flow where flow.affiliate.affiliateId = :affiliateId and flow.isDefault = 'Y'")
	AffiliateFlow findByAffiliateIdAndIsDefault(@Param("affiliateId") Long affiliateId);

	@Query("select flow from AffiliateFlow flow where affiliateflowId in (:flowIdSet)")
	List<AffiliateFlow> getAffiliateFlowsByIds(@Param("flowIdSet") Set<Integer> flowIdSet);

	@Query("select flow.id, flow.flowName from AffiliateFlow flow where flow.affiliate.affiliateId = :affiliateId ")
	List<Object[]> findByAffiliateId(@Param("affiliateId") Long affiliateId);
	
	@Query("select flow.id, flow.flowName, flow.configurations from AffiliateFlow flow where flow.affiliate.affiliateId = :affiliateId ")
	List<Object[]> getNameConfigById(@Param("affiliateId") Long affiliateId);

	@Query("select flow from AffiliateFlow flow where flow.url = :url")
	AffiliateFlow findByUrl(@Param("url") String url);
	
	@Query(" select affFlow FROM AffiliateFlow as affFlow inner join fetch affFlow.affiliate b"+
			" where replace(affFlow.flowName, '''', '_') = :flowName ")
	AffiliateFlow getAffFlowDetailsByName(@Param("flowName") String flowName);
	
	@Query("select flow from AffiliateFlow flow where flow.configurations LIKE CONCAT('%',:capaignLikeString,'%')")
	List<AffiliateFlow> findAffiliateIdFlowIdByCampaignId(@Param("capaignLikeString") String capaignLikeString);
	
	

}
