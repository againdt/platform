package com.getinsured.hix.platform.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import com.getinsured.hix.platform.util.exception.GIException;

public class DateRange {
	private static SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
	private Date startDate = null;
	private Date endDate = null;
	
	public DateRange(){
		sdf.setLenient(false);
	}
	
	public DateRange(Date startDate, Date endDate){
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) throws GIException {
		if(this.endDate != null){
			if(this.endDate.before(startDate)){
				throw new GIException("Invalid Start Date, can not be set later than "+endDate.toString());
			}
		}
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) throws GIException {
		if(this.startDate != null){
			if(this.startDate.after(endDate)){
				throw new GIException("Invalid End Date, can not set before "+this.startDate.toString());
			}
		}
		this.endDate = endDate;
	}
	
	public boolean isCurrent(){
		if(this.endDate == null || this.startDate == null){
			throw new RuntimeException("Range not initialized");
		}
		Date currentDate = new TSDate(System.currentTimeMillis());
		if(currentDate.after(startDate) && currentDate.before(endDate)){
			return true;
		}
		return false;
	}
	
	public boolean isCurrent(String date, String dateFormat) throws ParseException{
		if(this.endDate == null || this.startDate == null){
			throw new RuntimeException("Range not initialized");
		}
		SimpleDateFormat sdf  = new SimpleDateFormat(dateFormat);
		Date dt = sdf.parse(date);
		if(dt.after(startDate) && dt.before(endDate)){
			return true;
		}
		return false;
	}
	
	public static Date getDate(String dateInString) throws ParseException{
		return sdf.parse(dateInString);
	}
}
