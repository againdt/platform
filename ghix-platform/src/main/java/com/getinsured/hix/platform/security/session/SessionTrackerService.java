package com.getinsured.hix.platform.security.session;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.UserSession;

public interface SessionTrackerService {
	
	UserSession fetchUserSession(int userId);

	boolean createUserSession(HttpSession session, String browserIp, long userId, boolean privileged);
	
	boolean createUserSessionOnRegistration(HttpSession session, String browserIp, long userId, String sessionVal, boolean privileged);

	boolean markUserSessionClosed(String sessionId);

	boolean markUserSessionLoggedOut(String sessionId);
	boolean markUserSessionLoggedOutByApplication(String sessionId) ;
	
	boolean markUserSessionInitToInProgress(String sessionId, int userId);
	
	void cleanupMemoryMappedSessions();
	
	boolean killUserActiveSession(int userId);
	
	void storeHttpSessionInDB(HttpServletRequest request,
			AccountUser user);
	
	HttpSession replaceHttpSessionWithDBSession(HttpServletRequest request,
			UserSession userSession, HttpSession httpSession, AccountUser user);

	boolean checkSessionMultiplicityAllowed(boolean privilegedUser);

	boolean createUserSession(HttpSession httpSession, String browserIp, AccountUser user, boolean privileged);

	List<UserSession> invalidateAllSessions(List<UserSession> sessions, int userId);

	UserSession invalidateSession(UserSession userSession);
}
