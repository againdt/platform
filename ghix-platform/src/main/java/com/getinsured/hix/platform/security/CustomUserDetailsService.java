package com.getinsured.hix.platform.security;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.UserSession;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRoleRepository;
import com.getinsured.hix.platform.security.repository.IUserSessionRepository;
import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.security.tokens.GiBinaryToken;
import com.getinsured.hix.platform.security.tokens.SecureTokenFactory;
import com.getinsured.hix.platform.util.GhixPlatformConstants;


@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService,AuthenticationUserDetailsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	@Autowired private IUserRepository userRepository;
	@Autowired private IUserRoleRepository userRoleRepo;
	@Autowired IUserSessionRepository sessionRepo;
	@Autowired SessionTrackerService sessionTracker;
		
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Getting user details for {}", username );
		}
		List<UserRole> userRoles;
		AccountUser domainUser = null;
		try {
			
			if(username.contains("ssoAuth:")){
				username=StringUtils.substringAfter(username, "ssoAuth:".intern());
				domainUser = userRepository.findByExtnAppUserId(username);
				domainUser.setPassword("ssoAuth:NOT_APPLICABLE");
				domainUser.setUuid("ssoAuth:NOT_APPLICABLE");
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("SSO Auth received for {}, using external App Id for lookup", username );
				}
				
			}else if(username.equalsIgnoreCase("anonymous".intern())) {
				domainUser = this.getAnonymousUser();
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Created dummy user with name {}", username );
				}
			}
			else {
				domainUser = userRepository.findByUserName(username.toLowerCase());
				if(domainUser == null){
					if(LOGGER.isInfoEnabled()){
						LOGGER.info("No user found with user name {} returning dummy anonymous",username);
					}
					domainUser = this.getAnonymousUser();
				}else{
					checkSessionMultiplicity(domainUser);
					userRoles = this.userRoleRepo.findByUser(domainUser);
					domainUser.setUserRole(new HashSet<UserRole>(userRoles));
					Map<String, String> loginContext = this.initializeLoginContext();
					if(loginContext != null) {
						domainUser.setLoginContext(loginContext);
					}
				}
			}
		} catch (Exception ex) {
        	LOGGER.error("Exception loading the user : " + ex.getMessage(),ex);
            throw new AuthenticationServiceException(ex.getMessage(), ex);
        }
		return domainUser;
	}
	private void checkSessionMultiplicity(AccountUser dbUser){
		int id = dbUser.getId();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Checking session multiplicity for user:{}", id);
		}
		List<UserSession> existingSessions = this.sessionRepo.findActiveUserSession(id);
		List<UserSession> invalidatedSession = null;
		if(existingSessions != null && existingSessions.size() > 0){
			// Check if multiple Sessions are allowed
			if(!this.sessionTracker.checkSessionMultiplicityAllowed(this.isPrivilegedUser(dbUser))){
				invalidatedSession = this.sessionTracker.invalidateAllSessions(existingSessions, id);
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Invalidated existing session "+invalidatedSession.size());
				}
			}else {
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("User with {} allowed to have multiple sessions");
				}
			}
		}
	}
	
	private boolean isPrivilegedUser(AccountUser user){
		Set<UserRole> userRoles = user.getUserRole();
		boolean isPrivilegedRole = false;
		UserRole tmp = null;
		Iterator<UserRole> cursor = userRoles.iterator();
		while(cursor.hasNext()){
			tmp = cursor.next();
			if(tmp.isDefaultRole()){
				if(tmp.getRole().getPrivileged() == 1){
					isPrivilegedRole = true;
					break;
				}
			}
		}
		return isPrivilegedRole;
	}
	
	private Map<String, String> initializeLoginContext() {
		ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		if(requestAttribute != null) {
			HttpServletRequest request = requestAttribute.getRequest();
			String ref = request.getParameter("refId");
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Received login context token:"+ref);
			}
			GiBinaryToken token = null;
			if(ref != null) {
				try {
					token = SecureTokenFactory.processBinaryToken(ref, GhixPlatformConstants.TOKEN_VALIDATION_KEY);
					return  token.getAllParameters();
				} catch (Exception e) {
					LOGGER.error("Error processing the token from relayState {}, assuming not a valid token",ref,e);
				}
			}
		}
		return null;
	}
	
	private AccountUser getAnonymousUser() {
		AccountUser domainUser = new AccountUser();
		domainUser.setUserName("anonymous");
		domainUser.setAuthenticated(true);
		domainUser.setConfirmed(1);
		return domainUser;
	}

	@Override
	public UserDetails loadUserDetails(Authentication token)
			throws UsernameNotFoundException {
		String username = token.getName();
		UserDetails domainUser = null;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Loading user with username {}",username);
		}
		domainUser= loadUserByUsername(username);
		return domainUser;
	}
}
