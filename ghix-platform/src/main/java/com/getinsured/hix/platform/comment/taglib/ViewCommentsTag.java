package com.getinsured.hix.platform.comment.taglib;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.service.UserService;


/**
 * @author talreja_n
 * @since November 02, 2012
 *
 */
public class ViewCommentsTag extends TagSupport {

	private String targetId;
	private String targetName;
	private String NEWLINEBREAK="&#xa;"; //declaration for Newline Breaks for editComment parameter to be passed without line breaks
	private String HTMLBREAK="<br/>"; //declaration for HTML Breaks

	private ApplicationContext applicationContext;

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewCommentsTag.class);

	private static final long serialVersionUID = 1;
	//Commented: special character string will throw exception on use of replace all.
	/*private static final String template = "<div class=''>" +
			"<br>" +
			"<MESSAGE>" +
			"</div>";*/

	@Override
	public int doStartTag() throws JspException {
		applicationContext = RequestContextUtils.getWebApplicationContext(
				pageContext.getRequest(),
				pageContext.getServletContext()
				);

		final CommentTargetService commentTargetService = applicationContext.getBean("commentTargetService",CommentTargetService.class);
		final UserService userService = applicationContext.getBean("userService",UserService.class);
		
		try{

			String message = "";
			List<Comment> comments = null;

			final CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(getTargetId()), TargetName.valueOf(getTargetName()));


			LOGGER.info("Checking if target ID " + getTargetId() + " exists for entity " + getTargetName());


			if(commentTarget == null){
				message="<i>No comments</i>";

				LOGGER.info("Target ID " + getTargetId() + " does not exist for entity " + getTargetName());

			}
			else{
				comments = commentTarget.getComments();
				if(comments == null || comments.size() == 0){
					message="<i>No comments</i>";

					LOGGER.info("No comments for Target ID " + getTargetId() + " of entity " + getTargetName());

				}
				else{

					//Soting comments
					LOGGER.info("Sorting comments in descending order of date");

					Collections.sort(comments);

					LOGGER.info("Fetching comments for Target ID " + getTargetId() + " of entity " + getTargetName());


					for(final Comment comment : comments){

						//Skipping null comments
						if(comment.getComment() == null || comment.getComment().trim().length() == 0){
							continue;
						}

						//						message += "<div>";
						//						message += "<i>"+comment.getComment().trim()+"</i>";
						//						message += "</div>";
						//						message += "-" + comment.getCommenterName().trim() + " on ";
						AccountUser user = userService.findById(comment.getCommentedBy());
						if (user != null) {
							message += user.getFirstName() + " " + user.getLastName() + " added a comment - ";
						}else {
							message += comment.getCommenterName().trim() + " added a comment - ";
						}
						//Formatting created date
						SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
						formatter.setTimeZone(TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
						final String postedDate = formatter.format(comment.getUpdated());
						String cmnt=comment.getComment(); //New variable for storing comment Text
						cmnt=cmnt.replaceAll(NEWLINEBREAK,HTMLBREAK); //Replacing newline feed with HTML equivalent <br/>
						message += "<b>" + postedDate + "</b>";
						message += "<span class=\"pull-right hide_comment_btn\"><a id=\"a"+comment.getId()+"\" href=\"#\" onClick=\"return editComment(" + comment.getId() +");\" class=\"btn btn-mini\"><i class=\"icon-pencil\"></i><span class=\"aria-hidden\">Edit Comment</span></a> <a href=\"#\" onClick=\"return delComment(" + comment.getId() +");\" class=\"btn btn-mini deleteComment\"><i class=\"icon-trash\"></i><span class=\"aria-hidden\">Delete Comment</span></a></span>"; // Changed call to EditComment method by sending only one parameter;commentId
						message += "<div class=\"controls gutter10-tb\" id=\"commentText" +comment.getId() + "\">";
						message += "<div id=\"comment_text" + comment.getId() + "\"><em>"+cmnt.trim()+"</em></div>";
						message += "<input type=\"hidden\" value=\""+ comment.getComment() +"\" id=\"cmt"+comment.getId()+"\"></div><hr>"; //commentText sent as a hidden field
						
					}
				}
			}

			LOGGER.info("Displaying comments for Target ID " + getTargetId() + " of entity " + getTargetName());


			final JspWriter out = pageContext.getOut();
			//Commented: special character string will throw exception on use of replace all.
			//out.println(ViewCommentsTag.template.replaceAll("<MESSAGE>", message));
			out.println("<div class=''> <br>"+message+" </div>");
		}
		catch (final Exception e) {
			LOGGER.error("Exception Thrown"+e);
		}
		return SKIP_BODY;
	}
	public String getTargetId() {
		return targetId;
	}
	public void setTargetId(final String targetId) {
		this.targetId = targetId;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(final String targetName) {
		this.targetName = targetName;
	}


}
