package com.getinsured.hix.platform.couchbase.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.couchbase.client.core.CouchbaseException;
import com.couchbase.client.java.document.JsonDocument;
import com.getinsured.hix.platform.couchbase.CouchbaseConnectionManager;
import com.getinsured.hix.platform.couchbase.helper.DocumentHelper;

@Service("couchAction")
public class CouchAction<T> {

	private static final String CB_DOC_ID_SEPARATOR = "::";

	@Autowired private CouchbaseConnectionManager couchbaseConnectionManager;
	
	@Autowired private DocumentHelper documentHelper;
	
	public T getDocumentById(String id, Class<T> type){
		
		JsonDocument doc = couchbaseConnectionManager.getNonbinaryBucket().get(id);
		
		T document = (doc == null) ? null : documentHelper.fromJsonDocument(doc, type);
		
		return document;
	}
	
	public String createDocument(T document, String id){
		
		JsonDocument docIn = documentHelper.toJsonDocument(document, id);
		
		JsonDocument docOut = null;
		try {
			docOut = couchbaseConnectionManager.getNonbinaryBucket().insert(docIn);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}
	
	public String getNextId(String category, String subCategory, String type) {
		return UUID.randomUUID().toString() + CB_DOC_ID_SEPARATOR + type + 
				CB_DOC_ID_SEPARATOR + category + CB_DOC_ID_SEPARATOR + subCategory;
	}
}
