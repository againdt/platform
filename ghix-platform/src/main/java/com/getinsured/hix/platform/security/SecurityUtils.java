package com.getinsured.hix.platform.security;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;

public class SecurityUtils {
	public static final String INTERNAL_REF = "SECURITY_UTILS.INTERNAL_REF";
	private static Logger log = LoggerFactory.getLogger(SecurityUtils.class);
	
	public static boolean isExternalRedirect() {
		String host = null;
		StringBuffer incoming = null;
		ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		if (requestAttribute == null) {
			log.debug("No request attributes available".intern());
			return false;
		}
		boolean isExternal = false;
		HttpServletRequest request = requestAttribute.getRequest();
		HttpSession session = request.getSession();
		if(session != null) {
			Object internalRef = session.getAttribute(INTERNAL_REF);
			if(internalRef != null) {
				if(log.isInfoEnabled()) {
					log.info("Internal redirect to {}",request.getRequestURI());
				}
				return false;
			}
		}
		String referrer = request.getHeader("Referer".intern());
		if (referrer != null) {
			incoming = request.getRequestURL();
			try {
				URL ref = new URL(referrer);
				host = ref.getHost();
				//incoming = request.getRequestURL();
				isExternal = (incoming.indexOf(host) == -1) || (referrer.indexOf("/hix".intern()) == -1);
				if(log.isDebugEnabled()) {
					log.debug("Received request from {} and host {} for URL {} External redirect {}".intern(), referrer, host, incoming, isExternal);
				}
			} catch (MalformedURLException e) {
				//  Not one of our page responsible for this request.	
				isExternal = true;
			}
		}
		return isExternal;
	}
	
	public static AccountUser getLoggedInUser() {
		AccountUser user = null;
		ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		if (requestAttribute == null) {
			log.info("No request attributes available".intern());
			return null;
		}
		HttpServletRequest request = requestAttribute.getRequest();
		HttpSession session = request.getSession(false);
		String id = "Not available".intern();
		if (session != null) {
			id = session.getId();
			SecurityContextImpl sci = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
			if (sci != null) {
				Authentication auth = sci.getAuthentication();
				if (auth != null) {
					Object principal = auth.getPrincipal();
					if (principal instanceof UserDetails) {
						UserDetails cud = (UserDetails) principal;
						String name = cud.getUsername();
						log.info("DEBUG : Incoming URL {}, Referer {}, User Context {}, session Id {}",
								request.getRequestURL(), request.getHeader("Referer".intern()), name, id);
						if (cud instanceof AccountUser) {
							user = (AccountUser) cud;
						}
					} else {
						log.info("DEBUG : Incoming URL {}, Referer {}, Principal {} and Session Id {}",
								request.getRequestURL(), request.getHeader("Referer".intern()), auth, id);
					}
				} else {
					log.info("DEBUG : Incoming URL {}, Referer {}, Auth null and Session Id {}",
							request.getRequestURL(), request.getHeader("Referer".intern()), id);
				}
			} else {
				log.info("DEBUG : Incoming URL {}, Referer {}, Security Context Context null and Session Id {}",
						request.getRequestURL(), request.getHeader("Referer".intern()), id);
			}
		}
		if (user == null) {
			log.info(
					"DEBUG : Failed to find the user from session, Incoming URL {}, Referer {}, Security Context Context null and Session Id {}, checking the security context",
					request.getRequestURL(), request.getHeader("Referer".intern()), id);
			// NULL session, check if the SecurityContextHolder is initialized
			SecurityContext secContext = SecurityContextHolder.getContext();
			if (secContext != null) {
				Authentication auth = secContext.getAuthentication();
				if (auth != null) {
					Object principal = auth.getPrincipal();
					if (principal instanceof UserDetails) {
						UserDetails cud = (UserDetails) principal;
						String name = cud.getUsername();
						log.info("DEBUG : Incoming URL {}, Referer {}, User Context {}, session Id {}",
								request.getRequestURL(), request.getHeader("Referer".intern()), name, id);
						if (cud instanceof AccountUser) {
							user = (AccountUser) cud;
						} else {
							log.info("DEBUG : Incoming URL {}, Referer {}, Principal {} and Session Id {}",
									request.getRequestURL(), request.getHeader("Referer".intern()), auth, id);
						}
					}
				} else {
					log.info("DEBUG : Incoming URL {}, Referer {}, Auth null and Session Id {}",
							request.getRequestURL(), request.getHeader("Referer".intern()), id);
				}
			} else {
				log.info("DEBUG : Incoming URL {}, Referer {}, Security Context Context null and Session Id {}",
						request.getRequestURL(), request.getHeader("Referer".intern()), id);
			}
		}
		return user;
	}
}
