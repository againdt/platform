package com.getinsured.hix.platform.config;


import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.spring.cache.CacheBuilder;
import com.couchbase.client.spring.cache.CouchbaseCacheManager;
import com.getinsured.hix.platform.couchbase.CouchbaseConnectionManager;
import com.getinsured.hix.platform.service.GIAppConfigService;
import com.getinsured.hix.platform.service.jpa.GIAppConfigServiceImpl;

/**
 * Couchbase-backed spring cache configuration.
 * <p>
 * This is to be used instead of EhCache or In-Memory cache implementations.
 * </p>
 *
 * @see CacheConfiguration
 * @see CouchbaseConnectionManager
 * @author golubenko_y
 * @since 6/2/2016
 */
@ConditionalOnProperty(name = "cache.enabled", havingValue = "couch")
@EnableCaching(mode = AdviceMode.PROXY)
@Configuration
@CacheConfig(cacheManager = "couchCacheManager")
public class CacheConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(CacheConfiguration.class);

  private static int FIVE_MIN = 1000 * 60 * 5;

  @Autowired
  private CouchbaseConnectionManager couchbaseConnectionManager;

  @Bean
  @Primary
  public GIAppConfigService giAppConfigService()
  {
    return new GIAppConfigServiceImpl();
  }

  /**
   * Initializes {@link CacheManager}.
   * <p>
   * This will be picked up by the Spring Framework and used 
   * for <code>@Cacheable</code> interceptor to cache objects
   * in configured Couchbase environment.
   * </p>
   * @return configured couchbase connection manager.
   */
  @Bean(name = {"couchCacheManager"})
  public CacheManager cacheManager()
  {
    if (log.isInfoEnabled())
    {
      log.info("[[ Enabling CouchBase @Cacheable ]]");
    }

    final Bucket cacheableBucket = couchbaseConnectionManager.getNonbinaryBucket();

    Map<String, CacheBuilder> caches = new HashMap<>();

    // @formatter:off
    caches.put("zip",                 CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 8));
    caches.put("companyLogo",         CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("plan_summary",        CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("downloadDoc",         CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("issuerId",            CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("employerName",        CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("giappconfig",         CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 4));
    caches.put("zipCodes",            CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 8));
    caches.put("getInsuedBlogs",      CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("ghix_static_content", CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 8));
    caches.put("lookupvalue",         CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("ecmcontent",          CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("specialtyDomainList", CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("languageDomainList",  CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("sepevents",           CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN));
    caches.put("menuItems",           CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 8));
    caches.put("tenants",             CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 8));
    caches.put("junit",               CacheBuilder.newInstance(cacheableBucket).withExpirationInMillis(FIVE_MIN * 8));
    // @formatter:on

    if (log.isInfoEnabled())
    {
      log.info("Configured @Cacheable with Couchbase backend");
    }

    return new CouchbaseCacheManager(caches);
  }
}
