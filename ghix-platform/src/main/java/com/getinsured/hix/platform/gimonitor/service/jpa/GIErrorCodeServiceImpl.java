package com.getinsured.hix.platform.gimonitor.service.jpa;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.GIErrorCode;
import com.getinsured.hix.platform.gimonitor.repository.GIErrorCodeRepository;
import com.getinsured.hix.platform.gimonitor.service.GIErrorCodeService;

@Service("giErrorCodeService")
public class GIErrorCodeServiceImpl implements GIErrorCodeService {

	@Autowired
	private GIErrorCodeRepository giErrorCodeRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(GIErrorCodeServiceImpl.class);

	@Override
	@Transactional
	public GIErrorCode saveOrUpdateGIErrorCode(GIErrorCode giErrorCode) {
		if (LOGGER.isDebugEnabled()){
			LOGGER.debug("Saving/Updating GIErrorCode object - "+ giErrorCode);
		}
		return giErrorCodeRepository.save(giErrorCode);
	}

	@Override
	@Transactional
	public GIErrorCode getGIErrorCodeByErrorCode(String errorCode) {
		if (LOGGER.isDebugEnabled()){
			LOGGER.debug("Find GIErrorCode by - "+ errorCode);
		}
		List<GIErrorCode> errorCodeList = giErrorCodeRepository.findByErrorCode(errorCode);
		return (errorCodeList!= null && !errorCodeList.isEmpty())?errorCodeList.get(0):null ;
		
	}

}
