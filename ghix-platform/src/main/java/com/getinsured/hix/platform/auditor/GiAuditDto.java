package com.getinsured.hix.platform.auditor;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;

public class GiAuditDto {


	private static final String OPENING_SQUARE_BRACKET = "[ ";
	private static final String CLOSING_SQUARE_BRACKET = "]";
	private static final String OPENING_CLURY_BRACKET = " { ";
	private static final String CLOSING_CLURY_BRACKET = " } ";
	private static final String COMMA = ", ";
	private static final String EXCEPTION_MESSAGE = " exceptionMessage= ";
	private static final String EXCEPTION_INFO_HEADER = " ExceptionInfo: ";
	private static final String PARAMETER_LIST = " parameterList=";
	private static final String PARAMETER_INFO_HEADER = " ParameterInfo: ";
	private static final String TRANSACTION_NAME = "transactionName=";
	private static final String EVENT_NAME = " eventName=";
	private static final String EVENT_TYPE = " eventType=";
	private static final String EVENT_INFO_HEADER = " EventInfo: ";
	private static final String IP_ADDRESS = " ipAddress=";
	private static final String USER_AGENT = " userAgent=";
	private static final String BROWSER_INFO_HEADER = " BrowserInfo: ";
	private static final String REFERRER_URL = " referrerUrl=";
	private static final String IS_J_SESSION_ACTIVE = " isJSessionActive=";
	private static final String J_SESSION_ID = " jSessionId=";
	private static final String CONTROLLER_METHOD_NAME = " controllerMethodName=";
	private static final String REQUEST_URL = " requestUrl=";
	private static final String STATUS = " status=";
	private static final String CONTROLLER_INFO_HEADER = " ControllerInfo: ";
	private static final String LOGGED_IN_USER_ROLES = " loggedInUserRoles=";
	private static final String LOGGED_IN_USER_ACTIVE_MODULE_NAME = " loggedInUserActiveModuleName=";
	private static final String LOGGED_IN_USER_ACTIVE_MODULE_ID = " loggedInUserActiveModuleId=";
	private static final String LOGGED_IN_USER_ID = " loggedInUserId=";
	private static final String LOGGED_IN_USERNAME = " loggedInUsername=";
	private static final String LOGGED_IN_USER_INFO_HEADER = "LoggedInUserInfo: ";


	private String loggedInUsername;
	private int loggedInUserId;
	private int loggedInUserActiveModuleId;
	private String loggedInUserActiveModuleName;
	private Collection<? extends GrantedAuthority> loggedInUserRoles;

	private String status;
	private String requestUrl;
	private String controllerMethodName;
	private String jSessionId;
	private boolean isJSessionActive;
	private String referrerUrl;

	private String userAgent;
	private String ipAddress;

	private String transactionName;
	private EventTypeEnum eventType;
	private EventNameEnum eventName;

	private List<String> parameterList;

	private String exceptionMessage;



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(OPENING_SQUARE_BRACKET)
				.append(LOGGED_IN_USER_INFO_HEADER).append(OPENING_CLURY_BRACKET)
				.append(LOGGED_IN_USERNAME).append(loggedInUsername).append(COMMA)
				.append(LOGGED_IN_USER_ID).append(loggedInUserId).append(COMMA)
				.append(LOGGED_IN_USER_ACTIVE_MODULE_ID).append(loggedInUserActiveModuleId).append(COMMA)
				.append(LOGGED_IN_USER_ACTIVE_MODULE_NAME).append(loggedInUserActiveModuleName).append(COMMA)
				.append(LOGGED_IN_USER_ROLES).append(loggedInUserRoles)

				.append(CLOSING_CLURY_BRACKET).append(CONTROLLER_INFO_HEADER).append(OPENING_CLURY_BRACKET)
				.append(STATUS).append(status).append(COMMA)
				.append(REQUEST_URL).append(requestUrl).append(COMMA)
				.append(CONTROLLER_METHOD_NAME).append(controllerMethodName).append(COMMA)
				.append(J_SESSION_ID).append(jSessionId).append(COMMA)
				.append(IS_J_SESSION_ACTIVE).append(isJSessionActive).append(COMMA)
				.append(REFERRER_URL).append(referrerUrl)

				.append(CLOSING_CLURY_BRACKET).append(BROWSER_INFO_HEADER).append(OPENING_CLURY_BRACKET)
				.append(USER_AGENT).append(userAgent).append(COMMA)
				.append(IP_ADDRESS).append(ipAddress)

				.append(CLOSING_CLURY_BRACKET).append(EVENT_INFO_HEADER).append(OPENING_CLURY_BRACKET)
				.append(TRANSACTION_NAME).append(transactionName).append(COMMA)
				.append(EVENT_TYPE).append(eventType).append(COMMA)
				.append(EVENT_NAME).append(eventName)

				.append(CLOSING_CLURY_BRACKET).append(PARAMETER_INFO_HEADER).append(OPENING_CLURY_BRACKET)
				.append(PARAMETER_LIST).append(parameterList)

				.append(CLOSING_CLURY_BRACKET).append(EXCEPTION_INFO_HEADER).append(OPENING_CLURY_BRACKET)
				.append(EXCEPTION_MESSAGE).append(exceptionMessage)
				.append(CLOSING_CLURY_BRACKET)

				.append(CLOSING_SQUARE_BRACKET);
		return builder.toString();
	}



	public static GiAuditDto create(){
		return new GiAuditDto();
	}

	public void setControllerInfo(String status, String controllerMethodName, String requestUrl, String referrerUrl, String jSessionId, boolean isJSessionActive){
		this.status = status;
		this.controllerMethodName = controllerMethodName;
		this.requestUrl = requestUrl;
		this.referrerUrl = referrerUrl;
		this.jSessionId = jSessionId;
		this.isJSessionActive = isJSessionActive;
	}

	public void setLoggedInUserInfo(int userId, String username, int activeModuleId, String activeModuleName, Collection<? extends GrantedAuthority> userRoles){
		this.loggedInUserId = userId;
		this.loggedInUsername = username;
		this.loggedInUserActiveModuleId = activeModuleId;
		this.loggedInUserActiveModuleName = activeModuleName;
		this.loggedInUserRoles = userRoles;
	}

	public void setAnonymousUserInfo(String username){
		this.loggedInUsername = username;
	}


	public void setBrowserInfo(String ipAddress, String userAgent) {
		this.ipAddress = ipAddress;
		this.userAgent = userAgent;
	}

	public void setParameterInfo(List<String> parameters){
		this.parameterList = parameters;
	}

	public void setAuditEventInfo(GiAudit giAudit) {
		this.transactionName = giAudit.transactionName();
		this.eventType = giAudit.eventType();
		this.eventName = giAudit.eventName();
	}

	public void setExceptionInfo(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

}
