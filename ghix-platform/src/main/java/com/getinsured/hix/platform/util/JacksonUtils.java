package com.getinsured.hix.platform.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TimeShifterUtil;

public class JacksonUtils {
	private static ObjectMapper jacksonObjectMapper = new ObjectMapper();
	private static Logger logger = LoggerFactory.getLogger(JacksonUtils.class);
	
	static{
		InputStream resource = JacksonUtils.class.getClassLoader().getResourceAsStream("JacksonMapping.xml");
		if(resource != null){
			try {
				DocumentBuilderFactory factory = Utils.getDocumentBuilderFactoryInstance();
				Document doc = factory.newDocumentBuilder().parse(resource);
				NodeList mixinMaps = doc.getElementsByTagName("jackson_mixin_map");
				int len = mixinMaps.getLength();
				if(len == 0){
					logger.info("Jackson mixin map available without any entry");
					//return;
				}
				NamedNodeMap childs = null;
				for(int i = 0; i < len; i++){
					String targetClassName = null;
					String mixinClassName = null;
					childs = mixinMaps.item(i).getAttributes();
					Node tmp = childs.getNamedItem("target_class");
					if(tmp != null){
						targetClassName = tmp.getNodeValue();
					}
					tmp = childs.getNamedItem("mixin_class");
					if(tmp != null){
						mixinClassName = tmp.getNodeValue();
					}
					if(targetClassName != null && mixinClassName != null){
						Class<?> targetClass = null;
						Class<?> mixinInClass = null;
						try{
							targetClass = Class.forName(targetClassName);
							mixinInClass = Class.forName(mixinClassName);
						}catch(ClassNotFoundException ce){
							throw new GIRuntimeException("Jackson mapper initialization failed with Class Not found for class:"+ce.getMessage());
						}
						jacksonObjectMapper.addMixIn(targetClass, mixinInClass);
					}
				}
			} catch (SAXException | IOException | ParserConfigurationException e) {
			}finally{
				IOUtils.closeQuietly(resource);
			}
		}
	}
	
	public static ObjectWriter getJacksonObjectWriter(Class<?> T){
		return jacksonObjectMapper.writerWithType(T);
	}
	
	public static ObjectReader getJacksonObjectReader(Class<?> T){
		return jacksonObjectMapper.reader(T);
	}
	
	/**
	 * Use this method if you have a stabnard java type to serialize
	 * @param T
	 * @return
	 */
	public static final ObjectWriter getJacksonObjectWriterForJavaType(Class<?> T){
		JavaType type = jacksonObjectMapper.constructType(T);
		return jacksonObjectMapper.writerWithType(type);
	}

	public static ObjectReader getJacksonObjectReaderForJavaType(Class<?> T){
		JavaType type = jacksonObjectMapper.constructType(T);
		return jacksonObjectMapper.reader(type);
	}
	
	/**
	 * Make sure collectionElementType class is a Standard POJO class or think of providing Serializer for this class, i.e all qualified 
	 * Getters and Setters for the object attributes 
	 * Object returned by this method are not thread safe
	 * @param collectionClassType
	 * @param collectionElementType
	 * @return
	 */
	public static final ObjectReader getJacksonObjectReaderForJavaTypeList(Class<?> collectionClassType, Class<?> collectionElementType){
		@SuppressWarnings("unchecked")
		JavaType type = jacksonObjectMapper.getTypeFactory().constructCollectionType((Class<? extends Collection<?>>) collectionClassType, collectionElementType);
		return jacksonObjectMapper.reader(type);
	}

	/**
	 * Make sure Value class is a Standard POJO class or think of providing Serializer for this class, i.e all qualified 
	 * Getters and Setters for the object attributes 
	 * Object returned by this method are not thread safe
	 * @param keyClass
	 * @param valueClass
	 * @return
	 */
	public static final ObjectReader getJacksonObjectReaderForHashMap(Class<?> keyClass, Class<?> valueClass){
		TypeFactory typeFactory = jacksonObjectMapper.getTypeFactory();
		JavaType type = typeFactory.constructMapType(HashMap.class, keyClass, valueClass);//((Class<? extends Collection>) collectionClassType, collectionElementType);
		return jacksonObjectMapper.reader(type);
	}
	
	/**
	 * Make sure Value class is a Standard POJO class or think of providing Serializer for this class, i.e all qualified 
	 * Getters and Setters for the object attributes 
	 * Object returned by this method are not thread safe
	 * @param keyClass
	 * @param valueClass
	 * @return
	 */
	public static final ObjectWriter getJacksonObjectWriterForHashMap(Class<?> keyClass, Class<?> valueClass){
		TypeFactory typeFactory = jacksonObjectMapper.getTypeFactory();
		JavaType type = typeFactory.constructMapType(HashMap.class, keyClass, valueClass);//((Class<? extends Collection>) collectionClassType, collectionElementType);
		return jacksonObjectMapper.writerWithType(type);
	}
	
	/**
	 * Make sure collectionElementType class is a Standard POJO class or think of providing Serializer for this class, i.e all qualified 
	 * Getters and Setters for the object attributes 
	 * @param collectionClassType
	 * @param collectionElementType
	 */
	public static ObjectWriter getJacksonObjectWriterForJavaTypeList(Class<?> collectionClassType, Class<?> collectionElementType){
		@SuppressWarnings("unchecked")
		JavaType type = jacksonObjectMapper.getTypeFactory().constructCollectionType((Class<? extends Collection<?>>) collectionClassType, collectionElementType);
		return jacksonObjectMapper.writerWithType(type);
	}
	
	//List or Vector serialization and de-serialization
	private static void example1() throws JsonProcessingException{
		Vector<String> stringLIst = new Vector<String>();
		stringLIst.add("Abhai");
		stringLIst.add("Chaudhary");
		ObjectWriter writer1 = getJacksonObjectWriterForJavaTypeList(stringLIst.getClass(),String.class);
		String json1 = writer1.writeValueAsString(stringLIst);
		//System.out.println(json1);
	}
	
	private static void example2() throws JsonProcessingException{
		// Map serialization and de-serialization
		HashMap<String,Integer> stringList1 = new HashMap<String,Integer>();
		stringList1.put("first_name",2);
		stringList1.put("last_name",4);
		ObjectWriter writer2 = getJacksonObjectWriterForHashMap(String.class,Integer.class);
		String json2 = writer2.writeValueAsString(stringList1);
		//System.out.println(json2);
	}
	
	private static void example3() throws IOException{
		// POJO serialization and de-serialization
		HashMap<String,MyObject> stringList = new HashMap<String,MyObject>();
		stringList.put("first_name",new MyObject("Abhai","Chaudhay"));
		stringList.put("Lastname",new MyObject("Chaudhary","Yogi"));
		ObjectWriter writer = getJacksonObjectWriterForHashMap(String.class,MyObject.class);
		String json = writer.writeValueAsString(stringList);
		//System.out.println(json);
		ObjectReader reader = getJacksonObjectReaderForHashMap(String.class,MyObject.class);
		Map<String, MyObject> ret = reader.readValue(json);
		MyObject abhai = ret.get("first_name");
		//System.out.println(abhai.getLname());
	}
	private static void example4() throws IOException{
		// List or Vector of a POJO
		Vector<MyObject> objList = new Vector<MyObject>();
		objList.add(new MyObject("Abhai", "Chaudhary"));
		objList.add(new MyObject("Yogi", "Chaudhary"));
		ObjectWriter objwriter = getJacksonObjectWriterForJavaTypeList(objList.getClass(),MyObject.class);
		String objjson = objwriter.writeValueAsString(objList);
		//System.out.println(objjson);
		MyObject abhaiObj = new MyObject("**Abhai", "Chaudhary");
		ObjectWriter wr =  getJacksonObjectWriter(MyObject.class);
		//System.out.println(wr.writeValueAsString(abhaiObj));
	}
	
	public static void main(String[] args) throws IOException{
		long time = TimeShifterUtil.currentTimeMillis();
		Runtime runtime = Runtime.getRuntime(); 
		long maxConsumed = 0;
		long consumed  = 0;
		long consumedNow = consumed;
		int i = 0;
			for(; i < 1000000; i++){
				long memory = runtime.freeMemory();
				example1();
				example2();
				example3();
				example4();
				
				// for smaller objects,this difference is sometimes reported 0
				consumedNow = memory - runtime.freeMemory();
				 
				if(consumedNow  > 0 && consumedNow < consumed){
					System.out.println("GC freed some memory, last consumed:"+consumed+" Consumed now = "+consumedNow);
				}
				consumed = consumedNow;
				if(consumed > maxConsumed){
					maxConsumed = consumed;
				}
			}
			System.out.println(Thread.currentThread().getId()+":"+i+" Max consumed:"+maxConsumed+" Total Available:"+runtime.maxMemory()+ " Time Taken:"+(TimeShifterUtil.currentTimeMillis()-time));
		}
	
		private static class MyObject{
			private String name= null;
			
			private String lname= null;
			
			public String getLname() {
				return lname;
			}
			public void setLname(String lname) {
				this.lname = lname;
			}
			MyObject(){
				
			}
			MyObject(String name, String lname){
				this.setName(name);
				this.lname = lname;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
		}
}
