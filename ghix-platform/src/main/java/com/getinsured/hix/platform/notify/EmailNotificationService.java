package com.getinsured.hix.platform.notify;

import java.net.URI;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.account.inboxnotification.SecureInboxNotificationEmailType;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecureHttpClient;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component("emailNotificationServiceNew")
public class EmailNotificationService {

	private static final String REDIRECT_URL = "redirectUrl";
	private static final String FROM = "From";
	private static final String FOOTER_ADDRESS = "footerAddress";
	private static final String LOGO_URL = "logoUrl";
	private static final String CUSTOMER_CARE_NUM = "customerCareNum";
	private static final String EMAIL_SENDING_FAILED = "Email sending failed ";
	private static final String NAME = "name";
	private static final String USER_NAME = "userName";
	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	private static final List<String> HEADER_FOOTER_TOKENS = Arrays.asList(FROM, CUSTOMER_CARE_NUM,LOGO_URL, FOOTER_ADDRESS, REDIRECT_URL,DISCLAIMER_CONTENT);
	private HttpClient restHttpClient;
	private JSONParser parser;
	
	@Autowired private EmailNotificationHelper emailNotificationHelper;
	@Autowired private PrintNotificationHelper printNotificationHelper;
	@Autowired private EmailService emailService;
	@Autowired private ConfigurationService configurationService;

	private Logger LOGGER = LoggerFactory.getLogger(EmailNotificationService.class);

	@PostConstruct
	public void init() {
		restHttpClient = SecureHttpClient.getHttpClient();
	}
	
	/**
	 *
	 * @param emailMetaData - email meta data - to override fields like Subject, etc
	 * @param messageTokens - actual message in notice - body to draw
	 * @param emailClazzName - custom class which <strong>should map to notice type object's email class name</strong>. Please refer agreement {@link SecureInboxNotificationEmailType}
	 * @param notificationDto - used to draw address (consumer or targeted audience) and create pdf.
	 * 		<strong> If notificationDto is not null
	 * 					1. Location is mandatory.
	 * 					2. If isPrintable is set then PDF is generated (at that time ECM variables are required).
	 * 				 location parameters can be used to draw address (consumer or targeted audience) in notice.
	 * 		<strong> If  notificationDto is null, then no location/address or PDF would come in effect.
	 *
	 * @return generated {@link Notice}
	 * @throws NotificationTypeNotFound, {@link GIRuntimeException}
	 */
	public Notice generateEmail(Map<String, String> emailMetaData, Map<String, String> messageTokens, String emailClazzName, 
			NotificationDto notificationDto)  throws NotificationTypeNotFound {

		// 1. verify notice type object exist in db
		NoticeType noticeType = emailNotificationHelper.getNoticeType(emailClazzName);
		
		// 1.1 verify notificationDto for location and PDF generation
		printNotificationHelper.validateNotificationData(notificationDto);
		
		// 1.2 update the messageTokens if it does not have the default tokens for header and footer 
		populateDefaultHeaderAndFooterTokensIfNull(emailMetaData, messageTokens);

		// 2. form staticTokens object for template (header, footer & notice unique id)
		// 2.1 pass Map<String, String> messageTokens for PHIX - custom header and parameters
		Map<String, String> staticTokens = emailNotificationHelper.populateFinalTokens(noticeType, messageTokens, notificationDto);

		// 3. form finalTokens (staticTokens & messageTokens - dynamic content passed by client)
		Map<String, String> finalTokens = new HashMap<String, String>();
		finalTokens.putAll(messageTokens);
		finalTokens.putAll(staticTokens);
		finalTokens.put(USER_NAME, messageTokens.get(NAME)); // used to draw address. will be used later
		

		// 4. create Email
		Notice noticeObj = emailNotificationHelper.createEmail(noticeType, emailMetaData, finalTokens, emailClazzName);

		// 5. create PDF
		noticeObj = printNotificationHelper.generatePrintNotice(notificationDto, noticeObj);

		return noticeObj;
	}
	

	private void populateDefaultHeaderAndFooterTokensIfNull(Map<String, String> emailMetaData, Map<String, String> messageTokens) {
		Long flowId = null, affiliateId = null, tenantId = null;
		if(messageTokens != null && !messageTokens.isEmpty()) {
			String tempToken = messageTokens.get("flowId");
			if(tempToken != null && NumberUtils.isNumber(tempToken)) {
				flowId = Long.valueOf(tempToken);
			}
			tempToken = messageTokens.get("affiliateId");
			if(tempToken != null && NumberUtils.isNumber(tempToken)) {
				affiliateId = Long.valueOf(tempToken);
			}
			tempToken = messageTokens.get("tenantId");
			if(tempToken != null && NumberUtils.isNumber(tempToken)) {
				tenantId = Long.valueOf(tempToken);
			}
			
			if(emailMetaData.get(FROM) == null) {
				if(affiliateId != null || tenantId != null) {
					String fromEmailAddress = configurationService.fromEmailAddress(affiliateId, tenantId);
					if(StringUtils.isBlank(fromEmailAddress)) {
						fromEmailAddress = StringUtils.EMPTY;
					}
					emailMetaData.put(FROM, fromEmailAddress);
				}
			}
			
			for (String token : HEADER_FOOTER_TOKENS) {
				String tokenPassed = messageTokens.get(token);
				if(StringUtils.isBlank(tokenPassed)) {
					switch(token) {
					case CUSTOMER_CARE_NUM : 
						if(flowId != null || affiliateId != null || tenantId != null) {
							String customerCareNumber = configurationService.customerCareNum(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(customerCareNumber)) {
								customerCareNumber = StringUtils.EMPTY;
							}
							messageTokens.put(CUSTOMER_CARE_NUM, customerCareNumber);
						}
						break;
					case LOGO_URL : 
						if(flowId != null || affiliateId != null || tenantId != null) {
							String logoUrl = configurationService.logoUrl(flowId, affiliateId, tenantId);
							if(StringUtils.isBlank(logoUrl)) {
								logoUrl = StringUtils.EMPTY;
							}
							messageTokens.put(LOGO_URL, logoUrl);
						}
						break;			
					case FOOTER_ADDRESS : 
						if(affiliateId != null || tenantId != null) {
							String footerAddress = configurationService.postalAddress(affiliateId, tenantId);
							if(StringUtils.isBlank(footerAddress)) {
								footerAddress = StringUtils.EMPTY;
							}
							messageTokens.put(FOOTER_ADDRESS, footerAddress);
						}
						break;			
					case REDIRECT_URL : 
						if(affiliateId != null || tenantId != null) {
							String redirectUrl = configurationService.baseUrl(affiliateId, tenantId);
							if(StringUtils.isBlank(redirectUrl)) {
								redirectUrl = StringUtils.EMPTY;
							} else {
								redirectUrl = redirectUrl + GhixPlatformEndPoints.LOGIN_PAGE;
							}
							messageTokens.put(REDIRECT_URL, redirectUrl);
						}
						break;
					case DISCLAIMER_CONTENT : 
						if(affiliateId != null || tenantId != null) {
							String disclaimerContent = configurationService.getDisclaimerTextForEmailFooter(flowId,affiliateId, tenantId);
							if(StringUtils.isBlank(disclaimerContent)) {
								disclaimerContent = StringUtils.EMPTY;
							} 
							messageTokens.put(DISCLAIMER_CONTENT, disclaimerContent);
						}
						break;	
					}
				}
			}
		}
	}


	/**
	 * send email out to external email ids
	 *  
	 * @param noticeObj
	 * @return updated notice object with status
	 * 
	 */
	public Notice sendEmail(Notice noticeObj) {

		try{
				emailService.dispatch(noticeObj);
				noticeObj.setSentDate(new Date ());
				noticeObj.setStatus(STATUS.EMAIL_SENT);
			
		}catch (Exception e) {
			LOGGER.error(EMAIL_SENDING_FAILED+ e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}

		return emailNotificationHelper.updateNotice(noticeObj);
	}
	/**
	 * 
	 * NOTE : Use this New "Send Email" Method with Email tracking capability
	 *
	 * send email out to external email ids
	 *  
	 * @param noticeObj
	 * @return updated notice object with status
	 * 
	 */
	public Notice sendEmail(Notice noticeObj,Map<GhixUtils.EMAIL_STATS, String> emailStatData) {

		try{
			emailService.dispatch(noticeObj,emailStatData);
			noticeObj.setSentDate(new Date ());
			noticeObj.setStatus(STATUS.EMAIL_SENT);
		}catch (Exception e) {
			LOGGER.error(EMAIL_SENDING_FAILED+ e.getMessage(),e);
			noticeObj.setStatus(STATUS.FAILED);
		}

		return emailNotificationHelper.updateNotice(noticeObj);
	}
	
	 public boolean sendEmailRequest(Notice notice) {
	    	boolean success = false;
	    	notice.setStatus(STATUS.PENDING);
	    	HttpPost post = null;
			try {
				post = new HttpPost(new URI(GhixPlatformConstants.REMOTE_NOTIFICATION_URL));
				HttpEntity payload = null;
				if(GhixPlatformConstants.INSECURE_AHBX_SEND) {
					if(!GhixPlatformConstants.REMOTE_NOTIFICATION_URL.startsWith("https:")) {
						LOGGER.warn("********* Insecure URL {} user for remote send and no encryption used ***********",GhixPlatformConstants.REMOTE_NOTIFICATION_URL);
					}
					payload = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(notice.getEmailBody()).build();
					post.setEntity(payload);
				}else {
					payload = EntityBuilder.create()
					.setContentType(ContentType.TEXT_PLAIN)
					.setText(GhixAESCipherPool.encrypt(notice.getEmailBody())).build();
					post.setEntity(payload);
				}
				
				CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
				int status = response.getStatusLine().getStatusCode();
				String responseMsg = Utils.getResponseString(response.getEntity());
				if(HttpStatus.SC_OK != status){
					notice.setStatus(STATUS.FAILED);
				}else{
					notice.setStatus(STATUS.EMAIL_SENT);
					success= true;
				}
				this.updateResponseInNoticeBody(notice, responseMsg);
				response.close();
			} catch (Exception e) {
				this.updateResponseInNoticeBody(notice, e.getMessage());
				notice.setStatus(STATUS.FAILED);
				success = false;
			}finally {
				if(post != null) {
					post.releaseConnection();
				}
			}
	    return success;
		}
	 
	 	private void updateResponseInNoticeBody(Notice noticeObj,String message) {
	    	String emailBody = noticeObj.getEmailBody();
	    	if(this.parser == null) {
	    		this.parser = new JSONParser();
	    	}
	    	if(emailBody != null) {
	    		try {
	    			JSONObject obj = (JSONObject) parser.parse(emailBody);
					obj.put("BASE64_RESPONSE".intern(), Base64.getEncoder().encodeToString(message.getBytes()));
				} catch (Exception e) {
					LOGGER.error("Email body is not is json format and remote send attempted, please check the configuration if remote send should be enabled", e);
				}
	    		
	    	}
			
		}

}
