package com.getinsured.hix.platform.affiliate.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.enums.AffiliateAncillary;
import com.getinsured.affiliate.enums.AffiliateAncillaryStatus;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.affiliate.model.AffiliateFlow.WindowConfig;
import com.getinsured.hix.model.CampaignDTO;
import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.model.ProductConfigurationDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.service.TenantService;

@Component
public class AffiliateFlowConfigServiceImpl implements AffiliateFlowConfigService {
	
	private static final String STM_CODE = "STM"; 
	private static final String HEALTH_CODE = "HLT"; 
	private static final String DENTAL_CODE = "DEN"; 
	private static final String ACCIDENT_CODE = "AME"; 
	private static final String VISION_CODE = "VSN"; 
	private static final String MEDICARE_CODE = "MDR";
	private static final String AFFILIATE_LOGO = "affiliateLogo";
	private static final String AFFILIATE_THEME = "affiliateTheme";
	private static final String AFFILIATE_FAVICON = "affiliateFavicon";
	private static final String AFFILIATE_BRANDING_DATA = "affiliateBrandingData";
	private static final String AFFILIATE_HERO_IMAGE = "affiliateHeroImage";
	private static final String AFFILIATE_HAS_EMPLOYER = "affiliateHasEmployer";
	private static final String AFFILIATE_CONFIG = "affiliateConfig";
	private static final String FLOW_ID = "ClickTrackAffiliateFlowId";
	private static final String AFFILIATE_ID = "ClickTrackAffiliateId";
	private static Logger LOGGER = Logger.getLogger(AffiliateFlowConfigServiceImpl.class);
	
	@Autowired
	private TenantService tenantService;
	
	@Override
	public void setFlowConfigurationInSession(HttpServletRequest request, Long affiliateId, AffiliateFlow affFlow, String showCallUsNumber, Object logoExists, String logoUrl, ConfigurationDTO affiliateConfig, Object affiliateHasEmployer) {
		Map<Enum<AffiliateAncillary>,Enum<AffiliateAncillaryStatus>> ancillaryConfig=new HashMap<Enum<AffiliateAncillary>,Enum<AffiliateAncillaryStatus>>();
		String logoCheck;
		logoCheck="notfound";
		if(logoExists != null && !logoExists.toString().equalsIgnoreCase("0"))
		{
			logoCheck = logoExists.toString();
		}
		request.getSession().setAttribute("ClickTrackIVRNumber", affFlow.getIvrNumber());
		// Take affiliateId and flowId from the flow object itself
		request.getSession().setAttribute("ClickTrackAffiliateId", affiliateId);
		request.getSession().setAttribute("ClickTrackAffiliateFlowId", affFlow.getAffiliateflowId());
		if(StringUtils.isNotBlank(logoUrl)) {
			request.getSession().setAttribute("FlowPhoto", logoUrl);
		} else {
			request.getSession().setAttribute("FlowPhoto", logoCheck);
		}
		
		if(affiliateConfig != null) {
			request.getSession().setAttribute("affiliateConfig", affiliateConfig);
			this.setAffiliateConfigurationInSession(request, affiliateId, affiliateHasEmployer, affiliateConfig);
		}
		
		if(affFlow.getAffiliateFlowConfig() != null && 
				affFlow.getAffiliateFlowConfig().getUiConfiguration() != null && 
				affFlow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration() != null && 
				StringUtils.isNotBlank(affFlow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration().getLogoRedirectUrl())) {
			request.getSession().setAttribute("flowLogoRedirectUrl", affFlow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration().getLogoRedirectUrl());
		}
		
		if(affFlow.getAffiliateFlowConfig() != null && 
				affFlow.getAffiliateFlowConfig().getUiConfiguration() != null && 
				affFlow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration() != null && 
				StringUtils.isNotBlank(affFlow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration().getBrandingDataUrl())) {
			request.getSession().setAttribute("flowBrandingDataUrl", affFlow.getAffiliateFlowConfig().getUiConfiguration().getBrandingConfiguration().getBrandingDataUrl());
		}
		
		if(affFlow.getAffiliateFlowConfig() != null && 
				affFlow.getAffiliateFlowConfig().getUiConfiguration() != null && 
				StringUtils.isNotBlank(affFlow.getAffiliateFlowConfig().getUiConfiguration().getGaAnalyticsTrackingCode())) {
			request.getSession().setAttribute("flowGaAnalyticsTrackingCode", affFlow.getAffiliateFlowConfig().getUiConfiguration().getGaAnalyticsTrackingCode());
		}
		request.getSession().setAttribute("affFlowCustomField1", affFlow.getCustomField1());
		request.getSession().setAttribute("WidgetLogo", affFlow.getCustomField4());
		
		// Set affFlowCustomCSS to ON/OFF based on the custom field1 Config
		request.getSession().setAttribute("affFlowCustomCSS",
				affFlow.getCustomField1() == null? AffiliateAncillaryStatus.OFF.getDescription() : affFlow.getCustomField1());
		
		// Set affiliateMasthead to ON/OFF based on the customField2 Config
		request.getSession().setAttribute("affiliateMasthead",
				affFlow.getCustomField2() == null? AffiliateAncillaryStatus.OFF.getDescription() : affFlow.getCustomField2());
		
		//Affiliate Template Urls 
		request.getSession().setAttribute("affFlowOnExchRedirectionUrlTemplate", affFlow.getOnExchgRedirectionUrl());
		request.getSession().setAttribute("affFlowOffExchRedirectionUrlTemplate", affFlow.getOffExchgRedirectionUrl());
		request.getSession().setAttribute("affFlowMedicareRedirectionUrlTemplate", affFlow.getMedicareRedirectionUrl());
		request.getSession().setAttribute("affFlowMedicaidRedirectionUrlTemplate", affFlow.getMedicaidRedirectionUrl());
		request.getSession().setAttribute("affFlowSTMRedirectionUrl", affFlow.getShortTermMedicalRedirectionUrl());
		
		// Redirect Window configurations
		request.getSession().setAttribute("affFlowOnExchRedirectionInSameWindow",
				affFlow.getOnExchSelfWindow() == null? WindowConfig._blank : affFlow.getOnExchSelfWindow().toString());
		request.getSession().setAttribute("affFlowOffExchRedirectionInSameWindow",
				affFlow.getOffExchSelfWindow() == null? WindowConfig._blank  : affFlow.getOffExchSelfWindow().toString());
				
		request.getSession().setAttribute("affFlowMedicareRedirectionInSameWindow", 
				affFlow.getMedicareSelfWindow()==null?WindowConfig._blank :affFlow.getMedicareSelfWindow().toString());
		request.getSession().setAttribute("affFlowMedicaidRedirectionInSameWindow", 
				affFlow.getMedicaidSelfWindow()==null?WindowConfig._blank:affFlow.getMedicaidSelfWindow().toString());
		request.getSession().setAttribute("affFlowSTMRedirectionInSameWindow", 
				affFlow.getStmSelfWindow()==null?WindowConfig._blank:affFlow.getStmSelfWindow().toString());
		
		//Ancillary Config Map
		ancillaryConfig.put(AffiliateAncillary.STM,affFlow.getAncillarySTM() != null ? affFlow.getAncillarySTM() : AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.AME,affFlow.getAncillaryAME() != null ? affFlow.getAncillaryAME() : AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.DENTAL,affFlow.getAncillaryDental() != null ? affFlow.getAncillaryDental() : AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.VISION,affFlow.getAncillaryVision() != null ? affFlow.getAncillaryVision() : AffiliateAncillaryStatus.OFF);
		
		request.getSession().setAttribute("ancillaryConfig",ancillaryConfig);
		
		if(affFlow.getGpsPreferencePopUp() != null) {
			request.getSession().setAttribute("affiliateGpsOnOff",affFlow.getGpsPreferencePopUp().getDescription());
		}

		if(affFlow.getAffiliateMedicare() == null) {
			affFlow.setAffiliateMedicare(AffiliateAncillaryStatus.OFF);
		}

		if(affFlow.getAffiliateMedicare() != null){
			request.getSession().setAttribute("medicareConfig",affFlow.getAffiliateMedicare().getDescription());
		}

		request.getSession().setAttribute("affFlowSTMRedirectionUrl", affFlow.getShortTermMedicalRedirectionUrl());
		
		//HIX-58669: Set the config flag in session which will allow showing/hiding Call Us numbers on our website.
		if(StringUtils.isNotBlank(showCallUsNumber)){
			request.getSession().setAttribute("showCallUsNumber", showCallUsNumber);
		}
		
		if (affFlow.getAffiliateFlowConfig() != null) {
			CampaignDTO campaignDTO = affFlow.getAffiliateFlowConfig().getCampaignConfigurations();
			if (campaignDTO != null && StringUtils.isNotEmpty(campaignDTO.getCampaignId())) {
				request.getSession().setAttribute("campaignId",campaignDTO.getCampaignId());
			}
		}
		request.getSession().setAttribute("visionOnOff", DynamicPropertiesUtil.getPropertyValue("planSelection.VisionOnOff"));
	}
	 
	public void setAncillaryConfigInSession(
			final HttpServletRequest httpRequest,
			ConfigurationDTO tenantConfiguration,
			ConfigurationDTO affiliateConfiguration) {
		
		Map<Enum<AffiliateAncillary>,Enum<AffiliateAncillaryStatus>> ancillaryConfig=new HashMap<Enum<AffiliateAncillary>,Enum<AffiliateAncillaryStatus>>();
		String medicareConfig = AffiliateAncillaryStatus.OFF.getDescription();
		ancillaryConfig.put(AffiliateAncillary.HEALTH,AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.STM,AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.DENTAL,AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.VISION,AffiliateAncillaryStatus.OFF);
		ancillaryConfig.put(AffiliateAncillary.AME,AffiliateAncillaryStatus.OFF);
		Set<String> selectedProductSet = new HashSet<>();
		if(null != tenantConfiguration && 
				null != tenantConfiguration.getProductConfigurations() && 
				!tenantConfiguration.getProductConfigurations().isEmpty()) {
			for (ProductConfigurationDTO product : tenantConfiguration.getProductConfigurations()) {
				if(BooleanUtils.isTrue(product.getSelected())) {
					selectedProductSet.add(product.getCode());
				}
			}
		}
		
		if(affiliateConfiguration != null) {
			List<ProductConfigurationDTO> productConfigurations = affiliateConfiguration.getProductConfigurations();
			if(productConfigurations != null && !productConfigurations.isEmpty()) {
				for (ProductConfigurationDTO product : productConfigurations) {
					if(BooleanUtils.isFalse(product.getSelected()) && selectedProductSet.contains(product.getCode())) {
						selectedProductSet.remove(product.getCode());
					}
				}
			}
		}
		
		LOGGER.debug("Tenant has the following products enabled: " + selectedProductSet);
		if(selectedProductSet.contains(HEALTH_CODE)){
			ancillaryConfig.put(AffiliateAncillary.HEALTH,AffiliateAncillaryStatus.ON);
		}
		if(selectedProductSet.contains(STM_CODE)){
			ancillaryConfig.put(AffiliateAncillary.STM,AffiliateAncillaryStatus.ON);
		}
		if(selectedProductSet.contains(MEDICARE_CODE)){
			medicareConfig = AffiliateAncillaryStatus.ON.getDescription();
		}
		if(selectedProductSet.contains(DENTAL_CODE)){
			ancillaryConfig.put(AffiliateAncillary.DENTAL,AffiliateAncillaryStatus.ON);
		}
		if(selectedProductSet.contains(VISION_CODE)){
			ancillaryConfig.put(AffiliateAncillary.VISION,AffiliateAncillaryStatus.ON);
		}
		if(selectedProductSet.contains(ACCIDENT_CODE)){
			ancillaryConfig.put(AffiliateAncillary.AME,AffiliateAncillaryStatus.ON);
		}
		
		httpRequest.getSession().setAttribute("ancillaryConfig",ancillaryConfig);
		httpRequest.getSession().setAttribute("medicareConfig",medicareConfig);
	}

	public void setAffiliateConfigurationInSession(final HttpServletRequest httpRequest, Long affiliateId, Object affiliateHasEmployer, ConfigurationDTO affiliateConfig) {
		httpRequest.getSession().setAttribute(AFFILIATE_ID, affiliateId);
		if (null != affiliateConfig.getUiConfiguration()
				&& null != affiliateConfig.getUiConfiguration().getBrandingConfiguration()) {

		httpRequest.getSession().setAttribute(AFFILIATE_LOGO,affiliateConfig.getUiConfiguration().getBrandingConfiguration().getLogoUrl());
		httpRequest.getSession().setAttribute(AFFILIATE_THEME, affiliateConfig.getUiConfiguration().getBrandingConfiguration().getThemeUrl());
		httpRequest.getSession().setAttribute(AFFILIATE_FAVICON, affiliateConfig.getUiConfiguration().getBrandingConfiguration().getFaviconUrl());
		httpRequest.getSession().setAttribute(AFFILIATE_BRANDING_DATA, affiliateConfig.getUiConfiguration().getBrandingConfiguration().getBrandingDataUrl());
		httpRequest.getSession().setAttribute(AFFILIATE_HERO_IMAGE, affiliateConfig.getUiConfiguration().getBrandingConfiguration().getHeroImageUrl());
		}
		httpRequest.getSession().setAttribute(AFFILIATE_HAS_EMPLOYER, affiliateHasEmployer);
		
		httpRequest.getSession().setAttribute(AFFILIATE_CONFIG, affiliateConfig);
		TenantContextHolder.setAffiliateId(affiliateId);
		setFlowIdIfNotSetAlready(httpRequest, affiliateId);
	}
	
	public void setFlowIdIfNotSetAlready(HttpServletRequest httpRequest, Long affiliateId) {
		if(httpRequest.getSession().getAttribute(FLOW_ID) == null) {
			AffiliateFlow flow = tenantService.getDefaultFlowForAffiliate(affiliateId);
			if(flow != null) {
				setFlowConfigurationInSession(httpRequest, flow.getAffiliate().getAffiliateId(), flow, null, null, flow.getLogoURL(), null, flow.getAffiliate().getHasEmployer());
			}
		}
	}
}
