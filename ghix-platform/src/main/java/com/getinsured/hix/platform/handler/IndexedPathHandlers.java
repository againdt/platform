package com.getinsured.hix.platform.handler;

import java.util.HashMap;
import java.util.List;

public class IndexedPathHandlers {
	
	private HashMap<Integer, IndexedRequestHandler> handlersMap;
	private String path = null;
	
	public IndexedPathHandlers(List<IndexedRequestHandler> handlers) throws InvalidHandlerIndexException{
		if(handlers != null && handlers.size() > 0){
			int idx = -1;
			String handlerPath = null;
			for(IndexedRequestHandler handler:handlers ){
				if(path == null){
					path = handler.getHandlerPath();
				}
				idx = handler.getHandlerIndex();
				if(this.handlersMap.containsKey(idx)){
					throw new InvalidHandlerIndexException("Index:"+idx+" already added, duplicate indexes are not allowed");
				}
				handlerPath = handler.getHandlerPath();
				if(!path.equals(handlerPath)){
					throw new InvalidHandlerIndexException("Invalid path received "+handlerPath+" Single instance of IndexedRequestHandlersIterator will accept the same path, already initialized for:"+path);
				}
				handlersMap.put(idx, handler);
			}
		}
	}
	
	public IndexedPathHandlers() {
		
	}
	/**
	 * Adds a an indexed handler, index returned by this handler should be unique otherwise
	 * This call with throw @InvalidHandlerException. 
	 * All the handlers added to this instance must return the same path otherwise this function will 
	 * throw exception as well.
	 * @param handler
	 * @return
	 * @throws InvalidHandlerIndexException
	 */
	public int addHandler(IndexedRequestHandler handler) throws InvalidHandlerIndexException{
		if(this.path == null){
			this.path = handler.getHandlerPath();
		}
		String hPath = handler.getHandlerPath();
		if(!this.path.equals(hPath)){
			throw new InvalidHandlerIndexException("Invalid path received "+hPath+" Single instance of IndexedRequestHandlersIterator will accept the same path, already initialized for:"+path);
		}
		int idx = handler.getHandlerIndex();
		if(idx < 0){
			throw new InvalidHandlerIndexException("Invalid index received "+idx+" for path:"+path);
		}
		if(this.handlersMap == null){
			this.handlersMap = new HashMap<Integer, IndexedRequestHandler>();
		}
		this.handlersMap.put(idx, handler);
		return this.handlersMap.size();
	}
	
	/**
	 * Returns the path served by this handler
	 * @return
	 */
	public String getHandlerPath(){
		return this.path;
	}
	
	/**
	 * Returns NULL if given index is not available
	 * @param index
	 * @return
	 */
	public IndexedRequestHandler getHandler(int index){
		return this.handlersMap.get(index);
	}

	/**
	 * Returns the count of handlers available for this path
	 * @return
	 */
	public int getHandlersCount(){
		return this.handlersMap.size();
	}
}
