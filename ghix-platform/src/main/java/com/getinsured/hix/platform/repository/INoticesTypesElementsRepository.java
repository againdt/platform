package com.getinsured.hix.platform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.NoticesTypesElements;

public interface INoticesTypesElementsRepository extends JpaRepository<NoticesTypesElements, Integer>{

	List<NoticesTypesElements> findByNoticeType(NoticeType noticeType);

}
