//package org.springframework.security.web.authentication.preauth;
package com.getinsured.hix.platform.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


public class GhixUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    
	@Override
	 public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
	            throws IOException, ServletException {
		
		super.doFilter(req, res, chain);
		
		
	}
	
	@Override
	   protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
			
			
			/*Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
			
			if(currentUser==null){
				return true;
			}
		
			return false;*/
	        String uri = request.getRequestURI();
	        int pathParamIndex = uri.indexOf(';');

	        if (pathParamIndex > 0) {
	            // strip everything after the first semi-colon
	            uri = uri.substring(0, pathParamIndex);
	        }
	        String filterProcessesUrl= getFilterProcessesUrl();

	        if ("".equals(request.getContextPath())) {
	            return uri.endsWith(filterProcessesUrl);
	        }

	        return uri.endsWith(request.getContextPath() + filterProcessesUrl);
	    }
	

}