package com.getinsured.hix.platform.config;

public class PlanDisplayConfiguration {

	public enum PlanDisplayConfigurationEnum implements PropertiesEnumMarker
	{  
		QUALITY_SECTION("planSelection.QualitySection"),
		
		SPLIT_HOUSEHOLD("planSelection.SplitHousehold"),
		
		PROVIDERS("planSelection.Providers"),
		
		DOCTOR_SECTION("planSelection.DoctorSection"),
		
		FACILITY_SECTION("planSelection.FacilitySection"),
		
		DENTIST_SECTION("planSelection.DentistSection"),
		
		QUALITY_RATING("planSelection.QualityRating"),
		
		PROVIDERS_LINK("planSelection.ProvidersLink"),
		
		MULTIPLE_ITEMS_ENTRY("planSelection.MultipleItemsEntry"),
		
		MIN_PREMIUM_PER_MEMBER("planSelection.MinPremiumPerMember"),
		
		ESTIMATE_COST("preferences.EstimateCost"),
		
		FIND_DOCTOR("preferences.FindDoctor"),
		
		SHOP_DENTAL("preferences.ShopDental"),
		
		FIND_DENTIST("preferences.FindDentist"),
		
		DENTAL_PLAN_SELECTION("planSelection.DentalPlanSelection"),
		
		DOCTOR_SEARCH_AA_OFF_EXCHANGE("planSelection.AA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AE_OFF_EXCHANGE("planSelection.AE.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AK_OFF_EXCHANGE("planSelection.AK.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AL_OFF_EXCHANGE("planSelection.AL.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AP_OFF_EXCHANGE("planSelection.AP.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AR_OFF_EXCHANGE("planSelection.AR.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AS_OFF_EXCHANGE("planSelection.AS.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_AZ_OFF_EXCHANGE("planSelection.AZ.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_CA_OFF_EXCHANGE("planSelection.CA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_CO_OFF_EXCHANGE("planSelection.CO.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_CT_OFF_EXCHANGE("planSelection.CT.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_DC_OFF_EXCHANGE("planSelection.DC.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_DE_OFF_EXCHANGE("planSelection.DE.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_FL_OFF_EXCHANGE("planSelection.FL.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_FM_OFF_EXCHANGE("planSelection.FM.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_GA_OFF_EXCHANGE("planSelection.GA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_GU_OFF_EXCHANGE("planSelection.GU.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_HI_OFF_EXCHANGE("planSelection.HI.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_IA_OFF_EXCHANGE("planSelection.IA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_ID_OFF_EXCHANGE("planSelection.ID.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_IL_OFF_EXCHANGE("planSelection.IL.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_IN_OFF_EXCHANGE("planSelection.IN.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_KS_OFF_EXCHANGE("planSelection.KS.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_KY_OFF_EXCHANGE("planSelection.KY.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_LA_OFF_EXCHANGE("planSelection.LA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MA_OFF_EXCHANGE("planSelection.MA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MD_OFF_EXCHANGE("planSelection.MD.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_ME_OFF_EXCHANGE("planSelection.ME.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MH_OFF_EXCHANGE("planSelection.MH.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MI_OFF_EXCHANGE("planSelection.MI.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MN_OFF_EXCHANGE("planSelection.MN.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MO_OFF_EXCHANGE("planSelection.MO.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MP_OFF_EXCHANGE("planSelection.MP.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MS_OFF_EXCHANGE("planSelection.MS.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_MT_OFF_EXCHANGE("planSelection.MT.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NC_OFF_EXCHANGE("planSelection.NC.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_ND_OFF_EXCHANGE("planSelection.ND.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NE_OFF_EXCHANGE("planSelection.NE.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NH_OFF_EXCHANGE("planSelection.NH.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NJ_OFF_EXCHANGE("planSelection.NJ.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NM_OFF_EXCHANGE("planSelection.NM.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NV_OFF_EXCHANGE("planSelection.NV.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_NY_OFF_EXCHANGE("planSelection.NY.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_OH_OFF_EXCHANGE("planSelection.OH.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_OK_OFF_EXCHANGE("planSelection.OK.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_OR_OFF_EXCHANGE("planSelection.OR.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_PA_OFF_EXCHANGE("planSelection.PA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_PR_OFF_EXCHANGE("planSelection.PR.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_PW_OFF_EXCHANGE("planSelection.PW.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_RI_OFF_EXCHANGE("planSelection.RI.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_SC_OFF_EXCHANGE("planSelection.SC.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_SD_OFF_EXCHANGE("planSelection.SD.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_TN_OFF_EXCHANGE("planSelection.TN.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_TX_OFF_EXCHANGE("planSelection.TX.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_UT_OFF_EXCHANGE("planSelection.UT.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_VA_OFF_EXCHANGE("planSelection.VA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_VI_OFF_EXCHANGE("planSelection.VI.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_VT_OFF_EXCHANGE("planSelection.VT.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_WA_OFF_EXCHANGE("planSelection.WA.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_WI_OFF_EXCHANGE("planSelection.WI.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_WV_OFF_EXCHANGE("planSelection.WV.DOCTOR_SEARCH_OFF_EXCHANGE"),

		DOCTOR_SEARCH_WY_OFF_EXCHANGE("planSelection.WY.DOCTOR_SEARCH_OFF_EXCHANGE"),
		
		DOCTOR_SEARCH_AA_ON_EXCHANGE("planSelection.AA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AE_ON_EXCHANGE("planSelection.AE.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AK_ON_EXCHANGE("planSelection.AK.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AL_ON_EXCHANGE("planSelection.AL.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AP_ON_EXCHANGE("planSelection.AP.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AR_ON_EXCHANGE("planSelection.AR.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AS_ON_EXCHANGE("planSelection.AS.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_AZ_ON_EXCHANGE("planSelection.AZ.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_CA_ON_EXCHANGE("planSelection.CA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_CO_ON_EXCHANGE("planSelection.CO.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_CT_ON_EXCHANGE("planSelection.CT.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_DC_ON_EXCHANGE("planSelection.DC.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_DE_ON_EXCHANGE("planSelection.DE.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_FL_ON_EXCHANGE("planSelection.FL.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_FM_ON_EXCHANGE("planSelection.FM.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_GA_ON_EXCHANGE("planSelection.GA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_GU_ON_EXCHANGE("planSelection.GU.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_HI_ON_EXCHANGE("planSelection.HI.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_IA_ON_EXCHANGE("planSelection.IA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_ID_ON_EXCHANGE("planSelection.ID.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_IL_ON_EXCHANGE("planSelection.IL.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_IN_ON_EXCHANGE("planSelection.IN.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_KS_ON_EXCHANGE("planSelection.KS.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_KY_ON_EXCHANGE("planSelection.KY.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_LA_ON_EXCHANGE("planSelection.LA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MA_ON_EXCHANGE("planSelection.MA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MD_ON_EXCHANGE("planSelection.MD.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_ME_ON_EXCHANGE("planSelection.ME.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MH_ON_EXCHANGE("planSelection.MH.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MI_ON_EXCHANGE("planSelection.MI.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MN_ON_EXCHANGE("planSelection.MN.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MO_ON_EXCHANGE("planSelection.MO.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MP_ON_EXCHANGE("planSelection.MP.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MS_ON_EXCHANGE("planSelection.MS.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_MT_ON_EXCHANGE("planSelection.MT.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NC_ON_EXCHANGE("planSelection.NC.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_ND_ON_EXCHANGE("planSelection.ND.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NE_ON_EXCHANGE("planSelection.NE.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NH_ON_EXCHANGE("planSelection.NH.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NJ_ON_EXCHANGE("planSelection.NJ.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NM_ON_EXCHANGE("planSelection.NM.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NV_ON_EXCHANGE("planSelection.NV.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_NY_ON_EXCHANGE("planSelection.NY.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_OH_ON_EXCHANGE("planSelection.OH.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_OK_ON_EXCHANGE("planSelection.OK.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_OR_ON_EXCHANGE("planSelection.OR.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_PA_ON_EXCHANGE("planSelection.PA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_PR_ON_EXCHANGE("planSelection.PR.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_PW_ON_EXCHANGE("planSelection.PW.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_RI_ON_EXCHANGE("planSelection.RI.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_SC_ON_EXCHANGE("planSelection.SC.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_SD_ON_EXCHANGE("planSelection.SD.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_TN_ON_EXCHANGE("planSelection.TN.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_TX_ON_EXCHANGE("planSelection.TX.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_UT_ON_EXCHANGE("planSelection.UT.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_VA_ON_EXCHANGE("planSelection.VA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_VI_ON_EXCHANGE("planSelection.VI.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_VT_ON_EXCHANGE("planSelection.VT.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_WA_ON_EXCHANGE("planSelection.WA.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_WI_ON_EXCHANGE("planSelection.WI.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_WV_ON_EXCHANGE("planSelection.WV.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		DOCTOR_SEARCH_WY_ON_EXCHANGE("planSelection.WY.DOCTOR_SEARCH_ON_EXCHANGE"),
		
		
		AME_PLAN_SELECTION("planSelection.AMEOnOff"),
		
		VISION_PLAN_SELECTION("planSelection.VisionOnOff"),
		
		SHOW_OOP("planSelection.ShowOop"),
		
		SHOW_STM("planSelection.StmOnOff"),
		
		SHOW_CROSS_SELL("planSelection.CrossSellOnOff"),
		
		SHOW_CROSS_SELL_LIGHTBOX("planSelection.LightBoxOnOff"),
		
		SHOW_GPS_TOOLTIP("planSelection.GPSTooltipOnOff"),
		
		SHOW_PUF_PREMIUM("planSelection.ShowPufPremium"),
		
		SAVE_SHOPPING_SESSION_ENABLED("planSelection.saveShoppingSession.Enabled"),
		
		PLAN_SELECTION_BUNDLING_CONF("planSelection.BundlingOnOff"),
		
		AGENT_GPS_VERSION("planSelection.AgentGpsVersion"),
		
		SHOW_STM_BUNDLING("planSelection.StmBundlingOnOff"),
		
		DENTAL_GUARANTEED("planSelection.DentalGuaranteed"),
		
		CATCH_ALL_LIGHTBOX_INDIVIDUAL("planSelection.catchAllLightBoxIndividual"),
		
		CATCH_ALL_LIGHTBOX_SHOP("planSelection.catchAllLightBoxShop"),
		
		DEFAULT_SORT("planSelection.DefaultSort"),
		
		SHOW_QUALITY_RATING_EMPLOYEE("planSelection.showQualityRatingEmployee"),
		
		ADMIN_GPS_CONFIGURATION("planSelection.GpsVersion"),
		
		SELECT_DENTAL_ONLY("planSelection.selectDentalOnly"),
		
		ANONYMOUS_APPLY_URL("planSelection.anonymousApplyURL"),
		
		OFFER_LIFE_INSURANCE("planSelection.OfferLifeInsurance"),
		
		SIMPLIFIED_DEDUCTIBLE("planSelection.simplifiedDeductible"),
		
		DENTAL_BUNDLING_CONFIGURATION("planSelection.dentalBundlingOnOff"),
		
		VISION_BUNDLING_CONFIGURATION("planSelection.visionBundlingOnOff"),
		
		AME_BUNDLING_CONFIGURATION("planSelection.ameBundlingOnOff"),
		
		ENABLE_ENROLLED_PLAN_COMPARISON("planSelection.comparison.showPrevYearEnrolledPlan");

		private final String value;	  
		
		public String getValue(){return this.value;}
		
		PlanDisplayConfigurationEnum(String value){
	        this.value = value;
	    }
	};
	
}
