package com.getinsured.hix.platform.couchbase.dto;

import java.util.Date;

import com.getinsured.hix.platform.couchbase.helper.BucketMetaHelper;
import com.google.gson.annotations.Expose;

public class CouchMeta {

	@Expose
	private String category;
	@Expose
	private String subCategory;
	@Expose
	private String createdBy;
	@Expose
	private String modifiedBy;
	@Expose
	private Date creationDate;
	@Expose
	private Date modifiedDate;
	@Expose
	private String tenantCode;
	@Expose
	private String branch;
	@Expose
	private String env;
	@Expose
	private String customKey;
	
	public CouchMeta() {
		this.branch = BucketMetaHelper.getBuildArtifactVersion();
		this.env = BucketMetaHelper.getEnv();
		this.tenantCode = BucketMetaHelper.getTenantCode();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getCustomKey() {
		return customKey;
	}

	public void setCustomKey(String customKey) {
		this.customKey = customKey;
	}

}
