package com.getinsured.hix.platform.util;

// Phone Util class consisiting of the generic functions of a phone number
public class PhoneUtil {

	/* this method takes string as input 
	 * @param phone 
	 * and returns the parsed phone number in the format (###) ### ####
	 */  
	public static String formatPhoneNumber(String phone) {  
		String formatPhone = "";
		if((phone!=null && !phone.isEmpty())){
			String phoneDigits = getDigits(phone);
			if(phoneDigits.length()>6){
				formatPhone = String.format("(%s) %s %s", phoneDigits.substring(0, 3),phoneDigits.subSequence(3, 6),phoneDigits.substring(6));
			}else {
				return phoneDigits; 
			}
		}
		return formatPhone;  
	 }  
	
	/* getDigits takes string as input 
	 * @param phoneStr
	 * and returns the String as digits from the input string. 
	*/
	public static String getDigits(String phoneStr)
	  {
		  int length = phoneStr.length();
		  StringBuffer buffer = new StringBuffer(length);
	      for(int i = 0; i < length; i++) {
	          char ch = phoneStr.charAt(i);
	          if (Character.isDigit(ch)) {
	          buffer.append(ch);
		     }
		  }
		   return buffer.toString();
	 }
	
	public static String formatElevenDigitPhoneNumber(String phone) {  
		String formatPhone = "";
		if((phone!=null && !phone.isEmpty())){
			String phoneDigits = getDigits(phone);
			if(phoneDigits.length()>6){
				if(phoneDigits.length()==11){
					if(phone.contains("-")){
						formatPhone = String.format("%s(%s)%s-%s", phoneDigits.charAt(0),phoneDigits.substring(1, 4),phoneDigits.subSequence(4, 7),phoneDigits.substring(7));
					}else{
						formatPhone = String.format("%s(%s)%s %s", phoneDigits.charAt(0),phoneDigits.substring(1, 4),phoneDigits.subSequence(4, 7),phoneDigits.substring(7));
					}
				}else{
					if(phone.contains("-")){
						formatPhone = String.format("%s-%s-%s", phoneDigits.substring(0, 3),phoneDigits.subSequence(3, 6),phoneDigits.substring(6));
					}else{
						formatPhone = String.format("(%s) %s %s", phoneDigits.substring(0, 3),phoneDigits.subSequence(3, 6),phoneDigits.substring(6));
					}
				}
			}else {
				return phoneDigits; 
			}
		}
		return formatPhone;  
	 } 
	
}
