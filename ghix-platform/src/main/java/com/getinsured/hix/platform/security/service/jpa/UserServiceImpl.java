/**
 * Service implementation for user management
 * @author venkata_tadepalli
 * @since 11/30/2012
 */
package com.getinsured.hix.platform.security.service.jpa;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GhixNotificationType;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.Role.IS_EXCLUSIVE;
import com.getinsured.hix.model.SecurableTarget;
import com.getinsured.hix.model.Securables;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.VimoEncryptor;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.handler.HandlerConstants;
import com.getinsured.hix.platform.handler.RequestHandler;
import com.getinsured.hix.platform.handler.RequestHandlerFactory;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.notification.EmailService;
import com.getinsured.hix.platform.security.RemoteValidationFailedException;
import com.getinsured.hix.platform.security.RoleAccessControl;
import com.getinsured.hix.platform.security.SecurityUtils;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.IModuleUserRepository;
import com.getinsured.hix.platform.security.repository.ISecurableTargetRepository;
import com.getinsured.hix.platform.security.repository.ISecurablesRepository;
import com.getinsured.hix.platform.security.repository.ITenantUnawareUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.GiUserAudit;
import com.getinsured.hix.platform.security.service.PasswordService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.um.handlers.PasswordHandler;
import com.getinsured.hix.platform.um.handlers.PreUserCreateHandler;
import com.getinsured.hix.platform.um.handlers.UpdateUserHandler;
import com.getinsured.hix.platform.useraud.repository.UsersAudRepository;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.platform.util.exception.WSO2Exception;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Service("userService")
public class UserServiceImpl implements UserService, ApplicationContextAware {
	private static final char ACTIVE_ROLE = 'Y';
	
	private static final int USER_SIZE = 1;

	private static final String ACCOUNT_USER_MODEL = "com.getinsured.hix.model.AccountUser";


	private static final String COLUMN_PASSWORD = "password";

	private static final String COLUMN_EMAIL = "email";

	private static final String COLUMN_ID = "id";

	private static final String COLUMN_UPDATED = "updated";

	private static final String COLUMN_CREATED = "created";
	
	public static final String RESET_CONTROLLER_URL = "account/user/forgotpassword";

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired  
	private GiUserAudit userAudit;
	@Autowired
	private SCIMUserManager scimUserManager;
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private ISecurablesRepository iSecurablesRepository;
	@Autowired
	private RoleAccessControl accessControl;
	@Autowired
	private ITenantUnawareUserRepository iTenantUnawareUserRepository;
	
	@Autowired private EmailService emailService;
	
	@Autowired ISecurableTargetRepository iSecurablesTargetRepository;
	@Autowired  UsersAudRepository usersAudRepository;
	/*
	 * HIX-32110 : Modify Change/Reset Password Process to accept salted password
	 */
	@Autowired private PasswordService passwordService;  

	@Autowired
	private IModuleUserRepository moduleUserRepository;

	@SuppressWarnings("rawtypes")
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	
	@Autowired private ObjectFactory<DisplayAuditService> objectFactory;
	
	@Autowired private GhixEncryptorUtil ghixEncryptorUtil;
	
	@Autowired private RequestHandlerFactory handlerFactory;


	private boolean scimEnabled = GhixPlatformConstants.SCIM_ENABLED;
	
	private String exceptionMessage="";

	private ApplicationContext appContext;
	private static final String MESSAGE = "message";

	// Role Specific Repositories
	// @Autowired private IGhixIssuerRepository iGhixIssuerRepository;
	// @Autowired private JpaRepository<Issuer, Integer> iGhixIssuerRepository;

	@Override
	@Transactional(readOnly = true)
	public AccountUser findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	

	
	/**
	 * Returns AccountUser object for the given extnAppUserId from the GI' database.
	 * If not exists then returns null.
	 * HIX-42022 - Implement hybrid authentication (sso and db login)
	 * 
	 * @author venkata tadepalli 
	 * @since  June 30, 2014
	 * @param extnAppUserId
	 * @return AccountUser
	 * 
	 */
	@Override
	@Transactional(readOnly = true)
	public AccountUser findByExtnAppUserId(String extnAppUserId) {
		return userRepository.findByExtnAppUserId(extnAppUserId);
	}
	

	@Override
	@Transactional(readOnly = true)
	public AccountUser findByUserName(String userName) {
		AccountUser user = null;
			try {
				return userRepository.findByUserName(userName);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return user;
	}
	
	/**
	 * if a user primary data source is remote we need to check the external system first
	 */
	@Override
	@Transactional(readOnly = true)
	public AccountUser findByUserName(String userName, boolean remote) throws RemoteValidationFailedException{
		String err = "User with id:"+userName+" not found in external system but is available in DB".intern();
		AccountUser remoteUser = null;
		AccountUser dbuser = null;
		boolean remoteNotAvailable = false;
		if(GhixPlatformConstants.SCIM_ENABLED && remote ){
			try {
				if(LOGGER.isTraceEnabled()){
					LOGGER.trace("Checking if user is available in remote repository");
				}
				remoteUser = this.scimUserManager.findByEmail(userName);
				if(remoteUser == null){
					LOGGER.info("No user with id:"+userName+" found in remote system, check previous logs for more informtion");
					remoteNotAvailable = true;
				}
				if(LOGGER.isTraceEnabled()){
					LOGGER.trace("Checking if user is available in local DB");
				}
				dbuser = userRepository.findByUserName(userName);
				if(dbuser == null && remoteUser != null){
					LOGGER.info("Remote User, not yet provisioned in HIX");
					return remoteUser;
				}
				if(LOGGER.isTraceEnabled()){
					LOGGER.trace("Evaluating if user is deleted from remote system "+((remoteNotAvailable?"not available":"available")+" DB user"+(dbuser == null)));
				}
				if(dbuser != null && remoteNotAvailable){
					LOGGER.info(err);
					throw new RemoteValidationFailedException(err);
				}
			} catch (GIException e) {
				// TODO Auto-generated catch block
				LOGGER.error("Error finding user",e);
			}
		}else{
			if(LOGGER.isTraceEnabled()){
				LOGGER.trace("Extracting user from DB");
			}
			dbuser= userRepository.findByUserName(userName);
		}
		return dbuser;
	}
	
	@Override
	@Transactional(readOnly = true)
	public AccountUser findByUserNameAndEmail(String userName, String email) {
		return userRepository.findByUserNameAndEmail(userName, email);
	}


	@Override
	public List<AccountUser> findAll(){
		List<AccountUser> userList = this.userRepository.findAll();
		return this.accessControl.filterUserListForCurrentUser(userList);
	}

	@Override
	@Transactional
	public AccountUser createLocalUser(AccountUser newUser, String roleName) throws GIException {
		if (newUser == null || roleName == null) {
			throw new GIException("NULL / Blank parameters' data for " 
					+ "->createUser(..) :: accountUser, userRoleName=" + roleName);
		}
		String userRoleName = roleName.toUpperCase();
		// First check if the role exists
		Role newRole = roleService.findRoleByName(userRoleName);
		if (newRole == null) {
				throw new GIException("Invalid User Role ::" + userRoleName + " does not exist in GHIX");
		}
		// Now check if the logged in user can create a user with this role
		if(!this.accessControl.canCreateUserForRole(userRoleName)){
			throw new GIRuntimeException("Permission Denied, current user can not add a user with role:"+userRoleName);
		}
		newUser.setConfirmed(UserService.CONFIRMED);
		RequestHandler handler = this.handlerFactory.getHandlerByName(PreUserCreateHandler.NAME);
		if(handler != null){
			boolean success = false;
			// Create the pre create handling context
			TenantDTO tenantDTO = TenantContextHolder.getTenant();
			HashMap<String, Object> context = new HashMap<>();
			if(tenantDTO != null){
				context.put(HandlerConstants.TENANT_DTO_KEY, tenantDTO);
				context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDTO.isRemotelyManagedUsers());
			}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, false); //Sending false, as it is to be done on local db irrespective of scimEnabled value
			}
			context.put(HandlerConstants.ACCOUNT_USER_KEY, newUser);
			context.put(HandlerConstants.PROVISIONIG_ROLE_KEY, newRole);
			
			success = handler.handleRequest(context, null, false);
			if(!success){
				String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
				String errorException = (String)context.get(HandlerConstants.HANDLER_EXCEPTION);
				LOGGER.error("Pre creation handler failed with error :"+error+" Exception reported:\n"+ errorException);
				return null;
			}
			
			if(tenantDTO != null && tenantDTO.isRemotelyManagedUsers()) {
				String updatedSecAns1 = newUser.getSecurityAnswer1() ;
				if(updatedSecAns1!= null && !updatedSecAns1.isEmpty()) {
					newUser.setSecurityAnswer1(PlatformServiceUtil.getHash(updatedSecAns1.toLowerCase()));
				}
			
			
				String updatedSecAns2 = newUser.getSecurityAnswer2() ;
				if(updatedSecAns2!= null && !updatedSecAns2.isEmpty()) {
					newUser.setSecurityAnswer2(PlatformServiceUtil.getHash(updatedSecAns2.toLowerCase()));
				}
			}
		}

		newUser.setPassword("PASSWORD_NOT_IN_USE".intern());
		newUser.setUuid(null);
		
		if(scimEnabled) {
			AccountUser wso2User = scimUserManager.findByEmail(newUser.getEmail().toLowerCase());
			if(wso2User != null) {
				newUser.setExtnAppUserId(wso2User.getExtnAppUserId());
				newUser.setHasGhixProvisioned(true);
			}
			
		}
		AccountUser newAccountUser = userRepository.save(newUser);
		addUserRole(newAccountUser, newRole);
		LOGGER.info("User provisioning complete");
		return newAccountUser;
	}
	
	/**
	 * Centralized user registration. Creates an entry in to Users, UserRole
	 * tables.
	 * 
	 * @param accountUser
	 *            AccountUser object
	 * @param roleName
	 *            String object
	 * @return AccountUser object
	 * @throws GIException 
	 * @throws Exception
	 */

	@Override
	@Transactional
	public AccountUser createUser(AccountUser newUser, String roleName) throws GIException {
		if (newUser == null || roleName == null) {
			throw new GIException("NULL / Blank parameters' data for " 
					+ "->createUser(..) :: accountUser, userRoleName=" + roleName);
		}
		String userRoleName = roleName.toUpperCase();
		// First check if the role exists
		Role newRole = roleService.findRoleByName(userRoleName);
		if (newRole == null) {
				throw new GIException("Invalid User Role ::" + userRoleName + " does not exist in GHIX");
		}
		// Now check if the logged in user can create a user with this role
		if(!this.accessControl.canCreateUserForRole(userRoleName)){
			throw new GIRuntimeException("Permission Denied, current user can not add a user with role:"+userRoleName);
		}
		newUser.setConfirmed(UserService.CONFIRMED);
		RequestHandler handler = this.handlerFactory.getHandlerByName(PreUserCreateHandler.NAME);
		if(handler != null){
			boolean success = false;
			// Create the pre create handling context
			TenantDTO tenantDTO = TenantContextHolder.getTenant();
			HashMap<String, Object> context = new HashMap<>();
			if(tenantDTO != null){
				context.put(HandlerConstants.TENANT_DTO_KEY, tenantDTO);
				context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDTO.isRemotelyManagedUsers());
			}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
			}
			context.put(HandlerConstants.ACCOUNT_USER_KEY, newUser);
			context.put(HandlerConstants.PROVISIONIG_ROLE_KEY, newRole);
			
			success = handler.handleRequest(context, null, false);
			if(!success){
				String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
				String errorException = (String)context.get(HandlerConstants.HANDLER_EXCEPTION);
				LOGGER.error("Pre creation handler failed with error :"+error+" Exception reported:\n"+ errorException);
				return null;
			}
			
			if(tenantDTO != null && tenantDTO.isRemotelyManagedUsers()) {
				String updatedSecAns1 = newUser.getSecurityAnswer1() ;
				if(updatedSecAns1!= null && !updatedSecAns1.isEmpty()) {
					newUser.setSecurityAnswer1(PlatformServiceUtil.getHash(updatedSecAns1.toLowerCase()));
				}
			
			
				String updatedSecAns2 = newUser.getSecurityAnswer2() ;
				if(updatedSecAns2!= null && !updatedSecAns2.isEmpty()) {
					newUser.setSecurityAnswer2(PlatformServiceUtil.getHash(updatedSecAns2.toLowerCase()));
				}
			}
		}
		
		AccountUser newAccountUser = userRepository.save(newUser);
		addUserRole(newAccountUser, newRole);
		LOGGER.info("User provisioning complete");
		return newAccountUser;
	}

	/**
	 * Saves / Updates the password into user's record.
	 * HIX-32110 : deprecated the function use passwordService.updatePassword
	 * 
	 * @param id
	 *            user id
	 * @param password
	 *            String object
	 * @return void
	 */
	/*@Deprecated
	@Override
	@Transactional
	public void savePassword(int id, String password) {
		AccountUser userObj = null;
		try {
			if (userRepository.exists(id)) {
				userObj = userRepository.findById(id);
				//userObj.setConfirmed(Short.parseShort("1")); // dont set this attribute
				userObj.setPassword(password);
				// reset token...
				userObj.setPasswordRecoveryToken(null);
				userObj.setPasswordRecoveryTokenExpiration(null);
				userObj.setPasswordLastUpdatedTimeStamp(new TSDate());
				userRepository.save(userObj);
			}
		}

		catch (Exception ex) {
			LOGGER.error("User password set up failed, UserId :: " + id, ex);
		}

	}*/

	/**
	 * Adds role to the user. Checks if default Role had defined, if not, sets
	 * the role as default role
	 * 
	 * @param accountUser
	 *            AccountUser object
	 * @param role
	 *            Role object
	 * @return void
	 * @throws Exception 
	 */
	@Override
	@Transactional
	public void addUserRole(AccountUser user, String userNewRoleName) throws Exception {

		try {
			userNewRoleName = userNewRoleName.toUpperCase();
			Role newRole = roleService.findRoleByName(userNewRoleName);
			if (newRole == null) {
				throw new InvalidUserException("Invalid User Role ::" + userNewRoleName + " does not exist in GHIX");

			}
			if (!hasUserRole(user, newRole.getName())) {
				addUserRole(user, newRole);
			}else if(!hasActiveUserRole(user, newRole.getName())){
				UserRole userRole = getUserRoleByRoleName(user, newRole.getName());
				userRole.setIsActive(RoleService.ROLE_IS_ACTIVE_DEFAULT);
				updateUserRole(user, userRole);
			}
						
		}

		catch (Exception ex) {
			LOGGER.error("Addition of Role assignment failed. User Id: " + user.getId(), ex);
			throw ex;
			
		}

	}
	
	/**
	 * Adds role to the user. Checks if default Role had defined, if not, sets
	 * the role as default role
	 * 
	 * @param accountUser
	 *            AccountUser object
	 * @param role
	 *            Role object
	 * @return void
	 */
	@Override
	@Transactional
	public void addUserRole(AccountUser user, Role role) {

		try {
			UserRole newUserRole = new UserRole();

			newUserRole.setRole(role);
			newUserRole.setUser(user);
			// newUserRole.getUser().setConfirmed(UserService.confirmed);

			// Check if default Role had defined, if not set the role as default
			// role

			Role chkDefaultRole = getDefaultRole(user);

			if (chkDefaultRole == null || !user.isHasGhixProvisioned()) {
				newUserRole.setRoleFlag(RoleService.ROLE_FLAG_DEFAULT);
			} else {
				newUserRole.setRoleFlag(RoleService.ROLE_FLAG_NOT_DEFAULT);
			}
			//Added for HIX-27184 and HIX-27305. Set always active for new user role.
			newUserRole.setIsActive(RoleService.ROLE_IS_ACTIVE_DEFAULT);
			newUserRole = userRoleService.addUserRole(newUserRole);
			user.getUserRole().add(newUserRole);// Add the newly added user role
		}

		catch (Exception ex) {
			LOGGER.error("Role assignment failed. User Id: " + user.getId(), ex);
			throw new GIRuntimeException("PLATFORM-00002",ex,null,Severity.HIGH);
		}

	}	

	/**
	 * Returns user object of the given user id
	 * 
	 * @param id
	 *            int
	 * @return AccountUser
	 */
	@Override
	@Transactional(readOnly = true)
	public AccountUser findById(int id) {
		AccountUser userObj = null;
		if (userRepository.exists(id)) {
			userObj = userRepository.findById(id);
		}
		return userObj;
	}
	
	/**
	 * HIX-49151
	 * Returns the logged in user object.
	 * NOTE: This method will be deprecated after re-factoring the getLoggedInUser (after removal of InvalidUserException)
	 * -Venkata Tadepalli: Sept-10-2014
	 * 
	 * @return AccountUser
	 */

	@Override
	@Deprecated
	@Transactional(readOnly = true)
	public AccountUser getPrincipalUser()  {
		LOGGER.info("Getting logged in user :: START ... ");
		AccountUser user = null;
		try {
			
			Object  userObj= SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (userObj instanceof AccountUser) {
				user = (AccountUser) userObj;
				LOGGER.info("Logged in user name:: " + user.getUserName());
			}else{
				// NOTE:  In case of Anonymous user, the userObj will be instance of
				// string ::(userObj instanceof String) 
				user = null;
				LOGGER.info("User object is not an instance of AccountUser / anonymus user access ::(User ::"+userObj+")");
			}
			
		} catch (Exception ex) {
			exceptionMessage=ex.getMessage();
			LOGGER.warn("User object is not an instance of AccountUser / anonymus user access ::"+exceptionMessage);
			user = null;
		}
		
		return user;
		
	}

	/**
	 * Returns the logged in user object.
	 * 
	 * @return AccountUser
	 */

	@Override
	@Transactional(readOnly = true)
	public AccountUser getLoggedInUser() throws InvalidUserException {
		LOGGER.debug("Getting logged in user :: START ... ");
		AccountUser user = null;
		user = SecurityUtils.getLoggedInUser();
	/*	if(user != null) {
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if(sra != null) {
				HttpServletRequest req = sra.getRequest();
				HttpSession session = req.getSession();
				if(session != null) {
					String activeModuleName = (String) session.getAttribute("userActiveRoleName".intern());
					if(activeModuleName != null) {
						user.setActiveModuleName(activeModuleName);
					}
					Integer activeModuleId = (Integer)session.getAttribute("userActiveRoleId".intern());
					if(activeModuleId != null) {
						user.setActiveModuleId(activeModuleId);
					}
				}else {
					LOGGER.info("Anonymous session, logged  in information is not available".intern());
				}
			}
		}*/
		return user;
	}

	//   

	/**
	 *   Created as part for the HIX-32110 :
	 *  If the given pwd1 matches to the pwd2 then returns true for the given user.
	 *  Get the salt from user object
	 * 
	 * @param user
	 *            AccountUser object
	 * @param pwd1
	 *            String object
	 * @param pwd2
	 *            String object	 *            
	 * @return boolean
	 */
	@Override
	public boolean isPasswordMatch(AccountUser user, String userProvidedPass, String pwd2) {

		RequestHandler handler = this.handlerFactory.getHandlerByName(PasswordHandler.NAME);
		if (handler == null) {
			LOGGER.error("No password handler availble for name:" + PasswordHandler.NAME);
		}
		HashMap<String, Object> context = new HashMap<String, Object>(10);
		context.put(HandlerConstants.ACCOUNT_USER_KEY, user);
		context.put(HandlerConstants.PASSWORD_KEY, userProvidedPass);
		context.put(PasswordHandler.PASSWORD_OPRATION_KEY, PasswordHandler.MATCH);

		TenantDTO tenantDto = TenantContextHolder.getTenant();
		if (tenantDto != null) {
			context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
			context.put(RequestHandler.TENANT_DTO_KEY, tenantDto);
		}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
			}

		boolean success = handler.handleRequest(context, null, false);
		if (!success) {
			LOGGER.error("Password match failed with error " + (String) context.get(HandlerConstants.HANDLER_ERROR));
			String errMsg = (String) context.get(HandlerConstants.HANDLER_EXCEPTION);
			if (errMsg != null) {
				LOGGER.error("Password match failed message :" + errMsg);
			}
		}else{
			LOGGER.info("User existing password validation successful");
		}
		return success;

	}
	
	@Override
	public boolean isPasswordSameInDatabase(AccountUser user, String pwd1, String pwd2) {
		
		 String userUuid=user.getUuid();
		 pwd1 = pwd1+userUuid;
         
		boolean result=ghixEncryptorUtil.verifyEncryptedPassword(pwd1, pwd2);
		return result;
	}

	/**
	 * 
	 * This method returns the user's Max Retry Count while trying for resting the password
	 * @param userDetails
	 * @return
	 */
	@Override
	public int getUsersMaxRetryCount(AccountUser user) {
		int maxAttempts = 0;
		if (user != null){
			Set<Role> userRoles = getAllRolesOfUser(user);
			if (userRoles != null){
				
				//if User has multiple roles then set lower limit as the cut-off.
				for (Role userRole : userRoles)
				{
					if (maxAttempts == 0)
					{
						maxAttempts = userRole.getMaxRetryCount();
					}
					else if (maxAttempts > userRole.getMaxRetryCount())
					{
						maxAttempts = userRole.getMaxRetryCount();
					}
				}
			}
		}
		return maxAttempts;
	}
	

	/**
	 * Returns true if the User had assigned to given rule Name,else returns
	 * false
	 * 
	 * @param user
	 *            AccountUser object
	 * @param roleName
	 *            String object
	 * @return boolean
	 */
	@Override
	public boolean hasUserRole(AccountUser user, String roleName) {
		if(user!=null){
			Set<UserRole> userRoles = user.getUserRole();
			for (UserRole userRole : userRoles) {
				if (StringUtils.equalsIgnoreCase(roleName, userRole.getRole().getName())) {
					return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * Returns true if the User had assigned to given rule Name,else returns
	 * false
	 * 
	 * @param user
	 *            AccountUser object
	 * @param roleName
	 *            String object
	 * @return boolean
	 */

	// Checks if the login user with the given role has authorization
	@Override
	public boolean hasAuthorization(String roleName, Model model, HttpServletRequest request) {
		AccountUser user = null;
		HashMap<String, Object> errorMsg = new HashMap<String, Object>();

		try {
			user = getLoggedInUser();
			if (!hasUserRole(user, roleName)) {
				errorMsg.put(MESSAGE, "User not authorized.");
				model.addAttribute("authfailed", errorMsg.get(MESSAGE));
				request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
				return false;
			}
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in");
			errorMsg.put(MESSAGE, "User not logged in.");
			model.addAttribute("authfailed", errorMsg.get(MESSAGE));
			request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
			return false;
		}
		return true;
	}

	@Override
	public boolean isEmployeeAdmin(AccountUser user) {
		return (this.hasUserRole(user, RoleService.EMPLOYER_ROLE));
	}

	@Override
	public boolean isEmployee(AccountUser user) {
		return (this.hasUserRole(user, RoleService.EMPLOYEE_ROLE));
	}

	/*
	 * @Override public boolean isIssuer(AccountUser user) { return
	 * (this.hasUserRole(user, RoleService.ISSUER_ROLE)); }
	 */

	@Override
	public boolean isIssuerRep(AccountUser user) {
		return (this.hasUserRole(user, RoleService.ISSUER_REP_ROLE));
	}

	@Override
	public boolean isAdmin(AccountUser user) {
		return (this.hasUserRole(user, RoleService.ADMIN_ROLE));
	}
	
	@Override
	public boolean isAffiliateAdmin(AccountUser user) {
		return (this.hasUserRole(user, RoleService.AFFILIATE_ADMIN));
	}
	/*
	 * @Override public boolean isEmployer(AccountUser user) { return
	 * (this.hasUserRole(user, RoleService.EMPLOYER_ROLE)); }
	 */

	@Override
	@Transactional(readOnly = true)
	public AccountUser findUserByRecovery(String recovery) {
		return userRepository.findByRecovery(recovery);
	}
	
	/**
	 * Updates confirmed flag of users table to 1 or 0
	 * 
	 * If confFlag=0 user will be inactive i.e will not able to login
	 * If confFlag=1 user will be active i.e will be able to login
	 */

	@Override
	@Transactional
	public void updateConfirmed(AccountUser user,int confFlag) {
		try {
			AccountUser userObj = userRepository.findById(user.getId());
			if (userObj!=null) {
				userObj.setConfirmed(confFlag);
				userObj.setLastUpdatedBy(userObj.getId());
				userRepository.save(userObj);
			}
		}

		catch (Exception ex) {
			LOGGER.error("Failed to update confirmed flag", ex);
		}

	}

	@Override
	public boolean isBroker(AccountUser user) {
		return (this.hasUserRole(user, RoleService.BROKER_ROLE));
	}

	/**
	 * Returns user's default role. If there is no default role, then the user
	 * profile requires to setup default role. In case of no default role, the
	 * method returns null.
	 * 
	 * @param user
	 *            AccountUser object
	 * @return UserRole
	 */
	@Override
	public Role getDefaultRole(AccountUser user) {
		Role defaultRole = user.getDefRole();
		if(defaultRole != null) {
			return defaultRole;
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.info("In getDefaultRole");
			LOGGER.info("isHasGhixProvisioned:" + user.isHasGhixProvisioned());
		}
		if(!user.isHasGhixProvisioned()){
			LOGGER.info("User not provisioned in target user repository, checking if provisioning module has set the provisioning role");
			String provisioningRoleName = user.getGhixProvisionRoleName();
			if(provisioningRoleName == null){
				LOGGER.error("No provisioning role information available for user:"+user.getUsername());
				return null;
			}
			return this.roleService.findRoleByName(provisioningRoleName);
		}
		Iterable<UserRole> userRoleList = user.getUserRole();
		if(userRoleList == null || ((Set<UserRole>)userRoleList).size() == 0){
			//Its not possible to have a user in the DB and no user roles associated
			LOGGER.info("User appears to be in the table but no roles returned from the object with id:"+user.getId()+", looks like user is still under provisioning");
			userRoleList = this.userRoleService.findByUser(user);
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.info("Roles are present");
		}
		for (UserRole userRole : userRoleList) {
			if(LOGGER.isDebugEnabled()){
				LOGGER.info("Role name:"+userRole.getId()+", isDefaultRole = "+userRole.isDefaultRole());
			}
			if (userRole.isDefaultRole()) {
				defaultRole = userRole.getRole();
				break;
			}

		}
		user.setDefRole(defaultRole);
		return defaultRole;
	}

	/**
	 * Returns user's permissions list.
	 * 
	 * @param user
	 *            AccountUser object
	 * @return List<String>
	 */
	@Override
	@Transactional(readOnly = true)
	public List<String> getUserPermissions(AccountUser user) {
		List<String> rolePermissions = new ArrayList<String>();

		@SuppressWarnings("unchecked")
		List<GrantedAuthority> rsList = (List<GrantedAuthority>) user.getAuthorities();
		Iterator<GrantedAuthority> rsIterator = rsList.iterator();

		LOGGER.debug("getUserPermissions for " + user.getUsername() + " : No of Permissions : "
				+ rolePermissions.size() + " :: ");

		while (rsIterator.hasNext()) {
			String currPermissionName = rsIterator.next().getAuthority();
			rolePermissions.add(currPermissionName);
			LOGGER.debug("\t\t" + currPermissionName);
		}

		return rolePermissions;

	}

	/**
	 * At the end of user registration process, let auto login the user account.
	 * 
	 * @param accountUser
	 *            AccountUser object
	 * @return String object
	 */
	@Override
	public String getAutoLoginUrl(AccountUser accountUser) {

		String autoLoginUrl = "";
		try {
			autoLoginUrl = "forward:/j_spring_security_check?j_username=" +URLEncoder.encode(accountUser.getUserName(),"UTF-8") ;
		} catch (UnsupportedEncodingException e) {
			 throw new GIRuntimeException("Error occured while URL encoding of username",e);
		}

		return autoLoginUrl;

	}
	
	/**
	 * Get the auto login url with the gien username and password.
	 * HIX-42022 - Implement hybrid authentication (sso and db login)
	 * 
	 * @author venkata tadepalli 
	 * @since  June 30, 2014
	 * 
	 * @param userName
	 * @param password
	 * @return login url as string type
	 * 
	 */

	@Override
	public String getAutoLoginUrl(String userName, String password) {

		String autoLoginUrl = "";

		autoLoginUrl = "forward:/j_spring_security_check?j_username=" + userName+ "&j_password="+ password;

		return autoLoginUrl;

	}
	
	@Override
	public void updateUserRole(AccountUser user, Role userRole) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void updateUserAffiliate(ModuleUser moduleUser, String affilaiteId, AccountUser user ) throws GIException {

		if(null != moduleUser){
			moduleUserRepository.delete(moduleUser);
		}
		
		if(! affilaiteId.equals("")){
			try {
				int moduleId = Integer.parseInt( affilaiteId);
				this.createModuleUser(moduleId , LINK_AFFILIATE_ROLE,  user, true);
			} catch (NumberFormatException e) {
				throw new GIException("Wrong or invalid moduleid passed",e);
			}
			
		}
	}
	
	/**
	 * HIX-26903 : Create a module user record. Ensure to have only one record for the
	 * combination; moduleId, moduleName, userId and isSelfSigned flag
	 * Note: For the given userId and moduleName combination there should be 
	 * 		only on module user record with isSelfSigned='Y' (true)
	 * 
	 * 
	 * @param moduleId
	 *            int object
	 * @param moduleName
	 *            String object
	 * @param accountUser
	 *            AccountUser object
	 * @param moduleUser
	 *            ModuleUser object
	 * @param isSelfSigned
	 *            boolean
	 * 
	 * @return ModuleUser object
	 * @throws GIException 
	 */

	@Override
	public ModuleUser createModuleUser(int moduleId, String moduleName, AccountUser user, boolean isSelfSigned) throws GIException {
		String moduleNameInLowerCase=moduleName.toLowerCase();
		LOGGER.info("Creating Module ("+moduleNameInLowerCase+") for the User :"+user.getId()+"::..");
		
		ModuleUser newModuleUser = null;
		if(isSelfSigned){
			LOGGER.debug("Creating Self Signed Module ("+moduleNameInLowerCase+") for the User :"+user.getId()+"::..");
			newModuleUser = this.getUserSelfSignedModule(moduleNameInLowerCase,user.getId());
			
			if(newModuleUser!=null){
				
				throw new GIException("Invalid Self Signed Module creation :: "+
						"Already exists Self Signed Module ("+moduleNameInLowerCase+") for the User :"+user.getUserName());
			}
		}
		newModuleUser = moduleUserRepository.findModuleUser(moduleId, moduleNameInLowerCase, user.getId());

		if (newModuleUser == null) {
			String selfSignedFlag = (isSelfSigned) ? "Y" : "N" ;
			newModuleUser = new ModuleUser();
			newModuleUser.setModuleId(moduleId);
			newModuleUser.setModuleName(moduleNameInLowerCase);
			newModuleUser.setUser(user);
			newModuleUser.setIsSelfSigned(selfSignedFlag);

			newModuleUser = moduleUserRepository.save(newModuleUser);
		}
		LOGGER.info("Created Module ("+moduleNameInLowerCase+") for the User ::"+user.getId()+"::...");
		return newModuleUser;

	}
	
	/**
	 * Create a module user record. Ensure to have only one record for the
	 * combination; moduleId, moduleName and userId.
	 * HIX-26903 : Deprecated, instead
	 *             use createModuleUser(int moduleId, String moduleName, AccountUser user, boolean isSelfSigned)
	 * 
	 * @param moduleId
	 *            int object
	 * @param moduleName
	 *            String object
	 * @param accountUser
	 *            AccountUser object
	 * @param moduleUser
	 *            ModuleUser object
	 * 
	 * @return ModuleUser object
	 */
	@Deprecated
	@Override
	public ModuleUser createModuleUser(int moduleId, String moduleName, AccountUser user) {

		ModuleUser newModuleUser=null;
		try {
			newModuleUser = createModuleUser( moduleId, moduleName, user, false);
		} catch (GIException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Invalid API usage : Creating Module ("+moduleName+") for the User ::"+user.getId()+"::...");
		}
		LOGGER.info("Invalid User of deprecated method:: Created Module ("+moduleName+") for the User ::"+user.getId()+"::...");
		return newModuleUser;

	}

	@Override
	/**
	 * Returns the User Name of specified Id
	 */
	public String getUserName(int userId) {
		return userRepository.getUserNameById(userId);
	}

	/**
	 * Returns list of {@link AccountUser} objects with only
	 * first name, last name and phone number fields filled.
	 *
	 * @param userIds list of user ids.
	 * @return list of {@link AccountUser} objects.
	 */
	@Override
	public List<AccountUser> getUserNameAndPhoneByUserIds(List<Integer> userIds)
	{
		return userRepository.getUserNameAndPhoneByIds(userIds);
	}
	
	@Override
	/**
	 * Returns the User Name of specified Id
	 */
	public List<AccountUser> getUserNames(List<Integer> userIds) {
		return userRepository.getUserNamesByIds(userIds);
	}

	@Override
	public void updateModuleUser(AccountUser user, ModuleUser moduleUser) {
		// TODO Auto-generated method stub
		// NOT YET IMPLEMENTED

	}

	/**
	 * Deletes a module user record. Ensure to delete only one record for the
	 * combination; moduleId, moduleName and userId.
	 * 
	 * @param accountUser
	 *            AccountUser object
	 * @param moduleUser
	 *            ModuleUser object
	 * @return void
	 */

	@Override
	public void deleteModuleUser(AccountUser user, ModuleUser moduleUser) {
		ModuleUser delModuleUser = moduleUserRepository.findModuleUser(moduleUser.getModuleId(),
				moduleUser.getModuleName(), moduleUser.getUser().getId());

		if (delModuleUser != null) {
			moduleUserRepository.delete(delModuleUser);
		}

	}
	
	@Override
	public void changeEmail(AccountUser accountUser) {
		boolean isEmailUpdated = false;
		
		RequestHandler handler = this.handlerFactory.getHandlerByName(UpdateUserHandler.NAME);
		try {
			if(handler != null){
				HashMap<String, Object> context = new HashMap<>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, accountUser);
				context.put(UpdateUserHandler.UPDATE_USER_OPERATION_KEY, UpdateUserHandler.UPDATE_USER_NAME);
				TenantDTO tenantDto = TenantContextHolder.getTenant();
				if(tenantDto != null){
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
				}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}

				isEmailUpdated = handler.handleRequest(context, null, false);
				if(!isEmailUpdated){
					String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
					LOGGER.error("Error updating email:"+error);
					throw new Exception(error);
				}
			}
			if(isEmailUpdated) {
				accountUser.setUserName(accountUser.getEmail());
				int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : accountUser.getId();
				accountUser.setLastUpdatedBy(updatedBy);
				userRepository.save(accountUser);
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CHNGNG EMAIL VIA WSO2 CLAIM: ["+e.getMessage()+"] Please check the GI Monitor for more details");
			throw new GIRuntimeException("Failed to update the user using SCIM ", e);
		}
		
	}
	
	private boolean  updateIDPUserName(AccountUser accountUser, String newUsername, String password) {
		boolean isUserNameUpdated = true;
		RequestHandler handler = this.handlerFactory.getHandlerByName(UpdateUserHandler.NAME);
		try {
		if(handler != null){
			HashMap<String, Object> context = new HashMap<>();
			context.put(HandlerConstants.ACCOUNT_USER_KEY, accountUser);
			TenantDTO tenantDto = TenantContextHolder.getTenant();
			context.put(UpdateUserHandler.UPDATE_USER_OPERATION_KEY, UpdateUserHandler.UPDATE_USER_NAME);
			context.put(UpdateUserHandler.CURRENT_PASSWORD, password); 
			context.put(UpdateUserHandler.NEW_USER_NAME, newUsername); 
			if(tenantDto != null){
				context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
				context.put(HandlerConstants.TENANT_DTO_KEY, tenantDto);
			}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
			}
			
			isUserNameUpdated = handler.handleRequest(context, null, false);
			if(!isUserNameUpdated){
				String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
				LOGGER.error("Error updating email:"+error);
			}
		}
		
	} catch (Exception e) {
		LOGGER.error("ERR: WHILE CHNGNG EMAIL VIA WSO2 CLAIM: ["+e.getMessage()+"] Please check the GI Monitor for more details");
		throw new GIRuntimeException("Failed to update the user using SCIM ", e);
	}
	   return isUserNameUpdated;
	}
	
	@Override
	public AccountUser updateUserName(AccountUser accountUser, String password) {
		boolean success = false;
		AccountUser currentUser = this.accessControl.getCurrentUser();
		Role managedUserRole = this.getDefaultRole(accountUser);
		if(currentUser == null){
			throw new GIRuntimeException("User update can not happen in anonymous mode");
		}
		if(currentUser.getId() != accountUser.getId()){
			// Logged in user is not same as the user being updated, Check the permissions
			if(!this.accessControl.canUpdateUserDetails(managedUserRole.getName())){
				throw new GIRuntimeException("Permission Denied, Current user can not update the user details for role:"+managedUserRole.getName());
			}
		}
		try {
			RequestHandler handler = this.handlerFactory.getHandlerByName(UpdateUserHandler.NAME);
			if(handler != null){
				HashMap<String, Object> context = new HashMap<>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, accountUser);
				TenantDTO tenantDto = TenantContextHolder.getTenant();
				context.put(UpdateUserHandler.UPDATE_USER_OPERATION_KEY, UpdateUserHandler.UPDATE_USER_NAME);
				context.put(UpdateUserHandler.CURRENT_PASSWORD, password); 
				if(tenantDto != null){
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
					context.put(HandlerConstants.TENANT_DTO_KEY, tenantDto);
				
				}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}

				success = handler.handleRequest(context, null, false);
				if(!success){
					String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
					LOGGER.error("Error updating email:"+error);
					throw new Exception(error);
				}
			}
			if(success) {
				accountUser.setUserName(accountUser.getEmail());
				int updatedBy =currentUser.getId();
				accountUser.setLastUpdatedBy(updatedBy);
				return userRepository.save(accountUser);
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CHNGNG EMAIL VIA WSO2 CLAIM: ["+e.getMessage()+"] Please check the GI Monitor for more details");
			throw new GIRuntimeException("Failed to update the user using SCIM ", e);
		}
		return null;
	}
	
	/**
	 * Currently used in PHIX
	 */
	@Override
	public AccountUser updateUserName(AccountUser accountUser, String newUsername, String password) {
		boolean success = false;
		AccountUser currentUser = this.accessControl.getCurrentUser();
		Role managedUserRole = this.getDefaultRole(accountUser);
		if(currentUser == null){
			throw new GIRuntimeException("User update can not happen in anonymous mode");
		}
		if(currentUser.getId() != accountUser.getId()){
			// Logged in user is not same as the user being updated, Check the permissions
			if(!this.accessControl.canUpdateUserDetails(managedUserRole.getName())){
				throw new GIRuntimeException("Permission Denied, Current user can not update the user details for role:"+managedUserRole.getName());
			}
		} 
		try {
			RequestHandler handler = this.handlerFactory.getHandlerByName(UpdateUserHandler.NAME);
			if(handler != null){
				HashMap<String, Object> context = new HashMap<>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, accountUser);
				TenantDTO tenantDto = TenantContextHolder.getTenant();
				context.put(UpdateUserHandler.UPDATE_USER_OPERATION_KEY, UpdateUserHandler.UPDATE_USER_NAME);
				context.put(UpdateUserHandler.CURRENT_PASSWORD, password); 
				context.put(UpdateUserHandler.NEW_USER_NAME, newUsername); 
				if(tenantDto != null){
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
					context.put(HandlerConstants.TENANT_DTO_KEY, tenantDto);
				}else{
					context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}
				success = handler.handleRequest(context, null, false);
				if(!success){
					String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
					LOGGER.error("Error updating email:"+error);
					throw new Exception(error);
				}
			}
			if(success) {
				accountUser.setUserName(newUsername);
				accountUser.setEmail(newUsername);
				int updatedBy =currentUser.getId();
				accountUser.setLastUpdatedBy(updatedBy);
				return userRepository.save(accountUser);
				
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CHNGNG EMAIL VIA WSO2 CLAIM: ["+e.getMessage()+"] Please check the GI Monitor for more details");
			throw new GIRuntimeException("Failed to update the user using SCIM ", e);
		}
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AccountUser updateUser(AccountUser accountUser) {
		boolean success = false;
		AccountUser currentUser = this.accessControl.getCurrentUser();
		boolean hashSecAns1 = false;
		boolean hashSecAns2 = false;
		AccountUser existingUserData = (accountUser.getId() > 0) ? userRepository.getUserBasicInfo(accountUser.getId()) : null;
		existingUserData.setExtnAppUserId(accountUser.getExtnAppUserId());
		
		Role managedUserRole = this.getDefaultRole(accountUser);
		
		if(currentUser == null){
			throw new GIRuntimeException("User update can not happen in anonymous mode");
		}
		if(currentUser.getId() != accountUser.getId()){
			// Logged in user is not same as the user being updated, Check the permissions
			if(!this.accessControl.canUpdateUserDetails(managedUserRole.getName())){
				throw new GIRuntimeException("Permission Denied, Current user can not update the user details for role:"+managedUserRole.getName());
			}
		}
		try {
			RequestHandler handler = this.handlerFactory.getHandlerByName(UpdateUserHandler.NAME);

			if(handler != null){
				if(null != existingUserData && ! existingUserData.getUsername().equals(accountUser.getUserName())) {
					if ( updateIDPUserName(existingUserData, accountUser.getUserName(), GhixPlatformConstants.TEMPORARY_USER_PASSWORD) ) {
						accountUser.setExtnAppUserId(existingUserData.getExtnAppUserId());
						sendEmailToUser(accountUser,currentUser.getEmail());
					}
				}
				
				HashMap<String, Object> context = new HashMap<>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, accountUser);
				context.put(UpdateUserHandler.UPDATE_USER_OPERATION_KEY, UpdateUserHandler.UPDATE_USER);
				
				String secAns1 = accountUser.getSecurityAnswer1();
				if(null != existingUserData && secAns1 != null && (existingUserData.getSecurityAnswer1() == null  || !existingUserData.getSecurityAnswer1().equals(secAns1))) {
					context.put(HandlerConstants.HASH_ANSWER_1, "Y");
					hashSecAns1 = true;
				}else {
					context.put(HandlerConstants.HASH_ANSWER_1, "N");
				}
				
				String secAns2 = accountUser.getSecurityAnswer2();
				if(null != existingUserData && secAns2 != null  && (existingUserData.getSecurityAnswer2() == null   || !existingUserData.getSecurityAnswer2().equals(secAns2))) {
					context.put(HandlerConstants.HASH_ANSWER_2, "Y");
					hashSecAns2 = true;
				}else {
					context.put(HandlerConstants.HASH_ANSWER_1, "N");
				}

				TenantDTO tenantDto = TenantContextHolder.getTenant();
				if(tenantDto != null){
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
					context.put(HandlerConstants.TENANT_DTO_KEY, tenantDto);
				}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}

				success = handler.handleRequest(context, null, false);
				if(!success){
					String error = (String)context.get(HandlerConstants.HANDLER_ERROR);
					LOGGER.error("Error updating email:"+error);
					throw new Exception(error);
				}
				if(success) {
					accountUser.setUserName(accountUser.getEmail());
					int updatedBy =currentUser.getId();
					accountUser.setLastUpdatedBy(updatedBy);
					
					if(tenantDto != null && tenantDto.isRemotelyManagedUsers()) {
						if(hashSecAns1) {
							accountUser.setSecurityAnswer1( PlatformServiceUtil.getHash(secAns1.toLowerCase()));
						}
						
						if(hashSecAns2) {
							accountUser.setSecurityAnswer2(PlatformServiceUtil.getHash(secAns2.toLowerCase()));
						}
					}
					return userRepository.save(accountUser);
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CHNGNG EMAIL VIA WSO2 CLAIM: ["+e.getMessage()+"] Please check the GI Monitor for more details");
			throw new GIRuntimeException("Failed to update the user using SCIM ", e);
		}
		return null;
	}
	
	/**
	 * TEMP function to get the user record updated. (PHIX ONLY) 
	 */
	@Deprecated
	@Override
	public AccountUser updateUserPHIX(AccountUser accountUser) {
		return userRepository.save(accountUser);
	}
	
	private void sendEmailToUser(AccountUser user,String fromAddr) throws Exception {
		Notice notice = new Notice();
		notice.setSubject("Your username has been changed");
		notice.setFromAddress(fromAddr);
		notice.setToAddress(user.getEmail());
		NoticeType noticeType = new NoticeType();
		noticeType.setType(GhixNotificationType.EMAIL);
		notice.setNoticeType(noticeType);
		String link = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL+RESET_CONTROLLER_URL; 
		StringBuilder emailBody = new StringBuilder();
		emailBody.append("<p>This email is to notify you that your username and email has been updated to "+ user.getUsername()+" in ");
		emailBody.append(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+". </p>");
		emailBody.append("<p>You are required to reset your password before you can access your account with updated username." );
		emailBody.append("In order to access your record, please <a href='"+link+"'>click here</a>.</p>");
		emailBody.append("<p>If the link doesn't work, please copy the following link to your browser window:" );
		emailBody.append(link + "</p>");
		emailBody.append("<p> Thanks, </p><p> Admin </p>");
		
		notice.setEmailBody(emailBody.toString());
		try {
			emailService.dispatch(notice);
		} catch (Exception e) {
			LOGGER.error("Unable to sendEmailToUser", e);
		}
	}

	@Override
	public AccountUser savePasswordRecoveryDetails(int id, String passwordRecoveryToken) {

		AccountUser userObj = null;
		int pwdExpryHours = 24;
		try {
			String noOfExpirationsDays = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORDEXP);
			if(StringUtils.isNotEmpty(noOfExpirationsDays)){
				pwdExpryHours = Integer.parseInt(noOfExpirationsDays)*24;
			}
			if (userRepository.exists(id)) {
				//HIX-30630. 32 char UUID 
				//String encryptString = vimoEncryptor.encrypt(passwordRecoveryToken);

				userObj = userRepository.findById(id);
				userObj.setPasswordRecoveryToken(passwordRecoveryToken);//HIX-30630
				java.util.Date passwordExpiryDate = null;
				
				if(passwordRecoveryToken != null) {
					passwordExpiryDate = TSDateTime.getInstance().plusHours(pwdExpryHours).toDate();;
				}
				
				userObj.setPasswordRecoveryTokenExpiration(passwordExpiryDate);
				int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : userObj.getId();
				userObj.setLastUpdatedBy(updatedBy);
				userObj = userRepository.save(userObj);
			}else{
				LOGGER.error("No user available with Id:"+id);
			}
		} catch (Exception ex) {
			LOGGER.error("User password recovery details set up failed, UserId :: " + id, ex);
		}
		return userObj;
	}
	
	@Override
	public AccountUser savePasswordRecoveryDetails(String email, String passwordRecoveryToken) {
		AccountUser userObj = null;
		int pwdExpryHours = 24;
		try {
			String noOfExpirationsDays = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORDEXP);
			if(StringUtils.isNotEmpty(noOfExpirationsDays)){
				pwdExpryHours = Integer.parseInt(noOfExpirationsDays)*24;
			}
			userObj = userRepository.findByEmail(email);
			if (null != userObj) {
				//HIX-30630. 32 char UUID 
				//String encryptString = vimoEncryptor.encrypt(passwordRecoveryToken);
				userObj.setPasswordRecoveryToken(passwordRecoveryToken);//HIX-30630
				java.util.Date passwordExpiryDate = null;
				
				if(passwordRecoveryToken != null) {
					passwordExpiryDate =TSDateTime.getInstance().plusHours(pwdExpryHours).toDate();
				}
				
				userObj.setPasswordRecoveryTokenExpiration(passwordExpiryDate);
				int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : userObj.getId();
				userObj.setLastUpdatedBy(updatedBy);
				userObj = userRepository.save(userObj);
			}else{
				LOGGER.info("No user available with EMAIL: {}", email);
			}
		} catch (Exception ex) {
			LOGGER.error("User password recovery details set up failed, EMAIL :: " + email, ex);
		}
		return userObj;
	}

	@Override
	public AccountUser findByPasswordRecoveryToken(String passwordRecoveryToken) {
		return userRepository.findByPasswordRecoveryToken(passwordRecoveryToken);
	}

	@Override
	public void enableRoleMenu(HttpServletRequest request, String roleName) {

		request.getSession().setAttribute("switchToModuleName", roleName.toLowerCase());
	}

	@Override
	public List<ModuleUser> getUserModules(int userId) {
		return moduleUserRepository.findUserModules(userId);
	}

	@Override
	public List<ModuleUser> getUserModules(int userId, String moduleName) {
		return moduleUserRepository.findUserModules(userId, moduleName);
	}

	@Override
	public List<ModuleUser> getModuleUsers(int moduleId) {
		return moduleUserRepository.findModuleUsers(moduleId);
	}

	@Override
	public List<ModuleUser> getModuleUsers(int moduleId, String moduleName) {
		return moduleUserRepository.findModuleUsers(moduleId, moduleName);
	}

	@Override
	public List<Integer> getModuleIdsByModuleName(String moduleName) {
		return moduleUserRepository.findModuleIdByModuleName(moduleName);
	}
	/**
	 * Query the ModuleUser table for the given modueId, moduelName and userId;
	 * If no record then returns null
	 * 
	 * @param moduleId
	 *            int
	 * @param moduleName
	 *            String object
	 * @param user
	 *            AccountUser object
	 * @return ModuleUser object
	 */

	@Override
	public ModuleUser findModuleUser(int moduleId, String moduleName, AccountUser user) {
		ModuleUser moduleUser = moduleUserRepository.findModuleUser(moduleId, moduleName, user.getId());

		return moduleUser;
	}

	/**
	 * Query the ModuleUser table for the given modueId, moduelName and userId;
	 * If no record then returns null
	 * 
	 * @param moduleId
	 *            int
	 * @param moduleName
	 *            String object
	 * @param user
	 *            AccountUser object
	 * @return ModuleUser object
	 */

	@Override
	public ModuleUser getUserDefModule(String defModuleName,int userId){
		ModuleUser moduleUser = moduleUserRepository.findModuleUserByModuleNameAndUserId(defModuleName, userId);
		return moduleUser;
	}
	
	

	
	@Override
	public List<AccountUser> getAccountUsersByUserRole(int roleId) {
		return userRepository.getUserListByRoleId(roleId);
	}


	@Override
	public List<Integer> getAdminUsers() {
		 return userRepository.getUsersWithAdminRole();
	}
	
	@Override
	public boolean isEnrollmentEntity(AccountUser user) {
		return (this.hasUserRole(user, RoleService.ENROLLMENT_ENTITY));
	}
	
	@Override
	public List<AccountUser> getAdminAndCSRRoleUsers() {
		return userRepository.getAdminAndCSRRoleUsers();
	}
	
	@Override
	public List<AccountUser> getAgentList() {
		return userRepository.getAgentList();
	}

	@Override
	@Transactional
	public List<String> disableInactiveAccounts()  {
		List<String> inactiveAccounts=new ArrayList<String>();
		List<AccountUser> userList = userRepository.getUserListToInactivate();
		Iterator<AccountUser> itrAccoutnUser=userList.iterator();
		int expiryDayCount = 0;
		Role tmpRole = null;
		while(itrAccoutnUser.hasNext()){
			AccountUser currAccountUser= itrAccoutnUser.next();
			if(currAccountUser.getUsername().endsWith("ghix.com")){
				userAudit.createDeActivateAudit( new GiAuditParameter("User id : ", currAccountUser.getId()),
						  new GiAuditParameter("User name : ", currAccountUser.getUsername()),
		                  new GiAuditParameter("Action : ", "By-passed for account disablement based on in activity")
                       );
				continue;
			}
			try{
				List<String> userRoleNames = userRoleService.findUserRoleNames(currAccountUser);
				// we use the max expiry allowances among all roles
				for (String usrRoleName: userRoleNames){
					int currExpDayCount = 0;
					tmpRole = roleService.findRoleByName(usrRoleName);
					if (tmpRole != null){
						currExpDayCount = tmpRole.getAccountIdlePeriod();
					}
									
					if (currExpDayCount > expiryDayCount)
						expiryDayCount = currExpDayCount;
				}				
				if(expiryDayCount>0){
					Date lastLogin = (currAccountUser.getLastLogin() == null) ? currAccountUser.getCreated() : currAccountUser.getLastLogin();
					Date now =TSDateTime.getInstance().minusDays(expiryDayCount).toDate();
					boolean isBefore = lastLogin.before(now);
					if (isBefore){
						//invalidateConfirmed(currAccountUser);
						//String strInactiveDormant = "Inactive-dormant";//AccountUser.user_status.InactiveDormant.toString();
						//updateStatusCode(strInactiveDormant,currAccountUser.getId());
						currAccountUser = markAccountInactive(currAccountUser);
						
						
						List <GiAuditParameter> duoAuditParams =  userRoleService.toggleDuoAssociation(currAccountUser);
						if(!duoAuditParams.isEmpty()){
							for(GiAuditParameter auditParameter : duoAuditParams ){
								GiAuditParameterUtil.add(auditParameter);
							}
						}
						inactiveAccounts.add(currAccountUser.getId()+" :: "+currAccountUser.getUserName());
						userAudit.createDeActivateAudit(new GiAuditParameter("User id : ", currAccountUser.getId()),
												  new GiAuditParameter("User Last Login : ", lastLogin.toString()),
								                  new GiAuditParameter("Status Changed : ", "Inactive-dormant")
						                         );
					}
				}
				expiryDayCount = 0;
				tmpRole = null;
			}catch(Exception e){
				
				LOGGER.error(e.getMessage(),e);
			}
		}
		return inactiveAccounts;
	}
	

		
	@Override
	@Transactional
	public List<String> migrateUsers(String userNamePattern)  {
		
		List<String> failedUsers=new ArrayList<String>();
		List<AccountUser> userList= null;
		if(userNamePattern.equals("*") || userNamePattern.equalsIgnoreCase("ALL")){
			userList =userRepository.findByUuidIsNull();
		}else{
			userList =userRepository.findByUuidIsNullAndUserNameStartingWithOrderByUserNameDesc(userNamePattern);
		}
		
		VimoEncryptor _VE = (VimoEncryptor) GHIXApplicationContext.getBean("vimoencryptor");
		
		
		
		Iterator<AccountUser> itrAccoutnUser=userList.iterator();
		while(itrAccoutnUser.hasNext()){
			AccountUser currAccountUser= itrAccoutnUser.next();
			
			try{
				String uniquesalt=UUID.randomUUID()+"";
				
				String userPassword=_VE.decrypt(currAccountUser.getPassword());
				//System.out.println("user Id:: "+currAccountUser.getId()+" user Name:: "+currAccountUser.getUserName()+" user pwd:: "+userPassword);
				userPassword=userPassword+uniquesalt;
				String encPwd = ghixEncryptorUtil.getEncryptedPassword(userPassword, uniquesalt);
				currAccountUser.setUuid(uniquesalt);
				currAccountUser.setLastUpdatedBy(currAccountUser.getId());
				currAccountUser=passwordService.updatePassword(currAccountUser, encPwd, true);
			}catch(Exception e){
				String failedRec=currAccountUser.getId()+" :: "+currAccountUser.getUserName();
				failedUsers.add(failedRec);
			}
			
			
		}
		
		return failedUsers;
	}

	
	@Override
	public Map<String, Object> searchUsers(Map<String, Object> searchCriteria) {
		
		QueryBuilder<AccountUser> query = delegateFactory.getObject();
		query.buildObjectQuery(AccountUser.class);
		
		List<AccountUser> userList = new ArrayList<AccountUser>();
		Map<String, Object> userListAndRecordCount = new HashMap<String, Object>();


		if (searchCriteria == null) 
		{
			return userListAndRecordCount;
		}
		

		String userName = (String) searchCriteria.get("userName");
		if (StringUtils.isNotEmpty(userName) && !userName.equalsIgnoreCase("any"))
		{
			query.applyWhere("userName", userName.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		
		String userType = (String) searchCriteria.get("userType");
		if(userType != null && userType.length() > 0 && !userType.equalsIgnoreCase("any")){
			query.applyWhere("userRole.role.name", userType, DataType.STRING,ComparisonType.EQUALS_CASE_IGNORE);
		}else{
			List<String> allRoleName = roleService.findAllManagedRoleNames();
			if(allRoleName != null && !allRoleName.isEmpty()) {
				query.applyWhere("userRole.role.name", roleService.findAllManagedRoleNames(), DataType.LIST,ComparisonType.EQUALS_CASE_IGNORE);
			}else {
				return userListAndRecordCount;
			}
		}
		

		//logic to hide skipped roles 
		//HIX-97616
		List<String> skipUserTypyes = (List<String>) searchCriteria.get("userTypeNotIn");
		if( skipUserTypyes != null ){
			Iterator<String> i = skipUserTypyes.iterator();
			while (i.hasNext()) {
				String skipedRole = i.next();
				query.applyWhere("userRole.role.name", skipedRole, DataType.STRING,ComparisonType.NE);
			}
		}
		
		//Logic ends
		
		
		
		query.applyWhere("userRole.isActive",ACTIVE_ROLE,DataType.CHAR,ComparisonType.EQUALS);

		String lastUpdated = (String) searchCriteria.get("lastUpdated");
		if (StringUtils.isNotEmpty(lastUpdated))
		{
			prepareDateCriteria(query, lastUpdated);
		}
		
		String userStatus = (String) searchCriteria.get("userStatus");
		if (StringUtils.isNotEmpty(userStatus) && !userStatus.equalsIgnoreCase("any"))
		{
			/*HIX-60819 - Now we have two different set of data. We need to compare both the column while searching.
			 * PHIX- status will be Active and Inactive but
			 * STATE - staus will be Active , Inactive and Inactive-dormant */
			if(userStatus.equalsIgnoreCase("Active")){
				query.applyWhere("status", "Active", DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
				query.applyWhere("confirmed", 1, DataType.NUMERIC, ComparisonType.EQ);
			}
			else /*if(userStatus.equalsIgnoreCase("InActive") || userStatus.equalsIgnoreCase("Inactive-dormant")  )*/{
				query.applyWhere("status", userStatus, DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
				query.applyWhere("confirmed", 0, DataType.NUMERIC, ComparisonType.EQ);
			}
			
		}
		
		
		query.applySort(searchCriteria.get("sortBy").toString(),
				SortOrder.valueOf(searchCriteria.get("sortOrder").toString()));

		try
		{
			query.setFetchDistinct(true);
			userList = query.getRecords(
					(Integer) searchCriteria.get("startRecord"),
					(Integer) searchCriteria.get("pageSize"));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in user service search users", e);
			
		}

		userListAndRecordCount.put("userList", userList);
		userListAndRecordCount.put("recordCount", query.getRecordCount());
		return userListAndRecordCount;
	}
	
	private void prepareDateCriteria(QueryBuilder<AccountUser> query, String lastUpdatedString) {
		Calendar calendarDate = null;
		Date effectiveEndDate = null;
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		
		if(StringUtils.isEmpty(lastUpdatedString)) {
			return;
		}
		
		try {
			effectiveEndDate = formatter.parse(lastUpdatedString);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			return;
		}

		calendarDate = TSCalendar.getInstance();
		calendarDate.setTime(effectiveEndDate);
		calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
		calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
		calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));
		
		query.applyWhere(COLUMN_UPDATED, calendarDate.getTime(), DataType.DATE, ComparisonType.LE);
		
		return;
	}

	/**
	 * This is an ambiguous method, I am not sure about the purpose of calling module
	 * Whether it is for update or view
	 */
	@Override
	public List<String> findAllUsersRoles() {
		
		return userRoleService.getAllUsersRoles();
	}

	@Override
	public List<AccountUser> getUsersByRoleName(String roleName) {
		
		return userRepository.getUsersByRoleName(roleName);
	}
	
	@Override
	public void changeUserStatus(int status,int id) {
			userRepository.changeUserStatus(status, id);
	}
	
	@Override
	public void updateStatusCode(String status,int id) {
			userRepository.updateStatusCode(status, id);
	}
	
	@Override
	public List<ModuleUser> getModuleUsersByModuleIds(List<Integer> moduleIdList,
			String moduleName) {
		return moduleUserRepository.getModuleUsersByModuleIdsAndModuleName(moduleIdList, moduleName);
	}
	
	/*
	 * Saves the FFM ID
	 * @see com.getinsured.hix.platform.security.service.UserService#saveFfmUserId(int, java.lang.String)
	 * Fix me: The parameter id should be long
	 */
	@Override
	@Transactional
	public void saveFFMUserId(int id, String ffmUserId) {
		AccountUser userObj = null;
		try {
			if (userRepository.exists(id)) {
				userObj = userRepository.findById(id);
				userObj.setFfmUserId((ffmUserId));
				int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : userObj.getId();
				userObj.setLastUpdatedBy(updatedBy);
				userRepository.save(userObj);
			}
		}

		catch (Exception ex) {
			LOGGER.error("User FFM ID set up failed, UserId :: " + id, ex);
		}

	}

	
	@Override
	public boolean hasPrincipalChanged(String userName) {
		boolean principalChanged=false;
		
		if(SecurityContextHolder.getContext()!=null && SecurityContextHolder.getContext().getAuthentication()!=null){
			AccountUser principal= (AccountUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			if(principal!=null){
				if(!StringUtils.equals(principal.getUserName(),userName)){
					principalChanged= true;
				}
			}
			
		}
		return principalChanged;
	}
	
	@Override
	public void setTenantContext(String userName) throws UsernameNotFoundException {
		//LOGGER.info("Set TenantContex for UserName :: "+userName );
		
		AccountUser tenantUser = null;
		try {
			tenantUser = userRepository.findByUserName(userName.toLowerCase());
			
			if (tenantUser == null) {
				LOGGER.error("Tenant User Not Found :: ");
	            throw new UsernameNotFoundException(userName);
	        }
			
			SecurityContextHolder.clearContext();
			GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();
			UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(tenantUser,
					tenantUser.getPassword(), authoritiesMapper.mapAuthorities(tenantUser.getAuthorities()));
	        result.setDetails(tenantUser);//authentication.getDetails());
	        
			SecurityContextHolder.getContext().setAuthentication(result);
			 
			 
			//LOGGER.info("TenantContex set to UserName :: "+this.getLoggedInUser().getUserName() );
		} catch (Exception ex) {
        	LOGGER.error("Exception : " + ex.getMessage(),ex);
            throw new AuthenticationServiceException(ex.getMessage(), ex);
        }
		
	}

	@Override
	public void signOutTenantContext(String userName) {
		
		if(!hasPrincipalChanged(userName)){
			SecurityContextHolder.clearContext();
		}
	}

	@Override
	public List<String> getIssuerRepresentativeEmailIds(int userRoleId) {
		return userRepository.getIssuerRepresentativeEmailIds(userRoleId);
	}

	@Override
	public Set<UserRole> getNonDefaultUserRole(AccountUser user) {
		//Added for HIX-27184 and HIX-27305
		Set<UserRole> nonDefaultRoles = new HashSet<UserRole>();
		Set<UserRole> userRoleList = user.getUserRole();
		for (UserRole userRole : userRoleList) {
			if (!userRole.isDefaultRole()) {
				nonDefaultRoles.add(userRole);
			}
		}
		return nonDefaultRoles;
	}

	@Override
	public Set<Role> getAllRolesOfUser(AccountUser user) {
		Set<Role> userRoles = new HashSet<Role>();
		for (UserRole userRole : user.getUserRole()) {
			userRoles.add(userRole.getRole());
		}
		return userRoles;
	}

	@Override
	public void updateRetryCount(AccountUser user, int retryCount) {
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setRetryCount(retryCount);
					int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : userObj.getId();
					userObj.setLastUpdatedBy(updatedBy);
					userRepository.save(userObj);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user retry count. ", ex);
		}
		
	}

	@Override
	public void updateConfirmedAndRetryCount(AccountUser user, int confFlag,
			int retryCount) {
		
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setConfirmed(confFlag);
					userObj.setStatus("Inactive");
					userObj.setRetryCount(retryCount);
					int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : userObj.getId(); 
					userObj.setLastUpdatedBy(updatedBy);
					userRepository.save(userObj);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user retry count. ", ex);
		}
		
	}
	
	@Override
	public void updateUserStatusWithRetries(AccountUser user, int confFlag, String status, int retryCount) {
		
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setConfirmed(confFlag);
					userObj.setStatus(status);
					userObj.setRetryCount(retryCount);
					int updatedBy = (null != getLoggedInUser()) ? getLoggedInUser().getId() : userObj.getId(); 
					userObj.setLastUpdatedBy(updatedBy);
					userRepository.save(userObj);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user retry count. ", ex);
		}
		
	}

  	@Override
	public UserRole updateUserRole(AccountUser user,UserRole toUpdateUserRole) {
		//Added for HIX-27184 and HIX-27305
		UserRole userrole; 
		try {
			userrole = userRoleService.saveUserRole(toUpdateUserRole);
			if(userrole != null){
				user.getUserRole().add(userrole);
			}
		}catch (Exception ex) {
			LOGGER.error("UserRole update failed. User Id: " + user.getId(), ex);
			throw new GIRuntimeException("PLATFORM-00003",ex,null,Severity.HIGH);
		}
		return userrole;
	}

	@Override
	public UserRole getUserRoleByRoleName(AccountUser user, String roleName){
		//Added for HIX-27305 and HIX-27184 
		Set<UserRole> userRoles = user.getUserRole();
		UserRole retuenUserRole = null;
		for (UserRole userRole : userRoles) {
			if(userRole.getRole().getName().equalsIgnoreCase(roleName)){
				retuenUserRole = userRole;
			}
		}
		return retuenUserRole;
	}
	
	/**
	 * Returns true if the User had assigned to given active role Name,else returns
	 * false
	 * Added for HIX-27305 and HIX-27184 
	 * @param user
	 *            AccountUser object
	 * @param roleName
	 *            String object
	 * @return boolean
	 */
	@Override
	public boolean hasActiveUserRole(AccountUser user, String roleName) {
		Set<UserRole> userRoles = user.getUserRole();
		for (UserRole userRole : userRoles) {
			if (StringUtils.equalsIgnoreCase(roleName, userRole.getRole().getName()) && userRole.isActiveUserRole()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * HIX-28851 Application error is displayed on switching from Agent to Employer
	 */

	@Override
	public ModuleUser getUserSelfSignedModule(String defModuleName, int userId) {
			ModuleUser moduleUser = moduleUserRepository.findUserSelfSignedModule(defModuleName.toLowerCase(), userId);
			return moduleUser;
	
	}
	
	@Override
	public void updateSecQueRetryCount(AccountUser user, int secQueRetryCount) {
		try {
			userRepository.updateSecQueRetryCount(secQueRetryCount, user.getId());
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user retry count. ", ex);
		}
	}

	@Override
	public void updateLastLogin(AccountUser user) { 
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setLastLogin(new TSDate());
					userObj.setLastUpdatedBy(userObj.getId());
					userObj.setRetryCount(0);
					userRepository.save(userObj);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user last login. ", ex);
		}
	}
	
	@Override
	public AccountUser markAccountInactive(AccountUser user) { 
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setConfirmed(0);
					userObj.setStatus("Inactive-dormant");
					if(null != getLoggedInUser()){
						userObj.setLastUpdatedBy(getLoggedInUser().getId());
					}else{
						userObj.setLastUpdatedBy(this.getSuperUserId("batchadmin@ghix.com"));
					}
					userObj = userRepository.save(userObj);
				}
				return userObj;
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user last login. ", ex);
		}
		return user;
	}

	@Override
	public void invalidateConfirmed(AccountUser user) { 
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setConfirmed(0);
					if(null != getLoggedInUser()){
						userObj.setLastUpdatedBy(getLoggedInUser().getId());
					}
					userRepository.save(userObj);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user last login. ", ex);
		}
	}
	
	@Override
	public void updateConfirmedAndSecQueRetryCount(AccountUser user,
			int confFlag, int secQueRetryCount) {
		try {
			userRepository.updateConfirmedAndSecQueRetryCount(confFlag, secQueRetryCount, user.getId());
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user retry count. ", ex);
		}
		
	}

	@Override
	public void updateRetryCounts(AccountUser user, int retryCount,
			int secQueRetryCount) {
		try {
			if (user != null)
			{
				AccountUser userObj = userRepository.findById(user.getId());
				if (userObj!=null) {
					userObj.setRetryCount(retryCount);
					userObj.setSecQueRetryCount(secQueRetryCount);
					userObj.setLastUpdatedBy(userObj.getId());
					userRepository.save(userObj);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error while updating user retry count. ", ex);
		}
		
	}
	
	/**
	 * We do not manage the password is SCIM is used, passwords owned by 
	 * the identity management solution 
	 */
	
	@Override
	public boolean changePassword(int userId, String newPassword) {
		AccountUser userObj = userRepository.findById(userId);
		boolean success = false;
		try {
			if (userObj != null) {
				RequestHandler handler = this.handlerFactory.getHandlerByName(PasswordHandler.NAME);
				if (handler == null) {
					LOGGER.error("No password handler availble for name:" + PasswordHandler.NAME);
				}
				HashMap<String, Object> context = new HashMap<String, Object>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, userObj);
				context.put(HandlerConstants.PASSWORD_KEY, newPassword);
				context.put(PasswordHandler.NEW_PASSWORD, newPassword);
				context.put(PasswordHandler.PASSWORD_OPRATION_KEY, PasswordHandler.CHANGE_ADMIN);
				TenantDTO tenantDto = TenantContextHolder.getTenant();
				if (tenantDto != null) {
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
					context.put(RequestHandler.TENANT_DTO_KEY, tenantDto);
				}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}

				success = handler.handleRequest(context, null, false);
				if (!success) {
					LOGGER.error("Password CHANGE failed with error "
							+ (String) context.get(HandlerConstants.HANDLER_ERROR));
					String errMsg = (String) context.get(HandlerConstants.HANDLER_EXCEPTION);
					if (errMsg != null) {
						LOGGER.error("Password CHANGE failed message :" + errMsg);
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Password Change failed, UserId :: " + userId, ex);
		}
		return success;
	}
	
	@Override
	public boolean changePasswordAdmin(int userId, String newPassword) {
		AccountUser userObj = userRepository.findById(userId);
		boolean success = false;
		try {
			if (userObj != null) {
				RequestHandler handler = this.handlerFactory.getHandlerByName(PasswordHandler.NAME);
				if (handler == null) {
					LOGGER.error("No password handler availble for name:" + PasswordHandler.NAME);
				}
				HashMap<String, Object> context = new HashMap<String, Object>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, userObj);
				context.put(PasswordHandler.NEW_PASSWORD, newPassword);
				// context.put(HandlerConstants.CURRENT_PASSWORD_KEY,
				// currentPassword);
				context.put(PasswordHandler.PASSWORD_OPRATION_KEY, PasswordHandler.CHANGE_ADMIN);

				TenantDTO tenantDto = TenantContextHolder.getTenant();
				if (tenantDto != null) {
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
					context.put(RequestHandler.TENANT_DTO_KEY, tenantDto);
				}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}

				success = handler.handleRequest(context, null, false);
				if (!success) {
					LOGGER.error("Password CHANGE failed with error "
							+ (String) context.get(HandlerConstants.HANDLER_ERROR));
					String errMsg = (String) context.get(HandlerConstants.HANDLER_EXCEPTION);
					if (errMsg != null) {
						LOGGER.error("Password CHANGE failed message :" + errMsg);
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Password Change failed, UserId :: " + userId, ex);
		}
		return success;
	}
	
	@Override
	public boolean changePasswordSelf(int userId, String existingPassword, String newPassword) {
		AccountUser userObj = userRepository.findById(userId);
		boolean success=false;
		try {
			if (userObj != null) {
				RequestHandler handler = this.handlerFactory.getHandlerByName(PasswordHandler.NAME);
				if (handler == null) {
					LOGGER.error("No password handler availble for name:" + PasswordHandler.NAME);
				}
				HashMap<String, Object> context = new HashMap<String, Object>();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, userObj);
				context.put(PasswordHandler.NEW_PASSWORD,newPassword );
				context.put(PasswordHandler.CURRENT_PASSWORD, existingPassword);
				context.put(PasswordHandler.PASSWORD_OPRATION_KEY, PasswordHandler.CHANGE_SELF);
				TenantDTO tenantDto = TenantContextHolder.getTenant();
				if (tenantDto != null) {
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
					context.put(RequestHandler.TENANT_DTO_KEY, tenantDto);
				}else{
				context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}

				success = handler.handleRequest(context, null, false);
				if (!success) {
					LOGGER.error("Password CHANGE failed with error "
							+ (String) context.get(HandlerConstants.HANDLER_ERROR));
					String errMsg = (String) context.get(HandlerConstants.HANDLER_EXCEPTION);
					if (errMsg != null) {
						LOGGER.error("Password CHANGE failed message :" + errMsg);
					}
				}else{
					LOGGER.info("Password Updated successfully");
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Password Change failed, UserId :: " + userId, ex);
		}
		return success;
	}
	
	@Override
	public List<Map<String, String>> loadUserHistory(String userName) {
		
		AccountUser accountUser = userRepository.findByUserName(userName);
		
		List<Map<String, String>> displayCols = new ArrayList<Map<String, String>>();
		
		
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(COLUMN_CREATED,"");
		requiredFieldsMap.put(COLUMN_UPDATED,"");
		requiredFieldsMap.put(COLUMN_ID,"");
		requiredFieldsMap.put(COLUMN_EMAIL,"");
		requiredFieldsMap.put(COLUMN_PASSWORD,"");
		
		List<String> compareColumns = new ArrayList<String>();
		compareColumns.add(COLUMN_PASSWORD);

		try {
			DisplayAuditService service = objectFactory.getObject(); 
			service.setApplicationContext(appContext);
			List<Map<String, String>> userRevisionData = service.findRevisions(userRepository, ACCOUNT_USER_MODEL, requiredFieldsMap,accountUser.getId());
			if(userRevisionData!=null && userRevisionData.size()==0){
				requiredFieldsMap.put(COLUMN_CREATED,accountUser.getCreated().toString());
				requiredFieldsMap.put(COLUMN_UPDATED,accountUser.getUpdated().toString());
				requiredFieldsMap.put(COLUMN_ID,String.valueOf(accountUser.getId()));
				requiredFieldsMap.put(COLUMN_EMAIL,accountUser.getUsername());
				requiredFieldsMap.put(COLUMN_PASSWORD,accountUser.getPassword());
				userRevisionData = new ArrayList<Map<String, String>>();
			    userRevisionData.add(0, requiredFieldsMap);
			}
			
			List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(userRevisionData);
			Collections.reverse(orderedData);
			int size = orderedData.size();
			Map<String, String> firstElement = null;
			//Map<String, String> secondElement = null;
			Map<String, String> firstElementCmp = null;
			Map<String, String> secondElementCmp = null;
			firstElement = orderedData.get(0);
			
			for (int i=0; i<size-1;i++)
			{
				firstElementCmp = orderedData.get(i);
				secondElementCmp = orderedData.get(i+1);
				for (String keyColumn : compareColumns)
				{
					boolean valueChanged = false;
					if(keyColumn.equals(COLUMN_PASSWORD)){
						valueChanged = isEqual(firstElementCmp,secondElementCmp,keyColumn);
					}
					if(!valueChanged)
					{
						displayCols.add(firstElementCmp);
					}
				}
			}
			
			if(size == USER_SIZE || (displayCols!=null && displayCols.size()==0))
			{
				//secondElement = firstElement;
				displayCols.clear();
				if(secondElementCmp == null){
					displayCols.add(firstElement);
				} else {
					displayCols.add(secondElementCmp);
				}
				
			}
			
			
			//LOGGER.info("displayCols-------->"+displayCols);
		} catch (GIRuntimeException e) {
			LOGGER.error("Error while loading Issuer history : "+ e.getMessage());
		}
		return displayCols;
	
	}
	
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) 
	{
		return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
	}

	@Override
	public Set<UserRole> getActiveUserRole(AccountUser user) {
		//Added for HIX-30004
		Set<UserRole> activeUserRoles = new HashSet<UserRole>();
		Set<UserRole> userRoleList = user.getUserRole();
		for (UserRole userRole : userRoleList) {
			if (userRole.isActiveUserRole()) {
				activeUserRoles.add(userRole);
			}
		}
		return activeUserRoles;
	}

	@Override
	public boolean isPasswordPresentInHistory(AccountUser userObj, String newPassword, int passwordHistoryLimit){
		boolean passwordInHistory = true;
		
		if(userObj != null){
			String userUuid = userObj.getUuid();
			newPassword = newPassword + userUuid;
			String encPwd = ghixEncryptorUtil.getEncryptedPassword(newPassword,	userUuid);
			LOGGER.debug("isPasswordPresentInHistory: " + encPwd);
			List<String> listPasswords = usersAudRepository.getPasswordsFromHistory(new PageRequest(0,passwordHistoryLimit), userObj.getId());
			if(encPwd != null && !encPwd.isEmpty() && listPasswords != null){
				LOGGER.debug("isPasswordPresentInHistory: " + listPasswords.size());
				passwordInHistory = listPasswords.contains(encPwd);
			}
		}
		LOGGER.debug("passwordInHistory: " + passwordInHistory);
		return passwordInHistory;
	}
	
	@Override
	public Set<String> getUserPasswordHistory(String userName) {
		AccountUser accountUser = userRepository.findByUserName(userName);
		
		Set<String> distinctPasswords =  new LinkedHashSet<String>();
		
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(COLUMN_CREATED,"");
		requiredFieldsMap.put(COLUMN_UPDATED,"");
		requiredFieldsMap.put(COLUMN_ID,"");
		requiredFieldsMap.put(COLUMN_EMAIL,"");
		requiredFieldsMap.put(COLUMN_PASSWORD,"");
		
		
		DisplayAuditService service = objectFactory.getObject();     
		service.setApplicationContext(appContext);
		List<Map<String, String>> userRevisionData = service.findRevisions(userRepository, ACCOUNT_USER_MODEL, requiredFieldsMap,accountUser.getId());
		
		if(userRevisionData!=null && userRevisionData.size()==0){
			requiredFieldsMap.put(COLUMN_CREATED,accountUser.getCreated().toString());
			requiredFieldsMap.put(COLUMN_UPDATED,accountUser.getUpdated().toString());
			requiredFieldsMap.put(COLUMN_ID,String.valueOf(accountUser.getId()));
			requiredFieldsMap.put(COLUMN_EMAIL,accountUser.getUsername());
			requiredFieldsMap.put(COLUMN_PASSWORD,accountUser.getPassword());
			userRevisionData = new ArrayList<Map<String, String>>();
		    userRevisionData.add(0, requiredFieldsMap);
		}
		
		List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(userRevisionData);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> element = null;
		for(int i=0;i<size;i++){
			element = orderedData.get(i);
			distinctPasswords.add(element.get(COLUMN_PASSWORD));
		}
		return distinctPasswords;
	}
	/*
	 * The following implementation was commented by Venkata Tadepalli
	 * (while code review) : May 09, 2014
	 * 
	 *
	public Set<String> getUserPasswordHistoryTBD(String userName) {
		AccountUser accountUser = userRepository.findByUserName(userName);
		
		Set<String> distinctPasswords =  new LinkedHashSet<String>();
		
		
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(COLUMN_CREATED,"");
		requiredFieldsMap.put(COLUMN_UPDATED,"");
		requiredFieldsMap.put(COLUMN_ID,"");
		requiredFieldsMap.put(COLUMN_EMAIL,"");
		requiredFieldsMap.put(COLUMN_PASSWORD,"");
		
		
		DisplayAuditService service = objectFactory.getObject();     
		List<Map<String, String>> userRevisionData = service.findRevisions(I_USER_REPOSITORY, ACCOUNT_USER_MODEL, requiredFieldsMap,accountUser.getId());
		
		if(userRevisionData!=null && userRevisionData.size()==0){
			requiredFieldsMap.put(COLUMN_CREATED,accountUser.getCreated().toString());
			requiredFieldsMap.put(COLUMN_UPDATED,accountUser.getUpdated().toString());
			requiredFieldsMap.put(COLUMN_ID,String.valueOf(accountUser.getId()));
			requiredFieldsMap.put(COLUMN_EMAIL,accountUser.getUsername());
			requiredFieldsMap.put(COLUMN_PASSWORD,accountUser.getPassword());
			userRevisionData = new ArrayList<Map<String, String>>();
		    userRevisionData.add(0, requiredFieldsMap);
		}
		
		List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(userRevisionData);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> element = null;
		for(int i=0;i<size;i++){
			element = orderedData.get(i);
			distinctPasswords.add(element.get(COLUMN_PASSWORD));
		}
		return distinctPasswords;
	}
*/
	/**
     * update Individual User As Dangling
     * @param userName
     * @throws GIException
     */
	@Override
    public void updateIndividualUserAsDangling(int userId) throws GIException{
           AccountUser accountUser = userRepository.findById(userId);
           
           if (accountUser == null){
                  throw new GIException("No user found for supplied userId - " + userId);
           }
           List<ModuleUser> moduleUsers = moduleUserRepository.findUserModules(accountUser.getId());
           
           boolean danglingIndividual = true;
           boolean otherTypeOfUserFound = false;
           for (ModuleUser moduleUser : moduleUsers) {
                  if ("individual".equalsIgnoreCase(moduleUser.getModuleName())){
                        if (moduleUser.getModuleId() != 0){
                               danglingIndividual = false;
                               break;
                        }
                  } else if ("individuals".equalsIgnoreCase(moduleUser.getModuleName())) {
                	  /*ignore*/
                	  continue;
                  } else {
                        otherTypeOfUserFound = true;
                        break;
                  }
                  
           }
           
           if (!danglingIndividual){
                  throw new GIException("Module User for type individual found for supplied userId - " + userId);
           }
           
           if (otherTypeOfUserFound){
                  throw new GIException("Module User for type other than individual found for supplied userId - " + userId);
           }
           
           accountUser.setEmail(accountUser.getEmail() + "_ul" + accountUser.getId());
           accountUser.setUserName(accountUser.getUserName() + "_ul" + accountUser.getId());
           accountUser.setUpdated(new TSDate());
           accountUser.setConfirmed(0);
           accountUser.setStatus("Inactive");
           accountUser.setExtnAppUserId(null);
           userRepository.save(accountUser);
           
    }
	
	@Override
	public void saveFFMDetails(String ffmUsername,String ffmPassword,String proxy,int userId)
	{  

		SecurableTarget securableTarget;
	    Integer targetId = userId;
	    String targetName = "USER";
	    securableTarget = iSecurablesTargetRepository.findByTargetIdAndTargetType(targetId,
					SecurableTarget.TargetName.USER);
	if (securableTarget == null) {
		securableTarget = new SecurableTarget();
		securableTarget.setTargetId(targetId);
		securableTarget.setTargetName(SecurableTarget.TargetName.valueOf(targetName));
		securableTarget.setCreated(new TSDate());
		securableTarget.setUpdated(new TSDate());
		
	}
	createAndSaveUserInfos(ffmUsername, ffmPassword, proxy, securableTarget);
			
	}	
		
	private List<Securables> createAndSaveUserInfos(String ffmUsername,String ffmPassword,String proxy, SecurableTarget securableTarget) {
	List<Securables> securableList = null;
		
		
		if(securableTarget.getSecurables() == null || securableTarget.getSecurables().size() == 0 ){
			securableList = new ArrayList<Securables>();
		}
		else{
			securableList = securableTarget.getSecurables();
		}
		
		Securables securables = new Securables();
	
		//Setting logged in user's id
		if(StringUtils.isNotEmpty(ffmUsername)){
		 securables.setUserName(ffmUsername);
		}
		
		if(StringUtils.isNotEmpty(ffmPassword)){
		 securables.setPassword(ffmPassword);
		}
		securables.setApplicationData(proxy);
		securables.setSecurableTarget(securableTarget);
		
		securableList.add(securables);
		securableTarget.setSecurables(securableList);
		
		iSecurablesTargetRepository.save(securableTarget);
		return securableTarget.getSecurables();	
	}
	
	@Override
	public void updateFFMDetails(String ffmUsername, String ffmPassword,String proxy,int userId) {

		SecurableTarget securableTarget;
		Integer targetId = userId;
		// String targetName = "CMR";
		 securableTarget = iSecurablesTargetRepository.findByTargetIdAndTargetType(targetId,
					SecurableTarget.TargetName.USER);
		if (securableTarget != null) {

			updateUserInfo(ffmUsername, ffmPassword, proxy,securableTarget,targetId);

		}

	}

	private List<Securables> updateUserInfo(String ffmUsername,
			String ffmPassword,String proxy, SecurableTarget securableTarget,Integer targetId) {
		List<Securables> securableList = null;

		if (securableTarget.getSecurables() == null
				|| securableTarget.getSecurables().size() == 0) {
			securableList = new ArrayList<Securables>();
		} else {
			securableList = securableTarget.getSecurables();
		}

		Securables securables = iSecurablesRepository.findExistedFfmUserInfoByTargetId(targetId);
		
		if (StringUtils.isNotEmpty(ffmUsername)) {
			securables.setUserName(ffmUsername);
		}

		if (StringUtils.isNotEmpty(ffmPassword)) {
			securables.setPassword(ffmPassword);
		}
		if (StringUtils.isNotEmpty(proxy)) {
			securables.setApplicationData(proxy);
		}

		securables.setSecurableTarget(securableTarget);

		securableList.add(securables);
		securableTarget.setSecurables(securableList);

		iSecurablesTargetRepository.save(securableTarget);
		return securableTarget.getSecurables();
	}
	
	@Override
	public List<Object[]> getAccountUsersInfoByUserRole(int roleId) {
		return userRepository.getUsersInfoByRoleId(roleId);
	}
	
	 @Override
	public List<Object[]> getAccountUsersInfoByRoleAndName(int roleId,
			String userName) {
		String name = userName;
		if (null != name) {
			name = userName.toUpperCase();
		}
		return userRepository.getUsersInfoByRoleAndUserName(roleId, name);

	}
	@Override
	public int getSuperUserId(String emailAddress)
	{
		int userId = 0;
		if(emailAddress != null)
		{
			List<Integer> userIds = userRepository.userIdsFromEmail(emailAddress);
			if(userIds != null && userIds.size() > 0)
			{
				userId = userIds.get(0);
			}
		}
		
		return userId;
	}
	@Override
	public List<AccountUser> findAccountUsersForRoleNames(List<String> roleNameList){
		return userRepository.findAccountUsersForRoleNames(roleNameList);
	}
	@Override
	public List<Integer> getAccountUserIdsForRoleNames(List<String> roleNameList,Long tenantId){
		List<String> filteredList = new ArrayList<String>();
		for(String roleName: roleNameList){
			try{
				this.accessControl.canAddOrUpdateRole(roleName);
				filteredList.add(roleName);
			}catch(Exception e){
//				Ignored - Deliberately as we don't want to process this role
				LOGGER.error("Not allowed to change this role name: "+roleName,e);
			}
			
		}
		return iTenantUnawareUserRepository.getAccountUserIdsForRoleNames(filteredList,tenantId);
	}
	
	@Override
	public Map<Integer, String> getWorkgroupUsersToAdd(String username) {
		List<Object[]> userList = userRepository.getWorkgroupUsersToAdd(username);
		Map<Integer, String> mapUserDto= new HashMap<>();
		
		if(userList != null) {
			for (Object[] userData : userList) {	
				mapUserDto.put((int) userData[0],(String) userData[1]);
			}
		}
		
		return mapUserDto;
	}




	@Override
	public void checkUpdatesAllowed(AccountUser user) {
		Set<UserRole> userRoles = user.getUserRole();
		Iterator<UserRole> cursor = userRoles.iterator();
		UserRole temp;
		String roleName = null;
		while(cursor.hasNext()){
			temp =cursor.next();
			if(temp.isDefaultRole()){
				roleName = temp.getRole().getName();
				if(!this.accessControl.canUpdateUserDetails(temp.getRole().getName())){
					throw new GIRuntimeException("Permission Denied current user can not update user's having role:"+roleName);
				}
			}
		}
	}

	@Override
	public boolean hasAccessToUpdateUser(AccountUser user) {
		boolean canUpdateUser = false;
		try {
			checkUpdatesAllowed(user);
			canUpdateUser = true;
		} catch (GIRuntimeException e) {
			LOGGER.error("Permission Denied: Current user can not update roles", e);
		}
		return canUpdateUser;

	}



	@Override
	public void checkIfRoleUpdateAllowed(AccountUser user) {
		Set<UserRole> userRoles = user.getUserRole();
		Iterator<UserRole> cursor = userRoles.iterator();
		UserRole temp;
		String roleName = null;
		IS_EXCLUSIVE isExclusive = IS_EXCLUSIVE.N;
		while(cursor.hasNext()){
			temp =cursor.next();
			if(temp.isDefaultRole()){
				roleName = temp.getRole().getName();
				isExclusive = temp.getRole().getIsExclusive();
				if(isExclusive.name().equals(IS_EXCLUSIVE.Y.name()) || !this.accessControl.canAddOrUpdateRole(roleName)){
					throw new GIRuntimeException("Permission Denied current user can not Add roles, role is either exclusive or not allowed for current user");
				}
			}
		}
		
	}

	@Override
	public void updateUserStatusAndConfirmedFields(List<Integer> userIds, String status,Integer confirmed) {
			userRepository.updateUserStatusAndConfirmedFields(userIds, status, confirmed);
	}
	
	@Override
	@GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.ACCOUNT_SETTINGS, eventName = EventNameEnum.PII_WRITE)
	public boolean changePasswordOnWSO2(HttpServletRequest request, String newPassword,  String currentPassword) throws GIException, WSO2Exception, InvalidUserException {
			boolean passwordChanged = false;
			AccountUser domainUser = userRepository.findByUserName(getLoggedInUser().getUsername());
			RequestHandler handler = this.handlerFactory.getHandlerByName(PasswordHandler.NAME);
			HashMap<String, Object> context = new HashMap<>();
			if(handler != null){
				TenantDTO tenantDto = TenantContextHolder.getTenant();
				context.put(HandlerConstants.ACCOUNT_USER_KEY, domainUser);
				context.put(PasswordHandler.PASSWORD_OPRATION_KEY, PasswordHandler.CHANGE_SELF);
				context.put(PasswordHandler.NEW_PASSWORD,newPassword );
				context.put(PasswordHandler.CURRENT_PASSWORD, currentPassword);
				if(tenantDto != null){
					context.put(RequestHandler.TENANT_DTO_KEY, tenantDto);
					context.put(HandlerConstants.WSO2_ENABLED_KEY, tenantDto.isRemotelyManagedUsers());
				}else{
					context.put(HandlerConstants.WSO2_ENABLED_KEY, this.scimEnabled);
				}
				passwordChanged = handler.handleRequest(context, null, false);
			}
			if(passwordChanged){
				LOGGER.info("Using SCIM Manager to update the password for remote user");
				domainUser.setPassword("PASSWORD_NOT_IN_USE");
				domainUser.setPasswordLastUpdatedTimeStamp(new TSDate());
				domainUser.setRetryCount(0);
				domainUser.setSecQueRetryCount(0);
				domainUser.setConfirmed(1);
				domainUser.setPasswordRecoveryToken(null);
				domainUser.setPasswordRecoveryTokenExpiration(null);
				domainUser.setLastUpdatedBy(getLoggedInUser().getId());
				userRepository.save(domainUser);
			}else {
				LOGGER.error("Password CHANGE failed with error "
						+ (String) context.get(HandlerConstants.HANDLER_ERROR));
				String errMsg = (String) context.get(HandlerConstants.HANDLER_EXCEPTION);
				if (errMsg != null) {
					LOGGER.error("Password CHANGE failed message :" + errMsg);
				}
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
							new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Unable to change password for remote user"));
					throw new GIException("Unable to change password for remote user:" + errMsg);
			}
				
			return passwordChanged;
		}

	@Override
	public AccountUser getBasicInfoByUserName(String userName){
		return userRepository.getUserBasicInfo(userName);
	}
	
	@Override
	public AccountUser findByPhoneAndUserName(String phone, String userName){
		return userRepository.findByPhoneAndUserName(phone, userName);
	}
	
	@Override
	public List<AccountUser> findByPhone(String phone){
		return userRepository.findByPhone(phone);
	}
	
	@Override
	public AccountUser findByUserNpn(String userNPN){
		return userRepository.findByUserNPN(userNPN);
	}
	@Override
	public ModuleUser createOrUpdateModuleUser(int moduleId, String moduleName, AccountUser user, boolean isSelfSigned) throws GIException {
		String moduleNameInLowerCase=moduleName.toLowerCase();
		LOGGER.info("Creating / Module ("+moduleNameInLowerCase+") for the User :"+user.getId()+"::..");
		
		ModuleUser newModuleUser = null;
		 /*if(isSelfSigned){
			LOGGER.debug("Creating Self Signed Module ("+moduleNameInLowerCase+") for the User :"+user.getId()+"::..");
			  /*newModuleUser = this.getUserSelfSignedModule(moduleNameInLowerCase,user.getId());
			
		 	if(newModuleUser!=null){
				
				throw new GIException("Invalid Self Signed Module creation :: "+
						"Already exists Self Signed Module ("+moduleNameInLowerCase+") for the User :"+user.getUserName());
			}
		} */
		List<ModuleUser>  existingModuleUsers=getModuleUsers(moduleId,moduleNameInLowerCase);

		if (existingModuleUsers == null || existingModuleUsers.size()==0 ) {
			String selfSignedFlag = (isSelfSigned) ? "Y" : "N" ;
			newModuleUser = new ModuleUser();
			newModuleUser.setModuleId(moduleId);
			newModuleUser.setModuleName(moduleNameInLowerCase);
			newModuleUser.setUser(user);
			newModuleUser.setIsSelfSigned(selfSignedFlag);

			newModuleUser = moduleUserRepository.save(newModuleUser);
			LOGGER.info("Created Module ("+moduleNameInLowerCase+") for the User ::"+user.getId()+"::...");
		}
		else
		{
			for(ModuleUser existingModuleUser :existingModuleUsers ) {
			String selfSignedFlag = (isSelfSigned) ? "Y" : "N" ;
			existingModuleUser.setModuleId(moduleId);
			existingModuleUser.setModuleName(moduleNameInLowerCase);
			existingModuleUser.setUser(user);
			existingModuleUser.setIsSelfSigned(selfSignedFlag);
			existingModuleUser = moduleUserRepository.save(existingModuleUser);
			LOGGER.info("Updated Module ("+moduleNameInLowerCase+") for the User ::"+user.getId()+"::...");
			}
		}
		
		return newModuleUser;

	}
	
	@Override
	public List<ModuleUser> getUserSelfSignedModules(String defModuleName, int userId) {
			List<ModuleUser> moduleUsers = moduleUserRepository.findUserSelfSignedModules(defModuleName.toLowerCase(), userId);
			return moduleUsers;
	
	}




	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext=applicationContext;
		
	}


	@Override
	public List<Object[]> getAccountUsersInfoByName(String userName) {
		String name = userName;
		if (null != name) {
			name = userName.toUpperCase();
		}
		return userRepository.getUsersInfoByUserName(name);

	}
}

 
