package com.getinsured.hix.platform.accountactivation.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * Repository for AccountActivation entity.
 * 
 * @author Ekram Ali Kazi
 *
 */
@Repository
@Transactional(readOnly = true)
public interface IAccountActivationRepository extends TenantAwareRepository<AccountActivation, Integer> {

	AccountActivation findByActivationToken(String token);
	
	AccountActivation findById(Integer activationId);
	
	@Query("SELECT t FROM AccountActivation t WHERE t.createdObjectType IN('ADMIN','BROKER_ADMIN','ISSUER_ADMIN','CSR')")
	List<AccountActivation> findAllAdminUsers();

	AccountActivation findByUsername(String userName);
	
	@Query("SELECT accountActivation FROM AccountActivation accountActivation WHERE accountActivation.username = :userName " +
			"and accountActivation.status = 'NOTPROCESSED'")
	List<AccountActivation> findUnProcessedByUsername(@Param("userName")String userName);
	
	//To sync with PHIX-MAIN implementation
	@Query("SELECT accountActivation FROM AccountActivation accountActivation WHERE accountActivation.createdObjectId = :createdObjectId " +
			"and accountActivation.createdObjectType = :createdObjectType")
	AccountActivation findByCreatedObjectId(@Param("createdObjectId")Integer createdObjectId,@Param("createdObjectType")String createdObjectType);
	
	@Query("SELECT accountActivation FROM AccountActivation accountActivation WHERE accountActivation.createdObjectId = :createdObjectId " +
			"and accountActivation.createdObjectType = :createdObjectType and accountActivation.creatorObjectId = :creatorObjectId and accountActivation.creatorObjectType = :creatorObjectType")
	AccountActivation findByCreatedObjectId(@Param("createdObjectId")Integer createdObjectId,@Param("createdObjectType")String createdObjectType,
			@Param("creatorObjectId")Integer creatorObjectId,@Param("creatorObjectType")String creatorObjectType);
	
	@Query("SELECT accountActivation FROM AccountActivation accountActivation WHERE accountActivation.createdObjectId = :createdObjectId " +
			"and accountActivation.creatorObjectId = :creatorObjectId and accountActivation.createdObjectType = :createdObjectType and accountActivation.creatorObjectType = :creatorObjectType and accountActivation.status = :status")
	
	AccountActivation findByActivationObjCreatedObjectIdAndCreatorObjectId(@Param("createdObjectType")String createdObjectType,
			@Param("createdObjectId")Integer createdObjectId,@Param("creatorObjectId")Integer creatorObjectId,@Param("creatorObjectType")String creatorObjectType,@Param("status")STATUS status);
	
	@Query(" from AccountActivation aa where aa.createdObjectType = :createdObjectType and aa.status='NOTPROCESSED' and aa.sentDate >= :startDate and aa.sentDate < :endDate ")
	List<AccountActivation> findByCreatedObjectTypeAndSentDateBetween(@Param("createdObjectType")String createdObjectType,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query("SELECT aa FROM AccountActivation aa WHERE aa.createdObjectId = :createdObjectId " +
			"and aa.createdObjectType = :createdObjectType order by id desc ")
	List<AccountActivation> findByCreatedObjectTypeAndCreatorObjectId(@Param("createdObjectType") String createdObjectType, @Param("createdObjectId") Integer createdObjectId);
	
	@Modifying
	@Transactional
	@Query("UPDATE AccountActivation SET status = :statusTo WHERE status = :statusFrom AND createdObjectId = :createdObjectId and createdObjectType = :createdObjectType")
	void updateStatusByCreatedObjectTypeAndCreatorObjectId(@Param("statusTo") STATUS statusTo, @Param("statusFrom") STATUS statusFrom, @Param("createdObjectType") String createdObjectType, @Param("createdObjectId") Integer createdObjectId);

	

}