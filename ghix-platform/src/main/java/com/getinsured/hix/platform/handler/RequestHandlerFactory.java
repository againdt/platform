package com.getinsured.hix.platform.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RequestHandlerFactory {
	
	@Autowired(required=false)
	private List<RequestHandler> requestHandlers;
	@Autowired(required=false)
	private List<IndexedRequestHandler> indexedRequestHandlers;
	
	private static Logger LOGGER = LoggerFactory.getLogger(RequestHandlerFactory.class);
	private HashMap<String, List<RequestHandler>> pathHandlers = null;
	private HashMap<String, IndexedPathHandlers> indexedPathHandlers = null;
	private HashMap<String, RequestHandler> namedHandlers = null;
	
	@PostConstruct
	public void init() throws InvalidHandlerIndexException{
		if(this.requestHandlers != null){
			if(this.pathHandlers == null){
				this.pathHandlers = new HashMap<String,List<RequestHandler>>();
			}
			for(RequestHandler handler: requestHandlers){
				String handlerName = handler.getName();
				if(handlerName != null){
					if(this.namedHandlers == null){
						this.namedHandlers = new HashMap<String,RequestHandler>();
					}
					if(this.namedHandlers.containsKey(handlerName)){
						throw new InvalidHandlerIndexException("Handler with name "+handlerName+" Already provided, duplicate handler names are not allowed, consider a module prifix for your handler name");
					}
					this.namedHandlers.put(handlerName, handler);
				}
				List<String> paths = handler.getHandlerPath();
				if(paths != null){
					for(String path: paths){
						List<RequestHandler> handlers = this.pathHandlers.get(path);
						if(handlers == null){
							handlers = new ArrayList<RequestHandler>(5);
							this.pathHandlers.put(path, handlers);
						}
						handlers.add(handler);
					}
				}//else this handler will be looked up using its name
			}
		}
		if(this.indexedRequestHandlers != null && this.indexedRequestHandlers.size() > 0){
			if(this.indexedPathHandlers == null){
				this.indexedPathHandlers = new HashMap<String, IndexedPathHandlers>();
			}
			for(IndexedRequestHandler idxHandler:indexedRequestHandlers){
				String path = idxHandler.getHandlerPath();
				IndexedPathHandlers pathIdxHandler = this.indexedPathHandlers.get(path);
				if(pathIdxHandler == null){
					pathIdxHandler = new IndexedPathHandlers();
					this.indexedPathHandlers.put(path, pathIdxHandler);
				}
				pathIdxHandler.addHandler(idxHandler);
			}
		}
	}
	
	/**
	 * Returns the list of handlers available for a given path and an empty list
	 * if none, Never returns a NULL
	 * @param path
	 * @return
	 */
	public List<RequestHandler> getRequestHandler(String path){
		if(this.pathHandlers != null){
			List<RequestHandler> handlers = this.pathHandlers.get(path);
			if(handlers != null && handlers.size() > 0){
				return handlers;
			}
		}
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Handler not found for path:"+path);
		}
		return new ArrayList<RequestHandler>(1);
	}
	
	public IndexedPathHandlers getIndexedRequestHandler(String path){
		if(indexedPathHandlers == null){
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("No registered handler available, returning Empty handler list for path:"+path);
			}
			return null;
		}
		return this.indexedPathHandlers.get(path);
	}
	
	/**
	 * Returns NULL if handler with a given name is not available of implementing class has not provided a name
	 * @param name
	 * @return
	 */
	public RequestHandler getHandlerByName(String name){
		RequestHandler handler = this.namedHandlers.get(name);
		if(handler == null && LOGGER.isInfoEnabled()){
			LOGGER.info("No handler available for name:"+name);
		}
		return handler;
	}
}
