package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.TenantSSOConfiguration;

/**
 * Repository interface for {@link TenantSSOConfiguration}
 * @author Yevgen Golubenko
 * @since 2/23/17
 */
@Repository
public interface TenantSSOConfigurationRepository extends JpaRepository<TenantSSOConfiguration, Integer>
{
  @Query("SELECT tsso FROM TenantSSOConfiguration tsso WHERE tsso.tenantId = (SELECT id FROM Tenant WHERE code = :tenantCode)")
  TenantSSOConfiguration findByTenantCode(@Param("tenantCode") String tenantCode);

  TenantSSOConfiguration findByTenantId(Long tenantId);
}
