package com.getinsured.hix.platform.um.handlers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.atlassian.httpclient.api.HttpStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.handler.HandlerConstants;
import com.getinsured.hix.platform.handler.RequestHandler;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.ws_trust.saml.TenantSpMetadata;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Component
@DependsOn({"configProp","platformConstants"})
public class PreUserCreateHandler implements RequestHandler {
	public static final String NAME = "PRE_USER_CREATE";
	public static final String PASSWORD_SERVICE = "PASSWORD_SERVICE";
	public static final String USER_SERVICE = "USER_SERVICE";
	
	@Autowired
	private GhixEncryptorUtil ghixEncryptorUtil;
	@Autowired
	private HttpClient restHttpClient;
	@Autowired
	private SCIMUserManager scimUserManager;
	@Autowired
	@Lazy
	private TenantSpMetadata tenantSpMetadata;
	
	private Logger logger = LoggerFactory.getLogger(PreUserCreateHandler.class);
	
	@Value("#{configProp['platform.im.wso2environment'] != null ? configProp['platform.im.wso2environment'] : '5.3'}")
	private String wso2Environment;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public List<String> getHandlerPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean handleRequest(HashMap<String, Object> context, ServletRequest request, boolean async) {
		boolean success = false;
		boolean wso2Enabled = (Boolean)context.get(WSO2_ENABLED_KEY);
		if(logger.isInfoEnabled()){
			logger.info("Handling pre user create event WSO2 enabled:\""+wso2Enabled+"\"");
		}
		TenantDTO tenant = (TenantDTO) context.get(HandlerConstants.TENANT_DTO_KEY);
		if(tenant != null && !wso2Enabled){
			// Check if SSO configuration is available
			 String metadata = tenantSpMetadata.getTenantSPMetadata(tenant.getId());
			 if(metadata != null){
				 context.put(HandlerConstants.HANDLER_ERROR, "Found SSO Enabled for tenant "+tenant.getCode()+" but Remote User management is disabled, this configuration is not supported currently");
				 return false;
			 }
		}
		AccountUser user = (AccountUser)context.get(ACCOUNT_USER_KEY);
		// We'll create the user only if user is not authenticated
		// Authenticated means user has already been provisioned in WSO2
		if(!user.isAuthenticated()){
			Role provisioningRole =  (Role)context.get(PROVISIONIG_ROLE_KEY);
			if(wso2Enabled){
				if(logger.isInfoEnabled()){
					logger.info("Using WSO2 Environment "+this.wso2Environment);
				}
				success = handleWso2Environment(user, provisioningRole,context);
			}
			else{
				if(logger.isInfoEnabled()){
					logger.info("Generating UUID for user, Make sure we are seeing this log in a DB based envieonent");
				}
				String userUuid = UUID.nameUUIDFromBytes(user.getUsername().getBytes()).toString();
				String newPassword = user.getPassword() + userUuid;
				String encPwd = ghixEncryptorUtil.getEncryptedPassword(newPassword,
						userUuid);
				user.setUuid(userUuid);
				user.setPassword(encPwd);
				success = true;
			}
		}else{
			String id = user.getExtnAppUserId();
			if(logger.isInfoEnabled()){
				logger.info("User has already provisioned on WSO2 with id :"+id);
			}
			success = true;
			context.put("id", id);
		}
		return success;
	}
	
	

	private boolean handleWso2Environment(AccountUser user, Role provisioningRole, HashMap<String,Object> context) {
		boolean success = false;
		JSONParser parser = new JSONParser();
		switch(wso2Environment){
			case WSO2_ENV_5_3:
			{
				String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"scim/tenant/user/add";
				if(logger.isInfoEnabled()){
					logger.info("Using WSO2 Environment 5.3 for URI:"+uri);
				}
				HttpPost post = new HttpPost(uri);
				HttpEntity payload = EntityBuilder.create()
						.setContentType(ContentType.APPLICATION_JSON)
						.setText(createAddTenantUserRequest(user, (provisioningRole.getPwdConstraintsEnabled()=='Y'), context)).build();
				post.setEntity(payload);
				try {
					CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
					int status = response.getStatusLine().getStatusCode();
					if(HttpStatus.CREATED.code != status){
						logger.error("Error response received with status:"+status);
						context.put(HandlerConstants.HANDLER_ERROR, getResponseString(response.getEntity()));
					}else{
						success= true;
						String createUserResponse = getResponseString(response.getEntity());
						JSONObject createUserResponseObj = (JSONObject) parser.parse(createUserResponse);
						String id = (String) createUserResponseObj.get("id");
						if(logger.isInfoEnabled()){
							logger.info("Created user with id:"+id);
						}
						user.setExtnAppUserId(id);
						user.setPassword("PASSWORD_NOT_IN_USE".intern());
					}
					response.close();
				} catch (IOException | ParseException e) {
					logger.error("Error creating user",e);
					success = false;
					context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
					context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
				}
				break;
			}
			case WSO2_ENV_5_0:
			{
				if(logger.isInfoEnabled()){
					logger.info("Using WSO2 Environment 5.0 makse sure we have IEX builds running for Idaho");
				}
				try {
					scimUserManager.createUser(user, (provisioningRole.getPwdConstraintsEnabled()=='Y'?1:0));
					success= true;
					user.setPassword("PASSWORD_NOT_IN_USE".intern());
				} catch (GIException e) {
					logger.error("Error creating user",e);
					success = false;
					context.put(HandlerConstants.HANDLER_ERROR, e.getClass().getName());
					context.put(HANDLER_EXCEPTION,PlatformServiceUtil.exceptionToJson(e));
				}
				break;
			}
		}
		if(logger.isInfoEnabled()){
			logger.info("Pre user create operation completed with success :"+success);
		}
		return success;
	
	}
	
	private String getResponseString(HttpEntity entity){
		try {
			StringBuilder builder = new StringBuilder();
			BufferedReader sb = new BufferedReader(new InputStreamReader(entity.getContent()));
			String str = null;
			while((str = sb.readLine()) != null){
				builder.append(str);
			}
			sb.close();
			return builder.toString();
		} catch (UnsupportedOperationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void put(String attrName, Object val, boolean mandatory, JSONObject payload){
		if(val == null && mandatory){
			throw new RuntimeException("Mandatory attribute "+attrName+" Not provided");
		}
		if(val != null){
			payload.put(attrName, val);
		}
	}
	
	@SuppressWarnings("unchecked")
	private String createAddTenantUserRequest(AccountUser accountUser, boolean isPrivileged, HashMap<String,Object> context) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		TenantDTO tenantDTO = (TenantDTO)context.get(TENANT_DTO_KEY);
		if(logger.isInfoEnabled()){
			logger.info("Constructing payload for user creation for uset type:"+isPrivileged+" Tenant code:"+tenantDTO.getCode());
		}
		reqJsonObject = new JSONObject();
		put("tenantAdmin".intern(), tenantDTO.getTenantProvisionigUser(),true,reqJsonObject);
		put("tenantPassword".intern(), tenantDTO.getTenantProvisionigUserPassword(),true,reqJsonObject);
		put("tenantDomain".intern(), tenantDTO.getTenantDomain(),true,reqJsonObject);

		payloadJsonObject = new JSONObject();
		put("userName".intern(), accountUser.getUserName(),true,payloadJsonObject);
		put("userPassword".intern(), StringEscapeUtils.unescapeHtml(accountUser.getPassword()).trim(),true,payloadJsonObject);
		put("firstName".intern(), accountUser.getFirstName(),true,payloadJsonObject);
		put("lastName".intern(), accountUser.getLastName(),true,payloadJsonObject);
		put("userType".intern(), (isPrivileged?1:0),true,payloadJsonObject);
		put("phoneNumber".intern(), accountUser.getPhone(),true,payloadJsonObject);
		put("mail".intern(), accountUser.getEmail(),true,payloadJsonObject);
		put("prefMethComm".intern(), "Email",false,payloadJsonObject);
		put("preferredLanguage".intern(), "English",false,payloadJsonObject);
		put("tenantCode".intern(), tenantDTO.getCode(),true,payloadJsonObject);
		put("secQuestion1".intern(), accountUser.getSecurityQuestion1(),false,payloadJsonObject);
		put("secAnswer1".intern(), accountUser.getSecurityAnswer1(),false,payloadJsonObject);
		put("secQuestion2".intern(), accountUser.getSecurityQuestion2(),false,payloadJsonObject);
		put("secAnswer2".intern(), accountUser.getSecurityAnswer2(),false,payloadJsonObject);

		reqJsonObject.put("payload".intern(), payloadJsonObject);
		if (null != reqJsonObject) {
			request = reqJsonObject.toJSONString();
			if(logger.isTraceEnabled()){
				logger.trace("Create user Payload:"+request);
			}
		}
		return request;
	}

}
