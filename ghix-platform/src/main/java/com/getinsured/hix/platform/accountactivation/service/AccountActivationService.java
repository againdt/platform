package com.getinsured.hix.platform.accountactivation.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;


public interface AccountActivationService {

	String ACCOUNT_PHONEVERIFICATION_SENDCODE = "/account/phoneverification/sendcode/";
	String ACCOUNT_SECURITYQUESTIONS = "/account/securityquestions";
	String ACCOUNT_SIGNUP = "/account/signup";

	/**
	 * create a new record in AccountActivation table and initiates an email to the created entity to complete activation flow.
	 * 
	 * @param createdObject
	 * @param creatorObject
	 * @param expirationDays
	 * @return AccountActivation object for success else exception is thrown
	 * @throws GIException, if email sending fails
	 */
	AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject,Integer expirationDays ) throws GIException;
	
	/**
	 * create a new record in AccountActivation table and initiates an email to the created entity to complete activation flow.
	 * 
	 * @param createdObject
	 * @param creatorObject
	 * @return AccountActivation object for success else exception is thrown
	 * @throws GIException, if email sending fails
	 */
	
	
	AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject) throws GIException;

	/**
	 * fetches AccountActivation object for supplied activationToken.
	 * 
	 * @param activationToken
	 * @return AccountActivation
	 */
	AccountActivation findByActivationToken(String activationToken);

	/**
	 * forms ActivationJson object for the AccountActivation.jsonString value .
	 * 
	 * @param jsonString
	 * @returnActivationJson
	 */
	ActivationJson getActivationJson(String jsonString);

	/**
	 * evaluates next landing page for a supplied token.
	 * This is useful to decide landing page when user re-initiates activation process after completing some steps.
	 * 
	 * @param token
	 * @return url, string
	 */
	String getLandingPage(String token);

	/**
	 * evaluates next landing page for a supplied activation id.
	 * This is useful to decide landing page when user re-initiates activation process after completing some steps.
	 * 
	 * @param token
	 * @return url, string
	 */
	String getLandingPage(Integer id);

	/**
	 * updates lastVisitedUrl and username for suppiled activation id.
	 * This is useful to decide landing page when user re-initiates activation process after completing some steps.
	 * Also to auto login using the username.
	 * 
	 * @param lastVisitedUrl
	 * @param username
	 * @param id
	 * @return AccountActivation
	 */
	AccountActivation updateLastVisitedUrlAndUsername(String lastVisitedUrl, String username, Integer id);

	/**
	 * fetches AccountActivation object for a supplied activation id.
	 * 
	 * @param activationId
	 * @return AccountActivation object
	 */
	AccountActivation findByActivationId(Integer activationId);

	/**
	 * sends activation code to a supplied toPhone number.
	 * Stores valid activation code in db to be validated in verification flow.
	 * 
	 * @param toPhone
	 * @param id
	 * @return SUCCESS or failure reason received from TWILIO API.
	 */
	String sendCode(String toPhone, Integer id,String codeType);

	/**
	 * Verifies entered activation code with the stored one.
	 * 
	 * @param codeToBeVerified
	 * @param id
	 * @return SUCCESS or failure reason
	 */
	String verifyCode(String codeToBeVerified, Integer id);

	/**
	 * updates user information in the created object master table.
	 * This delegates the call to the created object repository configured as enum in activationJSON class.
	 * 
	 * @param accountUser
	 * @param createdId
	 * @param jsonObject
	 */
	void updateUserInCreatedObject(AccountUser accountUser, Integer createdId,
			ActivationJson jsonObject);

	List<AccountActivation> getAccountActivationAdminUsers();

	/** Find the AccountActivation object based on the username (login)
	 * @param userName
	 * @return AccountActivation
	 */
	AccountActivation getAccountActivationByUserName(String userName);
	
	AccountActivation getAccountActivationByCreatedObjectId(int createdObjectId,String createdObjectType);
	
	AccountActivation getAccountActivationByCreatedObjectId(int createdObjectId,String createdObjectType,int creatorObjectId, String creatorObjectType);
	
	AccountActivation getAccountActivationByCreatedObjectIdAndCreatorObjectId(int createdObjectId, String createdObjectType,
			int creatorObjectId, String creatorObjectType,STATUS status);
	
	List<AccountActivation> getAccountActivationsByCreatedObjectIdAndType(int createdObjectId, String createdObjectType);
	
	/** Resending email to the entity 
	 * @param createdObject,creatorObject,accountActivation
	 * @return AccountActivation
	 */
	AccountActivation resendActivationMail(CreatedObject createdObject, CreatorObject creatorObject, Integer expirationDays,
			AccountActivation accountActivation) throws GIException;
	
	/** 
	 * Generate and send activation/phone code 
	 * @param toPhone,id,codeType,request
	 * @return String
	 */
	String sendCode(String toPhone, Integer id,String codeType,HttpServletRequest request);

	int sendActivationReminders(AccountActivation accountActivation);
	
	AccountActivation update(AccountActivation accountActivation);

	void linkUserTo(AccountUser newAccountUser, CreatedObject createdObject) throws GIException;
	
	void expireAccountActivation(STATUS statusTo, STATUS statusFrom, String createdObjectType, Integer createdObjectId);

	AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject,Integer expirationDays, Map<GhixUtils.EMAIL_STATS, String> notificationTrackingAttributes)
			throws GIException;

	AccountActivation getUnProcessedAccountActivationByUserName(String userName);
}