package com.getinsured.hix.platform.affiliate.service;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.model.ProductConfigurationDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.repository.AffiliateRepository;

@Component
public class AffiliateConfigServiceImpl implements AffiliateConfigService {

	@Autowired
	private AffiliateRepository affiliateRepository;
	
	@Override
	public ConfigurationDTO getAffiliateConfig(Long affiliateId) {
		ConfigurationDTO config = null;
		Affiliate affiliate = affiliateRepository.findOne(affiliateId);
		if(affiliate != null) {
			config = affiliate.getAffiliateConfig();
		}
		return config;
	}

	@Override
	public Affiliate getAffiliate(Long affiliateId) {
		return affiliateRepository.findOne(affiliateId);
	}
	
	public boolean isProductForCrossSell(ConfigurationDTO configuration, String productCode){
		if((configuration == null || configuration.getProductConfigurations() == null) && TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getConfiguration() != null) {
			configuration = TenantContextHolder.getTenant().getConfiguration();
		}
		if(configuration != null) {
			String productUrl = configuration.getProductUrl(productCode);
			if(StringUtils.isEmpty(productUrl)){
				List<ProductConfigurationDTO> productConfigurations = configuration.getProductConfigurations();
				if(productConfigurations != null && !productConfigurations.isEmpty()) {
					for (ProductConfigurationDTO product : productConfigurations) {
						if(productCode.equalsIgnoreCase(product.getCode()) && BooleanUtils.isTrue(product.getSelected())) {
							return true;
						}
					}
				}
			}				
		}
		return false;
	}
}
