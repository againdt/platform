package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.RolePermission;

public interface IRolePermissionRepository extends JpaRepository<RolePermission, Integer> {
	@Query("Select rp from RolePermission rp where rp.role.name=:roleName")
	public List<RolePermission> getPermissionsByRole(@Param("roleName")String roleName);
	
	@Query("Select rp from RolePermission rp where rp.permission.name=:permissionName")
	public List<RolePermission> getRolesByPermission(@Param("permissionName")String permissionName);

}
