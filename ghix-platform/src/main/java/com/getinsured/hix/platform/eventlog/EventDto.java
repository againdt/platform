package com.getinsured.hix.platform.eventlog;

import java.sql.Timestamp;
import java.util.Map;

/**
 * This DTO will be used to display the event history of the user.
 * @author hardas_d
 *
 */
public class EventDto {
	private EventUserInfo loggedInUserInfo;
	private ControllerInfo controllerInfo;
	private BrowserInfo browserInfo;
	private EventInfoDto eventInfoDto;

	private String moduleName; 
	private String moduleId; 
	private Timestamp eventCreationTimeStamp;

	private Map<String, Object> mapParameters;

	public EventUserInfo getLoggedInUserInfo() {
		return loggedInUserInfo;
	}

	public void setLoggedInUserInfo(EventUserInfo loggedInUserInfo) {
		this.loggedInUserInfo = loggedInUserInfo;
	}

	public ControllerInfo getControllerInfo() {
		return controllerInfo;
	}

	public void setControllerInfo(ControllerInfo controllerInfo) {
		this.controllerInfo = controllerInfo;
	}

	public BrowserInfo getBrowserInfo() {
		return browserInfo;
	}

	public void setBrowserInfo(BrowserInfo browserInfo) {
		this.browserInfo = browserInfo;
	}

	public EventInfoDto getEventInfoDto() {
		return eventInfoDto;
	}

	public void setEventInfoDto(EventInfoDto eventInfoDto) {
		this.eventInfoDto = eventInfoDto;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public Timestamp getEventCreationTimeStamp() {
		return eventCreationTimeStamp;
	}
	
	public void setEventCreationTimeStamp(Timestamp eventCreationTimeStamp) {
		this.eventCreationTimeStamp = eventCreationTimeStamp;
	}

	public Map<String, Object> getMapParameters() {
		return mapParameters;
	}

	public void setMapParameters(Map<String, Object> mapParameters) {
		this.mapParameters = mapParameters;
	}
	
	private EventDto() {
		this.loggedInUserInfo = null;
		this.controllerInfo = null;
		this.browserInfo = null;
		this.eventInfoDto = null;
		this.moduleName = null;
		this.moduleId = null;
		this.eventCreationTimeStamp = null;
		this.mapParameters = null;
	}
	
	public static EventDto create() {
		return new EventDto();
	}
}
