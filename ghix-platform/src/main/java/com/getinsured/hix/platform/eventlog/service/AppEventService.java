package com.getinsured.hix.platform.eventlog.service;

import java.util.Map;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.eventlog.EventInfoDto;

public interface AppEventService {

	void record(EventInfoDto eventInfoDto, Map<String, String> mapParameters);
	
	/**
	 * Jira Id: HIX-101842
	 * To Record Application event with passed Account User.
	 * @param eventInfoDto Object<EventInfoDto>
	 * @param mapParameters Object<Map<String, String>>
	 * @param accountUser Object<AccountUser>
	 */
	void record(EventInfoDto eventInfoDto, Map<String, String> mapParameters, AccountUser accountUser);
}
