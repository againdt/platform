package com.getinsured.hix.platform.content;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

/***
 *
 * TODO:
 * 		1. Need to implement logic to hold loaded properties per tenant per locale. - Done.
 * 		2. Set the number of seconds to cache loaded properties files - cacheMillis, do we need this?
 *
 */
public class GiReloadableResourceBundleMessageSource extends ReloadableResourceBundleMessageSource {

	private static final String CLEARING_ENTIRE_RESOURCE_BUNDLE_CACHE_GI_RELOADABLE_RESOURCE_BUNDLE_MESSAGE_SOURCE = "Clearing entire resource bundle cache - GiReloadableResourceBundleMessageSource";
	private static final String HITTING_CONTENT_SERVICE_TO_GET_DATA_URL = "Hitting content service to get data, URL - ";
	private static final String FILENAME_COMMON_PREFIX = "classpath:/i18n/json/";
	private static final String FILE_SPLITTER = "_";
	private static final String APPLICATION_TXT = "&application=";
	private static final String REST_TEMPLATE = "restTemplate";
	private static final String CONTENT_SERVICE_CONFIGURED = "Content Service configured and points to - ";
	private static final String CONTENT_SERVICE_RESPONSE_OK_NOT = "Content Service response <> OK ; fallback to project ...";
	private static final String CONTENT_SERVICE_RESPONSE_OK = "Content Service response = OK ";
	private static final String LANG_TXT = "?lang=";
	private static final String LOCALE_TXT = "/locale/";
	private static final String COULD_NOT_PARSE_JSON_FILE_END = "]";
	private static final String COULD_NOT_PARSE_JSON_FILE_START = "Could not parse json file [";
	private static final String JSON_FILE_FOUND = "JSON file found!";
	private static final String JSON_FILE_NOT_FOUND = "JSON file NOT found!, delegating call to ReloadableResourceBundleMessageSource ...";
	private static final String JSON_SUFFIX = ".json";
	private static final String CONTENT_SERVICE_NOT_CONFIGURED = "Content Service NOT configured";

	private PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

	private long cacheMillis = -1;

	private String giContentServiceUrl;

	public void setGiContentServiceUrl(String giContentServiceUrl) {
		this.giContentServiceUrl = giContentServiceUrl;
	}

	/** Cache to hold merged loaded properties per tenant per locale */
	private static final ConcurrentHashMap<String, ConcurrentHashMap<Locale, PropertiesHolder>> tenantCachedMergedProperties =
			new ConcurrentHashMap<String, ConcurrentHashMap<Locale, PropertiesHolder>>();

	private String[] basenames = new String[0];

	@Override
	public void setBasenames(String... basenames) {
		if (basenames != null) {
			this.basenames = new String[basenames.length];
			for (int i = 0; i < basenames.length; i++) {
				String basename = basenames[i];
				Assert.hasText(basename, "Basename must not be empty");
				this.basenames[i] = basename.trim();
			}
		}
		else {
			this.basenames = new String[0];
		}
	}

	@Override
	protected String getMessageInternal(String code, Object[] args, Locale locale) {
		if (code == null) {
			return null;
		}
		if (locale == null) {
			locale = Locale.getDefault();
		}
		Object[] argsToUse = args;

		if (!isAlwaysUseMessageFormat() && ObjectUtils.isEmpty(args)) {
			// Optimized resolution: no arguments to apply,
			// therefore no MessageFormat needs to be involved.
			// Note that the default implementation still uses MessageFormat;
			// this can be overridden in specific subclasses.
			String message = resolveCodeWithoutArguments(code, locale);
			if (message != null) {
				return message;
			}
		}

		else {
			// Resolve arguments eagerly, for the case where the message
			// is defined in a parent MessageSource but resolvable arguments
			// are defined in the child MessageSource.
			argsToUse = resolveArguments(args, locale);

			MessageFormat messageFormat = resolveCode(code, locale);
			if (messageFormat != null) {
				synchronized (messageFormat) {
					return messageFormat.format(argsToUse);
				}
			}
		}

		// Check locale-independent common messages for the given message code.
		Properties commonMessages = getCommonMessages();
		if (commonMessages != null) {
			String commonMessage = commonMessages.getProperty(code);
			if (commonMessage != null) {
				return formatMessage(commonMessage, args, locale);
			}
		}

		// Not found -> check parent, if any.
		return getMessageFromParent(code, argsToUse, locale);
	}

	@Override
	protected String resolveCodeWithoutArguments(String code, Locale locale) {
		if (this.cacheMillis < 0) {
			PropertiesHolder propHolder = getMergedProperties(locale);
			String result = propHolder.getProperty(code);
			if (result != null) {
				return result;
			}
		}
		else {
			for (String basename : this.basenames) {
				List<String> filenames = calculateAllFilenames(basename, locale);
				for (String filename : filenames) {
					PropertiesHolder propHolder = getProperties(filename);
					String result = propHolder.getProperty(code);
					if (result != null) {
						return result;
					}
				}
			}
		}
		return null;
	}

	@Override
	protected MessageFormat resolveCode(String code, Locale locale) {
		if (this.cacheMillis < 0) {
			PropertiesHolder propHolder = getMergedProperties(locale);
			MessageFormat result = propHolder.getMessageFormat(code, locale);
			if (result != null) {
				return result;
			}
		}
		else {
			for (String basename : this.basenames) {
				List<String> filenames = calculateAllFilenames(basename, locale);
				for (String filename : filenames) {
					PropertiesHolder propHolder = getProperties(filename);
					MessageFormat result = propHolder.getMessageFormat(code, locale);
					if (result != null) {
						return result;
					}
				}
			}
		}
		return null;
	}

	@Override
	protected PropertiesHolder getMergedProperties(Locale locale) {
		String tenantCode = getTenantCode();
		ConcurrentHashMap<Locale, PropertiesHolder> cachedMergedProperties = tenantCachedMergedProperties.get(tenantCode);
		PropertiesHolder mergedHolder = null;
		if (cachedMergedProperties != null){
			mergedHolder = cachedMergedProperties.get(locale);
			if (mergedHolder != null) {
				return mergedHolder;
			}
		} else {
			cachedMergedProperties = new ConcurrentHashMap<>();
		}

		Properties mergedProps = new Properties();
		mergedHolder = new PropertiesHolder(mergedProps, -1);
		for (int i = this.basenames.length - 1; i >= 0; i--) {
			List<String> filenames = calculateAllFilenames(this.basenames[i], locale);
			for (int j = filenames.size() - 1; j >= 0; j--) {
				String filename = filenames.get(j);
				PropertiesHolder propHolder = getProperties(filename);
				if (propHolder.getProperties() != null) {
					mergedProps.putAll(propHolder.getProperties());
				}
			}
		}
		cachedMergedProperties.putIfAbsent(locale, mergedHolder);

		tenantCachedMergedProperties.putIfAbsent(tenantCode, cachedMergedProperties);
		return mergedHolder;
	}

	@Override
	protected PropertiesHolder refreshProperties(String filename, PropertiesHolder propHolder) {
		Resource resource = resolver.getResource(filename + JSON_SUFFIX);

		if (!resource.exists()) {
			if (logger.isWarnEnabled()) {
				logger.warn(JSON_FILE_NOT_FOUND);
			}
			return super.refreshProperties(filename, propHolder);
		}

		if (logger.isInfoEnabled()) {
			logger.info(JSON_FILE_FOUND);
		}

		Locale locale = LocaleContextHolder.getLocale();

		/**
		 * if giContentServiceUrl is not empty
		 * 	-	ignore loaded resource handle
		 * 	-	try to load resource from content service
		 *
		 */
		if (!StringUtils.isEmpty(giContentServiceUrl)){
			if (logger.isInfoEnabled()) {
				logger.info(CONTENT_SERVICE_CONFIGURED + giContentServiceUrl);
			}

			RestTemplate restTemplate = (RestTemplate) GHIXApplicationContext.getBean(REST_TEMPLATE);
			try {
				String tenantCode = getTenantCode();
				String[] filenames = filename.split(FILE_SPLITTER);
				String lang =  locale.getLanguage();
				String applicationName = StringUtils.substringAfter(filenames[0], FILENAME_COMMON_PREFIX);

				String url = giContentServiceUrl + LOCALE_TXT + tenantCode + LANG_TXT + lang + APPLICATION_TXT + applicationName;

				if (logger.isInfoEnabled()) {
					logger.info(HITTING_CONTENT_SERVICE_TO_GET_DATA_URL + url );
				}

				ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
				if (HttpStatus.OK == response.getStatusCode()){
					logger.info(CONTENT_SERVICE_RESPONSE_OK);
					String resourceString = response.getBody();
					propHolder = preparePropHolder(resourceString, null);
					return propHolder;
				}
			} catch (RestClientException e) {
				logger.error(CONTENT_SERVICE_RESPONSE_OK_NOT);
			}

		} else {
			if (logger.isInfoEnabled()) {
				logger.info(CONTENT_SERVICE_NOT_CONFIGURED );
			}
		}

		/** If content service configured and rest call fails for any reason then default to project packaging (default behavior) */
		propHolder = preparePropHolder(filename, resource);
		return propHolder;
	}

	private String getTenantCode() {
		TenantDTO tenantDto = TenantContextHolder.getTenant();
		String tenantCode = "";

		if (tenantDto != null && !StringUtils.isEmpty(tenantDto.getName())){
			tenantCode = tenantDto.getCode();
		}
		return tenantCode;
	}

	private PropertiesHolder preparePropHolder(String filename, Resource resource) {
		PropertiesHolder propHolder;
		long refreshTimestamp = -1;

		long fileTimestamp = -1;
		try {
			Properties props = loadProperties(resource, filename);
			propHolder = new PropertiesHolder(props, fileTimestamp);
		}
		catch (Exception ex) {
			if (logger.isWarnEnabled()) {
				logger.warn(COULD_NOT_PARSE_JSON_FILE_START + resource.getFilename() + COULD_NOT_PARSE_JSON_FILE_END, ex);
			}
			// Empty holder representing "not valid".
			propHolder = new PropertiesHolder();
		}

		propHolder.setRefreshTimestamp(refreshTimestamp);
		return propHolder;
	}

	@Override
	protected Properties loadProperties(Resource resource, String filename) throws IOException {
		if (resource != null){
			return JsonUtils.readJson(resource);
		} else {
			return JsonUtils.readJson(filename);
		}
	}


	/**
	 * Clear the resource bundle cache.
	 * Subsequent resolve calls will lead to reloading of the properties files.
	 */
	@Override
	public void clearCache() {
		if (logger.isInfoEnabled()) {
			logger.debug(CLEARING_ENTIRE_RESOURCE_BUNDLE_CACHE_GI_RELOADABLE_RESOURCE_BUNDLE_MESSAGE_SOURCE);
		}
		super.clearCache();
		tenantCachedMergedProperties.clear();
	}

	/**
	 * Clear the resource bundle caches of this MessageSource and all its ancestors.
	 * @see #clearCache
	 */
	@Override
	public void clearCacheIncludingAncestors() {
		clearCache();
		if (getParentMessageSource() instanceof ReloadableResourceBundleMessageSource) {
			((ReloadableResourceBundleMessageSource) getParentMessageSource()).clearCacheIncludingAncestors();
		}
	}

}
