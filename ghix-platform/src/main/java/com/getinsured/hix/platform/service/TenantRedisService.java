package com.getinsured.hix.platform.service;

import com.getinsured.hix.model.TenantDTO;

public interface TenantRedisService {
	
	TenantDTO get(String url);
	void set(String url, TenantDTO tenant);

}
