package com.getinsured.hix.platform.db;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.getinsured.hix.model.AccountUser;

/**
 * Intercepts a request to application and modifies created Database connection
 * to add custom metrics data to it for the purpose of security and tracking.
 * <p>
 * There is a way to proxy existing data source, but I found that it's somewhat not stable,
 * and undentified exceptions occurs in some cases within Oracle driver and they are not at all
 * meaningful, even though that would be perhaps optimal solution, for starters I think
 * this will be sufficient.
 * </p>
 */
public class RequestMapperInterceptor extends HandlerInterceptorAdapter 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestMapperInterceptor.class);	
	
	/**
	 * Default value to use for Oracle Connection metrics when user is not logged in 
	 * or information is not available about the user.
	 * Currently {@value #DEFAULT_USER_ID_STRING}
	 */
	public static final String DEFAULT_USER_ID_STRING = "ANONYMOUS";
	
	/**
	 * Default value for MODULE metric to be used in Oracle connection settings. 
	 * Current value is: {@value #MODULE_UNSPECIFIED}
	 */
	public static final String MODULE_UNSPECIFIED = "MODULE_UNSPECIFIED";
	
	/**
	 * Default value for ACTION metric to be used in Oracle connection settings.
	 * Current value is: {@value #ACTION_UNSPECIFIED}
	 */
	public static final String ACTION_UNSPECIFIED = "ACTION_UNSPECIFIED";
	
	private DataSource dataSource = null;
	
	private static final ThreadLocal<Connection> connectionThread =
			new ThreadLocal<Connection>() {
	             @Override 
	             protected Connection initialValue() 
	             {
	                 return null;
	             }
	         };

	/**
	 * Constructor that takes configured java.sql.DataSoure (currently jndi dataSource in applicationContext*.xml)
	 * 
	 * @param ds underlying application DataSource.
	 */
	public RequestMapperInterceptor(DataSource ds)
	{
		if(ds != null)
		{
			LOGGER.debug("RequestMapperInterceptor::construct(), data-source: " + ds);
			this.dataSource = ds;
		}
	}
	
	/**
	 * Intercept the execution of a handler. Called after HandlerMapping determined an appropriate 
	 * handler object, but before HandlerAdapter invokes the handler.
	 * DispatcherServlet processes a handler in an execution chain, consisting of any number of interceptors, 
	 * with the handler itself at the end. With this method, each interceptor can decide to abort the execution 
	 * chain, typically sending a HTTP error or writing a custom response.
	 * 
	 * @param request current HTTP request
	 * @param response current HTTP response
	 * @handler chosen handler to execute for type and/or instance evaluation. 
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception
	{
		if(this.dataSource != null)
		{
			Connection connection = null;
			
			if(connectionThread.get() == null)
			{
				LOGGER.debug("RequestMapperInterceptor::preHandle() Setting new connection in ThreadLocal");
				connectionThread.set(this.dataSource.getConnection());
			}
			
			connection = connectionThread.get();
			
			setMetrics(connection, handler, false);
		}
		//return super.preHandle(request, response, handler);
		return true;
	}
	
	/**
	 * Intercept the execution of a handler. 
	 * Called after HandlerAdapter actually invoked the handler, but before the DispatcherServlet renders the view. 
	 * Can expose additional model objects to the view via the given ModelAndView.
	 * DispatcherServlet processes a handler in an execution chain, consisting of any number of interceptors, 
	 * with the handler itself at the end. With this method, each interceptor can post-process an execution, 
	 * getting applied in inverse order of the execution chain.
	 * 
	 * @param request Current HTTP request.
	 * @param response Current HTTP response. 
	 * @param handler chosen handler to execute.
	 * @param modelAndView the <code>ModelAndView</code> that handler returned, can be null.
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
	{
		LOGGER.debug("RequestMapperInterceptor:postHandle()");
		if(this.dataSource != null)
		{
			Connection connection = null;
			
			try
			{
				connection = connectionThread.get();
				setMetrics(connection, request, true);
			}
			finally
			{
				try
				{
					if(connection != null && !connection.isClosed())
					{
						connection.close();
						connectionThread.remove();
					}
				}
				catch(SQLException e)
				{
					connectionThread.remove();
					LOGGER.error("RequestMapperInterceptor::postHandle() cannot close connection: " + e.getMessage(), e);
				}
			}
		}
	}
	
	/**
	 * Callback after completion of request processing, that is, after rendering the view. 
	 * Will be called on any outcome of handler execution, thus allows for proper resource cleanup.
	 * Note: Will only be called if this interceptor's preHandle method has successfully completed 
	 * and returned true!
	 * 
	 * @param request current HTTP request.
	 * @param response current HTTP response.
	 * @param handler chosen handler to excute.
	 * @param ex exception thrown on handler execution if any.
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
	{
		LOGGER.debug("RequestMapperInterceptor:afterCompletion()");
		Connection connection = null;
		
		try
		{
			connection = connectionThread.get();
			setMetrics(connection, request, true);
		}
		finally
		{
			try
			{
				if(connection != null && !connection.isClosed())
				{
					connection.close();
					connectionThread.remove();
				}
			}
			catch(SQLException e)
			{
				connectionThread.remove();
				//e.printStackTrace();
				LOGGER.error("RequestMapperInterceptor::postHandle() cannot close connection: " + e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Sets additional connection metrics on established Oracle SQL connection.
	 * 
	 * @param connection Oracle implementation of <code>java.sql.Connection</code>
	 * @param handler handler chosen handler to excute which contains interesting info about execution path.
	 * @param reset boolean value, if true, that tells to reset all previously set metrics for 
	 * current Oracle Connection (1 per thread).
	 */
	private void setMetrics(Connection connection, Object handler, final boolean reset)
	{
		try
		{
			if (connection.isWrapperFor(OracleConnection.class))
			{
				final short zero = 0;
				
				OracleConnection oraConnection = connection.unwrap(OracleConnection.class);
				
				// If we are resetting
				if(reset)
				{
					oraConnection.setEndToEndMetrics(getResetMetrics(), Short.MIN_VALUE);
					return;
				}
				
				String module = null;
				String action = null;
				
				if(handler != null && handler instanceof HandlerMethod)
				{
					HandlerMethod hm = (HandlerMethod) handler;
					Method method = hm.getMethod();
					
					if(method != null)
					{
						// Here we can get anotation of the method, and for example if we create 
						// cust anotation like @DoNotSetClientIdInOracle then we can skip this whole ordeal.
						RequestMapping rmapping = method.getAnnotation(RequestMapping.class);
						if(rmapping != null)
						{
							// value will contain our mapping paths, we'll take 1st path if multiple paths are specified.
							String[] paths = rmapping.value();
							
							if(paths.length > 0)
							{
								module = paths[0];
							}
						}
						
						action = method.getName();
					}
				}
				
				final String[] metrics = getMetricsForConnection(module, action);
				
				final String[] existingMetrics = oraConnection.getEndToEndMetrics();
				
				if(existingMetrics != null && existingMetrics.length == OracleConnection.END_TO_END_CLIENTID_INDEX)
				{
					try
					{
						LOGGER.debug("RequestMapperInterceptor::setMetrics() " + existingMetrics[OracleConnection.END_TO_END_CLIENTID_INDEX]);
						if(
							existingMetrics[OracleConnection.END_TO_END_ACTION_INDEX].equalsIgnoreCase(metrics[OracleConnection.END_TO_END_ACTION_INDEX]) &&
							existingMetrics[OracleConnection.END_TO_END_CLIENTID_INDEX].equalsIgnoreCase(metrics[OracleConnection.END_TO_END_CLIENTID_INDEX]) &&
							existingMetrics[OracleConnection.END_TO_END_ECID_INDEX].equalsIgnoreCase(metrics[OracleConnection.END_TO_END_ECID_INDEX]) &&
							existingMetrics[OracleConnection.END_TO_END_MODULE_INDEX].equalsIgnoreCase(metrics[OracleConnection.END_TO_END_MODULE_INDEX])
						)
						{
							LOGGER.debug("RequestMapperInterceptor::setMetrics() Oracle Connection metrics are the same as previous, not setting it to new values.");
						}
						else
						{
							LOGGER.debug("RequestMapperInterceptor::setMetrics() setting new client identifier: " + metrics[OracleConnection.END_TO_END_CLIENTID_INDEX]);
							oraConnection.setEndToEndMetrics(metrics, zero);
						}
					}
					catch(NullPointerException e)
					{
						LOGGER.error("RequestMapperInterceptor::setMetrics() NPE: " + e.getMessage(), e);
					}
				}
				else
				{
					LOGGER.info("RequestMapperInterceptor::setMetrics() No existing Oracle JDBC Connection metrics are set, setting up new metrics.");
					oraConnection.setEndToEndMetrics(metrics, zero);
				}
			}
			else
			{
				LOGGER.warn("RequestMapperInterceptor::setMetrics() We are not dealing with Oracle Connection, thus we cant set metrics");
			}
		}
		catch(SQLException ex)
		{
			LOGGER.warn("RequestMapperInterceptor::setMetrics() SQL Exception while dealing with oracle connection: " + ex.getMessage(), ex);
			
			try
			{
				if (connection.isWrapperFor(OracleConnection.class))
				{
					OracleConnection oraConnection = connection.unwrap(OracleConnection.class);
					oraConnection.setEndToEndMetrics(getResetMetrics(), Short.MIN_VALUE);
				}
			}
			catch(Exception e)
			{
				LOGGER.warn("RequestMapperInterceptor::setMetrics() Exception while dealing with SQLException and trying to reset metrics: " + e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Gets array of metrics to be set on established Oracle SQL Connection.
	 * 
	 * From <a href="http://docs.oracle.com/cd/B19306_01/java.102/b14355/endtoend.htm" target="_new">Oracle documentation</a>:
	 * <p>
	 *  <h3>Maximum Lengths for End-to-End Metrics</h3>
	 *  <table width="400px;">
	 *  	<thead>
	 *  		<td>Metric</td><td>Maximum Length</td>
	 * 		</thead>
	 * 		<tbody>
	 * 			<tr><td>ACTION</td><td>32</td></tr>
	 * 			<tr><td>CLIENTID</td><td>64</td></tr>
	 * 			<tr><td>ECID (string component)</td><td>64</td></tr>
	 * 			<tr><td>MODULE</td><td>48</td></tr>
	 * 		</tbody>
	 * 	</table>
	 * </p>
	 * <p>
	 * Even though Oracle states that thouse are the maximum limits, some people have noted that at least on 10g
	 * you cannot query by module if it's more then 8 chars long... So it's something to keep in mind.
	 * </p>
	 * <p>
	 * If <code>module</code> and/or <code>action</code> parameters are null,
	 * then they will be set to default values, which are 
	 * </p>
	 * 
	 * @param module custom module attribute, 48 chars max, or it'll be cut off.
	 * @param action custom action 32 attribute, 32 chars max, rest will be cut off. 
	 * @return array of strings to be used to set metrics on Oracle connection.
	 */
	protected String[] getMetricsForConnection(String module, String action)
	{
		if(module == null) { module = MODULE_UNSPECIFIED; }
		if(action == null) { action = ACTION_UNSPECIFIED; }
		
		if(module.length() > 48) { module = module.substring(0, 48); }
		if(action.length() > 32) { action = action.substring(0, 32); }
		
		final String loggedInUserId = getLoggedInUser();
		
		String[] metrics = new String[OracleConnection.END_TO_END_STATE_INDEX_MAX];
		metrics[OracleConnection.END_TO_END_ACTION_INDEX]   = action;
		metrics[OracleConnection.END_TO_END_CLIENTID_INDEX] = loggedInUserId;
		metrics[OracleConnection.END_TO_END_ECID_INDEX]     = loggedInUserId;
		metrics[OracleConnection.END_TO_END_MODULE_INDEX]   = module;
		
		// TODO: Take this out before commiting to 
		System.out.println("*********************");
		System.out.println(StringUtils.arrayToCommaDelimitedString(metrics));
		System.out.println("*********************");
		
		// ExecutionContextId
		// metrics[OracleConnection.END_TO_END_ECID_INDEX] = "";
		return metrics;
	}
	
	/**
	 * Just provides array to be used as metrics to reset previous values.
	 * 
	 * @return array of nulls
	 */
	private String[] getResetMetrics()
	{
		String metrics[] = new String[OracleConnection.END_TO_END_STATE_INDEX_MAX];
		metrics[OracleConnection.END_TO_END_ACTION_INDEX] = null;
		metrics[OracleConnection.END_TO_END_MODULE_INDEX] = null;
		metrics[OracleConnection.END_TO_END_CLIENTID_INDEX] = null;
		
		return metrics;
	}
	
	/**
	 * Uses <a href="http://docs.spring.io/spring-security/site/docs/2.0.x/apidocs/org/springframework/security/context/SecurityContextHolder.html" 
	 * target="_new">SecurityContextHolder</a>
	 * class to get application context, from there it tries to get Authenticated user, from which UID will be taken and returned.
	 * 
	 * @return user id of currently logged in user as String or {@link #DEFAULT_USER_ID_STRING};
	 */
	private String getLoggedInUser()
	{
		SecurityContext ctx = SecurityContextHolder.getContext();
		
		if(ctx != null)
		{
			Authentication auth = ctx.getAuthentication();
			
			if(auth != null)
			{
				Object principal = auth.getPrincipal();
				
				LOGGER.debug("&&&& Logged In User Account: " + principal);
				
				if(principal instanceof AccountUser)
				{
					AccountUser accountUser = (AccountUser)principal;
					//return accountUser.getUsername();
					return String.valueOf(accountUser.getId());
				}
			}
		}
		
		return DEFAULT_USER_ID_STRING;
	}
	
	/**
	 * Gets current <code>java.sql.DataSource</code>.
	 * 
	 * @return current data source.
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * Sets data source to be used to a given value.
	 * @param dataSource <code>java.sql.DataSource</code>
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
