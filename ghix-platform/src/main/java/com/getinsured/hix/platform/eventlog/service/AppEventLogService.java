package com.getinsured.hix.platform.eventlog.service;

import com.getinsured.hix.platform.eventlog.EventDto;

public interface AppEventLogService {

	void record(EventDto eventDto, String comments);

}
