package com.getinsured.hix.platform.notification.exception;

public class NotificationException extends Exception 
{
	private static final long serialVersionUID = 1L;

	public NotificationException(String message)
	{
		super(message);
	}

	public NotificationException (String message, Exception cause) {
		super(message, cause); 
	}
}
