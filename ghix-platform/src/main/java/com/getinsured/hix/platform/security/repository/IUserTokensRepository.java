/**
 * 
 */
package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.UserTokens;

/**
 * @author Biswakesh
 *
 */
public interface IUserTokensRepository extends JpaRepository<UserTokens, Long> {
	
	@Query("select ut from UserTokens as ut where ut.userIdentifier=:userIdentifier and ut.sourceName=:sourceName and ut.eventName=:eventName")
	public List<UserTokens> findUnusedTokensWithSameEventAndSource(@Param("userIdentifier") String userIdentifier,@Param("sourceName") String sourceName,@Param("eventName") String eventName);
	
	@Query("select ut from UserTokens as ut where ut.token=:token")
	public UserTokens findUserToken(@Param("token") String token);
	
}
