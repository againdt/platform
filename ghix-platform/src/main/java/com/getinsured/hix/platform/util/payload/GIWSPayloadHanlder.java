package com.getinsured.hix.platform.util.payload;

import org.springframework.ws.context.MessageContext;

import com.getinsured.hix.model.GIWSPayload;


/**
 * GIWSPayloadHanlder interface to be implemented by all payload handler.
 * @author nayak_b
 *
 */
public interface GIWSPayloadHanlder {
	
	/**
	 * This method will provide handle to saved giwsPayload object and response object of invoked method.
	 * We need to be careful while type casting to specific response object.
	 * This handle method will run after all the operation completed in intercepter(This is the last handle point for the intercepter). 
	 * 
	 * @param giwsPayload
	 * @param response
	 */
	void handlePayload(GIWSPayload giwsPayload, Object response);
	
	/**
	 * This method can handle/modify the giwspayload object content. Here we have the handle to make any extraction from {@link MessageContext} 
	 * and direct payload in {@link GIWSPayload} object. Return giwsPayload with the data according to need.
	 * 
	 * Never set primary key i.e. id column/property of {@link GIWSPayload} object. 
	 * 
	 * @param giwsPayload
	 * @param messageContext
	 * @return
	 */
	//GIWSPayload handlePayload(GIWSPayload giwsPayload,MessageContext messageContext);
	
}
