package com.getinsured.hix.platform.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.databind.ObjectReader;

public class HttpErrorHandler extends DefaultResponseErrorHandler {
	
	private Logger logger = LoggerFactory.getLogger(HttpErrorHandler.class);

	
	@Override
	public void handleError(ClientHttpResponse response) throws IOException
	{
		RestfulResponseException re = null;

		try
		{
			super.handleError(response);
		}
		/*
		 * If client sent valid request, but server was not able to process it.
		 *
		 * Example:
		 *
		 * 1. Something in web layer sent valid request to svc layer, but things got out of whack
		 * there, maybe NPE during processing, maybe some conversion error between types, database exception,
		 * wrong parsing, and so on.
		 *
		 * 2. Everything went well during the process of handling incoming request on the SVC layer,
		 * and some database exception occurred that was not handled correctly (e.g. constrain violation, invalid sql, etc.)
		 *
		 * Basically, it was not the fault of the callee who sent request (web), but fault of who recieved it (svc), UNLESS
		 * its #2 above.
		 */

		/*
		 * If Client caused the error.
		 *
		 * Example: something in web layer sent wrong request to svc layer and svc layer rejected it
		 */
		catch (HttpServerErrorException | HttpClientErrorException e)
		{
			String responseStr = e.getResponseBodyAsString();
			logger.info("Error handling the error response, checking if the response is an exception in REST format, processing response");
			if(logger.isDebugEnabled()) {
				logger.debug(responseStr);
			}
			try {
				ObjectReader reader = JacksonUtils.getJacksonObjectReader(RestfulResponseException.class);
				re = reader.readValue(responseStr);
				if(re.getMessage() == null || re.getLogId() == null || re.getUrl() == null || re.getTag() == null)
				{
					if(!e.getStatusCode().is2xxSuccessful())
					{
						throw e;
					}
				}
				if(re != null)
				{
					throw re;
				}
			} catch (IOException e1) {
				// Genuine exception e
			}
			throw e;
			
		}
	}
}
