package com.getinsured.hix.platform.security.service;


public interface ModuleUserService {

	String EMPLOYER_MODULE = "employer";
	String ISSUER_MODULE = "issuer";
	String BROKER_MODULE = "broker";
	String EMPLOYEE_MODULE = "employee";
	String PLAN_MODULE = "plan";
	String INDIVIDUAL_MODULE = "individuals";
	String ENROLLMENT_MODULE = "ENROLLMENT";
	String ENROLLMENTENTITY_MODULE = "assisterenrollmententity";
	String ASSISTER_MODULE = "assister";
	String AGENCY_MODULE = "agency_manager";
	String APPROVED_ADMIN_STAFF_L2 = "APPROVEDADMINSTAFFL2";
	String APPROVED_ADMIN_STAFF_L1 = "APPROVEDADMINSTAFFL1";
	
	/*
	 *  NOTE :: DONOT UNCOMMENT THE FOLLOWING (all) methods - Venkata Tadepalli
	 *  	public ModuleUser saveModuleUser(ModuleUser moduleUser);
	 *  	public ModuleUser findModuleUserByUser(int userid, String moduleName);
	 *  	public List<ModuleUser> findByModuleIdModuleName(int moduleId,
	 *		String moduleName);
	 *		public void deleteModuleUser(ModuleUser moduleUser);
	 *		void deleteModuleUserByBrokerId(int brokerId, String moduleName);
	*/
	/*public ModuleUser saveModuleUser(ModuleUser moduleUser);

	public ModuleUser findModuleUserByUser(int userid, String moduleName);

	public List<ModuleUser> findByModuleIdModuleName(int moduleId,
			String moduleName);

	public void deleteModuleUser(ModuleUser moduleUser);
	
	void deleteModuleUserByBrokerId(int brokerId, String moduleName);*/
}