package com.getinsured.hix.platform.util;


import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.util.payload.GIWSPayloadHandlerFactory;
import com.getinsured.timeshift.util.TSDate;

/**
 * 
 * SoapServerLoggingInterceptor to intercept 
 * request/response for incoming SOAP Web Services.
 * 
 * 
 * @author EkramAli Kazi
 *
 */
public class SoapServerLoggingInterceptor implements MethodInterceptor{

	private static final Logger LOGGER = LoggerFactory.getLogger(SoapServerLoggingInterceptor.class);

	@Autowired private GIWSPayloadService giwsPayloadService;

	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		
		final UUID log_corelation_id = UUID.randomUUID();
		
		Object result = null;
		
		String requestPayload = null;
		String responsePayload = null;
		String exceptionTrace = null;
		String status = "SUCCESS";
		String correlationId = null;
		String globalId = null;

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		correlationId = log_corelation_id.toString();
		request.setAttribute("LOG_CORRELATION_ID", log_corelation_id.toString());
	
		if(request.getAttribute("LOG_GLOBAL_ID")!=null){
			globalId = request.getAttribute("LOG_GLOBAL_ID").toString();
		}
		
		Object[] arg = methodInvocation.getArguments();
		
		try {
			requestPayload = SoapHelper.marshal(arg[0]);
			result = methodInvocation.proceed();
			responsePayload = SoapHelper.marshal(result);
		} catch (Exception e) {
			status = "FAILURE";
			exceptionTrace = ExceptionUtils.getFullStackTrace(e);
			throw e;
		} finally {
			try{
				/*Ekram : HIX-33045 - Added for Eligibility RP module */
				String responseCode = SoapHelper.extractResponseCode(responsePayload);
				if (!responseCode.equalsIgnoreCase("200")) {
					status = "FAILURE";
				}
				String requestUrl = request.getRequestURL().toString();
				String methodName = methodInvocation.getMethod().getName();
				String endpointFunctionName = SoapHelper.getEndpointFunctionName(methodName);
				Long ssapApplicationId = SoapHelper.getSsapApplicationId(endpointFunctionName, requestPayload);
				String wsdl = SoapHelper.getWSDL(requestUrl, methodName);
				

				LOGGER.info("Soap Server Logging Interceptor ");
				
				GIWSPayload giwsPayload = new GIWSPayload();
				giwsPayload.setCorrelationId(correlationId);
				giwsPayload.setCreatedTimestamp(new TSDate());
				giwsPayload.setCreatedUserId(null);
				giwsPayload.setEndpointFunction(endpointFunctionName);
				giwsPayload.setEndpointOperationName(methodName);
				giwsPayload.setEndpointUrl(wsdl);
				giwsPayload.setExceptionMessage(exceptionTrace);
				giwsPayload.setRequestPayload(requestPayload);
				giwsPayload.setResponseCode(responseCode);
				giwsPayload.setResponsePayload(responsePayload);
				giwsPayload.setStatus(status);
				giwsPayload.setGlobalId(globalId);
				giwsPayload.setSsapApplicationId(ssapApplicationId);
				
				GIWSPayload savedGiwsPayload  = saveSeviceLog(giwsPayload);
				GIWSPayloadHandlerFactory.getHandler(endpointFunctionName).handlePayload(savedGiwsPayload, result);
			} catch(Exception e){
				/** log and eat exception to give chance to get response back to the caller. Something gone wrong with SoapServerLoggingInterceptor recording*/
				LOGGER.error("Error recording request/response in SoapServerLoggingInterceptor --> " + ExceptionUtils.getFullStackTrace(e));
			}
		}
		return result;
	}

	private GIWSPayload saveSeviceLog(GIWSPayload giwsPayload){
		return giwsPayloadService.save(giwsPayload);
	}

}
