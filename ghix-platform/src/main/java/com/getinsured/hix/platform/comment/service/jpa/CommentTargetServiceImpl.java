/**
 * 
 */
package com.getinsured.hix.platform.comment.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.platform.comment.repository.ICommentTargetRepository;
import com.getinsured.hix.platform.comment.service.CommentTargetService;

/**
 * @author panda_p
 *
 */

@Service("commentTargetService")
@Repository

public class CommentTargetServiceImpl implements CommentTargetService{

	@Autowired ICommentTargetRepository commentTargetRepository; 
	
	/**
	 * 
	 */
	public CommentTargetServiceImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Transactional
	public CommentTarget saveCommentTarget(CommentTarget commentGroup){
		return  commentTargetRepository.save(commentGroup);
	}
	
	public CommentTarget findByTargetIdAndTargetType(Long targetId, CommentTarget.TargetName targetType){
		return  commentTargetRepository.findByTargetIdAndTargetType(targetId, targetType);
	}	

}
