package com.getinsured.hix.platform.file.manager;

/**
 * @author nayak_b
 *
 */
public interface FileManagerService {
	
	/**
	 * This method will create a file under mentioned path with provided content. 
	 * @param path - not null
	 * @param fileName - not null
	 * @param dataBytes - not null
	 * @return
	 */
	public boolean uploadFile(String fileName, byte[] dataBytes,String floderName) ;

}
