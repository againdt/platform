package com.getinsured.hix.platform.util;
/**
 * @author meher_a
 */
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

@Component
public class JiraUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JiraUtil.class);
	
	private static final String PROJECT_URL = "https://jira.getinsured.com/";
	private static final String PROJECT_KEY = "HIX";
	private static String jiraPass;
	private static String jiraUser;
	
	/**
	 * 
	 * @param component
	 * @param fixVersion
	 * @param description
	 * @param summery
	 */
	public static void logBug(List<String> component, List<String> fixVersion, String description, String summery, String assigneeName){
		
		final AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
		final JiraRestClient restClient = factory.createWithBasicHttpAuthentication(URI.create(PROJECT_URL), 
				jiraUser, jiraPass);
		//final JiraRestClient restClient = factory.createWithBasicHttpAuthentication(URI.create(PROJECT_URL),"ghix_app", "2595-Terra-Bella");
		final String default_component = "GIProduction";
		final String default_fixVersion = "Backlog";
		
		try{
			IssueRestClient issueClient = restClient.getIssueClient();
			
			IssueInputBuilder issueInputBuilder = new IssueInputBuilder(PROJECT_KEY, 1L, summery);
			
			if(assigneeName != null){
				issueInputBuilder.setAssigneeName(assigneeName);
			}
			
			if(component != null){
				issueInputBuilder.setComponentsNames(component);
			}else{
				issueInputBuilder.setComponentsNames(Arrays.asList(default_component));
			}
			
			if(fixVersion != null){
				issueInputBuilder.setFixVersionsNames(fixVersion);
			}else{
				issueInputBuilder.setFixVersionsNames(Arrays.asList(default_fixVersion));
			}
			
			issueInputBuilder.setIssueTypeId(1L);
			issueInputBuilder.setDescription(description);
			
			IssueInput newIssue = issueInputBuilder.build();
			Promise<BasicIssue> promise = issueClient.createIssue(newIssue);
			
			//Collecting responses..
			BasicIssue basicIssue = promise.claim();
			LOGGER.info("Created issues:\n" + basicIssue);
			
		}finally {
			closeClient(restClient);
		}
	}

	private static void closeClient(JiraRestClient restClient) {
		try {
			restClient.close();
		} catch (IOException e) {
			LOGGER.error(e.getMessage(),e);
		}		
	}

	@Value("#{configProp['global.jiraPass']}")
	public void setJiraPass(String jiraPass) {
		JiraUtil.jiraPass = jiraPass;
	}

	@Value("#{configProp['global.jiraUser']}")
	public void setJiraUser(String jiraUser) {
		JiraUtil.jiraUser = jiraUser;
		
	}

}
