package com.getinsured.hix.platform.security;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;

public class GhixBatchHttpRequestInterceptor implements ClientHttpRequestInterceptor {

	@Autowired private UserService userService;
	private static final String principalRequestHeader = "GHIX_SSO_USER";
	
    @Override
    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        HttpHeaders headers = request.getHeaders();
		headers.add(principalRequestHeader, getUserName());
        return execution.execute(request, body);
    }
    
    private String getUserName() {
    	AccountUser user = null;
    	try {
			user = userService.getLoggedInUser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	if(user != null) {
    		return user.getUsername();
    	}
    	
    	return null;
    }
}