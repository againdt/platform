package com.getinsured.hix.platform.clamav;


import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ClamConnectionFactory extends BasePooledObjectFactory<ClamConnection> {
	
	private Logger LOGGER = LoggerFactory.getLogger(ClamConnectionFactory.class);

	@Value("#{configProp['clamav.host'] != null ? configProp['clamav.host']:'sfo-av1.ghixqa.com'}")
	private String host;
	
	@Value("#{configProp['clamav.port'] != null ? configProp['clamav.port']:'3310'}")
	private String clamAVPort;
	
	@Value("#{configProp['clamav.timeout'] != null ? configProp['clamav.timeout']:'20000'}")
	private String clamAVTimeout;
	
	@Value("#{configProp['clamav.enabled'] != null ? configProp['clamav.enabled']:'false'}")
	private String clamAVEnabled;

	private int created=0;
	
	private synchronized int updateLiveObjectsCount(int count){
		created = created + count;
		return created;
	}
	
	public boolean validateObject(PooledObject<ClamConnection> wrappedObj){
		ClamConnection conn = wrappedObj.getObject();
		return conn.validate();
	}
	
	
	public void destroyObject(PooledObject<ClamConnection> wrappedObj){
		created = updateLiveObjectsCount(-1);
		ClamConnection conn = wrappedObj.getObject();
		conn.log("Connection closed by the pool");
		conn.close();
	}
	
	@Override
	public ClamConnection create() {
		if(this.clamAVEnabled.equalsIgnoreCase("true")){
			created = updateLiveObjectsCount(1);
			LOGGER.debug("Live Conection Objects:"+created);
			return new ClamConnection(host, Integer.parseInt(clamAVPort), Integer.parseInt(clamAVTimeout));
		}
		LOGGER.warn("Clam AV not enabled, no connection will be created");
		return null;
	}

	/**
	 * Use the default PooledObject implementation.
	 */
	@Override
	public PooledObject<ClamConnection> wrap(ClamConnection buffer) {
		return new DefaultPooledObject<ClamConnection>(buffer);
	}

	/**
	 * When an object is returned to the pool, clear the buffer.
	 */
	@Override
	public void passivateObject(PooledObject<ClamConnection> pooledObject) {
		// Do nothing
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getClamAVPort() {
		return clamAVPort;
	}

	public void setClamAVPort(String clamAVPort) {
		this.clamAVPort = clamAVPort;
	}

	public String getClamAVTimeout() {
		return clamAVTimeout;
	}

	public void setClamAVTimeout(String clamAVTimeout) {
		this.clamAVTimeout = clamAVTimeout;
	}

	public String getClamAVEnabled() {
		return clamAVEnabled;
	}

	public void setClamAVEnabled(String clamAVEnabled) {
		this.clamAVEnabled = clamAVEnabled;
	}

	// for all other methods, the no-op implementation
	// in BasePooledObjectFactory will suffice
}
