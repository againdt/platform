package com.getinsured.hix.platform.security;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.lang.ObjectUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.usertype.UserType;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class GhixDBSafeHtml implements UserType, ApplicationContextAware {

	public Class<String> returnedClass() {
		return String.class;
	}

	public int[] sqlTypes() {
		return new int[] { Types.VARCHAR };
	}

	public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner)
			throws HibernateException, SQLException {
		String value = (String) StandardBasicTypes.STRING.nullSafeGet(resultSet, names[0], session, owner);
		value = ((value != null) ? ESAPI.encoder().encodeForHTML(value) : null);
		return value;
	}

	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)
			throws HibernateException, SQLException {
		StandardBasicTypes.STRING.nullSafeSet(preparedStatement, (value != null) ? value.toString() : null, index, session);
	}

	/* "default" implementations */

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		return ObjectUtils.equals(x, y);
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		assert(x != null);
		return x.hashCode();
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		ac = arg0;

	}

	ApplicationContext ac;
}
