package com.getinsured.hix.platform.auditor.advice;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.auditor.GiAuditParameter;

@Component
public class GiAuditParameterUtil {

	
	private static final String AUDIT_PARAM="-Daudit.aop.enabled";

	private static boolean isauditEnabled=true;
	
	private static final String AUDIT_OFF="off";
	
	
	@PostConstruct
	public void checkIsAuditingEnabled()
	{
			List<String> startupParams=ManagementFactory.getRuntimeMXBean().getInputArguments();
			 for(String param:startupParams)
			 {
				 if(param.trim().startsWith(AUDIT_PARAM))
				 {
					 String[] strSplit=param.split("=");
					 if(AUDIT_OFF.equalsIgnoreCase(strSplit[1]))
					 {
						 isauditEnabled=false;
						 
					 }break;
				 }
			 }
		
	}

	public static void add(GiAuditParameter... parameters ){

		if (isauditEnabled) {
			for (GiAuditParameter giAuditParameter : parameters) {
				GiAuditContextHolder.put(giAuditParameter.getKey(),
						giAuditParameter.getValue());
			}
		}

	}

	public static void add(ArrayList<GiAuditParameter> paramsChanged) {
		for(GiAuditParameter param:paramsChanged){
			GiAuditContextHolder.put(param.getKey(),
					param.getValue());
		}
	}
}
