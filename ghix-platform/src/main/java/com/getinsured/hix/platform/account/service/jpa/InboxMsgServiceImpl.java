package com.getinsured.hix.platform.account.service.jpa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.model.InboxMsgClob;
import com.getinsured.hix.model.InboxMsgDoc;
import com.getinsured.hix.model.InboxMsgResponse;
import com.getinsured.hix.model.InboxUserAddressBookResp;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.platform.account.repository.IInboxMsgClobRepository;
import com.getinsured.hix.platform.account.repository.IInboxMsgDocRepository;
import com.getinsured.hix.platform.account.repository.IInboxMsgRepository;
import com.getinsured.hix.platform.account.service.InboxMsgService;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.ObjectClonner;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author polimetla_b, Nikhil Talreja, parmar_b
 * @since 11/16/2012
 * 
 */

@Service("inboxMsgService")
@DependsOn("couchDocumentHelper")
public class InboxMsgServiceImpl implements InboxMsgService {
	
	private static final String CLASS_NAME = "InboxMsgServiceImpl";

	
	@Autowired private ContentManagementService ecmService;
	@Autowired private UserService userService;
	
	@Autowired private IInboxMsgClobRepository iInboxMsgClobRepository;
	@Autowired private IInboxMsgRepository iInboxMsgRepository;
	@Autowired private IInboxMsgDocRepository iInboxMsgDocRepository;
	@Autowired private IUserRepository iUserRepository;
	
	@SuppressWarnings("rawtypes")
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(InboxMsgServiceImpl.class);
	
	//Constants
	private static final int MAX_MSG_LENGTH = 4000;
//	private static final int MAX_CONTACTS_SIZE = 100;
	
	private static final int SUCCESS_CODE = 0;
	private static final String SUCCESS_STRING = "SECURE_INBOX_SUCCESS";
	private static final String SUCCESS_STATUS = "SUCCESS";
	
	private static final int NULL_MESSAGE_ERROR_CODE = 1000;
	private static final String NULL_MESSAGE_ERROR_STRNG = "SECURE_INBOX_NULL_MESSAGE";
	
	private static final int INVALID_USER_ERROR_CODE = 1001;
	private static final String INVALID_USER_ERROR_STRNG = "SECURE_INBOX_INVALID_USER";
	
	private static final int INVALID_MESSAGE_STATUS_ERROR_CODE = 1002;
	private static final String INVALID_MESSAGE_STATUS_ERROR_MESSAGE = "SECURE_INBOX_INVALID_MESSAGE_STATUS";
	
	private static final int NULL_ATTACHMENT_ERROR_CODE = 1003;
	private static final String NULL_ATTACHMENT_ERROR_STRNG = "SECURE_INBOX_NULL_ATTACHMENT";
	
	private static final int INVALID_ATTACHMENT_DOCUMENT_ERROR_CODE = 1004;
	private static final String INVALID_ATTACHMENT_DOCUMENT_ERROR_STRING = "SECURE_INBOX_INVALID_ATTACHMENT_DOCUMENT";
	
	private static final String FAILURE_STATUS = "FAILED";
	
	//Column names
	private static final String STATUS_COLUMN = "status";
	private static final String DATE_CREATED_COLUMN = "dateCreated";

	/**
	 * Method to save a message in DB
	 * 
	 * <P>
	 * Possible incoming status is N-New, O-Open, P-Replied, F-Forwarded,
	 * A-Archived, R-Read, S-Sent, D-Deleted, C-Compose
	 * 
	 * @author Nikhil Talreja
	 * @param msg
	 *            - Message to be saved
	 * @return object of InboxMsgResponse which contains a list of messages that
	 *         have been saved
	 * @throws GIException
	 *             - In case cloning of Object fails
	 * @throws ContentManagementServiceException 
	 */
	@Transactional
	@Override
	public InboxMsgResponse saveMessage(final InboxMsg msg) throws GIException, ContentManagementServiceException {

		LOGGER.info(CLASS_NAME + ".saveMessage Begin ");
		String inboxMsgBodyInput = msg.getMsgBody();
		// Set the status to sent / replied / forward/archived..etc
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();
		InboxMsg savedMsg = null;
		try{
			inboxMsgResponse.startResponse();
			//startTime = TSCalendar.getInstance().getTimeInMillis();
	
			// Step 1: Validate incoming messages and check message body size.
			validateMessage(inboxMsgResponse,msg);
			
			if (inboxMsgResponse.getErrCode() != 0){
				inboxMsgResponse.endResponse();
				return inboxMsgResponse;
			}
		
			if(msg.getMsgBody() != null && msg.getMsgBody().length() > MAX_MSG_LENGTH){
				
				LOGGER.info("Using clob to store msg body");
				msg.setIsClobUsed(InboxMsg.CLOB_USED.Y);
				LOGGER.info("Saved CLOB status as YES for message : " + msg.getId());
				
				//Creating new INBOX_MSG_CLOB entry
				InboxMsgClob msgClob = new InboxMsgClob();
				msgClob.setMsgClob(msg.getMsgBody());
				
				/*
				 * Setting Message body contents to "CLOB"
				 * This will help in preventing overflow of Database column 
				 */
				msg.setMsgBody("CLOB");
				savedMsg = iInboxMsgRepository.save(msg);
				
				msg.setId(savedMsg.getId());
				msgClob.setId(msg.getId());
				
				iInboxMsgClobRepository.save(msgClob);
				LOGGER.info("Saved CLOB object for message : " + msg.getId());
			}
			else{
				/*
				 * Clob is not to be used for the message
				 * This update is being performed in case of multiple saves for a draft in which message body size keeps changing drastically
				 */
				msg.setIsClobUsed(InboxMsg.CLOB_USED.N);
				LOGGER.info("Saved CLOB status as NO for message : " + msg.getId());
			}
			
			
			/*
			 * Step 2: Saving newly created messages. 
			 * For C-Compose, A-Archived, D-Delete Just save the message and return.
			 * Previous status of the same message must be as follows:
			 * If new status is Compose, previous status should be null or C
			 * If new status is Archive or Delete, previous status should be Archive or Delete respectively
			 * A failed response is returned if above conditions are not met
			 * 
			 */
			
			//Fetching current status of message from DB
			InboxMsg msgFromDB = iInboxMsgRepository.findById(msg.getId());
			InboxMsg.STATUS previousStatus = null;
			if(msgFromDB != null){
				LOGGER.info("Previous message id : " + msgFromDB.getId());
				previousStatus = msgFromDB.getStatus();
			}
			else{
				LOGGER.info("No message found with id : " + msg.getId());
			}
			//LOGGER.info("Previous status of message is " + previousStatus);
			//LOGGER.info("New status of message is " + msg.getStatus());
			if(msg.getStatus() == InboxMsg.STATUS.C){ 
				if (previousStatus != null && previousStatus != InboxMsg.STATUS.C) {
					generateFailedResponse(inboxMsgResponse,INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
					return inboxMsgResponse;
				}
				//This is taken care of in validate message
				/*if(msg.getStatus() == InboxMsg.STATUS.A && (previousStatus == null || previousStatus != InboxMsg.STATUS.A)){
						generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
						inboxMsgResponse.endResponse();
						return inboxMsgResponse;
				}
				if(msg.getStatus() == InboxMsg.STATUS.D && (previousStatus == null || previousStatus != InboxMsg.STATUS.D)){
						generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
						inboxMsgResponse.endResponse();
						return inboxMsgResponse;
				}*/
				
				// Adding the saved message to InboxMsgResponse object and returning
			
				savedMsg = iInboxMsgRepository.save(msg);
	
				generateSuccessRepsonse(inboxMsgResponse, savedMsg);
				return inboxMsgResponse;
			}
			
				
			
			
			/*
			 * Step 3: For P-Replied, F-Forwarded, S-Sent , Generate records to given target user IDs
			 * Previous status of the same message must be as follows:
			 * C - If current status is Sent
			 * S, F, P, R, N - If current status is Replied/Forwarded
			 * 
			 */
			if(msg.getStatus() == InboxMsg.STATUS.S && (previousStatus != InboxMsg.STATUS.S && previousStatus != null)){
					generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
					return inboxMsgResponse;
			}
			if(msg.getStatus() == InboxMsg.STATUS.P || msg.getStatus() == InboxMsg.STATUS.F){
				if((previousStatus == InboxMsg.STATUS.A || previousStatus == InboxMsg.STATUS.D)){
					generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
					return inboxMsgResponse;
				}
				//Commenting this part since, replies and forwards should also result in creation of messages for recipients 
				/*else{
					// Adding the saved message to InboxMsgResponse object and returning
					savedMsg = iInboxMsgRepository.save(msg);
	
					generateSuccessRepsonse(inboxMsgResponse, savedMsg);
					return inboxMsgResponse;
				}*/
			}
			
			/*
			 * Step 4: For N-New Generate failed response
			 * This is taken care of in validate message
			 */
			/*if(msg.getStatus() == InboxMsg.STATUS.R){
				generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
				inboxMsgResponse.endResponse();
				return inboxMsgResponse;
			}*/
			
			/*
			 * Step 5: For N-New update the status
			 */
			if (msg.getStatus() == InboxMsg.STATUS.N) {

				// Updating message and returning
				//msg.setStatus(InboxMsg.STATUS.R);
				savedMsg = iInboxMsgRepository.save(msg);

				generateSuccessRepsonse(inboxMsgResponse,savedMsg);
				return inboxMsgResponse;
			}
			
			
			/*
			 * Step 6:  Save message for recipients
			 */
	
			// Getting sender details
			long senderId = msg.getFromUserId();
			AccountUser sender = userService.findById((int) senderId);
			LOGGER.info("Sender : " + sender);
	
			if (sender == null) {
				LOGGER.error("Invalid sender found with id " + senderId);
				
				generateFailedResponse(inboxMsgResponse, INVALID_USER_ERROR_CODE,INVALID_USER_ERROR_STRNG);
				return inboxMsgResponse;
				
			}
	
			// Setting Owner Id and Name
			msg.setOwnerUserId(sender.getId());
			msg.setOwnerUserName(sender.getFullName());
			//LOGGER.info("Setting owner's name as : " + msg.getOwnerUserName());
	
			// Setting notification sent status
			msg.setIsEmailSent(InboxMsg.EMAIL_SENT_STATUS.N);
	
			// Creating messages for each recipient
			if (msg.getToUserIdList() != null
					&& msg.getToUserIdList().length() != 0) {
	
				// Creating Recipient message with common fields
				InboxMsg recipientMsg = new InboxMsg();
				// From user Id and user Name
				recipientMsg.setFromUserId(msg.getOwnerUserId());
				recipientMsg.setFromUserName(msg.getOwnerUserName());
	
				// To user Id list and Name
				recipientMsg.setToUserIdList(msg.getToUserIdList());
				recipientMsg.setToUserNameList(msg.getToUserNameList());
	
				// Other fields
				recipientMsg.setLocale(msg.getLocale());
				recipientMsg.setStatus(InboxMsg.STATUS.N);
				recipientMsg.setPriority(msg.getPriority());
				recipientMsg.setType(msg.getType());
				recipientMsg.setMsgSub(msg.getMsgSub());
				recipientMsg.setMsgBody(inboxMsgBodyInput);
				recipientMsg.setContentType(msg.getContentType());
				if(recipientMsg.getMsgBody() != null && recipientMsg.getMsgBody().length() > MAX_MSG_LENGTH){
					
					LOGGER.info("Using clob to store msg body");
					recipientMsg.setIsClobUsed(InboxMsg.CLOB_USED.Y);
					LOGGER.info("Saved CLOB status as YES for message : " + recipientMsg.getId());
					//Creating new INBOX_MSG_CLOB entry
					InboxMsgClob recipientMsgClob = new InboxMsgClob();
					recipientMsgClob.setMsgClob(inboxMsgBodyInput);
					/*
					 * Setting Message body contents to "CLOB"
					 * This will help in preventing overflow of Database column 
					 */
					recipientMsg.setMsgBody("CLOB");
					recipientMsg = iInboxMsgRepository.save(recipientMsg);
					recipientMsgClob.setId(recipientMsg.getId());
					iInboxMsgClobRepository.save(recipientMsgClob);
					LOGGER.info("Saved CLOB object for message : " + recipientMsg.getId());
				}
				else{
					recipientMsg.setIsClobUsed(InboxMsg.CLOB_USED.N);
					LOGGER.info("Saved CLOB status as NO for message : " + recipientMsg.getId());
				}
				recipientMsg.setPreviousId(msg.getPreviousId());
				recipientMsg.setIsEmailSent(InboxMsg.EMAIL_SENT_STATUS.N);
			
				StrTokenizer recipientTokenizer = new StrTokenizer(
						msg.getToUserIdList(), ",");
				
				recipientTokenizer.setIgnoreEmptyTokens(true);
				
				while (recipientTokenizer.hasNext()) {
					int recipientId = Integer.parseInt(recipientTokenizer
							.nextToken());
					AccountUser recipient = userService.findById(recipientId);
					if (recipient == null) {
						LOGGER.error("Invalid recipient found with id "
								+ recipientId);
						continue;
					}
					// Creating new message for the recipient and populating it
					LOGGER.info("Creating new message for recipient " + recipient);
	
					// Creating object to be persisted
					InboxMsg savedMessage = new InboxMsg();
					ObjectClonner cloner = new ObjectClonner();
					savedMessage = (InboxMsg) cloner.clone(recipientMsg);
	
					// Setting Owner details
					savedMessage.setOwnerUserId(recipientId);
					savedMessage.setOwnerUserName(recipient.getFullName());
					LOGGER.info("Message created for recipient with Id : "
							+ recipientId);
					
					
					List<ModuleUser> moduleUserList = userService
							.getUserModules(recipientId);
					if (null != moduleUserList && !moduleUserList.isEmpty()) {
						
						savedMessage.setModuleId(moduleUserList.get(0)
								.getModuleId());
						savedMessage.setModuleName(moduleUserList.get(0)
								.getModuleName());
					}
					
					LOGGER.info("Saving the recipient message to DB");
					savedMessage = iInboxMsgRepository.save(savedMessage);
					LOGGER.info("Recipient Message saved with id " + savedMessage.getId());
					
					//Creating attachment list for recipient message
					if(msg.getMessageDocs() != null && msg.getMessageDocs().size() != 0){
						
						LOGGER.info("Creating attachment objects for recipient");
						List<InboxMsgDoc> recipientAttachments = new ArrayList<InboxMsgDoc>();
						savedMessage.setMessageDocs(recipientAttachments);
						
						for(InboxMsgDoc doc : msg.getMessageDocs()){
							
							InboxMsgDoc recipientAttachment = (InboxMsgDoc)cloner.clone(doc);
							//Primary key of the document is made 0 so that a new record is created
							//Adding attachments to recipient messages in DB and Alfresco
							if(recipientAttachment != null && recipientAttachment.getDocId() != null){
								recipientAttachment.setId(0L);
								byte[] attachmentBytes = getAttachment(new InboxMsgResponse(), recipientAttachment.getDocId());
								if(attachmentBytes != null){
									savedMessage.getMessageDocs().add(recipientAttachment);
									addAttachment(savedMessage, attachmentBytes);
									LOGGER.info("Attachment added to recipient message");
								}
							}
						}
						
					}
					
					// Adding messages to list for batch update
					//recipientMessages.add(savedMessage);
				}
				/*LOGGER.info("Saving the batch of recipient messages to DB");
				recipientMessages = iInboxMsgRepository.save(recipientMessages);
				LOGGER.info("Recipient Messages saved");*/
	
				// Saving the Incoming msg Object
				savedMsg = iInboxMsgRepository.save(msg);
	
				// Adding the saved messages to InboxMsgResponse object
				generateSuccessRepsonse(inboxMsgResponse, savedMsg);
				return inboxMsgResponse;
			}
			
		}
		finally{
			if(savedMsg != null){
				LOGGER.info("Message saved");
			}
			LOGGER.info(CLASS_NAME + ".saveMessage End ");
			inboxMsgResponse.endResponse();
			
		}
		return inboxMsgResponse;

		// FIXME - Send alerts in case of sent/replied/forward (To be implemented later)
	}

	/**
	 * Method is used to add attachment file to Alfresco Content Management API.
	 * @param msg, Object of InboxMsg POJO contains existing message from DB.
	 * @param attachment, Byte array contains attached file data.
	 * 
	 * @return object of InboxMsg with attachment file details.
	 */
	@Override
	public InboxMsgResponse addAttachment(final InboxMsg msg, final byte[] attachment)
			throws GIException, ContentManagementServiceException {
		// Send the document to Alfresco and update file details in attachments table
		LOGGER.info("{}.addAttachment Begin", CLASS_NAME);
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();

		try {

			// Set the status to sent / replied / forward/archived..etc
			inboxMsgResponse.startResponse();
			
			if (null == msg) {
				LOGGER.debug("InboxMsg(msg) was null");
				generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE, NULL_MESSAGE_ERROR_STRNG);
				return inboxMsgResponse;
			}
			
			if (null == attachment) {
				LOGGER.debug("byte[] for attachment was null");
				generateFailedResponse(inboxMsgResponse, NULL_ATTACHMENT_ERROR_CODE, NULL_ATTACHMENT_ERROR_STRNG);
				return inboxMsgResponse;
			}
			
			List <InboxMsgDoc> inboxMsgDocList = msg.getMessageDocs();
			
			LOGGER.info("Message found with Id: {}", msg.getId()) ;
			
			//InboxMsg.STATUS msgStatus = inboxMsg.getStatus();
			
			if (null != inboxMsgDocList && !inboxMsgDocList.isEmpty()) {
				LOGGER.info("Message has {} attachments", inboxMsgDocList.size() );

				InboxMsgDoc inboxMsgDoc = inboxMsgDocList.get(inboxMsgDocList.size() - 1);
				//LOGGER.info("inboxMsgDoc is null: " + (inboxMsgDoc == null));
				//LOGGER.info("inboxMsgDocList.size(): " + inboxMsgDocList.size());
				
				if (null != inboxMsgDoc
						&& !StringUtils.isEmpty(inboxMsgDoc.getDocName())) {

					inboxMsgDoc.setMessage(msg);
					// String relativePath =
					// inboxMsgDoc.getDocName().substring(0,
					// inboxMsgDoc.getDocName().lastIndexOf("/"));
					// String fileName =
					// inboxMsgDoc.getDocName().substring(inboxMsgDoc.getDocName().lastIndexOf("/")
					// + 1);

//					String relativePath = "inbox/" + msg.getId() + "/";
					String relativePath = msg.getOwnerUserId() + "/inbox/" + msg.getId() + "/";
					String fileName = inboxMsgDoc.getDocName();

					//LOGGER.info("Saving file : " + relativePath + fileName);

					String documentId = ecmService.createContent(relativePath,
							fileName, attachment, ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.ATTACHMENT, null);
					LOGGER.info("ContentManagementService.uploadDocument documentId: {}", documentId);
					inboxMsgDoc.setDocId(documentId);
					inboxMsgDoc = iInboxMsgDocRepository.save(inboxMsgDoc);
					generateSuccessRepsonse(inboxMsgResponse, msg);

				} else {
					generateFailedResponse(inboxMsgResponse,
							INVALID_ATTACHMENT_DOCUMENT_ERROR_CODE,
							INVALID_ATTACHMENT_DOCUMENT_ERROR_STRING);
				}
				
				
			} 
			else {
				generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE, INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
			}
			
		} finally {
			LOGGER.info("{}.addAttachment End", CLASS_NAME);
			inboxMsgResponse.endResponse();
		}
		return inboxMsgResponse;
	}

	/**
	 * Method is used to get attachment file from Alfresco Content Management API.
	 * @param documentID, Object of String contains Document ID for ECM attachment file.
	 * 
	 * @return Byte array contains attached file data.
	 */
	@Override
	public byte[] getAttachment(InboxMsgResponse inboxMsgResponse, String documentID) 
			throws GIException, ContentManagementServiceException {
		// Get the file from Alfresco and return to user for download.
		LOGGER.info(CLASS_NAME + ".getAttachment Begin ");
		byte[] attachment = null;
		
		try {

			// Set the status to sent / replied / forward/archived..etc	
			inboxMsgResponse.startResponse();
			
			if (StringUtils.isEmpty(documentID)) {
				LOGGER.debug("Document ID for ECM attachment file was null");
				generateFailedResponse(inboxMsgResponse, NULL_ATTACHMENT_ERROR_CODE, NULL_ATTACHMENT_ERROR_STRNG);
				return attachment;
			}
			
			attachment = ecmService.getContentDataById(documentID);
			LOGGER.info("AlfrescoContentManagementService.getContentDataById attachment bytes is null: " + (attachment == null));
		
		} finally {
			LOGGER.info(CLASS_NAME + ".getAttachment End ");
			inboxMsgResponse.endResponse();
		}
		return attachment;
	}
	
	/**
	 * Removes attachments from a message and Alfresco
	 * 
	 * @author Nikhil Talreja
	 * @since 19 December 2012
	 * @param msg
	 * @param attachment
	 * @return InboxMsgResponse Object containing state of operation
	 * @throws GIException, ContentManagementServiceException 
	 */
	@Override
	public InboxMsgResponse removeAttachment(InboxMsg msg, String fileName) throws GIException, ContentManagementServiceException{
		
		LOGGER.info(CLASS_NAME + ".removeAttachment Begin ");
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();

		try {
			
			if (null == msg) {
				LOGGER.debug("InboxMsg(msg) was null");
				generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE, NULL_MESSAGE_ERROR_STRNG);
				return inboxMsgResponse;
			}
			
			List<InboxMsgDoc> existingAttachments = msg.getMessageDocs();
			List<InboxMsgDoc> attachmentsForRemoval = new ArrayList<InboxMsgDoc>();
			if(existingAttachments == null || existingAttachments.size() == 0){
				LOGGER.info("No attachments for this Message");
				return null;
			}
			else{
				
				LOGGER.info("Removing attachment from Alfresco");
				for(InboxMsgDoc attachment : existingAttachments){
					
					if(attachment != null && attachment.getDocName().equalsIgnoreCase(fileName)){
						String docId = attachment.getDocId();
						if(docId != null && docId.length() != 0){
							ecmService.deleteContent(docId);
							//LOGGER.info("Doc " + attachment.getDocName() + " removed from Alfresco");
						}
						attachmentsForRemoval.add(attachment);
					}
				}
				
			}
			LOGGER.info("Removing attachments from DB");
			iInboxMsgDocRepository.deleteInBatch(attachmentsForRemoval);
			LOGGER.info("Attachment removed from the message");
			
		}
		finally {
			LOGGER.info(CLASS_NAME + ".removeAttachment End ");
			inboxMsgResponse.endResponse();
		}
		return inboxMsgResponse;
		
	}
	
	/**
	 * Method is used to cancel/delete message from Inbox .
	 * @param msg, Object of InboxMsg POJO contains existing message from DB.
	 * 
	 * @return object of InboxMsgResponse which contains a list of messages.
	 */
	@Override
	public InboxMsgResponse cancelMessage(InboxMsg msg) throws GIException {
		// Do this only if the message is in compose sate.
		// Just change the status to archive/delete
		// If deleted it won't appear in any folder.

		LOGGER.info(CLASS_NAME + ".cancelMessage Begin");

		// Set the status to sent / replied / forward/archived..etc
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();
		InboxMsg inboxMsg = null;
		
		try {
			
			inboxMsgResponse.startResponse();
			// Checking message
			if (msg == null) {
				LOGGER.debug(NULL_MESSAGE_ERROR_STRNG);
				generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE,NULL_MESSAGE_ERROR_STRNG);
			}
			
			if (inboxMsgResponse.getErrCode() != 0) {
				return inboxMsgResponse;
			}
			
			//Checking if call is for Delete/Discard
			if(null != msg && msg.getStatus() != null && msg.getStatus() == InboxMsg.STATUS.D){
				
				LOGGER.debug("Message is being discarded, setting status to DELETED");
				
				// Saving the message
				inboxMsg = iInboxMsgRepository.save(msg);
				LOGGER.debug("Message with id " + inboxMsg.getId()+ " saved with status " + inboxMsg.getStatus());
				generateSuccessRepsonse(inboxMsgResponse, inboxMsg);
				return inboxMsgResponse;
			}
			if(null != msg ){
				inboxMsg = iInboxMsgRepository.findById(msg.getId());
			}
			
			InboxMsg.STATUS prevStatus = null;
			LOGGER.debug("InboxMsg is null: " + (inboxMsg == null));
			
			if(null != inboxMsg) {
				prevStatus = inboxMsg.getStatus();
				LOGGER.debug("prevStatus: " + prevStatus);
			}
	
			if (null == prevStatus || prevStatus == InboxMsg.STATUS.C) {
	
				msg.setStatus(InboxMsg.STATUS.A);
				LOGGER.debug("Message is being cancelled, setting status to ARCHIEVED");
				
				// Saving the message
				inboxMsg = iInboxMsgRepository.save(msg);
				LOGGER.debug("Message with id " + inboxMsg.getId()+ " saved with status " + inboxMsg.getStatus());
				generateSuccessRepsonse(inboxMsgResponse, inboxMsg);
				return inboxMsgResponse;
				
			} 
			else {
				generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE, INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
			}
		
		} finally {
			LOGGER.info(CLASS_NAME + ".cancelMessage End");
			inboxMsgResponse.endResponse();
			
		}
		generateSuccessRepsonse(inboxMsgResponse, inboxMsg);
		return inboxMsgResponse;
	}

	/**
	 * Method to search messages in DB based on search parameters
	 * 
	 * @author Nikhil Talreja
	 * @param msg
	 *            - Object containing search parameters
	 * @param startDate
	 *            - Start Date parameter for search
	 * @param endDate
	 *            - End Date parameter for search
	 * @return object of InboxMsgResponse which contains a list of messages that
	 *         have been returned from the search
	 */
	@Override
	public InboxMsgResponse searchMessages(final InboxMsg msg, final Calendar startDate,
			Calendar endDate) {

		/*
		 *  Search only in database and returns the results. Don't search archived and deleted messages.
		 *  No need to check with Alfresco at this time
		 */

		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();
		List<InboxMsg> messages = new ArrayList<InboxMsg>();
		
		LOGGER.info(CLASS_NAME + ".searchMessages Begin");
		

		// Step 1: Validate incoming messages.
		if (msg == null) {
			LOGGER.debug(NULL_MESSAGE_ERROR_STRNG);
			generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE,NULL_MESSAGE_ERROR_STRNG);
		}
		
		//Query builder for creating dynamic queries
		@SuppressWarnings("unchecked")
		QueryBuilder<InboxMsg> inboxMsgQuery = delegateFactory.getObject();
		inboxMsgQuery.buildObjectQuery(InboxMsg.class);
		
		// Fetching messages for a particular owner which are not
		// deleted/archived
		if (msg.getOwnerUserId() != 0) {
			LOGGER.info("Fetching messages for owner Id " + msg.getOwnerUserId());
			inboxMsgQuery.applyWhere("ownerUserId", (int)msg.getOwnerUserId(), DataType.NUMERIC, ComparisonType.EQ);
			inboxMsgQuery.applyWhere(STATUS_COLUMN, InboxMsg.STATUS.A, DataType.ENUM, ComparisonType.NE);
			inboxMsgQuery.applyWhere(STATUS_COLUMN, InboxMsg.STATUS.D, DataType.ENUM, ComparisonType.NE);
		
			
		} 
		else {
			LOGGER.info("Invalid user Id " + msg.getOwnerUserId());
			generateFailedResponse(inboxMsgResponse, INVALID_USER_ERROR_CODE, INVALID_USER_ERROR_STRNG);
			inboxMsgResponse.endResponse();
			return inboxMsgResponse;
		}

		/*
		 * Fetching messages as per data criteria. If no end date is provided,
		 * current system date is used as end date
		 */
		if (startDate != null) {
			Date endDateCriteria = (endDate == null) ? new TSDate() : endDate
					.getTime();
			LOGGER.info("Fetching messages between : " + startDate.getTime()
					+ " and " + endDateCriteria);
			inboxMsgQuery.applyWhere(DATE_CREATED_COLUMN, startDate.getTime(), DataType.DATE, ComparisonType.GE);
			inboxMsgQuery.applyWhere(DATE_CREATED_COLUMN, endDateCriteria, DataType.DATE, ComparisonType.LE);
		}
		

		// Fetching messages by status
		if (msg.getStatus() != null) {
			//LOGGER.info("Fetching messages with status : " + msg.getStatus());
			inboxMsgQuery.applyWhere(STATUS_COLUMN, msg.getStatus(), DataType.ENUM, ComparisonType.EQ);
		}
		
		// Fetching messages by type
		if (msg.getType() != null) {
			//LOGGER.info("Fetching messages of type : " + msg.getType());
			inboxMsgQuery.applyWhere("type", msg.getType(), DataType.ENUM,ComparisonType.EQ);
		}

		// Fetching messages by priority
		if (msg.getPriority() != null) {
			/*LOGGER.info("Fetching messages with priority : "
					+ msg.getPriority());*/
			inboxMsgQuery.applyWhere("priority", msg.getPriority(), DataType.ENUM, ComparisonType.EQ);
		}
		
		//Search message Subject
		if (msg.getMsgSub() != null && msg.getMsgSub().trim().length() > 0){
			//LOGGER.info("Searching message subject for : "+ msg.getMsgSub());
			inboxMsgQuery.applyWhere("msgSub", msg.getMsgSub().trim().toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		
		//Search message Body
		if (msg.getMsgBody() != null && msg.getMsgBody().trim().length() > 0) {
			//LOGGER.info("Searching message body for : " + msg.getMsgBody());
			inboxMsgQuery.applyWhere("msgBody", msg.getMsgBody().trim().toUpperCase(),DataType.STRING, ComparisonType.LIKE);
		}
		
		//Search is_email_Sent
		if (msg.getIsEmailSent() != null && msg.getIsEmailSent() != InboxMsg.EMAIL_SENT_STATUS.A) {
			//LOGGER.info("Fetching messages by email sent status : "	+ msg.getIsEmailSent());
			inboxMsgQuery.applyWhere("isEmailSent", msg.getIsEmailSent(), DataType.ENUM, ComparisonType.EQ);
		}

		inboxMsgQuery.setFetchDistinct(true);
		messages = inboxMsgQuery.getRecords(1, -1);
		
		LOGGER.info("Fetch size : " + messages.size());
		
		generateSuccessRepsonseForSearch(inboxMsgResponse, messages);
		inboxMsgResponse.endResponse();
		
		return inboxMsgResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.account.service.InboxMsgService#searchAttachments(
	 * com.getinsured.hix.account.model.InboxMsg)
	 */
	@Override
	public InboxMsgResponse searchAttachments(InboxMsg msg, Calendar startDate,
			Calendar endDate) throws GIException {

		LOGGER.info(CLASS_NAME + ".searchAttachments Begin ");
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();

		try {
			// Set the status to sent / replied / forward/archived..etc
			List<InboxMsg> messages = new ArrayList<InboxMsg>();
			inboxMsgResponse.startResponse();

			if (msg == null) {
				LOGGER.debug(NULL_MESSAGE_ERROR_STRNG);
				generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE, NULL_MESSAGE_ERROR_STRNG);
				return inboxMsgResponse;
			}

			// Query builder for creating dynamic queries
			@SuppressWarnings("unchecked")
			QueryBuilder<InboxMsg> inboxMsgQuery = delegateFactory.getObject();
			inboxMsgQuery.buildObjectQuery(InboxMsg.class);

			if (msg.getOwnerUserId() != 0) {

				// Step 1: Search in Alfresco for given user.
				LOGGER.info("Fetching messages for owner Id " + msg.getOwnerUserId());
				inboxMsgQuery.applyWhere("ownerUserId", (int) msg.getOwnerUserId(), DataType.NUMERIC, ComparisonType.EQ);

				// Step 2: Remove search results which messages are not
				// archived/deleted.
				inboxMsgQuery.applyWhere(STATUS_COLUMN, InboxMsg.STATUS.A, DataType.ENUM, ComparisonType.NE);
				inboxMsgQuery.applyWhere(STATUS_COLUMN, InboxMsg.STATUS.D, DataType.ENUM, ComparisonType.NE);

				/*
				 * Fetching messages as per data criteria. If no end date is provided,
				 * current system date is used as end date
				 */
				if (startDate != null) {
					Date endDateCriteria = (endDate == null) ? new TSDate() : endDate.getTime();
					LOGGER.info("Fetching messages between : " + startDate.getTime() + " and " + endDateCriteria);
					inboxMsgQuery.applyWhere(DATE_CREATED_COLUMN, startDate.getTime(), DataType.DATE, ComparisonType.GE);
					inboxMsgQuery.applyWhere(DATE_CREATED_COLUMN, endDateCriteria, DataType.DATE, ComparisonType.LE);
				}
				
				// Step 3: Search in Database for given file ids
				List<InboxMsgDoc> inboxMsgDocList = msg.getMessageDocs();
				LOGGER.debug("inboxMsgDocList is null: " + (inboxMsgDocList == null));

				if (null != inboxMsgDocList && !inboxMsgDocList.isEmpty()) {

					InboxMsgDoc inboxMsgDoc = inboxMsgDocList.get(0);
					LOGGER.debug("inboxMsgDoc is null: " + (inboxMsgDoc == null));
					LOGGER.debug("inboxMsgDocList.size(): " + inboxMsgDocList.size());
					
					inboxMsgQuery.applyWhere("messageDocs.docId", inboxMsgDoc.getDocId(), DataType.STRING, ComparisonType.EQUALS);
					inboxMsgQuery.setFetchDistinct(true);
					messages = inboxMsgQuery.getRecords(1, -1);
					LOGGER.info("Fetch size : " + messages.size());

					generateSuccessRepsonseForSearch(inboxMsgResponse, messages);
					inboxMsgResponse.endResponse();

				} 
				else {
					generateFailedResponse(inboxMsgResponse, INVALID_ATTACHMENT_DOCUMENT_ERROR_CODE, INVALID_ATTACHMENT_DOCUMENT_ERROR_STRING);
				}

			} 
			else {
				LOGGER.info("Invalid user Id " + msg.getOwnerUserId());
				generateFailedResponse(inboxMsgResponse, INVALID_USER_ERROR_CODE, INVALID_USER_ERROR_STRNG);
			}

		} finally {
			LOGGER.info(" " + CLASS_NAME + ".searchAttachments End ");
			inboxMsgResponse.endResponse();
		}
		return inboxMsgResponse;
	}

	@Override
	public InboxMsgResponse getMessageTree(InboxMsg msg, Calendar startDate,
			Calendar endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Method to get the Address Book for a user for a particular role. 
	 * User can communicate with only these individual/groups.
	 * 
	 * @author Nikhil Talreja
	 * @param userID
	 *            - User Id of the user
	 * @return object of InboxUserAddressBookResp which contains a list of Address Book objects that
	 *         the user can communicate to
	 */
	@Override
	public InboxUserAddressBookResp getUserAddressBook(long userID, String role) {
		
		LOGGER.info(CLASS_NAME + ".getUserAddressBook Begin");
		
		InboxUserAddressBookResp inboxUserAddressBookResp = new InboxUserAddressBookResp();
		inboxUserAddressBookResp.startResponse();
		
		/*
		 * Step 1: Validating the user
		 */
		
		//This is taken care of inside the controller
		/*AccountUser user = userService.findById((int)userID);
		if(user == null){
			LOGGER.error("Invalid user found with id " + userID);
			
			generateFailedResponseForAddressBook(inboxUserAddressBookResp, INVALID_USER_ERROR_CODE,INVALID_USER_ERROR_STRNG);
			inboxUserAddressBookResp.endResponse();
			return inboxUserAddressBookResp;
		}*/
		
		/*
		 * Step 2 : Fetching the User's Address book
		 */
		
		LOGGER.debug("Fetching address book for user witd id" + userID);
		/*List<AccountUser> contacts = null;
		if(role.equalsIgnoreCase(ModuleUserService.EMPLOYER_MODULE)){
			LOGGER.debug("User is an Employer");
			
			//Fetching employees of the employer
			contacts = iInboxMsgRepository.findEmployeeContactsForEmployer((int)userID, role);
			
			//Fetching the employer's broker
			AccountUser broker = iInboxMsgRepository.findBrokerForEmployer((int)userID, role);
			if(broker == null){
				LOGGER.info("Employer has no assiter");
			}
			else{
				LOGGER.info("Employer's assiter is:  " + broker.getFullName());
				contacts.add(broker);
			}
		}
		//FIXME - To add logic for fetching contacts for other roles
		else if (role.equalsIgnoreCase(ModuleUserService.EMPLOYEE_MODULE)){
			LOGGER.info("User is an Employee");
			
			//Fetching employer of the employee
			AccountUser employeer = iInboxMsgRepository.findEmployerContactForEmployee((int)userID);
			if(employeer == null){
				LOGGER.info("Employee has no employer");
			}
			else{
				LOGGER.info("Employee's employer is:  " + employeer.getFullName());
				contacts = new ArrayList<AccountUser>();
				contacts.add(employeer);
			}
		}*/
		//FIXME - For time being we are fetching all users from USERS table
		//Page<AccountUser> contacts = null;
		List<AccountUser> contacts = null;
		//contacts = iUserRepository.findAll(new PageRequest(0,MAX_CONTACTS_SIZE));/
		//contacts = iUserRepository.getUserList(new PageRequest(0,MAX_CONTACTS_SIZE));
		contacts = iUserRepository.getUserList();
		
		Map<Long, String> addresses = null;
		if(contacts != null){
			LOGGER.debug("Total contacts found : " + contacts.size());
			addresses = new HashMap<Long, String>();
			for(AccountUser contact : contacts){
				addresses.put(new Long(contact.getId()), contact.getFullName());
				//LOGGER.debug("Contact info : " + contact.getId() + ", " + contact.getFullName());
			}
		}
		else{
			LOGGER.debug("No contacts found for " + userID);
		}
		generateSuccessRepsonseForAddressBook(inboxUserAddressBookResp, addresses);
		inboxUserAddressBookResp.endResponse();
		
		return inboxUserAddressBookResp;
	}

	@Override
	public InboxUserAddressBookResp saveUserAddressBook(long userID,
			List<InboxMsg> messageList) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Method to get details of the selected message
	 * 
	 *  
	 * @author Geetha Chandran
	 * @param messageId - Message id for which message needs to be fetched
	 * @return String - String representation of clob data.
	 */
	@Override
	public String getMessageCLOB(long messageID) {
		LOGGER.debug("Retreiving of message clob begins");

		String inboxMessageClob = null;
		InboxMsgClob inboxMsgClob = iInboxMsgClobRepository.findById(messageID);
		
		if(null != inboxMsgClob){
			inboxMessageClob = inboxMsgClob.getMsgClob();
		}

		LOGGER.debug("Retreiving of message clob ends");
		return inboxMessageClob;

	}

	@Override
	public InboxUserAddressBookResp deleteUserAddressBook(long userID,
			List<InboxMsg> messageList) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * This method is used to validate the incoming msg Object
	 * @author polimetla_b, Nikhil Talreja
	 * @param msg - Message to be validated
	 * @return InboxMsgResponse - response Object containing state of the response
	 */
	private void validateMessage(InboxMsgResponse inboxMsgResponse, InboxMsg msg) {
		
		// Step 1: Check for null values
		// Checking message
		if (msg == null) {
			LOGGER.debug(NULL_MESSAGE_ERROR_STRNG);
			generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE,NULL_MESSAGE_ERROR_STRNG);
		}

		// Step 2: Check for incoming message status. Reject archived and deleted messages
		else if (msg.getStatus() == InboxMsg.STATUS.A || msg.getStatus() == InboxMsg.STATUS.D ){
			LOGGER.debug("Message status is invalid");
			generateFailedResponse(inboxMsgResponse,INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
		}
	}
	
	/**
	 * This method creates a response object for a failed operation
	 * @author Nikhil Talreja
	 * @param inboxMsgResponse - current response object
	 * @param errCode - Error code for the response
	 * @param errMsg - Error message for the response
	 */
	private void generateFailedResponse(InboxMsgResponse inboxMsgResponse, int errCode, String errMsg){
		
		LOGGER.debug("Operation Failed with error code : " + errCode + ". Reason : " + errMsg);
		inboxMsgResponse.setMessageList(null);
		inboxMsgResponse.setErrCode(errCode);
		inboxMsgResponse.setErrMsg(errMsg);
		inboxMsgResponse.setStatus(FAILURE_STATUS);
		
	}
	
	/**
	 * This method creates a response object for a successful operation
	 * @author Nikhil Talreja
	 * @param inboxMsgResponse - current response object
	 * @param msg - Message object to be returned
	 */
	private void generateSuccessRepsonse(InboxMsgResponse inboxMsgResponse, InboxMsg msg){
		
		LOGGER.debug("Generating success response");
		inboxMsgResponse.getMessageList().add(msg);
		inboxMsgResponse.setErrCode(SUCCESS_CODE);
		inboxMsgResponse.setErrMsg(SUCCESS_STRING);
		inboxMsgResponse.setStatus(SUCCESS_STATUS);
		
	}
	
	/**
	 * This method creates a response object for a failed operation for Address Book
	 * @author Nikhil Talreja
	 * @param inboxUserAddressBookResp - current response object
	 * @param errCode - Error code for the response
	 * @param errMsg - Error message for the response
	 */
	/*private void generateFailedResponseForAddressBook(InboxUserAddressBookResp inboxUserAddressBookResp, int errCode, String errMsg){
		
		LOGGER.debug("Operation Failed with error code : " + errCode + ". Reason : " + errMsg);
		inboxUserAddressBookResp.setAddressBook(null);
		inboxUserAddressBookResp.setErrCode(errCode);
		inboxUserAddressBookResp.setErrMsg(errMsg);
		inboxUserAddressBookResp.setStatus(FAILURE_STATUS);
		
	}*/
	
	/**
	 * This method creates a response object for a successful operation for Address Book 
	 * @author Nikhil Talreja
	 * @param inboxUserAddressBookResp - current response object
	 * @param addresses - List of addresses to be saved
	 */
	private void generateSuccessRepsonseForAddressBook(InboxUserAddressBookResp inboxUserAddressBookResp, Map<Long, String> addresses){
		
		LOGGER.debug("Generating success response for address book operation");
		inboxUserAddressBookResp.setAddressBook(addresses);
		inboxUserAddressBookResp.setErrCode(SUCCESS_CODE);
		inboxUserAddressBookResp.setErrMsg(SUCCESS_STRING);
		inboxUserAddressBookResp.setStatus(SUCCESS_STATUS);
	}
	
	/**
	 * This method creates a response object for a successful operation for Address Book 
	 * @author Nikhil Talreja
	 * @param inboxUserAddressBookResp - current response object
	 * @param addresses - List of addresses to be saved
	 */
	private void generateSuccessRepsonseForSearch(InboxMsgResponse inboxMsgResponse, List<InboxMsg> messageList){
		
		LOGGER.debug("Generating success response for search operation");
		inboxMsgResponse.setMessageList(messageList);
		inboxMsgResponse.setErrCode(SUCCESS_CODE);
		inboxMsgResponse.setErrMsg(SUCCESS_STRING);
		inboxMsgResponse.setStatus(SUCCESS_STATUS);
		
	}
	
	
	/**
	 * Method to find the full name of a recipient
	 * @param recipient - Whose full name needs to be found out
	 * @return String - The full name of a recipient
	 *//*
	private String getFullName(AccountUser recipient) {

	
		StringBuffer fullName = new StringBuffer();
		if (recipient.getFirstName() != null
				&& recipient.getFirstName().trim().length() > 0) {
			fullName.append(recipient.getFirstName().trim());
		}

		if (recipient.getLastName() != null
				&& recipient.getLastName().trim().length() > 0) {
			fullName.append(" ").append(recipient.getLastName().trim());
		}
		return fullName.toString();
	}*/
	
	/**
	 * Method to get details of the selected message
	 * 
	 *  
	 * @author Geetha Chandran
	 * @param messageId - Message id whose details needs to be fetched
	 * @return InboxMsg - object of InboxMsg which contains all details for a messageId.
	 */
	@Deprecated
	@Override
	public InboxMsg readMessage(long messageId) {

		LOGGER.info(CLASS_NAME + ".readMessage Begin");

		InboxMsg inboxMsg = iInboxMsgRepository.findById(messageId);

		if (null != inboxMsg) {
			// Checking and getting the Msg clob
			if ((null != inboxMsg.getIsClobUsed() && inboxMsg.getIsClobUsed().equals(InboxMsg.CLOB_USED.Y))) {

				String inboxMsgClob = getMessageCLOB(messageId);
				
				if (null != inboxMsgClob) {
					LOGGER.debug("Successfully retrieved clob for given message id");
					inboxMsg.setMsgBody(inboxMsgClob);
				} 
				else {
					LOGGER.debug("No clob found for given message id");
					// TODO Code to handle negative test case scenarios
				}

				LOGGER.debug("Retreiving of message clob begins");
			}
		} 
//		else {
			// TODO Code to handle negative test case scenarios
//		}

		// TODO Replacing comma from ToUserNameList with semi-colon - need to confirm

		return inboxMsg;
	}

	/**
	 * Method is used to get messages from InboxMsg base on the owner and status.
	 * @param ownerUserId, Owner User Id
	 * @param statusList, Status List
	 * @param pageable, Pageable object
	 * @return InboxMsg data in Page format
	 */
	@Override
	public Page<InboxMsg> findByOwnerUserIdAndStatusIn(long ownerUserId, List<InboxMsg.STATUS> statusList, Pageable pageable) throws GIException {
		
		LOGGER.info(CLASS_NAME + ".findByOwnerUserIdAndStatusIn Begin ");
		
		Page<InboxMsg> inboxMsgPageList = null;
		
		try {
			
			LOGGER.info("ownerUserId: " + ownerUserId);
			//LOGGER.info("statusList is null: " + (statusList == null));
			//LOGGER.info("iInboxMsgRepository is null: " + (iInboxMsgRepository == null));
			//LOGGER.info("pageable is null: " + (pageable == null));
			
			if(null != pageable && ownerUserId != 0 && null != statusList && !statusList.isEmpty()) {
				
				inboxMsgPageList = iInboxMsgRepository.findByOwnerUserIdAndStatusIn(ownerUserId, statusList, pageable);
				//LOGGER.info("inboxMsgPageList is null: " + (inboxMsgPageList == null));
				
				if(inboxMsgPageList != null) {
					LOGGER.info("Page Size: " + inboxMsgPageList.getSize());
					//LOGGER.info("Page Number: " + inboxMsgPageList.getNumber());
					//LOGGER.info("Page Number Of rows: " + inboxMsgPageList.getNumberOfElements());
					//LOGGER.info("Total Pages: " + inboxMsgPageList.getTotalPages());
					//LOGGER.info("Total Elements: " + inboxMsgPageList.getTotalElements());
				}
			}
			
		}
		finally {
			LOGGER.info(CLASS_NAME + ".findByOwnerUserIdAndStatusIn End ");
		}
		return inboxMsgPageList;
	}
	
	/**
	 * This method is used to get messages from InboxMsg based on the module Id and module Name and status.
	 */
	@Override
	public Page<InboxMsg> findByModuleIdAndModuleNameAndStatusIn(long moduleId, String moduleName, List<InboxMsg.STATUS> statusList, Pageable pageable) {
		
		LOGGER.info(CLASS_NAME + ".findByModuleIdAndModuleNameAndStatusIn Begin ");
		
		Page<InboxMsg> inboxMsgPageList = null;
		
		try {
			
			LOGGER.info("Module Id: " + moduleId);
			LOGGER.info("Module Name: " + moduleName);
			
			if(null != pageable && moduleId != 0 && null != statusList && !statusList.isEmpty()) {
				
				inboxMsgPageList = iInboxMsgRepository.findByModuleIdAndModuleNameIgnoreCaseAndStatusIn(moduleId, moduleName,statusList, pageable);
				
				if(inboxMsgPageList != null) {
					LOGGER.info("Total Messages: " + inboxMsgPageList.getSize());
				}
			}
			
		}
		finally {
			LOGGER.info(CLASS_NAME + ".findByModuleIdAndModuleNameAndStatusIn End ");
		}
		return inboxMsgPageList;
		
	}
	
	/**
	 * Method is used to get messages from InboxMsg base on search criteria with status is not a Delete.
	 * @param ownerUserId, Owner User Id
	 * @param searchText, search text
	 * @param pageable, Pageable object
	 * @return InboxMsg data in Page format
	 */
	@Override
	public Page<InboxMsg> getMessagesBySearchCriteria(long ownerUserId,long moduleId,String moduleName,String searchText, Pageable pageable) throws GIException {
		
		LOGGER.info(CLASS_NAME + ".getMessagesBySearchCriteria Begin ");
		
		Page<InboxMsg> inboxMsgPageList = null;
		
		try {
			
			//LOGGER.info("ownerUserId: " + ownerUserId);
			//LOGGER.debug("moduleId: " + moduleId);
			//LOGGER.debug("moduleName: " + moduleName);
			//LOGGER.info("searchText: " + searchText);
			//LOGGER.info("iInboxMsgRepository is null: " + (iInboxMsgRepository == null));
			//LOGGER.info("pageable is null: " + (pageable == null));
			
			if(null != pageable && StringUtils.isNotEmpty(searchText)) {
				
				final String LIKE1 = "%";
				final String LIKE2 = "_";
				final String BACK_SLASH = "\\\\";
				
				if(searchText.indexOf(LIKE1) > -1) {
					searchText = searchText.replaceAll(LIKE1, BACK_SLASH + LIKE1);
                }
				
				if(searchText.indexOf(LIKE2) > -1) {
					searchText = searchText.replaceAll(LIKE2, BACK_SLASH + LIKE2);
                }
				
				//If active module Id and name is set for user, use them to fetch messages
				if(moduleId != 0 && StringUtils.isNotBlank(moduleName)){
					inboxMsgPageList = iInboxMsgRepository.getMessagesBySearchCriteria(moduleId,moduleName,LIKE1 + searchText + LIKE1, pageable);
				}
				else{
					inboxMsgPageList = iInboxMsgRepository.getMessagesBySearchCriteria(ownerUserId, LIKE1 + searchText + LIKE1, pageable);
				}
				
				
				LOGGER.info("inboxMsgPageList is null: " + (inboxMsgPageList == null));
				
				if(inboxMsgPageList != null) {
					LOGGER.info("Page Size: " + inboxMsgPageList.getSize());
					//LOGGER.info("Page Number: " + inboxMsgPageList.getNumber());
					//LOGGER.info("Page Number Of rows: " + inboxMsgPageList.getNumberOfElements());
					//LOGGER.info("Total Pages: " + inboxMsgPageList.getTotalPages());
					//LOGGER.info("Total Elements: " + inboxMsgPageList.getTotalElements());
				}
			}
		}
		finally {
			LOGGER.info(CLASS_NAME + ".getMessagesBySearchCriteria End ");
		}
		return inboxMsgPageList;
	}
	
	/**
	 * Method to find a message by its id.
	 * @author Nikhil Talreja
	 * @param id, Primary key of the message
	 * @return InboxMsg object in
	 */
	@Override
	public InboxMsg findMessageById(long id){
		
		LOGGER.info(CLASS_NAME + ".findMessageById Begin ");
		
		InboxMsg inboxMsg = null;
		
		try {
			
			LOGGER.info("iInboxMsgRepository is null: " + (iInboxMsgRepository == null));
			
			if(null != iInboxMsgRepository && id != 0 ) {
				
				inboxMsg = iInboxMsgRepository.findById(id);
				LOGGER.info("inboxMsg is null: " + (inboxMsg == null));
					
			}
			
		}
		finally {
			
			LOGGER.info(CLASS_NAME + ".findMessageById End ");
		}
		return inboxMsg;
	}
	
	/**
	 * Method is used to modify inbox message with Archive, Read or Unread status.
	 * @param ownerUserId, owner user id in long
	 * @param inboxMsgIds, inbox messsage ids in String.
	 * @param inboxMsgStatus, inbox message status in enum.
	 * @return InboxMsgResponse Object containing state of operation
	 * @throws GIException
	 */
	@Override
	public InboxMsgResponse secureInboxAction(long ownerUserId, String inboxMsgIds, InboxMsg.STATUS inboxMsgStatus) throws GIException {
		
		LOGGER.info(CLASS_NAME + ".secureInboxAction Begin ");
		
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();
		List<Long> messageIdList = null;
		
		try {
			inboxMsgResponse.startResponse();
			//LOGGER.info("ownerUserId: " + ownerUserId);
			//LOGGER.info("inboxMsgIds: " + inboxMsgIds);
			//LOGGER.info("inboxMsgStatus: " + inboxMsgStatus);
			
			if (StringUtils.isEmpty(inboxMsgIds)) {
				generateFailedResponse(inboxMsgResponse, NULL_MESSAGE_ERROR_CODE, NULL_MESSAGE_ERROR_STRNG);
				return inboxMsgResponse;
			}

			if (null == inboxMsgStatus) {
				generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE, INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
				return inboxMsgResponse;
			}
			
			if(ownerUserId > 0) {
				
				messageIdList = new ArrayList<Long>();
				inboxMsgIds = inboxMsgIds.replaceAll(" ", "");
				
				if(inboxMsgIds.indexOf(',') > -1) {
					
					String msgIds[] = inboxMsgIds.split(",");
					
					for(String msgId : msgIds) {
						
						if(msgId.trim().length() > 0 && StringUtils.isNumeric(msgId) && Long.parseLong(msgId) > 0) {
							messageIdList.add(Long.parseLong(msgId));
						}
					}
					msgIds = null;
				}
				else if(inboxMsgIds.trim().length() > 0 && StringUtils.isNumeric(inboxMsgIds) && Long.parseLong(inboxMsgIds) > 0) {
					messageIdList.add(Long.parseLong(inboxMsgIds));
				}
				
				if(!messageIdList.isEmpty()) {
					int updateRecords = iInboxMsgRepository.modifyStatusInboxMsg(ownerUserId, inboxMsgStatus, messageIdList);
					LOGGER.info("updateRecords: " + updateRecords);
				}
			}
			else {
				generateFailedResponse(inboxMsgResponse, INVALID_USER_ERROR_CODE, INVALID_USER_ERROR_STRNG);
				return inboxMsgResponse;
			}
		}
		finally {
			
			if(null != messageIdList) {
				messageIdList.clear();
				messageIdList = null;
			}
			inboxMsgResponse.endResponse();
			LOGGER.info(CLASS_NAME + ".secureInboxAction End ");
		}
		return inboxMsgResponse;
	}
	
	/**
	 * Method to count the no. of messages created by a user on the current day
	 * This count cannot be more than 100
	 * @author Nikhil Talreja
	 * @param id, Primary key of the message
	 * @return InboxMsg object in
	 */
	@Override
	public Long countMessagesPerDayForUser(long ownerId){
		
		LOGGER.info(CLASS_NAME + ".countMessagesPerDayForUser Begin ");
		try{
			String currentDateStr = DateUtil.dateToString(new TSDate(), "MM/dd/yy");
			return iInboxMsgRepository.countMessagesPerDayForUser(ownerId, currentDateStr);
		}
		finally{
			LOGGER.info(CLASS_NAME + ".countMessagesPerDayForUser End ");
		}
	}
	
	/**
	 * Method is used to get count of Unread Messages for respected owner user.
	 * @param ownerUserId, owner user id in long form.
	 * @return Long, number of count
	 */
	@Override
	public Long countUnreadMessagesByOwner(long ownerUserId) {
		
		LOGGER.debug(CLASS_NAME + ".countUnreadMessagesByOwner Begin ");
		
		try {
			
			if(ownerUserId > 0) {
				return iInboxMsgRepository.countUnreadMessagesByOwner(ownerUserId);
			}
			else {
				return 0L;
			}
		}
		finally{
			LOGGER.debug(CLASS_NAME + ".countUnreadMessagesByOwner End ");
		}
	}
	
	/**
	 * Method is used to get count of Unread Messages for module
	 */
	@Override
	public Long countUnreadMessagesByModule(long moduleId, String moduleName) {
		
		LOGGER.debug(CLASS_NAME + ".countUnreadMessagesByModule Begin ");
		
		try {
			
			if(moduleId > 0) {
				return iInboxMsgRepository.countUnreadMessagesByModule(moduleId, moduleName);
			}
			else {
				return 0L;
			}
		}
		finally{
			LOGGER.debug(CLASS_NAME + ".countUnreadMessagesByModule End ");
		}
	}

	@Transactional
	@Override
	public InboxMsgResponse updateMessage(final InboxMsg msg) throws GIException, ContentManagementServiceException {
		LOGGER.info(CLASS_NAME + ".updateMessage Begin ");

		// Set the status to sent / replied / forward/archived..etc
		InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();
		InboxMsg savedMsg = null;
		try{
			inboxMsgResponse.startResponse();
	
			// Step 1: Validate incoming messages and check message body size.
			validateMessage(inboxMsgResponse,msg);
			
			if (inboxMsgResponse.getErrCode() != 0){
				inboxMsgResponse.endResponse();
				return inboxMsgResponse;
			}

			//Fetching current status of message from DB
			InboxMsg msgFromDB = iInboxMsgRepository.findById(msg.getId());
			InboxMsg.STATUS previousStatus = null;
			if(msgFromDB != null){
				LOGGER.info("Previous message id : " + msgFromDB.getId());
				previousStatus = msgFromDB.getStatus();
			}
			else{
				LOGGER.info("No message found with id : " + msg.getId());
			}
			LOGGER.info("Previous status of message is " + previousStatus);
			//LOGGER.info("New status of message is " + msg.getStatus());
			if(msg.getStatus() == InboxMsg.STATUS.C){ 
				if (previousStatus != null && previousStatus != InboxMsg.STATUS.C) {
					generateFailedResponse(inboxMsgResponse,INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
					return inboxMsgResponse;
				}
				savedMsg = iInboxMsgRepository.save(msg);
	
				generateSuccessRepsonse(inboxMsgResponse, savedMsg);
				return inboxMsgResponse;
			}

			/*
			 * Step 3: For P-Replied, F-Forwarded, S-Sent , Generate records to given target user IDs
			 * Previous status of the same message must be as follows:
			 * C - If current status is Sent
			 * S, F, P, R, N - If current status is Replied/Forwarded
			 * 
			 */
			if(msg.getStatus() == InboxMsg.STATUS.S && (previousStatus != InboxMsg.STATUS.S && previousStatus != null)){
					generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
					return inboxMsgResponse;
			}
			if(msg.getStatus() == InboxMsg.STATUS.P || msg.getStatus() == InboxMsg.STATUS.F){
				if((previousStatus == InboxMsg.STATUS.A || previousStatus == InboxMsg.STATUS.D)){
					generateFailedResponse(inboxMsgResponse, INVALID_MESSAGE_STATUS_ERROR_CODE,INVALID_MESSAGE_STATUS_ERROR_MESSAGE);
					return inboxMsgResponse;
				}
			}
			
			/*
			 * Step 4: For N-New Generate failed response
			 * This is taken care of in validate message
			 */
			/*
			 * Step 5: For N-New update the status
			 */
			if (msg.getStatus() == InboxMsg.STATUS.N) {

				// Updating message and returning
				msg.setStatus(InboxMsg.STATUS.R);
				savedMsg = iInboxMsgRepository.save(msg);

				generateSuccessRepsonse(inboxMsgResponse,savedMsg);
				return inboxMsgResponse;
			}
		}
		finally{
			if(savedMsg != null){
				LOGGER.info("Message saved with status ");
			}
			LOGGER.info(CLASS_NAME + ".saveMessage End ");
			inboxMsgResponse.endResponse();
			
		}
		return inboxMsgResponse;

		// FIXME - Send alerts in case of sent/replied/forward (To be implemented later)

	}
}
