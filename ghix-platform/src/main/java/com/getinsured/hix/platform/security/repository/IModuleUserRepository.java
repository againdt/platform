package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ModuleUser;

public interface IModuleUserRepository extends JpaRepository<ModuleUser, Integer> {
	
	@Query(" FROM ModuleUser moduleUser "+
			   " where moduleUser.user.id = :userId "+
			    " and moduleUser.moduleName = :moduleName ")
	 List<ModuleUser> findUserModules(@Param("userId")int userId,@Param("moduleName")String moduleName);
	
	@Query(" FROM ModuleUser moduleUser "+
			   " where moduleUser.user.id = :userId ")
	 List<ModuleUser> findUserModules(@Param("userId")int userId);
	

	@Query(" FROM ModuleUser moduleUser "+
			   " where moduleUser.moduleId = :moduleId "+
			    " and moduleUser.moduleName = :moduleName ")
	 List<ModuleUser> findModuleUsers(@Param("moduleId") int moduleId, @Param("moduleName") String moduleName);
	
	@Query(" FROM ModuleUser moduleUser "+
			   " where moduleUser.moduleId = :moduleId ")
	 List<ModuleUser> findModuleUsers(@Param("moduleId") int moduleId);

	
	@Query("SELECT mu FROM ModuleUser mu" +
			" WHERE mu.moduleId = :moduleId" +
			" AND   mu.moduleName = :moduleName" +
			" AND   mu.user.id = :userId")
	 ModuleUser findModuleUser(@Param("moduleId") int moduleId,@Param("moduleName") String moduleName,
			@Param("userId") int userId);
	
	ModuleUser findModuleUserByModuleNameAndUserId(String defModuleName, int userId);
	
	@Query("SELECT mu.moduleId FROM ModuleUser mu WHERE mu.moduleName = :moduleName")
	List<Integer> findModuleIdByModuleName(@Param("moduleName") String moduleName);	
	
	@Query("SELECT mu.moduleId FROM ModuleUser mu" +
			" WHERE upper(mu.moduleName) = :moduleName")
	 List<String> findModuleIdsByModuleName(@Param("moduleName") String moduleName);
	
	@Query("FROM ModuleUser mu WHERE mu.moduleName = :moduleName AND mu.moduleId IN (:moduleIds)")
	List<ModuleUser> getModuleUsersByModuleIdsAndModuleName(@Param("moduleIds") List<Integer> moduleIds, @Param("moduleName") String moduleName);

	//HIX-28851 Application error is displayed on switching from Agent to Employer
	@Query("FROM ModuleUser mu WHERE mu.moduleName = :moduleName " +
			"AND mu.user.id = :userId AND mu.isSelfSigned ='Y'")
	ModuleUser findUserSelfSignedModule(
			@Param("moduleName")String defModuleName,@Param("userId") int userId);

	ModuleUser findById(Integer id);

	// used in ghix-affiliate-svc AffiliateRestController
	@Query("FROM ModuleUser mu WHERE mu.moduleName = :moduleName AND mu.user.id IN(:userIds)")
	List<ModuleUser> getModuleUsersByUserIdsAndModuleName(@Param("userIds") List<Integer> userIds, @Param("moduleName") String moduleName);
	
	
	@Query("FROM ModuleUser mu WHERE mu.moduleName = :moduleName " +
			"AND mu.user.id = :userId AND mu.isSelfSigned ='Y'")
	List<ModuleUser> findUserSelfSignedModules(
			@Param("moduleName")String defModuleName,@Param("userId") int userId);
}
