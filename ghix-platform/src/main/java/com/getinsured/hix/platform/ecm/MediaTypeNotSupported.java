package com.getinsured.hix.platform.ecm;

public class MediaTypeNotSupported extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MediaTypeNotSupported() {
		// TODO Auto-generated constructor stub
	}

	public MediaTypeNotSupported(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MediaTypeNotSupported(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MediaTypeNotSupported(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MediaTypeNotSupported(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Throwable fillInStackTrace() {
		return null;
	}

}
