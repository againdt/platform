package com.getinsured.hix.platform.util;

import org.owasp.esapi.ESAPI;

/**
 * HIX-20384 ESAPI Implementation To prevent XSS Attack
 * @author Kenil Shah
 *
 */
public final class SecurityUtil {
	
	 private SecurityUtil(){
		 
	 }
	/**
	 * Encode data for insertion inside a data value or function argument in JavaScript
	 * @param input
	 * @return
	 */
	public static String encodeForJavaScript(String input) {
	    return ESAPI.encoder().encodeForJavaScript( input);
	}
	
	/**
	 * Encode data for use in Cascading Style Sheets (CSS) content.
	 * @param input
	 * @return
	 */
	public static String encodeForCSS(String input) {
	    return  ESAPI.encoder().encodeForCSS( input );
	}
	
	/**
	 * Encode data for use in HTML using HTML entity encoding 
	 * @param input
	 * @return
	 */
	
	public static String encodeForHTML(String input) {
	    return ESAPI.encoder().encodeForHTML( input );    
	 }

	/**
	 * Sanitize user inputs before logging them to prevent log forging vulnerability
	 * @param input
	 * @return
	 */
	public static String sanitizeForLogging(String input) {
		String sanitizedInput = null;
		if(null != input) {
			// ensure no CRLF injection into logs for forging records
			sanitizedInput = input.replace( '\n', '_' ).replace( '\r', '_' );
		    sanitizedInput = encodeForHTML(sanitizedInput);
		    if (!input.equals(sanitizedInput)) {
		        sanitizedInput += " (Encoded)";
		    }
		}
	    return sanitizedInput;  
	 }
}
