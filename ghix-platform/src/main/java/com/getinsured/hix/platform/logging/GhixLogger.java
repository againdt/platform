package com.getinsured.hix.platform.logging;





/**
 * This class is for wrapper logger for ghix application. Whenever there will be need
 * of changing logging we can modify this interface or implement class as per need. 
 * 
 * @author Biswakalyan(BK)
 * 
 */
public interface GhixLogger {



	/**
	 * Log a message at the TRACE level.
	 * 
	 * @param message
	 *            the message string to be logged
	 */
	public void trace(String message);

	/**
	 * Log a message at the TRACE level according to the specified message and
	 * argument.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the TRACE level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg
	 *            the argument
	 */
	public void trace(String message, Object arg);

	/**
	 * Log a message at the TRACE level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the TRACE level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 */
	public void trace(String message, Object arg1, Object arg2);

	/**
	 * Log a message at the TRACE level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous string concatenation when the logger is
	 * disabled for the TRACE level. However, this variant incurs the hidden
	 * (and relatively small) cost of creating an <code>Object[]</code> before
	 * invoking the method, even if this logger is disabled for TRACE. The
	 * variants taking {@link #trace(String, Object) one} and
	 * {@link #trace(String, Object, Object) two} arguments exist solely in
	 * order to avoid this hidden cost.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arguments
	 *            a list of 3 or more arguments
	 */
	public void trace(String message, Object... arguments);

	/**
	 * Log an exception (throwable) at the TRACE level with an accompanying
	 * message.
	 * 
	 * @param message
	 *            the message accompanying the exception
	 * @param t
	 *            the exception (throwable) to log
	 */
	public void trace(String message, Throwable t);


	/**
	 * Log a message at the DEBUG level.
	 * 
	 * @param message
	 *            the message string to be logged
	 */
	public void debug(String message);

	/**
	 * Log a message at the DEBUG level according to the specified message and
	 * argument.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the DEBUG level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg
	 *            the argument
	 */
	public void debug(String message, Object arg);

	/**
	 * Log a message at the DEBUG level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the DEBUG level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 */
	public void debug(String message, Object arg1, Object arg2);

	/**
	 * Log a message at the DEBUG level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous string concatenation when the logger is
	 * disabled for the DEBUG level. However, this variant incurs the hidden
	 * (and relatively small) cost of creating an <code>Object[]</code> before
	 * invoking the method, even if this logger is disabled for DEBUG. The
	 * variants taking {@link #debug(String, Object) one} and
	 * {@link #debug(String, Object, Object) two} arguments exist solely in
	 * order to avoid this hidden cost.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arguments
	 *            a list of 3 or more arguments
	 */
	public void debug(String message, Object... arguments);

	/**
	 * Log an exception (throwable) at the DEBUG level with an accompanying
	 * message.
	 * 
	 * @param message
	 *            the message accompanying the exception
	 * @param t
	 *            the exception (throwable) to log
	 */
	public void debug(String message, Throwable t);


	/**
	 * Log a message at the INFO level.
	 * 
	 * @param message
	 *            the message string to be logged
	 */
	public void info(String message);

	/**
	 * Log a message at the INFO level according to the specified message and
	 * argument.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the INFO level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg
	 *            the argument
	 */
	public void info(String message, Object arg);

	/**
	 * Log a message at the INFO level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the INFO level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 */
	public void info(String message, Object arg1, Object arg2);

	/**
	 * Log a message at the INFO level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous string concatenation when the logger is
	 * disabled for the INFO level. However, this variant incurs the hidden (and
	 * relatively small) cost of creating an <code>Object[]</code> before
	 * invoking the method, even if this logger is disabled for INFO. The
	 * variants taking {@link #info(String, Object) one} and
	 * {@link #info(String, Object, Object) two} arguments exist solely in order
	 * to avoid this hidden cost.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arguments
	 *            a list of 3 or more arguments
	 */
	public void info(String message, Object... arguments);

	/**
	 * Log an exception (throwable) at the INFO level with an accompanying
	 * message.
	 * 
	 * @param message
	 *            the message accompanying the exception
	 * @param t
	 *            the exception (throwable) to log
	 */
	public void info(String message, Throwable t);
	
	
	
	/**
     * Logs a message object with the {@link Level#INFO INFO} level.
     *
     * @param message the message object to log.
     */
    void info(Object message);

    /**
     * Logs a message at the {@link Level#INFO INFO} level including the stack trace of the {@link Throwable}
     * <code>t</code> passed as parameter.
     *
     * @param message the message object to log.
     * @param t the exception to log, including its stack trace.
     */
    void info(Object message, Throwable t);
	

	/**
	 * Log a message at the WARN level.
	 * 
	 * @param message
	 *            the message string to be logged
	 */
	public void warn(String message);

	/**
	 * Log a message at the WARN level according to the specified message and
	 * argument.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the WARN level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg
	 *            the argument
	 */
	public void warn(String message, Object arg);

	/**
	 * Log a message at the WARN level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous string concatenation when the logger is
	 * disabled for the WARN level. However, this variant incurs the hidden (and
	 * relatively small) cost of creating an <code>Object[]</code> before
	 * invoking the method, even if this logger is disabled for WARN. The
	 * variants taking {@link #warn(String, Object) one} and
	 * {@link #warn(String, Object, Object) two} arguments exist solely in order
	 * to avoid this hidden cost.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arguments
	 *            a list of 3 or more arguments
	 */
	public void warn(String message, Object... arguments);

	/**
	 * Log a message at the WARN level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the WARN level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 */
	public void warn(String message, Object arg1, Object arg2);

	/**
	 * Log an exception (throwable) at the WARN level with an accompanying
	 * message.
	 * 
	 * @param message
	 *            the message accompanying the exception
	 * @param t
	 *            the exception (throwable) to log
	 */
	public void warn(String message, Throwable t);

	/**
	 * Is the logger instance enabled for the ERROR level?
	 * 
	 * @return True if this Logger is enabled for the ERROR level, false
	 *         otherwise.
	 */
	public void error(String message);

	/**
	 * Log a message at the ERROR level according to the specified message and
	 * argument.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the ERROR level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg
	 *            the argument
	 */
	public void error(String message, Object arg);

	/**
	 * Log a message at the ERROR level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous object creation when the logger is disabled
	 * for the ERROR level.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 */
	public void error(String message, Object arg1, Object arg2);

	/**
	 * Log a message at the ERROR level according to the specified message and
	 * arguments.
	 * <p/>
	 * <p>
	 * This form avoids superfluous string concatenation when the logger is
	 * disabled for the ERROR level. However, this variant incurs the hidden
	 * (and relatively small) cost of creating an <code>Object[]</code> before
	 * invoking the method, even if this logger is disabled for ERROR. The
	 * variants taking {@link #error(String, Object) one} and
	 * {@link #error(String, Object, Object) two} arguments exist solely in
	 * order to avoid this hidden cost.
	 * </p>
	 * 
	 * @param message
	 *            the message string
	 * @param arguments
	 *            a list of 3 or more arguments
	 */
	public void error(String message, Object... arguments);

	/**
	 * Log an exception (throwable) at the ERROR level with an accompanying
	 * message.
	 * 
	 * @param message
	 *            the message accompanying the exception
	 * @param t
	 *            the exception (throwable) to log
	 */
	public void error(String message, Throwable t);
	
	
	/**
     * Checks whether this Logger is enabled for the {@link Level#DEBUG DEBUG} Level.
     *
     * @return boolean - {@code true} if this Logger is enabled for level DEBUG, {@code false} otherwise.
     */
    boolean isDebugEnabled();
    
    
    /**
     * Checks whether this Logger is enabled for the {@link Level#INFO INFO} Level.
     *
     * @return boolean - {@code true} if this Logger is enabled for level INFO, {@code false} otherwise.
     */
    boolean isInfoEnabled();
    
    /**
     * Logs a message object with the {@link Level#FATAL FATAL} level.
     *
     * @param message the message object to log.
     */
    void fatal(String message);
	
	

}
