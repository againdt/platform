package com.getinsured.hix.platform.couchbase.helper;

public class DocumentHelperException extends RuntimeException {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1171066863506131268L;

	public DocumentHelperException() {
		super();
	}

	public DocumentHelperException(String message, Throwable cause) {
		super(message, cause);
	}

	public DocumentHelperException(String message) {
		super(message);
	}

	public DocumentHelperException(Throwable cause) {
		super(cause);
	}

}
