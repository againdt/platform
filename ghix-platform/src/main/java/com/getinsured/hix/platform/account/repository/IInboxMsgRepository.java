package com.getinsured.hix.platform.account.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.InboxMsg;

/**
 * @author talreja_n
 * @since 22 November, 2012
 */
public interface IInboxMsgRepository extends JpaRepository<InboxMsg, Long> {

	List<InboxMsg> findByDateCreatedBetween(Date startDate, Date endDate);
	List<InboxMsg> findByOwnerUserId(long ownerUserId);
	List<InboxMsg> findByOwnerUserIdAndStatusIn(long ownerUserId, List<InboxMsg.STATUS> statusList);
	Page<InboxMsg> findByOwnerUserIdAndStatusIn(long ownerUserId, List<InboxMsg.STATUS> statusList, Pageable pageable);
	Page<InboxMsg> findByModuleIdAndModuleNameIgnoreCaseAndStatusIn(long moduleId, String moduleName, List<InboxMsg.STATUS> statusList, Pageable pageable);
	List<InboxMsg> findByStatus(InboxMsg.STATUS status);
	List<InboxMsg> findByPriority(InboxMsg.PRIORITY priority);
	List<InboxMsg> findByFromUserId(long fromUserId);
	
	InboxMsg findById(long id);

	/*
	 * Methods to get address book for an employer
	 * 1. All employees : input parameters - user Id of the employer and role="employers"
	 * 2. Broker : input parameters - user Id of the employer and role="employers"
	 */
	//	@Query("SELECT U FROM Employee E, ModuleUser MU, AccountUser U WHERE E.employer.id = MU.moduleId AND U.id = E.user.id " +
	//			" AND MU.user.id = :userId AND MU.moduleName = :role Order By U.firstName ASC")
	//	List<AccountUser> findEmployeeContactsForEmployer(@Param("userId") int userId, @Param("role") String role);
	//
	//	@Query("SELECT U FROM DesignateBroker DA, ModuleUser MU, Broker A, AccountUser U WHERE DA.employerId = MU.moduleId AND DA.brokerId = A.id " +
	//			"AND A.user.id = U.id AND MU.user.id = :userId AND MU.moduleName = :role ")
	//	AccountUser findBrokerForEmployer(@Param("userId") int userId, @Param("role") String role);

	/*
	 * Methods to get address book for an employee
	 * Fetches the User record for the employee's employer
	 */
	//	@Query("SELECT U FROM Employee E, AccountUser U, Employer ER, ModuleUser MU WHERE E.user.id = :userId " +
	//			" AND E.employer.id = ER.id AND MU.moduleId = ER.id AND U.id = MU.user.id")
	//	AccountUser findEmployerContactForEmployee(@Param("userId") int userId);

	/**
	 * Method is used to modify inbox message with Archive, Read or Unread status.
	 * @param ownerUserId, owner user id in long form.
	 * @param status, Inbox message Status in enum form.
	 * @param messageIdList, inbox messsage ids in List form.
	 * @return
	 */
	@Modifying
	@Transactional
	@Query("UPDATE InboxMsg SET status = :status WHERE ownerUserId = :ownerUserId AND id IN (:messageIdList)")
	int modifyStatusInboxMsg(@Param("ownerUserId") long ownerUserId, @Param("status") InboxMsg.STATUS status, @Param("messageIdList") List<Long> messageIdList);

	/*
	 * Method to count the no. of messages created by a user on the current day
	 * This count cannot be more than 100
	 */
	@Query("SELECT COUNT(M) FROM InboxMsg M WHERE M.ownerUserId = :ownerId "+
			" AND M.status IN ('S','C','F','P','A') AND TO_CHAR(M.dateCreated, 'MM/DD/YY') = :currentDate")
	Long countMessagesPerDayForUser(@Param("ownerId") long ownerId, @Param("currentDate") String currentDateStr);

	/**
	 * Method is used to get count of Unread Messages for respected owner user.
	 * @param ownerUserId, owner user id in long form.
	 * @return Long, number of count
	 */
	@Query("SELECT COUNT(inboxMsg) FROM InboxMsg inboxMsg WHERE inboxMsg.ownerUserId = :ownerUserId AND inboxMsg.status = 'N'")
	Long countUnreadMessagesByOwner(@Param("ownerUserId") long ownerUserId);

	/**
	 * Method is used to get count of Unread Messages for module.
	 */
	@Query("SELECT COUNT(inboxMsg) FROM InboxMsg inboxMsg WHERE inboxMsg.moduleId = :moduleId AND  lower(inboxMsg.moduleName) = lower(:moduleName) AND inboxMsg.status = 'N'")
	Long countUnreadMessagesByModule(@Param("moduleId") long moduleId, @Param("moduleName") String moduleName);

	/**
	 * Method is used to get messages from InboxMsg base on search criteria with status is not a Delete.
	 * @param ownerUserId, Owner User Id
	 * @param searchText, search text
	 * @param pageable, Pageable object
	 * @return InboxMsg data in Page format
	 */
	@Query("select inbox from InboxMsg as inbox where inbox.ownerUserId = :ownerUserId and inbox.status <> 'D' and " +
			"(lower(inbox.fromUserName) like lower(:searchText) ESCAPE '\\' or lower(inbox.toUserNameList) like lower(:searchText) ESCAPE '\\' or " +
			"lower(inbox.msgSub) like lower(:searchText) ESCAPE '\\')")
	Page<InboxMsg> getMessagesBySearchCriteria(@Param("ownerUserId") long ownerUserId, @Param("searchText") String searchText, Pageable pageable);

	/**
	 * Method to search messages based on module Id and Name
	 * @author Nikhil Talreja
	 * @since 16 September, 2013
	 */
	@Query("select inbox from InboxMsg as inbox where inbox.moduleId = :moduleId and lower(inbox.moduleName) = lower(:moduleName) AND inbox.status <> 'D' and " +
			"(lower(inbox.fromUserName) like lower(:searchText) ESCAPE '\\' or lower(inbox.toUserNameList) like lower(:searchText) ESCAPE '\\' or " +
			"lower(inbox.msgSub) like lower(:searchText) ESCAPE '\\')")
	Page<InboxMsg> getMessagesBySearchCriteria(@Param("moduleId") long moduleId, @Param("moduleName") String moduleName, @Param("searchText") String searchText, Pageable pageable);
	
}
