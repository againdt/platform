package com.getinsured.hix.platform.couchbase.dto;

import com.google.gson.annotations.Expose;

public class CouchBinaryDocument {
	@Expose
	private byte[] data;

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
}
