package com.getinsured.hix.platform.config;

import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.couchbase.client.core.event.consumers.LoggingConsumer;
import com.couchbase.client.core.logging.CouchbaseLogLevel;
import com.couchbase.client.core.metrics.MetricsCollectorConfig;
import com.couchbase.client.deps.io.netty.util.ResourceLeakDetector;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;

/**
 * Defines couchbase environment to be used for all CB connections.
 *
 * Created by golubenko_y on 1/25/17.
 */
@Component
public class CBEnvironment
{
  private static final Logger log = LoggerFactory.getLogger(CBEnvironment.class);

  private static CouchbaseEnvironment environment;

  private static final int SOCKET_CONNECTION_TIMEOUT = 1000 * 60;
  private static final int CONNECTION_TIMEOUT = 100_000;
  private static final int MEMCACHED_KEYVALUE_TIMEOUT = 30_000;

  static {
    // -Dcouchbase.detectleaks=true to enable ADVANCED leak reporting.
    if("true".equals(System.getProperty("couchbase.detectleaks")))
    {
      ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID);
    }
    else {
      ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);
    }
  }

  public CouchbaseEnvironment environment()
  {
    if (environment != null)
    {
      return environment;
    }

    MetricsCollectorConfig metricsCollectorConfig = new MetricsCollectorConfig()
    {
      @Override
      public long emitFrequency()
      {
        return 1;
      }

      @Override
      public TimeUnit emitFrequencyUnit()
      {
        return TimeUnit.HOURS;
      }
    };

    environment =
        DefaultCouchbaseEnvironment.builder()
        .connectTimeout(CONNECTION_TIMEOUT)
          .socketConnectTimeout(SOCKET_CONNECTION_TIMEOUT)
            .kvTimeout(MEMCACHED_KEYVALUE_TIMEOUT)
              .tcpNodelayEnabled(true)
                .bufferPoolingEnabled(true) // Override with -Dcom.couchbase.bufferPoolingEnabled=true|false startup parameter in /etc/init.d/web|svc
                  .defaultMetricsLoggingConsumer(false, CouchbaseLogLevel.ERROR, LoggingConsumer.OutputFormat.JSON)
                    .runtimeMetricsCollectorConfig(metricsCollectorConfig)
            .build();

    if(log.isInfoEnabled())
    {
      log.info("CBEnvironment: {}", environment.toString());
    }

    return environment;
  }

  @PreDestroy
  public void cleanup()
  {
    if(environment != null)
    {
      try
      {
        Boolean shutdown = environment.shutdownAsync().toBlocking().single();

        if(log.isInfoEnabled())
        {
          log.info("shutdownAsync().toBlocking().single() on CacheConfiguration environment ok: " + shutdown);
        }
      }
      catch(Exception e)
      {
        if(log.isErrorEnabled())
        {
          log.error("shutdownAsync().toBlocking().single() on CacheConfiguration environment: " +  e.getMessage(), e);
        }
      }
    }
  }
}
