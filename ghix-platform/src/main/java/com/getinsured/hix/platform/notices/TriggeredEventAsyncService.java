package com.getinsured.hix.platform.notices;

import com.getinsured.hix.model.TriggeredEvent;

public interface TriggeredEventAsyncService {
	
	public TriggeredEvent save(TriggeredEvent triggeredEvent);
	
}
