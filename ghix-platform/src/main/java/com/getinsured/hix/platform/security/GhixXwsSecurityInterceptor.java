package com.getinsured.hix.platform.security;

import java.util.Locale;

import javax.security.auth.callback.CallbackHandler;

import org.springframework.core.io.Resource;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapBody;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.security.WsSecurityValidationException;
import org.springframework.ws.soap.security.xwss.XwsSecurityInterceptor;
import org.springframework.ws.soap.security.xwss.callback.XwssCallbackHandlerChain;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;



public class GhixXwsSecurityInterceptor extends XwsSecurityInterceptor {
	
	
	private CallbackHandler callbackHandler;

	private Resource policyConfiguration;

	/**
	 * Sets the handler to resolve XWSS callbacks. Setting either this propery, or {@code callbackHandlers}, is
	 * required.
	 *
	 * @see com.sun.xml.wss.impl.callback.XWSSCallback
	 * @see #setCallbackHandlers(javax.security.auth.callback.CallbackHandler[])
	 */
	public void setCallbackHandler(CallbackHandler callbackHandler) {
		this.callbackHandler = callbackHandler;
		super.setCallbackHandler(callbackHandler);
	}

	/**
	 * Sets the handlers to resolve XWSS callbacks. Setting either this propery, or {@code callbackHandlers}, is
	 * required.
	 *
	 * @see com.sun.xml.wss.impl.callback.XWSSCallback
	 * @see #setCallbackHandler(javax.security.auth.callback.CallbackHandler)
	 */
	public void setCallbackHandlers(CallbackHandler[] callbackHandler) {
		this.callbackHandler = new XwssCallbackHandlerChain(callbackHandler);
		super.setCallbackHandlers(callbackHandler);
	}
	
	public void setPolicyConfiguration(Resource policyConfiguration) {
		this.policyConfiguration = policyConfiguration;
		super.setPolicyConfiguration(this.policyConfiguration);
	}
	@Override
	protected boolean handleValidationException(WsSecurityValidationException ex, MessageContext messageContext) {
		
		if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
				IEXConfiguration.IEXConfigurationEnum.IEX_AT_TIME_TRAVEL))) {
			logger.info("iex.AT.timeTravel is set to true, Skipping all the Validations in XwsSecurityInterceptor  ");
			return true;
		}
		
		else {
		if (logger.isWarnEnabled()) {
			logger.warn("Could not validate request: " + ex.getMessage());
		}
		
			if (logger.isDebugEnabled()) {
				logger.debug("No exception resolver present, creating basic soap fault");
			}
			SoapBody response = ((SoapMessage) messageContext.getResponse()).getSoapBody();
			response.addClientOrSenderFault(ex.getMessage(), Locale.ENGLISH);
		}
		return false;
	}
}
