package com.getinsured.hix.platform.service.jpa;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.atlassian.httpclient.api.HttpStatus;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.model.String2ConfigurationDTO;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.Tenant.IS_ACTIVE;
import com.getinsured.hix.model.Tenant2TenantDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.external.cloud.service.ExternalCloudService;
import com.getinsured.hix.platform.repository.AffiliateFlowRepository;
import com.getinsured.hix.platform.repository.AffiliateFlowTenantUnawareRepository;
import com.getinsured.hix.platform.repository.AffiliateRepository;
import com.getinsured.hix.platform.repository.AffiliateTenantUnawareRepository;
import com.getinsured.hix.platform.repository.GIAppConfigRepository;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.security.service.TenantPermissionService;
import com.getinsured.hix.platform.security.service.TenantRoleService;
import com.getinsured.hix.platform.service.AffiliateRedisService;
import com.getinsured.hix.platform.service.GIAppConfigService;
import com.getinsured.hix.platform.service.TenantRedisService;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecureHttpClient;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;

@Service(value="tenantService")
public class TenantServiceImpl implements TenantService {

	private static final String GINS = "GINS";

	private static final String ERROR_TENANT_SAME_URL = "Error: Another tenant with same url found";
	private static final String ERROR_ADD_TENANT_WSO2 = "Error: Could not add tenant on WSO2 server";

	@Autowired private TenantRepository tenantRepository;

	@Autowired private AffiliateRepository affiliateRepository;

	@Autowired private AffiliateFlowRepository affiliateFlowRepository;

	@Autowired private AffiliateFlowTenantUnawareRepository affiliateFlowTenantUnawareRepository;
	@Autowired private AffiliateTenantUnawareRepository affiliateTenantUnawareRepository;

	@Autowired private TenantPermissionService tenantPermissionService;

	@Autowired private TenantRoleService tenantRoleService;

	@Autowired private GIAppConfigService giAppConfigService;

	@Autowired private ExternalCloudService externalCloudServiceImpl;

	@Autowired private AffiliateRedisService affiliateRedisService;

	@Autowired private TenantRedisService tenantRedisService;

	@Autowired private GIAppConfigRepository gIAppConfigRepository;

	private HttpClient restHttpClient;
	
	private static final Logger LOGGER = Logger.getLogger(TenantServiceImpl.class);

	private static final boolean isRedisEnabled = "on".equalsIgnoreCase(System.getProperty("redis.enabled"));

	private static final String SEPARATOR = "/";
	private static final String LOGO_URL = "logoUrl";
	private static final String REDIRECT_URL = "redirectUrl";
	private static final String FOOTER_ADDRESS = "footerAddress";
	private static final String CUSTOMER_CARE_NUM = "customerCareNum";
	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	private static final String GLOBAL_EXCHANGE_PHONE = "global.ExchangePhone";

	private TenantDTO ghixTenant;

	private static final List<String> HEADER_FOOTER_TOKENS = Arrays.asList(
			LOGO_URL, REDIRECT_URL, FOOTER_ADDRESS, CUSTOMER_CARE_NUM,
			DISCLAIMER_CONTENT);

	public Page<Tenant> getAllTenant(Pageable pageable) {
		return tenantRepository.findAll(pageable);
	}

	public TenantDTO getTenant(String code) {
		return Tenant2TenantDTO.Current.convert(tenantRepository.findByCode(code));
	}

	public TenantDTO getTenantForUrl(String url) {
		TenantDTO tenantDTO = null;
		if(isRedisEnabled) {
			tenantDTO = fetchTenantFromRedis(url);
			if(tenantDTO == null) {
				tenantDTO = fetchTenantFromDB(url);
			}
		} else {
			tenantDTO = fetchTenantFromDB(url);
		}
		return tenantDTO;
	}

	private TenantDTO fetchTenantFromRedis(String url) {
		return tenantRedisService.get(url);
	}

	private TenantDTO fetchTenantFromDB(String url) {
		TenantDTO tenantDTO = null;
		Tenant tenant = tenantRepository.findByUrl(url);
		if(tenant != null) {
			tenantDTO = Tenant2TenantDTO.Current.convert(tenant);
			if(isRedisEnabled) {
				tenantRedisService.set(url, tenantDTO);
			}
		}
		return tenantDTO;
	}

	public Affiliate getAffiliateForUrl(String url) {
		Affiliate affiliate = null;
		if(isRedisEnabled) {
			affiliate = fetchAffiliateFromRedis(url);
			if(affiliate == null) {
				affiliate = fetchAffiliateFromDB(url);
			}
		} else {
			affiliate = fetchAffiliateFromDB(url);
		}
		return affiliate;
	}

	/**
	 * Returns affiliate record for given affiliate id.
	 *
	 * @param affiliateId affiliate id.
	 * @return {@link Affiliate} object or null.
	 */
	@Override
	public Affiliate getAffiliateById(Long affiliateId)
	{
		return affiliateTenantUnawareRepository.findById(affiliateId);
	}

	private Affiliate fetchAffiliateFromDB(String url) {
		Affiliate affiliate = affiliateRepository.findByUrl(url);
		if(affiliate != null) {
			if(isRedisEnabled) {
				affiliateRedisService.set(url, affiliate);
			}
		}
		return affiliate;
	}

	private Affiliate fetchAffiliateFromRedis(String url) {
		return affiliateRedisService.get(url);
	}

	@Override
	public TenantDTO getTenant(Long tenantId) {
		TenantDTO retVal = null;
		Tenant tenant = tenantRepository.findById(tenantId);
		if(tenant != null) {
			retVal = Tenant2TenantDTO.Current.convert(tenant);
		}
		return retVal;
	}

	@SuppressWarnings("unchecked")
	private String createUpdateTenantRequest(TenantDTO tenantDTO) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		try {
			reqJsonObject = new JSONObject();
			reqJsonObject.put("adminUser", GhixPlatformConstants.SCIM_ADMIN_USER);
			reqJsonObject.put("adminPassword", GhixPlatformConstants.SCIM_ADMIN_PASS);
			
			payloadJsonObject = new JSONObject();
			
			payloadJsonObject.put("adminName", tenantDTO.getTenantProvisionigUser() ); 
			payloadJsonObject.put("adminEmail", tenantDTO.getTenantProvisionigUserPassword()); 
			payloadJsonObject.put("adminPassword", GhixPlatformConstants.SCIM_ADMIN_PASS); 
			payloadJsonObject.put("tenantDomain", tenantDTO.getTenantDomain());
			payloadJsonObject.put("firstName",tenantDTO.getConfiguration().getFirstName());
			payloadJsonObject.put("lastName", tenantDTO.getConfiguration().getLastName());
			
			reqJsonObject.put("payload", payloadJsonObject);
			if(null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING  add/update TenantInWSO2 JSON REQ: ",e);
		}
		return request;
	}
	
	@SuppressWarnings("unchecked")
	private String createAddTenantRequest(TenantDTO tenantDTO) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		try {
			reqJsonObject = new JSONObject();
			String tenantAdminUser = tenantDTO.getCode().toLowerCase() + "_provisioning@getinsured.com";
			reqJsonObject.put("adminUser", GhixPlatformConstants.SCIM_ADMIN_USER);
			reqJsonObject.put("adminPassword", GhixPlatformConstants.SCIM_ADMIN_PASS);
			
			payloadJsonObject = new JSONObject();
			
			payloadJsonObject.put("adminName", tenantAdminUser ); 
			payloadJsonObject.put("adminEmail", tenantAdminUser); 
			payloadJsonObject.put("adminPassword", GhixPlatformConstants.SCIM_ADMIN_PASS); 
			payloadJsonObject.put("tenantDomain", tenantDTO.getTenantDomain());
			payloadJsonObject.put("firstName",tenantDTO.getConfiguration().getFirstName());
			payloadJsonObject.put("lastName", tenantDTO.getConfiguration().getLastName());
			
			tenantDTO.setTenantProvisionigUser(tenantAdminUser);
			tenantDTO.setTenantProvisionigUserPassword(GhixPlatformConstants.SCIM_ADMIN_PASS);
		
			reqJsonObject.put("payload", payloadJsonObject);
			if(null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING  add/update TenantInWSO2 JSON REQ: ",e);
		}
		return request;
	}
	
	private boolean addTenantOnWSO2(TenantDTO tenantDTO) throws GIException{
		boolean success = false;
		JSONParser parser = new JSONParser();
		String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"tenant/add";
		if(LOGGER.isDebugEnabled()){
			if(uri.indexOf("gi-identity") == -1){
				LOGGER.warn("Tenant add endpoint generally points to \"gi-identity\" component and URL does not seems to be right, please check");
			}
			LOGGER.debug("Using WSO2 Environment 5.3 for URI:"+uri);
		}
		String requestPayload = createAddTenantRequest(tenantDTO);
		if(requestPayload == null){
			return false;
		}
		if(LOGGER.isTraceEnabled()){//PII LOG
			LOGGER.trace("Sending add tenant payload:"+requestPayload);
		}
		HttpPost post = new HttpPost(uri);
		HttpEntity payload = EntityBuilder.create()
				.setContentType(ContentType.APPLICATION_JSON)
				.setText(requestPayload).build();
		post.setEntity(payload);
		try {
			CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if(HttpStatus.OK.code != status){
				LOGGER.error("Error response received with status:"+status + " and response: " + Utils.getResponseString(response.getEntity()));
			}else{
				success= true;
				String updateTenantResponse = Utils.getResponseString(response.getEntity());
  			    JSONObject responseObj = (JSONObject) parser.parse(updateTenantResponse);
			}
			response.close();
		} catch (IOException | ParseException e) {
			LOGGER.error("Error adding tenant: ",e);
			success = false;
		} 
		return success;
	}
	
	public boolean updateTenantOnWSO2(TenantDTO tenantDTO) {
		boolean success = false;
		JSONParser parser = new JSONParser();
		String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"tenant/update";
		if(LOGGER.isDebugEnabled()){
			if(uri.indexOf("gi-identity") == -1){
				LOGGER.warn("Tenant update endpoint generally points to \"gi-identity\" component and URL does not seems to be right, please check");
			}
			LOGGER.debug("Using WSO2 Environment 5.3 for URI:"+uri);
		}
		String requestPayload = createUpdateTenantRequest(tenantDTO);
		if(requestPayload == null){
			return false;
		}
		if(LOGGER.isTraceEnabled()){//PII LOG
			LOGGER.trace("Sending update tenant payload:"+requestPayload);
		}
		HttpPost post = new HttpPost(uri);
		HttpEntity payload = EntityBuilder.create()
				.setContentType(ContentType.APPLICATION_JSON)
				.setText(requestPayload).build();
		post.setEntity(payload);
		try {
			CloseableHttpResponse response = (CloseableHttpResponse) this.restHttpClient.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if(HttpStatus.OK.code != status){
				LOGGER.error("Error response received with status:"+status + " and response: " + Utils.getResponseString(response.getEntity()));
			}else{
				success= true;
				String updateTenantResponse = Utils.getResponseString(response.getEntity());
  			    JSONObject responseObj = (JSONObject) parser.parse(updateTenantResponse);
			}
			response.close();
		} catch (IOException | ParseException e) {
			LOGGER.error("Error updating tenant: ",e);
			success = false;
		} 
		return success;
	}
	
	@Override
	public Tenant saveTenant(String code, TenantDTO tenantDTO) {
		Tenant tenant = tenantRepository.findByCode(code);
		boolean updateTenantInDB = true;

		if(updateTenantInDB){
			return updateTenant(tenant, tenantDTO);
		}
		
		return null;
	}

	private Tenant updateTenant(Tenant tenant, TenantDTO tenantToUpdate) {
		tenant.setName(tenantToUpdate.getName());
		TenantDTO tenantFromDB = null;

		// Check to see if URL already assigned to different tenant
		if(tenant != null && tenant.getUrl() != null && !tenant.getUrl().trim().equals(""))
		{
			tenantFromDB = getTenantForUrl(tenantToUpdate.getUrl());
			if(tenantFromDB != null && !tenantFromDB.getCode().equals(tenantToUpdate.getCode())) {
				throw new GIRuntimeException(ERROR_TENANT_SAME_URL);
			}
		}

		// If URL was not assigned and tenant was not found by URL, get it by id.s
		if(tenantFromDB == null)
		{
			tenantFromDB = getTenant(tenantToUpdate.getId());
		}

		tenant.setUrl(tenantToUpdate.getUrl());
		tenant.setCode(tenantToUpdate.getCode());
		tenant.setConfiguration(String2ConfigurationDTO.Current.reverseConvert(tenantToUpdate.getConfiguration()));
		tenant.setIsActive(Tenant.IS_ACTIVE.valueOf(tenantToUpdate.getIsActive()));
		tenant.setTestTenant(tenantToUpdate.getTestTenant());
		tenant.setTenantSSOConfiguration(tenantToUpdate.getTenantSSOConfiguration());

		return tenantRepository.save(tenant);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GIRuntimeException.class)
	public void addTenant(TenantDTO tenantDTO, int noOfExpirationDays, String activationUrl) {

		Tenant tenant = null;
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		boolean addTenantInDB = true;
		try {
			TenantDTO tenantForUrl = getTenantForUrl(tenantDTO.getUrl());
			if(tenantForUrl != null) {
				throw new Exception(ERROR_TENANT_SAME_URL);
			}
			
			if(tenantDTO.isRemotelyManagedUsers()){
				addTenantInDB = addTenantOnWSO2(tenantDTO);
			}

			if(addTenantInDB){
				tenant = new Tenant();
				tenant.setCode(tenantDTO.getCode());
				tenant.setName(tenantDTO.getName());
				tenant.setIsActive(IS_ACTIVE.Y);
				tenant.setCreatedDate(new TSDate());
				tenant.setUpdatedDate(new TSDate());
				tenant.setUrl(tenantDTO.getUrl());
				tenant.setTestTenant(tenantDTO.getTestTenant());
				if (tenantDTO.getConfiguration() != null) {
					tenant.setConfiguration(gson.toJson(tenantDTO.getConfiguration()));
				}
	
				tenant = tenantRepository.saveAndFlush(tenant);
	
				giAppConfigService.addTenantAppConfigurations(tenant.getId());
	
				tenantPermissionService.addTenantPermissions(tenant.getName());
	
				tenantRoleService.addTenantRoles(tenant.getId());
	
				tenantRoleService.addOpsadminUserForTenant(tenant.getId());
			}else{
				LOGGER.error("Could not add tenant on WSO2 server");
				throw new Exception(ERROR_ADD_TENANT_WSO2);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while provisioning tenant: ", e);
			throw new GIRuntimeException(e);
		}
	}

	@Override
	public String addTenantConfigurations(Tenant tenant){
		if(tenant != null && StringUtils.isNotBlank(tenant.getName())){
			giAppConfigService.addTenantAppConfigurations(tenant.getId());
			return "SUCCESS";
		}else{
			return "FAILURE";
		}

	}

	@Override
	public String addTenantPermissions(Tenant tenant){
		if(tenant != null && StringUtils.isNotBlank(tenant.getName())){
			tenantPermissionService.addTenantPermissions(tenant.getName());
			return "SUCCESS";
		}else{
			return "FAILURE";
		}

	}

	@Override
	public URL upload(String tenantName, String propertyKey, String fileName, byte[] dataBytes) {
		return externalCloudServiceImpl.upload(formRelativePath(tenantName, propertyKey), fileName, dataBytes);
	}

	private String formRelativePath(String tenantName, String propertyKey) {
		return tenantName + SEPARATOR + propertyKey;
	}

	@Override
	public String addTenantRoles(Tenant tenant) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AffiliateFlow getDefaultFlowForAffiliate(Long affiliateId) {
		return affiliateFlowRepository.findByAffiliateIdAndIsDefault(affiliateId);
	}

	@Override
	public AffiliateFlow getAffiliateFlowById(Integer affiliateFlowId)
	{
		return affiliateFlowTenantUnawareRepository.getAffiliateFlowByFlowId(affiliateFlowId);
	}

	@Override
	public AffiliateFlow getAffiliateFlowForUrl(String hostName) {
		return affiliateFlowRepository.findByUrl(hostName);
	}

	@PostConstruct
	public void initDefaultTenant() {
		ghixTenant = Tenant2TenantDTO.Current.convert(tenantRepository.findByCode(GINS));
		restHttpClient = SecureHttpClient.getHttpClient();
	}

	private Map<String, String> populateTenantSpecificTokens() {
		Map<String, String> messageTokens = new HashMap<>();

		for (String token : HEADER_FOOTER_TOKENS) {
			switch (token) {
				case LOGO_URL:
					String logoUrl = GhixPlatformEndPoints.APP_URL + "resources/img/logo_getinsured.png";
					if (ghixTenant != null && ghixTenant.getConfiguration() != null	&& ghixTenant.getConfiguration().getUiConfiguration() != null &&
							ghixTenant.getConfiguration().getUiConfiguration().getBrandingConfiguration() != null &&
							StringUtils.isNotBlank(ghixTenant.getConfiguration().getUiConfiguration().getBrandingConfiguration().getLogoUrl())) {
						logoUrl = ghixTenant.getConfiguration().getUiConfiguration().getBrandingConfiguration().getLogoUrl();
					}
					messageTokens.put(LOGO_URL, logoUrl);
					break;
				case REDIRECT_URL:
					messageTokens.put(REDIRECT_URL, GhixPlatformEndPoints.APP_URL);
					break;
				case FOOTER_ADDRESS:
					String footerAddress = StringUtils.EMPTY;
					if(ghixTenant != null && ghixTenant.getConfiguration() != null &&
							ghixTenant.getConfiguration().getEmailConfiguration() != null &&
							StringUtils.isNotBlank(ghixTenant.getConfiguration().getEmailConfiguration().getFooterAddress())) {
						footerAddress = ghixTenant.getConfiguration().getEmailConfiguration().getFooterAddress();
					}
					messageTokens.put(FOOTER_ADDRESS, footerAddress);
					break;
				case CUSTOMER_CARE_NUM:
					String customerCareNumber = null;
					GIAppProperties exchangePhone = gIAppConfigRepository.findByPropertyKeyAndTenantId(GLOBAL_EXCHANGE_PHONE, ghixTenant.getId());
					if(exchangePhone != null) {
						customerCareNumber = exchangePhone.getPropertyValue();
					}
					messageTokens.put(CUSTOMER_CARE_NUM, customerCareNumber);
					break;
				case DISCLAIMER_CONTENT:
					String disclaimerContent = StringUtils.EMPTY;
					if(ghixTenant.getConfiguration() != null &&
							ghixTenant.getConfiguration().getEmailConfiguration() != null &&
							StringUtils.isNotBlank(ghixTenant.getConfiguration().getEmailConfiguration().getDisclaimerContent())) {
						disclaimerContent = GhixUtils.parseForHtmlContent(ghixTenant.getConfiguration().getEmailConfiguration().getDisclaimerContent());
					}
					messageTokens.put(DISCLAIMER_CONTENT, disclaimerContent);
					break;
			}
		}
		return messageTokens;
	}

	@Override
	public List<Tenant> getAllTenant() {
		return tenantRepository.findAll();
	}

}
