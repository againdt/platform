package com.getinsured.hix.platform.ecm.web;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

/**
 * Controller for ECM File Download
 * @author Ekram Ali Kazi
 *
 */
@Controller
public class ECMFileDownloadController {

	@Autowired private ContentManagementService ecmService;

	/**
	 * downloads document based on ECM file Id
	 * @param response
	 * @param uploadedFileId
	 * @return
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/ecm/filedownloadbyid", method = RequestMethod.GET)
	public ModelAndView downloadFileById(HttpServletResponse response,
			@ModelAttribute("uploadedFileId") String uploadedFileId	) throws ContentManagementServiceException, IOException {

		byte[] bytes = null;
		try {
			bytes = ecmService.getContentDataById(uploadedFileId);
		} catch (ContentManagementServiceException e) {
			throw new ContentManagementServiceException("Error downloading file from ecm - " + e);
		}

		Content content = ecmService.getContentById(uploadedFileId);

		streamData(response, bytes, content.getTitle());
		return null;
	}

	/**
	 * downloads document based on ECM file name
	 * @param response
	 * @param uploadedFilePath
	 * @return
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/ecm/filedownloadbypath", method = RequestMethod.GET)
	public ModelAndView downloadFileByPath(HttpServletResponse response,
			@ModelAttribute("uploadedFilePath") String uploadedFilePath) throws ContentManagementServiceException, IOException {

		byte[] bytes = null;
		try {
			bytes = ecmService.getContentDataByPath(uploadedFilePath);
		} catch (ContentManagementServiceException e) {
			throw new ContentManagementServiceException("Error downloading file from ecm - " + e);
		}

		streamData(response, bytes,uploadedFilePath);
		return null;
	}

	private void streamData(HttpServletResponse response, byte[] bytes, String fileName) throws IOException {
		if(bytes == null)
		{
			response.setStatus(HttpStatus.NOT_FOUND.value());
			String error = "{\"error\":\"" + HttpStatus.NOT_FOUND.value() + ": document not found for given path.\"}";
			FileCopyUtils.copy(error.getBytes(), response.getOutputStream());
		}
		else
		{
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.setContentType("application/octet-stream");
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
	}
}
