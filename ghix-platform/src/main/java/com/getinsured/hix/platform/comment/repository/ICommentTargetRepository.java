/**
 * 
 */
package com.getinsured.hix.platform.comment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.CommentTarget;

/**
 * @author panda_p
 *
 */

@Transactional(readOnly = true)
public interface ICommentTargetRepository extends JpaRepository<CommentTarget, Integer> {

	@Query("SELECT cmntTarget FROM CommentTarget cmntTarget where targetId = :targetId and targetName= :targetName")
	public CommentTarget findByTargetIdAndTargetType(@Param("targetId") Long targetId,@Param("targetName") CommentTarget.TargetName targetName);

	//public CommentGroup findByTargetIdAndTargetType(String targetId, String targetType);
}
