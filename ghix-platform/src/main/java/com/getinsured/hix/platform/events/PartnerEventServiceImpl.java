package com.getinsured.hix.platform.events;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.repository.AffiliateRepository;

@Component
public class PartnerEventServiceImpl implements PartnerEventService {

	private static final Logger LOGGER = Logger.getLogger(PartnerEventServiceImpl.class);

	private static final String ZERO = "0";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private AffiliateRepository affiliateRepository;
	
	@Override
	@Async
	public void publishEvent(Map<PartnerEventDataFieldEnum, String> eventData) {
		LOGGER.debug("PARTNER EVENT NOTIFICATION: Received event notification for with event data " + eventData);
		if(eventData != null ) {
			String affiliateId = eventData.get(PartnerEventDataFieldEnum.AFFILIATE_ID);
			if ( affiliateId != null && !ZERO.equals(affiliateId) &&
					eventData.get(PartnerEventDataFieldEnum.LEAD_ID) != null && 
					StringUtils.isNotBlank(eventData.get(PartnerEventDataFieldEnum.EVENT_NAME)) && 
					(eventData.get(PartnerEventDataFieldEnum.ENROLLMENT_ID) != null || eventData.get(PartnerEventDataFieldEnum.CART_ITEM_ID) != null)) {
				try {
					Affiliate affiliate = affiliateRepository.findOne(Long.valueOf(affiliateId));
					LOGGER.debug("PARTNER EVENT NOTIFICATION: Affiliate configuration defined: " + affiliate.getConfiguration());
	
					if(affiliate.getAffiliateConfig() != null && StringUtils.isNotBlank(affiliate.getAffiliateConfig().getEventNotificationUrl())) {
						String eventNotificationUrl = affiliate.getAffiliateConfig().getEventNotificationUrl();
						LOGGER.debug("PARTNER EVENT NOTIFICATION: Event notification url for affiliate: " + eventNotificationUrl);
						eventNotificationUrl = eventNotificationUrl.replaceAll("&amp;", "&");
						
						LOGGER.debug("PARTNER EVENT NOTIFICATION: replacing LEAD_ID: " + eventData.get(PartnerEventDataFieldEnum.LEAD_ID));
						eventNotificationUrl = eventNotificationUrl.replaceAll(PartnerEventDataFieldEnum.LEAD_ID.name(), eventData.get(PartnerEventDataFieldEnum.LEAD_ID));
	
						LOGGER.debug("PARTNER EVENT NOTIFICATION: replacing EVENT_NAME: " + eventData.get(PartnerEventDataFieldEnum.EVENT_NAME));
						eventNotificationUrl = eventNotificationUrl.replaceAll(PartnerEventDataFieldEnum.EVENT_NAME.name(), eventData.get(PartnerEventDataFieldEnum.EVENT_NAME));
						
						LOGGER.debug("PARTNER EVENT NOTIFICATION: Replacing CART_ITEM_ID");
						if(eventData.get(PartnerEventDataFieldEnum.CART_ITEM_ID) != null) { 
							eventNotificationUrl = eventNotificationUrl.replaceAll(PartnerEventDataFieldEnum.CART_ITEM_ID.name(), eventData.get(PartnerEventDataFieldEnum.CART_ITEM_ID));
							LOGGER.debug("PARTNER EVENT NOTIFICATION: Replaced CART_ITEM_ID");
						}
						
						LOGGER.debug("PARTNER EVENT NOTIFICATION: Replacing ENROLLMENT_ID");
						if(eventData.get(PartnerEventDataFieldEnum.ENROLLMENT_ID) != null) {
							eventNotificationUrl = eventNotificationUrl.replaceAll(PartnerEventDataFieldEnum.ENROLLMENT_ID.name(), eventData.get(PartnerEventDataFieldEnum.ENROLLMENT_ID));
							LOGGER.debug("PARTNER EVENT NOTIFICATION: Replaced ENROLLMENT_ID");
						}
						
						LOGGER.info("PARTNER EVENT NOTIFICATION: Triggering event notification url: " + eventNotificationUrl);
						String response = restTemplate.getForObject(eventNotificationUrl, String.class);
						LOGGER.info("PARTNER EVENT NOTIFICATION: Event triggered for affiliate: " + affiliateId + " with event data " + eventData + " at url: " + eventNotificationUrl + ". Response received:" + response);
					}
				} catch (Exception ex) {
					LOGGER.error("PARTNER EVENT NOTIFICATION: Error processing event for " + TenantContextHolder.getAffiliateId() + " with event data " + eventData, ex);
				}
			}
		}
	}
}
