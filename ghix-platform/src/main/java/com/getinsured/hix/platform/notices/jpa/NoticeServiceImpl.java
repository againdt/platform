package com.getinsured.hix.platform.notices.jpa;

import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notification.NoticeTmplHelper;
import com.getinsured.hix.platform.notify.EmailNotificationService;
import com.getinsured.hix.platform.notify.NotificationTypeNotFound;
import com.getinsured.hix.platform.repository.NoticeQueuedRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;

/**
 * Implementation for NoticeService interface to generate PDFs based on either FLYING SAUCER or ADOBE LiveCycle Strategy.
 * 
 * @author EkramAli Kazi
 *
 */
@Service
public class NoticeServiceImpl implements NoticeService {

	@Value("#{configProp['notice_engine_type']}")
	private String noticeEngineType;

	@Autowired private DefaultNotice defaultNotice;
	@Autowired private AdobeLiveCycleNotice adobeLiveCycleNotice;
	@Autowired private NoticeTypeRepository noticeTypeRepository;
	@Autowired private EmailNotificationService emailNotificationService;
	@Autowired private NoticeQueuedRepository noticeQueuedRepository;
	private INoticeStrategy iNoticeStrategy;

	@Autowired private ContentManagementService ecmService;

	private static final Logger LOGGER = LoggerFactory.getLogger(NoticeServiceImpl.class);

	@PostConstruct
	public void createDocumentContextInitilizer() {
		if ("DEFAULT".equals(noticeEngineType)) {
			iNoticeStrategy = defaultNotice;
		}else if ("ADOBE_LIVECYCLE".equals(noticeEngineType)) {
			iNoticeStrategy = adobeLiveCycleNotice;
		}else{
			LOGGER.error("Unknow notice_engine_type set in configuration.properties.");
			throw new IllegalArgumentException("Unknow notice_engine_type set in configuration.properties.");
		}
	}

	@Override
	public Notice notify(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath,
			String ecmFileName, AccountUser user, Location location,
			Map<String, String> emailMetaData, Map<String, String> messageTokens, String emailClazzName, 
			GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException, NotificationTypeNotFound {
		Notice noticeObj = null;
		switch (communicationPref){
			case Mail:
				noticeObj = createNotice(noticeTemplateName, language, noticeDataMap,  ecmRelativePath,  ecmFileName,  user, location,  communicationPref);
				break;
			case Email:
				noticeObj = emailNotificationService.generateEmail(emailMetaData, messageTokens, emailClazzName, null);
				emailNotificationService.sendEmail(noticeObj);
				break;
			case EmailAndMail:	
				noticeObj = createNotice(noticeTemplateName, language, noticeDataMap,  ecmRelativePath,  ecmFileName,  user, location,  communicationPref);
				noticeObj = emailNotificationService.generateEmail(emailMetaData, messageTokens, emailClazzName, null);
				emailNotificationService.sendEmail(noticeObj);
			case None:
				noticeObj = createNotice(noticeTemplateName, language, noticeDataMap,  ecmRelativePath,  ecmFileName,  user, location,  communicationPref);
				break;
			default:
				throw new IllegalArgumentException("Invalid communicationpref passed.");
			
		}
		
		return noticeObj;
	}

	@Override
	public Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, AccountUser user) throws NoticeServiceException{

		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);

		if (user == null || user.getId() == 0){
			throw new IllegalArgumentException("Account User cannot be blank");
		}

		return iNoticeStrategy.createNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, user, null, null);

	}
	
	@Override
	public Notice createNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, 
			String ecmRelativePath, String ecmFileName, AccountUser user,Location location, GhixNoticeCommunicationMethod communicationPref) throws NoticeServiceException{

		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);
		
		if (user == null || user.getId() == 0){
			throw new IllegalArgumentException("Account User cannot be blank");
		}
		
		return iNoticeStrategy.createNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, user, location, communicationPref);
	}
	
	@Override
	public Notice createNoticeWOInbox(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, AccountUser user) throws NoticeServiceException{

		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);

		if (user == null || user.getId() == 0){
			throw new IllegalArgumentException("Account User cannot be blank");
		}

		return iNoticeStrategy.createNoticeWOInbox(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, user);

	}

	@Override
	public byte[] getNotificationPDFContent(Notice notice) throws ContentManagementServiceException{

		if (notice == null || StringUtils.isEmpty(notice.getEcmId())){
			throw new IllegalArgumentException("Notice ECM ID cannot be blank");
		}

		return ecmService.getContentDataById(notice.getEcmId());

	}

	@Override
	public Notice createModuleNotice(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName, String moduleName, long moduleId, List<String> sendToEmailList, String fromFullName, String toFullName) throws NoticeServiceException {
		
		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);
		if(!(moduleId>0 && moduleName !=null && moduleName.trim().length()>0)){
			throw new IllegalArgumentException("Module User information cannot be blank");				
		}
		return iNoticeStrategy.createModuleNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, moduleName, moduleId, sendToEmailList, fromFullName, toFullName, null, null);
	}
	
	@Override
	public Notice createModuleNotice(String noticeTemplateName,GhixLanguage language, Map<String, Object> noticeDataMap,
			String ecmRelativePath, String ecmFileName, String moduleName,long moduleId, List<String> sendToEmailList, String fromFullName,
			String toFullName, Location location,GhixNoticeCommunicationMethod communicationPref)throws NoticeServiceException {
		validateRequest(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName);
		
		return iNoticeStrategy.createModuleNotice(noticeTemplateName, language, noticeDataMap, ecmRelativePath, ecmFileName, moduleName, moduleId, sendToEmailList, fromFullName, toFullName, location, communicationPref);
	}
	
	private boolean validateRequest(String noticeTemplateName, GhixLanguage language, Map<String, Object> noticeDataMap, String ecmRelativePath, String ecmFileName){
		
		if (StringUtils.isEmpty(noticeTemplateName)){
			throw new IllegalArgumentException("Notice template name cannot be blank");
		}

		if (language == null || StringUtils.isEmpty(language.getLanguage())){
			throw new IllegalArgumentException("Notice template language cannot be blank");
		}

		if (StringUtils.isEmpty(ecmRelativePath)){
			throw new IllegalArgumentException("ECM path cannot be blank");
		}

		if (StringUtils.isEmpty(ecmFileName)){
			throw new IllegalArgumentException("ECM file name cannot be blank");
		}
		
		return true;
	}



	@Autowired private NoticeTmplHelper noticeTmplHelper;
	
	@Override
	public String getEcmContentId(NoticeType noticeType) throws ContentManagementServiceException {
		String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
				+ noticeType.getTemplateLocation();
		
		Content content = noticeTmplHelper.readContentMetaByPath(ecmTemplateFolderPath);;
		String ecmContentId = content.getContentId();
		noticeType.setExternalId(ecmContentId);
		noticeType.setUpdated(new TSDate());
		
		// save the content id to DB
		NoticeType updatedNotice = noticeTypeRepository.save(noticeType);
		return updatedNotice.getExternalId();
		
	}

	
	@Override
	public byte[] createPreviewContent(String  templateContent, Map<String, Object> tokens) throws NoticeServiceException{
		return defaultNotice.getPreviewContent(templateContent, tokens);
	}


	@Override
	public void postToInbox(AccountUser userObj, List<String> sendToEmailList, String moduleName, long moduleId,
			Notice notice, String docName, String fromFullName, String toFullName)
					throws ContentManagementServiceException, GIException {
		defaultNotice.postToInbox(userObj, sendToEmailList, moduleName, moduleId, notice, docName, fromFullName, toFullName);
	}

	@Override
	public String getPDFContentAsBase64Encoded(String documentId) throws ContentManagementServiceException {
		
		if (StringUtils.isEmpty(documentId)){
			throw new IllegalArgumentException("Document ID cannot be blank");
		}
		byte[] encodedBytes = Base64.getEncoder().encode(ecmService.getContentDataById(documentId));
		return new String(encodedBytes) ;
	}

	@Override
	public boolean scheduleQueuedNotice(String emailClassName, Date scheduleDate, NoticeQueued.TableNames tableName, String tableColumn, Long recordId) throws DataAccessException, IllegalArgumentException {
		if (StringUtils.isBlank(emailClassName)) {
			LOGGER.error("scheduleQueuedNotice::Email Class name null or empty");
			return false;
		} else if(scheduleDate == null) {
			LOGGER.error("scheduleQueuedNotice::Date for scheduling was null");
			return false;
		} else if(recordId == null) {
			LOGGER.error("scheduleQueuedNotice::Record id was null");
			return false;
		} else if(StringUtils.isBlank(tableColumn)) {
			LOGGER.error("scheduleQueuedNotice::Table column null or empty");
			return false;
		}

		try {
			NoticeType noticeType = noticeTypeRepository.findByEmailClass(emailClassName);
			LOGGER.debug("scheduleQueuedNotice::emailClassName class {}", emailClassName);

			if(noticeQueuedRepository.findByUniqueConstraint(noticeType, tableName, tableColumn, recordId, scheduleDate, NoticeQueued.QueuedStatus.STAGED) == null) {
				LOGGER.debug("scheduleQueuedNotice::Notice Queued does not already exist");
				NoticeQueued noticeQueued = new NoticeQueued();
				noticeQueued.setStatus(NoticeQueued.QueuedStatus.STAGED);
				noticeQueued.setNoticeType(noticeType);
				noticeQueued.setTableName(tableName);
				noticeQueued.setColumnName(tableColumn);
				noticeQueued.setColumnValue(recordId);
				noticeQueued.setProcessingDate(new Timestamp(scheduleDate.getTime()));
				noticeQueuedRepository.save(noticeQueued);
			} else {
				LOGGER.debug("scheduleQueuedNotice::Notice Queued already exists");
				return true;
			}
		} catch (Exception ex) {
			LOGGER.error("scheduleQueuedNotice::error saving notice queued", ex);
			return false;
		}

		LOGGER.debug("scheduleQueuedNotice::successful save of notice queued");
		return true;
	}
}
