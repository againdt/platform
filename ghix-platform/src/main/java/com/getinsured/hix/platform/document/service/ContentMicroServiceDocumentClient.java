package com.getinsured.hix.platform.document.service;

import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.content.dto.Document;
import com.getinsured.content.dto.Document.PropertyKey;
import com.getinsured.content.dto.DocumentTag;
import com.getinsured.hix.platform.couchbase.helper.BucketMetaHelper;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;

@Service
@DependsOn("dynamicPropertiesUtil")
public class ContentMicroServiceDocumentClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContentMicroServiceDocumentClient.class);

	private static final String ERROR_READING_DOC_FROM_DOCUMENT_CONTENT_SERVICE = "Error reading doc from Document Content Service";

	private static final String EXADMIN = "exadmin@ghix.com";

	@Value("#{configProp['msContentSvcUrl'] != null ? configProp['msContentSvcUrl'].trim() : null}")
	private String msContentSvcUrl;

	@Value("#{configProp['mscontent.document.bucketName'] != null ? configProp['mscontent.document.bucketName'].trim() : null }")
	private String bucketName;

	@Value("#{configProp['mscontent.document.baseFolder'] != null ? configProp['mscontent.document.baseFolder'].trim() : null}")
	private String baseFolder;

	private String branch;
	private String env;
	private String tenantCode;
	@Autowired
	private Gson platformGson;

	private static final String BUCKET_NAME_FOLDER_NAME = "document/%s/%s";
	private static final String UPLOAD_PATH = BUCKET_NAME_FOLDER_NAME + "?docPath=%s&skipAntiVirusCheck=%s";
	private static final String UPDATE_DOC_PATH = BUCKET_NAME_FOLDER_NAME + "?skipAntiVirusCheck=%s";
	private static final String DOC_PATH = BUCKET_NAME_FOLDER_NAME + "?docId=%s";
	private static final String DOC_TAG_PATH = BUCKET_NAME_FOLDER_NAME + "/tags?docId=%s";
	private static final String DOC_META_PATH = BUCKET_NAME_FOLDER_NAME + "/metadata?docId=%s";
	private static final String DOCS_PATH = "documents/%s/%s?docPath=%s";

	@PostConstruct
	private void post() {
		this.branch = BucketMetaHelper.getBuildArtifactVersion();
		this.env = BucketMetaHelper.getEnv();
		this.tenantCode = BucketMetaHelper.getTenantCode();
	}

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	public String createObject(String uploadPath, Document doc, boolean skipAntiVirusCheck) {

		doc.getProperties().put(PropertyKey.BRANCH, this.branch);
		doc.getProperties().put(PropertyKey.ENV, env);
		doc.getProperties().put(PropertyKey.TENANT_CODE, tenantCode);

		String content = platformGson.toJson(doc);

		String createUpdateUrl = msContentSvcUrl + String.format(UPLOAD_PATH, bucketName, baseFolder, uploadPath, skipAntiVirusCheck);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("createUpdateUrl - " + createUpdateUrl);
		}
		ResponseEntity<String> response = ghixRestTemplate.exchange(createUpdateUrl, EXADMIN,
				HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, content);

		return processAndReturn(response);

	}

	public String updateObject(Document doc, boolean skipAntiVirusCheck) {

		String content = platformGson.toJson(doc);

		String updateUrl = msContentSvcUrl + String.format(UPDATE_DOC_PATH, bucketName, baseFolder, skipAntiVirusCheck);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("updateUrl - " + updateUrl);
		}

		ResponseEntity<String> response = ghixRestTemplate.exchange(updateUrl, EXADMIN,
				HttpMethod.PUT, MediaType.APPLICATION_JSON, String.class, content);

		return processAndReturn(response);

	}

	public Document getObject(String key) {

		String getObjectUrl = msContentSvcUrl + String.format(DOC_PATH, bucketName, baseFolder, key);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("getObjectUrl - " + getObjectUrl);
		}

		ResponseEntity<Document> response = ghixRestTemplate.exchange(getObjectUrl, EXADMIN,
				HttpMethod.GET, MediaType.APPLICATION_JSON, Document.class, null);

		return processAndReturn(response);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Set<DocumentTag> getObjectTags(String key) {

		String getObjectTagsUrl = msContentSvcUrl + String.format(DOC_TAG_PATH, bucketName, baseFolder, key);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("getObjectTagsUrl - " + getObjectTagsUrl);
		}

		ResponseEntity<Set> response = ghixRestTemplate.exchange(getObjectTagsUrl, EXADMIN,
				HttpMethod.GET, MediaType.APPLICATION_JSON, Set.class, null);

		return processAndReturn(response);
	}

	public Document getObjectMetaData(String key) {

		String getObjectMetaDataUrl = msContentSvcUrl + String.format(DOC_META_PATH, bucketName, baseFolder, key);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("getObjectMetaDataUrl - " + getObjectMetaDataUrl);
		}

		ResponseEntity<Document> response = ghixRestTemplate.exchange(getObjectMetaDataUrl, EXADMIN,
				HttpMethod.GET, MediaType.APPLICATION_JSON, Document.class, null);

		return processAndReturn(response);
	}

	private <T> T processAndReturn(ResponseEntity<T> response) {
		if (response != null
				&& (response.getStatusCode() == HttpStatus.OK || response.getStatusCode() == HttpStatus.CREATED)) {
			return response.getBody();
		} else {
			throw new GIRuntimeException(ERROR_READING_DOC_FROM_DOCUMENT_CONTENT_SERVICE);
		}
	}

	public void deleteObject(String key) {
		String deleteUrl = msContentSvcUrl + String.format(DOC_PATH, bucketName, baseFolder, key);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("deleteUrl - " + deleteUrl);
		}

		ResponseEntity<Void> response = ghixRestTemplate.exchange(deleteUrl, EXADMIN,
				HttpMethod.DELETE, MediaType.APPLICATION_JSON, Void.class, null);

		processAndReturn(response);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> getLists(String docPath) {
		String docsUrl = msContentSvcUrl + String.format(DOCS_PATH, bucketName, baseFolder, docPath);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info("docsUrl - " + docsUrl);
		}

		ResponseEntity<List> response = ghixRestTemplate.exchange(docsUrl, EXADMIN,
				HttpMethod.GET, MediaType.APPLICATION_JSON, List.class, null);

		return processAndReturn(response);
	}

}
