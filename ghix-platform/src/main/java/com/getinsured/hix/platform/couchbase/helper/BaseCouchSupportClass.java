package com.getinsured.hix.platform.couchbase.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.couchbase.client.core.CouchbaseException;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.PersistTo;
import com.couchbase.client.java.ReplicaMode;
import com.couchbase.client.java.document.BinaryDocument;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.JsonLongDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;
import com.getinsured.hix.platform.couchbase.dto.CouchBinaryDocument;
import com.getinsured.hix.platform.couchbase.helper.DocumentHelper;

import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class BaseCouchSupportClass implements CouchbaseSupport {
	private static final Logger log = LoggerFactory.getLogger(BaseCouchSupportClass.class);

	private static final int _1 = 1;
	private static final int _100 = 100;
	private static final String HYPHEN = "-";
	private static final String IDENTITY = "IDENTITY";
	private static final String GLOBAL_RESOURCE = "GLOBAL-RESOURCE";
	private Bucket bucket;
	private static final int TIMEOUT = 100000;

	@Autowired
	@Qualifier("couchDocumentHelper")
	private DocumentHelper documentHelper;

	@Value("#{configProp['couchbase.nonbinary.bucketname']}")
	private String nonbinaryBucketName;

	@Override
	public void setBucket(Bucket bucket) {
		this.bucket = bucket;
	}

	@Override
	public Bucket getBucket() {
		return bucket;
	}

	@Override
	public <T> T findById(String id, Class<? extends T> type) {
		JsonDocument doc = this.bucket.get(id, TIMEOUT, TimeUnit.MILLISECONDS);
		return doc == null ? null : fromJsonDocument(doc, type);
	}

	/**
	 * Returns document(s) from all available Couchbase nodes, based on the availability.
	 * <p>
	 *   When multiple copies are returned, for example from Master + Replica #1, Replica #4 (with Replica #2 and #3 unavailable or down),
	 *   it will compare modification date of each returned document and will return the one with the latest Modification Date.
	 * </p>
	 * @param id document id.
	 * @param type type of the document.
	 * @param <T> type of the document.
	 * @return single document.
	 */
	public <T> T findByIdFromAllNodes(String id, Class<? extends T> type)
	{
		if(log.isDebugEnabled())
		{
			log.debug("[+] Retrieving CB Document by ID: {}", id);
		}

		List<JsonDocument> documents = this.bucket.async()
				.getFromReplica(id, ReplicaMode.ALL, JsonDocument.class)
				.toList().toBlocking().single();

		final JsonDocument[] singleDocument = {null};

		if(documents.size() > 1)
		{
			documents.forEach(d -> {
				if (singleDocument[0] == null)
				{
					singleDocument[0] = d;
				}
				if (singleDocument[0].content() != null)
				{
					JsonObject jsonMetadata = (JsonObject) singleDocument[0].content().get("metaData");
					Long modifiedDate = Long.valueOf(String.valueOf(jsonMetadata.get("modifiedDate")));

					JsonObject jsonMetadataNext = (JsonObject) d.content().get("metaData");
					Long newModifiedDate = Long.valueOf(String.valueOf(jsonMetadataNext.get("modifiedDate")));

					if (newModifiedDate > modifiedDate)
					{
						singleDocument[0] = d;
					}
				}
			});
		}
		else if(documents.size() == 1)
		{
			singleDocument[0] = documents.get(0);
		}


		if(log.isDebugEnabled())
		{
			log.debug("[+] Got CB Document by ID: {}", id);
		}

		return singleDocument[0] == null ? null : fromJsonDocument(singleDocument[0], type);
	}

	private <T> T fromJsonDocument(JsonDocument doc, Class<? extends T> type) {
		return documentHelper.fromJsonDocument(doc, type);
	}

	@Override
	public <T> String create(T entity, String id) {
		if (isBlank(id)) {
			id = getNextId(_1, _100);
		}
		JsonDocument docIn = toJsonDocument(entity, id);
		JsonDocument docOut;
		try {
			docOut = this.bucket.insert(docIn, TIMEOUT, TimeUnit.MILLISECONDS);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}

	private <T> JsonDocument toJsonDocument(T entity, String id) {
		return documentHelper.toJsonDocument(entity, id);
	}

	private <T> String getNextId(long incr, long init) {
		JsonLongDocument doc = this.bucket.counter(IDENTITY + HYPHEN + GLOBAL_RESOURCE, incr, init);
		return GLOBAL_RESOURCE + HYPHEN + doc.content().toString();
	}

	@Override
	public <T> String update(T entity, String id) {
		JsonDocument docIn = toJsonDocument(entity, id);
		JsonDocument docOut;
		try {
			docOut = this.bucket.replace(docIn, TIMEOUT, TimeUnit.MILLISECONDS);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}

	@Override
	public void delete(String id) {
		try {
			this.bucket.remove(id);
		} catch (CouchbaseException e) {
			throw e;
		}
	}

	@Override
	public String createBinary(CouchBinaryDocument entity, String id) {
		if (isBlank(id)) {
			id = getNextId(_1, _100);
		}
		JsonDocument docIn = toJsonDocument(entity, id);
		JsonDocument docOut;
		try {
			docOut = this.bucket.insert(docIn, PersistTo.MASTER, TIMEOUT, TimeUnit.MILLISECONDS);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}

	@Override
	public String updateBinary(CouchBinaryDocument entity, String id) {
		JsonDocument docIn = toJsonDocument(entity, id);
		JsonDocument docOut;
		try {
			docOut = this.bucket.replace(docIn, PersistTo.MASTER, TIMEOUT, TimeUnit.MILLISECONDS);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}

	@Override
	public byte[] findBinaryByIdNative(String id) {
		BinaryDocument doc = this.bucket.get(id, BinaryDocument.class, TIMEOUT, TimeUnit.MILLISECONDS);
		return doc == null ? null : fromBinaryDocument(doc);
	}

	private byte[] fromBinaryDocument(BinaryDocument doc) {
		return documentHelper.fromBinaryDocument(doc);
	}

	@Override
	public String createBinaryNative(byte[] data, String id) {
		if (isBlank(id)) {
			id = getNextId(_1, _100);
		}
		BinaryDocument docIn = toBinaryDocument(data, id);
		BinaryDocument docOut;
		try {
			docOut = this.bucket.insert(docIn, TIMEOUT, TimeUnit.MILLISECONDS);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}

	private BinaryDocument toBinaryDocument(byte[] data, String id) {
		return documentHelper.toBinaryDocument(data, id);
	}

	@Override
	public <T> String updateBinaryNative(byte[] data, String id) {
		BinaryDocument docIn = toBinaryDocument(data, id);
		BinaryDocument docOut;
		try {
			docOut = this.bucket.replace(docIn, TIMEOUT, TimeUnit.MILLISECONDS);
		} catch (CouchbaseException e) {
			throw e;
		}
		return docOut.id();
	}

	@Override
	public <T> List<T> query(String statement, Class<? extends T> type) {
		N1qlQuery query = N1qlQuery.simple(statement);
		N1qlQueryResult result = this.bucket.query(query, TIMEOUT, TimeUnit.MILLISECONDS);

		List<T> rows = new ArrayList<>();
		for (N1qlQueryRow row : result){
			JsonObject data = row.value().getObject(nonbinaryBucketName);
			T document = fromJsonDocument(data, type);
			rows.add(document);
		}

		return rows;
	}

	/**
	 * Queries all nodes instead of master.
	 *
	 * TODO: See if we can use Observable/async in some calling methods.
	 *
	 * @param statement N1QL query statement
	 * @param type type of class to bind results to
	 * @param <T> type returned
	 * @return List of results
	 */
	protected <T> List<T> executeN1Query(String statement, Class<? extends T> type) {
		N1qlQuery query = N1qlQuery.simple(statement);
		List<T> rows = new ArrayList<>();

		this.bucket.query(query, TIMEOUT, TimeUnit.MILLISECONDS).allRows().forEach(row -> {
			JsonObject data = row.value().getObject(nonbinaryBucketName);
			T document = fromJsonDocument(data, type);
			rows.add(document);
		});

		return rows;
	}

	private <T> T fromJsonDocument(JsonObject obj, Class<? extends T> type) {
		return documentHelper.fromJsonDocument(obj, type);
	}

}
