/**
 * GHIXFTPClient.java
 */

package com.getinsured.hix.platform.ftp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class has methods required for file operations on a source from an ftp
 * server.
 * 
 * @author Romin Irani, Sunil Desu, Bhavani Polimetla
 * 
 * 
 */
public class GHIXFTPClient {

	private static final int BUFFER_SIZE = 1024;
	private static final String INVALID_FTP_CLIENT = "Please initialize the FTP Client with host and credentials";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GHIXFTPClient.class);
	public static final String SUCCESS_FOLDER = "success";
	public static final String ERRORS_FOLDER = "errors";
	public static final String INPROCESS_FOLDER = "inprocess";

	public static enum PROCESSING_STATUS {
		PROCESSING, ERROR, SUCCESS
	};

	// FTP Login Credentials
	private String hostname;
	private String username;
	private String password;

	private boolean bInited = false;

	public GHIXFTPClient(String hostname, String username, String password) {
		super();
		this.hostname = hostname;
		this.username = username;
		this.password = password;
		bInited = true;
		LOGGER.info("GHIXFTPClient initialized for host " + hostname);		
	}

	private FTPClient client = null;

	public void initConnection() throws SocketException, IOException {
		if (null != client) {
			try {
				client.getStatus();				
				LOGGER.info("client.getStatus() " + client.getStatus());
				if (client.isConnected() && client.isAvailable()) {
					return;
				}
			} catch (Exception e) {
				LOGGER.info("SOCKET Exception occurred. Trying to login again");
				client.connect(hostname);
				client.login(username, password);
				LOGGER.info("SOCKET Exception occurred. Re-login successful");
				return;
			}
		}
		LOGGER.info("FTP client being instantiated.");
		client = new FTPClient();
		client.connect(hostname);
		client.login(username, password);		
		LOGGER.info("FTP client instance created. Login successful");
	}

	public void closeClient() {
		try {
			client.logout();			
			LOGGER.info("FTP Client logged out");
		} catch (Exception ex) {
			LOGGER.error("Exception while logging out",ex);
		}

		finally {
			try {
				client.disconnect();
				client = null;
				LOGGER.info("FTP Client disconnected");
			} catch (IOException e) {
				LOGGER.error("Exception while closing client: ",e);
			}
		}
	}

	/**
	 * This method returns the list of files in the root folder. The assumption
	 * is that the incoming files will be placed in this folder
	 * 
	 * @return A java.util.List of GHIXFTPFile objects
	 * @throws Exception
	 */
	public List<GHIXFTPFile> getFileList() throws Exception {
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		initConnection();
		LOGGER.info("initConnection completes");
		List<GHIXFTPFile> fileList = new ArrayList<GHIXFTPFile>();
		try {

			FTPFile[] ftpFiles = client.listFiles();
			for (FTPFile ftpFile : ftpFiles) {
				if ((ftpFile.getType() == FTPFile.FILE_TYPE)
						&& (!ftpFile.isDirectory())) {
					fileList.add(new GHIXFTPFile(ftpFile.getName(), ftpFile
							.getSize(), ftpFile.getTimestamp().getTime()
							.toString()));
				}
			}
			return fileList;
		} catch (Exception ex) {
			LOGGER.error("Error occured while getting file list",ex);
			throw ex;
		}

	}

	/**
	 * This method takes a remoteFileName and it will get the byte content for
	 * the file. The file is searched in the root folder of the FTP Server for
	 * the login account
	 * 
	 * @param remoteFileName
	 * @return byte[] content of the file
	 * @throws Exception
	 */
	public byte[] getFileContent(String remoteFileName) throws Exception {
		LOGGER.info("Getting file content for " + remoteFileName);
		
		InputStream in = null;
		BufferedInputStream inbf = null;
		try {
			initConnection();
			LOGGER.info("Connection establised. Client = " + client);
			LOGGER.info("Connection establised. Client = " + client);
			in = client.retrieveFileStream(remoteFileName);
			//LOGGER.info("Input stream is " + in);
			inbf = new BufferedInputStream(in);
			byte buffer[] = new byte[BUFFER_SIZE];
			int readCount;
			String result = null;
			StringBuffer resultString = new StringBuffer();
			while ((readCount = inbf.read(buffer)) > 0) {
				resultString.append(new String(buffer, 0, readCount));
			}
			result = resultString.toString();
			LOGGER.info("File content is:\n" + result);
			client.completePendingCommand();
			return result.getBytes();
		} catch (Exception ex) {
			LOGGER.info("Exception while reading file");
			throw ex;
		}
		finally {
			try {
				if (inbf != null){
					inbf.close();
				}
				if (in != null){
					in.close();
				}
			} catch (Exception e) {
				LOGGER.info("Exception while closing steams");
				throw e;
			}
		}
	}

	/**
	 * This method allows to move a file into any remote folder within the root
	 * folder and a target filename. For e.g. you can move a file named
	 * file1.txt and move it inside of a /success folder with the name
	 * file1-123.txt
	 * 
	 * @param remoteFileName
	 *            This is the original file name that you want to move
	 * @param remoteTargetFolder
	 *            This is the target folder that you want the above file to be
	 *            moved. This folder will be created inside of the FTP root
	 *            folder if it is not present
	 * @param remoteTargetFileName
	 *            This is the target file name
	 * @throws Exception
	 */
	public void moveFile(String sourceFolder, String sourceFileName, String remoteTargetFolder,
			String remoteTargetFileName) throws Exception {
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		boolean renameSuccessful = false;
		
		initConnection();
		// boolean success =
		client.makeDirectory(remoteTargetFolder);
		// if (!success) showServerReply(client); //OPTIONAL
		String sourceFile = sourceFolder+"/"+sourceFileName;
		String targetFile = remoteTargetFolder + "/"
				+ remoteTargetFileName;
		renameSuccessful = client.rename(sourceFile, targetFile);
		showServerReply(client);
		if(renameSuccessful && StringUtils.isNotBlank(sourceFolder)){
			client.removeDirectory(sourceFolder);
			showServerReply(client);
		}
		
	}

	/**
	 * This is a utility method to simply provide move a file inside of either
	 * the success, inprocessing or errors folders. All you need to do is give
	 * the filename and which of three status you want to mark for the file. The
	 * code will then move the file into that folder. If the folder is not
	 * present, it is created.
	 * 
	 * @param remoteFileName
	 *            The filename that you wish to move
	 * @param processing
	 *            One of the enum values for PROCESSING_STATUS i.e.
	 *            PROCESSING_STATUS.PROCESSING or PROCESSING_STATUS.ERROR or
	 *            PROCESSING_STATUS.ERROR
	 * @throws Exception
	 */
	public void updateFileStatus(String remoteFileName,
			PROCESSING_STATUS processing) throws Exception {
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		
			initConnection();
			String targetFileName = "";
			if (processing.equals(PROCESSING_STATUS.ERROR)) {
				// Create folder if not present
				boolean success = client.makeDirectory(ERRORS_FOLDER);
				// We could check the status here but we will leave it for now
				// assuming that rights are provided
				if (!success) {
					showServerReply(client);
				}
				targetFileName = ERRORS_FOLDER + "/" + remoteFileName;
			} else if (processing.equals(PROCESSING_STATUS.SUCCESS)) {
				// Create folder if not present
				boolean success = client.makeDirectory(SUCCESS_FOLDER);
				// We could check the status here but we will leave it for now
				// assuming that rights are provided
				if (!success) {
					showServerReply(client);
				}
				targetFileName = SUCCESS_FOLDER + "/" + remoteFileName;
			} else if (processing.equals(PROCESSING_STATUS.PROCESSING)) {
				// Create folder if not present
				boolean success = client.makeDirectory(INPROCESS_FOLDER);
				// We could check the status here but we will leave it for now
				// assuming that rights are provided
				if (!success) {
					showServerReply(client);
				}
				targetFileName = INPROCESS_FOLDER + "/" + remoteFileName;
			}
			client.rename(remoteFileName, targetFileName);
			showServerReply(client);
	}

	/**
	 * Utility method to show the FTP Server response. This can be used anytime
	 * to retrieve the last FTP Command status via the FTPClient object
	 * 
	 * @param ftpClient
	 */
	private void showServerReply(FTPClient ftpClient) {
		String[] replies = ftpClient.getReplyStrings();
		if (replies != null && replies.length > 0) {
			for (String aReply : replies) {
				LOGGER.info("SERVER: " + aReply);
			}
		}
	}
	
	/**
	 * Method is used to create directory to remote location.
	 * @param remoteFolserName
	 * @throws Exception
	 */
	public void createDirectory(String remoteFolserName)
			throws Exception {

		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		
		initConnection();
		client.makeDirectory(remoteFolserName);
	
	}
	
	/**
	 * Method to upload file from local to remote location
	 * 
	 * @param args
	 * @throws Exception
	 */
	public void uplodadFile(InputStream localFile, String remoteFileName) throws Exception{
		
		if (!bInited) {
			throw new Exception(INVALID_FTP_CLIENT);
		}
		
		initConnection();
		client.storeFile(remoteFileName, localFile);
	
	}

	/**
	 * Main method for demonstrating Test Usage
	 * 
	 * @param args
	 * @throws Exception
	 */
	/*
	public static void main(String[] args) throws Exception {
		GHIXFTPClient client = new GHIXFTPClient("74.205.108.146", "csruser",
				"GHIX123#");
		// Get List of Files
		List<GHIXFTPFile> files = client.getFileList();
		for (GHIXFTPFile file : files) {
			// Print out File Details
			LOGGER.info(file.getName() + "," + file.getSize() + ","
					+ file.getTimeStamp());
			// Get the File Contents
			byte[] fileContents = client.getFileContent(file.getName());
			LOGGER.info(new String(fileContents));

			// Various ways to move a file

			// Move the file to a folder name "test-123" and name the moved
			// files differently
			// _CLIENT.moveFile(file.getName(), "test-123", "ID-123-" +
			// file.getName());

			// Move the file to /processing folder
			// _CLIENT.updateFileStatus(file.getName(),
			// GHIXFTPClient.PROCESSING_STATUS.PROCESSING);

			// Move the file to the /success folder
			// _CLIENT.updateFileStatus(file.getName(),
			// GHIXFTPClient.PROCESSING_STATUS.SUCCESS);

			// Move the file to the /errors folder
			// _CLIENT.updateFileStatus(file.getName(),
			// GHIXFTPClient.PROCESSING_STATUS.ERROR);
		}

	}
	*/
}
