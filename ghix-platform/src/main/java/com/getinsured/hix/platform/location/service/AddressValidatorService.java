package com.getinsured.hix.platform.location.service;

import com.getinsured.hix.model.AddressValidatorResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;

/**
 * This service is used to validate the given address.
 * 
 * @author polimetla_b
 * @since 11/1/2012
 */
public interface AddressValidatorService {

	/**
	 * For given Address/Location, it validates and returns exact address or
	 * similar addresses. If the given address is wrong, it returns empty data.
	 * 
	 * @param Location
	 * @return
	 */
	public AddressValidatorResponse validateAddress(Location address);
	
	public AddressValidationResponse validateAddress(LocationDTO address) throws Exception;
	
	
}
