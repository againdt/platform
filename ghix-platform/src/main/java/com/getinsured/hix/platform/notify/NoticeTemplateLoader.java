package com.getinsured.hix.platform.notify;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notification.NoticeTmplHelper;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.timeshift.TimeShifterUtil;

import freemarker.cache.TemplateLoader;

class NoticeTemplateSource {
	private NoticeType noticeType;
	private TemplateSourceWrapper templateSource;
	public NoticeType getNoticeType() {
		return noticeType;
	}
	public void setNoticeType(NoticeType noticeType) {
		this.noticeType = noticeType;
	}
	public TemplateSourceWrapper getTemplateSource() {
		return templateSource;
	}
	public void setTemplateSource(TemplateSourceWrapper templateSource) {
		this.templateSource = templateSource;
	}
	
}

@Component
public class NoticeTemplateLoader implements TemplateLoader {
	Logger LOGGER = LoggerFactory.getLogger(NoticeTemplateLoader.class);

	@Autowired private NoticeTypeRepository noticeTypeRepo;
	
	private Map<String, NoticeTemplateSource> availableNotices = Collections.synchronizedMap(new HashMap<String, NoticeTemplateSource>(10));
	
	@PostConstruct
	public void init(){
		List<NoticeType> allNotices = this.noticeTypeRepo.findAll();
		for(NoticeType noticeType: allNotices){
			NoticeTemplateSource nts = new NoticeTemplateSource();
			nts.setNoticeType(noticeType);
			nts.setTemplateSource(null);
			this.availableNotices.put(noticeType.getEmailClass(), nts);
		}
	}

	private static Map<String, String> contentIdmap = Collections.synchronizedMap(new HashMap<String, String>());

	@Override
	public void closeTemplateSource(Object templateSource) throws IOException {
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Closing source:"+templateSource.getClass().getName());
		}
		TemplateSourceWrapper notice = (TemplateSourceWrapper)templateSource;
		Reader reader = notice.getSourceReader();
		if(reader != null){
			reader.close();
		}
		
	}

	@Override
	public Object findTemplateSource(String name) throws IOException {
		if("N".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))){
			LOGGER.info("ECM Not enabled, falling back on local resources for path:"+name);
			return null;
		}
		TemplateSourceWrapper wrapper = null;
		LOGGER.debug("Trying to find the template for "+name);
		NoticeTemplateSource nts = this.availableNotices.get(name);
		if(nts != null){
			LOGGER.info("Resource found:"+name);
			if(nts.getTemplateSource() == null){
				wrapper = new TemplateSourceWrapper(nts.getNoticeType());
				nts.setTemplateSource(wrapper);
			}else{
				wrapper = nts.getTemplateSource();
			}
			wrapper.setPath(name);
		}
		if(wrapper == null){
			LOGGER.warn("Failed to find the template source for "+name+" Check the database");
		}
		return wrapper;
	}

	/**
	 * Not sure of templates are always updated using the DB or direct push to the ECM, 
	 * This will take care of both, if template gets updated using the application, updated time
	 * will change and otherwise, wrapper will force the refresh after every one hour by returning
	 * a modified time as current time.
	 */
	@Override
	public long getLastModified(Object templateSource) {
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper)templateSource;
		NoticeType notice = (NoticeType)wrapper.getTemplateSource();
		if(wrapper.shouldRefreshTheSource()){
			contentIdmap.remove(wrapper.getPath());// Remove the stale entry, next call to findTemplateSource will result in template refresh.
			this.init(); //Refresh the data from the repository as well, just in case something changed.
			return TimeShifterUtil.currentTimeMillis();
		}
		return notice.getUpdated().getTime();
	}

	@Override
	public Reader getReader(Object templateSource, String encoding) throws IOException {
		Exception ex = null;
		InputStreamReader reader = null;
		TemplateSourceWrapper wrapper = (TemplateSourceWrapper)templateSource;
		NoticeType notice = (NoticeType)wrapper.getTemplateSource();
		try {
			reader = new InputStreamReader(getTemplateFromECM(notice));
			wrapper.setSourceReader(reader);
		} catch (ContentManagementServiceException | NotificationTypeNotFound e) {
			ex = e;
		}
		if(ex != null){
			throw new IOException(ex.getMessage(), ex);
		}
		LOGGER.debug("Template reader provided");
		return reader;
	}
	
	private InputStream getTemplateFromECM(NoticeType noticeType) throws ContentManagementServiceException,NotificationTypeNotFound {
		LOGGER.debug("Extracting Template from ECM");
		String ecmContentId = null;
		String ecmTemplateFolderPath = null;
		ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH);
		ecmTemplateFolderPath += noticeType.getTemplateLocation();
		ecmContentId = this.getEcmContentId(ecmTemplateFolderPath,noticeType);
		if(ecmContentId == null){
			throw new ContentManagementServiceException("No content Id found for "+noticeType.getTemplateLocation()+" with Name:"+noticeType.getNotificationName()+" Global ECM folder path used:"+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH));
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Creating stream for reading the content for Id:"+ecmContentId);
		}
		return new ByteArrayInputStream(noticeTmplHelper.readBytesByEcmId(ecmContentId));
	}
	
	
	@Autowired private NoticeTmplHelper noticeTmplHelper;
	
	private String getEcmContentId(String templatePath,NoticeType noticeType) throws ContentManagementServiceException {
		LOGGER.debug("Extracting content Id from ECM for path "+templatePath);
		String contentId = contentIdmap.get(templatePath);
		if(contentId != null){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Retrieved content Id from the local content Map");
			}
			return contentId;
		}
		// save the content id to DB
		Content content = null;
		try{
			content=noticeTmplHelper.readContentMetaByPath(templatePath);
		}catch(ContentManagementServiceException cme){
			LOGGER.warn("Path:"+templatePath+" does not exist in ECM");
			return null;
		}
		contentId = content.getContentId();
		if(LOGGER.isDebugEnabled() && contentId != null){
			LOGGER.debug("Retrieved content Id ["+contentId+"] ECM, Caching locally");
		}
		if(contentId == null){
			LOGGER.error("No content Id found for path:"+templatePath);
			return null;
		}
		contentIdmap.put(templatePath, contentId);
		return contentId;
	}

	public NoticeType getNoticeType(String name) {
		NoticeType noticeType = null;
		NoticeTemplateSource nts = this.availableNotices.get(name);
		if(nts != null){
			noticeType = nts.getNoticeType();
		}else{
			LOGGER.error("No Notice type available for name:"+name+" Please check the DB entry for "+name+" mapping to a notice template, trying to refresh the DB info");
			return refreshAndAttemptAgain(name);
		}
		return noticeType;
	}

	private NoticeType refreshAndAttemptAgain(String name) {
		init();
		NoticeType noticeType = null;
		NoticeTemplateSource nts = this.availableNotices.get(name);
		if(nts != null){
			noticeType = nts.getNoticeType();
		}else{
			LOGGER.error("No Notice type available for name:"+name+" Attempted DB refresh, Please check the DB entry for "+name+" mapping to a notice template");
		}
		return noticeType;
	}
}
