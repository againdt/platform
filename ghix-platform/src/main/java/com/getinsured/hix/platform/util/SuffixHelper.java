package com.getinsured.hix.platform.util;

import java.util.ArrayList;
import java.util.List;

public class SuffixHelper {

	private List<String> suffixes=null;
	
	public SuffixHelper(){
		suffixes = new ArrayList<String>(8);
		suffixes.add("Jr");
		suffixes.add("Sr");
		suffixes.add("I");
		suffixes.add("II");
		suffixes.add("III");
		suffixes.add("IV");
		suffixes.add("V");
		suffixes.add("VI");
	}
	
	public List<String> getAllSuffixes(){
		return suffixes;
	}
}
