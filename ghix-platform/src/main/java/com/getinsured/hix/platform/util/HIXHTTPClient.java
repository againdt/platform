package com.getinsured.hix.platform.util;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * GetInsurance's implementation of HTTP Client.
 * <p>
 *  Uses {@link SecureHttpClient} for underlying communication, which
 *  in turn is based on Apache's {@link CloseableHttpClient}.
 * </p>
 * <p>
 *   It's worth noting that this implementation will return successfull responses if
 *   eventual remote server response status code is between 200 and 300.
 * </p>
 */
public class HIXHTTPClient
{
  private static final Logger log = LoggerFactory.getLogger(HIXHTTPClient.class);

  private static CloseableHttpClient httpClient = null;
  private static ResponseHandler<String> handler = null;

  private WebServiceTemplate webServiceTemplate;

  private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
  private static final Map<String, ContentType> CONTENT_TYPES = new HashMap<>(12);

  static
  {
    CONTENT_TYPES.put("application/atom+xml", ContentType.create("application/atom+xml", UTF8_CHARSET));
    CONTENT_TYPES.put("application/x-www-form-urlencoded", ContentType.create("application/x-www-form-urlencoded", UTF8_CHARSET));
    CONTENT_TYPES.put("application/json", ContentType.create("application/json", UTF8_CHARSET));
    CONTENT_TYPES.put("application/octet-stream", ContentType.create("application/octet-stream", UTF8_CHARSET));
    CONTENT_TYPES.put("application/svg+xml", ContentType.create("application/svg+xml", UTF8_CHARSET));
    CONTENT_TYPES.put("application/xhtml+xml", ContentType.create("application/xhtml+xml", UTF8_CHARSET));
    CONTENT_TYPES.put("application/xml", ContentType.create("application/xml", UTF8_CHARSET));
    CONTENT_TYPES.put("multipart/form-data", ContentType.create("multipart/form-data", UTF8_CHARSET));
    CONTENT_TYPES.put("text/html", ContentType.create("text/html", UTF8_CHARSET));
    CONTENT_TYPES.put("text/plain", ContentType.create("text/plain", UTF8_CHARSET));
    CONTENT_TYPES.put("text/xml", ContentType.create("text/xml", UTF8_CHARSET));
    CONTENT_TYPES.put("*/*", ContentType.create("*/*", UTF8_CHARSET));
  }

  /**
   * Gets {@link ContentType} object from locally initialized list, or creates
   * new on if it's not in local cache.
   *
   * @param contentType String representation of content type, e.g. {@code applicaiton/json}
   * @return {@link ContentType} object created from the given string with UTF8 charset encoding.
   * @see ContentType#create(String, Charset)
   */
  private ContentType getContentType(final String contentType)
  {
    if (log.isDebugEnabled())
    {
      log.debug("Returning content type for: {}", contentType);
    }

    return CONTENT_TYPES.getOrDefault(contentType, ContentType.create(contentType, UTF8_CHARSET));
  }

  /**
   * Returns instance of {@link CloseableHttpClient}.
   *
   * @return local static instance of {@link CloseableHttpClient}
   */
  public CloseableHttpClient getHttpClient()
  {
    if (httpClient == null)
    {
      httpClient = SecureHttpClient.getHttpClient();
    }

    return httpClient;
  }

  /**
   * Checks for response status code to be between 200 and 300,
   * otherwise throws {@link ClientProtocolException}
   *
   * @return response if remote server responded with status code between 200 and 300.
   */
  private ResponseHandler<String> getResponseHandler()
  {
    if (handler != null)
    {
      return handler;
    }

    handler = response ->
    {
      int status = response.getStatusLine().getStatusCode();

      if (status >= 200 && status < 300)
      {
        HttpEntity entity = response.getEntity();
        return entity != null ? EntityUtils.toString(entity) : null;
      } else
      {
        throw new ClientProtocolException("Unexpected response status (expecting 200 <=> 300): " + status + "; Reason: " + response.getStatusLine().getReasonPhrase());
      }
    };

    return handler;
  }

  /**
   * Issues a {@code POST} to given {@code url} with specified parameters, content type and wait time.
   *
   * @param url URL to POST to.
   * @param postParams parameters to pass.
   * @param contentType content type to pass.
   * @param waitTime wait time.
   * @return Response from remote server if successful.
   * @throws GIRuntimeException if exception occurred.
   */
  public String getPOSTDataWithWaitTime(final String url, final String postParams,
                                        final String contentType, int waitTime)
      throws GIRuntimeException, ClientProtocolException, IOException
  {
    if (log.isDebugEnabled())
    {
      log.debug("getPOSTDataWithWaitTime: url: {}, postParams: {}, contentType: {}, waitTime: {}", url, postParams, contentType, waitTime);
    }

    HttpEntity requestEntity = null;
    String httpResponse = null;
    Exception ex = null;

    requestEntity = EntityBuilder.create().setContentType(getContentType(contentType)).setText(postParams).build();

    HttpPost httppost = null;

    try
    {
      httppost = new HttpPost(url);
      RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(waitTime).setConnectTimeout(waitTime).setSocketTimeout(waitTime).build();
      httppost.setConfig(config);

      httppost.setEntity(requestEntity);
      httpResponse = getHttpClient().execute(httppost, this.getResponseHandler());
    } catch (Exception rex)
    {
      if (httppost != null)
      {
        httppost.abort();
      }

      ex = rex;
    } finally
    {
      if (httppost != null)
      {
        httppost.releaseConnection();
      }
    }

    if (ex != null)
    {
      if (log.isErrorEnabled())
      {
        log.error("Error executing POST for URI: {}: {}", url, ex.getMessage());
      }

      throw new GIRuntimeException("Error executing POST for URI:" + url, ex);
    }

    return httpResponse;
  }

  /**
   * Issues a {@code GET} to given {@code url} with specified wait time.
   *
   * @param url URL to GET.
   * @param waitTime wait time.
   * @return Response from remote server if successful.
   * @throws GIRuntimeException if exception occurred.
   */
  public String getDataWithWaitTime(final String url, int waitTime)
      throws GIRuntimeException, ClientProtocolException, IOException
  {
    if (log.isDebugEnabled())
    {
      log.debug("getDataWithWaitTime: url: {}, waitTime: {}", url, waitTime);
    }

    String httpResponse = null;
    Exception ex = null;

    HttpGet httpget = null;

    try
    {
      httpget = new HttpGet(url);
      RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(waitTime).setConnectTimeout(waitTime).setSocketTimeout(waitTime).build();
      httpget.setConfig(config);
      httpResponse = getHttpClient().execute(httpget, this.getResponseHandler());
    } catch (Exception rex)
    {
      if (httpget != null)
      {
        httpget.abort();
      }

      ex = rex;
    } finally
    {
      if (httpget != null)
      {
        httpget.releaseConnection();
      }
    }

    if (ex != null)
    {
      if (log.isErrorEnabled())
      {
        log.error("Error executing GET with wait time of {} for URI: {}: {}", waitTime, url, ex.getMessage());
      }

      throw new GIRuntimeException("Error executing POST for URI:" + url, ex);
    }

    return httpResponse;
  }

  /**
   * Issues {@code POST} to given {@code url}.
   *
   * @param url URL to POST to
   * @param postParams parameters to post
   * @param contentType content type to use
   * @return Servers response as String
   * @throws GIRuntimeException if any exception occurred.
   */
  public String getPOSTData(final String url, final String postParams, final String contentType)
      throws GIRuntimeException, ClientProtocolException, IOException
  {
    if (log.isDebugEnabled())
    {
      log.debug("getPOSTData: url: {}, postParams: {}, contentType: {}", url, postParams, contentType);
    }

    HttpEntity requestEntity = null;
    String httpResponse = null;
    Exception ex = null;

    if (httpClient == null)
    {
      httpClient = getHttpClient();
    }

    requestEntity = EntityBuilder.create().setContentType(getContentType(contentType)).setText(postParams).build();

    HttpPost httppost = null;

    try
    {
      httppost = new HttpPost(url);
      httppost.setEntity(requestEntity);
      httpResponse = getHttpClient().execute(httppost, this.getResponseHandler());
    } catch (SocketTimeoutException ste)
    {
      if (httppost != null)
      {
        httppost.abort();
      }

      ex = ste;
    } catch (Exception rex)
    {
      if (httppost != null)
      {
        httppost.abort();
      }

      ex = rex;
    } finally
    {
      if (httppost != null)
      {
        httppost.releaseConnection();
      }
    }

    if (ex != null)
    {
      if (log.isErrorEnabled())
      {
        log.error("Error executing GET: url: {}, postParams: {}, contentType: {}", url, postParams, contentType);
      }

      throw new GIRuntimeException("Error executing POST for URI:" + url, ex);
    }

    return httpResponse;
  }

  /**
   * Issues {@code POST} to a given {@code url} as a form data.
   *
   * @param url url to post.
   * @param headers headers, could be null.
   * @param postParams form parameter/value pairs, cannot be null.
   * @return Server response as String.
   * @throws GIRuntimeException if any exception occurred or {@code postParams} == null || 0 in size.
   */
  public String postFormData(final String url,
                             final HashMap<String, String> headers,
                             final HashMap<String, String> postParams)
      throws GIRuntimeException, ClientProtocolException, IOException
  {
    if (log.isDebugEnabled())
    {
      log.debug("postFormData: url: {}", url);

      if (headers != null)
      {
        headers.forEach((k, v) ->
        {
          log.debug("HEADER[{}] => [{}]", k, v);
        });
      }

      if (postParams != null)
      {
        postParams.forEach((k, v) ->
        {
          log.debug("PARAM[{}] => [{}]", k, v);
        });
      }
    }

    String httpResponse = null;
    Exception ex = null;

    if (postParams != null && postParams.size() > 0)
    {
      List<NameValuePair> parameters = new ArrayList<>(postParams.size());

      postParams.forEach((k, v) ->
      {
        parameters.add(new BasicNameValuePair(k, v));
      });

      HttpPost httppost = null;

      try
      {
        httppost = new HttpPost(url);

        if (headers != null)
        {
          headers.forEach(httppost::addHeader);
        }

        httppost.setEntity(new UrlEncodedFormEntity(parameters));
        httpResponse = getHttpClient().execute(httppost, this.getResponseHandler());
      } catch (Exception rex)
      {
        if (httppost != null)
        {
          httppost.abort();
        }
        ex = rex;
      } finally
      {
        if (httppost != null)
        {
          httppost.releaseConnection();
        }
      }

      if (ex != null)
      {
        throw new GIRuntimeException("Error executing POST for URI:" + url, ex);
      }

      return httpResponse;
    }
    else {
      throw new GIRuntimeException("Nothing to post, no form data provided");
    }
  }

  /**
   * Issues {@code GET} to a given {@code url} with given headers
   *
   * @param url url to GET.
   * @param headers headers, could be null.
   * @return Server response as String.
   * @throws GIRuntimeException if any exception occurred.
   */
  public String getDataWithHeaders(final String url,
                                   final HashMap<String, String> headers)
      throws GIRuntimeException, ClientProtocolException, IOException
  {
    if (log.isDebugEnabled())
    {
      log.debug("postFormData: url: {}", url);

      if (headers != null)
      {
        headers.forEach((k, v) ->
        {
          log.debug("HEADER[{}] => [{}]", k, v);
        });
      }
    }

    String httpResponse = null;
    Exception ex = null;

    HttpGet httpget = null;

    try
    {
      httpget = new HttpGet(url);

      if (headers != null)
      {
        headers.forEach(httpget::addHeader);
      }

      httpResponse = getHttpClient().execute(httpget, this.getResponseHandler());
    } catch (Exception rex)
    {
      if (httpget != null)
      {
        httpget.abort();
      }
      ex = rex;
    } finally
    {
      if (httpget != null)
      {
        httpget.releaseConnection();
      }
    }

    if (ex != null)
    {
      throw new GIRuntimeException("Error executing GET for URI:" + url, ex);
    }

    return httpResponse;
  }

  /**
   * Uses GET to fetch data from given URL. Due to issues in underlying API,
   * we are not assembling parameters as on 11/1/2012
   *
   * @param url
   */
  public String getGETData(final String url) throws Exception
  {
    if (log.isDebugEnabled())
    {
      log.debug("getGETData: url: {}", url);
    }

    HttpGet httpGet = null;
    String httpResponse = null;
    Exception ex = null;

    try
    {
      httpGet = new HttpGet(url);
      httpResponse = getHttpClient().execute(httpGet, this.getResponseHandler());
    } catch (Exception e)
    {
      if (httpGet != null)
      {
        httpGet.abort();
      }

      ex = e;
    } finally
    {
      if (httpGet != null)
      {
        httpGet.releaseConnection();
      }
    }

    if (ex != null)
    {
      throw ex;
    }

    return httpResponse;
  }

  public WebServiceTemplate getWebServiceTemplate()
  {
    return webServiceTemplate;
  }

  public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate)
  {
    this.webServiceTemplate = webServiceTemplate;
  }
}
