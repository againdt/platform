package com.getinsured.hix.platform.clamav;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

public class ClamAVConnectionPool extends GenericObjectPool<ClamConnection> {

	public ClamAVConnectionPool(PooledObjectFactory<ClamConnection> factory) {
		super(factory);
	}
	
	public ClamAVConnectionPool(PooledObjectFactory<ClamConnection> factory,
            GenericObjectPoolConfig config) {
        super(factory, config);
    }

}
