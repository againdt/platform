package com.getinsured.hix.platform.external.cloud.service;

import static com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum.CDNURL;
import static com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum.CDN_ACTIVE_CONTAINER;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
@DependsOn("dynamicPropertiesUtil")
class RackspaceCloud extends AbstractStrategy{

	private static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";

	private static final String ALLOW_ANY_ORIGIN = "*";

	private static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";

	private static final Logger LOGGER = LoggerFactory.getLogger(RackspaceCloud.class);

	private static final String OAUTH_TOKEN = "X-Auth-Token";
	private static final String RACKSPACE_RESPONOSE_IS_NULL = "Rackspace responose is null";
	private static final String SEPARATOR = "/";
	private static final String RESOURCES = "/resources/";
	private static final String UNDEFINED_IN_GI_APP_CONFIG = " undefined in gi_app_config";
	private static final String TIMESTAMP_FORMAT = "yyyy-MM-ddHH:mm:SS";
	private static final String UNABLE_TO_UPLOAD_RESOURCE_TO_RACKSPACE = "Unable to upload resource to rackspace - ";

	@Value("#{configProp['cdn.server.username']}")
	private String cdnUserName;

	@Value("#{configProp['cdn.server.apiKey']}")
	private String cdnApiKey;

	@Value("#{configProp['cdn.server.region']}")
	private String cdnRegion;

	/*	@PostConstruct
	public void validate(){
		if (StringUtils.isEmpty(cdnUserName) || StringUtils.isEmpty(cdnApiKey) || StringUtils.isEmpty(cdnRegion)){
			throw new IllegalArgumentException(RACKSPACE_PARAMETER_IN_CONFIGURATION_PROPERTIES); 
		}
		
		readCdnActiveContainer();
		readCdnUrl();
	}*/

	@Autowired private RestTemplate restTemplate;

	@Override
	public String uploadResource(String relativePath, String fileName, byte[] dataBytes){
		return pushToCDN(relativePath, fileName, dataBytes);
	}	

	private String pushToCDN(String relativePath, String fileName, byte[] dataBytes) {
		String cdnActiveContianer = readCdnActiveContainer();
		String cdnUrl = readCdnUrl();

		String mimeType = ExternalCloudHelper.validateIncomingContent(fileName, dataBytes);
		
		/*List<String> tokenData = getXAuthToken();*/
		/* Get Rackspace OAuthToken and Public URL */
		Rackspace rackspace = new Rackspace();

		String resourceUrl = formResourceUrl(relativePath, fileName, DateFormatUtils.format(new TSDate(), TIMESTAMP_FORMAT));

		try {

			String targetUrl = formTargetUrl(cdnActiveContianer, rackspace.publicUrl, resourceUrl);
			HttpEntity<byte[]> requestEntity = formRequestEntity(dataBytes, rackspace.oAuthToken, mimeType);

			HttpEntity<String> response = restTemplate.exchange(targetUrl, HttpMethod.PUT, requestEntity, String.class);

			if (response == null){
				LOGGER.error(RACKSPACE_RESPONOSE_IS_NULL);
				throw new GIRuntimeException(RACKSPACE_RESPONOSE_IS_NULL);
			}
		} catch (Exception e) {
			LOGGER.error(UNABLE_TO_UPLOAD_RESOURCE_TO_RACKSPACE,e);
			throw new GIRuntimeException(UNABLE_TO_UPLOAD_RESOURCE_TO_RACKSPACE, e);
		}

		return cdnUrl + resourceUrl;
	}

	private HttpEntity<byte[]> formRequestEntity(byte[] dataBytes, String oAuthToken, String mimeType) {

		/*MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("file", dataBytes);*/

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.valueOf(mimeType));
		requestHeaders.setContentLength(dataBytes.length);
		requestHeaders.add(OAUTH_TOKEN, oAuthToken);
		requestHeaders.add(ACCESS_CONTROL_ALLOW_ORIGIN, ALLOW_ANY_ORIGIN);
		requestHeaders.add(ACCESS_CONTROL_EXPOSE_HEADERS, ACCESS_CONTROL_ALLOW_ORIGIN);

		HttpEntity<byte[]> requestEntity= new HttpEntity<byte[]>(dataBytes,requestHeaders);
		return requestEntity;
	}

	private String formTargetUrl(String cdnActiveContianer, String publicURL, String resourceUrl) {
		return new StringBuilder().append(publicURL).append(SEPARATOR)
				.append(cdnActiveContianer).append(resourceUrl)
				.toString();
	}

	private String formResourceUrl(String relativePath, String fileName, String currentTime) {
		return new StringBuilder().append(RESOURCES).append(relativePath)
				.append(SEPARATOR).append(currentTime)
				.append(SEPARATOR).append(fileName)
				.toString();
	}

	private String readCdnUrl() {
		String cdnUrl = DynamicPropertiesUtil.getPropertyValue(CDNURL.getValue());
		if (StringUtils.isEmpty(cdnUrl)){
			throw new GIRuntimeException(CDNURL.getValue() + UNDEFINED_IN_GI_APP_CONFIG);
		}
		return cdnUrl;
	}

	private String readCdnActiveContainer() {
		String cdnActiveContainer = DynamicPropertiesUtil.getPropertyValue(CDN_ACTIVE_CONTAINER.getValue());
		if (StringUtils.isEmpty(cdnActiveContainer)){
			throw new GIRuntimeException(CDN_ACTIVE_CONTAINER.getValue() + UNDEFINED_IN_GI_APP_CONFIG);
		}
		return cdnActiveContainer;
	}


	private class Rackspace {

		private static final String ERROR_FINDING_O_AUTH_TOKEN_FOR_RACKSPACE = "Error finding OAuth Token for Rackspace.";
		private static final String PUBLIC_URL = "publicURL";
		private static final String REGION = "region";
		private static final String ENDPOINTS = "endpoints";
		private static final String NAME = "name";
		private static final String CLOUD_FILES = "cloudFiles";
		private static final String SERVICE_CATALOG = "serviceCatalog";
		private static final String ID = "id";
		private static final String TOKEN = "token";
		private static final String ACCESS = "access";
		private static final String RACKSPACECLOUD_URL = "https://identity.api.rackspacecloud.com/v2.0/tokens";
		private static final String AUTH = "auth";
		private static final String RAX_KSKEY_API_KEY_CREDENTIALS = "RAX-KSKEY:apiKeyCredentials";
		private static final String API_KEY = "apiKey";
		private static final String USERNAME = "username";

		private String oAuthToken;
		private String publicUrl;

		private Rackspace() {
			List<String> tokenData = getXAuthToken();

			if (tokenData == null || tokenData.size() != 2) {
				throw new GIRuntimeException(ERROR_FINDING_O_AUTH_TOKEN_FOR_RACKSPACE);
			} else {
				oAuthToken = tokenData.get(0);
				publicUrl = tokenData.get(1);
			}
		}

		private List<String> getXAuthToken()  {

			List<String> accessDataList = new ArrayList<String>();
			try {

				ObjectMapper mapper = new ObjectMapper();
				ObjectNode credentialsNode = mapper.createObjectNode();
				credentialsNode.put(USERNAME, cdnUserName);
				credentialsNode.put(API_KEY, cdnApiKey);

				JsonNode keyNode = mapper.createObjectNode().set(RAX_KSKEY_API_KEY_CREDENTIALS,  credentialsNode);
				JsonNode actualObj = mapper.createObjectNode().set(AUTH, keyNode);

				String response = restTemplate.postForObject(RACKSPACECLOUD_URL, actualObj, String.class);

				JsonNode jsonNode = mapper.readTree(response);
				String oAuthToken = jsonNode.get(ACCESS).get(TOKEN).get(ID).asText();
				accessDataList.add(oAuthToken);	// Index 0 contains the Token
				// Index 1 contains the Regions public URL 
				JsonNode services = jsonNode.get(ACCESS).get(SERVICE_CATALOG);
				Iterator<JsonNode> ite = services.elements();
				while (ite.hasNext()) {
					JsonNode temp = ite.next();
					if (temp.get(NAME).asText().equalsIgnoreCase(CLOUD_FILES)) {
						Iterator<JsonNode> endpointNodes = temp.get(ENDPOINTS).elements();
						while (endpointNodes.hasNext()) {
							JsonNode endpoint = endpointNodes.next();
							if (endpoint.get(REGION).asText().equalsIgnoreCase(cdnRegion)) {
								String publicUrlforRegion = endpoint.get(PUBLIC_URL).asText();
								accessDataList.add(publicUrlforRegion); // Index 1 contains the Regions public URL 
								return accessDataList;
							}
						}

					}
				}
			} catch (Exception e) {
				throw new GIRuntimeException(ERROR_FINDING_O_AUTH_TOKEN_FOR_RACKSPACE, e);
			} 
			
			return accessDataList;
		}

	}


}
