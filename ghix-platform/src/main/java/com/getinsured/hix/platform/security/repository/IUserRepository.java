package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

public interface IUserRepository extends TenantAwareRepository<AccountUser, Integer>, RevisionRepository<AccountUser, Integer, Integer> {
	
	@Override
	List<AccountUser> findAll();
	List<AccountUser> findByUuid(String uuid); // HIX-32108 : Added to support unique salt
	List<AccountUser> findByUuidIsNull(); // HIX-32108 : Added to support unique salt
	List<AccountUser> findByUuidIsNullAndUserNameStartingWithOrderByUserNameDesc(String userNamePattern);
	
	AccountUser findByEmail(String email);
	
	AccountUser findById(int id);
	
	AccountUser findByExtnAppUserId(String extAppUserId);

	AccountUser findByRecovery(String recovery);
	
	AccountUser findByUserName(String userName);
	
	AccountUser findByUserNameAndEmail(String userName, String email);

	@Query("SELECT new AccountUser(accountUser.id, accountUser.userName, accountUser.firstName, accountUser.lastName, accountUser.securityAnswer1, accountUser.securityAnswer2,  accountUser.lastLogin) from AccountUser as accountUser WHERE accountUser.id = :userId")
	AccountUser getUserBasicInfo(@Param("userId") int userId);

	@Query("SELECT concat(u.firstName,' ',u.lastName) FROM AccountUser u WHERE u.id = :userId")
	String getUserNameById(@Param("userId") int userId);
	
	@Query("SELECT new AccountUser(accountUser.id, accountUser.userName, accountUser.firstName, accountUser.lastName, accountUser.phone) FROM AccountUser as accountUser WHERE accountUser.id in (:userIds)")
	List<AccountUser> getUserNameAndPhoneByIds(@Param("userIds") List<Integer> userIds);

	@Query("SELECT new AccountUser(accountUser.id, accountUser.firstName, accountUser.lastName) FROM AccountUser as accountUser WHERE accountUser.id in (:userIds)")
	List<AccountUser> getUserNamesByIds(@Param("userIds") List<Integer> userIds);

	AccountUser findByPasswordRecoveryToken(String token);

	@Query("SELECT new AccountUser(accountUser.id, accountUser.firstName, accountUser.lastName, accountUser.lastLogin) from AccountUser as accountUser")
	// Page<AccountUser> getUserList(Pageable pageable);
	List<AccountUser> getUserList();
	
	@Query("SELECT new AccountUser(au.id, au.userName, au.firstName, au.lastName, au.lastLogin, au.created) from AccountUser as au "+
	" WHERE au.status is null Or au.status not in ('Inactive','Inactive-dormant')")
	List<AccountUser> getUserListToInactivate();

	@Query("from AccountUser as u where u.id IN( select ur.user.id from UserRole as ur where ur.role.id = :roleId) ORDER BY u.firstName, u.lastName")
	List<AccountUser> getUserListByRoleId(@Param("roleId") int roleId);
	
	@Query("select u.id from AccountUser u,UserRole ur, Role r where u.id=ur.user.id and ur.role.id=r.id and r.name='ADMIN'")
	List<Integer> getUsersWithAdminRole();
	
	@Query("select u from AccountUser u,UserRole ur, Role r where u.id=ur.user.id and ur.role.id=r.id and r.name=:name")
	List<AccountUser> getUsersByRoleName(@Param("name")String roleName);
	
	@Query("select u FROM AccountUser u, UserRole ur WHERE u.id=ur.user.id AND ur.role.id IN (SELECT r.id FROM Role r WHERE r.name LIKE ('%ADMIN') OR r.name LIKE ('%CSR') OR r.name = 'SUPERVISOR' OR r.name = 'SALES_OPERATIONS' OR r.name = 'CUSTOMER_SUPPORT'  OR r.name = 'SALES_MANAGER' OR r.name = 'SALES_DIRECTOR')  ORDER BY u.firstName, u.lastName")
	List<AccountUser> getAdminAndCSRRoleUsers();

	@Query("update AccountUser set status =:status where id=:id")
	void changeUserStatus(@Param("status") String status,@Param("id") int id);
	
	@Modifying
	@Transactional
	@Query("update AccountUser set secQueRetryCount =:count where id=:id")
	void updateSecQueRetryCount(@Param("count") int count,@Param("id") int id);
	
	@Modifying
	@Transactional
	@Query("update AccountUser set status =:status where id=:id")
	void updateStatusCode(@Param("status") String status,@Param("id") int id);

	@Modifying
	@Transactional
	@Query("update AccountUser set confirmed =:confirmed,secQueRetryCount=:secQueRetryCount where id=:id")
	void updateConfirmedAndSecQueRetryCount(@Param("confirmed") int confirmed,@Param("secQueRetryCount") int secQueRetryCount,@Param("id") int id);
	
	@Modifying
	@Transactional
	@Query("update AccountUser set confirmed =:confirmed where id=:id")
	void changeUserStatus(@Param("confirmed") int confirmed,@Param("id") int id);
	
	@Query("select u.email from AccountUser as u where u.id IN( select ur.user.id from UserRole as ur where ur.role.id = :roleId) ORDER BY u.firstName, u.lastName")
	List<String> getIssuerRepresentativeEmailIds(@Param("roleId") int roleId);
	
	@Query("select u FROM AccountUser u, UserRole ur WHERE u.id=ur.user.id AND ur.role.id IN (SELECT r.id FROM Role r WHERE r.name IN ('ADMIN','CSR'))  ORDER BY u.firstName, u.lastName")
	List<AccountUser> getAgentList();
	
	//@Query("select u FROM AccountUser u WHERE (uuid is null or length(uuid)<1) and username like :userPattern  ORDER BY u.userName")
	//List<AccountUser> getUserListToMigrate(@Param("userPattern") String userPattern);
	@Query("select u FROM AccountUser u WHERE (u.uuid is null or length(u.uuid)<1)   ORDER BY u.userName")
	List<AccountUser> getUserListToMigrate();
	
	@Query("select u from AccountUser u where u.id not in (select targetId from SecurableTarget where targetName = 'USER') and u.ffmUserId IS NOT NULL")
	List<AccountUser> getListOfUsersToMigrateIn();
	
	//@Query("from AccountUser as u where u.id IN( select ug.user.id from USER_GROUPS as ug where ug.GROUPS_OF_USERS_ID = :groupId) ORDER BY u.firstName, u.lastName")
	//List<AccountUser> getUserListByGroupId(@Param("groupId") int groupId);
	
	@Query("select u.id,concat(u.firstName,' ',u.lastName) from AccountUser as u where u.id IN( select ur.user.id from UserRole as ur where ur.role.id = :roleId) ORDER BY u.firstName, u.lastName")
 	List<Object[]> getUsersInfoByRoleId(@Param("roleId") int roleId);
 	
 	 @Query("select u.id,concat(u.firstName,' ',u.lastName),u.email from AccountUser as u where u.id IN( select ur.user.id from UserRole as ur where ur.role.id = :roleId) and upper(concat(first_name,' ',last_name))  LIKE '%' || UPPER(:userName) || '%' ) ORDER BY u.firstName, u.lastName")
	 List<Object[]> getUsersInfoByRoleAndUserName(@Param("roleId") int roleId,@Param("userName") String userName);
	 
	@Query("select u.id from AccountUser u where u.email= :userEmail ")
	List<Integer> userIdsFromEmail(@Param("userEmail") String userEmail);
	
	/**
	 * Fetch Users for given Role Names.
	 *
	 * @param roleNameList
	 * @return
	 */
	@Query(" Select u FROM AccountUser u, UserRole ur " +
			" WHERE u.id = ur.user.id AND ur.role.id IN " +
			" (SELECT r.id FROM Role r WHERE r.name IN (:roleNameList)) ")
	List<AccountUser> findAccountUsersForRoleNames(@Param("roleNameList") List<String> roleNameList);	
	
	
		@Query("select u.id,concat(u.firstName,' ',u.lastName) FROM AccountUser u, UserRole ur WHERE u.id=ur.user.id AND ur.role.id IN (SELECT r.id FROM Role r WHERE r.name LIKE ('%ADMIN') OR r.name LIKE ('%CSR') OR r.name = 'SUPERVISOR' OR r.name = 'SALES_OPERATIONS' OR r.name = 'CUSTOMER_SUPPORT' OR r.name = 'SALES_DIRECTOR')  AND UPPER(u.firstName)  LIKE '%' || UPPER(:userName) || '%' ORDER BY u.firstName, u.lastName")
	List<Object[]> getWorkgroupUsersToAdd(@Param("userName") String userName);
	
	@Modifying
	@Transactional
	@Query("update AccountUser u set u.status= :status,u.confirmed = :confirmed where u.id IN :userIds")
	void updateUserStatusAndConfirmedFields(@Param("userIds") List<Integer> userIds, @Param("status") String status,@Param("confirmed") int confirmed);
	
	@Query("SELECT new AccountUser(accountUser.id, accountUser.userName, accountUser.firstName, accountUser.lastName, accountUser.lastLogin) from AccountUser as accountUser WHERE accountUser.userName = :userName")
	AccountUser getUserBasicInfo(@Param("userName") String userName);
	
	AccountUser findByPhoneAndUserName(String phone, String userName);
	
	List<AccountUser> findByPhone(String phone);
	
	AccountUser findByUserNPN(String userNPN);
	

    @Query("select u.id,concat(u.firstName,' ',u.lastName),u.email, u.phone, r.label from AccountUser as u, Role as r, UserRole as ur  where upper(concat(first_name,' ',last_name))  LIKE '%' || UPPER(:userName) || '%' and u.id = ur.user and ur.role = r.id ORDER BY u.firstName, u.lastName")
	List<Object[]> getUsersInfoByUserName(@Param("userName") String userName);
	
}
