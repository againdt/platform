/**
 * 
 */
package com.getinsured.hix.platform.ecm.taglib;

import java.util.Map;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.platform.ecm.ContentManagementService;

/**
 * Java Tag Class to get list of documents under a folder.
 * 
 * <P>renders the list of documents in a tabular format.
 * 
 * <P>If the supplied folder is missing in the GHIX ECM then only header is rendered.
 * 
 * @author Ekram Ali Kazi
 */
public class ListOfDocumentsByPathTag extends TagSupport {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = 6062850667445633357L;

	/** The log object for this class. */
	private static final Logger LOGGER = Logger.getLogger(ListOfDocumentsByPathTag.class);

	/** Content path. */
	private String contentPath;

	private ContentManagementService ecmService;

	public ListOfDocumentsByPathTag() {

	}
	/**
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {

		autowireDependencies();

		try {
			Map<String,String> files = null; //ecmService.getFolderFilesByPath(contentPath);
			String result = formOutput(files);
			pageContext.getOut().print(result);

		} catch (Exception ex) {
			LOGGER.error("Error processing CMIS request - ", ex);
			throw new JspException(ex);
		}

		return SKIP_BODY;
	}

	private String formOutput(Map<String, String> files) {
		Set<String> keys = files.keySet();

		StringBuilder sb = new StringBuilder();
		sb.append("<table class='table table-border-none table-condensed'><thead><tr>");
		//form header...
		String column1 = "File Id";
		String cClass = column1.replace(" ", ""); // creates a class for each column
		sb.append("<th class='" + cClass + "'>");
		sb.append(column1);
		sb.append("</th>");

		String column2 = "File Name";
		cClass = column2.replace(" ", ""); // creates a class for each column
		sb.append("<th class='" + cClass + "'>");
		sb.append(column2);
		sb.append("</th>");

		sb.append("</tr></thead><tbody>");
		for (String key : keys) {
			sb.append("<tr><td>");
			sb.append(key);
			sb.append("</td>");
			sb.append("<td>");
			sb.append(files.get(key));
			sb.append("</td></tr>");
		}
		sb.append("</tbody></table>");

		String result = sb.toString();
		// Make sure that no null String is returned
		result = result == null ? "" : result;
		return result;
	}


	private void autowireDependencies() {
		ApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext(pageContext.getRequest(), pageContext.getServletContext());
		ecmService = (ContentManagementService) applicationContext.getBean("ecmService");
	}

	public String getContentPath() {
		return contentPath;
	}
	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}

}
