package com.getinsured.hix.platform.eventlog;

public class ControllerInfo {
	private String requestUrl;
	private String controllerMethodName;
	private String jSessionId;
	private boolean isJSessionActive;
	private String referrerUrl;

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	public String getControllerMethodName() {
		return controllerMethodName;
	}

	public void setControllerMethodName(String controllerMethodName) {
		this.controllerMethodName = controllerMethodName;
	}

	public String getjSessionId() {
		return jSessionId;
	}

	public void setjSessionId(String jSessionId) {
		this.jSessionId = jSessionId;
	}

	public boolean isJSessionActive() {
		return isJSessionActive;
	}

	public void setJSessionActive(boolean isJSessionActive) {
		this.isJSessionActive = isJSessionActive;
	}

	public String getReferrerUrl() {
		return referrerUrl;
	}

	public void setReferrerUrl(String referrerUrl) {
		this.referrerUrl = referrerUrl;
	}
}
