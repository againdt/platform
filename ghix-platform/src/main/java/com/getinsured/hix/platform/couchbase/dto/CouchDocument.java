package com.getinsured.hix.platform.couchbase.dto;

import com.google.gson.annotations.Expose;

public class CouchDocument {
	@Expose
	private String id;
	@Expose
	private CouchMeta metaData = new CouchMeta();
	@Expose
	private String content;
	@Expose
	private CouchBinary binaryMetaData;
	@Expose
	private String _class;

	public String get_class() {
		return _class;
	}

	public void set_class(String _class) {
		this._class = _class;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CouchMeta getMetaData() {
		return metaData;
	}

	public void setMetaData(CouchMeta metaData) {
		this.metaData = metaData;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public CouchBinary getBinaryMetaData() {
		return binaryMetaData;
	}

	public void setBinaryMetaData(CouchBinary binaryMetaData) {
		this.binaryMetaData = binaryMetaData;
	}
}
