package com.getinsured.hix.platform.couchbase.converters;
public interface JsonConverter {
	<T> T fromJson(String source, Class<T> type);

	<T> String toJson(T source);
}