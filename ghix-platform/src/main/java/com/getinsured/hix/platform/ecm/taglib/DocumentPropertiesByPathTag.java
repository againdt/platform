/**
 * 
 */
package com.getinsured.hix.platform.ecm.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.ecm.ContentManagementService;

/**
 * Java Tag Class to get document properties based on name.
 * 
 * <P>renders the properties in a tabular format of the document.
 * 
 * <P>If the supplied document is missing in the GHIX ECM then only header is rendered.
 * 
 * @author Ekram Ali Kazi
 */
public class DocumentPropertiesByPathTag extends TagSupport {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = 6062850667445633357L;

	/** The log object for this class. */
	private static final Logger LOGGER = Logger.getLogger(DocumentPropertiesByPathTag.class);

	/** Content path. */
	private String contentPath;

	private ContentManagementService ecmService;


	public DocumentPropertiesByPathTag() {

	}
	/**
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {

		autowireDependencies();

		try {
			Content data = ecmService.getContentByPath(contentPath);
			String result = formOutput(data);
			pageContext.getOut().print(result);

		} catch (Exception ex) {
			LOGGER.error("Error processing CMIS request - ", ex);
			throw new JspException(ex);
		}

		return SKIP_BODY;
	}

	private String formOutput(Content data) {

		StringBuilder sb = new StringBuilder();
		sb.append("<table class='table table-border-none table-condensed'><thead><tr>");
		//form header...
		createColumn(sb, "Content Id");
		createColumn(sb, "Doc Type");
		createColumn(sb, "Mime Type");
		createColumn(sb, "Creation Date");

		sb.append("</tr></thead><tbody>");

		if (!data.isEmpty()) {
			sb.append("<tr>");
			fillColumn(data.getContentId(), sb);
			fillColumn(data.getDocType(), sb);
			fillColumn(data.getMimeType(), sb);
			fillColumn(data.getCreationDate() != null ? data.getCreationDate().toString() : "", sb);
			sb.append("</tr>");
		}else{
			sb.append("<tr><td>");
			sb.append("No data found for contentName - " + contentPath);
			sb.append("</td></tr>");
		}

		sb.append("</tbody></table>");

		String result = sb.toString();
		// Make sure that no null String is returned
		result = result == null ? "" : result;
		return result;
	}
	private void fillColumn(String data, StringBuilder sb) {
		sb.append("<td>");
		sb.append(data);
		sb.append("</td>");
	}
	private void createColumn(StringBuilder sb, String column) {
		String cClass = column.replace(" ", ""); // creates a class for each column
		sb.append("<th class='" + cClass + "'>");
		sb.append(column);
		sb.append("</th>");
	}


	private void autowireDependencies() {
		ApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext(pageContext.getRequest(), pageContext.getServletContext());
		ecmService = (ContentManagementService) applicationContext.getBean("ecmService");
	}

	public String getContentPath() {
		return contentPath;
	}
	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}


}
