/*
 * =============================================================================
 * 
 *   Copyright (c) 2011-2012, The THYMELEAF team (http://www.thymeleaf.org)
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * 
 * =============================================================================
 */
package com.getinsured.hix.platform.notification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notification.exception.NotificationException;
import com.getinsured.hix.platform.util.GhixUtils;

@Service
public class EmailService implements NotificationService{

	private static final Logger LOGGER = Logger.getLogger(EmailService.class);
    @Autowired private JavaMailSender mailSender;
	
    /* 
     * Send HTML mail (simple) via notice object
     */
    public void dispatch(Notice noticeObj) throws NotificationException {
        
    	boolean hasAttachments = false; 
    	boolean isHtml = true; 
    	
		if(noticeObj.getAttachment() != null){
			hasAttachments = true;
		}
		
		if(noticeObj.getNoticeType().getType() .equals("PLAIN_EMAIL")){
			isHtml = false;
		}
		try{
			
	    	final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
	    	final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, hasAttachments, "UTF-8");
	    	message.setSubject(noticeObj.getSubject());
	        message.setFrom(noticeObj.getFromAddress());
	        message.setTo(noticeObj.getToAddress());
	        message.setText(noticeObj.getEmailBody(), isHtml);
	    	
	    	if(hasAttachments){
		    	List<String> fileLocations = getLocations(noticeObj.getAttachment());
				List <EmailAttachments> emailAttachments = new ArrayList<EmailAttachments>();
				for(String filepath: fileLocations){
					emailAttachments.add(getAttachments(filepath)); 
				}
				
				// Add the attachment
		        for(EmailAttachments emailAttachment: emailAttachments){
		        	  final InputStreamSource attachmentSource = new ByteArrayResource(emailAttachment.getAttachmentBytes());
		              message.addAttachment(emailAttachment.getAttachmentName(), attachmentSource, emailAttachment.getContentType());
		        }
	    	}
	        
	        // Send mail
	        this.mailSender.send(mimeMessage);
		}catch(MessagingException excp){
			LOGGER.error("Error while sending mail "+excp.getMessage());
			throw new NotificationException(excp.getMessage(), excp);
		}
    }
    
    public EmailAttachments getAttachments(String filePath) throws NotificationException{
    	 EmailAttachments emailAttachment  = new EmailAttachments();
    	if(GhixUtils.isGhixValidPath(filePath)){
    		File file = new File(filePath);
    		byte fileContent[] = null;
    		String contentType = null;
    		FileInputStream fin = null;
    	    try
    	    {
    	      //create FileInputStream object
    	      fin = new FileInputStream(file);
              fileContent = new byte[(int)file.length()];
              fin.read(fileContent);
    	     
              URI uri = file.toURI();
              URL u = uri.toURL();
              URLConnection uc = u.openConnection();
              contentType = uc.getContentType();	

              emailAttachment.setAttachmentBytes(fileContent);
              emailAttachment.setAttachmentName(file.getName());
              emailAttachment.setContentType(contentType);       
    	    }
    	    catch(FileNotFoundException e)
    	    {
    	      //System.out.println("File not found" + e);
    	    	LOGGER.error("File not found",e);
    	    }
    	    catch(IOException ioe)
    	    {
    	      //System.out.println("Exception while reading the file " + ioe);
    	    	LOGGER.error("Exception while reading the file ",ioe);
    	    }finally{
    			IOUtils.closeQuietly(fin);
    			
    		}
    	}else{
    		throw new NotificationException("Application trying to reach a blacklisted folder or filename or extension type.");
    	}
		
		return emailAttachment;
	}
    
    private List<String> getLocations(String attachmentPath)
	{
		List<String> locations = new ArrayList<String>();
		StringTokenizer tokens = new StringTokenizer(attachmentPath, ";");
		while(tokens.hasMoreTokens())
		{
			locations.add(tokens.nextToken());
		}
		return locations;
	}


    public void dispatchToAll(Notification noticeObj) throws NotificationException{
        
    	boolean hasAttachments = false; 
    	boolean isHtml = true; 
    	
		if(noticeObj.getAttachment() != null){
			hasAttachments = true;
		}
		if(noticeObj.getNoticeType().getType() .equals("PLAIN_EMAIL")){
			isHtml = false;
		}
		try{
			
	    	final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
	    	final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, hasAttachments, "UTF-8");
	    	message.setSubject(noticeObj.getSubject());
	        message.setFrom(noticeObj.getFromAddress());
	        List<String> toRecipients = noticeObj.getToRecipients();
	        if(toRecipients != null){
	        for(String toRecipient: toRecipients){
	        	message.addTo(toRecipient);
	        }
	        }
	       
	        List<String> bCCRecipients = noticeObj.getbCCRecipients();
	        if(bCCRecipients != null){
	        for(String bCCRecipient: bCCRecipients){
	        	message.addBcc(bCCRecipient);
	        }
	        }
	        message.setText(noticeObj.getEmailBody(), isHtml);
	    	
	    	if(hasAttachments){
		    	List<String> fileLocations = getLocations(noticeObj.getAttachment());
				List <EmailAttachments> emailAttachments = new ArrayList<EmailAttachments>();
				for(String filepath: fileLocations){
					emailAttachments.add(getAttachments(filepath)); 
				}
				// Add the attachment
		        for(EmailAttachments emailAttachment: emailAttachments){
		        	  final InputStreamSource attachmentSource = new ByteArrayResource(emailAttachment.getAttachmentBytes());
		              message.addAttachment(emailAttachment.getAttachmentName(), attachmentSource, emailAttachment.getContentType());
		        }
	    	}
	        // Send mail
	        this.mailSender.send(mimeMessage);
		}catch(MessagingException excp){
			LOGGER.error("Error while sending mail "+excp.getMessage());
			throw new NotificationException(excp.getMessage(), excp);
		}
    }


    /*
    
     
     * Send HTML mail with inline image
     
    public void sendMailWithInline(
            final String recipientName, final String recipientEmail, final String imageResourceName, 
            final byte[] imageBytes, final String imageContentType, final Locale locale)
            throws MessagingException {
        
        // Prepare the evaluation context
        final Context ctx = new Context(locale);
        ctx.setVariable("name", recipientName);
        ctx.setVariable("subscriptionDate", new TSDate());
        ctx.setVariable("hobbies", Arrays.asList("Cinema", "Sports", "Music"));
        ctx.setVariable("imageResourceName", imageResourceName); // so that we can reference it from HTML
        
        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = 
                new MimeMessageHelper(mimeMessage, true  multipart , "UTF-8");
        message.setSubject("Example HTML email with inline image");
        message.setFrom("thymeleaf@example.com");
        message.setTo(recipientEmail);

        // Create the HTML body using Thymeleaf
        final String htmlContent = this.templateEngine.process("email-inlineimage.html", ctx);
        message.setText(htmlContent, true  isHtml );
        
        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
        message.addInline(imageResourceName, imageSource, imageContentType);
        
        // Send mail
        this.mailSender.send(mimeMessage);
        
    }*/
}
