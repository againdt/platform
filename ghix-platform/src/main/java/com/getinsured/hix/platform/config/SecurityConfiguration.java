package com.getinsured.hix.platform.config;

public class SecurityConfiguration {

	
	public enum SecurityConfigurationEnum implements PropertiesEnumMarker
	{  
		IS_EXTERNAL ("security.UserRegistration.IsExternal"),
        IS_EMAIL_ACTIVATION ("security.UserRegistration.IsEmailActivation"),
        IS_EMAIL_CHANGE_ENABLED("security.UserRegistration.IsEmailChangeEnabled"),
        IS_SECURITY_QUESTIONS_REQUIRED ("security.UserRegistration.IsSecurityQuestionsRequired"),
        NO_OF_SECURITY_QUESTIONS ("security.UserRegistration.NoOfSecurityQuestions"),
        EMPLOYER ("security.UserRegistration.ActivationEmailExpirationDays.Employer"),
        ISSUER ("security.UserRegistration.ActivationEmailExpirationDays.Issuer"),
        EMPLOYEE ("security.UserRegistration.ActivationEmailExpirationDays.Employee"),
        USER ("security.UserRegistration.ActivationEmailExpirationDays.User"),
	PASSWORDEXP ("security.UserRegistration.ResetPasswordEmailExpirationDays"),
        HIDECAPTCHA ("security.HideCaptcha"),
        HIDETERMSANDCONDITIONS ("security.HideTermsAndConditions"),
        ROLEPROVISIONROLES("security.RoleProvisionRoles"),
        IS_PASSWORD_POLICY_ACTIVE("security.PasswordPolicy.isActive"), 
        PASSWORD_HISTORY_LIMIT("security.PasswordPolicy.PasswordHistoryLimit"),
        PASSWORD_MAX_AGE("security.PasswordPolicy.PasswordMaxAge"), 
        PASSWORD_MIN_AGE("security.PasswordPolicy.PasswordMinAge"),  
        IS_PASSWORD_COMPLEXITY_ACTIVE("security.PasswordPolicy.PasswordComplexity.isActive"), 
        REGEX("security.PasswordPolicy.PasswordComplexity.Regex"), 
		IS_PASSWORD_HISTORY_LIMIT_ACTIVE("security.PasswordPolicy.PasswordHistoryLimitActive"), 
		IS_PASSWORD_PASSWORD_MAX_AGE_ACTIVE("security.PasswordPolicy.PasswordMaxAgePolicyActive"), 
		IS_PASSWORD_PASSWORD_MIN_AGE_ACTIVE("security.PasswordPolicy.PasswordMinAgePolicyActive"),
		PASSWORD_VIOLATION_MSG("security.PasswordPolicy.PasswordViolationMsg"),
		PASSWORD_MIN_LENGTH("security.PasswordPolicy.PasswordMinLength"),
		PASSWORD_STRENGTH("security.PasswordStrength"),
		JASYPT_ALGORITHM("security.JasyptEncryptor.Algorithm"),
		JASYPT_PASSWORD_KEY("security.JasyptEncryptor.PasswordKey"),
		JASYPT_OUTPUT_TYPE("security.JasyptEncryptor.OutputType"),
		ASSISTER("security.UserRegistration.ActivationEmailExpirationDays.Assister"),
		CLIENT_INACTIVITY_THRESHOLD("security.sessionTimeout.inactivityThreshold"),
		SESSION_POPUP_THRESHOLD("security.sessionTimeout.sessionPopupThreshold"),
		ENABLE_CONSUMER_SIGNUP("security.EnableConsumerSignup"),
		IDALINK_APPEND_TOKEN("security.idalink.appendToken"),
		ROLE_ACCESS_ENABLED("security.roleAccess.enabled"),
		BYPASS_MFA_CHECK("security.mfa.bypassUser"),
		ROLE_ACCESS_ALLOWED_IP_RANGES("security.roleAccess.allowedIPRanges");
		
		
		private final String value;	  
		@Override
		public String getValue(){return this.value;}
		SecurityConfigurationEnum(String value){
	        this.value = value;
	    }
	};
}
