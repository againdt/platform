package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.NoticeType;

public interface INoticeTypeRepository extends JpaRepository<NoticeType, Integer> {
	public NoticeType findByEmailClass(String emailClass);
	public NoticeType findByQueuedEmailClass(String queuedEmailClass);
	NoticeType findByEmailClassAndLanguage(String emailClass, GhixLanguage language);
	NoticeType findByNotificationNameAndLanguage(String notificationName, GhixLanguage language);
	public NoticeType findById(Integer id);
	
	
	
}