package com.getinsured.hix.platform.util;

import java.net.Socket;
import java.util.Map;

import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;

public class SSLKeyStrategy implements PrivateKeyStrategy{
	
	public SSLKeyStrategy(){
	}

	@Override
	public String chooseAlias(Map<String, PrivateKeyDetails> aliases, Socket socket) {
		return SSLConfiguration.getSSLConfiguration().chooseAliasFromKeyStore(aliases, socket);
	}

}
