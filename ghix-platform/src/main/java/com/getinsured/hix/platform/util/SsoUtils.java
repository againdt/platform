package com.getinsured.hix.platform.util;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SsoUtils {

	private static final Logger log = LoggerFactory
			.getLogger(SsoUtils.class);

	public static void main(String[] args) {
///		String val = getIdpServerEntityIDFromXml("/", "YEVG_idp.xml");
		
		System.out.println("Using file for tests: ");

	}

	/**
	 * Returns key/value pairs from context.xml for jndi data bind
	 * 
	 * @return Map or null.
	 */
	public static String getIdpServerEntityIDFromXml(String server_Folder, String xmlFileName)
	{
		String ghixHome = System.getenv("GHIX_HOME");				
		String xmlFilePath = ghixHome+server_Folder+xmlFileName;

		log.info(" Using Server_Folder: " + server_Folder);
		log.info("           GHIX_HOME: " + ghixHome);
		log.info("         xmlFilePath: " + xmlFilePath);

		if(server_Folder != null)
		{
			File f = new File(xmlFilePath.replace('\\', '/'));
			
			if(f.exists() && f.canRead())
			{
				log.info("Found configuration: " + xmlFilePath);
				Document doc = parseXmlFile(xmlFilePath);
				
				if(doc != null)
				{
					String kv = getAttrValueFromDocument(doc,"EntityDescriptor", "entityID");
					
					if(kv != null) 
					{
						log.info("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}

		return null;
	}
	
	/**
	 * Parses given XML file and returns Document object.
	 * 
	 * @param xmlFilePath absolute xml file path.
	 * @return Document object or null if unable to parse or exeption occurrs.
	 */
	private static Document parseXmlFile(String xmlFilePath)
	{
		
		Document doc = null;
		try
		{
			DocumentBuilderFactory factory = Utils.getDocumentBuilderFactoryInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc =builder.parse(xmlFilePath);
		}
		catch (Exception e)
		{
			log.error("Could not get Document from given XML file: " + xmlFilePath, e);
		}
		return doc;
	}
	
	/**
	 * Gets the XML attribute name/value from an IDP server config xml file.
	 * 
	 */
	protected static String getAttrValueFromDocument(Document doc, String contextName, String attrib)
	{
		String attrValue = null;
		
		if (doc != null)
		{
			log.info("Reading IDP Server config xml file and parsing relavent data to obtain entityID ...");

			NodeList root = doc.getChildNodes();
			Node context = getNode(contextName, root);

			if (context == null)
			{
				log.info("No <Context> node found, ignoring this IDP server config xml file");
				return null;
			}
			
			attrValue = getNodeAttr(attrib, context);
			
		}

		return attrValue;
	}
	
	/**
	 * Gets attribute from given node.
	 * 
	 * @param attrName name of the attribute.
	 * @param node node to read attribute from.
	 * @return node value or empty string.
	 */
	protected static String getNodeAttr(String attrName, Node node)
	{
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			NamedNodeMap attrs = node.getAttributes();

			for (int y = 0; y < attrs.getLength(); y++)
			{
				Node attr = attrs.item(y);

				if (attr.getNodeName().equalsIgnoreCase(attrName))
				{
					return attr.getNodeValue();
				}
			}
		}

		return "";
	}	

	/**
	 * Returns node from given node list.
	 * 
	 * @param tagName tag name of the node thats needed.
	 * @param nodes list of nodes where this node is placed
	 * @return Node or null.
	 */
	protected static Node getNode(String tagName, NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			Node node = nodes.item(x);
			log.info("Node Name="+node.getNodeName());
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				return node;
			}
		}

		return null;
	}

	
	/**
	 * Returns text node value.
	 * 
	 * @param tagName tag name of the node.
	 * @param nodes list of nodes.
	 * @return text node value or empty string.
	 */
	protected static String getNodeValue(String tagName, NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++)
				{
					Node data = childNodes.item(y);
					if (data.getNodeType() == Node.TEXT_NODE)
						return data.getNodeValue();
				}
			}
		}
		return "";
	}	
}
