package com.getinsured.hix.platform.repository;

import com.getinsured.affiliate.model.Affiliate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface AffiliateRepository extends TenantAwareRepository<Affiliate, Long> {

	Affiliate findByUrl(String url);
	
	Affiliate findByAffiliateId(Long id);
	
	Affiliate findByCompanyName(String companyName);

	@Query(value = "SELECT module_id FROM module_users WHERE module_name = 'affiliate' AND user_id = :userId", nativeQuery = true)
	List<BigDecimal> getAffiliateIdsByUserId(@Param("userId") int userId);
	
	@Query("SELECT moduleId from ModuleUser mu WHERE mu.moduleName='affiliate' and mu.user.id = :employerId")
	List<Integer> getAffiliateForEmployer(@Param("employerId") int employerId);


	Affiliate findByUrlAndAffiliateStatus(String url, String affiliateStatus);

	@Query("SELECT aff.id, aff.companyName from Affiliate aff where aff.hasEmployer ='Y' and aff.affiliateStatus = 'ACTIVE'" )
	List<Object[]> getAllAffiliatesForEmployer();

	List<Affiliate> findByAffiliateStatusAndTenantId(String affActiveStatus, Long tenantId);
	List<Affiliate> findByTenantId(Long tenantId);
	
	@Query("Select aff.id, aff.companyName  " +
			" FROM Affiliate aff"+
			" where aff.id IN :listAffiliateIds")
	List<Object[]> getAllAffiliatesNames(@Param("listAffiliateIds") List<Long> listAffiliateIds);
	
	@Query("Select aff.id, aff.companyName  " +
			" FROM Affiliate aff")
	List<Object[]> getAllAffiliatesNames();
	
	@Query("Select aff.id, aff.companyName,aff.accountExecutive,aff.apiKey,aff.affiliateStatus,aff.audit.lastUpdatedOn " +
			" FROM Affiliate aff"+
			" where aff.id IN :listAffiliateIds")
	List<Object[]> findAllAffiliates(@Param("listAffiliateIds") List<Long> listAffiliateIds);
	
	@Query("Select aff.id, aff.companyName,aff.accountExecutive,aff.apiKey,aff.affiliateStatus,aff.audit.lastUpdatedOn " +
			" FROM Affiliate aff")
	List<Object[]> findAllAffiliates();

}
