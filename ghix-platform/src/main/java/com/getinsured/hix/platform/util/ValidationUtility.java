package com.getinsured.hix.platform.util;



import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.security.service.UserService;

@Component
public class ValidationUtility {
	 
	private static Logger LOGGER = LoggerFactory.getLogger(ValidationUtility.class);
 	@Autowired private UserService userService;
 	private static final String COLUMN_UPDATED = "updated";

 	public static boolean isIntegerParseInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {}
        return false;
    }
	
	public  boolean checkForPasswordMaxAge(AccountUser userObj, String maxAgeAllowed)  
	{
		boolean result=true;
		String userName = userObj.getUserName();
		if(userName.endsWith("ghix.com")){
			// No password age expiration check for ghix.com users
			return true;
		}
		
		if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE) ,"Y" )){
			if(isIntegerParseInt(maxAgeAllowed) && (Integer.parseInt(maxAgeAllowed)==0 || Integer.parseInt(maxAgeAllowed)==-1)){
				return result;
			}
	
			
			
			if(userObj!=null)
			{
				Date lastPasswordUpdateDate=userObj.getPasswordLastUpdatedTimeStamp();
				
				
				if(lastPasswordUpdateDate!=null)
				{
					if(DateUtil.getDateIntervalInDaysInputFormatyyyyMMddHHmmssS(lastPasswordUpdateDate)>=Integer.parseInt(maxAgeAllowed))
					{
						result= false;
					}
				}
			}
			
		}
		
		 
		
		
		return result;
		
	}
	
	public  boolean checkForPasswordMinAge(AccountUser userObj, String minAgeAllowed) 
	{
		boolean result=true;
		if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE) ,"Y" )){
		
			int minAgeAllowedInt = Integer.parseInt(minAgeAllowed);
			if(minAgeAllowedInt==0){
				return true;
			}

			
			if(userObj!=null)
			{
				Date lastPasswordUpdateDate=userObj.getPasswordLastUpdatedTimeStamp();
				
				if(lastPasswordUpdateDate!=null)
				{
					if(DateUtil.getDateIntervalInDaysInputFormatyyyyMMddHHmmssS(lastPasswordUpdateDate)<Integer.parseInt(minAgeAllowed))
					{
						result= false;
					}
				}
			}
			
		}
		
		
		
		return result;
		
	}
	
	
	 /**
	   * Validate password with regular expression
	   * @param password password for validation
	   * @return true valid password, false invalid password
	   */
	 public  boolean checkForPasswordComplexity(String password, String regexExpression) 
	{
		if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE) ,"Y" )
			){
			int passwordminlength = StringUtils.isNumeric(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_LENGTH)) ? Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_LENGTH)):0;
			boolean isPasswordCorrect = true;
			if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_COMPLEXITY_ACTIVE) ,"Y" )){
				
					 Pattern pattern = Pattern.compile(DisplayUtil.replaceDynamicValuesInMessageSource(regexExpression));
					 Matcher matcher = pattern.matcher(password);
					 isPasswordCorrect =  matcher.matches(); 
				
			}
			if(isPasswordCorrect && password!=null && !StringUtils.isEmpty(password) && password.length()<passwordminlength){
				isPasswordCorrect = false;
			 }
			 return isPasswordCorrect;	
		}else{
			return true;
		}
		
		 
	}
	 
	 
	 /**
      * Validate password with regular expression
      * @param password password for validation
      * @return true valid password, false invalid password
      */
    public  boolean isPasswordAvailableInPasswordHistory(String userEmailId,String password,String passwordHistoryLimit) 
   {
    		boolean flag=false;
        
    		LOGGER.debug("isPasswordAvailableInPasswordHistory check: " + passwordHistoryLimit);
           if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE) ,"Y" )){
        	   
        	   int passwordHistoryLimitValue=Integer.parseInt(passwordHistoryLimit);
               if(passwordHistoryLimitValue==0){
            	   return false;
               }
        	   
               AccountUser userObj=userService.findByEmail(userEmailId); //
               Role userRole = userService.getDefaultRole(userObj);
               if(!"INDIVIDUAL".equalsIgnoreCase(userRole.getName())) {
            	   LOGGER.debug("isPasswordAvailableInPasswordHistory user: " + userObj.getId());
            	   flag = userService.isPasswordPresentInHistory( userObj, password, passwordHistoryLimitValue);
               }
           } 
            LOGGER.debug("isPasswordAvailableInPasswordHistory returned: " + flag);
           return flag;
           
   }
    
   
    
    public  boolean checkForPasswordMaxAge(String userEmailId, String maxAgeAllowed)  
	{
		boolean result=true;
		
		if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE) ,"Y" )){
			if(StringUtils.isNumeric(maxAgeAllowed) && Integer.parseInt(maxAgeAllowed)==0){
				return result;
			}
			List<Map<String, String>> userHistoryMap = userService.loadUserHistory(userEmailId);
			if(userHistoryMap!=null && userHistoryMap.size()>0){
				String lastPasswordUpdateDate = userHistoryMap.get(0)!=null?userHistoryMap.get(0).get(COLUMN_UPDATED):"1900-02-01 17:08:34.458";
				
				if(DateUtil.getDateIntervalInDaysInputFormatyyyyMMddHHmmssS(lastPasswordUpdateDate)>=Integer.parseInt(maxAgeAllowed))
				{
					result= false;
				}
			}
		}
		
		 
		
		
		return result;
		
	}
	
	public  boolean checkForPasswordMinAge(String userEmailId, String minAgeAllowed) 
	{
		boolean result=true;
		if(StringUtils.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE) ,"Y" )){
		
			int minAgeAllowedInt = Integer.parseInt(minAgeAllowed);
			if(minAgeAllowedInt==0){
				return true;
			}
			List<Map<String, String>> userHistoryMap = userService.loadUserHistory(userEmailId);
			
			if(userHistoryMap!=null && userHistoryMap.size()>0){
				String lastPasswordUpdateDate = userHistoryMap.get(0)!=null && userHistoryMap.get(0).get(COLUMN_UPDATED)!=null && DateUtil.isValidDate(userHistoryMap.get(0).get(COLUMN_UPDATED), "yyyy-MM-dd HH:mm:ss.S")?userHistoryMap.get(0).get(COLUMN_UPDATED):"1900-02-01 17:08:34.458";
				
				if(DateUtil.getDateIntervalInDaysInputFormatyyyyMMddHHmmssS(lastPasswordUpdateDate)<Integer.parseInt(minAgeAllowed))
				{
					result= false;
				}	
			}
		}
		
		
		
		return result;
		
	}
	
	
	

}
