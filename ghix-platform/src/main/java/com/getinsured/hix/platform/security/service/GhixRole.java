/**
 * Enumaration class to define GHIX Roles
 */
package com.getinsured.hix.platform.security.service;

/**
 * Enumerations used to define ROLE names
 * @author tadepalli_v
 * @since 12/08/2012
 *
 */

public enum GhixRole {
	 EMPLOYER("EMPLOYER"),
	 EMPLOYEE("EMPLOYEE"),
	 ISSUER("ISSUER"),
	 ISSUER_ADMIN("ISSUER_ADMIN"),
	 BROKER_ADMIN("BROKER_ADMIN"),
	 BROKER("BROKER"),
	 ISSUER_REP("ISSUER_REPRESENTATIVE"),
	 ISSUER_ENROLLMENT_REP("ISSUER_ENROLLMENT_REPRESENTATIVE"),
	 ADMIN("ADMIN"),
	 INDIVIDUAL("INDIVIDUAL"),
	 ENROLLMENTENTITY("ASSISTERENROLLMENTENTITY"),
	 CONSUMER("CONSUMER"),
	 ASSISTER("ASSISTER"),
	 CSR("CSR");
	 
	private String roleName;

	/**
	 * @param roleName
	 */
	private GhixRole(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

}
