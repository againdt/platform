package com.getinsured.hix.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.PrivateKeyDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.config.FileBasedConfiguration;

public class SSLConfiguration {
	private  Logger LOGGER = LoggerFactory.getLogger(SSLConfiguration.class);
	private  BasicCredentialsProvider credsProvider =new BasicCredentialsProvider();
	private  Map<String,String> aliasMapping = Collections.synchronizedMap(new HashMap<String,String>());
	private  String defaultAlias = null;
	private static SSLConfiguration sslConf = null;
	private Map<String , APIKeyMode> apiKeyInfo = Collections.synchronizedMap(new HashMap<>());
	
	public static SSLConfiguration getSSLConfiguration(){
		if(sslConf == null){
			sslConf = new SSLConfiguration();
		}
		return sslConf;
	}
	private SSLConfiguration(){
		
	}
	public boolean loadAliasMapping(){
		String ghixHome = System.getProperty("GHIX_HOME");
		if(ghixHome == null){
			throw new RuntimeException("Expected GHIX_HOME property to be available to load the alias mapping, found none");
		}
		String aliasMappingFile = ghixHome+File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+"sslConf.xml";
		LOGGER.info("Loading alias mapping from file:"+aliasMappingFile);
		Document doc = null;
		InputStream is = null;
		try {
			DocumentBuilderFactory factory = Utils.getDocumentBuilderFactoryInstance();
			is = new FileInputStream(aliasMappingFile);
			doc = factory.newDocumentBuilder().parse(is);
			return  buildAliasMap(doc);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			LOGGER.error("Failed to load the attribute map", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
		return false;
	}
	
	public String chooseAliasFromKeyStore(Map<String, PrivateKeyDetails> aliases,
			Socket socket) {
		String server = socket.getInetAddress().getHostName();
		String alias = aliasMapping.get(server);
		if(alias == null){
			if(defaultAlias == null){
				throw new RuntimeException("No alias found for server:"+server+", and No default alias provided, Expected one of the registered servers in sslConf.xml::"+aliasMapping);
			}
			LOGGER.info("No explicit alias provided for server:"+server+" using default alias:"+defaultAlias);
			return defaultAlias;
		}
		LOGGER.info("Using key entry with alias:"+alias+" for server "+server);
		return alias;
	}
	
	private boolean buildAliasMap(Document doc) {
		NodeList aliasMaps = doc.getElementsByTagName("aliasMap");
		Node aliasMapNode = null;
		String alias = null;
		String server = null;
		String tmpVal = null;
		Node tmp = null;
		NamedNodeMap attributesList = null;
		int len = aliasMaps.getLength();
		for(int i = 0; i < len; i++){
			aliasMapNode = aliasMaps.item(i);
			attributesList = aliasMapNode.getAttributes();
			tmp = attributesList.getNamedItem("alias");
			if(tmp != null){
				alias = tmp.getNodeValue();
			}else{
				LOGGER.error("No alias found for alias entry, skipping");
				continue;
			}
			tmp = attributesList.getNamedItem("server");
			if(tmp != null){
				server = tmp.getNodeValue();
			}else{
				LOGGER.error("No server entry found for alias:"+alias+", skipping");
				continue;
			}
			
			tmp = attributesList.getNamedItem("default");
			if(tmp != null){
				tmpVal = tmp.getNodeValue();
				if(Boolean.valueOf(tmpVal)){// This alias is set to default
					if(defaultAlias != null && !alias.equals(defaultAlias)){
						throw new RuntimeException("Alias:"+defaultAlias+" is already set to be a default alias, \""+alias+"\" can not be set to be default alias, please provide only one");
					}
					defaultAlias  = alias;
					LOGGER.info("Marking \""+alias+"\" entry as default alias");
				}
			}
			tmp = attributesList.getNamedItem("basicAuth");
			if(tmp != null){
				tmpVal = tmp.getNodeValue();
				if(Boolean.valueOf(tmpVal)){// This alias is set to default
					LOGGER.info("Enabling host {} for basic authentication",server);
					tmp = attributesList.getNamedItem("port");
					String portStr = null;
					if(tmp == null) {
						portStr = "443";
					}else {
						portStr = tmp.getNodeValue();
					}
					int port = -1;
					try {
						port = Integer.parseInt(portStr);
					}catch(NumberFormatException ne){
						port = 443;
					}
					Node userAuthKeyNode = attributesList.getNamedItem("userNameKey");
					if(userAuthKeyNode != null) {
						String userAuthName = userAuthKeyNode.getNodeValue();
						Node userPassNode = attributesList.getNamedItem("userPassKey");
						if(userPassNode != null) {
							String userPassKey = userPassNode.getNodeValue();
							Node authModeAttr = attributesList.getNamedItem("authMode");
							String apiSecretKey = null;
							String authMode = null;
							if(authModeAttr != null){
								authMode = authModeAttr.getNodeValue();
							}
							if(authMode == null){
								authMode = APIKeyMode.BASIC;
								LOGGER.warn("No authentication mode provided for server {} assuming basic authentication with plain text password", server);
							}
							if(authMode.equalsIgnoreCase(APIKeyMode.HMAC) || authMode.equalsIgnoreCase(APIKeyMode.HMAC_NOANCE)){
								Node apiSecretNode = attributesList.getNamedItem("apiSecretKey");
								if(apiSecretNode != null){
									apiSecretKey = apiSecretNode.getNodeValue();
								}
							}
							if((userAuthName != null && userAuthName.length() > 0) &&
									(userPassKey != null && userPassKey.length() > 0)) {
								registerBasicAuth(server, port, userAuthName, userPassKey,authMode, apiSecretKey);
							}
						}
					}
					
				}
			}
			tmp = null;
			tmp = attributesList.getNamedItem("apiKeyProperty");
			if(tmp != null){
				String headerName = null;
				String apiKey = tmp.getNodeValue().trim();
				tmp = attributesList.getNamedItem("apiKeyMode");
				String apiKeyMode = tmp.getNodeValue();
				String secret = null;
				tmp = attributesList.getNamedItem("apiSecretKey");
				if(tmp != null){
					secret = tmp.getNodeValue();
				}
				tmp = attributesList.getNamedItem("apiHeader");
				if(tmp != null){
					headerName = tmp.getNodeValue().trim();
				}
				this.apiKeyInfo.put(server, new APIKeyMode(apiKey, apiKeyMode, secret, headerName));
				
			}
			if(alias.length() > 0 && server.length() > 0){
				aliasMapping.put(server, alias);
				LOGGER.info("Added alias:"+alias+" for server:"+server);
			}else{
				LOGGER.error("Invalid alias mapping entry:[alias="+alias+" -> server="+server+"], Ignoring");
			}
		}
		return true;
	}
	
	public APIKeyMode getAPIKeyMode(String server){
		return this.apiKeyInfo.get(server);
	}
	
	private void registerBasicAuth(String server, int port,String authUserKey, String authUSerPassKey, String authMode, String apiSecretKey) {
		LOGGER.info("Registering server {} for basic auth for {}",server, authMode);
		HttpHost target = new HttpHost(server, port, "https");
		FileBasedConfiguration config = FileBasedConfiguration.getConfiguration();
		String userInfo= config.getProperty(authUserKey);
		String pass = config.getProperty(authUSerPassKey);
		String apiSecret = config.getProperty(apiSecretKey);
		if(userInfo == null || pass == null) {
			LOGGER.error("Server {} expected to provide the basic auth but not configured correctly, check \"sslConf.xml\" and make sure properties declared for this server are available in \"configuration.properties\"", server);
			return;
		}
		if(authMode != null && authMode.equalsIgnoreCase("HMAC")){
			if(apiSecret == null){
				LOGGER.error("Server {} expected to provide the basic auth using HMAC but not configured correctly, check \"sslConf.xml\" and make sure properties declared for this server are available in \"configuration.properties\", missing \"apiSecret\"", server);
				return;
			}
		}
        credsProvider.setCredentials(
                new AuthScope(target.getHostName(), target.getPort()),
                new GICredentials(userInfo,pass,authMode,apiSecret));
        LOGGER.info("Registered server  {} for basic authentication with {} mode", server, authMode);
	}

	public CredentialsProvider getCredentialsProvider() {
		return this.credsProvider;
	}
}
