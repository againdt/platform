package com.getinsured.hix.platform.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import com.getinsured.hix.platform.util.GhixUtils;

public class GhixContextSecurityFilter implements Filter {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		if(!GhixUtils.isStaticResourceRequest(httpRequest)){
			HttpSession session = httpRequest.getSession();
			
			/**
			 * If session has SPRING_SECURITY_CONTEXT, set it into SecurityContextHolder
			 * Else, clear SecurityContextHolder. 
			 */
			Object springSecurityContextObj = session.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);
			
			if(springSecurityContextObj != null && springSecurityContextObj instanceof SecurityContext){
				Authentication currentAuth = SecurityContextHolder.getContext().getAuthentication();
				if(currentAuth == null){
					SecurityContext securityContext = (SecurityContext) springSecurityContextObj;
					SecurityContextHolder.getContext().setAuthentication(securityContext.getAuthentication());
				}
			}else{
				SecurityContextHolder.clearContext();
			}
		}
		chain.doFilter(request, response);
		
	}
	
	@Override
	public void destroy() {}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}
	
}
