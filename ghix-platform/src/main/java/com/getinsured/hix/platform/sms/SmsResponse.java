package com.getinsured.hix.platform.sms;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Wrapper class to encapsulate response from SMS providers.
 * 
 * @author Ekram Ali Kazi
 *
 */
public class SmsResponse {

	/** third party sms provider name e.g. TWILIO */
	private String providerName;

	/** third party unique id
	 * TWILIO - A 34 character string that uniquely identifies this resource.
	 * */
	private String messageSID;

	/** incoming, outgoing */
	private String direction;

	/** to phone number */
	private String toPhone;

	/** from phone number */
	private String fromPhone;

	/** sms body */
	private String body;

	/**
	 * different statuses -
	 * FAILURE - GHIX specific
	 * queued, sending, sent,failed, or received - TWILIO specific
	 */
	private String status;

	/** failure reason for API call throwing an exception */
	private String failureReason;

	/** The date that this resource was created */
	private Date dateCreated;

	/** The date that this resource was last updated */
	private Date dateUpdated;

	/** The date that the SMS was sent */
	private Date dateSent;

	public SmsResponse(String providerName, String toPhone, String fromPhone, String body, String status, String failureReason, String messageSID, String direction, Date dateCreated, Date dateUpdated, Date dateSent  ) {
		this.providerName = providerName;
		this.toPhone = toPhone;
		this.fromPhone = fromPhone;
		this.body = body;
		this.status = status;
		this.failureReason = failureReason;
		this.messageSID = messageSID;
		this.direction = direction;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
		this.dateSent = dateSent;
	}


	public String getProviderName() {
		return providerName;
	}


	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}


	public String getMessageSID() {
		return messageSID;
	}

	public void setMessageSID(String messageSID) {
		this.messageSID = messageSID;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFailureReason() {
		return failureReason;
	}

	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getToPhone() {
		return toPhone;
	}


	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}


	public String getFromPhone() {
		return fromPhone;
	}


	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}


	public String getBody() {
		return body;
	}


	public void setBody(String body) {
		this.body = body;
	}


	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	@Override
	public String toString() {
				return "SmsResponse [providerName=" + providerName + ", messageSID="
												+ messageSID + ", direction=" + direction + ", toPhone="
												+ toPhone + ", fromPhone=" + fromPhone + ", body=" + body
												+ ", status=" + status + ", failureReason=" + failureReason
												+ ", dateCreated=" + dateCreated + ", dateUpdated="
												+ dateUpdated + ", dateSent=" + dateSent + "]";
	}



	public Date getDateSent() {
		return dateSent;
	}


	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}

}
