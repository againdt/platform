package com.getinsured.hix.platform.multitenant.resolver.filter;

import com.getinsured.hix.model.BrandingConfigurationDTO;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.multitenant.resolver.exception.TenantRuntimeException;

public class TenantContextHolder {

	private static final ThreadLocal<TenantDTO> contextHolder = new InheritableThreadLocal<TenantDTO>();
	private static final ThreadLocal<Long> affiliateId = new InheritableThreadLocal<Long>();
	private static final ThreadLocal<String> requestUrl = new InheritableThreadLocal<String>();
	private static final ThreadLocal<String> ipAddress = new InheritableThreadLocal<String>();
	private static final ThreadLocal<Integer> flowId = new InheritableThreadLocal<Integer>();


	public static Long getAffiliateId() {
		return affiliateId.get();
	}

   public static void setAffiliateId(Long newAffiliateId) {
	   affiliateId.set(newAffiliateId);
   }
   
   public static Integer getFlowId() {
	   return flowId.get();
   }
   
   public static void setFlowId(Integer newFlowId) {
	   flowId.set(newFlowId);
   }
   
   public static void setTenant(TenantDTO tenant) {
      if (tenant == null) {
    	  throw new TenantRuntimeException("TenantContextHolder::setTenant ERROR Tenant Cannot be NULL");
      }
      contextHolder.set(tenant);
   }

   public static TenantDTO getTenant() {
      return contextHolder.get();
   }

   public static void clearTenant() {
      contextHolder.remove();
   }
   
   public static void clear() {
	   contextHolder.remove();
	   affiliateId.remove();
	   flowId.remove();
	   requestUrl.remove();
	   ipAddress.remove();
   }
   
	public static BrandingConfigurationDTO getBrandingConfiguration() {
		BrandingConfigurationDTO brandingConfiguration = null;
		if (getTenant() != null
				&& getTenant().getConfiguration() != null
				&& getTenant().getConfiguration().getUiConfiguration() != null
				&& getTenant().getConfiguration().getUiConfiguration().getBrandingConfiguration() != null) {
			brandingConfiguration = contextHolder.get().getConfiguration().getUiConfiguration().getBrandingConfiguration();
		}
		return brandingConfiguration;
	}

	public static String getRequesturl() {
		return requestUrl.get();
	}
	
	public static void setRequesturl(String url) {
		requestUrl.set(url);
	}
	
	public static String getIPAddress() {
		return ipAddress.get();
	}
	
	public static void setIPAddress(String ip) {
		ipAddress.set(ip);
	}
	
}