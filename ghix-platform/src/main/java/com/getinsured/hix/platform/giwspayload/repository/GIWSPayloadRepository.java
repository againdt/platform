package com.getinsured.hix.platform.giwspayload.repository;

import com.getinsured.hix.model.GIWSPayload;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("giwsPayloadRepository")
public interface GIWSPayloadRepository extends JpaRepository<GIWSPayload, Integer>{

	List<GIWSPayload> findByCreatedUserId(int id);

	/*Ekram : HIX-33045 - Added for Eligibility RP module */
	List<GIWSPayload> findByCorrelationIdAndEndpointFunction(String correlationId, String endpointFunction);

	List<GIWSPayload> findByCorrelationIdAndEndpointFunction(String correlationId, String endpointFunction, Sort sort);
	
	@Query("select g from GIWSPayload g where g.ssapApplicationId = :ssapApplicationId and g.endpointFunction = :endpointFunction  order by g.id desc")
	List<GIWSPayload> findBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId, @Param("endpointFunction") String endpointFunction);

	@Query("select g from GIWSPayload g where g.ssapApplicationId = :ssapApplicationId order by id desc")
	List<GIWSPayload> findBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId);

	@Query("select g from GIWSPayload g where g.customKeyId1 = :customKeyId1 order by id desc")
	List<GIWSPayload> findByCustomKeyId1(@Param("customKeyId1") String customKeyId1);
	
	@Modifying
	@Transactional
	@Query("update GIWSPayload set ssapApplicationId = :ssapApplicationId where id = :id")
	void updateSsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId, @Param("id") int id);

	@Modifying
	@Transactional
	@Query("update GIWSPayload set exceptionMessage = :exceptionMessage, status = :status where id = :id")
	void updateStatus(@Param("exceptionMessage") String exceptionMessage, @Param("status") String status,@Param("id") int id);
	
	GIWSPayload findById(Integer id);

}
