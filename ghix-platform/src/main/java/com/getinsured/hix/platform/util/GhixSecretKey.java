package com.getinsured.hix.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.platform.util.GhixHome;

/**
 * Provides method to retrieve secret encryption key from the properties file.
 */
@Component("ghixSecretKey")
public class GhixSecretKey
{
  private static final Logger logger = LoggerFactory.getLogger(GhixSecretKey.class);

  private static final String NONE = "NONE".intern();
  private static final String PROPERTY_ENCRYPTION_KEY = "ENCRYPTION_KEY".intern();
  private static final String DEFAULT_ENCRYPTION_KEY = "ghix123".intern();

  private static String encryptionKey;
  private static String ENCRYPTION_KEY_FILE = NONE;

  private static final String[] SECRET_KEY_FILE_NAMES = new String[] {
      "secretKey.txt".intern(), // phix5dev-app1.ghixqa.com has this file
      "SecretKey.txt".intern(),  // iddemo.ghixqa.com has this file
      "encryption-key.properties".intern()
  };

  public GhixSecretKey()
  {
    File secretKeyFile = null;

    for(String fn : SECRET_KEY_FILE_NAMES)
    {
      secretKeyFile = GhixHome.getFileInBaseConfigurationDirectory(fn);

      // If it exists, readable and normal file
      if(secretKeyFile.exists() && secretKeyFile.canRead() && secretKeyFile.isFile())
      {
        ENCRYPTION_KEY_FILE = secretKeyFile.getAbsolutePath();
        break;
      }
    }

    if(NONE.equals(ENCRYPTION_KEY_FILE))
    {
      if (logger.isErrorEnabled())
      {
        final String configDir = GhixHome.getBaseConfigurationDir().getAbsolutePath();
        logger.error("Failed to find encryption key file from {}/{} or from {}/{}",
            configDir, SECRET_KEY_FILE_NAMES[0], configDir, SECRET_KEY_FILE_NAMES[1]);
      }
    }

    initSecureProperties();
  }

  private void initSecureProperties()
  {
    Properties props = new Properties();
    if (ENCRYPTION_KEY_FILE.equals(NONE))
    {
      if (logger.isWarnEnabled())
      {
        logger.warn("No properties file provided or it doesn't exist: [{}]; will use default secret key", ENCRYPTION_KEY_FILE);
      }
    }
    else
    {
      try
      {
        props.load(new FileInputStream(ENCRYPTION_KEY_FILE));
      }
      catch (IllegalArgumentException | IOException e)
      {
        if (logger.isErrorEnabled())
        {
          logger.error("Error loading the secure properties from file: [{}] failed with exception: {}", ENCRYPTION_KEY_FILE, e.getMessage());
          logger.error("Exception stacktrace", e);
        }
      }
    }

    if (props.size() == 0)
    {
      if (logger.isWarnEnabled())
      {
        logger.warn("No secure properties available from file: {}, does {} property exists in file?", ENCRYPTION_KEY_FILE, PROPERTY_ENCRYPTION_KEY);
      }
    }
    else
    {
      encryptionKey = props.getProperty(PROPERTY_ENCRYPTION_KEY);
    }

    if (encryptionKey == null)
    {
      if (logger.isErrorEnabled())
      {
        logger.error("Encryption key is a mandatory property required to run the application, using default, change as soon as possible");
      }

      encryptionKey = DEFAULT_ENCRYPTION_KEY;
    }
  }

  /**
   * Returns secret key assigned to property {@link #PROPERTY_ENCRYPTION_KEY} or default value {@link #DEFAULT_ENCRYPTION_KEY}.
   *
   * @return encryption key.
   */
  public static String getEncryptionKey()
  {
    return encryptionKey == null ? DEFAULT_ENCRYPTION_KEY : encryptionKey;
  }
}
