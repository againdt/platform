package com.getinsured.hix.platform.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.timeshift.TimeshiftContext;

public class GhixSSORequestInterceptor implements HttpRequestInterceptor {
	private static final String ssoHeader = "GHIX_SSO_USER";
	private static final String CONTENT_TYPE_HEADER = "Content-Type";
	private static Set<String> hostSet = new HashSet<String>();
	private static HashMap<String, String> hostMap = new HashMap<String, String>();
	private static Logger logger = LoggerFactory.getLogger(GhixSSORequestInterceptor.class);
	
	public GhixSSORequestInterceptor() {
		String ssoHostNameList = GhixPlatformConstants.SSOHOST_IP_LIST;
		String[] hostList = ssoHostNameList.split(",");
		for(String tmp: hostList){
			logger.info("Adding Intra SSO node:"+tmp);
			hostSet.add(tmp);
		}
	}
	
	private String resetHostHeader(HttpRequest request, HttpContext context) {
		Header host = request.getFirstHeader(HTTP.TARGET_HOST);
		String headerHost = host.getValue();
		final HttpCoreContext corecontext = HttpCoreContext.adapt(context);
		HttpHost contextTargetHost = (HttpHost)corecontext.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
		String contextHost = contextTargetHost.getHostName();
		if(contextHost.equals(headerHost)) {
			return contextHost;
		}
		if(logger.isInfoEnabled()) {
			logger.info("Host mismatch found, replacing {} with {}", headerHost,contextHost);
		}
		request.removeHeaders("Host");
		request.addHeader(HTTP.TARGET_HOST, contextHost);
		return contextHost;
	}
	
	@Override
	public void process(HttpRequest request, HttpContext context)
			throws HttpException, IOException {
		String currentRoleName = "not available";
		String clientId = getUserName();
		currentRoleName = getCurrentRole();
		String hostStr = resetHostHeader(request,context);
		if(hostStr != null){
			String hostName = hostMap.get(hostStr);
			if(hostName == null){
				int colonIdx = hostStr.indexOf(":");
				if(colonIdx != -1){
					hostName = hostStr.substring(0,colonIdx);
				}else{
					hostName = hostStr;
				}
				hostMap.put(hostStr, hostName);
			}
			if(hostName != null && hostSet.contains(hostName)){
				if(clientId == null){
					clientId = "Anonymous";
				}
				if(logger.isTraceEnabled()){
					logger.trace("Adding SSO information for outbound target:"+hostStr+" with client Id:"+clientId);
				}
				if(!request.containsHeader(ssoHeader)){
					request.addHeader(ssoHeader, clientId);
				}
				if(currentRoleName != null){
					request.addHeader("X-USER-ROLE", currentRoleName.toUpperCase());
				}
				request.addHeader("X-REQUESTER-ID".intern(),"IP:"+GhixPlatformConstants.LOCAL_NODE_IP);
			}else{
				if(logger.isTraceEnabled()){
					logger.trace("Outbound connection to "+hostStr+" No SSO information provided, could be a third party server");
				}
			}
			boolean isPost = request.getRequestLine().getMethod().equals("POST");
			if(logger.isDebugEnabled()) {
				logger.debug("Hostname {}, HUB host {}, POST {}", hostName, GhixPlatformConstants.HUB_INTEGRATION_HOST, isPost);
			}
			checkAndUpdateHeaderForFDSH(request, hostName, isPost);
			
		}
		// Override Accept-Encoding header, Deflate doesn't work
		request.removeHeaders("Accept-Encoding");
		request.addHeader("Accept-Encoding","gzip");
		addTSContext(request,clientId);
	}
	
	
	
	private void checkAndUpdateHeaderForFDSH(HttpRequest request, String hostName, boolean isPost) {
		if(hostName != null && !hostName.equalsIgnoreCase("localhost") &&
				hostName.equals(GhixPlatformConstants.HUB_INTEGRATION_HOST) &&
					isPost) {
			request.removeHeaders("GHIX_SSO_USER");
			request.removeHeaders("X-USER");
			request.removeHeaders("requestCorrelationId");
			request.removeHeaders("Accept-Charset");
			request.removeHeaders("Cache-Control");
			request.removeHeaders("Pragma");
			request.removeHeaders("connection");
			request.addHeader("Connection","Keep-Alive");
			request.removeHeaders("Accept");
			request.addHeader("Accept", "application/json, application/*+json");
			Header[] h = request.getHeaders(CONTENT_TYPE_HEADER);
			if(h == null || h.length == 0) {
				logger.warn("No content type provided for outbound FDSH call, assuming JSON");
				request.removeHeaders(CONTENT_TYPE_HEADER);
				request.addHeader(CONTENT_TYPE_HEADER,"application/json");
			}
		}
	}

	private void addTSContext(HttpRequest request, String clientId) {
		request.removeHeaders("X-USER".intern());
		request.addHeader("X-USER".intern(),clientId);
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			// First remove the TS if exists
			request.removeHeaders("X-USER-OFFSET".intern());
			Header[] target = request.getHeaders("Target".intern());
			boolean outBoundTs = false;
			for(Header hdr: target) {
				if(hdr.getValue().equals("TS".intern())) {
					outBoundTs = true;
					break;
				}
			}
			// If outbound is for TS, we can not add the TS context
			if(!outBoundTs) {
				String offset = TimeshiftContext.getCurrent().getTimeOffset();
				logger.info("Adding TS Context to outbound call URL {}, user {}, offset {}".intern(), request.getRequestLine().getUri(),clientId,offset );
				request.addHeader("X-USER-OFFSET".intern(),offset);
			}else {
				logger.info("No TimeContext provided for outbound call to {}", request.getRequestLine());
			}
		}
	}

	private String getUserName() {
		String clientId = null;
		boolean sec = false;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null){
			Object principle = auth.getPrincipal();
			if(principle instanceof AccountUser ){
				sec = true;
				AccountUser temp = (AccountUser)principle;
				clientId = temp.getUsername();
			}
		}
		if(clientId == null) {
			// Check the timeshifter context
			clientId = TimeshiftContext.getCurrent().getUserName();
		}
		logger.info("User context available for outbound call {} from Sec  Context {}",clientId, sec);
		return clientId;
	}

	private String getCurrentRole() {
		String currentRoleName = null;
		try {
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if(sra != null){
				HttpServletRequest req = sra.getRequest();
				HttpSession session = req.getSession(false);
				if(session != null){
					currentRoleName = (String)session.getAttribute("userActiveRoleName");
				}
			}else{
				logger.warn("No Request context available for outbound call originating from "+GhixPlatformConstants.LOCAL_NODE_IP);
			}
		}catch(Exception e) {
			logger.warn("Error extracting the user current role for outbound call, ignoring", e.getMessage());
		}
		return currentRoleName;
	}
}
