package com.getinsured.hix.platform.util.exception;

/**
 * Created by jabelardo on 4/21/14.
 */
public class FieldCalculatorNotFoundException extends Exception {
    public FieldCalculatorNotFoundException(String name) {
        super(String.format("\"%s\" is not a valid field type name", name));
    }
}
