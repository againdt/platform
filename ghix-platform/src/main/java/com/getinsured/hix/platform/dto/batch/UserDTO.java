/**
 * 
 */
package com.getinsured.hix.platform.dto.batch;


/**
 * @author Vijay D
 *
 */
public class UserDTO {
	
	private int id;
	private String userName;
	private String ffmUserId;
	private String userNPN;
	private String ffmPassword;
	private String applicationData;
	private Integer securableTargetId;
	
	public UserDTO(int id, String userName, String ffmUserId, String userNPN, String ffmPassword, String applicationData,Integer securableTargetId){
		this.id = id;
		this.userName = userName;
		this.ffmUserId = ffmUserId;
		this.userNPN = userNPN;
		this.ffmPassword = ffmPassword;
		this.applicationData = applicationData;
		this.securableTargetId = securableTargetId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFfmUserId() {
		return ffmUserId;
	}
	public void setFfmUserId(String ffmUserId) {
		this.ffmUserId = ffmUserId;
	}
	public String getUserNPN() {
		return userNPN;
	}
	public void setUserNPN(String userNPN) {
		this.userNPN = userNPN;
	}
	public String getApplicationData() {
		return applicationData;
	}
	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public String getFfmPassword() {
		return ffmPassword;
	}

	public void setFfmPassword(String ffmPassword) {
		this.ffmPassword = ffmPassword;
	}

	public Integer getSecurableTargetId() {
		return securableTargetId;
	}

	public void setSecurableTargetId(Integer securableTargetId) {
		this.securableTargetId = securableTargetId;
	}
	
		
}
