package com.getinsured.hix.platform.sms.service;

import com.getinsured.hix.platform.sms.SmsResponse;

/**
 * SmsSrvice interface to communicate with third party SMS providers.
 * 
 * @author Ekram Ali Kazi
 *
 */
public interface SmsService {

	/**
	 * To send SMS using third party API.
	 * 
	 * @param toPhone, not null
	 * @param body,not null
	 * @param pin, not null
	 * @return SmsResponse object
	 * @throws IllegalArgumentException, if toPhone/body/pin is null and body.length > 160
	 */
	SmsResponse send(String toPhone, String body, String pin);
	SmsResponse call(String toPhone, String body, String pin);

}
