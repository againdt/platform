package com.getinsured.hix.platform.dto.address;

/**
 * 
 * @author Sunil Desu
 *
 */
public class LocationDTO {

	private String addressLine1;
	private String addressLine2;
	private String city;
	private String countyName;
	private String countyCode;
	private String state;
	private String zipcode;
	
	private boolean redundantSecondaryNumber;

	private boolean zipcodeCorrected;
	private boolean cityStateSpellingFixed;
	private boolean zipInvalid;
	private boolean addressNotFound;
	private boolean missingSecondaryNumber;
	private boolean insufficientAddressData;
	private boolean fixedStreetSpelling;
	private boolean fixedAbreviations;
	private boolean invalidSecondaryAddress;
	private boolean cityStateZipComboInvalid;
	private boolean invalidDeliveryAddress;
	private boolean otherReason;

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public boolean isRedundantSecondaryNumber() {
		return redundantSecondaryNumber;
	}

	public void setRedundantSecondaryNumber(boolean redundantSecondaryNumber) {
		this.redundantSecondaryNumber = redundantSecondaryNumber;
	}

	public boolean isZipcodeCorrected() {
		return zipcodeCorrected;
	}

	public void setZipcodeCorrected(boolean zipcodeCorrected) {
		this.zipcodeCorrected = zipcodeCorrected;
	}

	public boolean isCityStateSpellingFixed() {
		return cityStateSpellingFixed;
	}

	public void setCityStateSpellingFixed(boolean cityStateSpellingFixed) {
		this.cityStateSpellingFixed = cityStateSpellingFixed;
	}

	public boolean isZipInvalid() {
		return zipInvalid;
	}

	public void setZipInvalid(boolean zipInvalid) {
		this.zipInvalid = zipInvalid;
	}

	public boolean isAddressNotFound() {
		return addressNotFound;
	}

	public void setAddressNotFound(boolean addressNotFound) {
		this.addressNotFound = addressNotFound;
	}

	public boolean isMissingSecondaryNumber() {
		return missingSecondaryNumber;
	}

	public void setMissingSecondaryNumber(boolean missingSecondaryNumber) {
		this.missingSecondaryNumber = missingSecondaryNumber;
	}

	public boolean isInsufficientAddressData() {
		return insufficientAddressData;
	}

	public void setInsufficientAddressData(boolean insufficientAddressData) {
		this.insufficientAddressData = insufficientAddressData;
	}

	public boolean isFixedStreetSpelling() {
		return fixedStreetSpelling;
	}

	public void setFixedStreetSpelling(boolean fixedStreetSpelling) {
		this.fixedStreetSpelling = fixedStreetSpelling;
	}

	public boolean isFixedAbreviations() {
		return fixedAbreviations;
	}

	public void setFixedAbreviations(boolean fixedAbreviations) {
		this.fixedAbreviations = fixedAbreviations;
	}

	public boolean isInvalidSecondaryAddress() {
		return invalidSecondaryAddress;
	}

	public void setInvalidSecondaryAddress(boolean invalidSecondaryAddress) {
		this.invalidSecondaryAddress = invalidSecondaryAddress;
	}

	public boolean isCityStateZipComboInvalid() {
		return cityStateZipComboInvalid;
	}

	public void setCityStateZipComboInvalid(boolean cityStateZipComboInvalid) {
		this.cityStateZipComboInvalid = cityStateZipComboInvalid;
	}

	public boolean isInvalidDeliveryAddress() {
		return invalidDeliveryAddress;
	}

	public void setInvalidDeliveryAddress(boolean invalidDeliveryAddress) {
		this.invalidDeliveryAddress = invalidDeliveryAddress;
	}

	public boolean isOtherReason() {
		return otherReason;
	}

	public void setOtherReason(boolean otherReason) {
		this.otherReason = otherReason;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LocationDTO [addressLine1=");
		builder.append(addressLine1);
		builder.append(", addressLine2=");
		builder.append(addressLine2);
		builder.append(", city=");
		builder.append(city);
		builder.append(", countyName=");
		builder.append(countyName);
		builder.append(", countyCode=");
		builder.append(countyCode);
		builder.append(", state=");
		builder.append(state);
		builder.append(", zipcode=");
		builder.append(zipcode);
		builder.append(", redundantSecondaryNumber=");
		builder.append(redundantSecondaryNumber);
		builder.append(", zipcodeCorrected=");
		builder.append(zipcodeCorrected);
		builder.append(", cityStateSpellingFixed=");
		builder.append(cityStateSpellingFixed);
		builder.append(", zipInvalid=");
		builder.append(zipInvalid);
		builder.append(", addressNotFound=");
		builder.append(addressNotFound);
		builder.append(", missingSecondaryNumber=");
		builder.append(missingSecondaryNumber);
		builder.append(", insufficientAddressData=");
		builder.append(insufficientAddressData);
		builder.append(", fixedStreetSpelling=");
		builder.append(fixedStreetSpelling);
		builder.append(", fixedAbreviations=");
		builder.append(fixedAbreviations);
		builder.append(", invalidSecondaryAddress=");
		builder.append(invalidSecondaryAddress);
		builder.append(", cityStateZipComboInvalid=");
		builder.append(cityStateZipComboInvalid);
		builder.append(", invalidDeliveryAddress=");
		builder.append(invalidDeliveryAddress);
		builder.append(", otherReason=");
		builder.append(otherReason);
		builder.append("]");
		return builder.toString();
	}

}
