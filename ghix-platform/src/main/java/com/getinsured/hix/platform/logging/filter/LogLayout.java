package com.getinsured.hix.platform.logging.filter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LocationInfo;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.couchbase.dto.CouchMeta;
import com.google.gson.Gson;

public class LogLayout extends Layout {

	public void activateOptions() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public String format(LoggingEvent event) {

		CouchbaseLog logEntry = new CouchbaseLog();

		// Couchbase Metadata
		CouchMeta couchMeta = new CouchMeta();
		
		couchMeta.setCategory("PLAT");
		couchMeta.setSubCategory("LOG");
		couchMeta.setCreationDate(new TSDate(event.getTimeStamp()));
		
		logEntry.setMetaData(couchMeta);

		// Log Data
		logEntry.setLogLevel(event.getLevel().toString());
		logEntry.setLoggerName(event.getLoggerName());
		logEntry.setThreadName(event.getThreadName());
		logEntry.setLogTimestamp(new TSDate(event.getTimeStamp()));
		addHostInfo(logEntry, event);
		logEntry.setMessage(event.getRenderedMessage());
		addLocationInfo(logEntry, event);
		
		if (event.getThrowableInformation() != null) {
			addExceptionInfo(logEntry, event);
		}

		logEntry.setMdc(event.getProperties());
		logEntry.setNdc(event.getNDC());
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return gson.toJson(logEntry);
	}

	private void addLocationInfo(CouchbaseLog logEntry, LoggingEvent event) {
		final LocationInfo locationInfo = event.getLocationInformation();
		logEntry.getLocationInfo().put("className", locationInfo.getClassName());
		logEntry.getLocationInfo().put("fileName", locationInfo.getFileName());
		logEntry.getLocationInfo().put("lineNumber", locationInfo.getLineNumber());
		logEntry.getLocationInfo().put("methodName", locationInfo.getMethodName());
		logEntry.getLocationInfo().put("fullInfo", locationInfo.fullInfo);
	}

	private void addExceptionInfo(CouchbaseLog logEntry, LoggingEvent event) {

		final ThrowableInformation throwableInformation = event.getThrowableInformation();

		if (throwableInformation.getThrowable().getClass().getCanonicalName() != null) {
			logEntry.getExceptionInfo().put("canonicalName", throwableInformation.getThrowable().getClass().getCanonicalName());
		}
		if (throwableInformation.getThrowable().getMessage() != null) {
			logEntry.getExceptionInfo().put("message", throwableInformation.getThrowable().getMessage());
		}
		if (throwableInformation.getThrowableStrRep() != null) {
			String stackTrace = StringUtils.join(throwableInformation.getThrowableStrRep(), "\n");
			logEntry.getExceptionInfo().put("stacktrace", stackTrace);
		}
	}

	@Override
	public boolean ignoresThrowable() {
		return false;
	}

	private void addHostInfo(CouchbaseLog logEntry, LoggingEvent event) {
		try {
			String hostName = InetAddress.getLocalHost().getHostName();
			logEntry.setHostName(hostName);

			String hostAddress = InetAddress.getLocalHost().getHostAddress();
			logEntry.setHostAddress(hostAddress);

		} catch (UnknownHostException e) {
			// Do nothing
		}
	}
}
