package com.getinsured.hix.platform.security.repository;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

import javax.annotation.PostConstruct;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.getinsured.hix.model.AccountUser;


public class CustomLdapUserRepository {
	private static final Logger logger = LoggerFactory.getLogger(CustomLdapUserRepository.class);

	private Hashtable<String, String> ldapContext = null;
	private Hashtable<String, String> ghixLdapAttriTable = new Hashtable<String, String>();
	//private Hashtable<String, String> ghixAttriValueTable = new Hashtable<String, String>();
	private Hashtable<String, String> ghixRoleNameMapTable = new Hashtable<String, String>();	
	String provideUrl;
	String repoBase;
	String managerBase;
	String managerPassword;
	String searchUserBase;
	String searchRoleBase;
	String ghixLdapAttriMap;//="userName-uid;email-mail;firstName-givenName;lastName-sn";
	String ghixRoleNameMap;
	String userSearchObjClass;	
	String userSearchKeyAttribute;
	String roleSearchObjClass;	
	String roleSearchExperssion;
	String roleAttribute;

	
	
	public CustomLdapUserRepository() {
		
	}
	
	public Hashtable<String, String> getLdapContext() {
		return ldapContext;
	}

	public void setLdapContext(Hashtable<String, String> ldapContext) {
		this.ldapContext = ldapContext;
	}

	private void initEnvContext() {
		try {
			if(ldapContext==null){
				ldapContext = new Hashtable<String, String>();
				ldapContext.put(Context.INITIAL_CONTEXT_FACTORY,
						"com.sun.jndi.ldap.LdapCtxFactory");
				logger.debug("PROVIDER_URL : " + provideUrl+"/"+repoBase);
				ldapContext.put(Context.PROVIDER_URL, provideUrl+"/"+repoBase);
				logger.debug("SECURITY_PRINCIPAL : " + managerBase);
				ldapContext.put(Context.SECURITY_PRINCIPAL, managerBase);
				logger.debug("SECURITY_CREDENTIALS : " + managerPassword);
				ldapContext.put(Context.SECURITY_CREDENTIALS, managerPassword);
			}			

		} catch (Exception e) {
			logger.error("Exception while initializing Environment Context : " + e.getMessage());
		}
	}
	
	private void initGhixLdapAttriTable(){
		String[] ghixLdapAttriPairs=StringUtils.split(ghixLdapAttriMap, ";");
		
		ghixLdapAttriTable.clear();
		//ghixAttriValueTable.clear();
		for(int idx=0;idx<ghixLdapAttriPairs.length;idx++){
			String ghixAttri=StringUtils.substringBefore(ghixLdapAttriPairs[idx], "-");
			String ldapAttri=StringUtils.substringAfter(ghixLdapAttriPairs[idx], "-");
			
			ghixLdapAttriTable.put(ghixAttri, ldapAttri);
			//ghixAttriValueTable.put(ghixAttri, ""); // initialize with empty string
		}
	}
	

	public String getProvideUrl() {
		return provideUrl;
	}


	public void setProvideUrl(String provideUrl) {
		this.provideUrl = provideUrl;
	}



	public String getRepoBase() {
		return repoBase;
	}

	public void setRepoBase(String repoBase) {
		this.repoBase = repoBase;
	}


	public String getManagerBase() {
		return managerBase;
	}


	public void setManagerBase(String managerBase) {
		this.managerBase = managerBase;
	}


	public String getManagerPassword() {
		return managerPassword;
	}


	public void setManagerPassword(String managerPassword) {
		this.managerPassword = managerPassword;
	}


	public String getSearchUserBase() {
		return searchUserBase;
	}


	public void setSearchUserBase(String searchUserBase) {
		this.searchUserBase = searchUserBase;
	}


	public String getSearchRoleBase() {
		return searchRoleBase;
	}


	public void setSearchRoleBase(String searchRoleBase) {
		this.searchRoleBase = searchRoleBase;
	}

	public String getGhixLdapAttriMap() {
		return ghixLdapAttriMap;
	}

	public void setGhixLdapAttriMap(String ghixLdapAttriMap) {
		this.ghixLdapAttriMap = ghixLdapAttriMap;
	}
	
	
	public String getUserSearchObjClass() {
		return userSearchObjClass;
	}

	public void setUserSearchObjClass(String userSearchObjClass) {
		this.userSearchObjClass = userSearchObjClass;
	}

	public String getUserSearchKeyAttribute() {
		return userSearchKeyAttribute;
	}

	public void setUserSearchKeyAttribute(String userSearchKeyAttribute) {
		this.userSearchKeyAttribute = userSearchKeyAttribute;
	}
	
	

	public String getRoleSearchObjClass() {
		return roleSearchObjClass;
	}

	public void setRoleSearchObjClass(String roleSearchObjClass) {
		this.roleSearchObjClass = roleSearchObjClass;
	}


	
	
	public String getRoleSearchExperssion() {
		return roleSearchExperssion;
	}

	public void setRoleSearchExperssion(String roleSearchExperssion) {
		this.roleSearchExperssion = roleSearchExperssion;
	}

	public String getRoleAttribute() {
		return roleAttribute;
	}

	public void setRoleAttribute(String roleAttribute) {
		this.roleAttribute = roleAttribute;
	}

	public String getGhixRoleNameMap() {
		return ghixRoleNameMap;
	}

	public void setGhixRoleNameMap(String ghixRoleNameMap) {
		this.ghixRoleNameMap = ghixRoleNameMap;
	}
	
	public AccountUser findLdapUser(String userName) {
		AccountUser ldapAccountUser = null;
		DirContext ctx  = null;
		HashMap<String, String> ghixAttriValueTable = new HashMap<String, String>();
		try {
			initEnvContext();
			initGhixLdapAttriTable();
			
			if(logger.isDebugEnabled()){
				logger.debug("LDAP Context Attributes : "+ldapContext);
			}
			
			ctx = new InitialDirContext(ldapContext);
			
			if(logger.isDebugEnabled()){
				logger.debug("LDAP Intitial Directory Context : "+ctx);
			}
			
			String base = searchUserBase;//"ou=users";

			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			//String filter = "(&("+searchObjClass+")(uid=" + userEmail + "))";
			String filter = "(&("+userSearchObjClass+")("+this.userSearchKeyAttribute+"=" + userName + "))";
			NamingEnumeration<SearchResult> results = ctx.search(base, filter, sc);

			if(logger.isDebugEnabled()){
				logger.debug("Directory Context Search Results : "+results);
			}
			
			logger.info("Directory Context Search Results : "+results);
			
			if (results.hasMore()) {
				ldapAccountUser = new AccountUser();
				SearchResult sr = results.next();
				Attributes ldapAttrs = sr.getAttributes();
				
				Enumeration<String> ldapAttriEnum=ghixLdapAttriTable.keys();
				while(ldapAttriEnum.hasMoreElements()){
					String ghixAttir=ldapAttriEnum.nextElement();
					String ldapAttir=ghixLdapAttriTable.get(ghixAttir);
					
					Attribute uid = ldapAttrs.get(ldapAttir);
					if (uid != null){
						
						String ldapValue = getAttributeValue(uid);
						
						if(StringUtils.isNotBlank(ldapValue)){
							logger.debug("LDAP Attribute Name : "+ghixAttir+" : Value : "+ldapValue);
							ghixAttriValueTable.put(ghixAttir, ldapValue);
						}
						
					}
					
				}
				String tmp = this.getLdapAttribute(ghixAttriValueTable, "userName", true);
				ldapAccountUser.setUserName(tmp);
				
				tmp = this.getLdapAttribute(ghixAttriValueTable,"email",false);
				ldapAccountUser.setEmail(tmp);
				
				tmp = getLdapAttribute(ghixAttriValueTable,"firstName",true);
				ldapAccountUser.setFirstName(tmp);
				
				tmp = getLdapAttribute(ghixAttriValueTable,"lastName",true);
				ldapAccountUser.setLastName(tmp);
				
				tmp = getLdapAttribute(ghixAttriValueTable,"recordId",false);
				if(tmp != null && tmp.length()>0){
					ldapAccountUser.setRecordId(tmp);
					ldapAccountUser.setActiveModuleId(Integer.parseInt(tmp));
				}
				
				tmp = getLdapAttribute(ghixAttriValueTable,"recordType",false);
				if(tmp != null && tmp.length()>0){
					ldapAccountUser.setRecordType(tmp);
					ldapAccountUser.setActiveModuleName(tmp);
				}
				
				tmp = getLdapAttribute(ghixAttriValueTable,"deligationAccessCode",false);
				if(tmp != null && tmp.length()>0){
					ldapAccountUser.setDeligationAccessCode(ghixAttriValueTable.get("deligationAccessCode"));
				}
				
				tmp = getLdapAttribute(ghixAttriValueTable,"externPin",false);
				if(tmp != null && tmp.length()>0){
					ldapAccountUser.setExternPin(tmp); // Refer: JIRA : HIX-9827 ;Expose PIN in the Account User object
				}
				
			}
			return ldapAccountUser;
		} catch (Exception e) {
			logger.error("Exception while finding LDAP User : " + e.getMessage());
			
			logger.error(" Adding Exception \n"+e);
			
			return null;
		}finally{
			try {
				ctx.close();
			} catch (NamingException e) {
				//Ignored
			}
		}
	}
	
	
	

	private String getLdapAttribute(HashMap<String, String> ghixAttriValTable, String attrName, boolean mandatory) {
		String val = ghixAttriValTable.get(attrName);
		
		logger.info ("Show LDAP Attribute Name Received ::: "+attrName);
		logger.info ("Show LDAP Attribute Value Received ::: "+val);
		
		if(val != null){
			logger.info ("Show LDAP Attribute Value Size ::: "+val.length());
		}
		
		if(val == null){
			if(mandatory){
				logger.error("Mandatory attribute :"+attrName+" is not available");
				throw new UsernameNotFoundException("Failed to find user, mandatory attribute:"+attrName+" is not available");
			}else{
				logger.warn("Attribute :"+attrName+" is not available");
			}
		}
		return val;
	}

	public String searchRole(String userName) {
		String userRole="anonymous";
		
		try {
			initEnvContext();
			logger.debug("LDAP Context Attributes : "+ldapContext);
			DirContext ctx = new InitialDirContext(ldapContext);
			String base = searchRoleBase;
			//String uniquemember = this.roleSearchKeyAttribute+"=" + userName+","+this.searchUserBase+","+this.repoBase;// ",ou=users,dc=ghixldap,dc=com";
			String uniquemember = StringUtils.replace(this.roleSearchExperssion, "$userName", userName);

			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			//String filter = "(&(objectclass=groupOfUniqueNames)(uniquemember="+ uniquemember + "))";
			String filter = "(&("+roleSearchObjClass+")("+ uniquemember + "))";
			NamingEnumeration results = ctx.search(base, filter, sc);
			logger.debug("Directory Context Search Results : "+results);

			while (results.hasMore()) {
				SearchResult sr = (SearchResult) results.next();
				Attributes attrs = sr.getAttributes();
				logger.debug("LDAP User Role Attribute : "+roleAttribute);
				Attribute roleAttr = attrs.get(roleAttribute);//"cn");
				if (roleAttr != null){
					userRole = getAttributeValue(roleAttr);//.toString();
					logger.debug("LDAP User Role : "+userRole);
				}
			}
			ctx.close();
			return userRole;
		} catch (Exception e) {
			logger.error("Exception while searching Role : " + e.getMessage());
			return userRole;
		}
	}

	public String getAttributeValue(Attribute attr) {
		String value=attr.toString();
		value=StringUtils.substringAfter(value, ":");
		
		value=StringUtils.trim(value);
		return value;
		
	}
	
	public String getGhixRoleName(String externRoleName){
		// Convert the externRoleName to upper case as the hashmap is created with the key in upper case
		String strExternalRoleName = (externRoleName == null) ? externRoleName : externRoleName.toUpperCase();
		String ghixRoleName=ghixRoleNameMapTable.get(strExternalRoleName);
		if(StringUtils.isBlank(ghixRoleName)){
			logger.debug("No explicit Ghix Role Mapping for ::"+externRoleName);
			ghixRoleName=externRoleName;
		}
		logger.debug("Ghix Role Name Mapping for external role name ("+externRoleName+") ::"+ghixRoleName);
		return ghixRoleName;
	}
	
	@PostConstruct
	private void loadSecurityData(){
		
		logger.info("Loading External Role Mappings ::.......");
		
		String[] roleMappingPairs=StringUtils.split(ghixRoleNameMap, ";");
		
		ghixRoleNameMapTable.clear();
		for(int idx=0;idx<roleMappingPairs.length;idx++){
			String ghixRole=StringUtils.substringBefore(roleMappingPairs[idx], "-");
			String ldapRole=StringUtils.substringAfter(roleMappingPairs[idx], "-");
			
			ghixRoleNameMapTable.put(ldapRole,ghixRole);
		}
		
		logger.info("No of External Role Mappings ::" + ghixRoleNameMapTable.size());
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*LOCAL Configuration */
		/* String provideUrl = "ldap://localhost:10389";
		String repoBase="dc=ghixldap,dc=com";//ldap.url/+ldap.repoBase
		String managerBase = "uid=admin,ou=system";
		String managerPassword = "secret";
		String searchUserBase="ou=users";
		String searchRoleBase="ou=groups";
		String ghixLdapAttriMap="userName-uid;email-mail;firstName-givenName;lastName-sn";		
		String userSearchObjClass="objectclass=*";
		String userSearchKeyAttribute="uid";
		String roleSearchObjClass="objectclass=groupOfUniqueNames";				
		String roleSearchExperssion="uniquemember=uid=$userName,ou=users,dc=ghixldap,dc=com";
		String roleAttribute="cn";
		*/
		
		/*OAM Configuration */
		/* String provideUrl = "ldap://10.166.129.14:3060";
		String repoBase="dc=cloud,dc=opsource,dc=net";//ldap.url/+ldap.repoBase
		String managerBase = "cn=orcladmin";
		String managerPassword = "apsp4Security";
		String searchUserBase="cn=Users";
		String searchRoleBase="cn=Groups";
		String ghixLdapAttriMap="userName-cn;email-mail;firstName-givenName;lastName-sn;recordId-teletexTerminalIdentifier;recordType-title";
		String userSearchObjClass="objectclass=*";		
		String userSearchKeyAttribute="cn";
		String roleSearchObjClass="objectclass=groupOfUniqueNames";		
		String roleSearchExperssion="uniquemember=cn=$userName,cn=Users,dc=cloud,dc=opsource,dc=net";
		String roleAttribute="cn";
		*/
		/*Ghix CA Configuration */
		/* */
		
		/*String provideUrl = "ldap://ghixca.com:10389";
		String repoBase="dc=example,dc=com";
		String managerBase = "uid=admin,ou=system";
		String managerPassword = "secret";
		String searchUserBase="ou=Users";
		String searchRoleBase="ou=Users";
		String ghixLdapAttriMap="userName-mail;email-mail;firstName-givenName;lastName-sn";
		String userSearchObjClass="objectclass=*";		
		String userSearchKeyAttribute="mail";
		String roleSearchObjClass="objectclass=*";		
		String roleSearchExperssion="mail=$userName";
		String roleAttribute="title";
		
		
	        
		Hashtable<String, String> testLdapContext = new Hashtable<String, String>();
		testLdapContext.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		testLdapContext.put(Context.PROVIDER_URL, provideUrl+"/"+repoBase);
		testLdapContext.put(Context.SECURITY_PRINCIPAL, managerBase);
		testLdapContext.put(Context.SECURITY_CREDENTIALS, managerPassword);
		
		CustomLdapUserRepository ldapService = new CustomLdapUserRepository();
		ldapService.setRepoBase(repoBase);
		ldapService.setSearchUserBase(searchUserBase);
		ldapService.setSearchRoleBase(searchRoleBase);
		ldapService.setUserSearchObjClass(userSearchObjClass);
		ldapService.setUserSearchKeyAttribute(userSearchKeyAttribute);
		ldapService.setRoleSearchObjClass(roleSearchObjClass);		
		ldapService.setRoleSearchExperssion(roleSearchExperssion);
		ldapService.setRoleAttribute(roleAttribute);
		ldapService.setGhixLdapAttriMap(ghixLdapAttriMap);

		
		ldapService.setLdapContext(testLdapContext);
		
		String localTestUser = "testuser001@vimo.com";
		String oamTestUser = "JamesTom1234";//"joansmit";
		String ghixcaTestUser = "tscott@broker.com";
		String userEmail = ghixcaTestUser;//
		
		ldapService.findLdapUser(userEmail);
		String userRole=ldapService.searchRole(userEmail);
		AccountUser ldapAccountUser = ldapService.findLdapUser(userEmail);*/

		//System.out.println("LdapService :: UserName ::"+ ldapAccountUser.getUserName());
		//logger.info("LdapService :: UserId ::"+ ldapAccountUser.getId());
		//System.out.println("LdapService :: Email ::"+ ldapAccountUser.getEmail());
		//logger.info("LdapService :: Email ::"+ ldapAccountUser.getEmail());
		//System.out.println("LdapService :: User Role ::"+ userRole);
		//logger.info("LdapService :: User Role ::"+ userRole);
	}

}
