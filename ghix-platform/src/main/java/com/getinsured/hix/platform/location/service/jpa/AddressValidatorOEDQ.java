package com.getinsured.hix.platform.location.service.jpa;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.dto.address.AddressValidationResponse;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

/**
 * This implements Address Validator with OEDQ - AHBX integration.
 *
 * @author Ekram Ali Kazi
 */
public class AddressValidatorOEDQ implements AddressValidatorComponent {

	private static final Logger LOGGER = Logger.getLogger(AddressValidatorOEDQ.class);



	@Autowired private RestTemplate restTemplate;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.getinsured.hix.platform.location.service.LocationValidatorService#
	 * validateLocation(com.getinsured.hix.platform.location.model.Location)
	 */
	@Override
	public List<Location> validateAddress(Location address) throws Exception {

		/**
		 * form platformRequest for address
		 */
		PlatformRequest platformRequest = new PlatformRequest();
		platformRequest.setAddress(address);

		/**
		 * Rest WS call to GHIX-AHBX module
		 */
		String ahbxResponse = restTemplate.postForObject(AHBXEndPoints.PLATFORM_VALIDATE_ADDRESS_OEDQ, platformRequest, String.class);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamGenericObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX OEDQ call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			LOGGER.warn("AHBX OEDQ call failed - " +  platformResponse.getErrCode() + platformResponse.getErrMsg());
			throw new GIException(platformResponse.getErrCode(), platformResponse.getErrMsg(), "WARN");
		}

		return platformResponse.getValidAddressList();
	}

	@Override
	public AddressValidationResponse validateAddress(LocationDTO address)
			throws Exception {
		List<LocationDTO> suggestions = new ArrayList<LocationDTO>();
		AddressValidationResponse addressValidationResponse = new AddressValidationResponse();
		addressValidationResponse.setInput(address);
		
		List<Location> locations = validateAddress(setLocation(address));
		if(locations!=null){
			for(Location location:locations){
				suggestions.add(setLocationDTO(location));
			}
		}
		addressValidationResponse.setSuggestions(suggestions);
		
		return addressValidationResponse;
	}
	
	private Location setLocation(LocationDTO address){
		Location location = new Location();
		location.setAddress1(address.getAddressLine1());
		location.setAddress2(address.getAddressLine2());
		location.setCity(address.getCity());
		location.setState(address.getState());
		location.setZip(address.getZipcode());
		
		return location;
	}
	
	private LocationDTO setLocationDTO(Location address){
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setAddressLine1(address.getAddress1());
		locationDTO.setAddressLine2(address.getAddress2());
		locationDTO.setCity(address.getCity());
		locationDTO.setState(address.getState());
		locationDTO.setZipcode(address.getZip());
		
		return locationDTO;
	}




}
