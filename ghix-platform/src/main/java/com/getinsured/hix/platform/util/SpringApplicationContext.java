package com.getinsured.hix.platform.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn("moduleContext")
public class SpringApplicationContext {

	@Autowired
	private ModuleContextProvider moduleContext;

	public Object getBean(String beanName, String contextName) {
		Object obj = null;
		try {
			if (contextName != null) {
				return obj = moduleContext.getBean(beanName, contextName);
			}
		} catch (Exception e) {

			throw e;
		}
		return obj;
	}

}
