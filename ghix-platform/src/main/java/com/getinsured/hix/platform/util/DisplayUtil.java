package com.getinsured.hix.platform.util;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;

public class DisplayUtil {

	// displayValuesInParanthesis() is to display the array of string values in
	// following display pattern (string1,string2,string3,........)

	public static String displayValuesInParanthesis(String... strArray) {
		StringBuffer displayString = new StringBuffer();
		StringBuffer tempStr = new StringBuffer();
		for (String str : strArray) {
			if (str != null && str.length() > 0) {
				tempStr.append(str).append(",").append(" ");
			}
		}

		if (tempStr.length() > 0) {
			displayString.append("(").append(tempStr.substring(0, tempStr.length() - 2)).append(")");
		}
		return displayString.toString();
	}
	
	public static String replaceDynamicValuesInMessageSource(String errorMsg){
		
		String[] searchList = {"{Dynamic_Minimum_Password_Length}","{Dynamic_Password_history}","{Dynamic_Password_Complexity_Msg}","{Dynamic_Password_Min_Age}"};
		String[] replacementList = {DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_LENGTH),
									DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT),
									DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_VIOLATION_MSG),
									DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_AGE)};
		
		return StringUtils.replaceEach(errorMsg, searchList, replacementList);
	}


}
