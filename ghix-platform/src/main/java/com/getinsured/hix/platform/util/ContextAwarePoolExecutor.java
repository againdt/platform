package com.getinsured.hix.platform.util;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.timeshift.TimeshiftContext;

public class ContextAwarePoolExecutor extends ThreadPoolTaskExecutor {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	
	public static final String X_USER= "X-USER".intern();
	public static final String X_USER_OFFSET="X-USER-OFFSET".intern();
	private Logger logger = LoggerFactory.getLogger(ContextAwarePoolExecutor.class);
	public ContextAwarePoolExecutor() {
		super.setMaxPoolSize(50);
		super.setQueueCapacity(150);
		RejectedExecutionHandler handler = new ThreadPoolExecutor.CallerRunsPolicy();
		super.setRejectedExecutionHandler(handler);
		super.setThreadNamePrefix("xx");
	}
	
	@Override
	    public <T> Future<T> submit(Callable<T> task) {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			if(ra != null) {
				ra.setAttribute(X_USER, TimeshiftContext.getCurrent().getUserName(),RequestAttributes.SCOPE_SESSION);
				ra.setAttribute(X_USER_OFFSET, TimeshiftContext.getCurrent().getTimeOffset(),RequestAttributes.SCOPE_SESSION);
			}
			logger.info("Submitting the processing task");
			return super.submit(new ContextAwareCallable<T>(task, ra));
	    }

	@Override
	public <T> ListenableFuture<T> submitListenable(Callable<T> task) {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		if(ra != null) {
			ra.setAttribute(X_USER, TimeshiftContext.getCurrent().getUserName(),RequestAttributes.SCOPE_SESSION);
			ra.setAttribute(X_USER_OFFSET, TimeshiftContext.getCurrent().getTimeOffset(),RequestAttributes.SCOPE_SESSION);
		}
		logger.info("Submitting the listenable processing task");
		return super.submitListenable(new ContextAwareCallable<T>(task, ra));
	}
}
