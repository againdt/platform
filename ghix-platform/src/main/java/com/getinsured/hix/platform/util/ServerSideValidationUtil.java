package com.getinsured.hix.platform.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ServerSideValidationUtil {
	
	private static final String EMAIL_REGEXP = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String PHONENUMBER_REGEXP  ="\\d{10}";
	private static final String ZIPCODE_REGEXP  ="\\d{5}";
	private static Pattern emailPattern;
	private static Pattern phoneNumberPattern;
	private static Pattern zipCodePattern;
	
	static {
		emailPattern = Pattern.compile(EMAIL_REGEXP);
		phoneNumberPattern = Pattern.compile(PHONENUMBER_REGEXP);
		zipCodePattern = Pattern.compile(ZIPCODE_REGEXP);
	}
	
	/**
     * <p>Checks if a String is not empty (""), not null and not whitespace only.</p>
     *
     * <pre>
     * ServerSideValidationUtil.isNotBlank(null)      = false
     * ServerSideValidationUtil.isNotBlank("")        = false
     * ServerSideValidationUtil.isNotBlank(" ")       = false
     * ServerSideValidationUtil.isNotBlank("bob")     = true
     * ServerSideValidationUtil.isNotBlank("  bob  ") = true
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is
     *  not empty and not null and not whitespace
     */
	public static boolean isNotBlank(String str) {
		return StringUtils.isNotBlank(str);
	}
	
	/**
     * <p>Checks if a String is not empty (""), not null and not whitespace only.</p>
     *
     * <p>If isBlankValid is true, <code>null</code> and an empty String ("") will return <code>true</code>.
     *
     * <pre>
     * ServerSideValidationUtil.isNotBlank(null, true)      = true
     * ServerSideValidationUtil.isNotBlank("", true)        = true
     * ServerSideValidationUtil.isNotBlank(" ", true)       = true
     * ServerSideValidationUtil.isNotBlank("bob", true)     = true
     * ServerSideValidationUtil.isNotBlank("  bob  ", true) = true
     * </pre>
     * 
     * <p>If isBlankValid is false, <code>null</code> and an empty String ("") will return <code>false</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isNotBlank(null, false)      = false
     * ServerSideValidationUtil.isNotBlank("", false)        = false
     * ServerSideValidationUtil.isNotBlank(" ", false)       = false
     * ServerSideValidationUtil.isNotBlank("bob", false)     = true
     * ServerSideValidationUtil.isNotBlank("  bob  ", false) = true
     * </pre>
     *
     * @param str  the String to check, may be null
     * @param isBlankValid  the boolean true if needs to consider str's null or blank value as valid, possible values are true and false
     * @return <code>true</code> if only contains letters, digits or space in middle.
     *  Will also return <code>true</code> for null or blank value if isBlankValid is true
     */
	public static boolean isNotBlank(String str, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(str)) || isNotBlank(str));
	}
	/**
     * <p>Checks if the String contains only unicode letters, digits
     * or space (<code>' '</code>) in middle.</p>
     *
     * <p><code>null</code> will return <code>false</code>.
     * An empty String ("") will return <code>false</code>.
     * A String ("abc") and ("ab c") will return <code>true</code>.</p>
     *
     * <pre>
     * ServerSideValidationUtil.isAlphanumeric(null)   = false
     * ServerSideValidationUtil.isAlphanumeric("")     = false
     * ServerSideValidationUtil.isAlphanumeric("  ")   = false
     * ServerSideValidationUtil.isAlphanumeric("abc")  = true
     * ServerSideValidationUtil.isAlphanumeric("ab c") = true
     * ServerSideValidationUtil.isAlphanumeric("ab2c") = true
     * ServerSideValidationUtil.isAlphanumeric("ab-c") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if only contains letters, digits or space in middle,
     *  and is non-null
     */
	public static boolean isAlphanumeric(String str) {
		return StringUtils.isNotBlank(str) && StringUtils.isAlphanumericSpace(str);
	}
	
	/**
     * <p>Checks if the String contains only unicode letters, digits
     * or space (<code>' '</code>) in middle.</p>
     *
     * <p>If isBlankValid is true, <code>null</code> and an empty String ("") will return <code>true</code>.
     * A String ("abc") and ("ab c") will return <code>true</code>.</p>
     *
     * <pre>
     * ServerSideValidationUtil.isAlphanumeric(null, true)   = true
     * ServerSideValidationUtil.isAlphanumeric("", true)     = true
     * ServerSideValidationUtil.isAlphanumeric("  ", true)   = true
     * ServerSideValidationUtil.isAlphanumeric("abc", true)  = true
     * ServerSideValidationUtil.isAlphanumeric("ab c", true) = true
     * ServerSideValidationUtil.isAlphanumeric("ab2c", true) = true
     * ServerSideValidationUtil.isAlphanumeric("ab-c", true) = false
     * </pre>
     * 
     * <p>If isBlankValid is false, <code>null</code> and an empty String ("") will return <code>false</code>.
     * A String ("abc") and ("ab c") will return <code>true</code>.</p>
     * 
     * <pre>
     * ServerSideValidationUtil.isAlphanumeric(null, false)   = false
     * ServerSideValidationUtil.isAlphanumeric("", false)     = false
     * ServerSideValidationUtil.isAlphanumeric("  ", false)   = false
     * ServerSideValidationUtil.isAlphanumeric("abc", false)  = true
     * ServerSideValidationUtil.isAlphanumeric("ab c", false) = true
     * ServerSideValidationUtil.isAlphanumeric("ab2c", false) = true
     * ServerSideValidationUtil.isAlphanumeric("ab-c", false) = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @param isBlankValid  the boolean true if needs to consider str's null or blank value as valid, possible values are true and false
     * @return <code>true</code> if only contains letters, digits or space in middle.
     *  Will also return <code>true</code> for null or blank value if isBlankValid is true
     */
	public static boolean isAlphanumeric(String str, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(str)) || isAlphanumeric(str));
	}
	
	/**
     * <p>Checks if the String contains only unicode digits.
     * A decimal point is not a unicode digit and returns false.</p>
     *
     * <p><code>null</code>, an empty String ("") and blank String (" ") will return <code>false</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isNumeric(null)   = false
     * ServerSideValidationUtil.isNumeric("")     = false
     * ServerSideValidationUtil.isNumeric("  ")   = false
     * ServerSideValidationUtil.isNumeric("123")  = true
     * ServerSideValidationUtil.isNumeric("12 3") = false
     * ServerSideValidationUtil.isNumeric("ab2c") = false
     * ServerSideValidationUtil.isNumeric("12-3") = false
     * ServerSideValidationUtil.isNumeric("12.3") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if only contains digits, is non-null and non-blank
     */
	public static boolean isNumeric(String str) {
		return StringUtils.isNotBlank(str) && StringUtils.isNumeric(str);
	}
	
	/**
     * <p>Checks if the String contains only unicode digits.
     * A decimal point is not a unicode digit and returns false.</p>
     *
     * <p>If isBlankValid is true, <code>null</code>, an empty String ("") and a blank String (" ") will return <code>true</code>.
     * A String ("123") will return <code>true</code>. A String ("12 3") will return <code>false</code>.</p>
     * 
     * <pre>
     * ServerSideValidationUtil.isNumeric(null)   = true
     * ServerSideValidationUtil.isNumeric("")     = true
     * ServerSideValidationUtil.isNumeric("  ")   = true
     * ServerSideValidationUtil.isNumeric("123")  = true
     * ServerSideValidationUtil.isNumeric("12 3") = false
     * ServerSideValidationUtil.isNumeric("ab2c") = false
     * ServerSideValidationUtil.isNumeric("12-3") = false
     * ServerSideValidationUtil.isNumeric("12.3") = false
     * </pre>
     * 
     * <p>If isBlankValid is false, <code>null</code>, an empty String ("") and a blank String (" ") will return <code>false</code>.
     * A String ("123") will return <code>true</code>. A String ("12 3") will return <code>false</code>.</p>
     * 
     * <pre>
     * ServerSideValidationUtil.isNumeric(null)   = false
     * ServerSideValidationUtil.isNumeric("")     = false
     * ServerSideValidationUtil.isNumeric("  ")   = false
     * ServerSideValidationUtil.isNumeric("123")  = true
     * ServerSideValidationUtil.isNumeric("12 3") = false
     * ServerSideValidationUtil.isNumeric("ab2c") = false
     * ServerSideValidationUtil.isNumeric("12-3") = false
     * ServerSideValidationUtil.isNumeric("12.3") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @param isBlankValid  the boolean true if needs to consider str's null or blank value as valid, possible values are true and false
     * @return <code>true</code> if only contains digits. Will also return <code>true</code> for null or blank value if isBlankValid is true
     */
	public static boolean isNumeric(String str, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(str)) || isNumeric(str));
	}
	
	public static boolean isValidEmail(String email) {
		return StringUtils.isNotBlank(email) && emailPattern.matcher(email).matches();
	}
	
	public static boolean isValidEmail(String email, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(email)) || isValidEmail(email));
	}

	/**
     * <p>Checks if the String contains only 10 digit number.</p>
     *
     * <p><code>null</code>, an empty String ("") and blank String (" ") will return <code>false</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isValidPhoneNumber(null)   = false
     * ServerSideValidationUtil.isValidPhoneNumber("")     = false
     * ServerSideValidationUtil.isValidPhoneNumber("  ")   = false
     * ServerSideValidationUtil.isValidPhoneNumber("1234567890")  = true
     * ServerSideValidationUtil.isValidPhoneNumber("123") = false
     * ServerSideValidationUtil.isValidPhoneNumber("123 456789") = false
     * ServerSideValidationUtil.isValidPhoneNumber("123456-789") = false
     * ServerSideValidationUtil.isValidPhoneNumber("12.3456789") = false
     * </pre>
     *
     * @param phone  the String to check, may be null
     * @return <code>true</code> if only contains 10 digit number, is non-null and non-blank
     */
	public static boolean isValidPhoneNumber(String phone) {
		return StringUtils.isNotBlank(phone) && phoneNumberPattern.matcher(phone).matches();
	}
	
	/**
     * <p>Checks if the String contains only 10 digit number.</p>
     *
     * <p>If isBlankValid is true, <code>null</code>, an empty String ("") and a blank String (" ") will return <code>true</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isValidPhoneNumber(null)   = true
     * ServerSideValidationUtil.isValidPhoneNumber("")     = true
     * ServerSideValidationUtil.isValidPhoneNumber("  ")   = true
     * ServerSideValidationUtil.isValidPhoneNumber("1234567890")  = true
     * ServerSideValidationUtil.isValidPhoneNumber("123") = false
     * ServerSideValidationUtil.isValidPhoneNumber("123 456789") = false
     * ServerSideValidationUtil.isValidPhoneNumber("123456-789") = false
     * ServerSideValidationUtil.isValidPhoneNumber("12.3456789") = false
     * </pre>
     * 
     * <p>If isBlankValid is false, <code>null</code>, an empty String ("") and a blank String (" ") will return <code>false</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isValidPhoneNumber(null)   = false
     * ServerSideValidationUtil.isValidPhoneNumber("")     = false
     * ServerSideValidationUtil.isValidPhoneNumber("  ")   = false
     * ServerSideValidationUtil.isValidPhoneNumber("1234567890")  = true
     * ServerSideValidationUtil.isValidPhoneNumber("123") = false
     * ServerSideValidationUtil.isValidPhoneNumber("123 456789") = false
     * ServerSideValidationUtil.isValidPhoneNumber("123456-789") = false
     * ServerSideValidationUtil.isValidPhoneNumber("12.3456789") = false
     * </pre>
     *
     * @param phone  the String to check, may be null
     * @return <code>true</code> if only contains 10 digit number. Will also return <code>true</code> for null or blank value if isBlankValid is true
     */
	public static boolean isValidPhoneNumber(String phone, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(phone)) || isValidPhoneNumber(phone));
	}
	
	/**
     * <p>Checks if the String zip contains only 5 digit number.</p>
     *
     * <p><code>null</code>, an empty String ("") and blank String (" ") will return <code>false</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isValidZipCode(null)   = false
     * ServerSideValidationUtil.isValidZipCode("")     = false
     * ServerSideValidationUtil.isValidZipCode("  ")   = false
     * ServerSideValidationUtil.isValidZipCode("1234567890")  = true
     * ServerSideValidationUtil.isValidZipCode("123") = false
     * ServerSideValidationUtil.isValidZipCode("123 456789") = false
     * ServerSideValidationUtil.isValidZipCode("123456-789") = false
     * ServerSideValidationUtil.isValidZipCode("12.3456789") = false
     * </pre>
     *
     * @param zip  the String to check, may be null
     * @return <code>true</code> if only contains 5 digit number, is non-null and non-blank
     */
	public static boolean isValidZipCode(String zip) {
		return StringUtils.isNotBlank(zip) && zipCodePattern.matcher(zip).matches();
	}
	
	/**
     * <p>Checks if the zip contains only 5 digit number.</p>
     *
     * <p>If isBlankValid is true, <code>null</code>, an empty String ("") and a blank String (" ") will return <code>true</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isValidZipCode(null)   = true
     * ServerSideValidationUtil.isValidZipCode("")     = true
     * ServerSideValidationUtil.isValidZipCode("  ")   = true
     * ServerSideValidationUtil.isValidZipCode("123450")  = true
     * ServerSideValidationUtil.isValidZipCode("123") = false
     * ServerSideValidationUtil.isValidZipCode("123 4") = false
     * ServerSideValidationUtil.isValidZipCode("12-45") = false
     * ServerSideValidationUtil.isValidZipCode("12.34") = false
     * </pre>
     * 
     * <p>If isBlankValid is false, <code>null</code>, an empty String ("") and a blank String (" ") will return <code>false</code>.
     * 
     * <pre>
     * ServerSideValidationUtil.isValidZipCode(null)   = false
     * ServerSideValidationUtil.isValidZipCode("")     = false
     * ServerSideValidationUtil.isValidZipCode("  ")   = false
     * ServerSideValidationUtil.isValidZipCode("1234567890")  = true
     * ServerSideValidationUtil.isValidZipCode("123") = false
     * ServerSideValidationUtil.isValidZipCode("123 456789") = false
     * ServerSideValidationUtil.isValidZipCode("123456-789") = false
     * ServerSideValidationUtil.isValidZipCode("12.3456789") = false
     * </pre>
     *
     * @param zip  the String to check, may be null
     * @return <code>true</code> if only contains 5 digit number. Will also return <code>true</code> for null or blank value if isBlankValid is true
     */
	public static boolean isValidZipCode(String zip, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(zip)) || isValidZipCode(zip));
	}
	
	public static boolean isValidDate(String dateToValidate, String dateFormat) {
		if(StringUtils.isBlank(dateToValidate)) {
			return false;
		}
 
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
 
		try {
 			//if not valid, throw ParseException
			sdf.parse(dateToValidate);
 		} catch (ParseException e) {
 			e.printStackTrace();
			return false;
		}
 
		return true;
	}
	
	public static boolean isValidDate(String dateToValidate, String dateFormat, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(dateToValidate)) || isValidDate(dateToValidate, dateFormat));
	}
	
	public static boolean isTrueOrFalse(String str) {
		return StringUtils.isNotBlank(str) && (("true".equalsIgnoreCase(str)) || ("false".equalsIgnoreCase(str)));
	}
	
	public static boolean isTrueOrFalse(String str, boolean isBlankValid) {
		return ((isBlankValid && StringUtils.isBlank(str)) || isTrueOrFalse(str));
	}

}
