package com.getinsured.hix.platform.multitenant.resolver;

public class Tenant {

	private String tenantName;
	private Long id = 1L;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantName() {
		return tenantName;
	}
	
	

}
