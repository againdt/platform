package com.getinsured.hix.platform.auditor;

public class GiAuditParameter {

	private final String key;

	private final Object value;

	public GiAuditParameter(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public Object getValue() {
		return value;
	}

}
