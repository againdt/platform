package com.getinsured.hix.platform.security;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;

/**
 * This class helps us to audit un-authorized access to any resource (controller URL, etc)
 * where  we are using hasPermission method.
 *
 */
@Component("auditAuthorizationDetails")
public class AuditAuthorizationDetails {

	private static final String GENERIC_UNAUTHORIZED_FLOW = "generic un-authorized flow";
	private static final String PERMISSION_NAME = "PERMISSION_NAME";
	private static final String AUTH_EVENT_NAME = "EVENT_NAME";
	private static final String AUTH_EVENT_TYPE = "EVENT_TYPE";

	public void logSuccessfulAuth(Map<String,String> customParams) {
		int params = 2;
		if(customParams != null && customParams.size() > 0){
			params = customParams.size() +2;
		}
		GiAuditParameter [] auditParameters = new GiAuditParameter[params];
		auditParameters[0] = new GiAuditParameter(AUTH_EVENT_TYPE, EventTypeEnum.AUTH);
		auditParameters[1] = new GiAuditParameter(AUTH_EVENT_NAME, EventNameEnum.SUCCESSFUL_AUTHORIZATION);
		if(params > 2){
			Entry<String,String> entry = null;
			int i = 2;
			Iterator<Entry<String,String>> cursor = customParams.entrySet().iterator();
			while(cursor.hasNext()){
				entry = cursor.next();
				auditParameters[i++]= new GiAuditParameter(entry.getKey(), entry.getValue());
			}
		}
		GiAuditParameterUtil.add(auditParameters);
	}
	
	public void logSuccessfulAuth() {
		GiAuditParameter [] auditParameters = new GiAuditParameter[2];
		auditParameters[0] = new GiAuditParameter(AUTH_EVENT_TYPE, EventTypeEnum.AUTH);
		auditParameters[1] = new GiAuditParameter(AUTH_EVENT_NAME, EventNameEnum.SUCCESSFUL_AUTHORIZATION);
		GiAuditParameterUtil.add(auditParameters);
	}

	/**
	 * log only UNSUCCESSFUL_AUTHORIZATION transactions.
	 *
	 */
	@GiAudit(eventName=EventNameEnum.UNSUCCESSFUL_AUTHORIZATION, eventType=EventTypeEnum.AUTH, transactionName=GENERIC_UNAUTHORIZED_FLOW)
	public void logUnSuccessfulAuth(Object permission) {
		GiAuditParameter [] auditParameters = new GiAuditParameter[3];
		auditParameters[0] = new GiAuditParameter(AUTH_EVENT_TYPE, EventTypeEnum.AUTH);
		auditParameters[1] = new GiAuditParameter(AUTH_EVENT_NAME, EventNameEnum.UNSUCCESSFUL_AUTHORIZATION);
		auditParameters[2] = new GiAuditParameter(PERMISSION_NAME, permission);
		GiAuditParameterUtil.add(auditParameters);
	}
	
	@GiAudit(eventName=EventNameEnum.UNSUCCESSFUL_AUTHORIZATION, eventType=EventTypeEnum.AUTH, transactionName=GENERIC_UNAUTHORIZED_FLOW)
	public void logUnSuccessfulAuth(Object permission,Map<String, String> customParams) {
		int params = 3;
		if(customParams != null && customParams.size() > 0){
			params = customParams.size() +3;
		}
		GiAuditParameter [] auditParameters = new GiAuditParameter[params];
		auditParameters[0] = new GiAuditParameter(AUTH_EVENT_TYPE, EventTypeEnum.AUTH);
		auditParameters[1] = new GiAuditParameter(AUTH_EVENT_NAME, EventNameEnum.UNSUCCESSFUL_AUTHORIZATION);
		auditParameters[2] = new GiAuditParameter(PERMISSION_NAME, permission);
		if(params > 3){
			Entry<String,String> entry = null;
			int i = 3;
			Iterator<Entry<String,String>> cursor = customParams.entrySet().iterator();
			while(cursor.hasNext()){
				entry = cursor.next();
				auditParameters[i++]= new GiAuditParameter(entry.getKey(), entry.getValue());
			}
		}
		GiAuditParameterUtil.add(auditParameters);
	}


}
