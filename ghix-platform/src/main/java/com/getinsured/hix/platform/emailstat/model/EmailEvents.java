package com.getinsured.hix.platform.emailstat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name="email_events")
public class EmailEvents implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMAIL_EVENTS_SEQ")
	@SequenceGenerator(name = "EMAIL_EVENTS_SEQ", sequenceName = "EMAIL_EVENTS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="notice_id")
	private int noticeId;
	
	@Column(name="event")
	private String event;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="event_date")
	private Date eventDate;
	
	@Column(name="state_code")
	private String stateCode;
	
	@Column(name="exchange_url")
	private String exchangeUrl;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getExchangeUrl() {
		return exchangeUrl;
	}

	public void setExchangeUrl(String exchangeUrl) {
		this.exchangeUrl = exchangeUrl;
	}
}
