package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.NoticesElements;

public interface INoticesElementsRepository extends JpaRepository<NoticesElements, Integer> {

}
