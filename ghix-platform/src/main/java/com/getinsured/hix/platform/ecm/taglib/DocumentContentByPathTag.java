/**
 * 
 */
package com.getinsured.hix.platform.ecm.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.platform.ecm.ContentManagementService;

/**
 * Java Tag Class to get document content based on name.
 * 
 * <P>renders the content of the document.
 * 
 * <P>If the supplied document is missing in the GHIX ECM then the output is empty string.
 * 
 * @author Ekram Ali Kazi
 */
public class DocumentContentByPathTag extends TagSupport {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = 6062850667445633357L;

	/** The log object for this class. */
	private static final Logger LOGGER = Logger.getLogger(DocumentContentByPathTag.class);

	/** Content path. */
	private String contentPath;

	private ContentManagementService ecmService;
	/**
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {
		autowireDependencies();

		try {
			byte[] data = null;

			if (data == null){
				data = ecmService.getContentDataByPath(contentPath);
			}
			String result = formOutput(data);
			pageContext.getOut().print(result);

		} catch (Exception ex) {
			LOGGER.error("Error processing CMIS request - ", ex);
			throw new javax.servlet.jsp.JspException(ex);
		}

		return SKIP_BODY;
	}


	private String formOutput(byte[] data) {
		String result;
		// Make sure that no null String is returned
		if (data == null) {
			result = "";
		}else{
			result = new String(data);
		}
		return result;
	}


	private void autowireDependencies() {
		ApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext(pageContext.getRequest(), pageContext.getServletContext());
		ecmService = (ContentManagementService) applicationContext.getBean("ecmService");
	}


	public String getContentPath() {
		return contentPath;
	}

	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}

}
