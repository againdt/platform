package com.getinsured.hix.platform.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RoleProvisioningRule;

public interface IRoleProvisioningRepository extends JpaRepository<RoleProvisioningRule, Integer>{
	@Query("select rule from RoleProvisioningRule rule where rule.provisioningRole.name=:roleName")
	List<RoleProvisioningRule> findProvisioningRoles(@Param("roleName")String roleName);
	
	@Query("select rule from RoleProvisioningRule rule where rule.provisioningRole.id=:provisioningRoleId AND rule.managedRole.id=:managedRoleId")
	List<RoleProvisioningRule> findProvisioningRuleByProvisionigAndManagedRoles(
			@Param("provisioningRoleId") int provisioningRoleId, @Param("managedRoleId") int managedRoleId);

	RoleProvisioningRule findByProvisionId(Integer provisionId);
}