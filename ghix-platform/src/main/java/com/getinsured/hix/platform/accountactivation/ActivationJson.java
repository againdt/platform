package com.getinsured.hix.platform.accountactivation;


/**
 * Place holder for created-creator flow. This information is used to draw JSP, send email & activation code via call/sms.
 *
 * @author Ekram Ali Kazi
 *
 */
public class ActivationJson {
	/** This enum stores information about created object repository */
	/** For new flow, add created object repository details here */
	public enum OBJECTTYPE {

		ASSISTERENROLLMENTENTITYADMIN,EMPLOYEE,ISSUER_REP,ISSUER_REPRESENTATIVE,EMPLOYER,ISSUER_ADMIN,OPERATIONS,BROKER_ADMIN,CSR,ADMIN,INDIVIDUAL,
		BROKER,CONSUMER,ASSISTER,SALES_MANAGER,L1_CSR,L2_CSR,SUPERVISOR,THIRD_PARTY_CSR,INDIVIDUAL_REFERRAL,RECONCILIATION_ADMIN,SALES_DIRECTOR,
		AFFILIATE_ADMIN,EMPLOYER_ADMIN,CUSTOMER_SUPPORT,SALES_OPERATIONS,BATCH_ADMIN,OPERATIONS_ADMIN,EXTERNAL_AGENT,A_1095,TENANT_ADMIN, SERFF_ADMIN,REMOTE_AGENT,ISSUER_ENROLLMENT_REPRESENTATIVE,L0_READONLY;
	

		public String getRepositoryName() {
			switch(this) {
			case EMPLOYEE: return "employeeRepository";
			case ISSUER_REP: return "issuerRepresentativeRepository";// check later as we dont ISSUER_REP
			case ISSUER_REPRESENTATIVE: return "issuerRepresentativeRepository";
			case ISSUER_ENROLLMENT_REPRESENTATIVE: return "issuerRepresentativeRepository";
			case EMPLOYER: return "employerRepository";
			case ISSUER_ADMIN: return "adminUsersRepository";
			case L1_CSR: return "adminUsersRepository";
			case L2_CSR: return "adminUsersRepository";
			case SUPERVISOR: return "adminUsersRepository";
			case BROKER_ADMIN: return "adminUsersRepository";
			case CSR: return "adminUsersRepository";
			case ADMIN: return "adminUsersRepository";
			case OPERATIONS: return "adminUsersRepository";		// HIX-26097 : Add new role for Operations
			case CONSUMER: return "householdRepository";
			case INDIVIDUAL: return "householdRepository";
			case BROKER: return "brokerRepository";
			case ASSISTER: return "assisterRepository";
			case ASSISTERENROLLMENTENTITYADMIN: return "adminUsersRepository";
			case SALES_MANAGER: return "adminUsersRepository";
			case THIRD_PARTY_CSR:return "adminUsersRepository";		//HIX-38183 : Add new role for Third Party Customer Support
			case INDIVIDUAL_REFERRAL: return "ssapRefRepository";  // HIX-38642 : Added new Enum for State Referal Flow		
			case RECONCILIATION_ADMIN: return "adminUsersRepository"; 
			case SALES_DIRECTOR: return "adminUsersRepository"; 
			case AFFILIATE_ADMIN: return "adminUsersRepository"; 
			case EMPLOYER_ADMIN: return "adminUsersRepository"; 
			case CUSTOMER_SUPPORT: return "adminUsersRepository"; 
			case SALES_OPERATIONS: return "adminUsersRepository";
			case BATCH_ADMIN: return "adminUsersRepository";
			case OPERATIONS_ADMIN: return "adminUsersRepository";
			case EXTERNAL_AGENT: return "adminUsersRepository";
			case A_1095: return "adminUsersRepository";
			case TENANT_ADMIN: return "adminUsersRepository";
			case SERFF_ADMIN: return "adminUsersRepository";
			case REMOTE_AGENT: return "adminUsersRepository";
			case L0_READONLY: return "adminUsersRepository";
			default: return "Unknown type";
			}
		}
	}

	private CreatedObject createdObject;
	private CreatorObject creatorObject;

	public CreatorObject getCreatorObject() {
		return creatorObject;
	}

	public void setCreatorObject(CreatorObject creatorObject) {
		this.creatorObject = creatorObject;
	}

	private OBJECTTYPE getType(String name){
		return OBJECTTYPE.valueOf(name);
	}

	public String getRepositoryName(){
		OBJECTTYPE type = getType(this.createdObject.getRoleName());
		return type.getRepositoryName();
	}
	

	public CreatedObject getCreatedObject() {
		return createdObject;
	}

	public void setCreatedObject(CreatedObject createdObject) {
		this.createdObject = createdObject;
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	@Override
	public String toString() {
					return "ActivationJson [createdObject=" + createdObject
													+ ", creatorObject=" + creatorObject + "]";
	}


}
