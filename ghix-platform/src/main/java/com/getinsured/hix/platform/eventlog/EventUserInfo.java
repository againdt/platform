package com.getinsured.hix.platform.eventlog;

public class EventUserInfo {
	// logged in user info
	private int userId;
	private String userRole;
	private String userName;

	// switch to account info
	private int activeModuleId;
	private String activeModuleName;
	private String activeModuleUserName;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getActiveModuleId() {
		return activeModuleId;
	}

	public void setActiveModuleId(int activeModuleId) {
		this.activeModuleId = activeModuleId;
	}

	public String getActiveModuleName() {
		return activeModuleName;
	}

	public void setActiveModuleName(String activeModuleName) {
		this.activeModuleName = activeModuleName;
	}

	public String getActiveModuleUserName() {
		return activeModuleUserName;
	}

	public void setActiveModuleUserName(String activeModuleUserName) {
		this.activeModuleUserName = activeModuleUserName;
	}
}
