/**
 * 
 */
package com.getinsured.hix.platform.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang3.StringUtils;
import org.opensaml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.saml.metadata.CachingMetadataManager;
import org.springframework.security.saml.util.SAMLUtil;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * @author panda_p In this class we can define constants for GHIX application. These Constants can
 *         be either direct (define a constant of type psf & directly assign any value to it) or it
 *         can from config file. (define a constant of type psf & populate value from config file by
 *         using 'Value' attribute of annotation)
 */

@Component("platformConstants")
@DependsOn({"buildProp","configProp","ghixSecretKey"})
public final class GhixPlatformConstants {
	public static final String ENC_START_TAG = "ENC(";
	public static final String ENC_END_TAG = ")";
	public static final String SYS_PROP_START_TAG = "${";
	public static final String SYS_PROP_END_TAG = "}";
	public static String DATABASE_TYPE;
	public static boolean SESSION_DEBUG = false;
	public static String USER_NAME_POLICY;
	public static Boolean EXTERNAL_TEMPLATE_ENABLED=false;
	public static String RESIDENT_IDP_METADATA;
	public static String DEFAULT_SP_METADATA_TEMPLATE;
	public static String DEFAULT_SP_ACS_TEMPLATE;
	public static String EXTERNAL_TEMPLATE_LOCATION;
	public static String VELOCITY_LOG;
	public static String HUB_INTEGRATION_HOST;
	
	@Autowired(required=false)
	private CachingMetadataManager metadata;
	
	private String IDP_CONFIG;

	public GhixPlatformConstants() {
	}
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	@PostConstruct
	public void checkJCEUnlimitedDeployment(){
		try {
			int maxKeyLen = Cipher.getMaxAllowedKeyLength("AES");
			if(maxKeyLen <256){
				throw new GIRuntimeException("JCE Unlimited Strength policy files not installed, max supported key length:"+maxKeyLen+" is not sufficient for Algorithm: AES");
			}
		} catch (NoSuchAlgorithmException e) {
			throw new GIRuntimeException("Required cipher not available in provided crypto library");
		} 
	}

	// for build detail configuration
	public static String BUILD_NUMBER;
	public static String BUILD_DATE;
    public static String BUILD_BRANCH_NAME;
    public static String BRANCH_NAME; // How is different from BUILD_BRANCH_NAME
	
	@Value("#{buildProp['build.number']}")
	public void setBUILD_NUMBER(String bUILD_NUMBER) {
		BUILD_NUMBER = bUILD_NUMBER;
	}

	@Value("#{buildProp['build.timestamp']}")
	public void setBUILD_DATE(String bUILD_DATE) {
		BUILD_DATE = bUILD_DATE;
	}

	@Value("#{buildProp['branch.name']}")
	public void setBUILD_BRANCH_NAME(String bUILD_BRANCH_NAME) {
		BUILD_BRANCH_NAME = bUILD_BRANCH_NAME;
	}

	@Value("#{configProp.BRANCH_NAME}")
	public void setBranchName(String branchName) {
		BRANCH_NAME = branchName;
	}
	
	@Value("#{configProp['database.type']}")
	public void setDatabase(String database) {
		DATABASE_TYPE = database;
	}
	
	@Value("#{configProp['platform.velocity.log']}")
	public void setVelocityLog(String vLog){
		if(vLog != null) {
			VELOCITY_LOG=vLog.trim();
		}
	}
	
	@Value("#{configProp['saml.idp.config'] != null ? configProp['saml.idp.config'] : 'NOT_AVAILABLE'}")
	public void setIDPConfig(String config) {
		IDP_CONFIG = config.trim();
	}
	
	@Value("#{configProp['platform.session.sharing_debug'] != null ? configProp['platform.session.sharing_debug'] :'false'}")
	public void setSessionDebug(String debug){
		SESSION_DEBUG = debug.trim().equalsIgnoreCase("true");
	}
	
	public static String MAXUPLOADSIZEEXCEEDED_EXCEPTION = "Maxuploadsizeexceededexception";
	public static String SERVER_SIDE_VALIDATION_EXCEPTION = "422:: Unprocessable / Tampered data exception.";//HIX-39174

	// two factor authentication properties and keys
	public static boolean IS_ENABLE2FACT_AUTH;	
	public static String TWO_FACTOR_AUTHENTICATION_ROLES;
	public static String DUO_SECRET_KEY;
	public static String DUO_INTEGRATION_KEY;
	public static String DUO_APPLICATION_SECRET_KEY;
	public static String DUO_HOSTNAME;
	
	public static String httpBasicAuthPass;
	public static String httpBasicAuthUser;
	public static String httpBasicAuthHost;
	public static String httpBasicAuthPort;
	
	@Value("#{configProp['httpBasicAuthHost']}")
	public void setHttpBasicAuthHost(String host) {
		httpBasicAuthHost = host;
	}
	
	@Value("#{configProp['httpBasicAuthPort']}")
	public void setHttpBasicAuthPort(String port) {
		httpBasicAuthPort = port;
	}
	
	@Value("#{configProp['httpBasicAuthUser']}")
	public void setHttpBasicAuthUser(String user) {
		httpBasicAuthUser = user;
	}
	
	/**
	 * POSSIBLE VALUES SAME_AS_EMAIL and ALLOW_DIFFERENT_EMAIL
	 * @param policy
	 */
	@Value("#{configProp['platform.username.policy'] != null ? configProp['platform.username.policy'] :'SAME_AS_EMAIL'}")
	public void setUserNamePolicy(String policy) {
		USER_NAME_POLICY = policy;
	}
	
	//ghixHubIntegrationURL
	@Value("#{configProp['ghixHubIntegrationURL'] != null ? configProp['ghixHubIntegrationURL'] :'http://localhost:8081/ghix/'}")
	public void setHubIntegrationURL(String baseUrl) {
		URL url = null;
		try {
			url = new URL(baseUrl);
			HUB_INTEGRATION_HOST = url.getHost();
		} catch (MalformedURLException e) {
			this.logger.error("Exception setting uo hub integration host for configured property {}",baseUrl,e);
		}
	}
	
	@Value("#{configProp['httpBasicAuthPass']}")
	public void setHttpBasicAuthPass(String password) {
		httpBasicAuthPass = password;
	}
	
	// HIX-29686 - Darshan Hardas : Server changes as new license is generated for duo security
	@Value("#{configProp['duoHostName']}")
	public void setDUO_HOSTNAME(String dUO_HOSTNAME) {
		DUO_HOSTNAME = dUO_HOSTNAME;
	}

	@Value("#{configProp['isEnable2FactAuth']}")
	public void setIS_ENABLE2FACT_AUTH(boolean iS_ENABLE2FACT_AUTH) {
		IS_ENABLE2FACT_AUTH = iS_ENABLE2FACT_AUTH;
	}
	
	@Value("#{configProp['twoFactorAuthenticationRoles']}")
	public void setTWO_FACTOR_AUTHENTICATION_ROLES(String two_Factor_Authenticaton_Roles) {
		TWO_FACTOR_AUTHENTICATION_ROLES = two_Factor_Authenticaton_Roles;
	}
	
	@Value("#{configProp['duoSecretKeyForTwoFactAuth']}")
	public void setDUO_SECRET_KEY(String duo_Secret_Key) {
		DUO_SECRET_KEY = duo_Secret_Key;
	}
	
	@Value("#{configProp['duoIntegrationKeyForTwoFactAuth']}")
	public void setDUO_INTEGRATION_KEY(String duo_Integration_Key) {
		DUO_INTEGRATION_KEY = duo_Integration_Key;
	}
	
	@Value("#{configProp['duoApplicationSecretKeyForTwoFactAuth']}")
	public void setDUO_APPLICATION_SECRET_KEY(String duo_Application_Secret_Key) {
		DUO_APPLICATION_SECRET_KEY = duo_Application_Secret_Key;
	}
	
	public static String DUO_SECRET_KEY_ADMINAPIS;
	public static String DUO_INTEGRATION_KEY_ADMINAPIS;
	public static String DUO_APPLICATION_SECRET_KEY_ADMINAPIS;
	public static String DUO_HOSTNAME_ADMINAPIS;
	
	@Value("#{configProp['duoHostNameAdminApis']}")
	public void setDUO_HOSTNAME_ADMINAPIS(String duo_hostname_adminapis) {
		DUO_HOSTNAME_ADMINAPIS = duo_hostname_adminapis;
	}

	@Value("#{configProp['duoSecretKeyForAdminApis']}")
	public void setDUO_SECRET_KEY_ADMINAPIS(String duo_secret_key_adminapis) {
		DUO_SECRET_KEY_ADMINAPIS = duo_secret_key_adminapis;
	}
	
	@Value("#{configProp['duoIntegrationKeyForAdminApis']}")
	public void setDUO_INTEGRATION_KEY_ADMINAPIS(String duo_integration_key_adminapis) {
		DUO_INTEGRATION_KEY_ADMINAPIS = duo_integration_key_adminapis;
	}
	
	@Value("#{configProp['duoApplicationSecretKeyForAdminApis']}")
	public void setDUO_APPLICATION_SECRET_KEY_ADMINAPIS(String duo_application_secret_key_adminapis) {
		DUO_APPLICATION_SECRET_KEY_ADMINAPIS = duo_application_secret_key_adminapis;
	}
	
/*
	// FFM Integration properties
	//public static String DEFAULT_NPN;
	
	 * Get the default NPN for getinsured.com to be used with FFM integration
	 
	@Value("#{configProp['defaultNPN:']}")
	public void setDEFAULT_NPN(String defaultNPN) {
		DEFAULT_NPN = defaultNPN;
	}
	//Cybersource Merchant ID required for transactions
	
	//public static String MERCHANTID;
	
	@Value("#{configProp.merchantID}")
	public  void setMERCHANTID(String mERCHANTID) {
		MERCHANTID = mERCHANTID;
	}
	*/

	/***************************************************************************************/
	/********************** ERROR CODES FOR FINANCE MODULE *********************************/
	/***************************************************************************************/
	//public static final String FIN_20012 = "FINANCE-20012";
	//public static final String FIN_20013 = "FINANCE-20013";
	//public static final String FIN_20014 = "FINANCE-20014";
	//public static final String FIN_20015 = "FINANCE-20015";
	//public static final String FIN_20016 = "FINANCE-20016";
	//public static final String FIN_20017 = "FINANCE-20017";
	//public static final String FIN_20018 = "FINANCE-20018";
	//public static final String FIN_20019 = "FINANCE-20019";
	
	//public static final String ERROR_SUFFIX_FIN = "FINANCE-";
	
	public static final String USER_NAME = "userName";
	public static final int TEN = 10;
	public static final char ZERO = '0';
	public static String LOCAL_NODE_IP = null;
	public static int LOCAL_NODE_PORT = -1;
	public static String LOCAL_NODE_PROTOCOL = null;
	public static String MULTIPLE_SESSION_MODE = "deny_all";
	public static String PLATFORM_KEY_STORE_LOCATION = null;
	public static String PLATFORM_KEY_STORE_FILE = null;
	public static String PLATFORM_KEYSTORE_PASS = null;
	public static String PLATFORM_KEY_ALIAS = null;
	public static String PLATFORM_KEY_PASSWORD = null;
	public static String PLATFORM_NODE_PROFILE = null;
	
	public static String PLATFORM_PING_URL = null;
	public static String PLATFORM_PING_SCHEDULE = null;
	public static String SERFF_CSR_PASSKEY = null;
	////////////////////// SCIM Parameters //////////////////////////////////
	public static boolean SCIM_ENABLED = false;
	public static String SCIM_USER_GROUP_ENDPOINT;
	public static String SCIM_USER_ENDPOINT;
	public static int WSO2_AVAILABILITY_CHECK_MILLIS;
	public static String SAML_ALIAS;
	public static String TOKEN_VALIDATION_KEY;
	
	public static String SCIM_ADMIN_USER;
	public static String SCIM_ADMIN_PASS;
	
	public static String IDENTITY_SVC_URL;
	public static String GI_IDENTITY_SVC_URL;
	
	public static String IDALINK_FORGOT_PASSWORD_URL;
	
	public static final int SESSION_TOKEN_VALIDITY = 30*60*1000; // 30 min
	public static final int URL_TOKEN_VALIDITY = 3*60*1000; // 3 min
	public static final String FORCE_AUTHENTICATION = "0";
	public static final String ALLOW_PASSTHROUGH = "1";
	public static String PAYNOW_SIGNING_KEY_ALIAS = null;
	public static String PAYNOW_ENTITY_ID = null;
	public static boolean INSECURE_AHBX_SEND = false;
	public static String PLATFORM_SSO_STRATEGY = null;
	public static String REMOTE_NOTIFICATION_URL = null;
	public static int OUTBOUND_REQUEST_TIMEOUT = -1;
	public static boolean TIMESHIFT_ENABLED = false;
	public static boolean REMOTE_EMAIL_ENABLED = false;
	public static String TIMESHIFT_URL = null;
	public static boolean ALLOW_MASKED_IDENTIFICATION_ID = false;
	public static String PLATFORM_IEDNTITY_LOOKUP_FIELD = null;
	public static String PLAFORM_IDENTITY_DEFAULT_ROLE = null;
	public static boolean PLAFORM_IDENTITY_JIT_CREATE = false;
	public static boolean PLAFORM_IDENTITY_JIT_SYNC = false;
	public static String ROLE_PROVISIONING_RULES_ROLE = null;
	public static String ROLE_PROVISIONING_RULES_FILE = null;
	
	public static String SSOHOST_IP_LIST = null;
	
	public static String TRUSTED_PARTIES;
	
	public static String TEMPORARY_USER_PASSWORD;
	
	@Value("#{configProp['platform.paynow.signingKeyAlias'] != null ? configProp['platform.paynow.signingKeyAlias'] : 'ghix-ws-client'}")
	public void setPaynowSigningAlias(String signingAlias) {
		PAYNOW_SIGNING_KEY_ALIAS = signingAlias.trim();
	}
	
	@Value("#{configProp['platform.paynow.entityId'] != null ? configProp['platform.paynow.entityId'] : 'www.coveredca.com'}")
	public void setPaynowEntityId(String payuNowId) {
		PAYNOW_ENTITY_ID = payuNowId.trim();
	}
	@Value("#{configProp['ahbx.notices.send_insecure'] != null ? configProp['ahbx.notices.send_insecure'] : 'false'}")
	public void setInsecureRemoteSend(String insecureRemoteSend) {
		INSECURE_AHBX_SEND = insecureRemoteSend.trim().equalsIgnoreCase("true");
	}
	@Value("#{configProp['platform.sso.strategy'] != null ? configProp['platform.sso.strategy'] : 'SP'}")
	public void setSSOStrategy(String ssoStrategy) {
		PLATFORM_SSO_STRATEGY = ssoStrategy.trim();
	}
	
	@Value("#{configProp['platform.notices.remoteUrl'] != null ? configProp['platform.notices.remoteUrl'] : 'http://localhost:8085/sendEmail'}")
	public void setRemoteNotificationUrl(String remoteUrl) {
		REMOTE_NOTIFICATION_URL = remoteUrl.trim();
	}

	@Value("#{configProp['platform.http_out.timeout'] != null ? configProp['platform.http_out.timeout'] : '-1'}")
	public void setOutboundHttpTimeout(String timeInMillis) {
		try {
			int val = Integer.parseInt(timeInMillis.trim());
			if(val > 0) {
				OUTBOUND_REQUEST_TIMEOUT = val;
			}
		}catch(NumberFormatException ne) {
			OUTBOUND_REQUEST_TIMEOUT = 90000;
		}
	}
	
	@Value("#{configProp['platform.timeshift.enabled'] != null ? configProp['platform.timeshift.enabled'] : 'false'}")
	public void setTimeshiftEnabled(String enabled) {
		TIMESHIFT_ENABLED = enabled.trim().equalsIgnoreCase("true");
	}
	
	@Value("#{configProp['platform.remoteemail.enabled'] != null ? configProp['platform.remoteemail.enabled'] : 'false'}")
	public void setRemoteEmailEnabled(String enabled) {
		REMOTE_EMAIL_ENABLED = enabled.trim().equalsIgnoreCase("true");
	}
	
	
	
	@Value("#{configProp['platform.timeshift.url'] != null ? configProp['platform.timeshift.url'] : 'http://localhost:8079/timeshift'}")
	public void setTimeshiftUrl(String url) {
		TIMESHIFT_URL = url.trim();
	}
	
	@Value("#{configProp['eligibility.at.idValidationSkip'] != null ? configProp['eligibility.at.idValidationSkip'] : 'false'}")
	public void setIdentificationIdValidationSkip(String skipAllowed) {
		ALLOW_MASKED_IDENTIFICATION_ID = skipAllowed.equalsIgnoreCase("true");
	}
	
	@Value("#{configProp['platform.identity.jit_create'] != null ? configProp['platform.identity.jit_create'] : 'true'}")
	public void setJustInTimeCreateEnabled(String jitCreate) {
		PLAFORM_IDENTITY_JIT_CREATE = jitCreate.equals("true");
	}
	
	@Value("#{configProp['platform.identity.jit_sync'] != null ? configProp['platform.identity.jit_sync'] : 'true'}")
	public void setJustInTimeSyncEnabled(String jitSync) {
		PLAFORM_IDENTITY_JIT_SYNC = jitSync.equals("true");
	}
	
	@Value("#{configProp['platform.identity.default_role'] != null ? configProp['platform.identity.default_role'] : 'INDIVIDUAL'}")
	public void setDefauleProvisioningRole(String role) {
		PLAFORM_IDENTITY_DEFAULT_ROLE = role;
	}
	
	@Value("#{configProp['platform.identity.lookup_field'] != null ? configProp['platform.identity.lookup_field'] : 'ExternalId'}")
	public void setIdentityLookupField(String lookupField) {
		PLATFORM_IEDNTITY_LOOKUP_FIELD = lookupField;
	}
	
	@Value("#{configProp['ghixIdentitySvcUrl'] != null ? configProp['ghixIdentitySvcUrl'] : 'http://localhost:8080/ghix-identity/'}")
	public void setGhixIdentitySvcURL(String ghixIdentitySvcURL) {
	 	IDENTITY_SVC_URL = ghixIdentitySvcURL;
	}
	
	@Value("#{configProp['platform.gi-identity.endpoint'] != null ? configProp['platform.gi-identity.endpoint'] : 'http://localhost:8080/gi-identity/'}")
	public void setGiIdentitySvcURL(String giIdentitySvcURL) {
	 	GI_IDENTITY_SVC_URL = giIdentitySvcURL;
	}
	
	@Value("#{configProp['saml.alias'] != null ? configProp['saml.alias'] : 'YHI'}")
	public void setSamlAlias(String alias){
		SAML_ALIAS = alias;
	}
	
	@Value("#{configProp['scim.user.endpoint'] != null ? configProp['scim.user.endpoint'] : 'https://localhost:9443/wso2/scim/Users'}")
	public void setSCIMUserEndpoint(String userEndpoint){
		SCIM_USER_ENDPOINT = userEndpoint;
	}

	@Value("#{configProp['scim.group.endpoint'] != null ? configProp['scim.group.endpoint'] : 'https://localhost:9443/wso2/scim/Groups'}")
	public void setSCIMGroupEndpoint(String groupEndpoint){
		SCIM_USER_GROUP_ENDPOINT = groupEndpoint;
	}
	
	@Value("#{configProp['scim.provisioning.user'] != null ? configProp['scim.provisioning.user'] : 'opadmin@ghix.com'}")
	public void setSCIMUser(String scimUserName){
		SCIM_ADMIN_USER = scimUserName;
	}
	
	@Value("#{configProp['saml.token.validation_key']}")
	public void setTokenValidationKey(String key){
		TOKEN_VALIDATION_KEY = key;
	}
	
	@Value("#{configProp['scim.provisioning.password'] != null ? configProp['scim.provisioning.password'] : 'notadmin123#'}")
	public void setSCIMUserPassword(String scimPass){
		SCIM_ADMIN_PASS = scimPass;
	}
		
	@Value("#{configProp['scim.enabled'] != null ? configProp['scim.enabled'] : 'false'}")
	public void setSCIMEnabeed(String scimEnabled){
		SCIM_ENABLED = scimEnabled.equalsIgnoreCase("true");
	}
	
	@Value("#{configProp['scim.temporary.user.password'] != null ? configProp['scim.temporary.user.password'] : 'Ghix123#'}")
	public void setTempPassword(String tempPassword){
		TEMPORARY_USER_PASSWORD = tempPassword;
	}
	
	
	/**
	* Endpoints for services hosted by ghix-identity module
	* 
	* @author Nikhil Talreja
	*
	*/
	public static class IdentityServiceEndPoints{
			public static final String WSO2_UPDATE_CRED_ENDPOINT = IDENTITY_SVC_URL + "changepswd";
			public static final String WSO2_AUTHENTICATE_ENDPOINT = IDENTITY_SVC_URL + "authenticate";
			public static final String WSO2_ADMIN_CHANGE_PSSWD_ENDPOINT = IDENTITY_SVC_URL + "changepswdByAdmin";
			public static final String WSO2_CREATE_USER_ENDPOINT = IDENTITY_SVC_URL + "createUser";
			public static final String WSO2_FIND_USER_ENDPOINT = IDENTITY_SVC_URL + "findUser";
			public static final String WSO2_UPDATE_ATTRIBS_ENDPOINT = IDENTITY_SVC_URL + "updateUserAdditionalAttribs";
			public static final String WSO2_UPDATE_USER_ENDPOINT = IDENTITY_SVC_URL + "updateUser";
			public static final String WSO2_UPDATE_MULTI_ATTR_ENDPOINT = IDENTITY_SVC_URL +"setMultipleUserAttributes";
	 }
	
	///////////////////////////// END SCIM PARAMETERS //////////////////////////////
	
	@Value("#{configProp['SERFF_CSR_passkey']}")
	public void setSerffCSRPassKey(String passKey){
		SERFF_CSR_PASSKEY = convertPropertyValue(passKey);
	}
	
	
	@Value("#{configProp['wso2.availability_check.time_in_millis']}")
	public void setWso2AvailabilityCheckFrequency(String timeInMillis){
		int time = 60*1000;
		if(timeInMillis != null){
			try{
				time = Integer.parseInt(timeInMillis.trim());
			}catch(NumberFormatException ne){
				//set the default 60 sec
			}
		}
		WSO2_AVAILABILITY_CHECK_MILLIS = time;
	}
	
	@Value("#{configProp['platform.keystore.location']}")
	public void setKeyStoreLocation(String location){
		PLATFORM_KEY_STORE_LOCATION = convertPropertyValue(location);
	}
	
	@Value("#{configProp['platform.keystore.file']}")
	public void setKeyStoreFile(String file){
		PLATFORM_KEY_STORE_FILE = file;
	}
	
	@Value("#{configProp['platform.keystore.pass']}")
	public void setKeyStorePassword(String pass){
		PLATFORM_KEYSTORE_PASS = convertPropertyValue(pass);
	}
	
	@Value("#{configProp['idalink.forgotpassword.url']}")
	public void setIdalinkForgotPasswordUrl(String idalinkForgotPasswordUrl) {
		IDALINK_FORGOT_PASSWORD_URL = idalinkForgotPasswordUrl;
	}
	
	@Value("#{configProp['saml.trusted.parties']}")
	public void setTrustedParties(String trustedParties) {
		TRUSTED_PARTIES = trustedParties;
	}
	
	@Value("#{configProp['platform.session_tracker.local_node_ip']}")
	public void setLocalNodeIp(String location){
		LOCAL_NODE_IP = location;
	}
	
	@Value("#{configProp['platform.privateKey.alias']}")
	public void setPlatformKeyAlias(String alias){
		PLATFORM_KEY_ALIAS = alias;
	}
	
	@Value("#{configProp['platform.privateKey.password']}")
	public void setPlatformKeyPassword(String password){
		String pass = password.trim();
		if(pass.startsWith(ENC_START_TAG) && pass.endsWith(ENC_END_TAG)){
			pass = convertPropertyValue(pass);
		}
		PLATFORM_KEY_PASSWORD = pass;
	}
	
	@Value("#{configProp['platform.session_tracker.local_node_port']}")
	public void setLocalNodePort(int port){
		LOCAL_NODE_PORT = port;
	}
	
	@Value("#{configProp['platform.session_tracker.access_protocol']}")
	public void setLocalNodeAccessProtocol(String protocol){
		LOCAL_NODE_PROTOCOL = protocol;
	}
	
	@Value("#{configProp['platform.session_tracker.multiple_sessions_mode']}")
	public void setMultipleSessionMode(String multiple_session_mode){
		MULTIPLE_SESSION_MODE = multiple_session_mode;
	}
	
	@Value("#{configProp['platform.sso.use_externalSpTemplate'] != null ? configProp['platform.sso.use_externalSpTemplate'] : 'false'}")
	public void setExternalTemplateEnabled(String useExternalTemplate){
		EXTERNAL_TEMPLATE_ENABLED = Boolean.valueOf(useExternalTemplate);
	}
	
	@Value("#{configProp['platform.sso.externalTemplateLocation'] != null ? configProp['platform.sso.externalTemplateLocation'] : '/opt/web/ghixhome/ghix-setup/conf/saml'}")
	public void setExternalTemplateLocation(String externalTemplateLocation){
		EXTERNAL_TEMPLATE_LOCATION = externalTemplateLocation;
	}
	
	@Value("#{configProp['platform.sso.defaultIdpMetadata'] != null ? configProp['platform.sso.defaultIdpMetadata'] : 'sso_ghixqa_com_443.xml'}")
	public void setDefaultIdpMetadatae(String idpMetadata){
		RESIDENT_IDP_METADATA = idpMetadata;
	}
	
	@Value("#{configProp['platform.sso.sp_template_md'] != null ? configProp['platform.sso.sp_template_md'] : 'metadata.vm'}")
	public void setDefaultSpMetadataTemplate(String spMetadataTemplate){
		DEFAULT_SP_METADATA_TEMPLATE = spMetadataTemplate;
	}
	
	@Value("#{configProp['platform.sso.sp_template_acs'] != null ? configProp['platform.sso.sp_template_acs'] : 'acs.vm'}")
	public void setDefaultSpAcsTemplate(String spAcsTemplate){
		DEFAULT_SP_ACS_TEMPLATE = spAcsTemplate;
	}
	
	@Value("#{configProp['platform.intra_gi_sso.ssoHostIpList']}")
	public void setServiceHostIpList(String serviceHostIp){
		SSOHOST_IP_LIST = serviceHostIp;
	}
	
	@Value("#{configProp['platform.provisioning.bootstrap_file']}")
	public void setProvisioningRolesBootstrapFile(String fileName){
		ROLE_PROVISIONING_RULES_FILE = fileName;
	}
	
	@Value("#{configProp['platform.provisioning.role']}")
	public void setRoleProvisioningRole(String roleName){
		ROLE_PROVISIONING_RULES_ROLE = roleName;
	}
	
	@Value("#{configProp['platform.session_tracker.node_profile'] != null ? configProp['platform.session_tracker.node_profile'] : 'web'}")
	public void setNodeProfile(String profile){
		PLATFORM_NODE_PROFILE = profile;
	}
	
	public static String PLATFORM_LOCAL_CACHED_GIAPPCONFIGS;
	@Value("#{configProp['platform.localCacheProperties'] != null ? configProp['platform.localCacheProperties'] : ''}")
	public void setLocalCacheProperties(String propertyNames){
		PLATFORM_LOCAL_CACHED_GIAPPCONFIGS = propertyNames;
	}
	
	//Heart beat config starts
	@Value("#{configProp['platform.ping_url'] != null ? configProp['platform.ping_url'] : ''}")
	public void setPingUrl(String url){

		if( ! StringUtils.isEmpty(url) ) {
			url += (url.contains("?")) ? "&callback?" : "?callback?"; 
		}
		PLATFORM_PING_URL = url;
	}
	
	
	//in seconds, by default 15 mints
	@Value("#{configProp['platform.ping_schedule'] != null ? configProp['platform.ping_schedule'] : '900000'}")
	public void setPingSchedule(String schedule){
		PLATFORM_PING_SCHEDULE = schedule;
	}
	
	//Heart beat config ends
	
	/**
	 * 
	 * Multitenant configuration properties
	 * 
	 * 
	 */
	/**
	 * TODO: Defaulting to true to support state
	 */
	public static boolean MULTITENANT_DEFAULT_FALLBACK = true;
	/**
	 * TODO: Defaulting to ghixDS. Consider agreement on the name.
	 */
	public static String MULTITENANT_DEFAULT_DATASOURCE_JNDI_NAME = "ghixDS";
	
	@Value("#{configProp['platform.multitenant.default_fallback']}")
	public void setDefaultFallBack(Boolean defaultFallBack) {
		if(null!=defaultFallBack && !defaultFallBack){
			MULTITENANT_DEFAULT_FALLBACK = defaultFallBack;
		}
	}
	
	@Value("#{configProp['platform.mt.default_datasource_jndiname']}")
	public void setDefaultDatasourceJndiName(String defaultDatasourceJndiname) {
		if(null!=defaultDatasourceJndiname && defaultDatasourceJndiname.trim().length()>0){
			MULTITENANT_DEFAULT_DATASOURCE_JNDI_NAME = defaultDatasourceJndiname.trim();
		}
	}
	
	private static  String convertPropertyValue(final String originalValue) {
		//System.out.println("Converting value:"+originalValue);
		if(originalValue == null){
			return null;
		}
		if(originalValue.startsWith(ENC_START_TAG) && originalValue.endsWith(ENC_END_TAG)){
			try {
				return GhixAESCipherPool.decrypt(getInnerEncryptedValue(originalValue));
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| InvalidKeySpecException | NoSuchPaddingException
					| InvalidAlgorithmParameterException
					| UnsupportedEncodingException | IllegalBlockSizeException
					| BadPaddingException e) {
				throw new GIRuntimeException("Failed to decrypt the property ["+originalValue+"]"+e.getMessage(),e);
			}
		}
		if(originalValue.startsWith(SYS_PROP_START_TAG)){
			try {
			//	System.out.println("Evaluating the property:"+originalValue);
				return getSystemProperty(originalValue);
			} catch (GIRuntimeException e) {
				throw new GIRuntimeException("Failed to decrypt the property ["+originalValue+"]"+e.getMessage(),e);
			}
		}
		return originalValue;
	}


	private static String getSystemProperty(String originalValue) {
		StringBuilder sb = new StringBuilder();
		String tmp = null;
		int endTag = originalValue.indexOf(SYS_PROP_END_TAG);
		if(endTag == -1){
		//	System.out.println("No End tag found for "+originalValue);
			return originalValue;
		}
		String sysProperty = originalValue.substring(SYS_PROP_START_TAG.length(),endTag);
	//	System.out.println("Looking for System property:"+sysProperty+" in System properties");
		
		String propValue = System.getProperty(sysProperty);
		if(propValue == null){
			// Try the environment
			//System.out.println("Looking for System property:"+sysProperty+" in Environment");
			propValue = System.getenv(sysProperty);
		}
	//	System.out.println("Retrieved property ["+sysProperty+"]:"+propValue);
		sb.append(propValue);
		sb.append(originalValue.substring(endTag+1));
		tmp = sb.toString();
	//	System.out.println("Resolved:"+tmp);
		return tmp;
	}
	private static String getInnerEncryptedValue(String value) {
		return value.substring(ENC_START_TAG.length(),
				(value.length() - ENC_END_TAG.length()));
	}
	
	// GI Monitor Configuration
	public static String GIMONITOR_STORE = "ORACLE";
	
	@Value("#{configProp['gimonitor.store'] != null ? configProp['gimonitor.store'] : 'ORACLE'}")
	public void setGIMonitorCouchBaseEnabled(String giMonitorStore) {
		GIMONITOR_STORE = giMonitorStore;
	}	
	
	public static String GTM_CONTAINER_ID;
	@Value("#{configProp['GTMContainerID'] != null ? configProp['GTMContainerID'] : ''}")
	public void setGTM_CONTAINER_ID(String gTM_CONTAINER_ID) {
		GTM_CONTAINER_ID = gTM_CONTAINER_ID;
	}
	
	public String getIDPInitiatedSSO(String redirectUrl) {
		if(this.metadata == null) {
			throw new RuntimeException("No IDP metadata available");
		}
		try {
			if (IDP_CONFIG.equalsIgnoreCase("NOT AVAILABLE")) {
				logger.info("No IDP configiration available");
				return null;
			}

			IDPSSODescriptor idpSSODescriptor = SAMLUtil.getIDPDescriptor(metadata, metadata.getDefaultIDP());
			List<SingleSignOnService> idpList = idpSSODescriptor.getSingleSignOnServices();
			String url = null;
			for (SingleSignOnService service : idpList) {
				url = service.getLocation();
				if(logger.isInfoEnabled()) {
					logger.info("Found single sign on URL {}"+url);
				}
				if (url != null) {
					break;
				}
			}
			if (url != null) {
				if(redirectUrl == null) {
					url = url.replace("samlv20", "initiatesso?providerid="+SAML_ALIAS);
				}else {
					url = url.replace("samlv20", "initiatesso?providerid="+SAML_ALIAS+"&returnurl="+redirectUrl);
				}
				if(logger.isInfoEnabled()) {
					logger.info("IDP Initiated SSO URL:{}",url);
				}
				return url;
			}

		} catch (MetadataProviderException e) {
			logger.error("Error:", e);
		}
		return null;
	}
	
	public static void main(String[] args)throws Exception {
		String host = "https://7ntk685owc.execute-api.us-gov-west-1.amazonaws.com/nv/ghix/";
		URL url = new URL(host);
		System.out.println(url.getHost());
		System.out.println(url.getPath());
		System.out.println(url.getRef());
		
		
	}
}
