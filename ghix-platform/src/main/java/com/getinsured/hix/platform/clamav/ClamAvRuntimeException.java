package com.getinsured.hix.platform.clamav;

/**
 * Thrown to indicate that a method has failed while processing ECM request.
 *
 */
public class ClamAvRuntimeException extends RuntimeException {

	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1053471180051791761L;
	
	private String errorCode;
	
	public String getErrorCode(){
		return this.errorCode;
	}
	
	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}

	public ClamAvRuntimeException() {
		super();
	}

	public ClamAvRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClamAvRuntimeException(String message) {
		super(message);
	}
	
	public ClamAvRuntimeException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public ClamAvRuntimeException(Throwable cause) {
		super(cause);
	}



}
