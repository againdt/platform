package com.getinsured.hix.platform.security;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;


/**
 * @author Biswakalyan
 * @since  May 03, 2014
 *
 */
public class GhixEncryptorTag extends TagSupport {

	private String value;
	private boolean isurl;

	private static final Logger LOGGER = LoggerFactory.getLogger(GhixEncryptorTag.class);

	private static final long serialVersionUID = 1;
	private ApplicationContext applicationContext;
	

	@Override
    public int doStartTag() throws JspException {
		applicationContext = RequestContextUtils.getWebApplicationContext(
				pageContext.getRequest(),
				pageContext.getServletContext()
			);
		GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil = applicationContext.getBean("ghixJasyptEncrytorUtil",GhixJasyptEncrytorUtil.class);
		try{
			String encryptValue = isurl?ghixJasyptEncrytorUtil.encryptStringByJasypt(getValue()):"GENCS"+ghixJasyptEncrytorUtil.encryptStringByJasypt(getValue())+"GENCE";
			if(LOGGER.isTraceEnabled()){
				LOGGER.trace("Target Value"+getValue()+" Encrypted to "+encryptValue);
			}
			JspWriter out = pageContext.getOut();
			out.print(encryptValue);
		}
		catch (Exception e) {
            LOGGER.error("Exception Thrown"+e);
        }
        return SKIP_BODY;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public boolean isIsurl() {
		return isurl;
	}


	public void setIsurl(boolean isurl) {
		this.isurl = isurl;
	}
	
	


}
