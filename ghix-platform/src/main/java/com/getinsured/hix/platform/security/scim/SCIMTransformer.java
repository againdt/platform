/**
 * 
 */
package com.getinsured.hix.platform.security.scim;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.wso2.charon.core.attributes.Attribute;
import org.wso2.charon.core.attributes.ComplexAttribute;
import org.wso2.charon.core.attributes.MultiValuedAttribute;
import org.wso2.charon.core.attributes.SimpleAttribute;
import org.wso2.charon.core.objects.User;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author Biswakesh.Praharaj
 * 
 */
public class SCIMTransformer {

	private static final Logger LOGGER = Logger
			.getLogger(SCIMTransformer.class);

	/**
	 * Method to convert GHIX user to SCIM user, expects User object to have UserRoles already populated
	 * 
	 * @param accountUser
	 *            object containing GHIX user attributes
	 * @return User object containing SCIM user attributes
	 */
	public User convertGhixToScim(AccountUser accountUser) {
		User scimUser = null;
		String id = null;
		int isPrivileged = 0;
		Set<UserRole> userRoles = accountUser.getUserRole();
		for(UserRole userRole:userRoles){
			if(userRole.getRole().getPrivileged() == 1){
				isPrivileged = 1;
			}
		}
		ComplexAttribute wso2ExtensionAttribute = null;
		try {
			scimUser = new User();
			
			id = accountUser.getExtnAppUserId();
			if (null != id && id.length() > 0) {
				scimUser.setId(id);
			}
			
			scimUser.setUserName(accountUser.getEmail().trim());
			
			wso2ExtensionAttribute = createWSO2ExtensionAttribute(accountUser,isPrivileged);
			scimUser.setAttribute(wso2ExtensionAttribute);
		} catch (Exception e) {
			LOGGER.error("WHILE CONVERTING TO SCIM USER", e);
		}
		return scimUser;
	}

	/**
	 * Method to convert SCIM user to GHIX user
	 * 
	 * @param scimUser
	 *            object containing SCIM user attributes
	 * @return AccountUser object containing GHIX user attributes
	 */
	public AccountUser convertScimToGhix(User scimUser) {
		AccountUser accountUser = null;
		String id = null;
		MultiValuedAttribute multiValuedAttribute = null;
		List<String> simpleValues = null;
		Iterator<String> iterator = null;
		try {
			accountUser = new AccountUser();
			id = scimUser.getId();
			if (null != id && id.length() > 0) {
				accountUser.setUuid(scimUser.getId());
			}
			accountUser.setUserName(scimUser.getUserName());
			accountUser.setFirstName(scimUser.getGivenName());
			accountUser.setLastName(scimUser.getFamilyName());
			accountUser.setEmail(scimUser.getEmails()[0]);
			multiValuedAttribute = (MultiValuedAttribute)scimUser.getAttribute("phoneNumbers");
			if (null != multiValuedAttribute) {
				simpleValues = multiValuedAttribute.getValuesAsStrings();
				if (null != simpleValues
						&& !simpleValues.isEmpty()) {
					iterator= simpleValues
							.iterator();
					while (iterator.hasNext()) {
						String phoneNumber = iterator.next();
						accountUser.setPhone(phoneNumber);
					}
				}
			}
			accountUser.setExternPin(scimUser.getExternalId());
			
			
		} catch (Exception e) {
			LOGGER.error("WHILE CONVERTING TO GHIX USER", e);
		}
		return accountUser;
	}
	
	/**
	 * Method to create WSO2Extension element
	 * 
	 * @param accountUser
	 *            Object containing GHIX user attributes
	 * @param attrMap
	 *            Map to populate SCIM WSO2 Extension attributes with
	 * @return ComplexAttribute representing the WSO2Extension element
	 * @throws GIException
	 */
	public ComplexAttribute createWSO2ExtensionAttribute(AccountUser accountUser, int isPrivileged)
			throws GIException {
		ComplexAttribute wso2ExtensionAttribute = null;
		GIException ex = null;
		Map<String, Attribute> attrMap = null;
		try {
			attrMap = new HashMap<String, Attribute>();
			createAndAddAttribute(attrMap, SCIMClientConstants.FIRST_NAME_ATTR,
					accountUser.getFirstName());
			createAndAddAttribute(attrMap, SCIMClientConstants.LAST_NAME_ATTR,
					accountUser.getLastName());
			createAndAddAttribute(attrMap, SCIMClientConstants.MAIL_ATTR,
					accountUser.getEmail());
			createAndAddAttribute(attrMap, SCIMClientConstants.PHONE_NUMBER_ATTR,
					accountUser.getPhone());
			createAndAddAttribute(attrMap, SCIMClientConstants.PREF_METH_COMM_ATTR,
					"email");
			createAndAddAttribute(attrMap,
					SCIMClientConstants.SECURITY_QUESTION_ATTR,
					accountUser.getSecurityQuestion1());
			createAndAddAttribute(attrMap, SCIMClientConstants.SECURITY_ANSWER_ATTR,
					accountUser.getSecurityAnswer1());
			createAndAddAttribute(attrMap, SCIMClientConstants.ACCOUNT_TYPE_CODE,
					Integer.toString(isPrivileged));
			wso2ExtensionAttribute = new ComplexAttribute(
					SCIMClientConstants.WSO2_EXTENSION_ATTR,
					SCIMClientConstants.WSO2_EXTENSION_SCHEMA);
			if(LOGGER.isDebugEnabled()){
				printAttributesBeingSent(attrMap);
			}
			wso2ExtensionAttribute.setSubAttributes(attrMap);
		} catch (Exception e) {
			LOGGER.error("ERR WHILE SETTING WSO2 EXTENSION ELEMENTS: ", e);
			ex = new GIException(e.getMessage(), e);
		}
		if (null != ex) {
			throw ex;
		}

		return wso2ExtensionAttribute;
	}
	
	
	private void printAttributesBeingSent(Map<String, Attribute> attrMap) {
		Set<Entry<String,Attribute>> entrySet = attrMap.entrySet();
		Entry<String,Attribute> entry = null;
		Iterator<Entry<String,Attribute>> cursor = entrySet.iterator();
		while(cursor.hasNext()){
			entry = cursor.next();
			LOGGER.debug("Name ="+entry.getKey()+"URI = "+entry.getValue().getAttributeURI()+ ", Value="+entry.getValue().getName());
		}
		
	}

	/**
	 *  Method to create WSO2Extension element
	 * @param dob String containing date of birth
	 * @param ssn String containing social security number
	 * @param preferredLanguage String containinig preferred language
	 * @param ridpFlag String containing ridp flag
	 * @return ComplexAttribute representing the WSO2Extension element
	 * @throws GIException
	 */
	public ComplexAttribute addWSO2ExtensionAdditonalAttributes(String dob, String ssn, String preferredLanguage, String ridpFlag)
			throws GIException {
		ComplexAttribute wso2ExtensionAttribute = null;
		GIException ex = null;
		Map<String, Attribute> attrMap = null;
		try {
			attrMap = new HashMap<String, Attribute>();
			createAndAddAttribute(attrMap, SCIMClientConstants.DOB_ATTR,
					dob);
			createAndAddAttribute(attrMap, SCIMClientConstants.SSN_ATTR, ssn);
			createAndAddAttribute(attrMap,
					SCIMClientConstants.PREFERRED_LANGUAGE_ATTR, preferredLanguage);
			createAndAddAttribute(attrMap, SCIMClientConstants.RIDP_FLAG_ATTR, ridpFlag);
			wso2ExtensionAttribute = new ComplexAttribute(
					SCIMClientConstants.WSO2_EXTENSION_ATTR,
					SCIMClientConstants.WSO2_EXTENSION_SCHEMA);
			wso2ExtensionAttribute.setSubAttributes(attrMap);
		} catch (Exception e) {
			LOGGER.error("ERR WHILE SETTING WSO2 EXTENSION ELEMENTS: ", e);
			ex = new GIException(e.getMessage(), e);
		}
		if (null != ex) {
			throw ex;
		}

		return wso2ExtensionAttribute;
	}

	/**
	 * Method to set simple attributes in attribute map
	 * 
	 * @param attrMap
	 *            Map of attributes stored in key,attribute format
	 * @param attrName
	 *            Name of the attribute
	 * @param attrValue
	 *            Attribute value
	 * @throws GIException
	 */
	public void createAndAddAttribute(Map<String, Attribute> attrMap,
			String attrName, String attrValue) throws GIException {
		Attribute attribute = null;
		GIException ex = null;
		try {
			if (null != attrName && attrName.length() > 0 && null != attrValue) {
				attribute = new SimpleAttribute(attrName, attrValue.trim());
				if (null != attribute) {
					attrMap.put(attrName, attribute);
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERR WHILE SETTING WSO2 EXTENSION ELEMENTS: ", e);
			ex = new GIException(e.getMessage(), e);
		}
		if (null != ex) {
			throw ex;
		}
	}

}
