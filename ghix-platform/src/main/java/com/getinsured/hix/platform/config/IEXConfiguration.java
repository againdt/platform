package com.getinsured.hix.platform.config;

public class IEXConfiguration {

	public enum IEXConfigurationEnum implements PropertiesEnumMarker
	{
		VERIFY_STATE_MEC_NM ("iex.verify.state.mec.nm"),
		STATE_MEC_TOLERENCE_LIMIT_NM("iex.state.mec.tolerance.limit.nm"),
		STATE_MEC_ELIGIBILITY_PERIOD_LIMIT_NM("iex.state.mec.eligibility.period.limit.nm"),
		FINANCIAL_SSAP_URL ("iex.financialSsapUrl"),
		IND_PORTAL_OE_ENROLL_START_DATE("iex.oe_enroll_start_date"),
		IND_PORTAL_OE_ENROLL_END_DATE("iex.oe_enroll_end_date"),
		IND_PORTAL_GLOBAL_COVERAGE_DATE("iex.indportal.global_oe_coverage_effective_date"),
		IEX_PRESCREENER_SHOW_CURRENT_YEAR("iex.prescreener.show_current_year"),
		IEX_PRESCREENER_SHOW_NUMBER_OF_PREV_YEAR("iex.prescreener.show_number_of_previous_year"),
		IND_PORTAL_SHOW_PREVIOUS_YEAR_TAB("iex.indportal.show_previous_year_tab_until"),
		IND_PORTAL_SHOW_CURRENT_YEAR_TAB("iex.indportal.show_current_year_tab_from"),
		IND_PORTAL_SHOW_CURRENT_YEAR_TAB_PRIVILEDGE_USER("iex.indportal.show_current_year_tab_for_priviledge_user_from"),
		IEX_PREV_OE_START_DATE("iex.prev_oe_start_date"),
		IEX_PREV_OE_END_DATE("iex.prev_oe_end_date"),
		IEX_PREV_COVERAGE_YEAR("iex.prev_coverage_year"),
		IEX_CURRENT_OE_START_DATE("iex.current_oe_start_date"),
		IEX_CURRENT_OE_END_DATE("iex.current_oe_end_date"),
		IEX_CURRENT_COVERAGE_YEAR("iex.current_coverage_year"),
		ACCOUNT_TRANSFER_COVERAGE_YEARS("iex.accounttransfer.coverage.years"),
		IEX_RENEWAL_COVERAGE_YEAR("iex.renewal_coverage_year"),
		IEX_ONE_TOUCH_RENEWAL_YEAR("iex.otr_year"),
		IEX_ONE_TOUCH_RENEWAL_START_DATE("iex.otr_start_date"),
		IEX_PRESCREENER_PREGNANCY_INDICATOR("iex.prescreener.pregnancy_indicator"),
		IEX_SKIP_OEP_QLE_VALIDATION("iex.skip.oep.qle.validation"),
		IEX_ONE_TOUCH_RENEWAL_END_DATE("iex.otr_end_date"),
		IEX_CUSTOM_GROUPING("iex.customGrouping"),
		IEX_GROUPING_KEEP_NON_STANDARD_RELATIONSHIP_SEPARATE("iex.grouping.keep_non_standard_relationships_separate"),
		IEX_PRESCREENER_MEDICAID_FAMILYSIZE_INCR_PREGNANCY("iex.prescreener.medicaid_familysize_increment_for_pregency"),
		IEX_PRESCREENER_MEDICAID_USE_LATEST_FPL_FROM("iex.prescreener.latest_medicaid_povertyguidelines_start_date"),
		IEX_AT_HOUSEHOLD_CASE_ID_ENABLE("iex.AT.householdcaseid.enable"),
		IEX_AT_DATA_MIGRATION_ENABLE("iex.at.datamigration.enable"),
		IEX_AT_LINK_USER("iex.AT.linkuser"),
		IEX_REMIND_PREFERENCES_REVIEW_DAYS("iex.remindPreferencesReviewDays"),
		IEX_PREFERENCES_SHOWPAPERLESS1095OPTION("iex.preferences.showPaperless1095Option"),
		IEX_DASHBOARD_VERSION("iex.dashboardVersion"),
		IEX_ADDITIONALINFO_TOBACCO("iex.additionalInfo.tobacco"),
		IEX_ADDITIONALINFO_HARDSHIP("iex.additionalInfo.hardship"),
		// HIX-111341 Added config to turn on/off Multiple eligibility span 
		IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG("iex.AT.MES"),
		IEX_AT_TIME_TRAVEL("iex.AT.timeTravel"),
		IEX_AT_AHBX_APTC_RECALC_ENABLE("iex.AT.aptcRecalc.enable"),
		IND_PORTAL_DELAY_POPUP("iex.Delay.popup"),
		IEX_SSAP_FLOW_VERSION("iex.ssapFlowVersion"),
		IEX_ASK_QLE_IN_OEP("iex.referral.ask.QLE.in.OEP"),
		IEX_OEP_EFFECTIVE_START_DATE("iex.referral.OEP.ESD"),
		IEX_ALLOW_AUTO_ADD_DENTAL("iex.referral.allow.auto.add.dental"),
		// HIX-114300 Renew batch process
		IEX_HEALTH_AGE_OUT_NFA("iex.HEALTH_AGE_OUT_NFA"),
		IEX_HEALTH_AGE_OUT_FA("iex.HEALTH_AGE_OUT_FA"),
		IEX_DENTAL_AGE_OUT("iex.DENTAL_AGE_OUT"),
		IEX_APPLY_APTC_FOR_DENTAL("iex.apply.aptc.for.dental"),
		IEX_IS_CONSENT_PAGE_ENABLED("iex.isConsentPageEnabled"),
		IEX_VALIDATE_AT_ELEMENTS("iex.AT.validateATElementValues"),
		IEX_LINK_AT_USING_APPLICATION_ID("iex.linkATUsingApplicationID"),
		IEX_QEP_DURING_OE_END_DATE("iex.QEPDuringOEEndDate"),
		IEX_QEP_DURING_OE_START_DATE("iex.QEPDuringOEStartDate"),
		IEX_DISENROLL_FROM_DENTAL("iex.disenrollFromDental"),
		IEX_DEFER_ACCOUNT_ACTIVATION_EMAIL("iex.deferAccountActivationEMail"),
		IEX_DEFER_ELIGIBILITY_EMAIL("iex.deferEligibilityEmail"),
		IEX_MARK_MEDICAID_APP_TO_ER("iex.mark_medicaid_app_to_er"),
		IEX_ACTIVE_RENEWAL_ENABLED("iex.activeRenewal.enabled"),
		//HIX-118227 Create configuration to enable or disable allow plan selection logic for SEP window
		IEX_PLAN_SELECTION_ENABLED("iex.portal.planSelectionEnabled"),
		IEX_CHANGE_TO_AUTOMATE("iex.referral.change.to.automate");
		
		private final String value;

		@Override
		public String getValue(){return this.value;}

		private IEXConfigurationEnum(String value){
	        this.value = value;
	    }
	};

}
