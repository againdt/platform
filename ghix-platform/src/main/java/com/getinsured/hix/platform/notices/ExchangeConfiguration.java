package com.getinsured.hix.platform.notices;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author Sunil Desu
 * @since September 17 2013
 */
@Component
@Deprecated
public class ExchangeConfiguration{

	private static final Logger LOGGER = Logger.getLogger(ExchangeConfiguration.class);
	
	
	public static String	EXCHANGE_TYPE ="global.ExchangeType";

	public static String	STATE_NAME ="global.StateName";

	public static String	STATE_CODE ="global.StateCode";

	public static String	EXCHANGE_NAME ="global.ExchangeName";

	public static String	EXCHANGE_PHONE ="global.ExchangePhone";

	public static String	EXCHANGE_URL ="global.ExchangeURL";

	public static String	CITY_NAME ="global.CityName";

	public static String	PIN_CODE ="global.PinCode";

	public static String	EXCHANGE_ADDRESS_1 ="global.ExchangeAddress1";

	public static String	EXCHANGE_ADDRESS_2 ="global.ExchangeAddress2";

	public static String	EXCHANGE_FULL_NAME ="global.ExchangeFullName";

	public static String	COUNTRY_NAME ="global.CountryName";
		
	public static String	DEFAULT_TENANT ="global.DefaultTenant";
}
