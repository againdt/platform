package com.getinsured.hix.platform.eventlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.AppEvent;

public interface IAppEventRepository extends JpaRepository<AppEvent, Integer> {
}
