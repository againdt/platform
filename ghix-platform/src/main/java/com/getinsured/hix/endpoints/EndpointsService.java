package com.getinsured.hix.endpoints;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.getinsured.hix.endpoints.annotation.EndpointDescription;
import com.getinsured.hix.endpoints.dto.Endpoint;
/**
 * 
 * @author desu_s
 *
 */
@Component
public class EndpointsService {
	
	public List<Endpoint> transform(RequestMappingHandlerMapping requestMappingHandlerMapping) {
		List<Endpoint> endpoints = new ArrayList<Endpoint>(0);
		Map<RequestMappingInfo, HandlerMethod> methodsMap = requestMappingHandlerMapping.getHandlerMethods();
		populateUrlAndMethodInfo(methodsMap, endpoints, requestMappingHandlerMapping);
		return endpoints;
	}

	private void populateUrlAndMethodInfo(Map<RequestMappingInfo, HandlerMethod> methodsMap, List<Endpoint> endpoints, RequestMappingHandlerMapping requestMappingHandlerMapping) {
		Endpoint endpoint = null;
		for (RequestMappingInfo rmi : methodsMap.keySet()) {
			endpoint = new Endpoint();
			endpoint.setMethodName(methodsMap.get(rmi).getMethod().getName());
			endpoint.setClassName(methodsMap.get(rmi).getMethod().getDeclaringClass().getName());
			endpoint.setUrl(rmi.getPatternsCondition().toString());
			endpoint.setHttpMethod(rmi.getMethodsCondition().toString());
			Method method = methodsMap.get(rmi).getMethod();
			addEndpointDescription(method, endpoint);
			addPermissionInfomation(method, endpoint);
			endpoints.add(endpoint);
		}
	}

	private void addPermissionInfomation(Method method, Endpoint endpoint) {
		String permissionName = null;
		PreAuthorize preAuthorize = method.getAnnotation(PreAuthorize.class);
		if (null != preAuthorize) {
			String[] hasPermissionValues = preAuthorize.value().split(",");
			permissionName = hasPermissionValues[hasPermissionValues.length - 1];
			int index = permissionName.indexOf("'");
			permissionName = permissionName.substring(index + 1, permissionName.length() - 1);
			index = permissionName.indexOf("'");
			permissionName = permissionName.substring(0, index);
		}
		endpoint.setPermission(permissionName);
	}

	private void addEndpointDescription(Method method, Endpoint endpoint) {
		String description = null;
		EndpointDescription endpointDescription = method.getAnnotation(EndpointDescription.class);
		if (null != endpointDescription) {
			description = endpointDescription.description();
		}
		endpoint.setDescription(description);
	}

}
