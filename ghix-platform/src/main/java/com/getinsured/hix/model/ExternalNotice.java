package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import com.getinsured.timeshift.util.TSDate;

import java.util.Date;

@Entity
@Audited
@Table(name="notices_external")
public class ExternalNotice implements Serializable, Cloneable {
	
	public ExternalNotice() {
		
	}

	public enum ExternalNoticesStatus {
		  INIT,	
		  QUEUED,
		  SENT,
		  FAILED,
		  RESEND
}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NOTICES_EXTERNAL_SEQ")
	@SequenceGenerator(name = "NOTICES_EXTERNAL_SEQ", sequenceName = "NOTICES_EXTERNAL_SEQ", allocationSize = 1)
	@Column(name = "id")
	private int id;
	
	@Audited
	@Column(name = "ssap_id")
	private int ssapId;
	
	@Audited
	@Column(name = "notice_id")
	private int noticeId;
	
	@Audited
	@Column(name = "external_household_case_id")
	private String externalHouseholdCaseId;
	
	@Audited
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private ExternalNoticesStatus statusType;
	
	@Audited
	@Column(name = "request_payload")
	private String requestPayload;
	
	@Audited
	@Column(name = "response_payload")
	private String responsePayload;
	
	@Audited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;
	
	@Audited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	@Audited
	@Column(name = "retry_count")
	private int retryCount;
	
	
	public int getRetryCount() {
		return retryCount;
	}


	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSsapId() {
		return ssapId;
	}


	public void setSsapId(int ssapId) {
		this.ssapId = ssapId;
	}


	public int getNoticeId() {
		return noticeId;
	}


	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}


	public String getExternalHouseholdCaseId() {
		return externalHouseholdCaseId;
	}


	public void setExternalHouseholdCaseId(String externalHouseholdCaseId) {
		this.externalHouseholdCaseId = externalHouseholdCaseId;
	}
	
	public ExternalNoticesStatus getStatusType() {
		return statusType;
	}
	
	public void setStatusType(ExternalNoticesStatus statusType) {
		this.statusType = statusType;
	}
	
	public String getRequestPayload() {
		return requestPayload;
	}


	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}


	public String getResponsePayload() {
		return responsePayload;
	}


	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(Date created) {
		this.created = created;
	}


	public Date getUpdated() {
		return updated;
	}


	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}
	
	public ExternalNotice(int id, int ssapId, int noticeId, String externalHouseholdCaseId,
			String statusType, String requestPayload, String responsePayload, Date created,
			Date updated, int retryCount) {
		this.id = id;
		this.ssapId = ssapId;
		this.noticeId = noticeId;
		this.externalHouseholdCaseId = externalHouseholdCaseId;
		this.statusType = ExternalNoticesStatus.valueOf(statusType);
		this.requestPayload = requestPayload;
		this.responsePayload = responsePayload;
		this.created = created;
		this.updated = updated;
		this.retryCount = retryCount;
	}

}
