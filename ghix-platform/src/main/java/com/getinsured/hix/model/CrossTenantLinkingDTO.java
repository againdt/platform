package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class CrossTenantLinkingDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1923799155478520952L;
	
	private Boolean crossTenantLinkingEnabled;
	private Long tenantId;
	private String tenantName;
	
	private Long affiliateId;
	private String affiliateName;
	
	private Integer affiliateFlowId;
	private String affiliateFlowName;
	
	public Boolean isCrossTenantLinkingEnabled() {
		return crossTenantLinkingEnabled;
	}
	public void setCrossTenantLinkingEnabled(Boolean crossTenantLinkingEnabled) {
		this.crossTenantLinkingEnabled = crossTenantLinkingEnabled;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}
	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}
	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getAffiliateName() {
		return affiliateName;
	}
	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	public String getAffiliateFlowName() {
		return affiliateFlowName;
	}
	public void setAffiliateFlowName(String affiliateFlowName) {
		this.affiliateFlowName = affiliateFlowName;
	}
	
	
	
	

}
