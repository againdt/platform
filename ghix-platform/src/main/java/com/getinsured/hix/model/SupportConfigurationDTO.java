package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * Call center configuration for Affiliate and Affiliate Flow.
	 * 
 * <p>
 *  This class contains configuration for call center phone numbers and agent scripts.
 * </p>
	 */
public class SupportConfigurationDTO implements Serializable
{
  private static final long serialVersionUID = -1192167709654880159L;
	private String callCenterHours;
	private String medicareCallCenterHours;
  private Boolean enableMedicareScripts;
  private String welcomeScriptUrl;
  private String preSalesScriptUrl;
  private String mapdEnrollmentScriptUrl;
  private String maEnrollmentScriptUrl;
  private String mediGapScriptUrl;
  private String pdpEnrollmentScriptUrl;
  private String agentFlowNotes;
  private String drxClientId;

  public String getCallCenterHours()
  {
		return callCenterHours;
	}

  public void setCallCenterHours(String callCenterHours)
  {
		this.callCenterHours = callCenterHours;
	}

  public String getMedicareCallCenterHours()
  {
		return medicareCallCenterHours;
	}

  public void setMedicareCallCenterHours(String medicareCallCenterHours)
  {
		this.medicareCallCenterHours = medicareCallCenterHours;
	}

  public Boolean getEnableMedicareScripts()
  {
    return enableMedicareScripts;
  }

  public void setEnableMedicareScripts(Boolean enableMedicareScripts)
  {
    this.enableMedicareScripts = enableMedicareScripts;
  }

  public String getWelcomeScriptUrl()
  {
    return welcomeScriptUrl;
  }

  public void setWelcomeScriptUrl(String welcomeScriptUrl)
  {
    this.welcomeScriptUrl = welcomeScriptUrl;
  }

  public String getPreSalesScriptUrl()
  {
    return preSalesScriptUrl;
  }

  public void setPreSalesScriptUrl(String preSalesScriptUrl)
  {
    this.preSalesScriptUrl = preSalesScriptUrl;
  }

  public String getMapdEnrollmentScriptUrl()
  {
    return mapdEnrollmentScriptUrl;
  }

  public void setMapdEnrollmentScriptUrl(String mapdEnrollmentScriptUrl)
  {
    this.mapdEnrollmentScriptUrl = mapdEnrollmentScriptUrl;
  }

  public String getMaEnrollmentScriptUrl()
  {
    return maEnrollmentScriptUrl;
  }

  public void setMaEnrollmentScriptUrl(String maEnrollmentScriptUrl)
  {
    this.maEnrollmentScriptUrl = maEnrollmentScriptUrl;
  }

  public String getMediGapScriptUrl()
  {
    return mediGapScriptUrl;
  }

  public void setMediGapScriptUrl(String mediGapScriptUrl)
  {
    this.mediGapScriptUrl = mediGapScriptUrl;
  }

  public String getPdpEnrollmentScriptUrl()
  {
    return pdpEnrollmentScriptUrl;
  }

  public void setPdpEnrollmentScriptUrl(String pdpEnrollmentScriptUrl)
  {
    this.pdpEnrollmentScriptUrl = pdpEnrollmentScriptUrl;
  }

  public String getAgentFlowNotes()
  {
    return agentFlowNotes;
  }

  public void setAgentFlowNotes(String agentFlowNotes)
  {
    this.agentFlowNotes = agentFlowNotes;
  }

  public String getDrxClientId()
  {
    return drxClientId;
  }

  public void setDrxClientId(String drxClientId)
  {
    this.drxClientId = drxClientId;
  }
}
