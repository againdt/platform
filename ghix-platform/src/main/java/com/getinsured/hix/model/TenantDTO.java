package com.getinsured.hix.model;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.getinsured.hix.model.sso.IdentityManagement;
import com.getinsured.hix.model.sso.XtraConfiguration;

public class TenantDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 713417525974305312L;
	private Long id;
	private String code;
	private Date createdDate;
	private String name;
	private String url;
	private String isActive;
	private String testTenant;
	private String tenantProvisionigUser = null;
	private String tenantProvisionigUserPassword = null;
	private boolean remotelyManagedUsers = false;

	private ConfigurationDTO configuration;
	private TenantSSOConfiguration tenantSSOConfiguration;
	private String tenantDomain;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ConfigurationDTO getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ConfigurationDTO configuration) {
		this.configuration = configuration;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTestTenant() {
		return testTenant;
	}

	public void setTestTenant(String testTenant) {
		this.testTenant = testTenant;
	}

	public TenantSSOConfiguration getTenantSSOConfiguration()
	{
		return tenantSSOConfiguration;
	}

	public void setTenantSSOConfiguration(TenantSSOConfiguration tenantSSOConfiguration)
	{
		this.tenantSSOConfiguration = tenantSSOConfiguration;
	}
	
	private IdentityManagement getIdentityManagementConfig(){
		IdentityManagement idMgt = null;
		if(this.tenantSSOConfiguration != null){
			XtraConfiguration xTraConfig = this.tenantSSOConfiguration.getXtraConfiguration();
			if(xTraConfig != null){
				idMgt = xTraConfig.getIdentityManagement();
			}
		}
		return idMgt;
	}
	public String getTenantProvisionigUser() {
		if(this.tenantProvisionigUser != null){
			return this.tenantProvisionigUser;
		}
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			this.tenantProvisionigUser = idMgt.getTenantAdminUsername();
		}
		return this.tenantProvisionigUser;
	}

	public void setTenantProvisionigUser(String tenantProvisionigUser) {
		if(this.tenantSSOConfiguration != null){
			this.tenantSSOConfiguration.getXtraConfiguration().getIdentityManagement().setTenantAdminUsername(tenantProvisionigUser);;
		}
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			idMgt.setTenantAdminUsername(tenantProvisionigUser);
		}
		this.tenantProvisionigUser = tenantProvisionigUser;
	}

	public String getTenantProvisionigUserPassword() {
		if(this.tenantProvisionigUserPassword != null){
			return this.tenantProvisionigUserPassword;
		}
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			this.tenantProvisionigUserPassword = idMgt.getTenantAdminPassword();
		}
		return this.tenantProvisionigUserPassword;
	}
	
	public String getTenantDomain() {
		if(this.tenantDomain != null){
			return this.tenantDomain;
		}
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			this.tenantDomain = idMgt.getTenantDomain();
		}
		return this.tenantDomain;
	}
	
	public void setTenantDomain(String tenantDomain) {
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			idMgt.setTenantDomain(tenantDomain);
		}
		this.tenantDomain = tenantDomain;
	}

	public void setTenantProvisionigUserPassword(String tenantProvisionigUserPassword) {
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			idMgt.setTenantAdminPassword(tenantProvisionigUserPassword);
		}
		this.tenantProvisionigUserPassword = tenantProvisionigUserPassword;
	}

	public boolean isRemotelyManagedUsers() {
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null && !this.remotelyManagedUsers){
			this.remotelyManagedUsers = idMgt.isRemoteUserManagementEnabled();
		}
		return this.remotelyManagedUsers;
	}

	public void setRemotelyManagedUsers(boolean remotelyManagedUsers) {
		IdentityManagement idMgt = this.getIdentityManagementConfig();
		if(idMgt != null){
			idMgt.setRemoteUserManagement(remotelyManagedUsers);
		}
		this.remotelyManagedUsers = remotelyManagedUsers;
	}
	
	

}

class CustomDateSerializer extends JsonSerializer<Date> {    
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2) throws 
        IOException, JsonProcessingException {      

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(value);

        gen.writeString(formattedDate);
    }
}
