package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

@Entity
@Table(name="nodes")
public class Nodes implements Serializable, JSONAware{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nodegen_seq")
	@SequenceGenerator(name = "nodegen_seq", sequenceName = "nodes_seq", allocationSize = 1)
	private int id;
	
	@Column(name="ip_addresses")
	private String ipAddresses = null;
	
	@Column(name="last_active")
	private Timestamp lastActive = null;
	
	@Column(name="is_enabled")
	private boolean isEnabled = false;
	
	@Column(name="node_type")
	private String nodeType = null;
	
	@Column(name="alias")
	private String alias = null;
	
	@Column(name="base_url")
	private String baseUrl = null;

	public int getId() {
		return id;
	}

	public String getIpAddresses() {
		return ipAddresses;
	}

	public Timestamp getLastActive() {
		return lastActive;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public String getNodeType() {
		return nodeType;
	}

	public String getAlias() {
		return alias;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setIpAddresses(String ipAddresses) {
		this.ipAddresses = ipAddresses;
	}

	public void setLastActive(Timestamp lastActive) {
		this.lastActive = lastActive;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
		result = prime * result + ((ipAddresses == null) ? 0 : ipAddresses.hashCode());
		result = prime * result + (isEnabled ? 1231 : 1237);
		result = prime * result + ((lastActive == null) ? 0 : lastActive.hashCode());
		result = prime * result + ((nodeType == null) ? 0 : nodeType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nodes other = (Nodes) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (baseUrl == null) {
			if (other.baseUrl != null)
				return false;
		} else if (!baseUrl.equals(other.baseUrl))
			return false;
		if (ipAddresses == null) {
			if (other.ipAddresses != null)
				return false;
		} else if (!ipAddresses.equals(other.ipAddresses))
			return false;
		if (isEnabled != other.isEnabled)
			return false;
		if (lastActive == null) {
			if (other.lastActive != null)
				return false;
		} else if (!lastActive.equals(other.lastActive))
			return false;
		if (nodeType == null) {
			if (other.nodeType != null)
				return false;
		} else if (!nodeType.equals(other.nodeType))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject nodeObj = new JSONObject();
		if(this.alias.compareToIgnoreCase("use_ip_address") == 0) {
			nodeObj.put("alias".intern(), this.ipAddresses);
		}else {
			nodeObj.put("alias".intern(), this.alias);
		}
		nodeObj.put("ip_addresses".intern(), this.ipAddresses);
		
		if(this.baseUrl.compareToIgnoreCase("use_ip_address") == 0) {
			nodeObj.put("base_url".intern(), "http://" + this.ipAddresses);
		}else {
			nodeObj.put("base_url".intern(), this.baseUrl);
		}
		
		nodeObj.put("enabled".intern(), this.isEnabled);
		nodeObj.put("node_type".intern(), this.nodeType);
		nodeObj.put("lastActive".intern(), this.lastActive.toString());
		return nodeObj.toJSONString();
	}
}
