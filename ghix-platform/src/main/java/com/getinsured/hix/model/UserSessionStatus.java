package com.getinsured.hix.model;

public enum UserSessionStatus {
	INIT("INIT"), CLOSED("CLOSED"), TIMEDOUT("TIMED_OUT"), IN_PROGRESS("IN_PROGRESS"), CLEANED_UP("CLEANED_UP"), USER_LOGOUT("USER_LOGOUT"), STALE_SESSION("STALE_SESSION"), DUP_STALE_SESSION("DUP_STALE_SESSION"), USER_LOGOUT_BY_APPLICATION("USER_LOGOUT_BY_APPLICATION");
	
	private String val;

	UserSessionStatus(String val){
		this.val = val;
	}
	
	public String toString(){
		return this.val;
	}

}
