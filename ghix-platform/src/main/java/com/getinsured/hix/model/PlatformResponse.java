package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.platform.ecm.Content;

/**
 * @author Ekram Ali Kazi
 *
 */

public class PlatformResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<Location> validAddressList;

	private String ahbxEcmId;

	private Content giContent;

	public List<Location> getValidAddressList() {
		return validAddressList;
	}

	public void setValidAddressList(List<Location> validAddressList) {
		this.validAddressList = validAddressList;
	}

	public String getAhbxEcmId() {
		return ahbxEcmId;
	}

	public void setAhbxEcmId(String ahbxEcmId) {
		this.ahbxEcmId = ahbxEcmId;
	}

	public Content getGiContent() {
		return giContent;
	}

	public void setGiContent(Content giContent) {
		this.giContent = giContent;
	}



}
