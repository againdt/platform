package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.getinsured.hix.dto.tenant.EappTenantDTO;
import com.getinsured.hix.platform.enums.OnOffEnum;
import com.getinsured.platform.im.conf.LDAPConfiguration;
import com.getinsured.platform.im.conf.SCIMConfiguration;

public class ConfigurationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2252181929816181130L;
	private List<ProductConfigurationDTO> productConfigurations;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String eventNotificationUrl;
	private UIConfigurationDTO uiConfiguration; 
	private SupportConfigurationDTO support; 
	private EmailConfigurationDTO emailConfiguration; 
	private EappTenantDTO eappConfiguration;
	private Map<String, String> productLabelMap;
	private Map<String, String> productLinkOutUrlMap;
	private Map<String, Boolean> productNewWindowMap;
	private ExitFlowConfigurationDTO exitFlowConfigurations;
	private ExitFlowConfigurationDTO incompleteExitFlowConfigurations;
	private EmxFlowConfigurationDTO emxFlowConfigurations;
	private CampaignDTO campaignConfigurations;
	private CrossTenantLinkingDTO crossTenantLinkingConfigurations;
	private SelfServiceRegistrationConfigurationDTO selfServiceRegistrationConfiguration;
	private PlansDTO plansConfiguration;
	private List<String> hiosIdConfiguration;
	private String enableMultiLanguageSupport;
	private String  extendedOEPContent;
	private String onExchangePriority;// Please refer to the HIX-102317 for the usage of this config.
	private OnOffEnum showPreferencesPopup;
	// Stores the company-wide daily goals for selling the products by CSR/Agent 
	private CapAgentGoalsDto capAgentGoalsDto;
	private SCIMConfiguration scimConfiguration;
	private LDAPConfiguration ldapConfiguration;
	//HIX-92363
	private OnOffEnum appointmentScheduler;
	// HIX-100347, HIX-98001: Expose additional RAS data for agent
	private String rasApiKey;
	private String rasAddtlInfoUrl;

	private OnOffEnum redirectToAgentExpress;
	
	private String signingKey;
	private String secretKey;
	
	public OnOffEnum getRedirectToAgentExpress() {
		return redirectToAgentExpress;
	}

	public void setRedirectToAgentExpress(OnOffEnum redirectToAgentExpress) {
		this.redirectToAgentExpress = redirectToAgentExpress;
	}

	public String getEventNotificationUrl() {
		return eventNotificationUrl;
	}

	public void setEventNotificationUrl(String eventNotificationUrl) {
		this.eventNotificationUrl = eventNotificationUrl;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UIConfigurationDTO getUiConfiguration() {
		return uiConfiguration;
	}

	public void setUiConfiguration(UIConfigurationDTO uiConfiguration) {
		this.uiConfiguration= uiConfiguration;
	}

	public EappTenantDTO getEappConfiguration() {
		return eappConfiguration;
	}

	public void setEappConfiguration(EappTenantDTO eappConfiguration) {
		this.eappConfiguration = eappConfiguration;
	}

	public List<ProductConfigurationDTO> getProductConfigurations() {
		return productConfigurations;
	}

	public void setProductConfigurations(List<ProductConfigurationDTO> productConfigurations) {
		this.productConfigurations = productConfigurations;
	}
	
	public EmailConfigurationDTO getEmailConfiguration() {
		return emailConfiguration;
	}
	
	public void setEmailConfiguration(EmailConfigurationDTO emailConfiguration) {
		this.emailConfiguration = emailConfiguration;
	}
	
	public String getProductLabel(String productCode) {
		String label = null;
		if(StringUtils.isNotBlank(productCode) && productConfigurations != null && !productConfigurations.isEmpty()) {
			if(productLabelMap == null) {
				productLabelMap = new HashMap<>();
				for (ProductConfigurationDTO productConfigurationDTO : productConfigurations) {
					if(!BooleanUtils.isFalse(productConfigurationDTO.getSelected())) {
						productLabelMap.put(productConfigurationDTO.getCode(), productConfigurationDTO.getLabel());
					}
				}
			} 
			label = productLabelMap.get(productCode); 
		}
		return label;
	}
	
	public String getProductUrl(String productCode) {
		String productUrl = null;
		if(StringUtils.isNotBlank(productCode) && productConfigurations != null && !productConfigurations.isEmpty()) {
			if(productLinkOutUrlMap == null) {
				productLinkOutUrlMap = new HashMap<>();
				for (ProductConfigurationDTO productConfigurationDTO : productConfigurations) {
					if(!BooleanUtils.isFalse(productConfigurationDTO.getSelected())) {
						productLinkOutUrlMap.put(productConfigurationDTO.getCode(), productConfigurationDTO.getLinkOutUrl());
					}
				}
			}
			productUrl = productLinkOutUrlMap.get(productCode); 
		}
		return productUrl;
	}

	public boolean getNewWindow(String productCode) {
		boolean newWindow = false;
		if(StringUtils.isNotBlank(productCode) && productConfigurations != null && !productConfigurations.isEmpty()) {
			if(productNewWindowMap == null) {
				productNewWindowMap = new HashMap<>();
				for (ProductConfigurationDTO productConfigurationDTO : productConfigurations) {
					if(BooleanUtils.isTrue(productConfigurationDTO.getNewWindow())) {
						productNewWindowMap.put(productConfigurationDTO.getCode(), productConfigurationDTO.getNewWindow());
					} else {
						productNewWindowMap.put(productConfigurationDTO.getCode(), false);
					}
				}
			}
			newWindow = productNewWindowMap.get(productCode); 
		}
		
		return newWindow;
	}

	public ExitFlowConfigurationDTO getExitFlowConfigurations() {
		return exitFlowConfigurations;
	}

	public void setExitFlowConfigurations(
			ExitFlowConfigurationDTO exitFlowConfigurations) {
		this.exitFlowConfigurations = exitFlowConfigurations;
	}

	public ExitFlowConfigurationDTO getIncompleteExitFlowConfigurations() {
		return incompleteExitFlowConfigurations;
	}

	public void setIncompleteExitFlowConfigurations(ExitFlowConfigurationDTO incompleteExitFlowConfigurations) {
		this.incompleteExitFlowConfigurations = incompleteExitFlowConfigurations;
	}

	public EmxFlowConfigurationDTO getEmxFlowConfigurations() {
		return emxFlowConfigurations;
	}

	public void setEmxFlowConfigurations(
			EmxFlowConfigurationDTO emxFlowConfigurations) {
		this.emxFlowConfigurations = emxFlowConfigurations;
	}

	public SupportConfigurationDTO getSupport() {
		return support;
	}

	public void setSupport(SupportConfigurationDTO support) {
		this.support = support;
	}

	public CampaignDTO getCampaignConfigurations() {
		return campaignConfigurations;
	}

	public void setCampaignConfigurations(CampaignDTO campaignConfigurations) {
		this.campaignConfigurations = campaignConfigurations;
	}

	public CrossTenantLinkingDTO getCrossTenantLinkingConfigurations() {
		return crossTenantLinkingConfigurations;
	}

	public void setCrossTenantLinkingConfigurations(CrossTenantLinkingDTO crossTenantLinkingConfigurations) {
		this.crossTenantLinkingConfigurations = crossTenantLinkingConfigurations;
	}

	public SelfServiceRegistrationConfigurationDTO getSelfServiceRegistrationConfiguration() {
		return selfServiceRegistrationConfiguration;
	}

	public void setSelfServiceRegistrationConfigurationDTO(
			SelfServiceRegistrationConfigurationDTO selfServiceRegistrationConfiguration) {
		this.selfServiceRegistrationConfiguration = selfServiceRegistrationConfiguration;
	}

	/**
	 * @return the plansConfiguration
	 */
	public PlansDTO getPlansConfiguration() {
		return plansConfiguration;
	}

	/**
	 * @param plansConfiguration the plansConfiguration to set
	 */
	public void setPlansConfiguration(PlansDTO plansConfiguration) {
		this.plansConfiguration = plansConfiguration;
	}

	public List<String> getHiosIdConfiguration() {
		return hiosIdConfiguration;
	}

	public void setHiosIdConfiguration(List<String> hiosIdConfiguration) {
		this.hiosIdConfiguration = hiosIdConfiguration;
	}
	
	public String getEnableMultiLanguageSupport() {
		if(enableMultiLanguageSupport==null || enableMultiLanguageSupport.trim().length()==0){
			enableMultiLanguageSupport="NO";
		}
		return enableMultiLanguageSupport;
	}

	public void setEnableMultiLanguageSupport(String enableMultiLanguageSupport) {
		this.enableMultiLanguageSupport = enableMultiLanguageSupport;
	}

	public String getExtendedOEPContent() {
		return extendedOEPContent;
	}

	public void setExtendedOEPContent(String extendedOEPContent) {
		this.extendedOEPContent = extendedOEPContent;
	}

	public CapAgentGoalsDto getCapAgentGoalsDto() {
		return capAgentGoalsDto;
	}

	public void setCapAgentGoalsDto(CapAgentGoalsDto capAgentGoalsDto) {
		this.capAgentGoalsDto = capAgentGoalsDto;
	}

	public SCIMConfiguration getScimConfiguration() {
		return scimConfiguration;
	}

	public void setScimConfiguration(SCIMConfiguration scimConfiguration) {
		this.scimConfiguration = scimConfiguration;
	}

	public LDAPConfiguration getLdapConfiguration() {
		return ldapConfiguration;
	}

	public void setLdapConfiguration(LDAPConfiguration ldapConfiguration) {
		this.ldapConfiguration = ldapConfiguration;
	}

	public OnOffEnum getShowPreferencesPopup() {
		return showPreferencesPopup;
	}

	public void setShowPreferencesPopup(OnOffEnum showPreferencesPopup) {
		this.showPreferencesPopup = showPreferencesPopup;
	}

	public OnOffEnum getAppointmentScheduler() {
		return appointmentScheduler;
	}

	public void setAppointmentScheduler(OnOffEnum appointmentScheduler) {
		this.appointmentScheduler = appointmentScheduler;
	}

	public String getRasApiKey()
	{
		return rasApiKey;
	}

	public void setRasApiKey(String rasApiKey)
	{
		this.rasApiKey = rasApiKey;
	}

	public String getRasAddtlInfoUrl()
	{
		return rasAddtlInfoUrl;
	}

	public void setRasAddtlInfoUrl(String rasAddtlInfoUrl)
	{
		this.rasAddtlInfoUrl = rasAddtlInfoUrl;
	}
	public String getOnExchangePriority() {
		return onExchangePriority;
	}

	public void setOnExchangePriority(String onExchangePriority) {
		this.onExchangePriority = onExchangePriority;
	}

	public String getSigningKey() {
		return signingKey;
	}

	public void setSigningKey(String signingKey) {
		this.signingKey = signingKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
}
