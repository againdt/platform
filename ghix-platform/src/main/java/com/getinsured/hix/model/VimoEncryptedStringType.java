package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.lang.ObjectUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.usertype.UserType;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class VimoEncryptedStringType implements UserType, ApplicationContextAware {

	public Class<String> returnedClass() {
		return String.class;
	}

	public int[] sqlTypes() {
		return new int[] { Types.VARCHAR };
	}

	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner)
			throws HibernateException, SQLException {
		String value = (String) StandardBasicTypes.STRING.nullSafeGet(resultSet, names[0], session);
		return ((value != null) ? decrypt(value) : null);
	}

	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)
			throws HibernateException, SQLException {
		StandardBasicTypes.STRING.nullSafeSet(preparedStatement, (value != null) ? encrypt(value.toString()) : null,
				index, session);
	}

	/* "default" implementations */

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		return ObjectUtils.equals(x, y);
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		assert(x != null);
		return x.hashCode();
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}

	private String encrypt(String data) {
		// Get Handle the VimoEncryptorBean
		/*
		 * ApplicationContext ctx = new
		 * ClassPathXmlApplicationContext("classpath*:/applicationContext.xml");
		 * VimoEncryptor _VE = (VimoEncryptor)ctx.getBean("vimoencryptor");
		 */
		VimoEncryptor _VE = (VimoEncryptor) GHIXApplicationContext.getBean("vimoencryptor");
		return _VE.encrypt(data);
	}

	private String decrypt(String data) {
		// Get Handle the VimoEncryptorBean
		/*
		 * ApplicationContext ctx = new
		 * ClassPathXmlApplicationContext("classpath*:/applicationContext.xml");
		 * VimoEncryptor _VE = (VimoEncryptor)ctx.getBean("vimoencryptor");
		 */
		VimoEncryptor _VE = (VimoEncryptor) GHIXApplicationContext.getBean("vimoencryptor");
		return _VE.decrypt(data);
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		ac = arg0;

	}

	ApplicationContext ac;
}
