package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author Suresh Kancherla
 */
public class EmxFlowConfigurationDTO implements Serializable
{
	private static final long serialVersionUID = 78959475208360980L;
	
  public enum AccessTypeConfig
  {
		NONE("NONE"), PIN("PIN"), SSN("SSN BASED") ;

		private String accessType;

    AccessTypeConfig(String type)
    {
			this.accessType = type;
		}

    public String getAccessType()
    {
			return accessType;
		}
	  }

  public enum HraAccountType
  {
		 
		

		INDIVIDUAL_MONTHLY_FUNDING("INDIVIDUAL_MONTHLY_FUNDING"), 
		INDIVIDUAL_QUARTERLY_FUNDING("INDIVIDUAL_QUARTERLY_FUNDING"), 
		INDIVIDUAL_ANNUAL_FUNDING("INDIVIDUAL_ANNUAL_FUNDING"),
		INDIVIDUAL_LUMPSUM_DEPOSIT("INDIVIDUAL_LUMPSUM_DEPOSIT"),
		// Disabling below options as we do not support right now (02/25/2016) - HIX-82622
		FAMILY_MONTHLY_FUNDING("FAMILY_MONTHLY_FUNDING") ,
		FAMILY_QUARTERLY_FUNDING("FAMILY_QUARTERLY_FUNDING"),
		FAMILY_ANNUAL_FUNDING("FAMILY_ANNUAL_FUNDING"),
		FAMILY_LUMPSUM_DEPOSIT("FAMILY_LUMPSUM_DEPOSIT"),
        NO_HRA_CONTRIBUTION("NO_HRA_CONTRIBUTION");
		
		private String accountType;

    HraAccountType(String type)
    {
			this.accountType = type;
		}

    public String getHraAccountType()
    {
			return accountType;
		}
	  }

	private boolean emxGatingEnabled;
	
	private AccessTypeConfig accessPinType=AccessTypeConfig.NONE; 
	
	//Retiree Configs
	private String retireeContributionExplanationText;
	private String retireeHRAAndAPTCWarningText;
	private String retireeDisclaimerText;
	private String retireeLearnMoreToolTipText;
  private boolean hraOnly;
	private HraAccountType retireeHraAccountType=HraAccountType.INDIVIDUAL_MONTHLY_FUNDING;
	private String[] supportedHraFundingTypes= new String[] {};
	
	
  public String[] getSupportedHraFundingTypes()
  {
		return supportedHraFundingTypes;
	}

  public void setSupportedHraFundingTypes(String[] supportedHraFundingTypes)
  {
		this.supportedHraFundingTypes = supportedHraFundingTypes;
	}

  public boolean isEmxGatingEnabled()
  {
		return emxGatingEnabled;
	}

  public void setEmxGatingEnabled(boolean emxGatingEnabled)
  {
		this.emxGatingEnabled = emxGatingEnabled;
	}

	@Override
  public String toString()
  {
		return "EmxFlowConfigurationDTO [emxGatingEnabled=" + emxGatingEnabled +" accessType"+accessPinType.accessType
				+ "]";
	}

  public AccessTypeConfig getAccessPinType()
  {
		return accessPinType;
	}

  public void setAccessPinType(AccessTypeConfig accessPinType)
  {
		this.accessPinType = accessPinType;
	}

  public String getRetireeContributionExplanationText()
  {
		return retireeContributionExplanationText;
	}

	public void setRetireeContributionExplanationText(
      String retireeContributionExplanationText)
  {
		this.retireeContributionExplanationText = retireeContributionExplanationText;
	}

	public String getRetireeHRAAndAPTCWarningText() {
		return retireeHRAAndAPTCWarningText;
	}
	
	public void setRetireeHRAAndAPTCWarningText(String retireeHRAAndAPTCWarningText) {
		this.retireeHRAAndAPTCWarningText = retireeHRAAndAPTCWarningText;
	}
	
  public String getRetireeDisclaimerText()
  {
		return retireeDisclaimerText;
	}

  public void setRetireeDisclaimerText(String retireeDisclaimerText)
  {
		this.retireeDisclaimerText = retireeDisclaimerText;
	}

  public String getRetireeLearnMoreToolTipText()
  {
		return retireeLearnMoreToolTipText;
	}

  public void setRetireeLearnMoreToolTipText(String retireeLearnMoreToolTipText)
  {
		this.retireeLearnMoreToolTipText = retireeLearnMoreToolTipText;
	}

  public HraAccountType getRetireeHraAccountType()
  {
		return retireeHraAccountType;
	}

  public void setRetireeHraAccountType(HraAccountType retireeHraAccountType)
  {
		this.retireeHraAccountType = retireeHraAccountType;
	}

  public static String[] getNames(Class<? extends Enum<?>> e)
  {
	    return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
	}

  public boolean isHraOnly()
  {
    return hraOnly;
}

  public void setHraOnly(boolean hraOnly)
  {
    this.hraOnly = hraOnly;
  }
}
