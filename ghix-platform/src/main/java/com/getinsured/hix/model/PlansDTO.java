/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * @author Krishna Gajulapalli
 *
 */
public class PlansDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static enum ShowPufPlansConfig {
		YES("YES"), NO("NO");
	
		private String label;
		
		ShowPufPlansConfig(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return this.label;
		}
	}	
	
	private ShowPufPlansConfig showPufPlans;

	/**
	 * @return the showPufPlans
	 */
	public ShowPufPlansConfig getShowPufPlans() {
		return showPufPlans;
	}

	/**
	 * @param showPufPlans the showPufPlans to set
	 */
	public void setShowPufPlans(ShowPufPlansConfig showPufPlans) {
		this.showPufPlans = showPufPlans;
	}

}
