package com.getinsured.hix.model;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class TenantRoleDTO {

	public String roleName;
	public boolean tenantDefault;
	public boolean active;
	public String tenantRoleId;
	public Long tenantId;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public boolean isTenantDefault() {
		return tenantDefault;
	}

	public void setTenantDefault(boolean tenantDefault) {
		this.tenantDefault = tenantDefault;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getTenantRoleId() {
		return tenantRoleId;
	}

	public void setTenantRoleId(String tenantRoleId) {
		this.tenantRoleId = tenantRoleId;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

}
