package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * The persistent class for the users database table.
 *
 */
@Audited
//@TypeDef(name = "ghixDBSafeHtml", typeClass = GhixDBSafeHtml.class)
@Entity
@Table(name="users")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class AccountUser implements Serializable,Cloneable, UserDetails, SortableEntity {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountUser.class);

	public interface AccountUserData extends Default{
		
	}
	
	public interface ManageEditAccountUser extends Default{
		
	}

	public enum user_status
	{
		Activate("Active"), Deactivate("Inactive"), InactiveDormant("Inactive-dormant"), Pending("Pending"), Locked("Locked");

		private String user_status;

		private user_status(String status) {

			this.user_status = status;
		}
		
		public String value(){
			return user_status;
		}
	}

	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface AddNewIssuer extends Default{
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountUser_Seq")
	@SequenceGenerator(name = "AccountUser_Seq", sequenceName = "users_seq", allocationSize = 1)
	private int id;

	/*
	 * HIX-32108 : Added to support unique salt
	 */
	@Column(name="uuid",unique = true)
	private String uuid;

	//bi-directional one-to-one association to Person
	/* @OneToOne(cascade = {CascadeType.ALL} )
    @JoinColumn(name="person_id")
	private Person person;*/
	@NotAudited
	@JsonManagedReference
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<UserRole> userRole = new HashSet<UserRole>();

	
	@Column(name="username",unique = true,nullable = false)
	private String userName;

	@Column(name="password" ,length=2000)
	@NotEmpty(message="{label.validatePassword}",groups={AccountUser.AccountUserData.class})
	private String password;

	@NotEmpty(message="{label.validateFirstName}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Column(name = "first_name")
	//@Type(type = "ghixDBSafeHtml")
	private String firstName;

	@NotEmpty(message="{label.validateLastName}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "title")
	private String title;

	
	@Column(name="security_question_1")
	private String securityQuestion1;

	
	@Column(name="security_answer_1")
	private String securityAnswer1;

	@Column(name="security_question_2")
	private String securityQuestion2;

	@Column(name="security_answer_2")
	private String securityAnswer2;

	@Column(name="security_question_3")
	private String securityQuestion3;

	@Column(name="security_answer_3")
	private String securityAnswer3;

	@Column(name="pin")
	private String pin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pin_expiration")
	private Date pinexpiration;

	@NotEmpty(message="{label.validateEmail}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Email(message="{label.validatePleaseEnterValidEmail}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Size(max=100,groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Column(name="email")
	private String email;

	@NotEmpty(message="{label.validatePhoneNo}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Size(min=10,max=10,message="{label.validatePhoneNo}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups={AccountUser.AccountUserData.class,AccountUser.AddNewIssuer.class,AccountUser.ManageEditAccountUser.class})
	@Column(name="phone", length=10)
	private String phone;

	@Column(name="recovery",unique = true,nullable = false)
	private String recovery;

	@Column(name="communication_pref")
	private String communicationPref;

	@Column(name="confirmed")
	private int confirmed;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date created;

	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;


	@Column(name="pass_recovery_token" ,length=4000)
	private String passwordRecoveryToken;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pass_recovery_token_expiration")
	private Date passwordRecoveryTokenExpiration;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="lastLogin")
	private Date lastLogin;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="startDate")
	private Date startDate;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="endDate")
	private Date endDate;


	@Column(name="ISSUPERUSER")
	private Integer isSuperUser;

	/*
	 * HIX-42019 : Persists extn_app_userid
	 */
	@Column(name="EXTN_APP_USERID",unique = true)
	private String extnAppUserId;

	/*@Transient ModuleUser activeModule stores users active module details.
	 * 1. When the user has logged in then activeModule will be the ModuleUser record that
	 *    had created while user's post-registration process
	 * 2. When broker is switching from broker role to employer role then activeModule will be
	 *    the ModuleUser record that matches;
	 *       a. moduleId=<switch to module id>
	 *       b. moduleName=<switch to module name> (its EMPLOYER_MODULE)
	 *       c. userId=loggedin user id
	 */

	@Column(name = "TENANT_ID")
	private Long tenantId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	    
	@Transient
	@JsonIgnore
	private ModuleUser activeModule;  // To be deprecated

	@Transient
	@JsonIgnore
	private int activeModuleId;

	@Transient
	@JsonIgnore
	private String activeModuleName;
	
	@Transient
	@JsonIgnore
	private String switchedRoleName;

	@Transient
	@JsonIgnore
	private boolean hasGhixProvisioned=true;

	@Transient
	@JsonIgnore
	private boolean authenticated = false;

	@Transient
	@JsonIgnore
	private String ghixProvisionRoleName;

	@Transient
	@JsonIgnore
	private String recordId;

	@Transient
	@JsonIgnore
	private String recordType;

	@Transient
	@JsonIgnore
	private String deligationAccessCode;

	/*
	 * JIRA : HIX-9827 ;Expose PIN in the Account User object
	 * Used to validate user's against external systems.
	 */
	@Transient
	@JsonIgnore
	private String externPin;

	/*
	 * JIRA : HIX-26903 Secondary role(s) sign up
	 *      : HIX-27181 Add controller method for secondary role provisioning.
	 */
	@Transient
	@JsonIgnore
	private Role defRole;
	
	@Transient
	@JsonIgnore
	private String personatedRoleName;

	

	@Transient
	@JsonIgnore
	private List<Integer> linkedModuleIds;
	
	public List<Integer> getLinkedModuleIds() {
		return linkedModuleIds;
	}

	public void setLinkedModuleIds(List<Integer> linkedModuleIds) {
		this.linkedModuleIds = linkedModuleIds;
	}

	public String getPersonatedRoleName() {
		return personatedRoleName;
	}

	public void setPersonatedRoleName(String personatedRoleName) {
		this.personatedRoleName = personatedRoleName;
	}

	/*
	 * JIRA : HIX-26903 Secondary role(s) sign up
	 *      : HIX-27181 Add controller method for secondary role provisioning.
	 */
	@Transient
	@JsonIgnore
	private int roleCount;
	
	@Transient
	@JsonIgnore
	private boolean isEmailChanged = false;
	
	 @Transient
	 @JsonIgnore
	 private String currentUserRole;
	 
	 @Transient
	 @JsonIgnore
	 private String samlPasswdTimeStamp;


	@Column(name="status")
	//@Transient
	@JsonIgnore
	private String status;

	@Column(name = "ffm_user_id")
	private String ffmUserId;

	@Column(name = "user_npn")
	private String userNPN;

	@NotAudited
	@Column(name="retry_count")
	private int retryCount;

	@NotAudited
	@Column(name="sec_que_retry_count")
	private int secQueRetryCount;



	//@NotAudited
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PWD_LAST_UPDATED")
	private Date PasswordLastUpdatedTimeStamp;
	
	@Column(name = "LAST_UPDATED_BY")
	@JsonIgnore
    private Integer lastUpdatedBy;
	
	@Transient
	@JsonIgnore
	private Map<String,String> loginContext;
	
	public AccountUser() {
	}

	public AccountUser(int id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public AccountUser(int id, String firstName, String lastName, Date lastLogin) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastLogin = lastLogin;
	}
	
	public AccountUser(int id, String userName, String firstName, String lastName, Date lastLogin) {
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastLogin = lastLogin;
	}
	
	public AccountUser(int id, String userName, String firstName, String lastName, Date lastLogin, Date created) {
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastLogin = lastLogin;
		this.created = created;
	}
	
	public AccountUser(int id, String userName, String firstName, String lastName, String secAns1, String secAns2, Date lastLogin) {
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.securityAnswer1 = secAns1;
		this.securityAnswer2 = secAns2;
		this.lastLogin = lastLogin;
	}
	

	/**
	 * Constructs new {@link AccountUser} object with only specified parameters filled.
	 *
	 * @param id user id
	 * @param userName user name.
	 * @param firstName first name.
	 * @param lastName last name.
	 * @param phoneNumber phone number.
	 */
	public AccountUser(final int id, final String userName, final String firstName, final String lastName, final String phoneNumber)
	{
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phoneNumber;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * HIX-32108 : Added to support unique salt
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * HIX-32108 : Added to support unique salt
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("AccountUser::setUserRole():Setting user roles"+userRole);
		}
		this.userRole = userRole;
		if(userRole != null && userRole.size() > 0){
			Iterator<UserRole> cursor = userRole.iterator();
			UserRole tmp = null;
			while(cursor.hasNext()){
				tmp = cursor.next();
				if(tmp.isDefaultRole()){
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("AccountUser::setUserRole():Setting default role"+tmp.getRole().getName());
					}
					tmp.setIsActive('Y');
					this.setDefRole(tmp.getRole());
				}
			}
		}
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		if(firstName != null){
			this.firstName = firstName.trim();
		}
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastname) {
		if(lastname != null){
		this.lastName = lastname.trim();
		}
	}

	public String getTitle() {
		return title;
	}

	public String getSecurityQuestion1() {
		return securityQuestion1;
	}

	public void setSecurityQuestion1(String securityQuestion1) {
		this.securityQuestion1 = securityQuestion1;
	}

	public String getSecurityAnswer1() {
		return securityAnswer1;
	}

	public void setSecurityAnswer1(String securityAnswer1) {
		this.securityAnswer1 = securityAnswer1;
	}

	public String getSecurityQuestion2() {
		return securityQuestion2;
	}

	public void setSecurityQuestion2(String securityQuestion2) {
		this.securityQuestion2 = securityQuestion2;
	}

	public String getSecurityAnswer2() {
		return securityAnswer2;
	}

	public void setSecurityAnswer2(String securityAnswer2) {
		this.securityAnswer2 = securityAnswer2;
	}

	public String getSecurityQuestion3() {
		return securityQuestion3;
	}

	public void setSecurityQuestion3(String securityQuestion3) {
		this.securityQuestion3 = securityQuestion3;
	}

	public String getSecurityAnswer3() {
		return securityAnswer3;
	}

	public void setSecurityAnswer3(String securityAnswer3) {
		this.securityAnswer3 = securityAnswer3;
	}

	/*
	 * Added to support HIX-46480 : Configuration to set the number of SecurityQuestion Sets 
	 * -Venkata Tadepalli
	 */
	
	public String getSecurityQuestion(int idx) {
		
		String secQuestion="";
		if(idx==1){
			secQuestion = this.getSecurityQuestion1();
		} else if(idx==2){
			secQuestion = this.getSecurityQuestion2();
		} else if(idx==3){
			secQuestion = this.getSecurityQuestion3();
		} else {
			secQuestion="";
		}
		
		if(StringUtils.isBlank(secQuestion)){
			secQuestion="";
		}
		return secQuestion;
	}
	
	/*
	 * Added to support HIX-46480 : Configuration to set the number of SecurityQuestion Sets 
	 * -Venkata Tadepalli
	 */
	public void setSecurityQuestion(int idx,String secQtn) {
		
		if(StringUtils.isBlank(secQtn)){
			secQtn="";
		}
		if(idx==1){
			this.setSecurityQuestion1(secQtn);
		} else if(idx==2){
			this.setSecurityQuestion2(secQtn);
		} else if(idx==3){
			this.setSecurityQuestion3(secQtn);
		} else {
			//NOTHING TO DO..Max no of quetions 3
		}
		
	}
	
	/*
	 * Added to support HIX-46480 : Configuration to set the number of SecurityQuestion Sets 
	 * -Venkata Tadepalli
	 */	
	public String getSecurityAnswer(int idx) {
		
		String secAnswer="";
		if(idx==1){
			secAnswer = this.getSecurityAnswer1();
		} else if(idx==2){
			secAnswer = this.getSecurityAnswer2();
		} else if(idx==3){
			secAnswer = this.getSecurityAnswer3();
		} else {
			secAnswer="";
		}
		
		if(StringUtils.isBlank(secAnswer)){
			secAnswer="";
		}
		return secAnswer;
	}
	
	/*
	 * Added to support HIX-46480 : Configuration to set the number of SecurityQuestion Sets 
	 * -Venkata Tadepalli
	 */	
	public void setSecurityAnswer(int idx, String secAns) {
		
		if(StringUtils.isBlank(secAns)){
			secAns="";
		}
		if(idx==1){
			this.setSecurityAnswer1(secAns);
		} else if(idx==2){
			this.setSecurityAnswer2(secAns);
		} else if(idx==3){
			this.setSecurityAnswer3(secAns);
		} else {
			//NOTHING TO DO..Max no of quetions 3
		}
		
	
	}
	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public Date getPinexpiration() {
		return pinexpiration;
	}

	public void setPinexpiration(Date pinexpiration) {
		this.pinexpiration = pinexpiration;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail(){

		return this.email;
	}

	public void setEmail(String email){
		if(email != null){
			this.email = email.trim().toLowerCase();
		}
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCommunicationPref() {
		return this.communicationPref;
	}

	public void setCommunicationPref(String communicationPref) {
		this.communicationPref = communicationPref;
	}

	public int getConfirmed() {
		return this.confirmed;
	}

	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}

	public String getRecovery() {
		return this.recovery;
	}

	public void setRecovery(String recovery) {
		this.recovery = recovery;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}



	public ModuleUser getActiveModule() {
		return activeModule;
	}

	public void setActiveModule(ModuleUser activeModule) {
		this.activeModule = activeModule;
	}




	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public boolean isHasGhixProvisioned() {
		return hasGhixProvisioned;
	}

	public void setHasGhixProvisioned(boolean hasGhixProvisioned) {
		this.hasGhixProvisioned = hasGhixProvisioned;
	}

	public String getGhixProvisionRoleName() {
		return ghixProvisionRoleName;
	}

	public void setGhixProvisionRoleName(String ghixProvisionRoleName) {
		this.ghixProvisionRoleName = ghixProvisionRoleName;
	}



	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}



	public String getDeligationAccessCode() {
		return deligationAccessCode;
	}

	public void setDeligationAccessCode(String deligationAccessCode) {
		this.deligationAccessCode = deligationAccessCode;
	}



	public String getExternPin() {
		return externPin;
	}

	public void setExternPin(String externPin) {
		this.externPin = externPin;
	}


	public String getExtnAppUserId() {
		return extnAppUserId;
	}

	public void setExtnAppUserId(String extnAppUserId) {
		this.extnAppUserId = extnAppUserId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		/*
		 * Save the user record with the default values
		 */
		Date now =new TSDate();
		this.setRecovery(Long.toString(TimeShifterUtil.currentTimeMillis()));
		this.setCreated(now);
		this.setUpdated(now);
		this.setPasswordLastUpdatedTimeStamp(now);
		this.setRetryCount(0);
		this.setSecQueRetryCount(0);
		if(this.getStatus()==null){
			this.setStatus("Active");
		}
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		if(GhixPlatformConstants.USER_NAME_POLICY.equals("SAME_AS_EMAIL")){
		if(this.email != null && this.userName != null && !this.email.equalsIgnoreCase(userName) ){
			throw new GIRuntimeException("Invalid user record, user's email and user name can not be different:[User Id"+this.getId()+"]");
		}
		Date now =new TSDate();
		//The user updated time is updated from user detail page.
		this.setUpdated(now);
	}
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return getAuthorities(getUserRole());
	}

	@Override
	@JsonIgnore
	public String getUsername() {
		return getUserName();
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired()
	{
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled()
	{
		//return this.status.equalsIgnoreCase("Active");
		return (this.confirmed == 1) ? true : false;
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Set<UserRole> userRoles)
	{
		String permissionType="ROLE";//
		//String permissionType="PERMISSION";

		Set<GrantedAuthority> authList = new HashSet<GrantedAuthority>();

		if(permissionType.equals("PERMISSION")){
			authList = getGrantedAuthorities(getUserPermissions(userRoles));
		} else {
			// DEFAULT permissionType="ROLE"
			authList= getGrantedAuthorities(getRoles(userRoles));
		}
		return authList;
	}

	private Set<String> getRoles(Set<UserRole> userRoles)
	{
		Set<String> roles = new HashSet<String>();
		for  (UserRole userRole : userRoles)
		{
			roles.add(userRole.getRole().getName());
		}
		return roles;
	}


	@JsonIgnore
	public Set<String> getUserPermission(String roleName) {
		Set<String> userPermissions = null;
		for (UserRole userRole : getUserRole()) {
			if(!userRole.isActiveUserRole()){
				continue;
			}
			
			if(userRole.getRole().getName().equalsIgnoreCase(roleName)) {
				Set<UserRole> requiredUserRole = new HashSet<UserRole>();
				requiredUserRole.add(userRole);

				userPermissions  = getUserPermissions(requiredUserRole);
				break;
			}
		}

		return userPermissions;
	}
	
	@JsonIgnore
	public Set<String> getUserPermissions(){
		Set<String> rolePermissions = new HashSet<String>();

		Set<UserRole> userRoles=getUserRole();
		for (UserRole userRole : userRoles) {
			if(!userRole.isActiveUserRole()){
				continue;
			}
			Role currRole=userRole.getRole();

			Set<RolePermission> userRolePermissions= currRole.getRolePermissions();


			for(RolePermission currUserRolePermissions: userRolePermissions){
				String currPermissionName=currUserRolePermissions.getPermission().getName();
				rolePermissions.add(currPermissionName);
			}
		}
		return rolePermissions;
	}

	@JsonIgnore
	public Set<String> getAllUserPermissions()
	{
		Set<String> rolePermissions = new HashSet<String>();

		Set<UserRole> userRoles=getUserRole();
		for (UserRole userRole : userRoles) {
			if(!userRole.isActiveUserRole()){
				continue;
			}
			Role currRole=userRole.getRole();
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("AccountUser::getAllUserPermission():Evaluating user permissions for role:"+currRole.getName());
			}
			Set<RolePermission> userRolePermissions= currRole.getRolePermissions();


			for(RolePermission currUserRolePermissions: userRolePermissions){
				String currPermissionName=currUserRolePermissions.getPermission().getName();
				rolePermissions.add(currPermissionName);
			}
		}

		return rolePermissions;
	}
	

	private Set<String> getUserPermissions(Set<UserRole> userRoles)
	{
		Set<String> rolePermissions = new HashSet<String>();

		for (UserRole userRole : userRoles) {
			if(!userRole.isActiveUserRole()){
				continue;
			}
			Role currRole=userRole.getRole();
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("AccountUser::getUserPermissions(Set<UserRole>) Evaluating user permissions for role:"+currRole.getName());
			}
			Set<RolePermission> userRolePermissions= currRole.getRolePermissions();


			for(RolePermission currUserRolePermissions: userRolePermissions){
				String currPermissionName=currUserRolePermissions.getPermission().getName();
				rolePermissions.add(currPermissionName);
			}
		}

		return rolePermissions;
	}


	private static Set<GrantedAuthority> getGrantedAuthorities(Set<String> permissions)
	{
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

		for (String permission : permissions) {
			authorities.add(new SimpleGrantedAuthority(permission));
		}

		return authorities;
	}


	@Override
	public Object clone(){
		AccountUser userObj = new AccountUser();

		userObj.setFirstName(getFirstName());
		userObj.setLastName(getLastName());
		userObj.setEmail(getEmail());
		userObj.setUserName(getUserName());

		return userObj;
	}


	@Override
	public String toString() {
		return "AccountUser [id=" + id + ", userName=" + userName + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + "]";
	}

	/**
	 * Method to find the full name of a user
	 * @author polimetla_b, Nikhil Talreja
	 * @param recipient - Whose full name needs to be found out
	 * @return String - The full name of a user
	 */
	@JsonIgnore
	public String getFullName() {


		StringBuffer fullName = new StringBuffer();
		if (getFirstName() != null
				&& getFirstName().trim().length() > 0) {
			fullName.append(getFirstName().trim());
		}

		if (getLastName() != null
				&& getLastName().trim().length() > 0) {
			fullName.append(" ").append(getLastName().trim());
		}
		return fullName.toString();
	}
	
	@JsonIgnore
	public String getAuditLogName() {
		StringBuffer fullName = new StringBuffer();
		if (getFirstName() != null
				&& getFirstName().trim().length() > 0) {
			fullName.append(getFirstName().trim());
		}

		if (getLastName() != null
				&& getLastName().trim().length() > 0) {
			fullName.append(", ").append(getLastName().trim());
		}
		fullName.append("<"+this.userName+">");
		return fullName.toString();
	}

	public String getPasswordRecoveryToken() {
		return passwordRecoveryToken;
	}

	public void setPasswordRecoveryToken(String passwordRecoveryToken) {
		this.passwordRecoveryToken = passwordRecoveryToken;
	}

	public Date getPasswordRecoveryTokenExpiration() {
		return passwordRecoveryTokenExpiration;
	}

	public void setPasswordRecoveryTokenExpiration(
			Date passwordRecoveryTokenExpiration) {
		this.passwordRecoveryTokenExpiration = passwordRecoveryTokenExpiration;
	}

	public String getActiveModuleName() {
		return activeModuleName;
	}

	public void setActiveModuleName(String activeModuleName) {
		this.activeModuleName = activeModuleName;
	}

	public String getSwitchedRoleName() {
		return switchedRoleName;
	}

	public void setSwitchedRoleName(String switchedRoleName) {
		this.switchedRoleName = switchedRoleName;
	}
	
	/**
	 * @return the activeModuleId
	 */
	public int getActiveModuleId() {
		return activeModuleId;
	}

	/**
	 * @param activeModuleId the activeModuleId to set
	 */
	public void setActiveModuleId(int activeModuleId) {
		this.activeModuleId = activeModuleId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getIsSuperUser() {
		return isSuperUser;
	}

	public void setIsSuperUser(Integer isSuperUser) {
		this.isSuperUser = isSuperUser;
	}
	
//	public static void main(String[] args){
//		Gson gson = new Gson();
//		AccountUser user = gson.fromJson("{ \"affiliate\" : null,\r\n  \"aptc\" : null,\r\n  \"countyCode\" : null,\r\n  \"created\" : null,\r\n  \"createdBy\" : null,\r\n  \"csr\" : null,\r\n  \"eligLead\" : { \"affiliateId\" : 0,\r\n      \"apiOutput\" : null,\r\n      \"aptc\" : null,\r\n      \"benefits\" : null,\r\n      \"clickId\" : 0,\r\n      \"countyCode\" : null,\r\n      \"createdBy\" : 0,\r\n      \"creationTimestamp\" : 1381801713115,\r\n      \"csr\" : null,\r\n      \"docVisitFrequency\" : null,\r\n      \"emailAddress\" : null,\r\n      \"errorCode\" : 0,\r\n      \"errorMessage\" : null,\r\n      \"extAffHouseholdId\" : 0,\r\n      \"familySize\" : 0,\r\n      \"flowId\" : 0,\r\n      \"householdIncome\" : 0.0,\r\n      \"id\" : 7153,\r\n      \"isOkToCall\" : \"N\",\r\n      \"lastUpdateTimestamp\" : 1381801713115,\r\n      \"lastUpdatedBy\" : 0,\r\n      \"leadType\" : \"WEB\",\r\n      \"memberData\" : null,\r\n      \"name\" : null,\r\n      \"noOfApplicants\" : 0,\r\n      \"noOfPrescriptions\" : null,\r\n      \"overallApiStatus\" : null,\r\n      \"phoneNumber\" : null,\r\n      \"premium\" : null,\r\n      \"stage\" : null,\r\n      \"status\" : null,\r\n      \"zipCode\" : null\r\n    },\r\n  \"email\" : null,\r\n  \"familySize\" : null,\r\n  \"ffmResponse\" : null,\r\n  \"ffmSBEHouseholdID\" : null,\r\n  \"firstName\" : null,\r\n  \"houseHoldIncome\" : null,\r\n  \"id\" : 0,\r\n  \"lastName\" : null,\r\n  \"lastVisited\" : null,\r\n  \"numberOfApplicants\" : null,\r\n  \"phoneNumber\" : null,\r\n  \"premium\" : null,\r\n  \"stage\" : null,\r\n  \"state\" : null,\r\n  \"status\" : null,\r\n  \"updated\" : null,\r\n  \"updatedBy\" : null,\r\n  \"user\" : { \"accountNonExpired\" : true,\r\n      \"accountNonLocked\" : true,\r\n      \"activeModule\" : null,\r\n      \"activeModuleId\" : 0,\r\n      \"activeModuleName\" : \"individual\",\r\n      \"authorities\" : [ { \"authority\" : \"INDIVIDUAL\" } ],\r\n      \"communicationPref\" : null,\r\n      \"confirmed\" : 1,\r\n      \"created\" : null,\r\n      \"credentialsNonExpired\" : true,\r\n      \"deligationAccessCode\" : null,\r\n      \"email\" : \"ress@mohito.com\",\r\n      \"enabled\" : true,\r\n      \"endDate\" : null,\r\n      \"externPin\" : null,\r\n      \"firstName\" : \"singa\",\r\n      \"fullName\" : \"singa pinga\",\r\n      \"ghixProvisionRoleName\" : null,\r\n      \"hasGhixProvisioned\" : true,\r\n      \"id\" : 3511,\r\n      \"isSuperUser\" : null,\r\n      \"lastName\" : \"pinga\",\r\n      \"password\" : \"password1\",\r\n      \"passwordRecoveryToken\" : null,\r\n      \"passwordRecoveryTokenExpiration\" : null,\r\n      \"phone\" : null,\r\n      \"pin\" : null,\r\n      \"pinexpiration\" : null,\r\n      \"recordId\" : null,\r\n      \"recordType\" : null,\r\n      \"recovery\" : \"1381801684305\",\r\n      \"securityAnswer1\" : \"me\",\r\n      \"securityAnswer2\" : \"de\",\r\n      \"securityAnswer3\" : \"ve\",\r\n      \"securityQuestion1\" : \"What was your childhood nickname?\",\r\n      \"securityQuestion2\" : \"What school did you attend for sixth grade?\",\r\n      \"securityQuestion3\" : \"In what city does your nearest sibling live?\",\r\n      \"startDate\" : null,\r\n      \"status\" : null,\r\n      \"title\" : null,\r\n      \"updated\" : null,\r\n      \"userName\" : \"ress@mohito.com\",\r\n      \"userPermissions\" : [  ],\r\n      \"userRole\" : [ { \"created\" : 1381801686311,\r\n            \"defaultRole\" : true,\r\n            \"id\" : 3522,\r\n            \"role\" : { \"created\" : 1357222064534,\r\n                \"description\" : \"CONSUMER\",\r\n                \"id\" : 20,\r\n                \"landingPage\" : \"/memberportal/home\",\r\n                \"name\" : \"INDIVIDUAL\",\r\n                \"postRegistrationUrl\" : \"/memberportal/registration\",\r\n                \"rolePermissions\" : [  ],\r\n                \"updated\" : 1357222064534\r\n              },\r\n            \"roleFlag\" : \"Y\",\r\n            \"updated\" : 1381801686311\r\n          } ],\r\n      \"username\" : \"ress@mohito.com\"\r\n    },\r\n  \"zipCode\" : null\r\n}", AccountUser.class);
//
//
//	}

	/*
	 * Set the ffmUserId for the brokers/CSRs
	 */
	public void setFfmUserId(String ffmUserId) {
		this.ffmUserId = ffmUserId;
	}

	/*
	 * Get the ffmUserId for the brokers/CSRs
	 */
	public String getFfmUserId() {
		return this.ffmUserId;
	}

	/*
	 * Get the NPN for the user. Usually for Brokers and CSRs
	 */
	public String getUserNPN() {
		return this.userNPN;
	}

	/*
	 * Set the NPN for the user. Usually for Brokers and CSRs
	 */
	public void setUserNPN(String userNPN) {
		this.userNPN = userNPN;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public Role getDefRole() {
		if(this.defRole == null){
			this.getUserRole();
		}
		return defRole;
	}

	public void setDefRole(Role defRole) {
		this.defRole = defRole;
	}

	public int getRoleCount() {
		return this.userRole.size();
	}

	public int getSecQueRetryCount() {
		return secQueRetryCount;
	}

	public void setSecQueRetryCount(int secQueRetryCount) {
		this.secQueRetryCount = secQueRetryCount;
	}

	public Date getPasswordLastUpdatedTimeStamp() {
		return PasswordLastUpdatedTimeStamp;
	}

	public void setPasswordLastUpdatedTimeStamp(Date passwordLastUpdatedTimeStamp) {
		PasswordLastUpdatedTimeStamp = passwordLastUpdatedTimeStamp;
	}

	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
    public boolean isAuthenticated() {
		return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
    }
    
	public boolean isEmailChanged() {
		return isEmailChanged;
	}

	public void setEmailChanged(boolean isEmailChanged) {
		this.isEmailChanged = isEmailChanged;
	}

	public String getCurrentUserRole() {
		return currentUserRole;
	}

	public void setCurrentUserRole(String currentUserRole) {
		this.currentUserRole = currentUserRole;
	}

	public String getSamlPasswdTimeStamp() {
		return samlPasswdTimeStamp;
	}

	public void setSamlPasswdTimeStamp(String samlPasswdTimeStamp) {
		if(samlPasswdTimeStamp != null)
		{
		Date date = TSDate.getNoOffsetTSDate(Long.parseLong(samlPasswdTimeStamp));
		setPasswordLastUpdatedTimeStamp(date);
	}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountUser other = (AccountUser) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
		columnNames.add("firstName");
		columnNames.add("lastName");
		columnNames.add("userName");
		columnNames.add("email");
		columnNames.add("tenantId");
		columnNames.add("updated");
		columnNames.add("status"); 
		return columnNames;
	}

	public void setLoginContext(Map loginContext) {
		this.loginContext= loginContext;
		
	}
	
	public Map<String,String> getLoginConext(){
		return this.loginContext;
	}


}
