package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * This stores CLOB objects for a message
 * 
 * @author Nikhil Talreja
 * @since 11/28/2012
 * 
 */

@Entity
@Table(name="INBOX_MSG_CLOB")
@DynamicInsert
@DynamicUpdate
public class InboxMsgClob implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5329814764481660563L;
	
	@Id
	@Column(name="MSG_ID")
	private long id;
	
	@Column(name="MSG_CLOB")
	private String msgClob;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public String getMsgClob() {
		return msgClob;
	}

	public void setMsgClob(String msgClob) {
		this.msgClob = msgClob;
	}
	
}
