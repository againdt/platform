package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the module_users database table.
 * 
 */
@Entity
@Table(name="module_users",uniqueConstraints=
@UniqueConstraint(columnNames={"module_id", "module_name", "user_id"}))
public class ModuleUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ModuleUser_Seq")
	@SequenceGenerator(name = "ModuleUser_Seq", sequenceName = "module_users_seq", allocationSize = 1)
	private int id;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="CREATION_TIMESTAMP")
	private Date created;

	@Column(name="module_id")
	private int moduleId;

	@Column(name="module_name")
	private String moduleName;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;

	//bi-directional many-to-one association to AccountUser
    @ManyToOne
    @JoinColumn(name="user_id")
    private AccountUser user;

    // HIX-28851: Application error is displayed on switching from Agent to Employer
    @Column(name="Is_Self_Signed")               
	private String isSelfSigned="Y";

    
    public ModuleUser() {
    }

	public ModuleUser(int moduleId, String moduleName, AccountUser user) {
		super();
		this.moduleId = moduleId;
		this.moduleName = moduleName;
		this.user = user;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public int getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public AccountUser getUser() {
		return this.user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	
	/**
	 * HIX-28851: Application error is displayed on switching from Agent to Employer
	 * @return the isSelfSigned
	 */
	public String getIsSelfSigned() {
		return isSelfSigned;
	}

	/**
	 * HIX-28851: Application error is displayed on switching from Agent to Employer
	 * @param isSelfSigned the isSelfSigned to set
	 */
	public void setIsSelfSigned(String isSelfSigned) {
		this.isSelfSigned = isSelfSigned;
	}

	/**
	 * HIX-28851: Application error is displayed on switching from Agent to Employer
	 * @return boolean true if isSelfSigned=="Y"
	 */

	@JsonIgnore
	public boolean isSelfSignedModule() {
		return (this.isSelfSigned.equals("Y")) ? true : false ;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
