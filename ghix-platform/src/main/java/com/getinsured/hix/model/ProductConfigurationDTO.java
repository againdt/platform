package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class ProductConfigurationDTO implements Serializable {
	private static final String SELECTED = "selected";
	private static final String PRODUCT_CODE = "code";
	private static final String DISPLAY_LABEL = "displayLabel";
	private static final String NEW_WINDOW = "newWindow";
	public static final List<Map<String, Object>> PRODUCTS = new ArrayList<>();
	
	public enum ProductCodeEnum {
		HLT("Health"), DEN("Dental"), AME("AME"), STM("STM"), VSN("Vision"), MDR("Medicare");
		
		private String label;
		
		ProductCodeEnum(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return this.label;
		}
	}
	static {
		PRODUCTS.add(createProductConfigEntry(ProductCodeEnum.HLT));
		PRODUCTS.add(createProductConfigEntry(ProductCodeEnum.DEN));
		PRODUCTS.add(createProductConfigEntry(ProductCodeEnum.AME));
		PRODUCTS.add(createProductConfigEntry(ProductCodeEnum.STM));
		PRODUCTS.add(createProductConfigEntry(ProductCodeEnum.VSN));
		PRODUCTS.add(createProductConfigEntry(ProductCodeEnum.MDR));
	}

	private static HashMap<String, Object> createProductConfigEntry(final ProductCodeEnum productCode) {
		return new HashMap<String, Object>() {{ put(PRODUCT_CODE, productCode.name()); put(DISPLAY_LABEL, productCode.getLabel()); put(SELECTED, Boolean.FALSE); put(NEW_WINDOW, Boolean.FALSE); }};
	}
	
	private String code;
	private String label;
	private String linkOutUrl;
	private Boolean selected;
	private Boolean newWindow;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLinkOutUrl() {
		return linkOutUrl;
	}

	public void setLinkOutUrl(String linkOutUrl) {
		this.linkOutUrl = linkOutUrl;
	}

	public String getLabel() {
		return label;
	}
	
	public String displayLabel() {
		return ProductCodeEnum.valueOf(code).getLabel();
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Boolean getNewWindow() {
		return newWindow;
	}

	public void setNewWindow(Boolean newWindow) {
		this.newWindow = newWindow;
	}
}
