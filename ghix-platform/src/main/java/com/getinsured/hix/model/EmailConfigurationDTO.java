package com.getinsured.hix.model;

import java.io.Serializable;

public class EmailConfigurationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1806763989828140385L;
	private String fromAddress;
	private String footerAddress;
	private String emailConfiguration;

	private String disclaimerContent;
	
	public String getFooterAddress() {
		return footerAddress;
	}

	public void setFooterAddress(String footerAddress) {
		this.footerAddress = footerAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getEmailConfiguration() {
		return emailConfiguration;
	}

	public void setEmailConfiguration(String emailConfiguration) {
		this.emailConfiguration = emailConfiguration;
	}

	public String getDisclaimerContent() {
		return disclaimerContent;
	}

	public void setDisclaimerContent(String disclaimerContent) {
		this.disclaimerContent = disclaimerContent;
	}
	
}
