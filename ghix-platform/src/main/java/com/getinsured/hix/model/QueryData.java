package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="query_data")
public class QueryData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUERY_DATA_Seq")
	@SequenceGenerator(name = "QUERY_DATA_Seq", sequenceName = "QUERY_DATA_Seq", allocationSize = 1)
	private Integer id;
	
	@Column(name="NAME")
	private String name;
	
	@Lob
	@Column(name="TAG_QUERY")
	private String tagQuery;
	
	@Lob
	@Column(name="SUBSCRIBER_QUERY")
	private String subscriberQuery;
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name="UPDATED_BY")
	private Integer updatedBy;
	
	/** Created Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date created;

	/** Updated Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTagQuery() {
		return tagQuery;
	}

	public void setTagQuery(String tagQuery) {
		this.tagQuery = tagQuery;
	}

	public String getSubscriberQuery() {
		return subscriberQuery;
	}

	public void setSubscriberQuery(String subscriberQuery) {
		this.subscriberQuery = subscriberQuery;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
		
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}
	
	

}
