package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author vasani_s
 * @since 31/03/2015 
 * 
 */

@Entity
@Table(name="LOOKUP_LOCALE")
public class LookupLocale implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOOKUP_LOCALE_SEQ")
	@SequenceGenerator(name = "LOOKUP_LOCALE_SEQ", sequenceName = "LOOKUP_LOCALE_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="LOCALE_CODE")
	private String localeCode ;
	
	@Column(name="LOCALE_LANGUAGE")
	private String localeLanguage ;

	/*@JsonManagedReference
	@OneToMany(mappedBy = "lookupLocale", cascade = CascadeType.ALL)
	 private List<LookupValue> lookupValue;

	@JsonManagedReference
	public List<LookupValue> getLookupValue() {
		return lookupValue;
	}

	@JsonManagedReference
	public void setLookupValue(List<LookupValue> lookupValue) {
		this.lookupValue = lookupValue;
	}*/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	public String getLocaleLanguage() {
		return localeLanguage;
	}

	public void setLocaleLanguage(String localeLanguage) {
		this.localeLanguage = localeLanguage;
	}

	

	
}
