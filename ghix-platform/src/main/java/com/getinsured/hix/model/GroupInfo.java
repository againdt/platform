package com.getinsured.hix.model;

import java.util.Date;

public class GroupInfo {
	private int id;
	private String groupLeadFirstName;
	private String groupLeadLastName;
	private String groupName;
	private int groupLeadUserId;
	private int groupSize;
	private Date created;
	private Date updated;
	private Date startDate;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getGroupLeadUserId() {
		return groupLeadUserId;
	}

	public void setGroupLeadUserId(int groupLeadUserId) {
		this.groupLeadUserId = groupLeadUserId;
	}

	public String getGroupLeadFirstName() {
		return groupLeadFirstName;
	}

	public void setGroupLeadFirstName(String groupLeadFirstName) {
		this.groupLeadFirstName = groupLeadFirstName;
	}

	public String getGroupLeadLastName() {
		return groupLeadLastName;
	}

	public void setGroupLeadLastName(String groupLeadLastName) {
		this.groupLeadLastName = groupLeadLastName;
	}

	public String getGroupLeadFullName() {

		StringBuffer fullName = new StringBuffer();
		if (getGroupLeadFirstName() != null
				&& getGroupLeadFirstName().trim().length() > 0) {
			fullName.append(getGroupLeadFirstName().trim());
		}

		if (getGroupLeadLastName() != null
				&& getGroupLeadLastName().trim().length() > 0) {
			fullName.append(" ").append(getGroupLeadLastName().trim());
		}
		return fullName.toString();
	}

	public int getGroupSize() {
		return groupSize;
	}

	public void setGroupSize(int groupSize) {
		this.groupSize = groupSize;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	

}
