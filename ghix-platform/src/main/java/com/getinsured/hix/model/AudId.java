package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * 
 * @author meher_a
 *
 */
public class AudId implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	
	private int rev;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof AudId))
		{
			return false;
		}

		final AudId audId = (AudId) o;

		if (id != audId.id)
		{
			return false;
		}
		return rev == audId.rev;

	}

	@Override
	public int hashCode()
	{
		int result = id;
		result = 31 * result + rev;
		return result;
	}
}
