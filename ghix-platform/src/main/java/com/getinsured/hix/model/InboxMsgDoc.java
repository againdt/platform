package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.platform.util.GhixAESCipherPool;

/**
 * This represents one attachment for given message ID.
 * This holds meta data of given file. File is available in Alfresco.
 * 
 * @author polimetla_b, talreja_n
 * @since 11/16/2012
 *
 */

@Entity
@Table(name="INBOX_MSG_DOC")
public class InboxMsgDoc implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6383303727200461605L;
	
	public enum TYPE{
		ASCII, BINARY;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INBOX_MSG_DOC_SEQ")
	@SequenceGenerator(name = "INBOX_MSG_DOC_SEQ", sequenceName = "INBOX_MSG_DOC_SEQ", allocationSize = 1)
	private long id;
	
	//Many to one join with message
	@ManyToOne(cascade = {CascadeType.ALL} )
	@JoinColumn(name = "MSG_ID")
	private InboxMsg message;
	
	@Column(name="DOCUMENT_ID")
	private String docId;
	
	@Column(name="DOCUMENT_NAME")
	private String docName;
	
	@Column(name="DOCUMENT_TYPE")
	@Enumerated(EnumType.STRING)
	private TYPE docType;
	
	@Column(name="DOCUMENT_SIZE")
	private long docSize;
	
	@Column(name="DOCUMENT_DESC")
	private String docDesc;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date createdOn;
	
	public InboxMsgDoc(){
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public InboxMsg getMessage() {
		return message;
	}

	public void setMessage(InboxMsg message) {
		this.message = message;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public TYPE getDocType() {
		return docType;
	}

	public void setDocType(TYPE docType) {
		this.docType = docType;
	}

	public long getDocSize() {
		return docSize;
	}

	public void setDocSize(long docSize) {
		this.docSize = docSize;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public String getSecureReference(String viewType) {
		HashMap<String,String> refMap = new HashMap<>(3);
		refMap.put("documentId".intern(), this.getDocId());
		refMap.put("documentName".intern(), this.getDocName());
		refMap.put("viewType".intern(), viewType);
		refMap.put("msgId", Long.toString(this.getMessage().getId()));
		return GhixAESCipherPool.encryptParameterMap(refMap);
		
	}
	
}
