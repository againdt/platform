package com.getinsured.hix.model;

import java.util.ArrayList;
import java.util.List;

public class InboxMsgResponse extends GHIXResponse{

	private List<InboxMsg> messageList;
	
	public InboxMsgResponse(){
		messageList = new ArrayList<InboxMsg>();
	}
	
	public List<InboxMsg> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<InboxMsg> messageList) {
		this.messageList = messageList;
	}
	
	
	
}
