/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author panda_p
 *
 */
@Entity
@Table(name="comment_targets")
public class CommentTarget implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum TargetName {
		LCE,BROKER,ISSUER,EMPLOYER,PLAN,PLAN_HEALTH,PLAN_DENTAL,DESIGNATEBROKER,ADMIN_HOME,TICKET,DESIGNATEASSISTER,EMPLOYEE,CONSUMER,CONSUMERCALLLOG,SSAP_APPLICATIONS,ENROLLMENT,Death,MinimumEssentialCoverage,NonESIMinimumEssentialCoverage,LegalPresence,IncarcerationStatus,Income,SocialSecurityNumber,Residency,Citizenship,AmericanIndian_AlaskaNativeStatus,ENROLLMENT_OVERRIDE,APP_EVENT,QLE_ADMIN_OVERRIDE,AGENCY,CAP_APPOINTMENTS,AGENCY_ASSISTANT;//,AmericanIndian/AlaskaNativeStatus;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_targets_Seq")
	@SequenceGenerator(name = "comment_targets_Seq", sequenceName = "comment_targets_seq", allocationSize = 1)
	private Integer id;
	
	
	@Column(name = "target_id")
	private Long targetId;
	
	@Column(name = "target_name")
	@Enumerated(EnumType.STRING)
	private TargetName targetName;
	
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp")
	private Date created;
	
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date updated;
    
    
    // bi-directional many-to-one association to EmployerDetails
 	@OneToMany(mappedBy = "commentTarget", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
 	private List<Comment> comments;
	
	/**
	 * 
	 */
	public CommentTarget() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}


	public TargetName getTargetName() {
		return targetName;
	}




	public void setTargetName(TargetName targetName) {
		this.targetName = targetName;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}


	public Long getTargetId() {
		return targetId;
	}
	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
