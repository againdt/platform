package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Entity
@Table(name="TENANT_ROLES")
public class TenantRole {
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="tenant_role_seq")
	@SequenceGenerator(sequenceName="TENANT_ROLES_SEQ", name = "tenant_role_seq")
	private long id;
	
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private Role role;
	
	@ManyToOne
	@JoinColumn(name="TENANT_ID")
	private Tenant tenant;
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	@Column(name="IS_ACTIVE")
	@Enumerated(EnumType.STRING)
	private ActiveFlag isActive;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATED_BY")
	private AccountUser createdBy;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser lastUpdatedBy;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public ActiveFlag getIsActive() {
		return isActive;
	}

	public void setIsActive(ActiveFlag isActive) {
		this.isActive = isActive;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(AccountUser lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	
	
}
