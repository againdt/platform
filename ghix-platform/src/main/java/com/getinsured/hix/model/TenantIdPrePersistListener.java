package com.getinsured.hix.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

public class TenantIdPrePersistListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TenantIdPrePersistListener.class);
	
	@PrePersist
    private void setTenantId(Object object) {
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        try {
				Method setTenantMethod = object.getClass().getMethod("setTenantId", Long.class);
				setTenantMethod.invoke(object, TenantContextHolder.getTenant().getId());
			} catch (NoSuchMethodException | SecurityException e) {
				LOGGER.error("No method named setTenantId defined on " + object.getClass().getName(), e);
			} catch (IllegalAccessException |IllegalArgumentException | InvocationTargetException e) {
				LOGGER.error("Error invoking setTenantId defined on " + object.getClass().getName(), e);
			}
		}
    }
}
