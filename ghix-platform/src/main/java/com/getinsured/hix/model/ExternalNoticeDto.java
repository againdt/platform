package com.getinsured.hix.model;

import java.io.Serializable;

public class ExternalNoticeDto implements Serializable, Cloneable {

	public enum DocumentCategory {
		  QHP_ENROLLMENT_NOTICE("QHP Enrollment Notice"),
		  TAX_FORMS("Tax Forms");
		 
		private String description;
		
		private DocumentCategory(String description)
		{
			this.description=description;
		}
		public String getDescription(){
			return description;
		}
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String noticeId;
	private String documentName;
	private String transactionId;
	private String transactionTimestamp;
	private String document;
	private String documentCategory=DocumentCategory.QHP_ENROLLMENT_NOTICE.getDescription();
	public String getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}
	
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}
	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}
	
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getDocumentCategory() {
		return documentCategory;
	}
	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}
		

}
