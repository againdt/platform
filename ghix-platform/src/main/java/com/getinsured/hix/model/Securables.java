/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable; 
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;



/**
 * @author ravi v
 *
 */

/*
 desc SECURABLES
Name                  Null     Type           
--------------------- -------- -------------- 
ID                    NOT NULL NUMBER         
SECURABLE_TARGET_ID   NOT NULL NUMBER         
USER_NAME                      VARCHAR2(50)   
PASSWORD                       VARCHAR2(256)  
URI                            VARCHAR2(1024) 
APPLICATION_DATA               VARCHAR2(4000) 
CREATION_TIMESTAMP             TIMESTAMP(6)   
CREATED_BY                     NUMBER         
LAST_UPDATE_TIMESTAMP          TIMESTAMP(6)   
LAST_UPDATED_BY                NUMBER      

 * 
 */
@Entity
@Table(name="securables")
@TypeDef(name="ghixJasyptEncrytorUtilString", typeClass= GhixJasyptEncrytorUtilStringType.class)
public class Securables implements Serializable,Comparable<Securables> {
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SECURABLES_SEQ")
	@SequenceGenerator(name = "SECURABLES_SEQ", sequenceName = "SECURABLES_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name = "USER_NAME")
	private String userName;

	@Type(type = "ghixJasyptEncrytorUtilString")
	@Column(name = "PASSWORD")
	 private String password;

	@Column(name = "URI")
	private String uri;

	@Column(name = "APPLICATION_DATA")
	private String applicationData;

	
 	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH} )
	@JoinColumn(name = "securable_target_id")
	private SecurableTarget securableTarget;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp")
	private Date created;
	
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date updated;
    
	//Column to store CREATED_BY 
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	//Column to store LAST_UPDATED_BY 
	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	/**
	 * 
	 */
	public Securables() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	 
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}


	 

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getApplicationData() {
		return applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public SecurableTarget getSecurableTarget() {
		return securableTarget;
	}

	public void setSecurableTarget(SecurableTarget securableTarget) {
		this.securableTarget = securableTarget;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}

	@Override
	public int compareTo(Securables o) {
		return o.created.compareTo(created);
	}

}
