/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Biswakesh
 *
 */
@Entity
@Table(name="user_tokens")
public class UserTokens implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserTokens_Seq")
	@SequenceGenerator(name = "UserTokens_Seq", sequenceName = "user_tokens_seq", allocationSize = 1)
	@Column(name="id")
	private long Id;
	
	@Column(name="source_name")
	private String sourceName;
	
	@Column(name="event_name")
	private String eventName;
	
	@Column(name="token")
	private String token;
	
	@Column(name="token_creation_time")
	private Timestamp tokenCreationTime;
	
	@Column(name="token_expiry")
	private Timestamp tokenExpiry;
	
	@Column(name="user_identifier")
	private String userIdentifier;
	
	@Column(name="status")
	private char status;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Timestamp getTokenCreationTime() {
		return tokenCreationTime;
	}

	public void setTokenCreationTime(Timestamp tokenCreationTime) {
		this.tokenCreationTime = tokenCreationTime;
	}

	public Timestamp getTokenExpiry() {
		return tokenExpiry;
	}

	public void setTokenExpiry(Timestamp tokenExpiry) {
		this.tokenExpiry = tokenExpiry;
	}

	public String getUserIdentifier() {
		return userIdentifier;
	}

	public void setUserIdentifier(String userIdentifier) {
		this.userIdentifier = userIdentifier;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result
				+ ((sourceName == null) ? 0 : sourceName.hashCode());
		result = prime * result + status;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime
				* result
				+ ((tokenCreationTime == null) ? 0 : tokenCreationTime
						.hashCode());
		result = prime * result
				+ ((tokenExpiry == null) ? 0 : tokenExpiry.hashCode());
		result = prime * result
				+ ((userIdentifier == null) ? 0 : userIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserTokens other = (UserTokens) obj;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
		if (sourceName == null) {
			if (other.sourceName != null)
				return false;
		} else if (!sourceName.equals(other.sourceName))
			return false;
		if (status != other.status)
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (tokenCreationTime == null) {
			if (other.tokenCreationTime != null)
				return false;
		} else if (!tokenCreationTime.equals(other.tokenCreationTime))
			return false;
		if (tokenExpiry == null) {
			if (other.tokenExpiry != null)
				return false;
		} else if (!tokenExpiry.equals(other.tokenExpiry))
			return false;
		if (userIdentifier == null) {
			if (other.userIdentifier != null)
				return false;
		} else if (!userIdentifier.equals(other.userIdentifier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserTokens [Id=" + Id + ", sourceName=" + sourceName
				+ ", eventName=" + eventName + ", token=" + token
				+ ", tokenCreationTime=" + tokenCreationTime + ", tokenExpiry="
				+ tokenExpiry + ", userIdentifier=" + userIdentifier
				+ ", status=" + status + "]";
	}
	
}
