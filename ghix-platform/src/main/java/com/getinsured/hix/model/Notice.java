package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the roles database table.
 *
 */
@Entity
@Table(name="notices")
public class Notice implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum STATUS 	{ EMAIL_SENT, PDF_GENERATED, PRINTED, FAILED, PENDING; }

	public enum KEY_NAME {ACCOUNT_ACTIVATION,PROXY_ENROLLMENT_FAIL}
	
	public enum PrintableFlag{ Y,N }
	
	public enum IsPrinted{Y, N}
	
	public enum NOTICESEQUENCE {notices_seq}
	
	@Id
	private int id;

	@ManyToOne
	@JoinColumn(name="notice_type_id")
	private NoticeType noticeType;

	@Column(name="email_address")
	private String emailIdentity;

	@Column(name="key_id")
	private int keyId;

	@Column(name="key_name")
	private String keyName;

	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private STATUS status;

	@Column(name="subject")
	private String subject;

	@Column(name="from_email_address")
	private String fromAddress;

	@Column(name="to_address")
	private String toAddress;

	@Column(name="cc_email_address")
	private String ccAddress;

	@Column(name="bcc_address")
	private String bccAddress;

	@Column(name="ret_address")
	private String retAddress;

	@Column(name="email_body")
	private String emailBody;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="viewed_date")
	private Date viewedDate;


	@Column(name="sent_date")
	private Date sentDate;

	@Column(name="click_count")
	private Integer clickCount;

	@Column(name="attachment")
	private String attachment;

	@Column(name="reply_to")
	private String replyTo;

	@Column(name="unsubscribed")
	private String unsubscribed;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date updated;

	@ManyToOne
	@JoinColumn(name="user_id")
	private AccountUser user;

	@Column(name="ECM_ID")
	private String ecmId;
	
	@Column(name="PRINTABLE")
	private String printable;
	
	@Column(name="IS_PRINTED", length=1)
	@Enumerated(EnumType.STRING)
	private IsPrinted isPrinted;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public NoticeType getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(NoticeType noticeType) {
		this.noticeType = noticeType;
	}

	public String getEmailIdentity() {
		return emailIdentity;
	}

	public void setEmailIdentity(String emailIdentity) {
		this.emailIdentity = emailIdentity;
	}

	public int getKeyId() {
		return keyId;
	}

	public void setKeyId(int keyId) {
		this.keyId = keyId;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}



	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getCcAddress() {
		return ccAddress;
	}

	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}

	public String getBccAddress() {
		return bccAddress;
	}

	public void setBccAddress(String bccAddress) {
		this.bccAddress = bccAddress;
	}

	public String getRetAddress() {
		return retAddress;
	}

	public void setRetAddress(String retAddress) {
		this.retAddress = retAddress;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}


	public Date getViewedDate() {
		return viewedDate;
	}

	public void setViewedDate(Date viewedDate) {
		this.viewedDate = viewedDate;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Integer getClickCount() {
		return clickCount;
	}

	public void setClickCount(Integer clickCount) {
		this.clickCount = clickCount;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public String getUnsubscribed() {
		return unsubscribed;
	}

	public void setUnsubscribed(String unsubscribed) {
		this.unsubscribed = unsubscribed;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public AccountUser getUser()
	{
		return user;
	}

	public void setUser(AccountUser user)
	{
		this.user = user;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public String getEcmId() {
		return ecmId;
	}

	public void setEcmId(String ecmId) {
		this.ecmId = ecmId;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getPrintable() {
		return printable;
	}

	public void setPrintable(String printable) {
		this.printable = printable;
	}

	/**
	 * @return the isPrinted
	 */
	public IsPrinted getIsPrinted() {
		return isPrinted;
	}

	/**
	 * @param isPrinted the isPrinted to set
	 */
	public void setIsPrinted(IsPrinted isPrinted) {
		this.isPrinted = isPrinted;
	}
}
