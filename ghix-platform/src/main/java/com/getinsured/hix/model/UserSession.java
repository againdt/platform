package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="user_session")
public class UserSession implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserSession_Seq")
	@SequenceGenerator(name = "UserSession_Seq", sequenceName = "user_session_seq", allocationSize = 1)
	@Column(name="ID")
	private long Id;
	
	@Column(name="SESSION_ID")
	private String sessionId;
	
	@Column(name="SESSION_VAL")
	private String sessionVal;
	
	@Column(name="LOGIN_TIME")
	private Timestamp loginTime;
	
	@Column(name="LOGOUT_TIME")
	private Timestamp logoutTime;
	
	@Column(name="NODE_ID")
	private String nodeId;
	
	@Column(name="IP_ADDRESS")
	private String browserIp;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="USER_ID")
	private AccountUser user;
	
	@Column(name="STATUS")
	private String status; // Possible values are "CLOSED", "CLOSED-CLEANUP", "CURRENT"

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSessionVal() {
		return sessionVal;
	}

	public void setSessionVal(String sessionVal) {
		this.sessionVal = sessionVal;
	}

	public Timestamp getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	public Timestamp getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Timestamp logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getBrowserIp() {
		return browserIp;
	}

	public void setBrowserIp(String browserIp) {
		this.browserIp = browserIp;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((browserIp == null) ? 0 : browserIp.hashCode());
		result = prime * result
				+ ((loginTime == null) ? 0 : loginTime.hashCode());
		result = prime * result
				+ ((logoutTime == null) ? 0 : logoutTime.hashCode());
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		result = prime * result
				+ ((sessionVal == null) ? 0 : sessionVal.hashCode());
		if(user != null) {
			result = prime * result + (int) (user.getId() ^ (user.getId() >>> 32));
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserSession other = (UserSession) obj;
		if (browserIp == null) {
			if (other.browserIp != null)
				return false;
		} else if (!browserIp.equals(other.browserIp))
			return false;
		if (loginTime == null) {
			if (other.loginTime != null)
				return false;
		} else if (!loginTime.equals(other.loginTime))
			return false;
		if (logoutTime == null) {
			if (other.logoutTime != null)
				return false;
		} else if (!logoutTime.equals(other.logoutTime))
			return false;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		if (sessionVal == null) {
			if (other.sessionVal != null)
				return false;
		} else if (!sessionVal.equals(other.sessionVal))
			return false;
		if (user != null && (user.getId() != other.getId()))
			return false;
		return true;
	}
	
	@PrePersist
	public void setLoginTime(){
		Timestamp timestamp = new TSTimestamp(System.currentTimeMillis());
		this.setLoginTime(timestamp);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
