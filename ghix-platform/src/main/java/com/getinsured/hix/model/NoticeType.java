package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;


/**
 * The persistent class for the notice_types database table.
 *
 */
@Audited
@Entity
@Table(name="notice_types")
public class NoticeType implements Serializable, SortableEntity {
	private static final long serialVersionUID = 1L;

	public enum NoticeTypeBooleanFlag { YES, NO;}
	
	public enum ExternalSendEmail{NONE, REMOTE, NATIVE,NATIVE_REMOTE}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NoticeType_Seq")
	@SequenceGenerator(name = "NoticeType_Seq", sequenceName = "notice_types_seq", allocationSize = 1)
	private Integer id;

	/** Notification Name */
	@Column(name="NAME")
	private String notificationName;

	/** Notification Language */
	@Column(name="GI_LANGUAGE")
	@Enumerated(EnumType.STRING)
	private GhixLanguage language;

	/** User Type - {GhixNoticeUser} */
	@Column(name="USER_TYPE")
	@Enumerated(EnumType.STRING)
	private GhixNoticeUser user;

	/** Notice Type - {PDF or Email} */
	@Column(name="GI_NOTICE_TYPE")
	@Enumerated(EnumType.STRING)
	private GhixNotificationType type;	// change to ghixnoticetype enum

	/** Method - {Email or Mail} */
	@Column(name="NOTICE_COMMUNICATION_METHOD")
	@Enumerated(EnumType.STRING)
	private GhixNoticeCommunicationMethod method;	

	/** Accenture Notice Id applicable only for PDF TYPE for Accenture; if this is present then templateLocation should not be set */
	@Column(name="EXTERNAL_ID")
	private String externalId;

	/** Physical location in the source control where template is checked-in by DEV/UI Team; if this is present then externalId should not be set */
	@Column(name="TEMPLATE_LOCATION")
	private String templateLocation;

	/** Email Subject applicable only for EMAIL TYPE */
	@Column(name="EMAIL_SUBJECT")
	private String emailSubject;

	/** Email From Address applicable only for EMAIL TYPE */
	@Column(name="EMAIL_FROM")
	private String emailFrom;
	
	/** Email To Address applicable only for EMAIL TYPE */
	@Column(name="EMAIL_TO")
	private String emailTo;

	/** Email Class applicable only for EMAIL TYPE; required to support backward compatibility with existing NotificationAgent code for sending email */
	@Column(name="EMAIL_CLASS")
	private String emailClass;

	/** Date from which notification can be used */
	@Temporal(value = TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE",nullable=false)
	private Date effectiveDate;

	/** Date after which notification cannot be used */
	@Temporal(value = TemporalType.DATE)
	@Column(name="TERMINATION_DATE")
	private Date terminationDate;

	/** Created Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date created;

	/** Updated Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date updated;
	
	@Column(name="EDITABLE")
	private String templateEditable;

	@Column(name = "last_updated_by", length = 10)
	private Integer lastUpdatedBy;
	
	@Column(name="EXTERNAL_SEND", length=1)
	@Enumerated(EnumType.STRING)
	private ExternalSendEmail externalSend;

	@Column(name="QUEUED_EMAIL_CLASS")
	private String queuedEmailClass;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public GhixLanguage getLanguage() {
		return language;
	}

	public void setLanguage(GhixLanguage language) {
		this.language = language;
	}

	public GhixNoticeUser getUser() {
		return user;
	}

	public void setUser(GhixNoticeUser user) {
		this.user = user;
	}

	public GhixNotificationType getType() {
		return type;
	}

	public void setType(GhixNotificationType type) {
		this.type = type;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getTemplateLocation() {
		return templateLocation;
	}

	public void setTemplateLocation(String templateLocation) {
		this.templateLocation = templateLocation;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	
	
	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailClass() {
		return emailClass;
	}

	public void setEmailClass(String emailClass) {
		this.emailClass = emailClass;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public String getTemplateEditable() {
		return templateEditable;
	}

	public void setTemplateEditable(String templateEditable) {
		this.templateEditable = templateEditable;
	}


	// To AutoUpdate created and updated dates while persisting object
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public GhixNoticeCommunicationMethod getMethod() {
		return method;
	}

	public void setMethod(GhixNoticeCommunicationMethod method) {
		this.method = method;
	}
	
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}


	public ExternalSendEmail getExternalSend() {
		return externalSend;
	}

	public void setExternalSend(ExternalSendEmail externalSend) {
		this.externalSend = externalSend;
	}

	public String getQueuedEmailClass() {
		return queuedEmailClass;
	}

	public void setQueuedEmailClass(String queuedEmailClass) {
		this.queuedEmailClass = queuedEmailClass;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
		columnNames.add("emailTo");
		columnNames.add("emailFrom");
		columnNames.add("emailSubject");
		columnNames.add("notificationName");
		columnNames.add("created");
		columnNames.add("updated");
		return columnNames;
	}
}
