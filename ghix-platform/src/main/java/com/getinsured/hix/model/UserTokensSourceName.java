package com.getinsured.hix.model;

public enum UserTokensSourceName {
	
	ACC_MGMT("ACC_MGMT");
	
	private String val;
	
	UserTokensSourceName(String val) {
		this.val = val;
	}
	
	public String toString(){
		return this.val;
	}

}
