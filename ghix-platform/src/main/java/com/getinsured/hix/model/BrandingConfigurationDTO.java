package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.getinsured.hix.platform.config.PropertiesEnumMarker;

public class BrandingConfigurationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6162056390814353493L;

	public enum BrandingConfigurationEnum implements PropertiesEnumMarker {
		THEME("Theme"), LOGO("Logo"), BRANDING_DATA("Branding data"), FAVICON("Favicon"), HERO_IMAGE("Hero Image");

		private final String value;

		@Override
		public String getValue() {
			return this.value;
		}

		BrandingConfigurationEnum(String value) {
			this.value = value;
		}
	};
	
	public static final List<BrandingConfigurationDTO.BrandingConfigurationEnum> configurationList = Arrays.asList(BrandingConfigurationEnum.THEME, 
			BrandingConfigurationEnum.LOGO,
			BrandingConfigurationEnum.BRANDING_DATA,
			BrandingConfigurationEnum.FAVICON,
			BrandingConfigurationEnum.HERO_IMAGE);
	
	private String logoUrl;
	
	private String faviconUrl;
	
	private String themeUrl;
	
	private String brandingDataUrl;
	
	private String heroImageUrl;
	
	private String logoRedirectUrl;
	
	private String landingPageCategory;

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getFaviconUrl() {
		return faviconUrl;
	}

	public void setFaviconUrl(String faviconUrl) {
		this.faviconUrl = faviconUrl;
	}

	public String getThemeUrl() {
		return themeUrl;
	}

	public void setThemeUrl(String themeUrl) {
		this.themeUrl = themeUrl;
	}

	public String getBrandingDataUrl() {
		return brandingDataUrl;
	}

	public void setBrandingDataUrl(String brandingDataUrl) {
		this.brandingDataUrl = brandingDataUrl;
	}

	public String getHeroImageUrl() {
		return heroImageUrl;
	}

	public void setHeroImageUrl(String heroImageUrl) {
		this.heroImageUrl = heroImageUrl;
	}

	public String getLogoRedirectUrl() {
		return logoRedirectUrl;
	}

	public void setLogoRedirectUrl(String logoRedirectUrl) {
		this.logoRedirectUrl = logoRedirectUrl;
	}

	public String getLandingPageCategory() {
		return landingPageCategory;
	}

	public void setLandingPageCategory(String landingPageCategory) {
		this.landingPageCategory = landingPageCategory;
	}
	
}
