/**
 * 
 */
package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.getinsured.hix.platform.security.GhixDBSafeHtml;



/**
 * @author panda_p
 *
 */
@Entity
@Table(name="comments")
@TypeDef(name = "ghixDBSafeHtml", typeClass = GhixDBSafeHtml.class)
public class Comment implements Comparable<Comment> {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comments_Seq")
	@SequenceGenerator(name = "comments_Seq", sequenceName = "comments_seq", allocationSize = 1)
	private Integer id;
	
	
	//Column to store commenter's id
	@Column(name = "commented_by")
	private Integer commentedBy;
	
	//Column to store commenter's name
	@Column(name = "commenter_name")
	private String commenterName;
	
	@Column(name = "comment_text")
	@Type(type = "ghixDBSafeHtml")
	private String comment;
		
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH} )
	@JoinColumn(name = "comment_target_id")
	private CommentTarget commentTarget;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp")
	private Date created;
	
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date updated;
	
	/**
	 * 
	 */
	public Comment() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getCommentedBy(){
		return commentedBy;
	}
	
	public String getCommenterName() {
		return commenterName;
	}

	public void setCommenterName(String commenterName) {
		this.commenterName = commenterName;
	}

	public void setComentedBy(Integer commentedBy) {
		this.commentedBy = commentedBy;
	}
	
	

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}


	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
	
	public CommentTarget getCommentTarget() {
		return commentTarget;
	}

	public void setCommentTarget(CommentTarget commentTarget) {
		this.commentTarget = commentTarget;
	}

	

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}

	@Override
	public int compareTo(Comment o) {
		return o.created.compareTo(created);
	}

}
