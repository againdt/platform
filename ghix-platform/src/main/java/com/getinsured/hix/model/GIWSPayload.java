package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="GI_WS_PAYLOAD")
public class GIWSPayload {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GIWSPAYLOAD_SEQ")
	@SequenceGenerator(name = "GIWSPAYLOAD_SEQ", sequenceName = "GI_WS_PAYLOAD_SEQ", allocationSize = 1)
	@Column(name = "GI_WS_PAYLOAD_ID")
	private Integer id;
	
	
	@Column(name="REQUEST_PAYLOAD")
    private String requestPayload;
	
	@Column(name="RESPONSE_PAYLOAD")
    private String responsePayload;
	

	@Column(name="RESPONSE_CODE")
    private String responseCode;
	
	
	@Column(name="STATUS")
    private String status;
	
	@Column(name = "CORRELATION_ID")
	private String correlationId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_TIMESTAMP")
    private Date createdTimestamp;

	@Column(name = "ENDPOINT_URL")
	private String endpointUrl;

	@Column(name = "ENDPOINT_FUNCTION")
	private String endpointFunction;
	
	@Column(name = "ENDPOINT_OPERATION_NAME")
	private String endpointOperationName;
	
	@Column(name="EXCEPTION_MESSAGE")
    private String exceptionMessage;
	
	@Column(name="CREATED_USER_ID")
	private Integer createdUserId;
	
	@Column(name="ACCESS_IP")
	private String accessIp;
	
	@Column(name="CLIENT_NODE_IP")
	private String clientNodeIp;
	
	@Column(name="RETRY_COUNT")
	private long retryCount;
	
	@Column(name="SSAP_APPLICANT_ID")
	private Long ssapApplicantId;

	@Column(name="SSAP_APPLICATION_ID")
	private Long ssapApplicationId;
	
	@Column(name="VERSION")
	private String version;
	
	@Column(name = "CUSTOM_KEY_ID1")
	private String customKeyId1;
		
	@Column(name = "CUSTOM_KEY_VALUE1")
	private String customKeyValue1;

	@Column(name = "CUSTOM_KEY_ID2")
	private String customKeyId2;
	
	@Column(name = "CUSTOM_KEY_VALUE2")
	private String customKeyValue2;
	
	@Column(name = "GLOBAL_ID")
	private String globalId;
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId;
	
	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getRequestPayload() {
		return requestPayload;
	}



	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}



	public String getResponsePayload() {
		return responsePayload;
	}


	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}






	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}


	public String getCorrelationId() {
		return correlationId;
	}


	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}



	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}


	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}



	public String getEndpointUrl() {
		return endpointUrl;
	}


	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}


	public String getEndpointFunction() {
		return endpointFunction;
	}


	public void setEndpointFunction(String endpointFunction) {
		this.endpointFunction = endpointFunction;
	}


	public String getEndpointOperationName() {
		return endpointOperationName;
	}



	public void setEndpointOperationName(String endpointOperationName) {
		this.endpointOperationName = endpointOperationName;
	}


	public String getExceptionMessage() {
		return exceptionMessage;
	}


	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}



	public Integer getCreatedUserId() {
		return createdUserId;
	}


	public void setCreatedUserId(Integer createdUserId) {
		this.createdUserId = createdUserId;
	}
	
	public String getAccessIp() {
		return accessIp;
	}



	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}



	public String getClientNodeIp() {
		return clientNodeIp;
	}



	public void setClientNodeIp(String clientNodeIp) {
		this.clientNodeIp = clientNodeIp;
	}



	public long getRetryCount() {
		return retryCount;
	}



	public void setRetryCount(long retryCount) {
		this.retryCount = retryCount;
	}



	public Long getSsapApplicantId() {
		return ssapApplicantId;
	}



	public void setSsapApplicantId(Long ssapApplicantId) {
		this.ssapApplicantId = ssapApplicantId;
	}



	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	
	public String getCustomKeyId1() {
		return customKeyId1;
	}

	public void setCustomKeyId1(String customKeyId1) {
		this.customKeyId1 = customKeyId1;
	}

	public String getCustomKeyValue1() {
		return customKeyValue1;
	}

	public void setCustomKeyValue1(String customKeyValue1) {
		this.customKeyValue1 = customKeyValue1;
	}

	public String getCustomKeyId2() {
		return customKeyId2;
	}

	public void setCustomKeyId2(String customKeyId2) {
		this.customKeyId2 = customKeyId2;
	}

	public String getCustomKeyValue2() {
		return customKeyValue2;
	}

	public void setCustomKeyValue2(String customKeyValue2) {
		this.customKeyValue2 = customKeyValue2;
	}
	
	// To AutoUpdate created and updated dates while persisting object
	@PrePersist
	public void prePersist()
	{
		this.setCreatedTimestamp(new TSDate());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void preUpdate()
	{
		this.setCreatedTimestamp(new TSDate());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GIWSPayload [responseCode=").append(responseCode)
				.append(", status=").append(status).append(", correlationId=")
				.append(correlationId).append(", createdTimestamp=")
				.append(createdTimestamp).append(", endpointUrl=")
				.append(endpointUrl).append(", endpointFunction=")
				.append(endpointFunction).append(", endpointOperationName=")
				.append(endpointOperationName).append(", retryCount=").append(retryCount)
				.append(", version=").append(version).append("]");
		return builder.toString();
	}



	public String getResponseCode() {
		return responseCode;
	}



	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}



	public String getVersion() {
		return version;
	}



	public void setVersion(String version) {
		this.version = version;
	}



	public String getGlobalId() {
		return globalId;
	}



	public void setGlobalId(String globalId) {
		this.globalId = globalId;
	}



	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}



	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}


}
