/**
 * 
 */
package com.getinsured.hix.model;

/**
 * @author Biswakesh
 *
 */
public enum UserTokensEventName {
	
	CHANGE_EMAIL("CHANGE_EMAIL"),FORGOT_PASSWORD("FORGOT_PASSWORD"),USER_ACTIVATION("USER_ACTIVATION");
	
	private String val;
	
	UserTokensEventName(String val) {
		this.val = val;
	}
	
	public String toString(){
		return this.val;
	}

}
