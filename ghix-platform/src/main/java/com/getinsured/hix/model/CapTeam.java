package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

/**
 * The persistent class for the GroupOfUsers table.
 * 
 */
@Audited
@Entity
@Table(name="CAP_TEAM")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class CapTeam {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAP_TEAM_SEQ")
	@SequenceGenerator(name = "CAP_TEAM_SEQ", sequenceName = "CAP_TEAM_SEQ", allocationSize = 1)
	@Column(name = "id")
	private Integer id;

	@Column(name="TEAM_NAME")
	private String groupName;
	
	@Column(name="CURRENT_TEAM_LEAD_USER_ID")
	private Integer groupLeadUserId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "END_DATE")
	private Date endDate;
	

	//nullable parent it of the team
	@Column(name = "PARENT_TEAM_ID")
	private Integer parentId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_TEAM_NAME_UPDATE")
	private Date lastTeamNameUpdate;

	@Column(name="TENANT_ID")
    private Long tenantId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupLeadUserId() {
		return groupLeadUserId;
	}

	public void setGroupLeadUserId(Integer groupLeadUserId) {
		this.groupLeadUserId = groupLeadUserId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        setTenantId(TenantContextHolder.getTenant().getId());
		}
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Date getLastTeamNameUpdate() {
		return lastTeamNameUpdate;
	}

	public void setLastTeamNameUpdate(Date lastTeamNameUpdate) {
		this.lastTeamNameUpdate = lastTeamNameUpdate;
	}
}
