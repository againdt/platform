package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * Response to get the partner config
 * @author hardas_d
 *
 */
public class PartnerConfigReponseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ivrNumber;
	private String logoUrl;
	private String logoRedirectUrl;
	private String campaignId;

	public String getIvrNumber() {
		return ivrNumber;
	}

	public void setIvrNumber(String ivrNumber) {
		this.ivrNumber = ivrNumber;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getLogoRedirectUrl() {
		return logoRedirectUrl;
	}

	public void setLogoRedirectUrl(String logoRedirectUrl) {
		this.logoRedirectUrl = logoRedirectUrl;
	}
	
	public String getCampaignId() {
		return campaignId;
	}
	
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
}
