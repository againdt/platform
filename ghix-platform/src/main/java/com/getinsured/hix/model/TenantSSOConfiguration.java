package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.opensaml.saml2.metadata.provider.MetadataProvider;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getinsured.hix.model.sso.XtraConfiguration;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.platform.im.util.SAMLMetadataProviderUtil;
import com.google.gson.Gson;

/**
 * Holds tenant sso configuration.
 * <p>
 *  It contains XML configurations for:
 *  <ul>
 *    <li>Identity Provider (IDP)</li>
 *    <li>Service Provider (SP)</li>
 *  </ul>
 *  Certificate information:
 *  <ul>
 *    <li>Base64-encoded certificate</li>
 *  </ul>
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 2/23/17
 */
@Entity
@Table(name = "tenant_sso_configuration")
public class TenantSSOConfiguration implements Serializable
{
  private static final long serialVersionUID = 9250853934294963L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tenant_sso_configuration_seq")
  @SequenceGenerator(name = "tenant_sso_configuration_seq")
  @Column(name = "id", unique = true)
  private Integer tenantSsoConfigurationId;

  @Column(name = "tenant_id", nullable = false, unique = true)
  private Long tenantId;

  @Column(name = "idp_xml")
  private String idpXml;

  @Column(name = "sp_xml")
  private String spXml;

  @Column(name = "sp_alias", length = 32)
  private String spAlias;

  @Column(name = "certificate")
  private String certificate;

  @Column(name = "certificate_alias", length = 32)
  private String certificateAlias;

  @Column(name = "configuration")
  private String configuration;

  @Column(name = "identity_owner", length = 32)
  @Enumerated(value = EnumType.STRING)
  private IdentityOwner identityOwner;

  @Column(name = "auth_method", length = 32)
  @Enumerated(value = EnumType.STRING)
  private AuthenticationMethod authMethod;

  @Column(name = "outbound_auth_method", length = 32)
  @Enumerated(value = EnumType.STRING)
  private OutboundAuthenticationMethod outboundAuthenticationMethod;

  @Column(name = "updated_by")
  private Integer updatedBy;

  @Temporal( TemporalType.TIMESTAMP)
  @Column(name = "updated_on")
  private Date updatedOn;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created_on")
  private Date createdOn;

  @Transient
  private XtraConfiguration xtraConfiguration;

  @Transient
  @JsonIgnore
  private MetadataProvider identityProviderMetadata;

  @Transient
  @JsonIgnore
  private MetadataProvider serviceProviderMetadata;

  /**
   * Identity Owner types.
   */
  public enum IdentityOwner
  {
	NONE("Select One"),
	GETINSURED("DB backed GI Provider"),
    GETINSURED_IDP("GI Resident Identity Provider"),
    THIRD_PARTY("Third Part Identity Provider");
    
	private String val = null;
    IdentityOwner(String val){
    	this.val = val;
    }
    
    public String val(){
    	return this.val;
    }
    
    public String toString(){
    	return this.val;
    }
  }

  /**
   * Authentication methods.
   */
  public enum AuthenticationMethod
  {
	NONE("Select One"),
    GETINSURED("DB backed authentication"),
    WEB_BASED_SSO("Web based SSO Autentication by Identity provider");
    
	AuthenticationMethod(String val){
		this.val = val;
	}
	
    private String val = null;
    
    public String val(){
    	return this.val;
    }
  }

  /**
   * Outbound Authentication methods.
   */
  public enum OutboundAuthenticationMethod
  {
	NONE,
    HTTP_BASIC
  }

  @PreUpdate
  @PrePersist
  private void beforeInsert()
  {
    if(this.updatedOn == null)
    {
      this.updatedOn = new TSDate();
    }

    if(this.createdOn == null)
    {
      this.createdOn = new TSDate();
    }

    if(this.identityOwner == null)
    {
      this.identityOwner = IdentityOwner.GETINSURED_IDP;
    }

    if(this.authMethod == null)
    {
      this.authMethod = AuthenticationMethod.GETINSURED;
    }

    if(this.outboundAuthenticationMethod == null)
    {
      this.outboundAuthenticationMethod = OutboundAuthenticationMethod.HTTP_BASIC;
    }
    if(this.spXml != null)
    {
    	this.serviceProviderMetadata = SAMLMetadataProviderUtil.getProvider(this.spXml);
    	this.spAlias = SAMLMetadataProviderUtil.getEntityId(this.serviceProviderMetadata);
    }

    if(this.xtraConfiguration != null)
    {
      Gson gson = GhixUtils.platformGson();
      this.configuration = gson.toJson(this.xtraConfiguration);
    }
  }

  @PostLoad
  private void postLoad()
  {
    if(this.configuration == null || this.configuration.trim().equals(""))
    {
      this.xtraConfiguration = new XtraConfiguration();
    }
    else
    {
      Gson gson = GhixUtils.platformGson();

      if(this.configuration != null)
      {
        this.xtraConfiguration = gson.fromJson(this.configuration, XtraConfiguration.class);
      }
      else
      {
        this.xtraConfiguration = new XtraConfiguration();
      }
    }
  }

  /**
   * Returns unique id associated with this record.
   *
   * @return unique id.
   */
  public Integer getTenantSsoConfigurationId()
  {
    return tenantSsoConfigurationId;
  }

  /**
   * Sets unique id for this record.
   *
   * @param tenantSsoConfigurationId unique id to set.
   */
  public void setTenantSsoConfigurationId(Integer tenantSsoConfigurationId)
  {
    this.tenantSsoConfigurationId = tenantSsoConfigurationId;
  }

  /**
   * Returns id of the Tenant that owns this record.
   *
   * @return Tenant Id
   * @see com.getinsured.hix.model.Tenant
   */
  public Long getTenantId()
  {
    return tenantId;
  }

  /**
   * Sets Tenant id for this record.
   *
   * @param tenantId tenant id to set.
   * @see com.getinsured.hix.model.Tenant
   */
  public void setTenantId(Long tenantId)
  {
    this.tenantId = tenantId;
  }

  /**
   * Returns Identity Provider (IDP) XML contents.
   *
   * @return XML string that defines IDP.
   */
  public String getIdpXml()
  {
    return idpXml;
  }

  /**
   * Set Identity Provider (IDP) XML configuration.
   *
   * @param idpXml XML string
   */
  public void setIdpXml(String idpXml)
  {
    this.idpXml = idpXml;
  }

  /**
   * Returns Service Provider (SP) XML contents.
   *
   * @return XML string describing SP
   */
  public String getSpXml()
  {
    return spXml;
  }

  /**
   * Set Service Provider (SP) XML configuration.
   *
   * @param spXml xml string of SP definition.
   */
  public void setSpXml(String spXml)
  {
    this.spXml = spXml;
  }

  /**
   * Returns Base64 encoded certificate configuration.
   *
   * @return Base64 encoded certificate.
   */
  public String getCertificate()
  {
    return certificate;
  }

  /**
   * Set Base64 encoded certificate for this record.
   *
   * @param certificate Base64 encoded certificate.
   */
  public void setCertificate(String certificate)
  {
    this.certificate = certificate;
  }

  /**
   * Returns user id for whoever updated this record last time.
   *
   * @return user id of person who updated this record last.
   */
  public Integer getUpdatedBy()
  {
    return updatedBy;
  }

  /**
   * Set user id of the person who updates this record.
   * @param updatedBy user id
   */
  public void setUpdatedBy(Integer updatedBy)
  {
    this.updatedBy = updatedBy;
  }

  /**
   * Returns date and time on which this record was updated last time.
   *
   * @return date and time on which this record was updated.
   */
  public Date getUpdatedOn()
  {
    return updatedOn;
  }

  /**
   * Sets date and time when this record was updated last time.
   *
   * @param updatedOn date of last time this record is updated.
   */
  public void setUpdatedOn(Date updatedOn)
  {
    this.updatedOn = updatedOn;
  }

  /**
   * Returns date on which this record was originally created.
   *
   * @return date when this record was created first time.
   */
  public Date getCreatedOn()
  {
    return createdOn;
  }

  /**
   * Sets date when this record was created first time.
   * @param createdOn date.
   */
  public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  /**
   * Returns Service Provider's alias (SP Alias)
   *
   * @return service provider alias
   */
  public String getSpAlias()
  {
    return spAlias;
  }

  /**
   * Sets Service Provider's alias.
   *
   * @param spAlias SP alias to set.
   */
  public void setSpAlias(final String spAlias)
  {
    this.spAlias = spAlias;
  }

  /**
   * Returns certificate alias.
   *
   * @return certificate alias.
   */
  public String getCertificateAlias()
  {
    return certificateAlias;
  }

  /**
   * Sets certificate lias.
   *
   * @param certificateAlias certificate lias to set.
   */
  public void setCertificateAlias(final String certificateAlias)
  {
    this.certificateAlias = certificateAlias;
  }

  /**
   * Returns Identity Owner information.
   * @return {@link IdentityOwner} object.
   */
  public IdentityOwner getIdentityOwner()
  {
    return identityOwner;
  }

  /**
   * Set identity owner information.
   *
   * @param identityOwner {@link IdentityOwner} to set.
   */
  public void setIdentityOwner(final IdentityOwner identityOwner)
  {
    this.identityOwner = identityOwner;
  }

  /**
   * Returns Authentication Method.
   *
   * @return {@link AuthenticationMethod} object.
   */
  public AuthenticationMethod getAuthMethod()
  {
    return authMethod;
  }

  /**
   * Set Authentication Method.
   *
   * @param authMethod {@link AuthenticationMethod} to set.
   */
  public void setAuthMethod(final AuthenticationMethod authMethod)
  {
    this.authMethod = authMethod;
  }

  /**
   * Returns Outbound Authentication method.
   *
   * @return {@link OutboundAuthenticationMethod} object.
   */
  public OutboundAuthenticationMethod getOutboundAuthenticationMethod()
  {
    return outboundAuthenticationMethod;
  }

  /**
   * Sets Outbound Authentication method.
   *
   * @param outboundAuthenticationMethod {@link OutboundAuthenticationMethod} to set.
   */
  public void setOutboundAuthenticationMethod(final OutboundAuthenticationMethod outboundAuthenticationMethod)
  {
    this.outboundAuthenticationMethod = outboundAuthenticationMethod;
  }

  public MetadataProvider getIdentityProviderMetadata()
  {
    return identityProviderMetadata;
  }

  public void setIdentityProviderMetadata(MetadataProvider identityProviderMetadata)
  {
    this.identityProviderMetadata = identityProviderMetadata;
  }

  public MetadataProvider getServiceProviderMetadata()
  {
    return serviceProviderMetadata;
  }

  public void setServiceProviderMetadata(MetadataProvider serviceProviderMetadata)
  {
    this.serviceProviderMetadata = serviceProviderMetadata;
  }

  public String getConfiguration()
  {
    return configuration;
  }

  public void setConfiguration(String configuration)
  {
    this.configuration = configuration;
  }

  public XtraConfiguration getXtraConfiguration()
  {
	if(this.xtraConfiguration == null){
		this.xtraConfiguration = new XtraConfiguration();
	}
    return this.xtraConfiguration;
  }

  public void setXtraConfiguration(XtraConfiguration xtraConfiguration)
  {
    this.xtraConfiguration = xtraConfiguration;
  }
}
