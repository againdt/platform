package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Audited
@Entity
@Table(name="ROLE_PROVISIONING_RULES")
public class RoleProvisioningRule implements JSONAware, Serializable {
	
	private static final Logger logger  = LoggerFactory.getLogger(RoleProvisioningRule.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String PROVISIONING_RULES_ROOT = "provisioning_rules";
	public static final String PROVISIONING_ROLE_KEY = "provisioning_role";
	public static final String PROVISIONING_ROLE_LABEL = "provisioning_role_label";
	public static final String MANAGED_ROLE_KEY = "managed_role";
	public static final String MANAGED_ROLE_LABEL = "managed_role_label";
	public static final String PRIVATE_ID_KEY = "privateId";
	public static final String ALLOW_ADD_KEY="allow_add";
	public static final String ALLOW_UPDATE_KEY = "allow_update";
	public static final String ACTIVE_KEY = "active";
	public static final String IS_MANAGING_PRIVILEGED = "is_managing_privileged";
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_PROV_SEQ")
	@SequenceGenerator(name = "ROLE_PROV_SEQ", sequenceName = "ROLE_PROV_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Integer provisionId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PROVISIONING_ROLE_ID")
    private Role provisioningRole;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MANAGED_ROLE_ID")
    private Role managedRole;
	
	@Column(name="ADD_ALLOWED")
    private char addAllowed = 'N';
	
	@Column(name="UPDATE_ALLOWED")
    private char updateAllowed = 'N';
	
	@Column(name="IS_ACTIVE")
    private char isActive = 'N';
	
	@Column(name="CREATION_TIMESTAMP")
	private Timestamp createdTimeStamp;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATED_BY")
	private AccountUser createdBy;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdatedTimeStamp;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser lastUpdatedBy;
	
	@Transient
	private String privatePrivilegeRuleId;

	public Role getProvisioningRole() {
		return provisioningRole;
	}

	public void setProvisioningRole(Role provisioningRole) {
		this.provisioningRole = provisioningRole;
	}

	public Role getManagedRole() {
		return managedRole;
	}

	public void setManagedRoles(Role managedRole) {
		this.managedRole = managedRole;
	}

	public char getAddAllowed() {
		return addAllowed;
	}

	public void setAddAllowed(char addAllowed) {
		this.addAllowed = addAllowed;
	}

	public char getUpdateAllowed() {
		return updateAllowed;
	}

	public void setUpdateAllowed(char updateAllowed) {
		this.updateAllowed = updateAllowed;
	}

	public char getIsActive() {
		return isActive;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	public void setManagedRole(Role managedRole) {
		this.managedRole = managedRole;
	}

	public boolean isActive() {
		return (this.getIsActive() == 'Y');
	}

	public boolean isAddAllowed() {
		return (this.getAddAllowed() == 'Y');
	}

	public boolean isUpdateAllowed() {
		return (this.getUpdateAllowed() == 'Y');
	}
	

	public String getPrivatePrivilegeRuleId() {
		if(this.provisionId != null && this.provisionId > 0){
			try{
				this.privatePrivilegeRuleId = GhixAESCipherPool.encrypt(Integer.toString(this.provisionId));
			}catch(Exception e){
				throw new GIRuntimeException("Private Privilege ID is not available, failed with:"+e.getMessage(),e);
			}
		}
		return this.privatePrivilegeRuleId;
	}

	public void setPrivatePrivilegeRuleId(String privatePrivilegeRuleId) {
		if(privatePrivilegeRuleId == null){
			logger.error("Null private Id received, nothing to set");
			return;
		}
		this.privatePrivilegeRuleId = privatePrivilegeRuleId;
		// This is a reverse initialization
		try{
			String provisionIdStr = GhixAESCipherPool.decrypt(privatePrivilegeRuleId);
			this.provisionId = Integer.parseInt(provisionIdStr);
		}catch(Exception e){
			logger.error("Error received, setting the privateId",e);
			throw new GIRuntimeException("Failed to initialize the provisioning ID from private Id",e);
		}
	}

	public Integer getProvisionId() {
		return provisionId;
	}

	public void setProvisionId(Integer provisionId) {
		this.provisionId = provisionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((managedRole == null) ? 0 : managedRole.getName().hashCode());
		result = prime
				* result
				+ ((provisioningRole == null) ? 0 : provisioningRole.getName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleProvisioningRule other = (RoleProvisioningRule) obj;
		if (managedRole == null) {
			if (other.managedRole != null)
				return false;
		} else if (!managedRole.equals(other.managedRole))
			return false;
		if (provisioningRole == null) {
			if (other.provisioningRole != null)
				return false;
		} 
		if(managedRole.getName().equals(other.getManagedRole().getName()) &&
				provisioningRole.getName().equals(other.getProvisioningRole().getName())){
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(MANAGED_ROLE_KEY, this.getManagedRole().getName());
		obj.put(MANAGED_ROLE_LABEL, this.getManagedRole().getLabel());
		obj.put(PROVISIONING_ROLE_KEY, this.getProvisioningRole().getName());
		obj.put(PROVISIONING_ROLE_LABEL, this.getProvisioningRole().getLabel());
		obj.put(PRIVATE_ID_KEY, this.getPrivatePrivilegeRuleId());
		obj.put(ALLOW_ADD_KEY, (this.getAddAllowed()=='Y'));
		obj.put(ALLOW_UPDATE_KEY, (this.getUpdateAllowed()=='Y'));
		obj.put(ACTIVE_KEY, (this.getIsActive()=='Y'));
		obj.put(IS_MANAGING_PRIVILEGED, (this.getManagedRole().getPrivileged() == 1));
		return obj;
	}
	
	@Override
	public String toJSONString() {
		return this.toJSONObject().toJSONString();
	}

	private static String getStringAttribute(JSONObject obj, String attribute){
		Object tmpObj = obj.get(attribute.trim());
		if(tmpObj == null || !(tmpObj instanceof String)){
			logger.error("No attribure with name "+attribute+" found in the input JSON");
			return "";
		}
		String tmp = (String)tmpObj;
		return tmp;
	}
	
	public static RoleProvisioningRule getNewRulefromJSONObj(JSONObject obj,
			HashMap<String, Role> rolesFromDB) {
		RoleProvisioningRule tmpRule = null;
		try{
			String managedRoleName = getStringAttribute(obj,RoleProvisioningRule.MANAGED_ROLE_KEY);
			String provisioningRoleName = getStringAttribute(obj, RoleProvisioningRule.PROVISIONING_ROLE_KEY);
			Role provisioningRole = rolesFromDB.get(provisioningRoleName);
			Role managedRole = rolesFromDB.get(managedRoleName);
			if(managedRole == null || provisioningRole == null){
				logger.error("Manfatory field, Managed role or provisioning role not available");
				return null;
			}
			logger.info("Found provisioned and managed roles");
			boolean allowAdd = getBooleanAttribute(obj, ALLOW_ADD_KEY);
			boolean allowUpdate = getBooleanAttribute(obj,ALLOW_UPDATE_KEY);
			boolean isActive = getBooleanAttribute(obj,ACTIVE_KEY);
			tmpRule = new RoleProvisioningRule();
			tmpRule.setProvisioningRole(provisioningRole);
			tmpRule.setManagedRole(managedRole);
			if(allowAdd){
				tmpRule.setAddAllowed('Y');
			}
			if(allowUpdate){
				tmpRule.setUpdateAllowed('Y');
			}
			if(isActive){
				tmpRule.setIsActive('Y');
			}
		}catch(Exception e){
			logger.error("Provisioning role creation from JSON failed with error:"+e.getMessage(),e);
			return null;
		}
		return tmpRule;
	}
	
	public static RoleProvisioningRule getNewRulefromJSONObj(JSONObject obj,Role provisioningRole, Role managedRole){
		RoleProvisioningRule tmpRule = null;
		try{
			boolean allowAdd = getBooleanAttribute(obj, ALLOW_ADD_KEY);
			boolean allowUpdate = getBooleanAttribute(obj,ALLOW_UPDATE_KEY);
			boolean isActive = getBooleanAttribute(obj,ACTIVE_KEY);
			tmpRule = new RoleProvisioningRule();
			tmpRule.setProvisioningRole(provisioningRole);
			tmpRule.setManagedRole(managedRole);
			if(allowAdd){
				tmpRule.setAddAllowed('Y');
			}
			if(allowUpdate){
				tmpRule.setUpdateAllowed('Y');
			}
			if(isActive){
				tmpRule.setIsActive('Y');
			}
		}catch(Exception e){
			logger.error("Provisioning role creation from JSON failed with error:"+e.getMessage(),e);
			return null;
		}
		return tmpRule;
	}

	private static boolean getBooleanAttribute(JSONObject obj, String attribute) {
		Object tmpObj = obj.get(attribute.trim());
		if(tmpObj == null || !(tmpObj instanceof Boolean)){
			logger.error("No attribure with name "+attribute+" found in the input JSON");
			return false;
		}
		Boolean tmp = (Boolean)tmpObj;
		return tmp.booleanValue();
	}

	
	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getLastUpdatedTimeStamp() {
		return lastUpdatedTimeStamp;
	}

	public void setLastUpdatedTimeStamp(Timestamp lastUpdatedTimeStamp) {
		this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
	}

	public AccountUser getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(AccountUser lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@PrePersist
	public void setDefaultTimestamps(){
		Timestamp time = new TSTimestamp(System.currentTimeMillis());
		this.setLastUpdatedTimeStamp(time);
		Integer pid = this.getProvisionId();
		if(pid == null || pid.intValue() <= 0){
			this.setCreatedTimeStamp(time);
		}
	}
	
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdatedTimeStamp(new TSTimestamp(System.currentTimeMillis()));
	}


	public static RoleProvisioningRule getExistingRulefromJSONObj(
			JSONObject obj, HashMap<String, Role> existingRoles) {
		RoleProvisioningRule tmpRule = null;
		try{
			if(existingRoles == null){
				logger.error("No existing roles supplied for validation");
				return null;
			}
			String managedRoleName = getStringAttribute(obj, MANAGED_ROLE_KEY);
			String provisioningRoleName = getStringAttribute(obj,PROVISIONING_ROLE_KEY);
			Role provisioningRole = existingRoles.get(provisioningRoleName);
			Role managedRole = existingRoles.get(managedRoleName);
			String privateId = getStringAttribute(obj, PRIVATE_ID_KEY);
			if(managedRole == null || provisioningRole == null || privateId == null){
				throw new GIRuntimeException("Invalid Input, one of required parameters are missing, expected valid provision, managed and an existing rule Identifier");
			}
			
			boolean allowAdd = getBooleanAttribute(obj, ALLOW_ADD_KEY);
			boolean allowUpdate = getBooleanAttribute(obj, ALLOW_UPDATE_KEY);
			boolean isActive = getBooleanAttribute(obj, ACTIVE_KEY);
			tmpRule = new RoleProvisioningRule();
			tmpRule.setPrivatePrivilegeRuleId(privateId);
			
			if(allowAdd){
				tmpRule.setAddAllowed('Y');
			}
			if(allowUpdate){
				tmpRule.setUpdateAllowed('Y');
			}
			if(isActive){
				tmpRule.setIsActive('Y');
			}
					
		}catch(Exception e){
			logger.error("Provisioning role creation from JSON failed with error:"+e.getMessage(),e);
			return null;
		}
		return tmpRule;
	}
	
	public boolean hasEqualAccess(RoleProvisioningRule other){
		return (this.getAddAllowed() == other.getAddAllowed()) &&
				(this.getUpdateAllowed() == other.getUpdateAllowed()) &&
				(this.getIsActive() == other.getIsActive());
	}
}

