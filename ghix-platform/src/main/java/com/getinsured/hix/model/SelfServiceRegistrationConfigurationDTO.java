package com.getinsured.hix.model;

import java.io.Serializable;

public class SelfServiceRegistrationConfigurationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6822670763573080981L;

	public static enum BypassTypeConfig {
		ON("ON"), OFF("OFF");

		private String enabled;

		private BypassTypeConfig(String enabled) {
			this.enabled = enabled;
		}

		public String getEnabled() {
			return enabled;
		}
	}

	private BypassTypeConfig bypassD2CRegistration;
	private BypassTypeConfig bypassFFMRegistration;

	public BypassTypeConfig getBypassFFMRegistration() {
		return bypassFFMRegistration;
	}

	public void setBypassFFMRegistration(BypassTypeConfig bypassFFMRegistration) {
		this.bypassFFMRegistration = bypassFFMRegistration;
	}

	public BypassTypeConfig getBypassD2CRegistration() {
		return bypassD2CRegistration;
	}

	public void setBypassD2CRegistration(BypassTypeConfig bypassD2CRegistration) {
		this.bypassD2CRegistration = bypassD2CRegistration;
	}

}
