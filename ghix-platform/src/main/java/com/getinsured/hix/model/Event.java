package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="events")
public class Event implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum STATUS { ENABLED, DISABLED;}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENTS_Seq")
	@SequenceGenerator(name = "EVENTS_Seq", sequenceName = "EVENTS_Seq", allocationSize = 1)
	private Integer id;
	
	@Column(name="EVENT_NAME")
	private String eventName;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name="START_DATE",nullable=false)
	private Date startDate;

	@Temporal(value = TemporalType.DATE)
	@Column(name="END_DATE")
	private Date endDate;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private STATUS status;
	
	@OneToOne
	@JoinColumn(name="NOTICE_TYPE_ID")
	private NoticeType noticeType;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	/** Created Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date created;

	/** Updated Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	public NoticeType getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(NoticeType noticeType) {
		this.noticeType = noticeType;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}
}
