package com.getinsured.hix.model;
public enum GhixNoticeCommunicationMethod {
	
	/**
	 * Please add any value in alphabetic order.
	 */
	Email("Email"),Mail("Mail"),EmailAndMail("EmailAndMail"),None("None");

	private String method;

	/**
	 * @param code
	 */
	private GhixNoticeCommunicationMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the code
	 */
	public String getMethod() {
		return method;
	}



}