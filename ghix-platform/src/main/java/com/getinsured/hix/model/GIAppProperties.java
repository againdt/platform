package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;



/**
 * The persistent class for the roles database table.
 *
 */
@Audited 
@Entity
@Cacheable
@Table(name="gi_app_config")
@Cache(region = "GIAppProperties", usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class GIAppProperties implements Serializable 
{
	private static final long serialVersionUID = 3459212926394174825L;

	public GIAppProperties() { }

	public GIAppProperties(final Integer id, final String propertyKey, final String propertyValue, 
			final String description, final Date creationTimestamp, final Date updated,
			final Integer createdBy, final Integer updatedBy)
	{
		this.id = id;
		this.propertyKey = propertyKey;
		this.propertyValue = propertyValue;
		this.description = description;
		this.creationTimestamp = creationTimestamp;
		this.updated = updated;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GI_APP_CONFIG_SEQ")
	@SequenceGenerator(name = "GI_APP_CONFIG_SEQ", sequenceName = "GI_APP_CONFIG_SEQ", allocationSize = 1)
	@org.springframework.data.annotation.Id
	private Integer id;


	@Column(name="PROPERTY_KEY")
	private String propertyKey;

	@Column(name="PROPERTY_VALUE")
	private String propertyValue;

	@NotAudited
	@Column(name="DESCRIPTION")
	private String description;



	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updated;


	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Column(name="LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getPropertyKey() {
		return propertyKey;
	}

	public void setPropertyKey(final String propertyKey) {
		this.propertyKey = propertyKey;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(final String propertyValue) {
		this.propertyValue = propertyValue;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}


	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(final Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(final Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(final Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(final Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

}
