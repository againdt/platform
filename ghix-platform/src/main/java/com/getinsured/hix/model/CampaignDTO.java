package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class CampaignDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -991498302381450293L;
	
	private String campaignId;

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
}
