package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The persistent class for the users database table.
 *
 */
@Entity
@Table(name="users_aud")
public class UsersAud implements Serializable {
	private static final Logger LOGGER = LoggerFactory.getLogger(UsersAud.class);

	@Id
	private int id;

	@Column(name="uuid",unique = true)
	private String uuid;

	@Column(name="password" ,length=2000)
	private String password;

	@Column(name="rev" ,length=2000)
	private int rev;
	
	public int getId() {
		return this.id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


}