package com.getinsured.hix.model.sso;

import java.io.Serializable;

/**
 * Identity Management information.
 *
 * @author Yevgen Golubenko
 * @since 7/25/17
 */
public class IdentityManagement implements Serializable
{
  private static final long serialVersionUID = 1918243178384405898L;
  private String tenantAdminUsername;
  private String tenantAdminPassword;
  private String tenantDomain;
  private boolean remoteUserManagementEnabled;

  public String getTenantAdminUsername()
  {
    return tenantAdminUsername;
  }

  public void setTenantAdminUsername(String tenantAdminUsername)
  {
    this.tenantAdminUsername = tenantAdminUsername;
  }

  public String getTenantAdminPassword()
  {
    return tenantAdminPassword;
  }

  public void setTenantAdminPassword(String tenantAdminPassword)
  {
    this.tenantAdminPassword = tenantAdminPassword;
  }

  public boolean isRemoteUserManagementEnabled()
  {
    return remoteUserManagementEnabled;
  }

  public void setRemoteUserManagement(boolean remoteUserManagement)
  {
    this.remoteUserManagementEnabled = remoteUserManagement;
  }

  public String getTenantDomain()
  {
    return tenantDomain;
  }

  public void setTenantDomain(String tenantDomain)
  {
    this.tenantDomain = tenantDomain;
  }
}