package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Map;

import com.getinsured.hix.platform.enums.OnOffEnum;
import com.getinsured.hix.platform.enums.YesNoEnum;

public class UIConfigurationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2371611435864690115L;
	
	public enum HomePageSections {
		INTRO("intro"), LOWER_YOUR_COST("lower_your_cost"), STORIES("stories"), OUR_PROCESS("our_process"), CARRIER_LOGOS("carrier_logos");
		
		private String title;
		
		HomePageSections(String title) {
			this.title = title;
		}
		
		public String title() {
			return this.title;
		}
	}

	public enum EligibilityPageSidebarSections {
		QUALIFIED_AMERICANS("qualified_americans", "Qualified Americans"), HELP_PEOPLE("help_people_get_insured", "Help People");
		
		private String sectionName;
		private String label;
		
		EligibilityPageSidebarSections(String sectionName, String label) {
			this.sectionName = sectionName;
			this.label = label;
		}
		
		public String label() {
			return this.label;
		}
		public String sectionName() {
			return this.sectionName;
		}
	}
	
	private String logoName;
	
	private String heroImageName;
	
	private String brandingName;
	
	private String themeName;
	
	private String favIconName;
	
	private String privacyNotice;
	
	private BrandingConfigurationDTO brandingConfiguration;
	
	private OnOffEnum layeredHelpEnabled;
	
	private OnOffEnum lightBoxEnabled;
	
	private String gaAnalyticsTrackingCode;
	
	private Map<String, String> homePageSections;
	
	private String capShortPrivacyText;
	
	private String capLongPrivacyText;
	
	private String customerAgreement;

	private String heroImageMessage1;
	
	private String heroImageMessage2;
	
	private OnOffEnum eligibilityRhsSectionEnabled;
	
	private String eligibilityRhsSection;
	
	private OnOffEnum issuerHiosIdRestricted;
	
	private YesNoEnum showPufPlans;
	
	private String tagManagerContainerId;
	
	private OnOffEnum tabbedShoppingView;
	
	private OnOffEnum recommendationFlag;
	
	private String heroImageSpanishMessage1;
	
	private String heroImageSpanishMessage2;
	
	private String tagManagerEnvironmentId;
	
	private String googleOptimize;
	
	public String getCustomerAgreement() {
		return customerAgreement;
	}

	public void setCustomerAgreement(String customerAgreement) {
		this.customerAgreement = customerAgreement;
	}

	public Map<String, String> getHomePageSections() {
		return homePageSections;
	}

	public void setHomePageSections(Map<String, String> homePageSections) {
		this.homePageSections = homePageSections;
	}

	public BrandingConfigurationDTO getBrandingConfiguration() {
		return brandingConfiguration;
	}

	public void setBrandingConfiguration(BrandingConfigurationDTO brandingConfiguration) {
		this.brandingConfiguration = brandingConfiguration;
	}

	public OnOffEnum getLayeredHelpEnabled() {
		return layeredHelpEnabled;
	}

	public void setLayeredHelpEnabled(OnOffEnum layeredHelpEnabled) {
		this.layeredHelpEnabled = layeredHelpEnabled;
	}
	
	public OnOffEnum getLightBoxEnabled() {
		return lightBoxEnabled;
	}

	public void setLightBoxEnabled(OnOffEnum lightBoxEnabled) {
		this.lightBoxEnabled = lightBoxEnabled;
	}

	public String getPrivacyNotice() {
		return privacyNotice;
	}

	public void setPrivacyNotice(String privacyNotice) {
		this.privacyNotice = privacyNotice;
	}

	public String getLogoName() {
		return logoName;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}

	public String getCapShortPrivacyText() {
		return capShortPrivacyText;
	}

	public void setCapShortPrivacyText(String capShortPrivacyText) {
		this.capShortPrivacyText = capShortPrivacyText;
	}

	public String getCapLongPrivacyText() {
		return capLongPrivacyText;
	}

	public void setCapLongPrivacyText(String capLongPrivacyText) {
		this.capLongPrivacyText = capLongPrivacyText;
	}

	public String getGaAnalyticsTrackingCode() {
		return gaAnalyticsTrackingCode;
	}

	public void setGaAnalyticsTrackingCode(String gaAnalyticsTrackingCode) {
		this.gaAnalyticsTrackingCode = gaAnalyticsTrackingCode;
	}

	public String getHeroImageMessage1() {
		return heroImageMessage1;
	}

	public void setHeroImageMessage1(String heroImageMessage1) {
		this.heroImageMessage1 = heroImageMessage1;
	}

	public String getHeroImageMessage2() {
		return heroImageMessage2;
	}

	public void setHeroImageMessage2(String heroImageMessage2) {
		this.heroImageMessage2 = heroImageMessage2;
	}

	public String getEligibilityRhsSection() {
		return eligibilityRhsSection;
	}

	public void setEligibilityRhsSection(String eligibilityRhsSection) {
		this.eligibilityRhsSection = eligibilityRhsSection;
	}

	public OnOffEnum getEligibilityRhsSectionEnabled() {
		return eligibilityRhsSectionEnabled;
	}

	public void setEligibilityRhsSectionEnabled(
			OnOffEnum eligibilityRhsSectionEnabled) {
		this.eligibilityRhsSectionEnabled = eligibilityRhsSectionEnabled;
	}

	public OnOffEnum getIssuerHiosIdRestricted() {
		return issuerHiosIdRestricted;
	}

	public void setIssuerHiosIdRestricted(OnOffEnum issuerHiosIdRestricted) {
		this.issuerHiosIdRestricted = issuerHiosIdRestricted;
	}
	
	public YesNoEnum getShowPufPlans() {
		return showPufPlans;
	}

	public void setShowPufPlans(YesNoEnum showPufPlans) {
		this.showPufPlans = showPufPlans;
	}

	public String getTagManagerContainerId() {
		return tagManagerContainerId;
	}

	public void setTagManagerContainerId(String tagManagerContainerId) {
		this.tagManagerContainerId = tagManagerContainerId;
	}

	public OnOffEnum getTabbedShoppingView() {
		return tabbedShoppingView;
	}

	public void setTabbedShoppingView(OnOffEnum tabbedShoppingView) {
		this.tabbedShoppingView = tabbedShoppingView;
	}

	public OnOffEnum getRecommendationFlag() {
		return recommendationFlag;
	}

	public void setRecommendationFlag(OnOffEnum recommendationFlag) {
		this.recommendationFlag = recommendationFlag;
	}

	public String getHeroImageSpanishMessage1() {
		return heroImageSpanishMessage1;
	}

	public void setHeroImageSpanishMessage1(String heroImageSpanishMessage1) {
		this.heroImageSpanishMessage1 = heroImageSpanishMessage1;
	}

	public String getHeroImageSpanishMessage2() {
		return heroImageSpanishMessage2;
	}

	public void setHeroImageSpanishMessage2(String heroImageSpanishMessage2) {
		this.heroImageSpanishMessage2 = heroImageSpanishMessage2;
	}

	public String getTagManagerEnvironmentId() {
		return tagManagerEnvironmentId;
	}

	public void setTagManagerEnvironmentId(String tagManagerEnvironmentId) {
		this.tagManagerEnvironmentId = tagManagerEnvironmentId;
	}

	public String getGoogleOptimize() {
		return googleOptimize;
	}

	public void setGoogleOptimize(String googleOptimize) {
		this.googleOptimize = googleOptimize;
	}

	public String getHeroImageName() {
		return heroImageName;
	}

	public void setHeroImageName(String heroImageName) {
		this.heroImageName = heroImageName;
	}

	public String getBrandingName() {
		return brandingName;
	}

	public void setBrandingName(String brandingName) {
		this.brandingName = brandingName;
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getFavIconName() {
		return favIconName;
	}

	public void setFavIconName(String favIconName) {
		this.favIconName = favIconName;
	}

}
