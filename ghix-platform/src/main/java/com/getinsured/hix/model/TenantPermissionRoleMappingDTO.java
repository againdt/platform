package com.getinsured.hix.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class TenantPermissionRoleMappingDTO {
	
	private String tenantName;
	private String permissionName;
	private String permissionDescription;
	private boolean isActiveForTenant;
	private String tenantPermissionId;
//	TODO - this can be used later in case we want to push more information for roles to UI
	private List<RoleDTO> rolesDTO = new ArrayList<RoleDTO>();
	private List<String> roleNames = new ArrayList<String>();
	
	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getPermissionDescription() {
		return permissionDescription;
	}

	public void setPermissionDescription(String permissionDescription) {
		this.permissionDescription = permissionDescription;
	}

	public boolean getIsActiveForTenant() {
		return isActiveForTenant;
	}

	public void setIsActiveForTenant(boolean isActiveForTenant) {
		this.isActiveForTenant = isActiveForTenant;
	}


	public List<String> getRoleNames() {
		return roleNames;
	}

	public void setRoleNames(List<String> roleNames) {
		this.roleNames = roleNames;
	}

	public String getTenantPermissionId() {
		return tenantPermissionId;
	}

	public void setTenantPermissionId(String tenantPermissionId) {
		this.tenantPermissionId = tenantPermissionId;
	}

	public List<RoleDTO> getRolesDTO() {
		return rolesDTO;
	}

	public void setRolesDTO(List<RoleDTO> rolesDTO) {
		this.rolesDTO = rolesDTO;
	}

	class RoleDTO{
		private String roleName;
		private boolean doesRolesHasThisPermission;
		private boolean isEnabled;
		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
		public boolean isDoesRolesHasThisPermission() {
			return doesRolesHasThisPermission;
		}
		public void setDoesRolesHasThisPermission(boolean doesRolesHasThisPermission) {
			this.doesRolesHasThisPermission = doesRolesHasThisPermission;
		}
		public boolean isEnabled() {
			return isEnabled;
		}
		public void setEnabled(boolean isEnabled) {
			this.isEnabled = isEnabled;
		}
		
		
	}
	

}
