package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="triggered_events")
public class TriggeredEvent implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRIGGERED_EVENTS_Seq")
	@SequenceGenerator(name = "TRIGGERED_EVENTS_Seq", sequenceName = "TRIGGERED_EVENTS_Seq", allocationSize = 1)
	private Integer id;
	
	@OneToOne
	@JoinColumn(name="NOTICE_ID")
	private Notice notice;
	
	@ManyToOne
	@JoinColumn(name="EVENT_ID")
	private Event Event;
	
	@Column(name = "JSON_MERGE_TAGS", length = 4000)
	private String jsonMergeTags;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	/** Created Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date created;

	/** Updated Date */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Event getEvent() {
		return Event;
	}

	public void setEvent(Event event) {
		Event = event;
	}
	
	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	public String getJsonMergeTags() {
		return jsonMergeTags;
	}

	public void setJsonMergeTags(String jsonMergeTags) {
		this.jsonMergeTags = jsonMergeTags;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}
	
}
