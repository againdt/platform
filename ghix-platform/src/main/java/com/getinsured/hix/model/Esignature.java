package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.groups.Default;

/**
 * @author raut_a
 * JIRA - HIX-2469
 * This is a generic table to all the modules.
 * This table is used to store the e-signatures of the user/issuer etc.
 * Currently the signature is to be stored in varchar format only
 * The service of this table is in /hix/platform/service/EsignatureService.java 
 */

@Audited
@Entity
@Table(name="ESIGNATURE")
public class Esignature implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 * This validation group provides groupings for fields  {@link #}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface VerifyPlan extends Default{
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Esignature_Seq")
	@SequenceGenerator(name = "Esignature_Seq", sequenceName = "Esignature_Seq", allocationSize = 1)
	private int id;
	
    @Column(name="MODULE_NAME", unique = false, nullable = true)
	private String moduleName;
	
    // The reference id can be like issuer id, plan id, employer id etc. (but not account user id)  
    @Column(name="REF_ID" , length=10)	
	private Integer refId;
	
	@NotEmpty(message="{err.firstName}",groups={Esignature.VerifyPlan.class})
    @Column(name = "FIRST_NAME")
	private String firstName;

	@NotEmpty(message="{err.lastName}",groups={Esignature.VerifyPlan.class})
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@NotEmpty(message="{err.statements}",groups={Esignature.VerifyPlan.class})
	@Column(name = "ESIGNATURE_STATEMENT")
	private String statements;
	
	@NotEmpty(message="{err.eSignature}",groups={Esignature.VerifyPlan.class})
	@Column(name = "ESIGN")
	private String eSignature;
    
	@Temporal( TemporalType.TIMESTAMP)
    @Column(name="ESIGN_DATE")
	private Date esignDate;

    //bi-directional many-to-one association to AccountUser
	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(cascade = CascadeType.REFRESH) 
    @JoinColumn(name="ESIGN_BY")
    private AccountUser user;
   
	
	@Column(name="ONBEHALF_ESIGN")
	private String onBehalfEsign;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@Audited
	@OneToOne
	@JoinColumn(name="CREATED_BY")
	private AccountUser createdBy;
	
	@Audited
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;
	
	
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getOnBehalfEsign() {
		return onBehalfEsign;
	}

	public void setOnBehalfEsign(String onBehalfEsign) {
		this.onBehalfEsign = onBehalfEsign;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName.toUpperCase();
	}

	public Integer getRefId() {
		return refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStatements() {
		return statements;
	}

	public void setStatements(String statements) {
		this.statements = statements;
	}

	public String geteSignature() {
		return eSignature;
	}

	public void seteSignature(String eSignature) {
		this.eSignature = eSignature;
	}

	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setEsignDate(new TSDate());
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}

}
