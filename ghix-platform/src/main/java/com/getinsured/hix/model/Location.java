package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for the locations database table.
 *
 */
@Audited
@Entity
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "locations")
public class Location implements Serializable, Comparable<Location> {
	private static final long serialVersionUID = 1L;

	public interface EmployerWorkistesGroup extends Default {	}



	@XmlElement(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Location_Seq")
	@SequenceGenerator(name = "Location_Seq", sequenceName = "locations_seq", allocationSize = 1)
	private int id;

	@Column(name = "add_on_zip")
	private int addOnZip;

	@Pattern(regexp="^[a-zA-Z0-9 '.#,-/]+$",message="{label.checkAddressValue}",groups=Location.EmployerWorkistesGroup.class)
	@NotEmpty(message="{label.validateAddress}",groups=Location.EmployerWorkistesGroup.class)
	@NotNull(message="{label.validateAddress}",groups=Location.EmployerWorkistesGroup.class)
	@Size(min=1, max=88, message="{label.validateaddressLength}",groups=Location.EmployerWorkistesGroup.class)
	@XmlElement(name="address1")
	@Column(name = "address1")
	private String address1;

	@Pattern(regexp="^[a-zA-Z0-9 '.#,-/]*$",message="{label.checkAddressValue}",groups=Location.EmployerWorkistesGroup.class)
	@XmlElement(name="address2")
	@Column(name = "address2")
	@Size(max=75, message="{label.validateaddressLength}",groups=Location.EmployerWorkistesGroup.class)
	private String address2;

	@NotEmpty(message="{label.validateCity}",groups=Location.EmployerWorkistesGroup.class)
	@NotNull(message="{label.validateCity}",groups=Location.EmployerWorkistesGroup.class)
	@Pattern(regexp = "^[0-9a-zA-Z '.-]+$", message = "{label.checkCityValue}", groups=Location.EmployerWorkistesGroup.class)
	@Size(min=1, max=100,message="{label.checkCityLength}",groups=Location.EmployerWorkistesGroup.class)
	@XmlElement(name="city")
	@Column(name = "city")
	private String city;

	@NotEmpty(message="{label.validateState}",groups=Location.EmployerWorkistesGroup.class)
	@XmlElement(name="state")
	@Column(name = "state")
	private String state;

	@NotEmpty(message="{label.validatecounty}",groups=Location.EmployerWorkistesGroup.class)
	@NotNull(message="{label.validatecounty}",groups=Location.EmployerWorkistesGroup.class)
    @Pattern(regexp = "^[a-zA-Z '-,]+#[0-9]+$", message = "{label.validatecounty}", groups=Location.EmployerWorkistesGroup.class)
	@XmlElement(name="county")
	@Column(name = "county")
	private String county;

	@XmlElement(name="county_code")
	@Column(name = "county_code")
	private String countycode;

    @Pattern(regexp = "^[0-9]{5}$", message = "{label.validateempzip}", groups=Location.EmployerWorkistesGroup.class)
    @NotNull(message = "{label.validateempzip}", groups=Location.EmployerWorkistesGroup.class)
	@XmlElement(name="zip")
	@Column(name = "zip")
	private String zip;

	// Residential Delivery Indicator (residential or commercial). R:
	// Residential, C:Commercial, U:Unknown
	@Column(name = "rdi")
	private String rdi;

	@Column(name = "lattitude", columnDefinition = "decimal(15,9)")
	private double lat;

	@Column(name = "longitude", columnDefinition = "decimal(15,9)")
	private double lon;

	@XmlElement(name="createdOn")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date created;

	@XmlElement(name="updatedOn")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date updated;

	public Location() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAddOnZip() {
		return this.addOnZip;
	}

	public void setAddOnZip(int addOnZip) {
		this.addOnZip = addOnZip;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		if(address2!=null && address2.length()==0){
			address2 = null;
		}
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountycode() {
		return countycode;
	}

	public void setCountycode(String countycode) {
		this.countycode = countycode;
	}

//	/**
//	 * @deprecated
//	 * @author - Nikhil Talreja
//	 * @since - 14 August, 2013
//	 *
//	 * HIX-15417 Change Datatype of Zipcode in Location table to Varchar2
//	 *
//	 * This method will be removed by the start of OO Sprint
//	 */
//	@Deprecated
//	public int getZipCode() {
//		if (getZip() != null && getZip().trim().length() > 0)
//			return Integer.parseInt(getZip());
//		else
//			return 0;
//	}
//
//	/**
//	 * @deprecated
//	 * @author - Nikhil Talreja
//	 * @since - 14 August, 2013
//	 *
//	 * HIX-15417 Change Datatype of Zipcode in Location table to Varchar2
//	 *
//	 * This method will be removed by the start of OO Sprint
//	 */
//	@Deprecated
//	public void setZip(int zip) {
//		setZip(String.valueOf(zip));
//	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}


	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdated(new TSDate());
	}

	/*
	 * public String getZip5() { DecimalFormat zip5 = new
	 * DecimalFormat("00000"); return zip5.format(zip); }
	 *//**
	 * This is very important function to get this full zip for postal
	 * address purpose.
	 *
	 * @return
	 */
	/*
	 * public String getFullZip() { DecimalFormat zip5 = new
	 * DecimalFormat("00000"); DecimalFormat zip4 = new DecimalFormat("0000");
	 * return zip5.format(zip) + "-" + zip4.format(addOnZip); }
	 */

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("address1=").append(address1).append(", ");
		sb.append("address2=").append(address2).append(", ");
		sb.append("city=").append(city).append(", ");
		sb.append("county=").append(county).append(", ");
		sb.append("countycode=").append(countycode).append(", ");
		sb.append("zip=").append(zip).append(", ");
		sb.append("state=").append(state).append(", ");
		sb.append("rdi=").append(rdi).append(", ");
		sb.append("lat=").append(lat).append(", ");
		sb.append("lon=").append(lon);
		return sb.toString();
	}

	public String getRdi() {
		return rdi;
	}

	public void setRdi(String rdi) {
		this.rdi = rdi;
	}

	@Override
	public int compareTo(Location obj) {
		if (obj instanceof Location) {

			Location loc = obj;
			if (loc != null) {
				if ((loc.getAddress1() == null && this.getAddress1() == null ) || (obj.getAddress1() != null
						&& loc.getAddress1() != null
						&& this.getAddress1().trim()
								.equalsIgnoreCase(loc.getAddress1().trim()))) {

					if ( (obj.getAddress2() == null && loc.getAddress2() == null && this.getAddress2() == null ) ||
						 (obj.getAddress2() != null	&& loc.getAddress2() != null &&	this.getAddress2() !=null &&
								this.getAddress2().trim().equalsIgnoreCase(loc.getAddress2().trim())) ){

						if ((loc.getCity() == null && this.getCity() == null ) || obj.getCity() != null
								&& loc.getCity() != null
								&& this.getCity() !=null 
								&& this.getCity().trim()
										.equalsIgnoreCase(loc.getCity().trim())) {

							if ((loc.getState() == null && this.getState() == null ) ||  obj.getState() != null
									&& loc.getState() != null
									&& this.getState() != null 
									&& this.getState().trim().equalsIgnoreCase(loc.getState().trim())) {

								if ((loc.getState() == null && this.getState() == null ) || (this.getZip()!=null && loc.getZip()!=null && this.getZip().equals(loc.getZip()))) {
									return 0;
								}
							}
						}
					}
				}
			}
		}
		return -1;
	}
}
