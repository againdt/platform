package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;


/**
 * The persistent class for the LOOKUP_VALUE database table.
 * 
 */
@Audited
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name="lookup_value")
public class LookupValue implements Serializable, Comparable<LookupValue>  {
	private static final long serialVersionUID = -2776266933414711511L;

	@Id
	@Column(name = "lookup_value_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lookupValue_seq")
	@SequenceGenerator(name = "lookupValue_seq", sequenceName = "lookupValue_seq", allocationSize = 1)
	private Integer lookupValueId;

	@ManyToOne
	@JoinColumn(name="lookup_type_id")
	private LookupType lookupType;
	
	@XmlElement(name="lookupValueCode")
	@Column(name = "lookup_value_code")
	private String lookupValueCode;
	
	@XmlElement(name="lookupValueLabel")
	@Column(name = "lookup_value_label")
	private String lookupValueLabel;
	
	@Column(name = "parent_lookup_value_id")
	private String parentLookupValueId;
	
	@Column(name = "isobsolete")
	private char isobsolete;
	
	@Column(name = "description")
	private String description;
	
	@NotAudited
	@ManyToOne
    @JoinColumn(name="lookup_locale_id")
	private LookupLocale lookupLocale;
	
	public LookupLocale getLookupLocale() {
		return lookupLocale;
	}

	public void setLookupLocale(LookupLocale lookupLocale) {
		this.lookupLocale = lookupLocale;
	}

	public LookupType getLookupType() {
		return lookupType;
	}

	public void setLookupType(LookupType lookupType) {
		this.lookupType = lookupType;
	}

	public Integer getLookupValueId() {
		return lookupValueId;
	}

	public void setLookupValueId(Integer lookupValueId) {
		this.lookupValueId = lookupValueId;
	}

	public String getLookupValueCode() {
		return lookupValueCode;
	}

	public void setLookupValueCode(String lookupValueCode) {
		this.lookupValueCode = lookupValueCode;
	}

	public String getLookupValueLabel() {
		return lookupValueLabel;
	}

	public void setLookupValueLabel(String lookupValueLabel) {
		this.lookupValueLabel = lookupValueLabel;
	}

	public String getParentLookupValueId() {
		return parentLookupValueId;
	}

	public void setParentLookupValueId(String parentLookupValueId) {
		this.parentLookupValueId = parentLookupValueId;
	}

	public char getIsobsolete() {
		return isobsolete;
	}

	public void setIsobsolete(char isobsolete) {
		this.isobsolete = isobsolete;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "LookupValue [lookupValueId=" + lookupValueId
				+ ", lookupTypeId=" + lookupType.getLookupTypeId() + ", lookupValueCode="
				+ lookupValueCode + ", lookupValueLabel=" + lookupValueLabel
				+ ", parentLookupValueId=" + parentLookupValueId
				+ ", isobsolete=" + isobsolete + ", description= "+ description +" ]";
	}

	@Override
	public int compareTo(LookupValue obj) {
		return obj.getLookupValueLabel().compareTo(this.lookupValueLabel);
	}
	
}
