package com.getinsured.hix.model;

import java.util.Comparator;
import java.util.List;

import com.getinsured.hix.platform.cache.CacheableObject;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class MenuItemDTO implements CacheableObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String OBJECT_KEY="MenuItem";
	
	private long id;
	private Long parentId;
	private String caption;
	private String url;
	private String redirecturl;
	private String title;
	private int order;
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	private List<MenuItemDTO> subMenus;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRedirecturl() {
		return redirecturl;
	}

	public void setRedirecturl(String redirecturl) {
		this.redirecturl = redirecturl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<MenuItemDTO> getSubMenus() {
		return subMenus;
	}

	public void setSubMenus(List<MenuItemDTO> subMenus) {
		this.subMenus = subMenus;
	}

	@Override
	public String getObjectKey() {
		return OBJECT_KEY;
	}

	@Override
	public String getKey() {
		return String.valueOf(id);
	}
	
	public static final Comparator<MenuItemDTO> MENU_ITEMS_COMPARATOR = new Comparator<MenuItemDTO>() {

		@Override
		public int compare(MenuItemDTO o1, MenuItemDTO o2) {
			
			return o1.getOrder() - o2.getOrder();
			
		}
	};
	
	
	public static final Comparator<MenuItemDTO> MENU_ITEMS_SORT_BY_CAPTION = new Comparator<MenuItemDTO>(){

		@Override
		public int compare(MenuItemDTO o1, MenuItemDTO o2) {
			return o1.getCaption().compareTo(o2.getCaption());
		}
		
	};

	 
}
