package com.getinsured.hix.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "application_event")
public class AppEvent {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AppEvent_Seq")
	@SequenceGenerator(name = "AppEvent_Seq", sequenceName = "application_event_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="gi_type")
	private String type;
	
	@Column(name="module_id")
	private String moduleId;
	
	@Column(name="module_name")
	private String moduleName;

	@Column(name="eventjson")
	private String eventJson;

	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="created_by_username")
	private String createdByUserName;
	
	@Column(name="created_by_role")
	private String createdByUserRole;
	
	@Column(name="creation_timestamp")
	private Timestamp creationTimeStamp;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getEventJson() {
		return eventJson;
	}

	public void setEventJson(String eventJson) {
		this.eventJson = eventJson;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedByUserName() {
		return createdByUserName;
	}

	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	public String getCreatedByUserRole() {
		return createdByUserRole;
	}

	public void setCreatedByUserRole(String createdByUserRole) {
		this.createdByUserRole = createdByUserRole;
	}

	public Timestamp getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Timestamp creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
}
