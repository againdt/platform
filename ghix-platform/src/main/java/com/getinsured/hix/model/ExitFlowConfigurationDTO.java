package com.getinsured.hix.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class ExitFlowConfigurationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -698609234007491027L;
	private boolean displayOffer;
	private String message;
	private String messageSpanish;
	private String buttonText;
	private String buttonTextSpanish;
	private String link;
	private String title;
	private String titleSpanish;
	
	public boolean isDisplayOffer() {
		return displayOffer;
	}
	public void setDisplayOffer(boolean displayOffer) {
		this.displayOffer = displayOffer;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getButtonText() {
		return buttonText;
	}
	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}
	public String getLink() {
		return link;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	public String getMessageSpanish() {
		return messageSpanish;
	}
	public void setMessageSpanish(String messageSpanish) {
		this.messageSpanish = messageSpanish;
	}
	public String getButtonTextSpanish() {
		return buttonTextSpanish;
	}
	public void setButtonTextSpanish(String buttonTextSpanish) {
		this.buttonTextSpanish = buttonTextSpanish;
	}
	public String getTitleSpanish() {
		return titleSpanish;
	}
	public void setTitleSpanish(String titleSpanish) {
		this.titleSpanish = titleSpanish;
	}
	@JsonIgnore
	public boolean isEmpty(){
		boolean isEmpty = true;
		
		if(StringUtils.isNotBlank(this.getButtonText()) && StringUtils.isNotBlank(this.getLink()) && StringUtils.isNotBlank(this.getMessage()) && StringUtils.isNotBlank(this.getTitle())){
			isEmpty = false;
		}
		
		return isEmpty;
	}
	@Override 
	public String toString(){
		return "ExitFlowConfigurationDTO [displayOfferOptions=" + displayOffer
		+ ", message=" + message + ", buttonText="
		+ buttonText + ", link=" + link
		+"]";
	}

}
