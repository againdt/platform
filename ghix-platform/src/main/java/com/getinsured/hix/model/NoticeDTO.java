package com.getinsured.hix.model;

import java.util.Date;

/**
 * @since 04th January 2016
 * @author Sharma_K
 * @version 1.0
 * NoticeDTO
 *
 */
public class NoticeDTO {
	private int noticeId;
	private String ecmDocId;
	private Date creationTimeStamp;
	
	/**
	 * 
	 * @param noticeId
	 * @param ecmDocId
	 * @param creationTimeStamp
	 */
	public NoticeDTO(int noticeId, String ecmDocId, Date creationTimeStamp){
		this.noticeId = noticeId;
		this.ecmDocId = ecmDocId;
		this.creationTimeStamp = creationTimeStamp;
	}
	
	/**
	 * @return the noticeId
	 */
	public int getNoticeId() {
		return noticeId;
	}
	/**
	 * @param noticeId the noticeId to set
	 */
	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}
	/**
	 * @return the ecmDocId
	 */
	public String getEcmDocId() {
		return ecmDocId;
	}
	/**
	 * @param ecmDocId the ecmDocId to set
	 */
	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}
	/**
	 * @return the creationTimeStamp
	 */
	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}
	/**
	 * @param creationTimeStamp the creationTimeStamp to set
	 */
	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
}
