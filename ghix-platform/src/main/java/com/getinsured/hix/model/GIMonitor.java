package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="GI_MONITOR")
public class GIMonitor {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GIMONITOR_SEQ")
	@SequenceGenerator(name = "GIMONITOR_SEQ", sequenceName = "GI_MONITOR_SEQ", allocationSize = 1)
	@Column(name = "monitor_id")
	private Integer id;
	
	@Column(name="component")
    private String component;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="error_code_id")
    private GIErrorCode errorCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="event_time")
    private Date eventTime;
	
	@Column(name="exception")
    private String exception;
	
	@Column(name="exception_stack_trace")
    private String exceptionStackTrace;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private AccountUser user;

	@Column(name="URL")
	private String url;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimeStamp;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public GIErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(GIErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getExceptionStackTrace() {
		return exceptionStackTrace;
	}

	public void setExceptionStackTrace(String exceptionStackTrace) {
		this.exceptionStackTrace = exceptionStackTrace;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	@PrePersist
	public void prePersist()
	{
		Date now = new TSDate();
		this.setCreationTimeStamp(now);
	}

	@Override
	public String toString() {
		return "GIMonitor [id=" + id + ", component=" + component + ", GIErrorCode=" + errorCode + ", eventTime=" + eventTime
				+ "user=" + user + ", exception=" + exception + ", exceptionStackTrace=" + exceptionStackTrace + ", " 
				+ "]";
	}
}
