package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity 
@Table(name="permissions")
public class Permission implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static enum DefaultFlag{
		Y,N
	};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Permission_Seq")
	@SequenceGenerator(name = "Permission_Seq", sequenceName = "permissions_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name="name",unique = true,nullable = false)
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="IS_DEFAULT")
	@Enumerated(EnumType.STRING)
	private DefaultFlag isDefault; 
	
	public Permission(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DefaultFlag getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(DefaultFlag isDefault) {
		this.isDefault = isDefault;
	}
}