package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * This class (DTO) will hold the goals column data from CAP_AGENT table
 * @author hardas_d
 *
 */
public class CapAgentGoalsDto implements Serializable {

	private static final long serialVersionUID = -6138164472969500922L;

	private String majorMedical;
	private String accidental;
	private String ancillary;

	public String getMajorMedical() {
		return majorMedical;
	}

	public void setMajorMedical(String majorMedical) {
		this.majorMedical = majorMedical;
	}

	public String getAccidental() {
		return accidental;
	}

	public void setAccidental(String accidental) {
		this.accidental = accidental;
	}

	public String getAncillary() {
		return ancillary;
	}

	public void setAncillary(String ancillary) {
		this.ancillary = ancillary;
	}
}
