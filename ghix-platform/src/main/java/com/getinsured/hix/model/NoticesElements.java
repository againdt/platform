package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="notices_elements")
public class NoticesElements implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NoticesElements_Id_Seq")
	@SequenceGenerator(name = "NoticesElements_Id_Seq", sequenceName = "notices_elements_id_seq", allocationSize = 1)
	private Integer id;

	@Column(name="internal_name", nullable=false)
	private String internalName;

	@Column(name="friendly_label", nullable=false)
	private String friendlyLabel;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public String getFriendlyLabel() {
		return friendlyLabel;
	}

	public void setFriendlyLabel(String friendlyLabel) {
		this.friendlyLabel = friendlyLabel;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
	}
}
