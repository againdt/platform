package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;



@Audited
@Entity
@Table(name="user_roles", uniqueConstraints= @UniqueConstraint(columnNames={"role_id", "user_id","default_role"}))
public class UserRole implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserRole_Seq")
	@SequenceGenerator(name = "UserRole_Seq", sequenceName = "user_roles_seq", allocationSize = 1)
	private int id;

	//uni-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name="role_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Role role;

  //uni-directional many-to-one association to Role
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name="user_id")
    @JsonBackReference
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private AccountUser user;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="CREATION_TIMESTAMP")
	private Date created;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;

    @Column(name="default_role")
	private char roleFlag;

    @Column(name="Is_Active")
	private char isActive;

    public UserRole(){

    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public char getRoleFlag() {
		return roleFlag;
	}

	public void setRoleFlag(char defaultRole) {
		this.roleFlag = defaultRole;
	}

	/**
	 * Returns true if the role is default_role for the user.
	 * Created to support Switch User Role
	 * @author venkata_tadepalli
	 * @since 11/29/2012
	 */
	@JsonIgnore
	public boolean isDefaultRole() {
		return (this.roleFlag=='Y') ? true : false ;
	}


	public char getIsActive() {
		return isActive;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	@JsonIgnore
	public boolean isActiveUserRole() {
		return (this.isActive=='Y') ? true : false ;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public String toString(){
		String userName = this.user.getUsername();
		String roleName = this.role.getName();
		return "[User Role: UserName"+userName+" and Role:"+roleName+"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRole other = (UserRole) obj;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
}
