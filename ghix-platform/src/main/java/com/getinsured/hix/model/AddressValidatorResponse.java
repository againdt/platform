package com.getinsured.hix.model;

import java.util.List;


/**
 * This represents Address Validator Response.
 * 
 * @author polimetla_b
 * 
 */
public class AddressValidatorResponse extends GHIXResponse {

	List<Location> list = null;
	String validatorName;

	public List<Location> getList() {
		return list;
	}

	public void setList(List<Location> list) {
		this.list = list;
	}

	public String getValidatorName() {
		return validatorName;
	}

	public void setValidatorName(String validatorName) {
		this.validatorName = validatorName;
	}

}
