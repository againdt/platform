package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


/**
 * The persistent class for the LOOKUP_TYPE database table.
 * 
 */

@Audited 
@Entity
@Table(name="lookup_type")
public class LookupType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "lookup_type_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lookupType_seq")
	@SequenceGenerator(name = "lookupType_seq", sequenceName = "lookupType_seq", allocationSize = 1)
	private Integer lookupTypeId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "data_type")
	private String dataType;
	
	@Column(name = "description")
	private String description;

	public Integer getLookupTypeId() {
		return lookupTypeId;
	}

	public void setLookupTypeId(Integer lookupTypeId) {
		this.lookupTypeId = lookupTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	/*public List<LookupValue> getLookupValue() {
		return lookupValue;
	}
*/
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public void setLookupValue(List<LookupValue> lookupValue) {
		this.lookupValue = lookupValue;
	}*/

	@Override
	public String toString() {
		return "LookupType [lookupTypeId=" + lookupTypeId 
				+ ", name=" + name 
				+ ", dataType=" + dataType 				
				+ " ,description="+description+" ]";
	}
}
