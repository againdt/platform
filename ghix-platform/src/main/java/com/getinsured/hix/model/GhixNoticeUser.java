package com.getinsured.hix.model;

/**
 * Enumerations used to define User List for Notices
 * @author Ekram Ali Kazi
 * @since 12/08/2012
 *
 *
 */

public enum GhixNoticeUser {

	/**
	 * Please add any value in alphabetic order.
	 */
	ALL("All"),
	ASSISTER("Assister"),
	ASSISTERENROLLMENTENTITYADMIN("AssisterEnrollmentEntityAdmin"),
	ASSISTERENROLLMENTENTITY("AssisterEnrollmentEntity"),
	BROKER("Broker"),
	EMPLOYER("Employer"),
	EMPLOYEE("Employee"),
	ENROLLMENT("Enrollment"),
	INDIVIDUAL("Individual"),
	ISSUER("Issuer"),
	BATCH("Batch");
	

	private String roleName;

	/**
	 * @param roleName
	 */
	private GhixNoticeUser(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

}
