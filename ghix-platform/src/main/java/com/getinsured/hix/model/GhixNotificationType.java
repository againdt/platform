package com.getinsured.hix.model;
public enum GhixNotificationType {
	
	/**
	 * Please add any value in alphabetic order.
	 */
	EMAIL("Email"),PDF("PDF");

	private String code;

	/**
	 * @param code
	 */
	private GhixNotificationType(String code) {
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}



}