package com.getinsured.hix.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.code.springcryptoutils.core.cipher.asymmetric.Base64EncodedCipherer;

@Component
public class VimoEncryptor {
	
	@Autowired 	private Base64EncodedCipherer encrypter;
	@Autowired 	private Base64EncodedCipherer decrypter;
 
	 public String encrypt(String data) {
		 return encrypter.encrypt(data);
	 }
	 
	 public String decrypt(String data) {
		 return decrypter.encrypt(data);
	 }
}