/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ravi_v
 *
 */
@Entity
@Table(name="securable_targets")
public class SecurableTarget implements Serializable{

 
	public enum TargetName {
		ENROLLMENT, CMR, USER, APPLICATION
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SECURABLE_TARGETS_SEQ")
	@SequenceGenerator(name = "SECURABLE_TARGETS_SEQ", sequenceName = "SECURABLE_TARGETS_SEQ", allocationSize = 1)
	private Integer id;
	
	
	@Column(name = "target_id")
	private Integer targetId;
	
	@Column(name = "target_name")
	@Enumerated(EnumType.STRING)
	private TargetName targetName;
	
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp")
	private Date created;
	
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date updated;
    
	//Column to store CREATED_BY 
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	//Column to store LAST_UPDATED_BY 
	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

    // bi-directional many-to-one association to EmployerDetails
 	@OneToMany(mappedBy = "securableTarget", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
 	private List<Securables> securables;
	
	/**
	 * 
	 */
	public SecurableTarget() {
		// TODO Auto-generated constructor stub
	}
	
	public List<Securables> getSecurables() {
		return securables;
	}
	public void setSecurables(List<Securables> securables) {
		this.securables = securables;
	}


	public TargetName getTargetName() {
		return targetName;
	}




	public void setTargetName(TargetName targetName) {
		this.targetName = targetName;
	}




	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}


	public Integer getTargetId() {
		return targetId;
	}
	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
