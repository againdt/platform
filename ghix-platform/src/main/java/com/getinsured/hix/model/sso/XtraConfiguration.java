package com.getinsured.hix.model.sso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Extra SSO Configuration.
 *
 * @author Yevgen Golubenko
 * @since 7/25/17
 */
public class XtraConfiguration implements Serializable
{
  private static final long serialVersionUID = -630231974463712333L;

  private String claimToAccountUserMap = null;
  private IdentityManagement identityManagement = new IdentityManagement();
  private List<String> accertionConsumerUrls = new ArrayList<>();

  public String getClaimToAccountUserMap()
  {
    return claimToAccountUserMap;
  }

  public void setClaimToAccountUserMap(String claimToAccountUserMap)
  {
    this.claimToAccountUserMap = claimToAccountUserMap;
  }

  public IdentityManagement getIdentityManagement()
  {
    return identityManagement;
  }

  public void setIdentityManagement(IdentityManagement identityManagement)
  {
    this.identityManagement = identityManagement;
  }

  public List<String> getAccertionConsumerUrls()
  {
    return accertionConsumerUrls;
  }

  public void setAccertionConsumerUrls(List<String> accertionConsumerUrls)
  {
    this.accertionConsumerUrls = accertionConsumerUrls;
  }
}
