package com.getinsured.hix.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the tax_year database table.
 * 
 */

@Entity
@Table(name="tax_year")
public class TaxYear implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TaxYear_Seq")
	@SequenceGenerator(name = "TaxYear_Seq", sequenceName = "tax_year_seq", allocationSize = 1)
	private int id;
	
	@Column(name="year_of_effective_date", length=10)
	private String yearOfEffectiveDate;
	
	@Column(name="tax_due_date", length=50)
	private String taxDueDate;
	
	@Column(name="tax_due_year", length=10)
	private String taxDueYear;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getYearOfEffectiveDate() {
		return yearOfEffectiveDate;
	}

	public void setYearOfEffectiveDate(String yearOfEffectiveDate) {
		this.yearOfEffectiveDate = yearOfEffectiveDate;
	}

	public String getTaxDueDate() {
		return taxDueDate;
	}

	public void setTaxDueDate(String taxDueDate) {
		this.taxDueDate = taxDueDate;
	}

	public String getTaxDueYear() {
		return taxDueYear;
	}

	public void setTaxDueYear(String taxDueYear) {
		this.taxDueYear = taxDueYear;
	}
	
}