package com.getinsured.hix.model;
public enum GhixLanguage {
	US_EN("English", "US_en");

	private String language;
	private String code1;

	/**
	 * @param language
	 */
	private GhixLanguage(String language, String code1) {
		this.language = language;
		this.code1 = code1;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @return the code1. This will be useful for integrations like ADOBE LIVECYCLE with Accenture.
	 */
	public String getCode1() {
		return code1;
	}


}