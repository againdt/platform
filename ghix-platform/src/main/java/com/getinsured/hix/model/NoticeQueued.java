package com.getinsured.hix.model;

import com.getinsured.timeshift.sql.TSTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name="notices_queued")
public class NoticeQueued implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum QueuedStatus {ERROR, PROCESSED, STAGED, SUPPRESSED}

    public enum TableNames {CMR_HOUSEHOLD, SSAP_APPLICATIONS}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NOTICES_QUEUED_SEQ")
    @SequenceGenerator(name = "NOTICES_QUEUED_SEQ", sequenceName = "NOTICES_QUEUED_SEQ", allocationSize = 1)
    private long id;

    @ManyToOne
    @JoinColumn(name = "NOTICE_TYPE_ID", nullable = false)
    private NoticeType noticeType;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private QueuedStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "TABLE_NAME")
    private TableNames tableName;

    @Column(name = "COLUMN_NAME")
    private String columnName;

    @Column(name = "COLUMN_VALUE")
    private Long columnValue;

    @Column(name = "PROCESSING_DATE")
    private Timestamp processingDate;

    @Column(name = "CREATION_TIMESTAMP")
    private Timestamp creationTimestamp;

    @Column(name = "LAST_UPDATE_TIMESTAMP")
    private Timestamp lastUpdateTimestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public NoticeType getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(NoticeType noticeType) {
        this.noticeType = noticeType;
    }

    public QueuedStatus getStatus() {
        return status;
    }

    public void setStatus(QueuedStatus status) {
        this.status = status;
    }

    public TableNames getTableName() {
        return tableName;
    }

    public void setTableName(TableNames tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Long getColumnValue() {
        return columnValue;
    }

    public void setColumnValue(Long columnValue) {
        this.columnValue = columnValue;
    }

    public Timestamp getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(Timestamp processingDate) {
        this.processingDate = processingDate;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Timestamp getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    /* To AutoUpdate created and updated dates while persisting object */
    @PrePersist
    public void PrePersist() {
        this.setCreationTimestamp(new TSTimestamp());
        this.setLastUpdateTimestamp(new TSTimestamp());
    }

    /* To AutoUpdate updated dates while updating object */
    @PreUpdate
    public void PreUpdate() {
        this.setLastUpdateTimestamp(new TSTimestamp());
    }
}
