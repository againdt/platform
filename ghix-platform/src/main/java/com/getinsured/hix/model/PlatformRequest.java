package com.getinsured.hix.model;

import java.util.HashMap;

public class PlatformRequest {

	private Location address;

	private AdobeLiveCycleRequest adobeLiveCycleRequest;

	private ECMRequest ecmRequest;

	public class ECMRequest{

		private String docId;
		private HashMap<String, String> metadata;
		private byte[] bytes;

		public String getDocId() {
			return docId;
		}

		public void setDocId(String docId) {
			this.docId = docId;
		}

		public byte[] getBytes() {
			return bytes;
		}

		public void setBytes(byte[] bytes) {
			this.bytes = bytes;
		}

		public HashMap<String, String> getMetadata() {
			return metadata;
		}

		public void setMetadata(HashMap<String, String> metadata) {
			this.metadata = metadata;
		}
	}

	public AdobeLiveCycleRequest getAdobeLiveCycleRequest() {
		return adobeLiveCycleRequest;
	}

	public void setAdobeLiveCycleRequest(AdobeLiveCycleRequest adobeLiveCycleRequest) {
		this.adobeLiveCycleRequest = adobeLiveCycleRequest;
	}


	public Location getAddress() {
		return address;
	}

	public void setAddress(Location address) {
		this.address = address;
	}


	public ECMRequest getEcmRequest() {
		return ecmRequest;
	}

	public void setEcmRequest(ECMRequest ecmRequest) {
		this.ecmRequest = ecmRequest;
	}


	public class AdobeLiveCycleRequest{
		private HashMap<String, String> tokens;
		private String format;
		private String templateName;

		public String getFormat() {
			return format;
		}
		public void setFormat(String format) {
			this.format = format;
		}
		public String getTemplateName() {
			return templateName;
		}
		public void setTemplateName(String templateName) {
			this.templateName = templateName;
		}
		public HashMap<String, String> getTokens() {
			return tokens;
		}
		public void setTokens(HashMap<String, String> tokens) {
			this.tokens = tokens;
		}
	}


}

