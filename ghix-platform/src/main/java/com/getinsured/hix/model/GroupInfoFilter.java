package com.getinsured.hix.model;

public class GroupInfoFilter {
	private Integer teamId;
	private Integer groupLeadId;
	private Integer teamUserId;
	private Integer groupMinSize;
	private Integer groupMaxSize;
	private boolean sortDirection; 
	private sort sortColumn;
	public enum sort {groupName, leadName, groupSize}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public Integer getGroupLeadId() {
		return groupLeadId;
	}
	public void setGroupLeadId(Integer groupLeadId) {
		this.groupLeadId = groupLeadId;
	}
	public Integer getGroupMinSize() {
		return groupMinSize;
	}
	public void setGroupMinSize(Integer groupMinSize) {
		this.groupMinSize = groupMinSize;
	}
	public Integer getGroupMaxSize() {
		return groupMaxSize;
	}
	public void setGroupMaxSize(Integer groupMaxSize) {
		this.groupMaxSize = groupMaxSize;
	}
	public boolean isSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(boolean sortDirection) {
		this.sortDirection = sortDirection;
	}
	public sort getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(sort sortColumn) {
		this.sortColumn = sortColumn;
	}
	public Integer getTeamUserId() {
		return teamUserId;
	}
	public void setTeamUserId(Integer teamUserId) {
		this.teamUserId = teamUserId;
	}
	
	
	
}
