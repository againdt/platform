package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.ExternalNotice.ExternalNoticesStatus;
import com.getinsured.timeshift.util.TSDate;

import java.util.Date;


@Entity
@IdClass(AudId.class)
@Table(name="notices_external_aud")
public class ExternalNoticeAud implements Serializable, Cloneable {
	
	public ExternalNoticeAud() {
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;

	@Id
	@Column(name = "REV")
	private int rev;
	
	@Column(name = "REVTYPE")
	private int revType;
	
	@Column(name = "ssap_id")
	private int ssapId;
	
	@Column(name = "notice_id")
	private int noticeId;
	
	@Column(name = "external_household_case_id")
	private String externalHouseholdCaseId;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private ExternalNoticesStatus statusType;
	
	@Column(name = "request_payload")
	private String requestPayload;
	
	@Column(name = "response_payload")
	private String responsePayload;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date created;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date updated;
	
	@Column(name = "retry_count")
	private int retryCount;
	
	public int getRev() {
		return rev;
	}


	public void setRev(int rev) {
		this.rev = rev;
	}


	public int getRevType() {
		return revType;
	}


	public void setRevType(int revType) {
		this.revType = revType;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public int getRetryCount() {
		return retryCount;
	}


	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSsapId() {
		return ssapId;
	}


	public void setSsapId(int ssapId) {
		this.ssapId = ssapId;
	}


	public int getNoticeId() {
		return noticeId;
	}


	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}


	public String getExternalHouseholdCaseId() {
		return externalHouseholdCaseId;
	}


	public void setExternalHouseholdCaseId(String externalHouseholdCaseId) {
		this.externalHouseholdCaseId = externalHouseholdCaseId;
	}
	
	public ExternalNoticesStatus getStatusType() {
		return statusType;
	}
	
	public void setStatusType(ExternalNoticesStatus statusType) {
		this.statusType = statusType;
	}
	
	public String getRequestPayload() {
		return requestPayload;
	}


	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}


	public String getResponsePayload() {
		return responsePayload;
	}


	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(Date created) {
		this.created = created;
	}


	public Date getUpdated() {
		return updated;
	}


	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	
	public ExternalNoticeAud(int id, int ssapId, int noticeId, String externalHouseholdCaseId,
			String statusType, String requestPayload, String responsePayload, Date created,
			Date updated) {
		this.id = id;
		this.ssapId = ssapId;
		this.noticeId = noticeId;
		this.externalHouseholdCaseId = externalHouseholdCaseId;
		this.statusType = ExternalNoticesStatus.valueOf(statusType);
		this.requestPayload = requestPayload;
		this.responsePayload = responsePayload;
		this.created = created;
		this.updated = updated;
	}

}
