package com.getinsured.hix.model;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.convert.converter.Converter;

import com.getinsured.hix.platform.util.GhixUtils;
import com.google.gson.Gson;


public class String2ConfigurationDTO implements Converter<String, ConfigurationDTO> {
	public static final String2ConfigurationDTO Current = new String2ConfigurationDTO();
	
	@Override
	public ConfigurationDTO convert(String configuration) {
		if (StringUtils.isEmpty(configuration)) {
			return new ConfigurationDTO();
		}

		final Gson gson = GhixUtils.platformGson();

		return gson.fromJson(configuration, ConfigurationDTO.class);
	}

	public String reverseConvert(ConfigurationDTO configurationDTO) {
		if (configurationDTO == null) {
			return null;
		}

		final Gson gson = GhixUtils.platformGson();

		return gson.toJson(configurationDTO);
	}
}
