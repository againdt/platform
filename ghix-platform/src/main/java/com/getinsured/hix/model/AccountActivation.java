package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;



/**
 * Entity to capture ACCOUNT_ACTIVATION table data.
 * This entity plays a role to capture created and creator object details for creator-created flow.
 * 		example: Employer-Employee, Admin-Broker, etc..
 * 
 * @author Ekram Ali Kazi
 *
 */
@Entity
@Table(name="ACCOUNT_ACTIVATION")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class AccountActivation implements Serializable {

	private static final long serialVersionUID = -4606994180377052158L;

	public enum STATUS { PROCESSED, NOTPROCESSED; }

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNT_ACTIVATION_SEQ")
	@SequenceGenerator(name = "ACCOUNT_ACTIVATION_SEQ", sequenceName = "ACCOUNT_ACTIVATION_SEQ", allocationSize = 1)
	private int id;

	@Column(name="CREATED_OBJECT_ID")
	private Integer createdObjectId;

	@Column(name ="CREATED_OBJECT_TYPE")
	private String createdObjectType;

	@Column(name="CREATOR_OBJECT_ID")
	private Integer creatorObjectId;

	@Column(name ="CREATOR_OBJECT_TYPE")
	private String creatorObjectType;

	@Column(name="TOKEN" ,length=4000)
	private String activationToken;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TOKEN_EXPIRATION")
	private Date activationTokenExpirationDate;

	@Column(name="ACTIVATION_CODE" ,length=8)
	private String activationCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ACTIVATION_CODE_EXPIRATION")
	private Date activationCodeExpirationDate;

	@Column(name="JSON_STRING" ,length=4000)
	private String jsonString;

	@Column(name="LAST_VISITED_URL")
	private String lastVisitedUrl;

	@Column(name="STATUS")
	@Enumerated(EnumType.STRING)
	private STATUS status;

	@Column(name="USERNAME")
	private String username;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "SENT_DATE")
	private Date sentDate;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getCreatedObjectId() {
		return createdObjectId;
	}

	public void setCreatedObjectId(Integer createdObjectId) {
		this.createdObjectId = createdObjectId;
	}

	public String getCreatedObjectType() {
		return createdObjectType;
	}

	public void setCreatedObjectType(String createdObjectType) {
		this.createdObjectType = createdObjectType;
	}

	public Integer getCreatorObjectId() {
		return creatorObjectId;
	}

	public void setCreatorObjectId(Integer creatorObjectId) {
		this.creatorObjectId = creatorObjectId;
	}

	public String getCreatorObjectType() {
		return creatorObjectType;
	}

	public void setCreatorObjectType(String creatorObjectType) {
		this.creatorObjectType = creatorObjectType;
	}

	public String getActivationToken() {
		return activationToken;
	}

	public void setActivationToken(String activationToken) {
		this.activationToken = activationToken;
	}

	public Date getActivationTokenExpirationDate() {
		return activationTokenExpirationDate;
	}

	public void setActivationTokenExpirationDate(Date activationTokenExpirationDate) {
		this.activationTokenExpirationDate = activationTokenExpirationDate;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public Date getActivationCodeExpirationDate() {
		return activationCodeExpirationDate;
	}

	public void setActivationCodeExpirationDate(Date activationCodeExpirationDate) {
		this.activationCodeExpirationDate = activationCodeExpirationDate;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public String getLastVisitedUrl() {
		return lastVisitedUrl;
	}

	public void setLastVisitedUrl(String lastVisitedUrl) {
		this.lastVisitedUrl = lastVisitedUrl;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		return "AccountActivation [id=" + id + ", createdObjectId="
				+ createdObjectId + ", createdObjectType=" + createdObjectType
				+ ", creatorObjectId=" + creatorObjectId
				+ ", creatorObjectType=" + creatorObjectType
				+ ", activationToken=" + activationToken
				+ ", activationTokenExpirationDate="
				+ activationTokenExpirationDate + ", activationCode="
				+ activationCode + ", activationCodeExpirationDate="
				+ activationCodeExpirationDate + ", jsonString=" + jsonString
				+ ", lastVisitedUrl=" + lastVisitedUrl + ", status=" + status
				+ ", username=" + username + "]";
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
}
