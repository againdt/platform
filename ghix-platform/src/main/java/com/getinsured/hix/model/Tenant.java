package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.log4j.Logger;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.getinsured.hix.platform.service.TenantRedisService;
import com.getinsured.hix.platform.util.ModuleContextProvider;
import com.getinsured.hix.platform.util.SpringApplicationContext;

/**
 * Model class for TENANT table.
 * 
 * @author Bhavin Parmar
 * @since 15 Oct, 2013
 */
@Entity
@Table(name = "TENANT")
@DynamicInsert
@DynamicUpdate
public class Tenant implements Serializable {

	private static final long serialVersionUID = -7446024228018700317L;
	
	private static final Logger LOGGER = Logger.getLogger(Tenant.class);
	
	private static final boolean isRedisEnabled = "on".equalsIgnoreCase(System.getProperty("redis.enabled"));

	public enum IS_ACTIVE {
		// Y-Yes, N-No
		Y, N;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TENANT_SEQ")
	@SequenceGenerator(name = "TENANT_SEQ", sequenceName = "TENANT_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "CODE")
	private String code;

	@Column(name = "NAME")
	private String name;

	@Column(name = "IS_ACTIVE")
	@Enumerated(EnumType.STRING)
	private IS_ACTIVE isActive;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

	@Column(name = "CONFIGURATION")
	private String configuration;
	
	@Column(name = "URL")
	private String url;
	
	@Column(name = "IS_DEMO")
	private String testTenant;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id", referencedColumnName = "tenant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	private TenantSSOConfiguration tenantSSOConfiguration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IS_ACTIVE getIsActive() {
		return isActive;
	}

	public void setIsActive(IS_ACTIVE isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTestTenant() {
		return testTenant;
	}

	public void setTestTenant(String testTenant) {
		this.testTenant = testTenant;
	}

	public TenantSSOConfiguration getTenantSSOConfiguration()
	{
		return tenantSSOConfiguration;
	}

	public void setTenantSSOConfiguration(TenantSSOConfiguration tenantSSOConfiguration)
	{
		this.tenantSSOConfiguration = tenantSSOConfiguration;
	}

	@PostUpdate
    @PostPersist
    public void onPostPersist() {
    	if(isRedisEnabled && this.getUrl() != null) {
    		try {
    			TenantRedisService tenantRedisService = (TenantRedisService) GHIXApplicationContext.getBean("tenantRedisService");
    			tenantRedisService.set(this.getUrl(), Tenant2TenantDTO.Current.convert(this));
    		} catch (Exception e) {
    			LOGGER.error("Error caching tenant in redis", e);
    		}
    	}
    }
}
