package com.getinsured.hix.model;

import org.springframework.core.convert.converter.Converter;

public class Tenant2TenantDTO implements Converter<Tenant, TenantDTO> {

	public static final Tenant2TenantDTO Current = new Tenant2TenantDTO();
	
	@Override
	public TenantDTO convert(Tenant tenant) {
		
		if (tenant == null) {
			return null;
		}

		TenantDTO tenantDTO = new TenantDTO();
		tenantDTO.setCode(tenant.getCode());
		tenantDTO.setUrl(tenant.getUrl());
		tenantDTO.setName(tenant.getName());
		tenantDTO.setConfiguration(String2ConfigurationDTO.Current.convert(tenant.getConfiguration()));
		tenantDTO.setIsActive(tenant.getIsActive().name());
		tenantDTO.setCreatedDate(tenant.getCreatedDate());
		tenantDTO.setId(tenant.getId());
		tenantDTO.setTestTenant(tenant.getTestTenant());
		tenantDTO.setTenantSSOConfiguration(tenant.getTenantSSOConfiguration());

		return tenantDTO;
	}

	public Tenant reverseConvert(TenantDTO tenantDTO) {
		
		if (tenantDTO == null) {
			return null;
		}

		Tenant tenant = new Tenant();
		tenant.setCode(tenantDTO.getCode());
		tenant.setUrl(tenantDTO.getUrl());
		tenant.setName(tenantDTO.getName());
		tenant.setConfiguration(String2ConfigurationDTO.Current.reverseConvert(tenantDTO.getConfiguration()));
		tenant.setIsActive(Tenant.IS_ACTIVE.valueOf(tenantDTO.getIsActive()));
		tenant.setCreatedDate(tenantDTO.getCreatedDate());
		tenant.setTestTenant(tenantDTO.getTestTenant());
		tenant.setTenantSSOConfiguration(tenantDTO.getTenantSSOConfiguration());
		return tenant;
	}
}
