package com.getinsured.hix.model;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;


/**
 * This is the base request object. Each request
 * must have appID, appPassword.
 * 
 * @author jyotisree
 * 
 */
public class GHIXRequest {
private String appID; //planSelection
private String appPassword; //password123
private String loggedInUserName;
private Calendar requestTime = TSCalendar.getInstance();

public String getAppID() {
	return appID;
}
public void setAppID(String appID) {
	this.appID = appID;
}
public String getAppPassword() {
	return appPassword;
}
public void setAppPassword(String appPassword) {
	this.appPassword = appPassword;
}
public Calendar getRequestTime() {
	return requestTime;
}
public void setRequestTime(Calendar requestTime) {
	this.requestTime = requestTime;
}
public String getLoggedInUserName() {
	return loggedInUserName;
}
public void setLoggedInUserName(String loggedInUserName) {
	this.loggedInUserName = loggedInUserName;
}


}
