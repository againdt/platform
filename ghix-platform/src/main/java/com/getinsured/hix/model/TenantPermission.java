package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Entity
@Table(name="TENANT_PERMISSIONS")
public class TenantPermission implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tenant_permission_seq")
	@SequenceGenerator(sequenceName = "TENANT_PERMISSIONS_SEQ", name = "tenant_permission_seq")
	private long id;
	
	
	@ManyToOne
	@JoinColumn(name="PERMISSION_ID")
	private Permission permission;
	
	@Column(name="TENANT_ID")
	private Long tenantId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdatedTimestamp;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATED_BY")
	private AccountUser createdBy;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser lastUpdatedBy;
	
	@Column(name="IS_ACTIVE")
	@Enumerated(EnumType.STRING)
	private ActiveFlag isActive;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenant(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(AccountUser lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public ActiveFlag getIsActive() {
		return isActive;
	}

	public void setIsActive(ActiveFlag isActive) {
		this.isActive = isActive;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	

}
