package com.getinsured.hix.model;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * This component can be autowired where needed and beans can be retrieved from the bean factory by
 * various methods.
 * <p>
 *   Example usage:
 *   <pre><code>
 *  {@literal @}Service
 *   public class SomeService {
 *    {@literal @}Autowired
 *     private GHIXApplicationContext gHIXApplicationContext;
 *
 *     public void castNotRequired() {
 *       Gson gson = gHIXApplicationContext.getBean("platformGson", Gson.class);
 *       // do something with gson object.
 *     }
 *
 *     public void castRequired() {
 *     	Gson gson = (Gson) gHIXApplicationContext.getBean("platformGson");
 *     	// do something with gson
 *     }
 *
 *     public void nonUniqueBeanHandling() {
 *       Gson gson = null;
 *       try
 *       {
 *          // Assuming @Bean public Gson a() { ... } and @Bean public Gson b() { ... } defined elsewhere
 *          gson = gHIXApplicationContext.getBean(Gson.class);
 *          // only 1 bean of type Gson.class in factory, do something with gson
 *       }
 *       catch(NoSuchBeanDefinitionException e) {
 *         // There is more than 1 bean of Gson.class defined in bean factory
 *         // TIP: try doing it like castNotRequired() method.
 *       }
 *     }
 *   }
 *   </code>
 *   </pre>
 * </p>
 */
@Component("gHIXApplicationContext")
public class GHIXApplicationContext implements ApplicationContextAware
{
	private static ApplicationContext CONTEXT;
  private static final Logger log = LoggerFactory.getLogger(GHIXApplicationContext.class);

	/**
	 * This method is called from within the ApplicationContext once it is 
	 * done starting up, it will stick a reference to itself into this bean.
	 * @param context a reference to the ApplicationContext.
	 */
	@Override
  public void setApplicationContext(ApplicationContext context) throws BeansException
  {
    if(log.isInfoEnabled())
    {
      log.info("Setting application context in static field");
    }

		CONTEXT = context;
	}

	/**
	 * This is about the same as context.getBean("beanName"), except it has its
	 * own static handle to the Spring context, so calling this method statically
	 * will give access to the beans by name in the Spring application context.
	 * As in the context.getBean("beanName") call, the caller must cast to the
   * appropriate target class. If context is not initialized, then a Runtime error
	 * will be thrown.
   *
	 * @param beanName the name of the bean to get.
	 * @return an Object reference to the named bean.
	 */
  public static Object getBean(String beanName)
  {
    validateContext(beanName);
		return CONTEXT.getBean(beanName);
	}

  public static <T> T getBean(Class<T> requiredType)
  {
    validateContext((requiredType != null) ? requiredType.getName() : "<null>");
    return CONTEXT.getBean(requiredType);
  }

  /**
   * Use this to retrive bean by name of required type. Doesn't need casting upon return.
   *
   * @param name name of the bean.
   * @param requiredType type of the bean to cast to.
   * @param <T> class type
   * @return bean class.
   */
  public static <T> T getBean(String name, Class<T> requiredType)
  {
    validateContext(name);
    return CONTEXT.getBean(name, requiredType);
  }

  private static void validateContext(String beanName) throws GIRuntimeException
  {
    if (CONTEXT == null)
    {
      throw new GIRuntimeException("Application Context is not yet initialized. Bean with name: " + beanName + " is not available." +
          "See documentation of GHIXApplicationContext for examples");
    }
  }
}
