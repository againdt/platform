package com.getinsured.hix.model;

import java.util.HashMap;
import java.util.Map;

/**
 * This represents response object for Address Book table.
 * 
 * @author polimetla_b, talreja_n
 * @since 11/21/2012
 */

public class InboxUserAddressBookResp extends GHIXResponse{
	
	/*
	 * This map holds the contacts for a user's address book
	 * Long - recipient's Id
	 * String - Full Name of recepient 
	 */
	private Map<Long, String> addressBook;
	
	public InboxUserAddressBookResp(){
		addressBook = new HashMap<Long, String>();
	}

	public Map<Long, String> getAddressBook() {
		return addressBook;
	}

	public void setAddressBook(Map<Long, String> addressBook) {
		this.addressBook = addressBook;
	}
	
	
}
