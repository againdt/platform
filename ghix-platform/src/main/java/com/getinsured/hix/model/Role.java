package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the roles database table.
 *
 */
@Entity
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = " id in (select tr.role_id from tenant_roles tr where tr.TENANT_ID = :tenantId and tr.IS_ACTIVE = 'Y')") })
@Table(name="roles")
public class Role implements Serializable, Comparable<Role>, JSONAware {
	private static final long serialVersionUID = 1L;
	
	public enum IS_DEFAULT{
		Y,N
	};
	public enum IS_EXCLUSIVE{
		Y,N
	};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Role_Seq")
	@SequenceGenerator(name = "Role_Seq", sequenceName = "roles_seq", allocationSize = 1)
	private Integer id;

	@Audited
	private String name;

	@Audited
	private String description;

	@Audited
	@Column(name="landing_page")
	private String landingPage;

	@Column(name="post_reg_url")
	private String postRegistrationUrl;


	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date created;

    @Audited
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;

    @JsonManagedReference
    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
 	private Set<RolePermission> rolePermissions = new HashSet<RolePermission>();

    //HIX-27186 : DB Schema: Add role label to role table
    @Audited
	private String label;

    @Audited
	private int privileged;

    @Audited
    @Column(name="max_retry_count")
	private int maxRetryCount;

    @Audited
    @Column(name="login_restricted")
	private int loginRestricted;
    
    @Column(name="IS_TENANT_DEFAULT")
    @Enumerated(EnumType.STRING)
    private IS_DEFAULT isTenantDefault;
    
    @Column(name="MFA_ENABLED")
    private String mfaEnabled;
    
    @Column(name="IS_EXCLUSIVE")
    @Enumerated(EnumType.STRING)
    private IS_EXCLUSIVE isExclusive;
    
    @Column(name="PWD_CONSTRAINTS_ENABLED")
    private char pwdConstraintsEnabled;
    
    @Column(name="PASSWORD_EXPIRY_PERIOD")
    private int passwordExpiryPeriod;
    
    @Column(name="ACCOUNT_IDLE_PERIOD")
    private int accountIdlePeriod;
    
	public Role() {
    }

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLandingPage() {
		return this.landingPage;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}


	public String getPostRegistrationUrl() {
		return postRegistrationUrl;
	}

	public void setPostRegistrationUrl(String postRegistrationUrl) {
		this.postRegistrationUrl = postRegistrationUrl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public Set<RolePermission> getRolePermissions() {
		return rolePermissions;
	}

	public void setRolePermissions(Set<RolePermission> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}
	//HIX-27186 : DB Schema: Add role label to role table
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getPrivileged() {
		return privileged;
	}

	public void setPrivileged(int privileged) {
		this.privileged = privileged;
	}

	public int getMaxRetryCount() {
		return maxRetryCount;
	}

	public void setMaxRetryCount(int maxRetryCount) {
		this.maxRetryCount = maxRetryCount;
	}
	
	public int getLoginRestricted() {
		return loginRestricted;
	}
	
	public void setLoginRestricted(int loginRestricted) {
		this.loginRestricted = loginRestricted;
	}
	
	

	public IS_DEFAULT getIsTenantDefault() {
		return isTenantDefault;
	}

	public void setIsTenantDefault(IS_DEFAULT isTenantDefault) {
		this.isTenantDefault = isTenantDefault;
	}

	public String getMfaEnabled() {
		return mfaEnabled;
	}

	public void setMfaEnabled(String mfaEnabled) {
		this.mfaEnabled = mfaEnabled;
	}

	public IS_EXCLUSIVE getIsExclusive() {
		return isExclusive;
	}

	public void setIsExclusive(IS_EXCLUSIVE isExclusive) {
		this.isExclusive = isExclusive;
	}

	public int getPasswordExpiryPeriod() {
		return passwordExpiryPeriod;
	}

	public void setPasswordExpiryPeriod(int passwordExpiryPeriod) {
		this.passwordExpiryPeriod = passwordExpiryPeriod;
	}

	public int getAccountIdlePeriod() {
		return accountIdlePeriod;
	}

	public void setAccountIdlePeriod(int accountIdlePeriod) {
		this.accountIdlePeriod = accountIdlePeriod;
	}

	public char getPwdConstraintsEnabled() {
		return pwdConstraintsEnabled;
	}

	public void setPwdConstraintsEnabled(char pwdConstraintsEnabled) {
		this.pwdConstraintsEnabled = pwdConstraintsEnabled;
	}

	@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Role other = (Role) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		public String toString(){
			return "Role:"+this.getName();
	}

		@Override
		public int compareTo(Role o) {
			// TODO Auto-generated method stub
			return this.name.compareTo(o.getName());
		}

		@SuppressWarnings("unchecked")
		@Override
		public String toJSONString() {
			JSONObject obj = new JSONObject();
			obj.put("name", this.name);
			obj.put("label", this.label);
			obj.put("privileged",this.getPrivileged());
			return obj.toJSONString();
		}

}
