package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * This represents one message in inbox_msg table.
 * 
 * @author polimetla_b, Nikhil Talreja
 * @since 11/16/2012
 * 
 */

@Entity
@Table(name="INBOX_MSG")
@DynamicUpdate
@DynamicInsert
public class InboxMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3394165259716843526L;
	
	public enum STATUS{
		//N-New, P-Replied, F-Forwarded, A-Archived, R-Read, S-Sent, D-Deleted, C-Compose
		N,P,F,A,R,S,D,C;
	}
	
	public enum PRIORITY{
		//H-High, M-Medium, L-Low
		H, M,L;
	}
	
	public enum TYPE{
		//M-Message, N-Notification, A-Alert
		M, N, A;
	}
	public enum CLOB_USED{
		//Y-Yes, N-NO
		Y,N;
	}
	public enum CONTENT_TYPE{
		//T-Text, H-HTML
		T,H;
	}
	public enum EMAIL_SENT_STATUS{
		//Y-Yes, N-No, A-Not Applicable
		Y,N,A;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INBOX_MSG_SEQ")
	@SequenceGenerator(name = "INBOX_MSG_SEQ", sequenceName = "INBOX_MSG_SEQ", allocationSize = 1)
	private long id;
	
	//Message Owner ID
	@Column(name="OWNER_USER_ID")
	private long ownerUserId;
	
	@Column(name="OWNER_USER_NAME")
	private String ownerUserName;
	
	//Sender's user Id
	@Column(name="FROM_USER_ID")
	private long fromUserId;
	
	@Column(name="FROM_USER_NAME")
	private String fromUserName;
	
	//Comma seperated list of user ids
	@Column(name="TO_USER_ID_LIST")
	private String toUserIdList;
	
	@Column(name="TO_USER_NAME_LIST")
	private String toUserNameList;
	
	@Column(name="LOCALE")
	private String locale;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP", updatable=false)
	private Date dateCreated;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="DATE_EXPIRE")
	private Date dateExpire;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date dateUpdated;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private STATUS status;
	
	@Column(name = "PRIORITY")
	@Enumerated(EnumType.STRING)
	private PRIORITY priority;
	
	@Column(name = "GI_MESSAGE_TYPE")
	@Enumerated(EnumType.STRING)
	private TYPE type;
	
	@Column(name="MSG_SUB")
	private String msgSub;
	
	@Column(name="MSG_BODY")
	private String msgBody;
	
	@Column(name="CONTENT_TYPE")
	@Enumerated(EnumType.STRING)
	private CONTENT_TYPE contentType;
	
	@Column(name="IS_CLOB_USED")
	@Enumerated(EnumType.STRING)
	private CLOB_USED isClobUsed;
	
	@Column(name="PREVIOUS_MSG_ID")
	private long previousId;
	
	@Column(name="IS_EMAIL_SENT")
	@Enumerated(EnumType.STRING)
	private EMAIL_SENT_STATUS isEmailSent;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="EMAIL_SENT_DATE")
	private Date emailSentDate;
	
	//One to many association with message documents
	//To test using JUnit, make fetch type as EAGER
	//The fetch type should always be EAGER for readMessage to wrok properly
 	@OneToMany(mappedBy = "message", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<InboxMsgDoc> messageDocs;
 	
 	@Column(name="MODULE_ID")
 	private long moduleId;
 	
 	@Column(name="MODULE_NAME")
 	private String moduleName;
 	
 	public InboxMsg(){
 		
 	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getOwnerUserId() {
		return ownerUserId;
	}

	public void setOwnerUserId(long ownerUserId) {
		this.ownerUserId = ownerUserId;
	}

	public String getOwnerUserName() {
		return ownerUserName;
	}

	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}

	public long getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(long fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getToUserIdList() {
		return toUserIdList;
	}

	public void setToUserIdList(String toUserIdList) {
		this.toUserIdList = toUserIdList;
	}

	public String getToUserNameList() {
		return toUserNameList;
	}

	public void setToUserNameList(String toUserNameList) {
		this.toUserNameList = toUserNameList;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateExpire() {
		return dateExpire;
	}

	public void setDateExpire(Date dateExpire) {
		this.dateExpire = dateExpire;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public PRIORITY getPriority() {
		return priority;
	}

	public void setPriority(PRIORITY priority) {
		this.priority = priority;
	}

	public TYPE getType() {
		return type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

	public String getMsgSub() {
		return msgSub;
	}

	public void setMsgSub(String msgSub) {
		this.msgSub = msgSub;
	}

	public String getMsgBody() {
		return msgBody;
	}

	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}

	public CONTENT_TYPE getContentType() {
		return contentType;
	}

	public void setContentType(CONTENT_TYPE contentType) {
		this.contentType = contentType;
	}

	public CLOB_USED getIsClobUsed() {
		return isClobUsed;
	}

	public void setIsClobUsed(CLOB_USED isClobUsed) {
		this.isClobUsed = isClobUsed;
	}

	public long getPreviousId() {
		return previousId;
	}

	public void setPreviousId(long previousId) {
		this.previousId = previousId;
	}

	public EMAIL_SENT_STATUS getIsEmailSent() {
		return isEmailSent;
	}

	public void setIsEmailSent(EMAIL_SENT_STATUS isEmailSent) {
		this.isEmailSent = isEmailSent;
	}

	public Date getEmailSentDate() {
		return emailSentDate;
	}

	public void setEmailSentDate(Date emailSentDate) {
		this.emailSentDate = emailSentDate;
	}

	public List<InboxMsgDoc> getMessageDocs() {
		return messageDocs;
	}

	public void setMessageDocs(List<InboxMsgDoc> messageDocs) {
		this.messageDocs = messageDocs;
	}
	
	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/* 
	 * Over riding the hashCode method
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* 
	 * Over riding the hashCode method
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof InboxMsg)) {
			return false;
		}
		InboxMsg other = (InboxMsg) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	};
	
	/* To AutoUpdate created and updated date while updating object */
	@PrePersist
	public void prePersist(){
		this.setDateCreated(new TSDate());
		this.setDateExpire(new TSDate());
		this.setDateUpdated(new TSDate()); 
		
	}
	
	/* To AutoUpdate updated date while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setDateUpdated(new TSDate()); 
		this.setDateExpire(new TSDate());
		
	}
 	
}
