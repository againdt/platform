package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the roles database table.
 *
 */
@Entity

@Table(name="zipcodes")
public class ZipCode implements Serializable {
	private static final long serialVersionUID = 7238788470266565528L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZipCode_Seq")
	@SequenceGenerator(name = "ZipCode_Seq", sequenceName = "zipcodes_seq", allocationSize = 1)
	private int id;

	@Column(name="zipcode")
	private String zip;

	@Column(name="city")
	private String city;

	@Column(name="city_type")
	private String cityType;

	@Column(name="county")
	private String county;

	@Column(name="county_fips")
	private String countyFips;

	@Column(name="state_name")
	private String stateName;

	@Column(name="state")
	private String state;

	@Column(name="state_fips")
	private String stateFips;

	@Column(name="msa")
	private String msa;

	@Column(name="area_code")
	private String areaCode;

	@Column(name="time_zone")
	private String timeZone;

	@Column(name="gmt_offset")
	private int gmtOffset;

	@Column(name="dst")
	private String dst;

	@Column(name="lattitude")
	private double lat;

	@Column(name="longitude")
	private double lon;

	@Transient
	private String countyCode;
	
	public ZipCode() {}
	
	public ZipCode(String county, String stateFips, String countyFips) {
		this.county = county;
		this.stateFips = stateFips;
		this.countyFips = countyFips;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityType() {
		return cityType;
	}

	public void setCityType(String cityType) {
		this.cityType = cityType;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountyFips() {
		return countyFips;
	}

	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateFips() {
		return stateFips;
	}

	public void setStateFips(String stateFips) {
		this.stateFips = stateFips;
	}

	public String getMsa() {
		return msa;
	}

	public void setMsa(String msa) {
		this.msa = msa;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public int getGmtOffset() {
		return gmtOffset;
	}

	public void setGmtOffset(int gmtOffset) {
		this.gmtOffset = gmtOffset;
	}

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getCountyCode() {
		this.setCountyCode((this.getStateFips()==null?"":this.getStateFips())+(this.getCountyFips()==null?"":this.getCountyFips()));
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	//since there are some records with duplication zipcode, checking id for equals and hashcode
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj){
			return true;
		}
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		// object must be Test at this point
		ZipCode zipCode = (ZipCode)obj;
		return id == zipCode.id;
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 31 * hash + id;

		return hash;
	}

}

