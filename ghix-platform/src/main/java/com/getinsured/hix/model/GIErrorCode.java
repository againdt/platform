package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@Entity
@Table(name="GI_ERROR_CODE")
public class GIErrorCode {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GIERRORCODE_SEQ")
	@SequenceGenerator(name = "GIERRORCODE_SEQ", sequenceName = "GI_ERROR_CODE_SEQ", allocationSize = 1)
	@Column(name = "code_id")
	private Integer id;
	
	@Column(name="error_code")
    private String errorCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="insert_date")
    private Date insertDate;
	
	@Column(name="message")
    private String message;
	
	@Column(name="module_name")
    private String moduleName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GIErrorCode [errorCode=").append(errorCode)
				.append(", insertDate=").append(insertDate)
				.append(", message=").append(message).append(", moduleName=")
				.append(moduleName).append("]");
		return builder.toString();
	}
	

}
