package com.getinsured.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.model.sso.XtraConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

/**
 * Utility methods relevant to Identity Provider.
 *
 * Created by golubenko_y on 9/19/16.
 */
public class SSOIdentityProviderUtils
{
  private static final Logger log = LoggerFactory.getLogger(SSOIdentityProviderUtils.class);
  private static final String GHIX_HOME = GhixHome.getGhixHome();
  private static final String XML_TAG_ENTITYDESCRIPTOR = "EntityDescriptor".intern();
  private static final String ENTITY_ID_ATTRIBUTE = "entityID".intern();
  private static final String DEFAULT_PROPERTIES_MAP = "saml/accountuser-samlattributes.properties";

  private static XMLInputFactory xmlInputFactory = null;

  private static final Map<String, Properties> tenantCodeToAttributeMap = new HashMap<>();
  private static final Method[] accountUserMethods = AccountUser.class.getMethods();

  static
  {
    xmlInputFactory = XMLInputFactory.newFactory();

    if (xmlInputFactory.isPropertySupported("javax.xml.stream.isValidating"))
    {
      xmlInputFactory.setProperty("javax.xml.stream.isValidating", Boolean.FALSE);
    }
  }

  private static XMLInputFactory getXmlInputFactory()
  {
    return xmlInputFactory.newInstance();
  }

  /**
   * Takes IDP File name as parameter and returns entityID attribute value from that file or null.
   *
   * <p>
   *   This method will assume that file is located under {@link GhixHome#getGhixHome()}
   * </p>
   *
   * @param idpFileName file name (e.g. YEVG_idp.xml)
   * @return entity id extracted from the file.
   */
  public static String getEntityIdFromXml(final String idpFileName)
  {
    final File f = new File(GHIX_HOME + File.separatorChar + idpFileName);
    return getEntityIdFromXml(f);
  }

  /**
   * Takes IDP File resource and returns entityID attribute value from that file or null.
   *
   * @param idpFile file resource.
   * @return entity id extracted from given file resource.
   */
  public static String getEntityIdFromXml(final File idpFile)
  {
    if(idpFile == null || !idpFile.isFile() || !idpFile.exists() || !idpFile.canRead())
    {
      if(log.isErrorEnabled())
      {
        if(idpFile != null)
        {
          log.error("Unable to read given XML file: {}. Regular file: {}; Exists: {}; Readable: {}",
              idpFile.getAbsolutePath(), idpFile.isFile(), idpFile.exists(), idpFile.canRead());
        }
        else
        {
          log.error("Unable to read given XML file. Null file given.");
        }
      }
    }
    else
    {
      if(log.isDebugEnabled())
      {
        log.debug("Reading EntityID attribute from given XML file: {}", idpFile.getAbsolutePath());
      }

      final XMLInputFactory factory = getXmlInputFactory();

      XMLStreamReader xmlStreamReader = null;
      InputStream fis = null;

      try
      {
        fis = new FileInputStream(idpFile);

        xmlStreamReader = factory.createXMLStreamReader(fis);

        return getEntityIdFromStreamReader(xmlStreamReader);
      }
      catch(FileNotFoundException | XMLStreamException fnfe)
      {
        if(log.isErrorEnabled())
        {
          log.error("Could not create FileInputStream from file: {}", idpFile.getAbsolutePath(), fnfe);
        }
      }
      finally
      {
        try
        {
          if(xmlStreamReader != null)
          {
            xmlStreamReader.close();
          }

          if(fis != null)
          {
            fis.close();
          }
        }
        catch(Exception e) {
          /* Ignore */
        }
      }
    }

    return null;
  }


  /**
   * Takes XML string and returns entityID attribute value from given XML.
   *
   * @param idpXml XML string.
   * @return entity id extracted from given XML string.
   */
  public static String getEntityIdFromXmlString(final String idpXml)
  {
    final StringReader sr = new StringReader(idpXml);
    XMLStreamReader xmlStreamReader = null;
    XMLInputFactory factory = getXmlInputFactory();

    try
    {
      xmlStreamReader = factory.createXMLStreamReader(sr);
      return getEntityIdFromStreamReader(xmlStreamReader);
    }
    catch (XMLStreamException e)
    {
      if(log.isErrorEnabled())
      {
        log.error("XML Stream exception while reading entityID attribute from given XML string", e);
      }
    }
    finally
    {
      if(xmlStreamReader != null)
      {
        try
        {
          xmlStreamReader.close();
        }
        catch(Exception e)
        {
          /* ignore */
        }
      }
    }

    return null;
  }

  private static String getEntityIdFromStreamReader(XMLStreamReader xmlStreamReader) throws XMLStreamException
  {
    while(xmlStreamReader.hasNext())
    {
      xmlStreamReader.next();

      if(xmlStreamReader.isStartElement())
      {
        final String tagName = xmlStreamReader.getLocalName();

        if(XML_TAG_ENTITYDESCRIPTOR.equals(tagName) ||
            XML_TAG_ENTITYDESCRIPTOR.equalsIgnoreCase(tagName))
        {
          for(int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
          {
            final QName attributeName = xmlStreamReader.getAttributeName(i);
            if(ENTITY_ID_ATTRIBUTE.equals(attributeName.getLocalPart()) ||
                ENTITY_ID_ATTRIBUTE.equalsIgnoreCase(attributeName.getLocalPart()))
            {
              return xmlStreamReader.getAttributeValue(i);
            }
          }
        }
      }
    }

    return null;
  }

  public static AccountUser getAccountUserFromSAMLAttributes(final Map<String, String> attributes)
  {
    final AccountUser accountUser = new AccountUser();
    if(log.isTraceEnabled()){
    	 Set<Entry<String, String>> attrs = attributes.entrySet();
    	 for(Entry<String,String> entry :attrs){
    		 log.trace("Received SAML Assertion: {} with value {}",entry.getKey(),entry.getValue());
    	 }
    }
    if(attributes == null)
    {
      if(log.isWarnEnabled())
      {
        log.warn("Unable to construct AccountUser from given saml attributes: {}", attributes);
      }

      return accountUser;
    }

    TenantDTO tenant = TenantContextHolder.getTenant();

    if(tenant == null)
    {
      if(log.isErrorEnabled())
      {
        log.error("Unable to retrieve current tenant information, tenant is: {}", tenant);
      }

      return accountUser;
    }

//    Properties properties = tenantCodeToAttributeMap.get(tenant.getCode());
//
//    if(properties == null)
//    {
//      properties = getPropertiesFromTenantSSOConfiguration(tenant);
//      tenantCodeToAttributeMap.put(tenant.getCode(), properties);
//    }

    Properties properties = tenantCodeToAttributeMap.computeIfAbsent(tenant.getCode(), p -> getPropertiesFromTenantSSOConfiguration(tenant));

    if(properties == null && log.isErrorEnabled())
    {
      log.error("Unable to retrieve SAML Attribute Mapping Properties: {}", properties);
    }
    else
    {
      properties.forEach((k, v) -> {
        String val = attributes.get(v.toString());
        switch (k.toString())
        {
          case "userName":
            accountUser.setUserName(val);
            break;
          case "firstName":
            accountUser.setFirstName(val);
            break;
          case "lastName":
            accountUser.setLastName(val);
            break;
          case "securityQuestion1":
            accountUser.setSecurityQuestion1(val);
            break;
          case "securityAnswer1":
            accountUser.setSecurityAnswer1(val);
            break;
          case "securityQuestion2":
            accountUser.setSecurityQuestion2(val);
            break;
          case "securityAnswer2":
            accountUser.setSecurityAnswer2(val);
            break;
          case "email":
            accountUser.setEmail(val);
            break;
          case "communicationPref":
            accountUser.setCommunicationPref(val);
            break;
          case "phone":
            accountUser.setPhone(val);
            break;
          case "extnAppUserId":
            accountUser.setExtnAppUserId(val);
            break;
          case "samlPasswdTimeStamp":
            accountUser.setSamlPasswdTimeStamp(val);
            break;
          default:
          {
            if (log.isWarnEnabled())
            {
              log.warn("Properties contains key {} that is not directly mapped to AccountUser, trying to invoke matched methods using reflection to set value: {}", k, val);
            }

            Arrays.stream(accountUserMethods).forEach(method -> {
              if(method.getName().contains("set") &&
                  method.getName().toLowerCase().contains(k.toString().toLowerCase()))
              {
                if(log.isDebugEnabled())
                {
                  log.debug("Found method that potentially matches property key: {} => method name: {}, will try to call it with value: {}", k.toString(), method.getName(), val);
                }

                try
                {
                  method.invoke(accountUser, val);
                }
                catch (IllegalAccessException | InvocationTargetException e)
                {
                  log.error("Exception while dynamically invoking method on accountUser object", e);
                }
              }
            });

            break;
          }
        }
      });
    }

    return accountUser;
  }

  /**
   * Returns {@link Properties} object from configured AccountUser 2 SAML Attributes map.
   *
   * @param tenantDTO {@link TenantDTO} object.
   * @return {@link Properties} file.
   */
  private static Properties getPropertiesFromTenantSSOConfiguration(TenantDTO tenantDTO)
  {
    final Properties properties = new Properties();

    TenantSSOConfiguration ssoConfiguration = tenantDTO.getTenantSSOConfiguration();

    if(ssoConfiguration != null)
    {
      XtraConfiguration xtraConfiguration = ssoConfiguration.getXtraConfiguration();

      if(xtraConfiguration != null && xtraConfiguration.getClaimToAccountUserMap() != null)
      {
        String claimToAccountUserMapString = new String(Base64.getDecoder().decode(xtraConfiguration.getClaimToAccountUserMap()));

        if(log.isDebugEnabled())
        {
          log.debug("Going to use following mapping file to map SAML attributes to AccountUser: {}", claimToAccountUserMapString);
        }
        try
        {
          properties.load(new StringReader(claimToAccountUserMapString));
        }
        catch (IOException e)
        {
          if(log.isErrorEnabled())
          {
            log.error("IOException while loading properties from the string", e);
            log.error("Unable to load properties from the string: {}", claimToAccountUserMapString);
          }
        }
      }
      else
      {
        InputStream inputMap = SSOIdentityProviderUtils.class.getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES_MAP);

        try
        {
          properties.load(inputMap);
        }
        catch (IOException e)
        {
          log.error("IOException while loading properties from input stream", e);
          log.error("Unable to load properties from the file: {}", DEFAULT_PROPERTIES_MAP);
        }
      }
    }

    return properties;
  }
}
