package com.getinsured.platform.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

/**
 * Provides some utility methods to parse WSO2 related XMLs
 *
 * @author Yevgen Golubenko
 * @since 3/7/17
 */
public class Wso2XmlUtil
{
  private static final Logger log = LoggerFactory.getLogger(Wso2XmlUtil.class);

  private static XMLInputFactory xmlInputFactory = null;
  private static final Pattern base64pattern = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$");
  static
  {
    xmlInputFactory = XMLInputFactory.newFactory();

    if (xmlInputFactory.isPropertySupported("javax.xml.stream.isValidating"))
    {
      xmlInputFactory.setProperty("javax.xml.stream.isValidating", Boolean.FALSE);
    }
  }

  private static XMLInputFactory getXmlInputFactory()
  {
    return xmlInputFactory.newInstance();
  }

  /**
   * Returns document's root element.
   *
   * @param xml xml string.
   * @return {@link Element}
   */
  public static Element getDocumentElement(String xml)
  {
    Element element = null;

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    dbf.setIgnoringComments(true);

    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(xml.trim()));

    try
    {
      DocumentBuilder db = dbf.newDocumentBuilder();
      Document doc = db.parse(is);
      element = doc.getDocumentElement();
    }
    catch (Exception e)
    {
      log.error("Unable to get document element from given XML: " + xml, e);
    }

    return element;
  }

  /**
   * Takes given XML as string or as Base64 encoded string and returns attribute value for given tag if it exists.
   * <p>It will return first match</p>.
   *
   * @param xml XML as plain string or as base64 encoded string.
   * @param tag tag containing attribute needed
   * @param attribute attribute name inside of given tag.
   * @return attribute value, first occurrence, or null.
   */
  public static String getAttributeName(String xml, String tag, String attribute)
  {
    assert xml != null       : "xml string must not be null";
    assert tag != null       : "tag must not be null";
    assert attribute != null : "attribute must not be null";
    xml = xml.trim();

    // If there is no space and no <?xml see if it's base64
    if(!xml.contains(" ") || !xml.contains("<?xml"))
    {
      Matcher m = base64pattern.matcher(xml);

      if(m.matches())
      {
        // Probably base64 encoded string passed, lets decode it.
        xml = new String(Base64.getDecoder().decode(xml.getBytes()));
      }
    }

    final XMLInputFactory factory = getXmlInputFactory();
    String value = null;

    XMLStreamReader xmlStreamReader = null;
    InputStream fis = null;

    try
    {
      fis = new ByteArrayInputStream(xml.getBytes());

      xmlStreamReader = factory.createXMLStreamReader(fis);

      value =  parseXml(xmlStreamReader, tag, attribute);
    } catch (XMLStreamException e)
    {
      if (log.isErrorEnabled())
      {
        log.error("Could not create ByteArrayInputStream from xml: {}", xml, e);
      }
    } finally
    {
      try
      {
        if (xmlStreamReader != null)
        {
          xmlStreamReader.close();
        }

        if (fis != null)
        {
          fis.close();
        }
      } catch (Exception e)
      {
          /* Ignore */
      }
    }

    return value;
  }

  private static String parseXml(XMLStreamReader xmlStreamReader, String tag, String attribute) throws XMLStreamException
  {
    String attributeValue = null;

    while (xmlStreamReader.hasNext() && attributeValue == null)
    {
      xmlStreamReader.next();

      if (xmlStreamReader.isStartElement())
      {
        final String tagName = xmlStreamReader.getLocalName();

        if (tag.equals(tagName))
        {
          for (int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
          {
            final String attributeName = xmlStreamReader.getAttributeName(i).getLocalPart();

            if (attribute.equals(attributeName))
            {
              attributeValue = xmlStreamReader.getAttributeValue(i);
              break;
            }
          }
        }
      }
    }

    return attributeValue;
  }
}