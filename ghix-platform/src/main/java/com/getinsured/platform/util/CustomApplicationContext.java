package com.getinsured.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.ResourceEntityResolver;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.web.context.support.XmlWebApplicationContext;


public class CustomApplicationContext extends XmlWebApplicationContext {
	private Logger logger = LoggerFactory.getLogger(CustomApplicationContext.class);
	private boolean disableValidation = false;
	public CustomApplicationContext(){
		super();
		String home = System.getProperty("GHIX_HOME");
		if(home != null) {
			StringBuilder builder = new StringBuilder();
			builder.append(home);
			if(home.endsWith(File.separator)) {
				builder.append("ghix-setup/conf/configuration.properties".intern());
			}else {
				builder.append("/ghix-setup/conf/configuration.properties".intern());
			}
			builder.trimToSize();
			Properties pr = new Properties();
			try {
				pr.load(new FileInputStream(new File(builder.toString())));
				String skipValidation = pr.getProperty("eligibility.at.idValidationSkip".intern());
				if(skipValidation != null) {
					this.disableValidation = skipValidation.trim().equalsIgnoreCase("true");
				}
			} catch (IOException e) {
				logger.error("Error reading the property".intern(),e);
			}
		}
		
	}
	
	protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) throws BeansException, IOException {
		// Create a new XmlBeanDefinitionReader for the given BeanFactory.
		XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
		if(this.disableValidation) {
			if(logger.isInfoEnabled()) {
				logger.info("Disabling all XSD validations");
			}
		}
		beanDefinitionReader.setValidating(false);

		// Configure the bean definition reader with this context's
		// resource loading environment.
		beanDefinitionReader.setEnvironment(getEnvironment());
		beanDefinitionReader.setResourceLoader(this);
		beanDefinitionReader.setEntityResolver(new ResourceEntityResolver(this));

		// Allow a subclass to provide custom initialization of the reader,
		// then proceed with actually loading the bean definitions.
		initBeanDefinitionReader(beanDefinitionReader);
		loadBeanDefinitions(beanDefinitionReader);
	}
}
