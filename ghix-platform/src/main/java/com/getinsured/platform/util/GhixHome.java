/**
 *
 */
package com.getinsured.platform.util;

import java.io.File;

/**
 * This class contains methods to resolve GHIX_HOME path.
 * <p>
 * Because there is so many places where we have methods to figure out GHIX_HOME I've named it like
 * this so that perhaps it would come up in auto completion for you.
 * </p>
 *
 * @author Yevgen Golubenko
 */
public class GhixHome
{
  private static String GHIX_HOME = System.getProperty("GHIX_HOME");
  private static String BASE_CONFIGURATION_DIR = "";
  private static DefinitionLocation location = DefinitionLocation.PROPERTY;

  /**
   * Defines possible locations of GHIX_HOME variable.
   *
   * @author Yevgen Golubenko
   */
  public enum DefinitionLocation
  {
    /**
     * System Environment was used to read {@code GHIX_HOME}
     */
    ENVIRONMENT,

    /**
     * System property was used to read {@code GHIX_HOME}
     */
    PROPERTY,

    /**
     * Key {@code GHIX_HOME} was not present in system properties or environment.
     */
    NOT_SET;
  }

  static
  {
    if(GHIX_HOME == null || "".equals(GHIX_HOME))
    {
      GHIX_HOME = System.getenv("GHIX_HOME");
      location = DefinitionLocation.ENVIRONMENT;
    }

    if(GHIX_HOME == null || "".equals(GHIX_HOME))
    {
      location = DefinitionLocation.NOT_SET;
    }

    if(GHIX_HOME != null)
    {
      if(GHIX_HOME.endsWith("\\/") || GHIX_HOME.endsWith("/"))
    {
      GHIX_HOME = GHIX_HOME.substring(0, GHIX_HOME.length() - 1);
    }

      final StringBuilder sb = new StringBuilder(36);
      sb.append(GHIX_HOME).append(File.separatorChar).append("ghix-setup").append(File.separatorChar).append("conf");

      BASE_CONFIGURATION_DIR = sb.toString();
    }
  }

  /**
   * Returns value of the {@code GHIX_HOME} system property or environment variable.
   * <p>
   *   If you need to know from where {@code GHIX_HOME} was read, you can call
   *   {@link #getDefinitionLocation()} method which will return you {@link DefinitionLocation}
   *   object, based on which you can figure out from where it was aquired.
   * </p>
   * @return value of {@code GHIX_HOME} property or environment.
   * @see #getDefinitionLocation()
   */
  public static String getGhixHome()
  {
    return GHIX_HOME;
  }

  /**
   * Returns base directory where configuration files are located.
   * <p>
   *   For example currently on dev and qa servers we have: /opt/web/ghixhome/ghix-setup/conf
   * </p>
   *
   * @return directory where configuration files are stored.
   */
  public static File getBaseConfigurationDir()
  {
    return new File(BASE_CONFIGURATION_DIR);
  }

  /**
   * Returns new File object relative to base configuration directory path.
   * <p>
   *   For example if <code>fileName</code> is <code>sample.xml</code>, then
   *   <code>$GHIX_HOME/ghix-setup/conf/sample.xml</code> will be returned.
   * </p>
   * <p>
   *   This method will not check if file exists or readable, it's up to the callee
   *   to validate file resource.
   * </p>
   * @param fileName file name relative to base configuration directory.
   * @return {@link File} object.
   */
  public static File getFileInBaseConfigurationDirectory(final String fileName)
  {
    return new File(BASE_CONFIGURATION_DIR + File.separatorChar + fileName);
  }

  /**
   * Returns where GHIX_HOME is defined.
   * Possible values for <code>DefinitionLocation</code> are:
   * <ul>
   *    <li>ENVIRONMENT - GHIX_HOME was found in Environment.</li>
   *    <li>PROPERTY    - GHIX_HOME was found in System properties.</li>
   *    <li>NOT_SET     - GHIX_HOME was not found anywhere</li>
   * </ul>
   * @return DefinitionLocation {@link DefinitionLocation}
   */
  public static DefinitionLocation getDefinitionLocation()
  {
    return location;
  }

  /**
   * Main entry point.
   *
   * @param args command line arguments.
   */
  public static void main(String[] args)
  {
    String ghixHome = getGhixHome();
    File confDirectory = getBaseConfigurationDir();
    DefinitionLocation location = getDefinitionLocation();
    System.out.println(String.format("GHIX_HOME=%s; Location=%s; Configuration Directory=%s%n", ghixHome, location, confDirectory.getAbsolutePath()));
  }
}
