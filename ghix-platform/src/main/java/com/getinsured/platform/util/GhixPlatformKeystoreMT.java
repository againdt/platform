package com.getinsured.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
@DependsOn("platformConstants")
public class GhixPlatformKeystoreMT {
	private static final String ALGORITHM = "RSA";
	private static Logger LOGGER = LoggerFactory.getLogger(GhixPlatformKeystoreMT.class);
	private static HashMap<String, String> aliasMapping = new HashMap<>();
	private static String defaultAlias = null;
	private static KeyStore platformKeyStore = null;
	private static char[] keyStorePassword = null;
	private static String keyStoreFileName = null;
	private static String keyStoreLocation = null;
	private static String ghixHome = null;
	private static String aliasMappingFile = null;

	@PostConstruct
	public void initKeyStore() {
		ghixHome = System.getProperty("GHIX_HOME");
		keyStorePassword = GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray();
		keyStoreFileName = GhixPlatformConstants.PLATFORM_KEY_STORE_FILE;
		keyStoreLocation = GhixPlatformConstants.PLATFORM_KEY_STORE_LOCATION;
		Assert.notNull(keyStoreFileName,
				"Required property, Keystore file name not availale, please check the configuration, check for password and location as well");
		Assert.notNull(keyStorePassword,
				"Required property, Keystore password not availale, please check the configuration, check for file name and location as well");
		Assert.notNull(keyStoreLocation,
				"Required property, Keystore location name not availale, please check the configuration, check for password and file name as well");
		aliasMappingFile = ghixHome + File.separatorChar + "ghix-setup" + File.separatorChar + "conf" + File.separatorChar + "sslConf.xml";
		File f = new File(aliasMappingFile);
		boolean aliasMapExists = f.exists() && f.canRead();
		Assert.isTrue(aliasMapExists, "File:" + aliasMappingFile + " doesn't exists or not readable, please check");
		Assert.isTrue(loadAliasMapping(), "Failed to buold the alias map");
	}

	/**
	 * Returns the private key from this key store for a given alias if exists.
	 * If no password is provided, keystore password will be used to retrieve
	 * the key
	 * 
	 * @param alias
	 * @param password
	 * @return PrivateKey for a given alias
	 */
	public PrivateKey getPrivateKey(String alias, char[] password) {
		char[] keyPass = null;
		PrivateKey aliasPrivateKey = null;
		if (password == null) {
			keyPass = keyStorePassword;
		} else {
			keyPass = password;
		}
		InputStream instream = null;
		try {
			if (platformKeyStore == null) {
				File keyStoreFile = getKeyStoreFile();
				platformKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());

				instream = new FileInputStream(keyStoreFile);
				platformKeyStore.load(instream, GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray());
				LOGGER.info("Done creating trust store...");
				validateKeyStore(platformKeyStore);
			}
			KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(keyPass);
			// get private key
			KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) platformKeyStore.getEntry(alias, protParam);
			aliasPrivateKey = pkEntry.getPrivateKey();
		} catch (Exception e) {
			LOGGER.error("Failed to find the private key for alias:" + alias + " faled with:" + e.getMessage(), e);
			return null;
		} finally {
			IOUtils.closeQuietly(instream);
		}
		return aliasPrivateKey;
	}

	public PublicKey getPublicKey(String alias) {
		Certificate cert = this.getCertificates(alias);
		if (cert != null) {
			return cert.getPublicKey();
		}
		return null;
	}

	public Certificate getCertificates(String aliasName) {
		Certificate certificate = null;
		try {
			Enumeration<String> aliases = platformKeyStore.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				if (!alias.equalsIgnoreCase(aliasName)) {
					continue;
				}
				certificate = platformKeyStore.getCertificate(alias);
			}
		} catch (KeyStoreException e) {
			LOGGER.error("Unexpected Exception reading key store", e);
			return null;
		}
		return certificate;
	}

	public Map<String, Certificate> getAvailableCertificates() {
		Certificate certificate = null;
		HashMap<String, Certificate> list = new HashMap<String, Certificate>();
		try {
			Enumeration<String> aliases = platformKeyStore.aliases();
			String alias = null;
			while (aliases.hasMoreElements()) {
				alias = aliases.nextElement();
				certificate = platformKeyStore.getCertificate(alias);
				if (certificate != null) {
					list.put(alias, certificate);
				}
			}
		} catch (KeyStoreException e) {
			LOGGER.error("Unexpected Exception reading key store", e);
			return null;
		}
		return list;
	}

	private static File getKeyStoreFile() {
		LOGGER.info("Keystore Location:" + keyStoreLocation);
		File keyStoreDir = new File(keyStoreLocation);
		File keyStore = new File(keyStoreDir, keyStoreFileName);
		if (!keyStore.exists() || !keyStore.isFile()) {
			throw new GIRuntimeException("Key store [" + keyStore.getAbsolutePath() + "] does not exist Or its not a file");
		}
		return keyStore;
	}

	/**
	 * Checks if configured aliases have a coressponding private key available
	 * in the supplied keystore
	 * 
	 * @param trustStore
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws UnrecoverableEntryException
	 */
	private static void validateKeyStore(KeyStore trustStore) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException {
		Collection<String> requiredAliases = aliasMapping.values();
		Enumeration<String> aliases = trustStore.aliases();
		String tmp = null;
		while (aliases.hasMoreElements()) {
			tmp = aliases.nextElement();
			if (requiredAliases.contains(tmp)) {

				// Try loading the key
				KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray());
				// get private key
				KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) trustStore.getEntry(tmp, protParam);
				PrivateKey aliasPrivateKey = pkEntry.getPrivateKey();
				if (aliasPrivateKey == null) {
					LOGGER.error("Alias:" + tmp + " is expected to have a coressponding private key entry, ....... false");
					throw new RuntimeException("No private key entry found for alias:" + tmp);
				}
				LOGGER.info("Alias:" + tmp + " is expected to have a coressponding private key entry, ....... true");
			}
		}
		// finally check if Keystore has all the aliases we have configured
		for (String requiredOne : requiredAliases) {
			if (!trustStore.containsAlias(requiredOne)) {
				LOGGER.error("Alias:" + requiredOne + " is expected to have a coressponding private key entry, ....... false, Ignoring");
			}
		}

	}

	private static boolean loadAliasMapping() {
		LOGGER.info("Loading alias mapping from file:" + aliasMappingFile);
		Document doc = null;
		InputStream is = null;
		try {
			DocumentBuilderFactory factory = Utils.getDocumentBuilderFactoryInstance();
			is = new FileInputStream(aliasMappingFile);
			doc = factory.newDocumentBuilder().parse(is);
			buildAliasMap(doc);
			return true;
		} catch (SAXException | IOException | ParserConfigurationException e) {
			LOGGER.error("Failed to load the attribute map", e);
		} finally {
			IOUtils.closeQuietly(is);
		}
		return false;
	}

	private static void buildAliasMap(Document doc) {
		NodeList aliasMaps = doc.getElementsByTagName("aliasMap");
		Node aliasMapNode = null;
		String alias = null;
		String server = null;
		String tmpVal = null;
		Node tmp = null;
		NamedNodeMap attributesList = null;
		int len = aliasMaps.getLength();
		for(int i = 0; i < len; i++){
			aliasMapNode = aliasMaps.item(i);
			attributesList = aliasMapNode.getAttributes();
			tmp = attributesList.getNamedItem("alias");
			if(tmp != null){
				alias = tmp.getNodeValue();
			}else{
				LOGGER.warn("No alias found for alias entry, skipping");
				continue;
			}
			tmp = attributesList.getNamedItem("server");
			if(tmp != null){
				server = tmp.getNodeValue();
			}else{
				LOGGER.warn("No server entry found for alias:"+alias+", skipping");
				continue;
			}
			
			tmp = attributesList.getNamedItem("default");
			if(tmp != null){
				tmpVal = tmp.getNodeValue();
				if(Boolean.valueOf(tmpVal)){// This alias is set to default
					if(defaultAlias != null && !alias.equals(defaultAlias)){
						throw new RuntimeException("Alias:"+defaultAlias+" is already set to be a default alias, \""+alias+"\" can not be set to be default alias, please provide only one");
					}
					defaultAlias  = alias;
					if(LOGGER.isInfoEnabled()){
						LOGGER.info("Marking \""+alias+"\" entry as default alias");
					}
				}
			}
			
			if(alias.length() > 0 && server.length() > 0){
				aliasMapping.put(server, alias);
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Added alias:"+alias+" for server:"+server);
				}
			}else{
				LOGGER.error("Invalid alias mapping entry:[alias="+alias+" -> server="+server+"], Ignoring");
			}
		}
	}
	
    public byte[] encrypt(String text, String alias)
    {
      if(!aliasMapping.containsKey(alias))
      {
        throw new GIRuntimeException("Failed to encrypt the data, alias not available in the key store:" + alias);
      }
      PublicKey key = this.getPublicKey(alias);
      if(key == null)
      {
        throw new GIRuntimeException("Failed to encrypt the data, no public key found for alias:" + alias);
      }
      byte[] cipherText = null;
      try
      {
        // get an RSA cipher object and print the provider
        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        // encrypt the plain text using the public key
        cipher.init(Cipher.ENCRYPT_MODE, key);
        cipherText = cipher.doFinal(text.getBytes());
      }
      catch(Exception e)
      {
        throw new GIRuntimeException(e);
      }
      return Base64.encodeBase64(cipherText);
    }
  
    public String decrypt(byte[] text, String alias, char[] pass)
    {
      char[] passwd = null;
      if(!aliasMapping.containsKey(alias))
      {
        throw new GIRuntimeException("Failed to decrypt the data, alias not available in the key store:" + alias);
      }
      if(pass == null)
      {
        passwd = keyStorePassword;
      }
      PrivateKey key = this.getPrivateKey(alias, passwd);
      byte[] dectyptedText = null;
      try
      {
        // get an RSA cipher object and print the provider
        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        // decrypt the text using the private key
        cipher.init(Cipher.DECRYPT_MODE, key);
        dectyptedText = cipher.doFinal(text);
      }
      catch(Exception ex)
      {
        throw new GIRuntimeException("Failed to decrypt the data, are you sure you used the right alias", ex);
      }
      return new String(dectyptedText);
    }

}
