package com.getinsured.platform.im.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Base64;

import javax.xml.parsers.DocumentBuilderFactory;

import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.provider.DOMMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.parse.BasicParserPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SAMLMetadataProviderUtil {
	private static Logger log = LoggerFactory.getLogger(SAMLMetadataProviderUtil.class);
	private static DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	private static BasicParserPool parserPool = new BasicParserPool();
	
	static{
		try {
			DefaultBootstrap.bootstrap();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		documentBuilderFactory.setNamespaceAware(true);
	}
	
	public static MetadataProvider getProvider(String providerData){
		try{
			
			Document document = documentBuilderFactory.newDocumentBuilder().parse(new ByteArrayInputStream(providerData.trim().getBytes()));
			Element elem = document.getDocumentElement();
			DOMMetadataProvider provider = new DOMMetadataProvider(elem);
			provider.setParserPool(parserPool);
			provider.setRequireValidMetadata(false);
			provider.initialize();
			return provider;
		}catch(Exception e){
			log.error("Failed to process XML data",e);
		}
		return null;
	}
	
	public static MetadataProvider getProvider(String encodedProviderData, boolean decodeBase64){
		try{
			byte[] providerData = null;
			if(decodeBase64){
				providerData = Base64.getDecoder().decode(encodedProviderData);
				Document document = documentBuilderFactory.newDocumentBuilder().parse(new ByteArrayInputStream(providerData));
				DOMMetadataProvider provider = new DOMMetadataProvider(document.getDocumentElement());
				provider.setRequireValidMetadata(false);
				provider.setParserPool(parserPool);
				provider.initialize();
				return provider;
			}else{
				return getProvider(encodedProviderData);
			}
		}catch(Exception e){
			log.error("Failed to process XML data",e);
		}
		return null;
	}
	
	public static String getEntityId(MetadataProvider provider){
		if(provider == null){
			return null;
		}
		try{
			XMLObject obj = provider.getMetadata();
			if(obj instanceof EntityDescriptor){
				EntityDescriptor desc = (EntityDescriptor)obj;
				return desc.getEntityID();
			}
		}catch(Exception e){
			log.error("Failed to retrieve the entity Id with error {}",e.getMessage());
		}
		return null;
	}
	
	public static MetadataProvider loadMetadataProvider(File path) throws Exception{
		BufferedReader br = new BufferedReader(
				  new InputStreamReader(
						  new FileInputStream(
								  path)));
		  StringBuilder idpData = new StringBuilder(4096);
		  String str = null;
		  while((str = br.readLine()) != null){
			  idpData.append(str);
		  }
		  br.close();
		  idpData.trimToSize();
		  return getProvider(idpData.toString());
	}
	
	public static void main(String[] args) throws Exception{
		File f = new File("/Users/abhai/Desktop/idp_sso_ghixqa.xml");
		MetadataProvider p = loadMetadataProvider(f);
		System.out.println(getEntityId(p));
	}
	

}
