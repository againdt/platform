package com.getinsured.platform.im.saml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.joda.time.DateTime;
import org.opensaml.util.resource.Resource;
import org.opensaml.util.resource.ResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.timeshift.TSDateTime;

/**
 * Memory base resource implementation.
 *
 * TODO: Add db support to pull XML content, etc.
 *
 * @author Yevgen Golubenko
 * @since 3/7/17
 */
public class MemoryBasedResource implements Resource
{
  private static final Logger log = LoggerFactory.getLogger(MemoryBasedResource.class);

  private static String XML = null;

  public MemoryBasedResource(String xml)
  {
    XML = xml;

    if(log.isInfoEnabled())
    {
      log.info("Created resource from given XML: {}", (xml != null ? xml.substring(0,50) + "...":"<NULL>"));
    }
  }

  @Override
  public String getLocation()
  {
    return "xml";
  }

  @Override
  public boolean exists() throws ResourceException
  {
    return XML != null && !XML.trim().equals("");
  }

  @Override
  public InputStream getInputStream() throws ResourceException
  {
    if(this.exists())
    {
      log.info("Returning new ByteArrayInputStream from current XML: {}", (XML != null));
      return new ByteArrayInputStream(XML.getBytes());
    }
    else
    {
      throw new ResourceException("Cannot get ByteArrayInputStream from XML string: " + XML);
    }
  }

  @Override
  public DateTime getLastModifiedTime() throws ResourceException
  {
    return TSDateTime.getInstance();
  }
}
