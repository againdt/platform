/**
 * 
 */
package com.getinsured.platform.im;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.context.SAMLMessageContext;
import org.springframework.security.saml.websso.WebSSOProfileOptions;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.ws_trust.saml.GhixSamlEntryPoint;

/**
 * Custom auth entry point that based on the available tenant information and tenant account owner type 
 * forwards authentication to the corresponding entry point, such as DB or SAML.
 * 
 * @author Yevgen Golubenko
 * @author tadepalli_v
 */
public class SAMLAuthenticationEntryPoint extends SAMLEntryPoint implements AuthenticationEntryPoint
{
  private static final Logger log = LoggerFactory.getLogger(SAMLAuthenticationEntryPoint.class);

  @Autowired
  private GhixSamlEntryPoint  samlEntryPoint;

  protected String            loginPageUrl = "/hix/account/user/login";

  public void commence(final HttpServletRequest request,
                       final HttpServletResponse response,
                       final AuthenticationException authException)
    throws IOException, ServletException
  {
    TenantDTO tenant = TenantContextHolder.getTenant();
    TenantSSOConfiguration tenantSSOConfiguration = null;

    if(tenant != null)
    {
      tenantSSOConfiguration = tenant.getTenantSSOConfiguration();

      if (tenantSSOConfiguration != null)
      {
        if (tenantSSOConfiguration.getIdentityOwner() == TenantSSOConfiguration.IdentityOwner.GETINSURED
            && tenantSSOConfiguration.getAuthMethod() == TenantSSOConfiguration.AuthenticationMethod.WEB_BASED_SSO)
        {
          log.info("Tenant {} has identity owner: {}, asking SAML entry point to commence.", tenant.getCode(), tenantSSOConfiguration.getIdentityOwner());
          samlEntryPoint.commence(request, response, authException);
          return;
        }
      }
    }

    log.info("Tenant {} has identity owner: {}, proceeding with [{}]", (tenant != null ? tenant.getCode() : "<null>"), (tenantSSOConfiguration!=null?tenantSSOConfiguration.getIdentityOwner():"<null>"), loginPageUrl);
    // If we have no sso/configuration we don't know IdentityOwnerType, proceed with default flow.
    response.sendRedirect(loginPageUrl);
  }

  public String getLoginPageUrl()
  {
    return loginPageUrl;
  }

  public void setLoginPageUrl(String loginPageUrl)
  {
    this.loginPageUrl = loginPageUrl;
  }

  protected WebSSOProfileOptions getProfileOptions(SAMLMessageContext context, AuthenticationException exception)
    throws MetadataProviderException
  {
    String relayState = context.getRelayState();
    WebSSOProfileOptions ssoProfileOptions;
    
    if(defaultOptions != null)
    {
      ssoProfileOptions = defaultOptions.clone();
    }
    else
    {
      ssoProfileOptions = new WebSSOProfileOptions();
    }
    
    if(relayState == null || relayState.equalsIgnoreCase(GhixPlatformConstants.FORCE_AUTHENTICATION))
    {
      logger.info("No relay state found, forcing authentication");
      ssoProfileOptions.setForceAuthN(true);
    }
    else
    {
      ssoProfileOptions.setForceAuthN(false);
    }
    
    return ssoProfileOptions;

  }
}
