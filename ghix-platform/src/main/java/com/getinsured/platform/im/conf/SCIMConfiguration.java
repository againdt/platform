package com.getinsured.platform.im.conf;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformKeystore;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;


public class SCIMConfiguration implements JSONAware, Externalizable{
	/*
	 * 
		scim.user.endpoint=https://sso.ghixqa.com:9443/wso2/scim/Users
		scim.group.endpoint=https://sso.ghixqa.com:9443/wso2/scim/Groups
		scim.provisioning.user=giscimuser@ghix.com
		scim.provisioning.password=ghix123#
		httpBasicAuthUser=giscimuser@ghix.com
		httpBasicAuthPass=ghix123#
		httpBasicAuthHost=sso.ghixqa.com
		httpBasicAuthPort=9443
	 */
	private int entityId;
	private String entityType;
	private String scimServerUserMgmtUrl = null;
	private String scimServerUserGroupMgmtUrl = null;
	private String scimUser = null;
	private String scimUserpass = null;
	private String authScheme = "HTTP_BASIC"; // For now
	
	@Autowired
	private GhixPlatformKeystore keyStore = null;
	
	
	public SCIMConfiguration(){
		keyStore = (GhixPlatformKeystore) GHIXApplicationContext.getBean(GhixPlatformKeystore.COMPONENT_NAME);
	}
	
	public String getScimServerUserMgmtUrl() {
		return scimServerUserMgmtUrl;
	}

	public void setScimServerUserMgmtUrl(String scimServerUserMgmtUrl) {
		this.scimServerUserMgmtUrl = scimServerUserMgmtUrl;
	}

	public String getScimServerUserGroupMgmtUrl() {
		return scimServerUserGroupMgmtUrl;
	}

	public void setScimServerUserGroupMgmtUrl(String scimServerUserGroupMgmtUrl) {
		this.scimServerUserGroupMgmtUrl = scimServerUserGroupMgmtUrl;
	}

	public String getScimUser() {
		return scimUser;
	}

	public void setScimUser(String scimUser) {
		this.scimUser = scimUser;
	}

	public String getScimUserpass() {
		return scimUserpass;
	}

	public void setScimUserpass(String scimUserpass) {
		this.scimUserpass = scimUserpass;
	}

	public String getAuthScheme() {
		return authScheme;
	}

	public void setAuthScheme(String authScheme) {
		this.authScheme = authScheme;
	}
	
	
	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ENTITY_ID".intern(), this.getEntityId());
		obj.put("ENTITY_TYPE".intern(), this.getEntityType());
		obj.put("AUTH_SCHEME".intern(), this.getAuthScheme());
		obj.put("SCIM_USERMGMT_URL".intern(), this.scimServerUserMgmtUrl);
		obj.put("SCIM_GROUPMGMT_URL".intern(), this.scimServerUserGroupMgmtUrl);
		obj.put("SCIM_PROVISIONING_USER".intern(), this.scimUser);
		try {
			obj.put("SCIM_PROVISIONING_PASS".intern(), this.keyStore.encryptEncodeUsingPrivateKey(
					this.keyStore.getDefaultAlias(), 
					GhixPlatformConstants.PLATFORM_KEYSTORE_PASS.toCharArray(),
					this.getScimUserpass().getBytes()));
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeySpecException
				| IllegalBlockSizeException | BadPaddingException e) {
			throw new GIRuntimeException("Failed to Encrypt the password for Entity Id:\""+this.getEntityId(), e);
		}
		return obj.toJSONString();
	}
	
	public void init(JSONObject obj){
		setEntityId((Integer)obj.get("ENTITY_ID".intern()));
		setEntityType((String)obj.get("ENTITY_TYPE".intern()));
		setAuthScheme((String)obj.get("AUTH_SCHEME".intern()));
		setScimServerUserMgmtUrl((String)obj.get("SCIM_USERMGMT_URL".intern()));
		setScimServerUserGroupMgmtUrl((String)obj.get("SCIM_GROUPMGMT_URL".intern()));
		setScimUser((String)obj.get("SCIM_PROVISIONING_USER".intern()));
		String encPassword = (String)obj.get("SCIM_PROVISIONING_PASS".intern());
		byte[] password;
		try {
			password = this.keyStore.decryptBase64EncodedUsingPublicKey(this.keyStore.getDefaultAlias(), encPassword);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeySpecException
				| IllegalBlockSizeException | BadPaddingException e) {
			throw new GIRuntimeException("Failed to retrieve the password for Entity Id:\""+this.getEntityId(), e);
		}
		setScimUserpass(new String(password));
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeUTF(this.toJSONString());
		out.flush();
	}
	
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		String str = in.readUTF();
		JSONParser parser = new JSONParser();
		try {
			JSONObject obj = (JSONObject) parser.parse(str);
			this.init(obj);
		} catch (ParseException e) {
			throw new IOException("Failed to read the SCIM configuration",e);
		}
	}
}
