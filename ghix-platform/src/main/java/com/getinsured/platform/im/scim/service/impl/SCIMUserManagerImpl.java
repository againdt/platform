/**
 * 
 */
package com.getinsured.platform.im.scim.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;
import org.wso2.charon.core.attributes.ComplexAttribute;
import org.wso2.charon.core.client.SCIMClient;
import org.wso2.charon.core.exceptions.AbstractCharonException;
import org.wso2.charon.core.exceptions.CharonException;
import org.wso2.charon.core.objects.User;
import org.wso2.charon.core.schema.SCIMConstants;

import com.atlassian.httpclient.api.HttpStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.WSO2Exception;
import com.getinsured.iex.hub.platform.security.HubSecureHttpClient;
import com.getinsured.platform.im.conf.SCIMConfiguration;
import com.getinsured.platform.im.scim.SCIMJSONParser;
import com.getinsured.platform.im.scim.SCIMTransformer;
import com.getinsured.platform.im.scim.service.SCIMUserManager;


/**
 * @author Biswakesh.Praharaj
 * 
 */
@Service("scimUserManagerMt")
public class SCIMUserManagerImpl implements SCIMUserManager {

	private static final Logger LOGGER = Logger.getLogger(SCIMUserManagerImpl.class);
	
	private CloseableHttpClient httpClient = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.security.service.scim.SCIMUserManager#createUser
	 * (com.getinsured.hix.model.AccountUser)
	 */
	@Override
	public AccountUser createUser(AccountUser accountUser) throws GIException {
		SCIMClient scimClient = null;
		User scimUser = null;
		String encodedUser = null;
		String authHeader = null;
		HttpEntity requestEntity = null;
		int responseStatus = -1;
		HttpPost httpost = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		JSONObject responseJson = null;
		SCIMJSONParser scimjsonParser = null;
		SCIMTransformer scimTransformer = null;
		GIException ex = null;
		ComplexAttribute wso2ExtensionAttribute = null;
		List<String> schemaList = null;
		try {
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}

			scimClient = new SCIMClient();
			scimUser = scimClient.createUser();

			scimUser.setUserName(accountUser.getEmail().trim());
			scimUser.setPassword(accountUser.getPassword().trim());

			scimTransformer = new SCIMTransformer();
			wso2ExtensionAttribute = scimTransformer
					.createWSO2ExtensionAttribute(accountUser);
			scimUser.setAttribute(wso2ExtensionAttribute);

			schemaList = new ArrayList<String>();
			schemaList.add("");
			scimUser.setSchemaList(schemaList);

			encodedUser = scimClient.encodeSCIMObject(scimUser,
					SCIMConstants.JSON);
			httpost = new HttpPost(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_CREATE_USER_ENDPOINT);
			SCIMConfiguration conf = TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(), conf.getScimUserpass());
			httpost.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(encodedUser).build();
			httpost.setEntity(requestEntity);

			httpResponse = httpClient.execute(httpost);

			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("USR CREATION RESPONSE RECEIVED FROM WSO2: "
						+ httpResponse.getStatusLine());
			}

			scimjsonParser = new SCIMJSONParser();
			responseJson = scimjsonParser
					.getJsonResponseFromEntity(responseEntity);

			if (SCIMClientConstants.USR_CREATED_INT != responseStatus) {
				ex = scimjsonParser.processCreateUserErrorResponse(
						responseStatus, responseJson);
			} else {
				scimjsonParser.processCreateUserSuccessResponse(responseStatus,
						responseJson, accountUser);
			}
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (Exception e) {
			LOGGER.error("ERR WHILE CREATING USR: ", e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					LOGGER.error(SCIMClientConstants.IO_EXCEPTION_MSG + e.getMessage()
							+ "]", e);
				}
			}
		}
		if (null != ex) {
			throw ex;
		}

		return accountUser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.security.service.scim.SCIMUserManager#findByEmail
	 * (java.lang.String)
	 */
	@Override
	public AccountUser findByEmail(String emailId) throws GIException {
		AccountUser accountUser = null;
		GIException ex = null;
		try {
			accountUser = findUser(SCIMClientConstants.USER_FILTER + emailId);
		} catch (Exception e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHIE FETCHING USR VIA SCIM: ", e);
		}
		if (ex != null) {
			throw ex;
		}
		return accountUser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.security.service.scim.SCIMUserManager#findUser
	 * (java.lang.String)
	 */
	@Override
	public AccountUser findUser(String filter) throws GIException {
		HttpGet getMethod = null;
		String authHeader = null;
		CloseableHttpResponse responseStatus = null;
		HttpEntity response = null;
		SCIMClient scimClient = null;
		GIException ex = null;
		AbstractCharonException ace = null;
		SCIMJSONParser scimjsonParser = null;
		AccountUser accountUser = null;
		try {
			getMethod = new HttpGet(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_FIND_USER_ENDPOINT
					+ "?" + filter);
			SCIMConfiguration conf = TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(), conf.getScimUserpass());
			getMethod.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			responseStatus = httpClient.execute(getMethod);
			response = responseStatus.getEntity();
			scimClient = new SCIMClient();
			scimjsonParser = new SCIMJSONParser();
			if (scimClient.evaluateResponseStatus(responseStatus
					.getStatusLine().getStatusCode())) {
				accountUser = scimjsonParser
						.processFetchUserResponse(scimjsonParser
								.getJsonResponseFromEntity(response));
			} else {
				// decode scim exception and extract the specific error message.
				ace = scimClient
						.decodeSCIMException(
								scimjsonParser.getJsonResponseFromEntity(
										response).toJSONString(),
								SCIMConstants
										.identifyFormat(SCIMClientConstants.CONTENT_TYPE));
				ex = new GIException(ace.getCode(), ace.getDescription(),
						"CRITICAL");
				LOGGER.error("Failed to find user");
			}
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (CharonException | IOException e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHIE FETCHING USR VIA SCIM: ", e);
		} catch (Exception e) {
			LOGGER.error("ERR WHILE FETCHIN USR VIA SCIM: ", e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (responseStatus != null) {
				try {
					responseStatus.close();
				} catch (IOException e) {
					LOGGER.error(SCIMClientConstants.IO_EXCEPTION_MSG + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		if (ex != null) {
			throw ex;
		}
		return accountUser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.getinsured.hix.platform.security.scim.service.SCIMUserManager#
	 * updateUserAdditionalAttribs(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String updateUserAdditionalAttribs(String id, String userName,
			String dob, String ssn, String preferredLanguage, String ridpFlag)
			throws GIException {
		String encodedUser = null;
		String authHeader = null;
		SCIMClient scimClient = null;
		HttpEntity requestEntity = null;
		HttpPut httpPut = null;
		CloseableHttpResponse responseStatus = null;
		GIException ex = null;
		AbstractCharonException ace = null;
		SCIMTransformer scimTransformer = null;
		SCIMJSONParser scimjsonParser = null;
		User scimUser = null;
		ComplexAttribute wso2ExtensionAttribute = null;
		String jsonResponseStr = null;
		try {
			scimUser = new User();
			scimUser.setId(id);
			scimUser.setUserName(userName);
			scimTransformer = new SCIMTransformer();
			wso2ExtensionAttribute = scimTransformer
					.addWSO2ExtensionAdditonalAttributes(dob, ssn,
							preferredLanguage, ridpFlag);
			scimUser.setAttribute(wso2ExtensionAttribute);

			scimjsonParser = new SCIMJSONParser();
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			
			httpPut = new HttpPut(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_UPDATE_ATTRIBS_ENDPOINT
					+ "/" + id);
			SCIMConfiguration conf = TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(), conf.getScimUserpass());
			httpPut.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);

			scimClient = new SCIMClient();
			encodedUser = scimClient.encodeSCIMObject(scimUser,
					SCIMConstants.JSON);

			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(encodedUser).build();

			httpPut.setEntity(requestEntity);

			responseStatus = httpClient.execute(httpPut);
			jsonResponseStr = scimjsonParser.getJsonResponseFromEntity(
					responseStatus.getEntity()).toJSONString();

		/*	if (LOGGER.isDebugEnabled()) {
				LOGGER.info("USR UPDATE RESPONSE RECIEVED: "
						+ responseStatus.getStatusLine());
			}*/
			if (scimClient.evaluateResponseStatus(responseStatus
					.getStatusLine().getStatusCode())) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("USR: UPDATED SUCCESSFULLY");
				}
			} else {
				ace = scimClient
						.decodeSCIMException(
								jsonResponseStr,
								SCIMConstants
										.identifyFormat(SCIMClientConstants.CONTENT_TYPE));
				ex = new GIException(ace.getCode(), ace.getDescription(),
						"CRITICAL");
				LOGGER.error(SCIMClientConstants.UPDATE_ERR_MSG, ace);
			}
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (CharonException | IOException e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error(SCIMClientConstants.UPDATE_ERR_MSG, e);
		} catch (Exception e) {
			LOGGER.error(SCIMClientConstants.UPDATE_ERR_MSG, e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (responseStatus != null) {
				try {
					responseStatus.close();
				} catch (IOException e) {
					LOGGER.error(SCIMClientConstants.IO_EXCEPTION_MSG + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		if (ex != null) {
			throw ex;
		}
		return jsonResponseStr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.getinsured.hix.platform.security.scim.service.SCIMUserManager#updateUser
	 * (com.getinsured.hix.model.AccountUser)
	 */
	@Override
	public AccountUser updateUser(AccountUser accountUser) throws GIException {

		String encodedUser = null;
		String authHeader = null;
		SCIMClient scimClient = null;
		HttpEntity requestEntity = null;
		HttpPut httpPut = null;
		User updatedScimUser = null;
		CloseableHttpResponse responseStatus = null;
		GIException ex = null;
		AbstractCharonException ace = null;
		SCIMTransformer scimTransformer = null;
		SCIMJSONParser scimjsonParser = null;
		try {
			scimTransformer = new SCIMTransformer();
			updatedScimUser = scimTransformer.convertGhixToScim(accountUser);
			scimjsonParser = new SCIMJSONParser();
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			SCIMConfiguration conf =TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
			httpPut = new HttpPut(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_UPDATE_USER_ENDPOINT
					+ "/" + updatedScimUser.getId());
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(), conf.getScimUserpass());
			httpPut.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);

			scimClient = new SCIMClient();
			encodedUser = scimClient.encodeSCIMObject(updatedScimUser,
					SCIMConstants.JSON);

			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(encodedUser).build();

			httpPut.setEntity(requestEntity);

			responseStatus = httpClient.execute(httpPut);

			/*if (LOGGER.isDebugEnabled()) {
				LOGGER.info("USR UPDATE RESPONSE RECIEVED: "
						+ responseStatus.getStatusLine());
			}*/

			if (scimClient.evaluateResponseStatus(responseStatus
					.getStatusLine().getStatusCode())) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("USR UPDATED SUCCESSFULLY");
				}
			} else {
				ace = scimClient.decodeSCIMException(scimjsonParser
						.getJsonResponseFromEntity(responseStatus.getEntity())
						.toJSONString(), SCIMConstants
						.identifyFormat(SCIMClientConstants.CONTENT_TYPE));
				ex = new GIException(ace.getCode(), ace.getDescription(),
						"CRITICAL");
				LOGGER.error(SCIMClientConstants.UPDATE_ERR_MSG, ace);
			}
		}
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (CharonException | IOException e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error(SCIMClientConstants.UPDATE_ERR_MSG, e);
		} catch (Exception e) {
			LOGGER.error("ERR WHILE UPDATING USR VIA SCIM: ", e);
			ex = new GIException(e.getMessage(), e);
		} finally {
			if (responseStatus != null) {
				try {
					responseStatus.close();
				} catch (IOException e) {
					LOGGER.error(SCIMClientConstants.IO_EXCEPTION_MSG + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		if (ex != null) {
			throw ex;
		}
		return accountUser;

	}
	
	/**
	 * This method authenticates the user on WSO2
	 * 
	 * @param username
	 *          Username of the user
	 * @param password
	 * 			Password of the user	
	 * @return Boolean indicating operation status
	 * @throws GIException
	 */
	public boolean authenticate(String username, String password) throws GIException{
		
		HttpEntity requestEntity = null;
		HttpEntity responseEntity = null;
		HttpPost httpPost = null;

		GIException ex = null;
		CloseableHttpResponse httpResponse = null;
		String authHeader = null;
		String requestBody = null;
		boolean isAuthenticateSuccessful = true;
		InputStream is = null;
		try {
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			httpPost = new HttpPost(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_AUTHENTICATE_ENDPOINT);
			SCIMConfiguration conf = TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(), conf.getScimUserpass());
			httpPost.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			requestBody = createAuthenticateRequest(username,password);
			
			requestEntity = EntityBuilder.create().setContentType(ContentType.APPLICATION_JSON)
					.setText(requestBody).build();
			
			httpPost.setEntity(requestEntity);

			httpResponse = httpClient.execute(httpPost);
			
			responseEntity = httpResponse.getEntity();
			
			//Check SCIM response
			is = responseEntity.getContent();
			String scimResponse = IOUtils.toString(is);
			if(StringUtils.contains(scimResponse,"false")){
				isAuthenticateSuccessful = false;
			}
			if(StringUtils.contains(scimResponse, "17003".intern())) {
				ex = new GIException("Account locked for "+username);
			}
			
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (Exception e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHILE AUTHENTICATING USER: ", e);
			isAuthenticateSuccessful = false;
		}finally{
			IOUtils.closeQuietly(is);
		}
		if (ex != null) {
			throw ex;
		}
		return isAuthenticateSuccessful;
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.getinsured.hix.platform.security.service.scim.SCIMUserManager#
	 * changePassword (com.getinsured.hix.model.AccountUser, java.lang.String)
	 */
	@Override
	public boolean changePassword(AccountUser updatedAccountUser,
			String newPassword) throws GIException {
		HttpEntity requestEntity = null;
		HttpEntity responseEntity = null;
		HttpPost httpPost = null;
		//int responseStatus = -1;
		GIException ex = null;
		CloseableHttpResponse httpResponse = null;
		String authHeader = null;
		String requestBody = null;
		boolean isUpdateSuccessful = true;
		InputStream is = null;
		SCIMConfiguration conf =TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
		try {
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			httpPost = new HttpPost(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_UPDATE_CRED_ENDPOINT);
			
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(),conf.getScimUserpass());
			httpPost.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			requestBody = createUpdateCredentialRequest(updatedAccountUser.getUsername(),updatedAccountUser.getPassword(),newPassword);
			
			requestEntity = EntityBuilder.create().setContentType(ContentType.APPLICATION_JSON)
					.setText(requestBody).build();
			
			httpPost.setEntity(requestEntity);

			httpResponse = httpClient.execute(httpPost);
			
			responseEntity = httpResponse.getEntity();
			
			//Check SCIM update password response for errors
			is = responseEntity.getContent();
			String scimResponse = IOUtils.toString(is);
			if(StringUtils.contains(scimResponse,"EXCEPTION")){
				if(StringUtils.contains(scimResponse, "30000")) {
					ex = new GIException("30000");
				}
				isUpdateSuccessful = false;
			}
			
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (Exception e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHIE UPDATING CRED: ", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
		if (ex != null) {
			throw ex;
		}
		return isUpdateSuccessful;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.getinsured.hix.platform.security.service.scim.SCIMUserManager#
	 * changePassword (com.getinsured.hix.model.AccountUser, java.lang.String)
	 */
	@Override
	public boolean changePasswordByAdmin(AccountUser updatedAccountUser,
			String newPassword) throws GIException {
		HttpEntity requestEntity = null;
		HttpEntity responseEntity = null;
		HttpPost httpPost = null;
		//int responseStatus = -1;
		GIException ex = null;
		CloseableHttpResponse httpResponse = null;
		String authHeader = null;
		String requestBody = null;
		boolean isUpdateSuccessful = true;
		SCIMConfiguration conf =TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
		InputStream is = null;
		try {
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			httpPost = new HttpPost(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_ADMIN_CHANGE_PSSWD_ENDPOINT);
			
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(),conf.getScimUserpass());
			httpPost.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			requestBody = createChangePasswordByAdminRequest(updatedAccountUser.getUsername(),newPassword);
			
			requestEntity = EntityBuilder.create().setContentType(ContentType.APPLICATION_JSON)
					.setText(requestBody).build();
			
			httpPost.setEntity(requestEntity);

			httpResponse = httpClient.execute(httpPost);
			
			responseEntity = httpResponse.getEntity();
						
			//Check SCIM update password response for errors
			is = responseEntity.getContent();
			String scimResponse = IOUtils.toString(is);
			if(StringUtils.contains(scimResponse,"EXCEPTION")){
				if(StringUtils.contains(scimResponse, "30000")) {
					ex = new GIException("30000");
				}
				isUpdateSuccessful = false;
			}
			
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (Exception e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHIE UPDATING CRED: ", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
		if (ex != null) {
			throw ex;
		}
		return isUpdateSuccessful;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.getinsured.hix.platform.security.service.scim.SCIMUserManager#
	 * changePassword (com.getinsured.hix.model.AccountUser, java.lang.String)
	 */
	@Override
	public boolean changeEmail(AccountUser accountUser) throws GIException {
		HttpEntity requestEntity = null;
		HttpEntity responseEntity = null;
		HttpPost httpPost = null;
		//int responseStatus = -1;
		GIException ex = null;
		CloseableHttpResponse httpResponse = null;
		String authHeader = null;
		String requestBody = null;
		boolean isUpdateSuccessful = true;
		SCIMConfiguration conf =TenantContextHolder.getTenant().getConfiguration().getScimConfiguration();
		InputStream is = null;
		try {
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			httpPost = new HttpPost(GhixPlatformConstants.IdentityServiceEndPoints.WSO2_UPDATE_USER_ENDPOINT);
			
			authHeader = Utils.getAuthorizationHeader(
					false,
					null,conf.getScimUser(),conf.getScimUserpass());
			httpPost.addHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			requestBody = createUpdateUserRequest(accountUser.getUserName(),"http://wso2.org/claims/userName",accountUser.getEmail());
			
			requestEntity = EntityBuilder.create().setContentType(ContentType.APPLICATION_JSON)
					.setText(requestBody).build();
			
			httpPost.setEntity(requestEntity);

			httpResponse = httpClient.execute(httpPost);
			
			if(200 != httpResponse.getStatusLine().getStatusCode()) {
				responseEntity = httpResponse.getEntity();
				
				// Check SCIM update password response for errors
				is = responseEntity.getContent();
				String scimResponse = IOUtils.toString(is);
				if (StringUtils.contains(scimResponse, "EXCEPTION")) {
					if (StringUtils.contains(scimResponse, "30000")) {
						ex = new GIException("30000");
					}
					isUpdateSuccessful = false;
				}
			}
		} 
		catch(ConnectException e){
			throw new WSO2Exception(SCIMClientConstants.WSO2_UNAVAILABLE_MSG,e);
		}
		catch (Exception e) {
			ex = new GIException(e.getMessage(), e);
			LOGGER.error("ERR: WHIE UPDATING CRED: ", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
		if (ex != null) {
			throw ex;
		}
		return isUpdateSuccessful;
	}
	
	/**
	 * Method to create json payload for update credentials
	 * 
	 * @param username
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String createUpdateCredentialRequest(String username, String oldPassword,
			String newPassword) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		try {
			payloadJsonObject = new JSONObject();
			payloadJsonObject.put("userName", username);
			payloadJsonObject.put("oldPassword", oldPassword);
			payloadJsonObject.put("password", newPassword);
			
			reqJsonObject = new JSONObject();
			reqJsonObject.put("clientIp", InetAddress.getLocalHost().getHostAddress());
			reqJsonObject.put("payload", payloadJsonObject);
			if(null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING UPDATE CRED JSON REQ: ",e);
		}
		return request;
	}
	
	/**
	 * Method to create json payload for update credentials
	 * 
	 * @param username
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String createChangePasswordByAdminRequest(String username, String newPassword) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		try {
			payloadJsonObject = new JSONObject();
			payloadJsonObject.put("userName", username);
			payloadJsonObject.put("password", newPassword);
			
			reqJsonObject = new JSONObject();
			reqJsonObject.put("clientIp", InetAddress.getLocalHost().getHostAddress());
			reqJsonObject.put("payload", payloadJsonObject);
			if(null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING ADMIN CHNG PSSWD JSON REQ: ",e);
		}
		return request;
	}
	
	/**
	 * Method to create json payload for update email address
	 * 
	 * @param username
	 * @param claimUri
	 * @param claimValue
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String createUpdateUserRequest(String username, String claimUri, String claimValue) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		try {
			payloadJsonObject = new JSONObject();
			payloadJsonObject.put("userName", username);
			payloadJsonObject.put("claimURI", claimUri);
			payloadJsonObject.put("claimValue", claimValue);
			
			reqJsonObject = new JSONObject();
			reqJsonObject.put("clientIp", InetAddress.getLocalHost().getHostAddress());
			reqJsonObject.put("payload", payloadJsonObject);
			if(null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING UPDATE USR JSON REQ: ",e);
		}
		return request;
	}
	
	@SuppressWarnings("unchecked")
	private String createAuthenticateRequest(String username, String password) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		try {
			payloadJsonObject = new JSONObject();
			payloadJsonObject.put("userName", username);
			payloadJsonObject.put("password", password);
			
			reqJsonObject = new JSONObject();
			reqJsonObject.put("clientIp", InetAddress.getLocalHost().getHostAddress());
			reqJsonObject.put("payload", payloadJsonObject);
			if(null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
			}
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CREATING UPDATE CRED JSON REQ: ",e);
		}
		return request;
	}
	private String getResponseString(HttpEntity entity){
		try {
			StringBuilder builder = new StringBuilder();
			BufferedReader sb = new BufferedReader(new InputStreamReader(entity.getContent()));
			String str = null;
			while((str = sb.readLine()) != null){
				builder.append(str);
			}
			sb.close();
			return builder.toString();
		} catch (UnsupportedOperationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void put(String attrName, Object val, boolean mandatory, JSONObject payload){
		if(val == null && mandatory){
			throw new RuntimeException("Mandatory attribute "+attrName+" Not provided");
		}
		if(val != null){
			payload.put(attrName, val);
		}
	}

	@SuppressWarnings("unchecked")
	private String createGetUserClaimRequest(String userName, String claimUri) {
		String request = null;
		JSONObject reqJsonObject = null;
		JSONObject payloadJsonObject = null;
		
		TenantDTO tenantDTO = TenantContextHolder.getTenant();
		if(tenantDTO != null) {
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Constructing payload for user claim with Tenant code:"+tenantDTO.getCode());
			}
			reqJsonObject = new JSONObject();
			put("tenantAdmin".intern(), tenantDTO.getTenantProvisionigUser(),true,reqJsonObject);
			put("tenantPassword".intern(), tenantDTO.getTenantProvisionigUserPassword(),true,reqJsonObject);
			put("tenantDomain".intern(), tenantDTO.getTenantDomain(),true,reqJsonObject);
	
			payloadJsonObject = new JSONObject();
			put("userName".intern(),userName,true,payloadJsonObject);
			put("profileName".intern(), "default".intern(),true,payloadJsonObject);
			put("claimURI".intern(), claimUri,true,payloadJsonObject);
	
			reqJsonObject.put("payload".intern(), payloadJsonObject);
			if (null != reqJsonObject) {
				request = reqJsonObject.toJSONString();
				if(LOGGER.isTraceEnabled()){
					LOGGER.trace("Get user claim Payload:"+request);
				}
			}
		}
		return request;
	}
	public String getClaimAttributeValue(String userName, String claimUri) {
		boolean success = false;
		String getClaimResponse = null;

		String uri = GhixPlatformConstants.GI_IDENTITY_SVC_URL+"getClaimAttributeValue";
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Using WSO2 Environment 5.3 for URI:"+uri);
		}
		HttpPost post = new HttpPost(uri);
		HttpEntity payload = EntityBuilder.create()
				.setContentType(ContentType.APPLICATION_JSON)
				.setText(createGetUserClaimRequest(userName,  claimUri)).build();
		post.setEntity(payload);
		try {
			if (this.httpClient == null) {
				this.httpClient = HubSecureHttpClient.getHttpClient();
			}
			
			CloseableHttpResponse response = (CloseableHttpResponse) this.httpClient.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if(HttpStatus.OK.code != status){
				LOGGER.error("Error response received with status:"+status);
			}else{
				success= true;
				getClaimResponse = getResponseString(response.getEntity());
			}
			response.close();
		} catch (IOException e) {
			LOGGER.error("Error creating user",e);
			success = false;
		}

		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Pre user create operation completed with success :"+success);
		}
		return getClaimResponse;

	}
}
