package com.getinsured.platform.im.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.saml.metadata.ExtendedMetadata;
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.platform.repository.TenantSSOConfigurationRepository;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.ws_trust.saml.TenantSpMetadata;
import com.getinsured.platform.config.bootstrap.GiTenantMetadataProvider;

/**
 * Various routines related to tenant saml/sso metadata configuration.
 *
 * @author Yevgen Golubenko
 * @since 3/17/17
 */
@Service
public class TenantMetadataUtil
{
  private static int defaultRefreshTime = 5*60*1000;
  private static final Logger log = LoggerFactory.getLogger(TenantMetadataUtil.class);
  
  @Autowired
  private TenantSSOConfigurationRepository tenantSSOConfigurationRepository;
  @Autowired
  private TenantSpMetadata tenantSpConfig;
  private String metaDataSigningKey = "gi_hosted_sp";
  private String metaDataEncryptionKey = "gi_hosted_sp";
  private HashMap<String, TenantSSOEnvironment> availableProviders = new HashMap<>();

  private List<TenantSSOConfiguration> ssoEnabledTenantConfigurations = new ArrayList<>();

  private MetadataProvider residentIdentityProvider;
  private int metadataRefreshTime;
  private static final StaticBasicParserPool parserPool = new StaticBasicParserPool();
  
  static{
	  try {
		if(GhixPlatformConstants.PLATFORM_NODE_PROFILE.equalsIgnoreCase("web")){
			  parserPool.initialize();
		}
		
	} catch (XMLParserException e) {
		log.error("Error Initializing the parser pool",e);
	}
  }

  @Value("#{configProp['platform.sso.metadataRefreshInterval'] != null ? configProp['platform.sso.metadataRefreshInterval'] : '2'}")
  public void setDefaultIdpMetadatae(String refreshTime){
	  	try{
	  		metadataRefreshTime = Integer.parseInt(refreshTime)*60*1000;
	  	}catch(Exception e){
	  		metadataRefreshTime = defaultRefreshTime;
	  	}
	}

  @PostConstruct
  public void loadTenantConfigurations()
  {
	if(!GhixPlatformConstants.PLATFORM_NODE_PROFILE.equalsIgnoreCase("web") || "false".equals(System.getProperty("wso2")))
	{
			return;
	}

    log.info("loading tenant sso/auth configurations");

    List<TenantSSOConfiguration> tenantSSOConfigurations = tenantSSOConfigurationRepository.findAll();

    ssoEnabledTenantConfigurations = tenantSSOConfigurations
        .stream()
        .filter(config -> config.getAuthMethod().equals(TenantSSOConfiguration.AuthenticationMethod.WEB_BASED_SSO))
        .collect(Collectors.toList());

    ssoEnabledTenantConfigurations.forEach(config ->
    {
      log.info("Metadata for tenant: {}", config.getTenantId());

      // MetadataMemoryProvider?
      try
      {
    	MetadataProvider identityProvider = null;
    	if(config.getIdentityOwner().equals(TenantSSOConfiguration.IdentityOwner.GETINSURED_IDP)){
    		identityProvider = getResidentIdentityProviderMetadata();
    	}else{
    		identityProvider = getIdentityProviderMetadata(config);
    	}
    	long tenantId = config.getTenantId();
    	String code = tenantSpConfig.getTenantCode(tenantId);
    	MetadataProvider serviceProvider = getExtendedMetadataDelegateForSp(tenantId, getServiceProviderMetadata(code));
        log.info("Metadata: service provider alias: {}", code);
        config.setIdentityProviderMetadata(identityProvider);
        config.setServiceProviderMetadata(serviceProvider);
        this.availableProviders.put(code, new TenantSSOEnvironment(SAMLMetadataProviderUtil.getEntityId(identityProvider), code));
        log.info("Done loading metadata providers for {}",code);
      }
      catch(Exception e)
      {
    	log.error("Error:",e);
        log.error("Exception occurred while constructing metadata for tenant: {}, IDP/SP configurations are not correct.", config.getTenantId());
      }
    });
  }

  /**
   * Returns Identity Provider Entity ID from SSO configuration for given tenant code.
   *
   * @param tenantCode tenant code
   * @return IDP Entity ID for Tenant identified by the given code, or <code>null</code>
   * if there is no SSO Configuration defined.
   */
  public String getIdentityProviderEntityId(final String tenantCode)
  {
	  TenantSSOEnvironment ssoEnv = this.availableProviders.get(tenantCode);

	  if(ssoEnv != null)
      {
        return ssoEnv.getIdpEntityId();
      }

      return null;
  }

  /**
   * Returns Service Provider Entity ID from SSO configuration for given tenant code.
   *
   * @param tenantCode tenant code
   * @return SP Entity ID for Tenant identified by the given code, or <code>null</code>
   * if there is no SSO Configuration defined.
   */
  public String getServiceProviderEntityId(String tenantCode){
	  TenantSSOEnvironment ssoEnv = this.availableProviders.get(tenantCode);

	  if(ssoEnv != null)
      {
        return ssoEnv.getSpEntityId();
      }

      return null;
  }

  public MetadataProvider getResidentIdentityProviderMetadata() throws Exception
  {
	  if(this.residentIdentityProvider != null)
	  {
		  log.info("Using previously initialized Resident IDP Metadata from location {}, file {}",GhixPlatformConstants.EXTERNAL_TEMPLATE_LOCATION,GhixPlatformConstants.RESIDENT_IDP_METADATA);
		  return this.residentIdentityProvider;
	  }
	  ExtendedMetadata identityProviderExtendedMetadata = new ExtendedMetadata();
      File idpFile = new File(GhixPlatformConstants.EXTERNAL_TEMPLATE_LOCATION,GhixPlatformConstants.RESIDENT_IDP_METADATA);
      log.info("Iinitialing Resident IDP Metadata from location {}, file {}",GhixPlatformConstants.EXTERNAL_TEMPLATE_LOCATION,GhixPlatformConstants.RESIDENT_IDP_METADATA);
      MetadataProvider provider = SAMLMetadataProviderUtil.loadMetadataProvider(idpFile);
      String entityId = SAMLMetadataProviderUtil.getEntityId(provider);
      log.info("Loaded IDP with EntityId {}",entityId);
      identityProviderExtendedMetadata.setAlias(entityId);
	  this.residentIdentityProvider = getResidentIdpExtendedMetadataDelegate(provider, identityProviderExtendedMetadata);
	  return this.residentIdentityProvider;
  }
  
  private MetadataProvider getIdentityProviderMetadata(TenantSSOConfiguration config) throws Exception
  {
	  ExtendedMetadata identityProviderExtendedMetadata = new ExtendedMetadata();
	  MetadataProvider idp = SAMLMetadataProviderUtil.getProvider(config.getIdpXml(), true);
	  String entityId = SAMLMetadataProviderUtil.getEntityId(idp);
	  log.info("Initializing external IDP metadata with Entity Id {}",entityId);
      identityProviderExtendedMetadata.setAlias(entityId);
	  return getExtendedMetadataDelegate(idp, identityProviderExtendedMetadata);
  }

  /**
   * Builds {@link MetadataProvider} to be loaded into {@link org.springframework.security.saml.metadata.MetadataManager}.
   *
   * @param base64EncodedXmlString
   * @param extraMetaData
   * @return
   */
  private MetadataProvider getExtendedMetadataDelegate(MetadataProvider provider, final ExtendedMetadata extraMetaData)
    throws Exception
  {
    ExtendedMetadataDelegate delegate = null;

    try
    {
      if (extraMetaData != null)
      {
        delegate = new ExtendedMetadataDelegate(provider, extraMetaData);
      } else
      {
        delegate = new ExtendedMetadataDelegate(provider);
      }
    } catch (Exception e)
    {
      if (log.isErrorEnabled())
      {
        log.error("Exception occurred while getting extended metadata provider", e);
      }

      throw new Exception(e);
    }

    // TODO: Create a matrix in confluence to finilize all of these singuare check things on wso2is and in apps.
    delegate.setMetadataRequireSignature(false);
    delegate.setMetadataTrustCheck(true);
    return delegate;
  }
  
  private MetadataProvider getResidentIdpExtendedMetadataDelegate(MetadataProvider provider, final ExtendedMetadata extraMetaData)
		    throws Exception
		  {
		    ExtendedMetadataDelegate delegate = null;
		    if (extraMetaData != null)
		      {
		        delegate = new ExtendedMetadataDelegate(provider, extraMetaData);
		      } else
		      {
		        delegate = new ExtendedMetadataDelegate(provider);
		      }

		    // TODO: Create a matrix in confluence to finilize all of these singuare check things on wso2is and in apps.
		    delegate.setMetadataRequireSignature(false);
		    delegate.setMetadataTrustCheck(true);
		    return delegate;
		  }
  
  private MetadataProvider getExtendedMetadataDelegateForSp(long tenantId, final ExtendedMetadata extraMetaData)
		    throws Exception
		  {
		    GiTenantMetadataProvider provider;
		    ExtendedMetadataDelegate delegate = null;
		   
		    try
		    {
		      provider = new GiTenantMetadataProvider(tenantId, this.metadataRefreshTime);
		      provider.setTenantSpMetadata(this.tenantSpConfig);
		      provider.setParserPool(parserPool);
		      if (extraMetaData != null)
		      {
		        delegate = new ExtendedMetadataDelegate(provider, extraMetaData);
		      } else
		      {
		        delegate = new ExtendedMetadataDelegate(provider);
		      }
		    } catch (Exception e)
		    {
		      if (log.isErrorEnabled())
		      {
		        log.error("Exception occurred while getting extended metadata provider for id: {}", tenantId);
		      }

		      throw new Exception(e);
		    }

		    // TODO: Create a matrix in confluence to finilize all of these singuare check things on wso2is and in apps.
		    delegate.setMetadataRequireSignature(false);
		    delegate.setMetadataTrustCheck(true);
		    return delegate;
		  }

  /**
   * Creates new {@link ExtendedMetadata} object with common/default GI parameters and given SP Alias.
   *
   * @param serviceProviderAlias service provider alias.
   * @return {@link ExtendedMetadata} object.
   */
  private ExtendedMetadata getServiceProviderMetadata(String serviceProviderAlias)
  {
    final ExtendedMetadata serviceProviderMetadata = new ExtendedMetadata();

    serviceProviderMetadata.setAlias(serviceProviderAlias);
    serviceProviderMetadata.setLocal(true);
    serviceProviderMetadata.setSecurityProfile("metaiop");
    serviceProviderMetadata.setSslSecurityProfile("metaiop");
    serviceProviderMetadata.setSslHostnameVerification("default"); // allowAll
    serviceProviderMetadata.setSignMetadata(false);
    serviceProviderMetadata.setSigningKey(metaDataSigningKey);
    serviceProviderMetadata.setEncryptionKey(metaDataEncryptionKey);
    serviceProviderMetadata.setRequireArtifactResolveSigned(false);
    serviceProviderMetadata.setRequireLogoutRequestSigned(false);
    serviceProviderMetadata.setRequireLogoutResponseSigned(false);
    serviceProviderMetadata.setIdpDiscoveryEnabled(false);

    return serviceProviderMetadata;
  }

  public TenantSSOConfiguration getConfigurationForTenantDTO(TenantDTO tenantDTO)
  {
    TenantSSOConfiguration config = null;

    for(TenantSSOConfiguration c : this.ssoEnabledTenantConfigurations)
    {
      if(c.getTenantId().equals(tenantDTO.getId()))
      {
        config = c;
        break;
      }
    }
    if(config == null){
    	log.info("Tenant {} not enabled for Web based SSO",tenantDTO.getCode());
    }
    return config;
  }

  public List<TenantSSOConfiguration> getSsoEnabledTenantConfigurations()
  {
    return ssoEnabledTenantConfigurations;
  }
  
  private class TenantSSOEnvironment
  {
	private String idpEntityId;
	private String spEntityId;

	public String getIdpEntityId() {
		return idpEntityId;
	}

	public String getSpEntityId() {
		return spEntityId;
	}

	TenantSSOEnvironment(String idpEntityId, String spEntityId){
		  this.idpEntityId = idpEntityId;
		  this.spEntityId = spEntityId;
	  }
  }
}
