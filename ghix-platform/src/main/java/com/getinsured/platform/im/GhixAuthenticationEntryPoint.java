/**
 * 
 */
package com.getinsured.platform.im;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.ws_trust.saml.GhixSamlEntryPoint;

/**
 * Custom auth entry point that based on the available tenant information and tenant account owner type 
 * forwards authentication to the corresponding entry point, such as DB or SAML.
 * 
 * @author Yevgen Golubenko
 * @author tadepalli_v
 */
public class GhixAuthenticationEntryPoint implements AuthenticationEntryPoint
{
  private static final Logger log = LoggerFactory.getLogger(GhixAuthenticationEntryPoint.class);

  private static final String GINS_TENANT_CODE = "GINS";
  
  private AuthenticationEntryPoint dbEntryPoint;
  
  @Autowired
  private SAMLEntryPoint samlEntryPoint;
  
  @Autowired 
  private TenantService tenantService;
  
  protected String loginPageUrl;

  private SAMLAuthenticationEntryPoint sAMLAuthenticationEntryPoint;

  public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authException) 
      throws IOException, ServletException
  {
    TenantDTO tenant = TenantContextHolder.getTenant();
    
    if(tenant == null)
    {
      tenant = (TenantDTO) request.getSession().getAttribute("TENANT");
      
      if(tenant == null)
      {
        if(log.isInfoEnabled())
        {
          log.info("GhixAuth: TenantContextHolder & Session doesn't hold any tenant, fetching default with code: {}", GINS_TENANT_CODE);
        }
        
        tenant = tenantService.getTenant(GINS_TENANT_CODE);
      }
    }

    // что если отсюда отхуярить их на самл и там разобраться?
    if(tenant != null && !"true".equals(System.getProperty("ignore.sso")))
    {
      TenantSSOConfiguration tenantSSOConfiguration = tenant.getTenantSSOConfiguration();

        if (tenantSSOConfiguration != null)
        {
          if (tenantSSOConfiguration.getAuthMethod() == TenantSSOConfiguration.AuthenticationMethod.WEB_BASED_SSO )
          {
            if(samlEntryPoint != null)
            {
              if(log.isDebugEnabled())
              {
                log.debug(" samlEntryPoint->commence");
              }

              samlEntryPoint.commence(request, response, authException);
            }
            else
            {
              if(log.isDebugEnabled())
              {
                log.debug(" sAMLAuthenticationEntryPoint->commence");
              }

              sAMLAuthenticationEntryPoint.commence(request, response, authException);
            }
            return;
          }
        }
    }

    // If we have no sso/configuration we don't know AccountOwnerType configuration,
    // proceed with default login page url.
    response.sendRedirect(request.getContextPath() + "/account/user/login");
  }

  public AuthenticationEntryPoint getDbEntcryPoint()
  {
    return dbEntryPoint;
  }

  public void setDbEntryPoint(AuthenticationEntryPoint dbEntryPoint)
  {
    this.dbEntryPoint = dbEntryPoint;
  }

  public String getLoginPageUrl()
  {
    return loginPageUrl;
  }

  public void setLoginPageUrl(String loginPageUrl)
  {
    this.loginPageUrl = loginPageUrl;
  }

  public void setSamlEntryPoint(GhixSamlEntryPoint samlEntryPoint)
  {
    this.samlEntryPoint = samlEntryPoint;
  }

  public void setSAMLAuthenticationEntryPoint(SAMLAuthenticationEntryPoint entry)
  {
    this.sAMLAuthenticationEntryPoint = entry;
  }
  
  public void setTenantService(TenantService service)
  {
    this.tenantService = service;
  }
}
