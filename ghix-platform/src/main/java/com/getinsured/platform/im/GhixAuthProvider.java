/**
 * 
 */
package com.getinsured.platform.im;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLAuthenticationToken;

/**
 * @author Yevgen Golubenko yevgen.golubenko@getinsured.com
 */
public class GhixAuthProvider implements AuthenticationProvider
{

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return SAMLAuthenticationToken.class.isAssignableFrom(authentication) || (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
	
}
