package com.getinsured.platform.im.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages and resolves relations of Tenant => IDP XML Definitions.
 *  
 * @author Yevgen Golubenko 
 */
public class IdentityProviderResolver
{
  private static final Logger log = LoggerFactory.getLogger(IdentityProviderResolver.class);
  
//  @Autowired
//  private TenantRepository tenantRepo;
//
//  @Value("#{configProp['use.name.for.IdentityServerEntityId']}")
//  private String useNameForIdentityServerEntityId;
//
//  private static List<Tenant> tenants;
//  private static Map<String, String> idpMetadataFiles;
//  private static Map<String, String> spMetadataFiles;
//  public static Map<String, String> spAliasToXml;
//
//  private static Map<Long, String> tenantIdEntityIdMap;
//
//  public static String idpEntityId;
//
//  @PostConstruct
//  public void init()
//  {
//    final List<Tenant> tenantCandidates = tenantRepo.findAllByActive(Tenant.IS_ACTIVE.Y);
//    tenants = new ArrayList<>();
//    idpMetadataFiles = new HashMap<>();
//    spMetadataFiles = new HashMap<>();
//    tenantIdEntityIdMap = new HashMap<>();
//    spAliasToXml = new HashMap<>();
//
//    for(Tenant t : tenantCandidates)
//    {
//      // Skip inactive tenants
//      if (t.getIsActive() == Tenant.IS_ACTIVE.N)
//      {
//        log.debug("Skipping tenant: {}, not active.", t.getCode());
//        continue;
//      }
//
//      if(t.getTenantSSOConfiguration() == null)
//      {
//        log.debug("Skipping tenant: {}, no sso configuration.", t.getCode());
//        continue;
//      }
//
//      if(t.getTenantSSOConfiguration().getIdpXml() != null && t.getTenantSSOConfiguration().getSpXml() != null)
//      {
//        idpMetadataFiles.put(t.getCode(), new String(Base64.getDecoder().decode(t.getTenantSSOConfiguration().getIdpXml().getBytes())));
//        spMetadataFiles.put(t.getCode(), new String(Base64.getDecoder().decode(t.getTenantSSOConfiguration().getSpXml().getBytes())));
//        spAliasToXml.put(t.getTenantSSOConfiguration().getSpAlias(), new String(Base64.getDecoder().decode(t.getTenantSSOConfiguration().getSpXml().getBytes())));
//        tenantIdEntityIdMap.put(t.getId(), t.getTenantSSOConfiguration().getIdpEntityId());
//
//        if(idpEntityId == null)
//        {
//          idpEntityId = t.getTenantSSOConfiguration().getIdpEntityId();
//        }
//
//        tenants.add(t);
//        log.info("Adding tenant to possible SSO flow: {}, idp entityID: {}", t.getCode(), t.getTenantSSOConfiguration().getIdpEntityId());
//      }
//      else {
//        log.info("Skipping tenant: {} with partial SSO config. No idp/sp xml: {}", t.getCode());
//      }
//    }
//  }
//
//  /**
//   * Returns parameter (key=value) of configured <code>entityID</code>
//   * for given {@link TenantDTO}.
//   *
//   * @param tenantDto {@link TenantDTO} object.
//   * @return idp=entityID value configured for given Tenant or empty string. Currently <code>idp</code> is taken from
//   * constant {@link SAMLEntryPoint.IDP_PARAMETER}
//   */
//  public String getIDPMetadataId(TenantDTO tenantDto)
//  {
//    String entityId = null;
//
//    if(tenantDto != null)
//    {
//      entityId = tenantIdEntityIdMap.get(tenantDto.getId());
//
//      if(log.isInfoEnabled())
//      {
//        log.info("Returning entityId [" + entityId + "] for tenant code [" + tenantDto.getCode() + "]");
//      }
//    }
//
//    if(entityId == null || "".equals(entityId.trim()))
//    {
//      return entityId;
//    }
//
//    // Only return this in case of entity Id is assigned to the tenant.
//    return SAMLEntryPoint.IDP_PARAMETER + "=" + entityId;
//  }
//
//  /**
//   * Returns map of Identity Provider (IDP) metadata files.
//   * <p>
//   *  Map consists of entries like: <br/>
//   *  <code>
//   *    [TENANT_ID => TENANT_ID_idp.xml]
//   *  </code>
//   * </p>
//   * @return map of tenant id to IDP xml files.
//   */
//  public Map<String, String> getIdpMetadataFiles()
//  {
//    return idpMetadataFiles;
//  }
//
//  public void setIdpMetadataFiles(Map<String, String> idpMetadataFiles)
//  {
//    this.idpMetadataFiles = idpMetadataFiles;
//  }
//
//  /**
//   * Returns map of Service Provider (SP) metadata files.
//   * <p>
//   *  Map consists of entries like:<br/>
//   *  <code>
//   *    [TENANT_ID => TENANT_ID_sp.xml]
//   *  </code>
//   * </p>
//   * @return map of tenant id to SP xml files.
//   */
//  public Map<String, String> getSpMetadataFiles()
//  {
//    return spMetadataFiles;
//  }
//
//  public void setSpMetadataFiles(Map<String, String> spMetadataFiles)
//  {
//    this.spMetadataFiles = spMetadataFiles;
//  }
}
