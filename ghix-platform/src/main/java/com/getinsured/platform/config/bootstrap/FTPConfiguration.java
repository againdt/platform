/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.getinsured.hix.platform.ftp.GHIXSFTPClient;

/**
 * FTP Client Configuration.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class FTPConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(FTPConfiguration.class);
  
  @Value("#{configProp['SERFF_CSR_ftpHost']}")
  private String host;
  
  @Value("#{configProp['SERFF_CSR_ftpPort']}")
  private Integer port;
  
  @Value("#{configProp['SERFF_CSR_ftpUser']}")
  private String username;
  
  @Value("#{configProp['SERFF_CSR_ftpPassword']}")
  private String password;
  
  public FTPConfiguration()
  {
    if(log.isInfoEnabled())
    {
      log.info("Entering <FTPConfiguration> bootstrap phase.");
    }
  }
  
  @Bean
  @Lazy
  public GHIXSFTPClient ftpClient()
  {
    final GHIXSFTPClient client = new GHIXSFTPClient(host, username, password, port);
    
    return client;
  }
}
