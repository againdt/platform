package com.getinsured.platform.config.bootstrap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * This class simply defines packages for Spring component scan.
 *
 * @since 12/16/16
 * @author Yevgen Golubenko
 */
// TODO: Narrow down packages. Currently most but not all packages are under */service/*, */jpa/*
@Configuration
@ComponentScan(basePackages = {
    // Service
    "com.getinsured.hix.platform.account.service.jpa",
    "com.getinsured.hix.platform.accountactivation.service.jpa",
    "com.getinsured.hix.platform.audit.service",
    "com.getinsured.hix.platform.batch.service",
    "com.getinsured.hix.platform.comment.service.jpa",
    "com.getinsured.hix.platform.couchbase",
    "com.getinsured.hix.platform.ecm.couchbase",
    "com.getinsured.hix.platform.emailstat.service",
    "com.getinsured.hix.platform.eventlog.service",
    "com.getinsured.hix.platform.external.cloud.service",
    "com.getinsured.hix.platform.gimonitor.service.jpa",
    "com.getinsured.hix.platform.giwspayload.service.jpa",
    "com.getinsured.hix.platform.location.service.jpa",
    "com.getinsured.hix.platform.notices.jpa",
    "com.getinsured.hix.platform.notification",
    "com.getinsured.hix.platform.notify",
    "com.getinsured.hix.platform.paper.service",
    "com.getinsured.hix.platform.security",
    "com.getinsured.hix.platform.security.scim.service.impl",
    "com.getinsured.hix.platform.security.service.impl",
    "com.getinsured.hix.platform.session",
    "com.getinsured.hix.platform.security.session",
    "com.getinsured.hix.platform.security.tokens",
    "com.getinsured.hix.platform.service.jpa",
    "com.getinsured.hix.platform.util",
    "com.getinsured.hix.ws_trust.saml",
    "com.getinsured.platform.im.scim.service.impl",
    "com.getinsured.platform.im.util",
    


    // Component
    "com.getinsured.hix.endpoints",
    "com.getinsured.hix.model",
    "com.getinsured.hix.platform.accountactivation",
    "com.getinsured.hix.platform.affiliate.service",
    "com.getinsured.hix.platform.auditor.advice",
    "com.getinsured.hix.platform.batch.notification",
    "com.getinsured.hix.platform.cache",
    "com.getinsured.hix.platform.clamav",
    "com.getinsured.hix.platform.config",
    "com.getinsured.hix.platform.couchbase.converters",
    "com.getinsured.hix.platform.couchbase.helper",
    "com.getinsured.hix.platform.events",
    "com.getinsured.hix.platform.external.cloud.service",
    "com.getinsured.hix.platform.handler",
    "com.getinsured.hix.platform.jmx",
    "com.getinsured.hix.platform.logging.filter",
    "com.getinsured.hix.platform.multitenant.resolver.filter",
    "com.getinsured.hix.filter",
    "com.getinsured.hix.platform.notices",
    "com.getinsured.hix.platform.notify",
    "com.getinsured.hix.platform.pdf.filler",
    "com.getinsured.hix.platform.startup",
    "com.getinsured.hix.platform.webclient",
    "com.getinsured.hix.platform.featureflag.service",
    "com.getinsured.hix.platform.document.service",
    "com.getinsured.hix.platform.ecm.document",
    "com.getinsured.hix.platform.aws",
    "com.getinsured.iex.hub.platform.utils",
    "com.getinsured.iex",
    "com.getinsured.hix.reports",

    /* Non platform packages referenced in appicationPlatform.xml */
    "com.getinsured.iex.ssap.builder", // ghix-iex-common - SsaoJsonBuilderImpl
    "com.getinsured.iex.client", // ghix-iex-common - SsapApplicationAccessorImpl
    "com.getinsured.iex.util", // ghix-iex-common - SsapUtil


    /* JPA */
    // @Repository
    "com.getinsured.hix.platform.accountactivation.repository",
    "com.getinsured.hix.platform.accountactivation.service.jpa",
    "com.getinsured.hix.platform.comment.service.jpa",
    "com.getinsured.hix.platform.gimonitor.repository",
    "com.getinsured.hix.platform.giwspayload.repository",
    "com.getinsured.hix.platform.location.service.jpa",
    "com.getinsured.hix.platform.repository",
    "com.getinsured.hix.platform.security.repository",
    "com.getinsured.hix.platform.security.service.jpa",
    "com.getinsured.hix.platform.service.jpa",
    "com.getinsured.hix.platform.useraud.repository",
    "com.getinsured.hix.platform.security.service.impl", // PasswordServiceImpl

    // JpaRepository
    "com.getinsured.hix.platform.account.repository",
    "com.getinsured.hix.platform.audit.envers.repository.support",
    "com.getinsured.hix.platform.comment.repository",
    "com.getinsured.hix.platform.emailstat.repository",
    "com.getinsured.hix.platform.eventlog.repository",
    "com.getinsured.hix.platform.gimonitor.repository",
    "com.getinsured.hix.platform.lookup.repository",
    "com.getinsured.hix.platform.payments.repository",
    "com.getinsured.affiliate.repository",

    // CrudRepository
    "com.getinsured.hix.platform.cache.repository",

    // EnversRevisionRepository
    "com.getinsured.hix.platform.audit.envers.repository.support",

    // FileManager service for PrintManagementServiceImpl
    "com.getinsured.hix.platform.file.manager.service",
    // GiAuditorService
    "com.getinsured.hix.platform.auditor.advice",

    // planmgmt
    "com.getinsured.hix.planmgmt.service",
    "com.getinsured.hix.pmms",
    "com.getinsured.hix.planmgmt.querybuilder",

    "com.getinsured.hix.planmgmt.history",
    "com.getinsured.hix.provider.service.jpa",
    "com.getinsured.hix.util",
    "com.getinsured.hix.planmgmt.mapper",

    "com.getinsured.hix.platform.service.jpa"
})
public class ComponentScanConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(ComponentScanConfiguration.class);

  @PostConstruct
  public void postConstruct()
  {
    if(log.isInfoEnabled())
    {
      log.info("<ComponentScanConfiguration> initialization complete");
    }
  }
}
