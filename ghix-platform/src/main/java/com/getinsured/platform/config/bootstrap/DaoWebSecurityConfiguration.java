package com.getinsured.platform.config.bootstrap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.GhixDaoAuthenticationProvider;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import com.getinsured.hix.platform.security.CustomUserDetailsService;
import com.getinsured.hix.platform.security.GhixUrlAuthenticationFailureHandler;
import com.getinsured.hix.platform.security.GhixUrlAuthenticationSuccessHandler;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.platform.config.bootstrap.security.GhixSecurity;
import com.getinsured.platform.im.GhixAuthenticationEntryPoint;

/**
 * DAO Based web security configuraiton.
 *
 * @author Yevgen Golubenko
 */
@GhixSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebMvcSecurity
@Order(Ordered.LOWEST_PRECEDENCE - 200)
public class DaoWebSecurityConfiguration extends WebSecurityConfigurerAdapter
{
  private static final Logger log = LoggerFactory.getLogger(DaoWebSecurityConfiguration.class);

  @Autowired
  private SamlWebSecurityConfiguration samlConfiguration;

  @Autowired
  private TenantService tenantService;

  @Value("#{configProp['security.rolesAllowedViaDomain']}")
  private String securityRolesAllowedViaDomain;

  @Value("#{configProp['security.domainAllowed']}")
  private String securityDomainAllowed;

  @Autowired
  private CustomUserDetailsService customUserDetailsService;

  /**
   * Default constructor
   */
  @PostConstruct
  public void postConstruct()
  {
    if (log.isInfoEnabled())
    {
      log.info("Entering <DaoWebSecurityConfiguration> bootstrap phase.");
    }
  }

  @Override
  public void configure(WebSecurity web) throws Exception
  {
    if (log.isInfoEnabled())
    {
      log.info("=> configuring DAO WebSecurity");
    }

    web.ignoring().antMatchers(UrlSecurityConfiguration.RESOURCES_PATH);
    web.debug(true); // TODO: Remove debugging before checkin
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception
  {
    return authenticationManager();
  }

  @Autowired
  public void registerDefaultAuthProvider(AuthenticationManagerBuilder authBuilder) throws Exception
  {
    authBuilder.authenticationProvider(ghixDaoAuthenticationProvider());

    if (log.isInfoEnabled())
    {
      log.info("=> registerDefaultAuthProvider added ghixDaoAuthenticationProvider...");

      if (log.isDebugEnabled())
      {
        System.err.println("=> registerDefaultAuthProvider added ghixDaoAuthenticationProvider...");
      }
    }
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception
  {
    final HttpSessionSecurityContextRepository repo = new HttpSessionSecurityContextRepository();
    repo.setDisableUrlRewriting(true);

    http.authenticationProvider(ghixDaoAuthenticationProvider());
    http.httpBasic().authenticationEntryPoint(customAuthenticationEntryPoint());
    http.securityContext().securityContextRepository(repo);
    http.exceptionHandling().accessDeniedPage("/account/user/denied");
    //http.exceptionHandling().accessDeniedPage("/error/accessDeniedPage");
    // web.ignoring().antMatchers("/resources/**")
    http.authorizeRequests()
        .filterSecurityInterceptorOncePerRequest(true)
        .antMatchers(UrlSecurityConfiguration.UNSECURED_URL_LIST).permitAll()
        .antMatchers("/**", "/**/**").access("( (!hasAnyRole( " + securityRolesAllowedViaDomain + " ) and isAuthenticated()) " +
        "or (   hasAnyRole( " + securityRolesAllowedViaDomain + " ) and " +
        "hasIpAddress( " + securityDomainAllowed + " ) ) )");//fullyAuthenticated();
    http.logout().deleteCookies("JSESSIONID", "SPRING_SECURITY_REMEMBER_ME_COOKIE", "SESSION").invalidateHttpSession(true).logoutSuccessUrl("/account/user/login");//.logoutUrl("/j_spring_security_logout");
    http.logout().logoutUrl("/j_spring_security_logout");

    http.addFilterAfter(samlConfiguration.samlFilter(), BasicAuthenticationFilter.class);


    // http.addFilterAfter(samlFilter(), BasicAuthenticationFilter.class);
    // http.addFilterAfter(new UsernamePasswordAuthenticationFilter(), BasicAuthenticationFilter.class);
    
    /* 
     * We use our custom CSRF toke generation/validation, so disable Spring's. Reason is because per 
     * our requirement token length generated by Spring is not enought for compliance. 
     */
    http.csrf().disable();

    http.formLogin()
        .usernameParameter("j_username")
        .passwordParameter("j_password")
        .loginProcessingUrl("/j_spring_security_check")
        .loginPage("/account/user/login?confirmId=Y")

        .successHandler(ghixSuccessHandler())
        .failureHandler(ghixFailureHandler())
        .defaultSuccessUrl("/account/user/loginSuccess", true).permitAll().and().logout().permitAll();

    http.exceptionHandling()
        .defaultAuthenticationEntryPointFor(
            customAuthenticationEntryPoint(),
            samlConfiguration.acceptHeaderRequestMatcher());
  }

  public GhixAuthenticationEntryPoint customAuthenticationEntryPoint()
  {
    final GhixAuthenticationEntryPoint entry = new GhixAuthenticationEntryPoint();
    entry.setDbEntryPoint(loginDbUrlAuthenticationEntryPoint());
    entry.setSamlEntryPoint(samlConfiguration.samlEntryPoint());
    entry.setTenantService(tenantService);
    entry.setDbEntryPoint(loginDbUrlAuthenticationEntryPoint());

    return entry;
  }

  @Bean
  public LoginUrlAuthenticationEntryPoint loginDbUrlAuthenticationEntryPoint()
  {
    final LoginUrlAuthenticationEntryPoint loginUrlAuthEntry = new LoginUrlAuthenticationEntryPoint(GhixPlatformEndPoints.LOGIN_PAGE + "?defaultLogin=1");
    //loginUrlAuthEntry.setUseForward(true); // Because we don't use full url (e.g. http://host/dir/page)

    return loginUrlAuthEntry;
  }

  ///-@Bean(name = "ghixDaoAuthenticationProvider")
  public GhixDaoAuthenticationProvider ghixDaoAuthenticationProvider()
  {
    final GhixDaoAuthenticationProvider daoAuthProvider = new GhixDaoAuthenticationProvider();

    daoAuthProvider.setPasswordEncoder(passwordEncoder());
    daoAuthProvider.setSaltSource(saltSource());
    daoAuthProvider.setUserDetailsService(daoUserDetailsService());

    return daoAuthProvider;
  }

  ///-@Bean
  ///-@Primary
  public UserDetailsService daoUserDetailsService()
  {
    return customUserDetailsService;
  }

  public BCryptPasswordEncoder passwordEncoder()
  {
    return new BCryptPasswordEncoder();
  }

  public ReflectionSaltSource saltSource()
  {
    final ReflectionSaltSource ss = new ReflectionSaltSource();
    ss.setUserPropertyToUse("uuid");
    return ss;
  }

  public GhixUrlAuthenticationSuccessHandler ghixSuccessHandler()
  {
    GhixUrlAuthenticationSuccessHandler handler = new GhixUrlAuthenticationSuccessHandler();
    handler.setDefaultTargetUrl("/account/user/login");
    handler.setBaseGhixAuthContext("/hix");

    return handler;
  }

  public GhixUrlAuthenticationFailureHandler ghixFailureHandler()
  {
    GhixUrlAuthenticationFailureHandler handler = new GhixUrlAuthenticationFailureHandler();
    handler.setDefaultFailureUrl("/account/user/loginfailed");

    return handler;
  }
}
