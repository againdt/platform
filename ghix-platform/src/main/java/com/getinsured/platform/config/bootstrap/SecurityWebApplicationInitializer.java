/**
 *
 */
package com.getinsured.platform.config.bootstrap;

import org.springframework.context.annotation.Configuration;

/**
 * @author Yevgen Golubenko
 */
@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity
public class SecurityWebApplicationInitializer
    /* extends AbstractSecurityWebApplicationInitializer*/
    /* extends WebSecurityConfigurerAdapter */
{
}
/*
{
  private static final Logger log = LoggerFactory.getLogger(SecurityWebApplicationInitializer.class);
  
  @Override
  protected void afterSpringSecurityFilterChain(ServletContext servletContext)
  {
    if(log.isInfoEnabled())
    {
      log.info("afterSpringSecurityFilterChain: " + servletContext.getContextPath());
    }
    
    super.afterSpringSecurityFilterChain(servletContext);
  }
  
  @Override
  protected void beforeSpringSecurityFilterChain(ServletContext servletContext)
  {
    if(log.isInfoEnabled())
    {
      log.info("beforeSpringSecurityFilterChain: " + servletContext.getContextPath());
    }
    super.beforeSpringSecurityFilterChain(servletContext);
  }
}*/
