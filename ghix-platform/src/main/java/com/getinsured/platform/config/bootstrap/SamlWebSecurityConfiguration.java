package com.getinsured.platform.config.bootstrap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.velocity.app.VelocityEngine;
import org.opensaml.saml2.metadata.provider.DOMMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.owasp.esapi.reference.DefaultSecurityConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLBootstrap;
import org.springframework.security.saml.SAMLDiscovery;
import org.springframework.security.saml.SAMLLogoutFilter;
import org.springframework.security.saml.SAMLLogoutProcessingFilter;
import org.springframework.security.saml.SAMLProcessingFilter;
import org.springframework.security.saml.SAMLWebSSOHoKProcessingFilter;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.security.saml.key.KeyManager;
import org.springframework.security.saml.log.SAMLDefaultLogger;
import org.springframework.security.saml.log.SAMLLogger;
import org.springframework.security.saml.metadata.CachingMetadataManager;
import org.springframework.security.saml.metadata.ExtendedMetadata;
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate;
import org.springframework.security.saml.metadata.MetadataDisplayFilter;
import org.springframework.security.saml.parser.ParserPoolHolder;
import org.springframework.security.saml.processor.HTTPArtifactBinding;
import org.springframework.security.saml.processor.HTTPPAOS11Binding;
import org.springframework.security.saml.processor.HTTPPostBinding;
import org.springframework.security.saml.processor.HTTPRedirectDeflateBinding;
import org.springframework.security.saml.processor.HTTPSOAP11Binding;
import org.springframework.security.saml.processor.SAMLBinding;
import org.springframework.security.saml.processor.SAMLProcessor;
import org.springframework.security.saml.processor.SAMLProcessorImpl;
import org.springframework.security.saml.util.VelocityFactory;
import org.springframework.security.saml.websso.ArtifactResolutionProfileImpl;
import org.springframework.security.saml.websso.SingleLogoutProfileImpl;
import org.springframework.security.saml.websso.WebSSOProfile;
import org.springframework.security.saml.websso.WebSSOProfileConsumer;
import org.springframework.security.saml.websso.WebSSOProfileConsumerHoKImpl;
import org.springframework.security.saml.websso.WebSSOProfileConsumerImpl;
import org.springframework.security.saml.websso.WebSSOProfileECPImpl;
import org.springframework.security.saml.websso.WebSSOProfileImpl;
import org.springframework.security.saml.websso.WebSSOProfileOptions;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.accept.ContentNegotiationStrategy;
import org.springframework.web.accept.HeaderContentNegotiationStrategy;

import com.getinsured.hix.platform.security.CustomPermissionEvaluator;
import com.getinsured.hix.platform.security.CustomWebSecurityExpressionHandler;
import com.getinsured.hix.platform.security.GhixUrlAuthenticationSuccessHandler;
import com.getinsured.hix.ws_trust.saml.GhixSamlContextProvider;
import com.getinsured.hix.ws_trust.saml.GhixSamlEntryPoint;
import com.getinsured.hix.ws_trust.saml.GhixSamlUserDetailsService;
import com.getinsured.platform.config.bootstrap.security.GhixSecurity;
import com.getinsured.platform.im.SAMLAuthenticationEntryPoint;
import com.getinsured.platform.im.util.SAMLMetadataProviderUtil;
import com.getinsured.platform.im.util.TenantMetadataUtil;
import com.getinsured.platform.util.Wso2XmlUtil;

/**
 * Replacement for applicationSecurity-saml.xml file.
 *
 * @author Yevgen Golubenko
 */
@GhixSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebMvcSecurity
@EnableWebSecurity
@Order(Ordered.LOWEST_PRECEDENCE - 100)
//@Order(Ordered.HIGHEST_PRECEDENCE)
public class SamlWebSecurityConfiguration extends WebSecurityConfigurerAdapter
{
  private static final Logger log = LoggerFactory.getLogger(SamlWebSecurityConfiguration.class);

  @Autowired
  private GhixSamlUserDetailsService ghixSamlUserDetailsService;

  @PostConstruct
  public void init()
  {
    if (log.isInfoEnabled())
    {
      log.info("Entering <SamlConfiguration> bootstrap phase.");
    }
  }

  @Override
  public void configure(AuthenticationManagerBuilder builder)
  {
    builder.authenticationProvider(samlAuthenticationProvider());
  }

  @Override
  public void configure(WebSecurity web) throws Exception
  {
    web.ignoring().antMatchers(UrlSecurityConfiguration.RESOURCES_PATH);
    web.debug("true".equals(System.getProperty("localdev"))); // TODO: Remove debugging before checkin
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception
  {
    final HttpSessionSecurityContextRepository repo = new HttpSessionSecurityContextRepository();
    repo.setDisableUrlRewriting(true);

    http.authenticationProvider(samlAuthenticationProvider());
    http.httpBasic().authenticationEntryPoint(samlAuthenticationEntryPoint());
    http.securityContext().securityContextRepository(repo);
    http.exceptionHandling().accessDeniedPage("/account/user/denied");
    http.authorizeRequests()
        .antMatchers(UrlSecurityConfiguration.UNSECURED_URL_LIST).permitAll()
        .antMatchers("/**", "/**/**").fullyAuthenticated();
    http.logout().deleteCookies("JSESSIONID", "SPRING_SECURITY_REMEMBER_ME_COOKIE", "SESSION").invalidateHttpSession(true).logoutSuccessUrl("/account/user/login");
    http.addFilterBefore(samlFilter(), BasicAuthenticationFilter.class);
    /*
     * Disable Sprint's CSRF; Use Getinsured custom CSRF token generation.
     */
    http.csrf().disable();
//
//    http.exceptionHandling()
//        .defaultAuthenticationEntryPointFor(
//            new GhixAuthenticationEntryPoint(),
//              acceptHeaderRequestMatcher());
  }

  @Bean
  public RequestMatcher acceptHeaderRequestMatcher()
  {
    ContentNegotiationStrategy contentNegotiationStrategy = new HeaderContentNegotiationStrategy();

    MediaTypeRequestMatcher mediaTypeRequestMatcher = new MediaTypeRequestMatcher(contentNegotiationStrategy,
        MediaType.APPLICATION_XHTML_XML,
        MediaType.TEXT_HTML,
        MediaType.TEXT_PLAIN,
        MediaType.APPLICATION_JSON,
        MediaType.APPLICATION_ATOM_XML,
        MediaType.APPLICATION_FORM_URLENCODED,
        MediaType.APPLICATION_XML,
        MediaType.APPLICATION_OCTET_STREAM,
        MediaType.MULTIPART_FORM_DATA,
        new MediaType("image","*")
    );
    mediaTypeRequestMatcher.setIgnoredMediaTypes(Collections.singleton(MediaType.ALL));

    return mediaTypeRequestMatcher;
  }

  @Bean
  public CustomWebSecurityExpressionHandler customExpressionHandler()
  {
    return new CustomWebSecurityExpressionHandler();
  }

  @Bean
  public FilterChainProxy samlFilter() throws Exception
  {
    final List<SecurityFilterChain> customFilters = new ArrayList<>(7);

    DefaultSecurityFilterChain dsfc = new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/login/**"), samlAuthenticationEntryPoint());
    customFilters.add(dsfc);
    customFilters.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/logout"), samlLogoutFilter()));
    customFilters.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/metadata/**"), metadataDisplayFilter()));

    try
    {
      customFilters.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SSO/**"), samlWebSSOProcessingFilter()));
      customFilters.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SSOHoK/**"), samlWebSSOHoKProcessingFilter()));
    } catch (Exception e)
    {
      log.error("Exception occurred while adding custom security filters: " + e.getMessage(), e);
      throw e;
    }

    customFilters.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SingleLogout/**"), samlLogoutProcessingFilter()));
    customFilters.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/discovery/**"), samlIDPDiscovery()));

    return new FilterChainProxy(customFilters);
  }

  @Bean(name = "keyManager")
  public KeyManager keyManager()
  {
    DefaultResourceLoader loader = new DefaultResourceLoader();
    Resource storeFile = loader
        .getResource("classpath:/saml/samlKeystore.jks");
    String storePass = "nalle123";
    Map<String, String> passwords = new HashMap<>();
    passwords.put("apollo", "nalle123");
    passwords.put("gi_hosted_sp", "nalle123");
    passwords.put("ms-starr", "nalle123");
    passwords.put("startcom", "nalle123");
    passwords.put("gi_qa", "nalle123");
    passwords.put("vishaka", "nalle123");
    passwords.put("sso.ghixqa.com", "nalle123");
    String defaultKey = "gi_hosted_sp";
    return new JKSKeyManager(storeFile, storePass, passwords, defaultKey);
  }

  @Autowired
  private TenantMetadataUtil tenantMetadataUtil;

  @Bean(name = "metadata")
  public CachingMetadataManager metadata() throws Exception
  {
    final List<MetadataProvider> providers = new ArrayList<>();
    MetadataProvider residentIdp = tenantMetadataUtil.getResidentIdentityProviderMetadata();
    String residentIdpEntityId = SAMLMetadataProviderUtil.getEntityId(residentIdp);

    tenantMetadataUtil.getSsoEnabledTenantConfigurations().forEach(config -> {
      MetadataProvider idpProvider = config.getIdentityProviderMetadata();

      if(idpProvider != null)
      {
        String entityId = SAMLMetadataProviderUtil.getEntityId(idpProvider);

        // If we do have idpProvider, but for example XML is invalid, we will not get entityId correctly.
        if (entityId != null && !entityId.equals(residentIdpEntityId))
        {
          log.info("Adding Identity provider configuration {}", entityId);
          providers.add(idpProvider);
        }

        MetadataProvider spProvider = config.getServiceProviderMetadata();

        if (spProvider != null)
        {
          providers.add(spProvider);
        }
      }
    });

    providers.add(residentIdp);

    CachingMetadataManager manager = new CachingMetadataManager(providers);
    return manager;
  }

  @Bean(name = "expressionHandler")
  public MethodSecurityExpressionHandler expressionHandler()
  {
    DefaultMethodSecurityExpressionHandler seh = new DefaultMethodSecurityExpressionHandler();
    seh.setPermissionEvaluator(customPermissionEvaluator());

    return seh;
  }

  @Bean(name = "customPermissionEvaluator")
  public CustomPermissionEvaluator customPermissionEvaluator()
  {
    return new CustomPermissionEvaluator();
  }

  @Bean(name = "parserPool", initMethod = "initialize")
  public ParserPool parserPool() throws XMLParserException
  {
    return new StaticBasicParserPool();
  }

  @Bean(name = "parserPoolHolder")
  public ParserPoolHolder parserPoolHolder()
  {
    return new ParserPoolHolder();
  }

  @Bean(name = "defaultSecurityConfiguration")
  public DefaultSecurityConfiguration defaultSecurityConfiguration()
  {
    return new DefaultSecurityConfiguration();
  }

  /**
   * Class loading incoming SAML messages from httpRequest stream
   *
   * @return instance of {@link SAMLProcessor}
   * @throws XMLParserException
   */
  @Bean(name = "processor")
  public SAMLProcessor processor() throws XMLParserException
  {
    final List<SAMLBinding> bindings = new ArrayList<>();
    bindings.add(redirectBinding());
    bindings.add(postBinding());
    bindings.add(artifactBinding());
    bindings.add(soapBinding());
    bindings.add(paosBinding());

    return new SAMLProcessorImpl(bindings);
  }

  /**
   * Logger for SAML messages and events.
   *
   * @return instance of {@link SAMLDefaultLogger}
   */
  @Bean(name = "samlLogger")
  public SAMLLogger samlLogger()
  {
    return new SAMLDefaultLogger();
  }

  /**
   * Entry point to initialize authentication, default values taken from properties file.
   *
   * @return instance of {@link GhixSamlEntryPoint}
   */
  @Bean(name = "samlEntryPoint")
  public GhixSamlEntryPoint samlEntryPoint()
  {
    final WebSSOProfileOptions options = new WebSSOProfileOptions();
    options.setIncludeScoping(false);
    options.setForceAuthN(true);

    final GhixSamlEntryPoint entry = new GhixSamlEntryPoint();
    entry.setDefaultProfileOptions(options);

    return entry;
  }

  /**
   * Returns SAML auth entry point bean.
   *
   * @return instance of {@link SAMLAuthenticationEntryPoint}
   */
  @Bean(name = "samlAuthenticationEtryPoint")
  public SAMLAuthenticationEntryPoint samlAuthenticationEntryPoint()
  {
    final WebSSOProfileOptions options = new WebSSOProfileOptions();
    options.setIncludeScoping(false);

    // This will be overridden in SAMLAuthenticationEntryPoint.getProfileOptions()
    options.setForceAuthN(true);

    final SAMLAuthenticationEntryPoint entry = new SAMLAuthenticationEntryPoint();
    entry.setDefaultProfileOptions(options);
    //entry.setDbEntryPoint(loginUrlAuthenticationEntryPoint());

    return entry;
  }

//  @Bean
//  public LoginUrlAuthenticationEntryPoint loginUrlAuthenticationEntryPoint()
//  {
//    final LoginUrlAuthenticationEntryPoint loginUrlAuthEntry = new LoginUrlAuthenticationEntryPoint(GhixPlatformEndPoints.LOGIN_PAGE + "?defaultLogin=1");
//    //loginUrlAuthEntry.setUseForward(true); // Because we don't use full url (e.g. http://host/dir/page)
//
//    return loginUrlAuthEntry;
//  }

  /**
   * IDP Discovery Service.
   *
   * @return instance of {@link SAMLDiscovery}
   */
  @Bean(name = "samlIDPDiscovery")
  public SAMLDiscovery samlIDPDiscovery()
  {
    final SAMLDiscovery sd = new SAMLDiscovery();
    sd.setIdpSelectionPath("security/idpSelection");

    return sd;
  }

  /**
   * Provider of default SAML Context.
   *
   * @return instance of {@link GhixSamlContextProvider}
   */
  @Bean(name = "contextProvider")
  public GhixSamlContextProvider contextProvider()
  {
    return new GhixSamlContextProvider();
  }

  /**
   * SAML Authentication Provider responsible for validating of received SAML messages with
   * optional property that can be used to store/load user data after login.
   */
  @Bean(name = "samlAuthenticationProvider")
  public SAMLAuthenticationProvider samlAuthenticationProvider()
  {
    final SAMLAuthenticationProvider authProvider = new SAMLAuthenticationProvider();
    authProvider.setUserDetails(ghixSamlUserDetailsService);
    authProvider.setForcePrincipalAsString(false);
    return authProvider;
  }

  @Bean(name = "samlWebSSOProcessingFilter")
  public SAMLProcessingFilter samlWebSSOProcessingFilter() throws Exception
  {
    final SAMLProcessingFilter fpf = new SAMLProcessingFilter();
    fpf.setAuthenticationManager(authenticationManager());
    fpf.setAuthenticationSuccessHandler(successRedirectHandler());
    fpf.setAuthenticationFailureHandler(failureRedirectHandler());

    return fpf;
  }

  @Bean(name = "samlWebSSOHoKProcessingFilter")
  public SAMLWebSSOHoKProcessingFilter samlWebSSOHoKProcessingFilter() throws Exception
  {
    final SAMLWebSSOHoKProcessingFilter pf = new SAMLWebSSOHoKProcessingFilter();
    pf.setAuthenticationManager(authenticationManager());
    pf.setAuthenticationSuccessHandler(successRedirectHandler());
    pf.setAuthenticationFailureHandler(failureRedirectHandler());
    return pf;
  }

  /**
   * Logout handler terminating local session.
   *
   * @return instance of {@link SecurityContextLogoutHandler}
   */
  @Bean(name = "logoutHandler")
  public SecurityContextLogoutHandler logoutHandler()
  {
    final SecurityContextLogoutHandler scLH = new SecurityContextLogoutHandler();
    scLH.setInvalidateHttpSession(true);

    return scLH;
  }

  @Bean(name = "successLogoutHandler")
  public SimpleUrlLogoutSuccessHandler successLogoutHandler()
  {
    final SimpleUrlLogoutSuccessHandler suLogoutHandler = new SimpleUrlLogoutSuccessHandler();
    suLogoutHandler.setDefaultTargetUrl("/");

    return suLogoutHandler;
  }

  @Bean(name = "successRedirectHandler")
  public GhixUrlAuthenticationSuccessHandler successRedirectHandler()
  {
    final GhixUrlAuthenticationSuccessHandler sr = new GhixUrlAuthenticationSuccessHandler();
    sr.setDefaultTargetUrl("/account/user/loginSuccess");
    sr.setBaseGhixAuthContext("/hix");

    return sr;
  }

  /**
   * Handler deciding where to redirect user after failed login
   *
   * @return instance of {@link SimpleUrlAuthenticationFailureHandler}
   */
  @Bean(name = "failureRedirectHandler")
  public SimpleUrlAuthenticationFailureHandler failureRedirectHandler()
  {
    final SimpleUrlAuthenticationFailureHandler fr = new SimpleUrlAuthenticationFailureHandler();
    fr.setUseForward(true);
    fr.setDefaultFailureUrl("/account/user/login");

    return fr;
  }

  @Bean(name = "passwordEncoder")
  public BCryptPasswordEncoder passwordEncoder()
  {
    return new BCryptPasswordEncoder();
  }

  @Bean(name = "saltSource")
  public ReflectionSaltSource saltSource()
  {
    final ReflectionSaltSource ss = new ReflectionSaltSource();
    ss.setUserPropertyToUse("uuid");
    return ss;
  }

  /**
   * Override default logout processing filter with the one processing SAML messages
   *
   * @return instance of {@link SAMLLogoutFilter}
   */
  @Bean(name = "samlLogoutFilter")
  public SAMLLogoutFilter samlLogoutFilter()
  {
    final LogoutHandler[] localAandGlobalhandlers = new LogoutHandler[]
        {logoutHandler()};

    return new SAMLLogoutFilter(successLogoutHandler(), localAandGlobalhandlers, localAandGlobalhandlers);
  }

  /**
   * Filter processing incoming logout messages
   * First argument determines URL user will be redirected to after successful global logout.
   *
   * @return instance of {@link SAMLLogoutProcessingFilter}
   */
  @Bean(name = "samlLogoutProcessingFilter")
  public SAMLLogoutProcessingFilter samlLogoutProcessingFilter()
  {
    return new SAMLLogoutProcessingFilter(successLogoutHandler(), logoutHandler());
  }

  @Bean(name = "artifactBinding")
  public HTTPArtifactBinding artifactBinding() throws XMLParserException
  {
    final HttpClient httpClient = new HttpClient(new MultiThreadedHttpConnectionManager());
    final ArtifactResolutionProfileImpl ar = new ArtifactResolutionProfileImpl(httpClient);
    ar.setProcessor(new SAMLProcessorImpl(soapBinding()));

    return new HTTPArtifactBinding(parserPool(), velocityEngine(), ar);
  }

  /**
   * SAML 2.0 WebSSO Assertion Consumer
   *
   * @return instance of {@link WebSSOProfileConsumerImpl}
   */
  @Bean(name = "webSSOprofileConsumer")
  public WebSSOProfileConsumer webSSOprofileConsumer()
  {
    WebSSOProfileConsumerImpl pc = new WebSSOProfileConsumerImpl();
    pc.setResponseSkew(3000);
    pc.setMaxAssertionTime(1800);

    return pc;
  }

  /**
   * SAML 2.0 Holder-of-Key WebSSO Assertion Consumer
   *
   * @return instance of {@link WebSSOProfileConsumerHoKImpl}
   */
  @Bean(name = "hokWebSSOprofileConsumer")
  public WebSSOProfileConsumerHoKImpl hokWebSSOprofileConsumer()
  {
    return new WebSSOProfileConsumerHoKImpl();
  }

  /**
   * SAML 2.0 Web SSO profile
   *
   * @return instance of {@link WebSSOProfile}
   */
  @Bean(name = "webSSOprofile")
  public WebSSOProfile webSSOprofile()
  {
    final WebSSOProfileImpl websso = new WebSSOProfileImpl();
    websso.setResponseSkew(3000);
    websso.setMaxAssertionTime(1800);
    return websso;
  }

  /**
   * SAML 2.0 Holder-of-Key Web SSO profile 
   *
   * @return instance of {@link WebSSOProfileConsumerHoKImpl}
   */
  @Bean(name = "hokWebSSOProfile")
  public WebSSOProfileConsumerHoKImpl hokWebSSOProfile()
  {
    return new WebSSOProfileConsumerHoKImpl();
  }

  /**
   * SAML 2.0 ECP profile
   *
   * @return instance of {@link WebSSOProfileECPImpl}
   */
  @Bean(name = "ecpprofile")
  public WebSSOProfileECPImpl ecpprofile()
  {
    return new WebSSOProfileECPImpl();
  }

  /**
   * SAML 2.0 Logout Profile 
   *
   * @return {@link SingleLogoutProfileImpl}
   */
  @Bean(name = "logoutprofile")
  public SingleLogoutProfileImpl logoutprofile()
  {
    return new SingleLogoutProfileImpl();
  }

  /**
   * Bindings, encoders and decoders used for creating and parsing messages 
   *
   * @return instance of {@link SAMLBinding}
   * @throws XMLParserException
   */
  @Bean(name = "postBinding")
  public SAMLBinding postBinding() throws XMLParserException
  {
    return new HTTPPostBinding(parserPool(), velocityEngine());
  }

  @Bean(name = "redirectBinding")
  public SAMLBinding redirectBinding() throws XMLParserException
  {
    return new HTTPRedirectDeflateBinding(parserPool());
  }

  @Bean(name = "soapBinding")
  public SAMLBinding soapBinding() throws XMLParserException
  {
    return new HTTPSOAP11Binding(parserPool());
  }

  @Bean(name = "paosBinding")
  public SAMLBinding paosBinding() throws XMLParserException
  {
    return new HTTPPAOS11Binding(parserPool());
  }

  @Bean(name = "sAMLBootstrap")
  public static SAMLBootstrap sAMLBootstrap()
  {
    return new SAMLBootstrap();
  }
  /*
  14:48:42.799 [localhost-startStop-1] WARN  o.s.c.a.ConfigurationClassEnhancer - @Bean method SamlConfiguration.sAMLBootstrap is non-static and returns an object assignable to Spring's BeanFactoryPostProcessor interface. This will result in a failure to process annotations such as @Autowired, @Resource and @PostConstruct within the method's declaring @Configuration class. Add the 'static' modifier to this method to avoid these container lifecycle issues; see @Bean Javadoc for complete details
  14:48:42.843 [localhost-startStop-1] DEBUG o.s.b.f.s.DefaultListableBeanFactory - Eagerly caching bean 'sAMLBootstrap' to allow for resolving potential circular references
  14:48:42.843 [localhost-startStop-1] DEBUG o.s.b.f.s.DefaultListableBeanFactory - Finished creating instance of bean 'sAMLBootstrap'
   */

  @Bean(name = "metadataDisplayFilter")
  public MetadataDisplayFilter metadataDisplayFilter()
  {
    return new MetadataDisplayFilter();
  }

  @Bean
  public VelocityEngine velocityEngine()
  {
    return VelocityFactory.getEngine();
  }

  private MetadataProvider getExtendedMetadataDelegate(String xmlString, final ExtendedMetadata extraMetaData)
  {
    DOMMetadataProvider provider;
    ExtendedMetadataDelegate delegate = null;
    try
    {
      provider = new DOMMetadataProvider(Wso2XmlUtil.getDocumentElement(xmlString));

      if (extraMetaData != null)
      {
        delegate = new ExtendedMetadataDelegate(provider, extraMetaData);
      } else
      {
        delegate = new ExtendedMetadataDelegate(provider);
      }
    } catch (Exception e)
    {
      if (log.isErrorEnabled())
      {
        log.error("Exception occurred while getting extended metadata provider for xml: {}", xmlString);
      }
    }

    return delegate;
  }

  private static String getFileName(final String fullPath)
  {
    return fullPath.substring(fullPath.lastIndexOf(File.separatorChar) + 1);
  }
}
