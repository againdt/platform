/**
 * 
 */
package com.getinsured.platform.config.bootstrap.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;

/**
 * Indicates that annotated class is related to the security of web-application.
 * 
 * Generally you would want to only annotate classes that needs to be loaded on the 'ghix-web' side, but not on 
 * *-svc components that are mostly &quot;headless&quot; or called from within ghix-web module.
 * 
 * @author Yevgen Golubenko
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ConditionalOnClass(name = "com.getinsured.hix.web.config.WebApplicationConfiguration")
public @interface GhixSecurity
{

}
