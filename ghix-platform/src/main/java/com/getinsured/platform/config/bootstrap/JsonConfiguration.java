package com.getinsured.platform.config.bootstrap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.getinsured.hix.platform.couchbase.converters.CouchbaseAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * JSON serializer configuration.
 *
 * @since 12/16/16
 * @author Yevgen Golubenko
 */
@Configuration
public class JsonConfiguration
{
  protected static final String JSON_DATE_FORMAT = "MMM dd, yyyy hh:mm:ss a";

  @Bean
  public Gson platformGson()
  {
    final GsonBuilder builder = new GsonBuilder();
    builder.serializeNulls();
    builder.disableHtmlEscaping();
    builder.setDateFormat(JSON_DATE_FORMAT);
    builder.registerTypeAdapterFactory(new CouchbaseAdapterFactory());
    builder.setLenient();

    return builder.create();
  }
}
