/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.getinsured.hix.platform.session.GiRedisHashValueSerializer;

/**
 * Redis Configuration.
 * 
 * @author Yevgen Golubenko.
 */
@Configuration
@ConditionalOnProperty(name = "redis.enabled", havingValue = "on", matchIfMissing = false)
public class RedisConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(RedisConfiguration.class);
  
  @Value("#{configProp['redis.host'] != null ? configProp['redis.host'] : 'f8f1710c0d2744d487c9e9ec94bb1420.publb.rackspaceclouddb.com'}")
  private String hostName;
  
  @Value("#{configProp['redis.port'] != null ? configProp['redis.port'] : '6379'}")
  private Integer port;
  
  @Value("#{configProp['redis.password'] != null ? configProp['redis.password'] : 'encPWAyzDMGs7v6gFURNURrkKpcuQUUzqwnu'}")
  private String password;
  
  @Value("#{configProp['redis.usepool'] != null ? configProp['redis.usepool'] : 'true'}")
  private Boolean usePool;
  
  @Value("#{configProp['redis.database'] != null ? configProp['redis.database'] : '1'}")
  private Integer databaseIndex;
  
  @Value("#{configProp['redis.timeout'] != null ? configProp['redis.timeout'] : '2000'}")
  private Integer timeout;

  @PostConstruct
  public void init()
  {
    if(log.isInfoEnabled())
    {
      log.info("Entering <RedisConfiguration> bootstrap phase.");
    }
  }
  
  @Bean
  @Lazy
  public JedisConnectionFactory jedisConnectionFactory()
  {
    final JedisConnectionFactory cf = new JedisConnectionFactory();
    
    cf.setHostName(hostName);
    cf.setPort(port);
    cf.setPassword(password);
    cf.setUsePool(usePool);
    cf.setDatabase(databaseIndex);
    cf.setTimeout(timeout);
    
    return cf;
  }

  /**
   * Redis template to be used by the platform.
   * <p>
   *  Bean alias 'sessionRedisTemplate' is required for Redis-backed Spring Session
   * </p>
   *
   * @return {@link RedisTemplate}
   */
  @Primary
  @Bean
  public RedisTemplate<?, ?> redisTemplate()
  {
    RedisTemplate<?, ?> redisTemplate = new RedisTemplate<>();

    redisTemplate.setConnectionFactory(jedisConnectionFactory());
    redisTemplate.setHashKeySerializer(new StringRedisSerializer());
    redisTemplate.setHashValueSerializer(new GiRedisHashValueSerializer());
    redisTemplate.setDefaultSerializer(new GiRedisHashValueSerializer());
    redisTemplate.setKeySerializer(new StringRedisSerializer());

    return redisTemplate;
  }

  /**
   * Redis Template used by Spring Session.
   *
   * @return The same Redis template as {@link #redisTemplate()}
   */
  @Bean
  @Primary
  public RedisTemplate<?, ?> sessionRedisTemplate()
  {
    return redisTemplate();
  }
}
