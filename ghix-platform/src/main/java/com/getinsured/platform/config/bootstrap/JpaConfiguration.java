/**
 *
 */
package com.getinsured.platform.config.bootstrap;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.getinsured.hix.platform.db.GHIXJpaDialect;

/**
 * Provides Java Configuration related to JPA. 
 * This is a replacement for all XML stuff.
 *
 * @author Yevgen Golubenko
 */
@Configuration
@Import({
    JpaScanConfiguration.class
})
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
@DependsOn({"configProp"})
public class JpaConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(JpaConfiguration.class);
  private static final String JNDI_JDBC_NAMESPACE = "java:comp/env/jdbc";
  private static final String JNDI_DATASOURCE_NAME = JNDI_JDBC_NAMESPACE + "/ghixDS";

  @Value("#{configProp['database.type']}")
  private String databaseType;

  @Value("#{configProp['hibernate.showsql']}")
  private String showSql;

  @PostConstruct
  public void init()
  {
    if (log.isInfoEnabled())
    {
      log.info("Entering <JpaConfiguration> bootstrap phase.");
    }

    if (databaseType == null || "".equals(databaseType))
    {
      databaseType = Database.POSTGRESQL.name();
    }
  }

  @Bean(name = "dataSource")
  public DataSource dataSource() throws Exception
  {
    final Context ctx = new InitialContext();
    DataSource ds = null;

    try
    {
      ds = (DataSource) ctx.lookup(JNDI_DATASOURCE_NAME);
    } catch (NameNotFoundException e)
    {
      if (log.isErrorEnabled())
      {
        log.error("You tried to bind JDBC via JNDI with name: {}", JNDI_DATASOURCE_NAME);

        try
        {
          NamingEnumeration<Binding> bindings = ctx.listBindings(JNDI_JDBC_NAMESPACE);
          log.error("But server context has only following names available:");

          while (bindings.hasMoreElements())
          {
            Binding b = bindings.next();
            log.error(JNDI_JDBC_NAMESPACE + "/" + b.getName());
          }
        } catch (NameNotFoundException nnf)
        {
          log.error("doh... :( Unable to list bindings for JNDI name", e);
        }
      }
    }

    assert ds != null : "Unable to find datasource with name: " + JNDI_DATASOURCE_NAME;

    return ds;
  }

  @Bean(name = "jpaVendorAdapter")
  public HibernateJpaVendorAdapter jpaVendorAdapter()
  {
    final HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
    adapter.setGenerateDdl(false);
    adapter.setShowSql("true".equals(showSql));

    // Grab database.type from configuration, if nothing there, default to POSTGRESQL
    if (databaseType != null && !"".equals(databaseType))
    {
      try
      {
        adapter.setDatabase(Database.valueOf(databaseType));
//        if(databaseType.toLowerCase().startsWith("postgres"))
//        {
//          adapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
//        }

        if (log.isInfoEnabled())
        {
          log.info("Using configuration.properties to fetch database type: [{}]", databaseType);
        }
      } catch (IllegalArgumentException e)
      {
        if (log.isErrorEnabled())
        {
          log.error("Exception while getting database type from configuration, no enum with [{}] name exists, defaulting to POSTGRESQL", databaseType);
        }

        adapter.setDatabase(Database.POSTGRESQL); // Postgres
        //adapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
      }
    } else
    {
      adapter.setDatabase(Database.POSTGRESQL); // ORACLE
      //adapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");

      if (log.isInfoEnabled())
      {
        log.info("Using default database type: [{}]", Database.POSTGRESQL);
      }
    }

    return adapter;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory()
  {
    final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();

    try
    {
      em.setDataSource(dataSource());
      em.setJpaVendorAdapter(jpaVendorAdapter());
      em.setJpaPropertyMap(getJpaPropertyMap());
      em.setJpaDialect(giJpaDialect());

      em.setPackagesToScan(
          "com.getinsured.hix",
          "com.getinsured.iex.ssap.model",
          "com.getinsured.eligibility.model",
          "com.getinsured.affiliate.model",
          "com.getinsured.hix.platform.emailstat.repository",
          "com.getinsured.hix.platform.repository"
      );
    } catch (Exception e)
    {
      log.error("Unable to configure LocalContainerEntityManagerFactoryBean with custom attributes", e);
    }

    return em;
  }

  /**
   * &lt;bean id=&quot;giJpaDialect&quot; class=&quot;com.getinsured.hix.platform.db.GHIXJpaDialect&quot; /&gt;
   * @return instance of {@link GHIXJpaDialect}
   */
  ///- @Bean
  public GHIXJpaDialect giJpaDialect()
  {
    return new GHIXJpaDialect();
  }

  /**
   * Additional JPA Properties.
   *
   * @return properties map.Ma
   */
  private Map<String, Object> getJpaPropertyMap()
  {
    Map<String, Object> pm = new HashMap<>();

    // Hibernate 4+
    // http://www.warski.org/blog/2011/04/envers-and-hibernate-4-0-0-alpha2-automatic-listener-registration/
    // No longer nessesary to register these listeners, if @Audited annotation is present, then it will be auto-registered.
    // For further customization of versioning http://javastudy.ru/hibernate/hibernate-envers-event-auditeventlistener
//    pm.put("hibernate.ejb.event.post-insert", "org.hibernate.ejb.event.EJB3PostInsertEventListener,org.hibernate.envers.event.AuditEventListener");
//    pm.put("hibernate.ejb.event.post-update", "org.hibernate.ejb.event.EJB3PostUpdateEventListener,org.hibernate.envers.event.AuditEventListener");
//    pm.put("hibernate.ejb.event.post-delete", "org.hibernate.ejb.event.EJB3PostDeleteEventListener,org.hibernate.envers.event.AuditEventListener");
//    pm.put("hibernate.ejb.event.pre-collection-update", "org.hibernate.envers.event.AuditEventListener");
//    pm.put("hibernate.ejb.event.pre-collection-remove", "org.hibernate.envers.event.AuditEventListener");
//    pm.put("hibernate.ejb.event.post-collection-recreate", "org.hibernate.envers.event.AuditEventListener");
//    
    pm.put("hibernate.format_sql", Boolean.FALSE);
    pm.put("hibernate.cache.use_second_level_cache", Boolean.FALSE);
    pm.put("hibernate.cache.use_query_cache", Boolean.FALSE);
    pm.put("hibernate.temp.use_jdbc_metadata_defaults", Boolean.FALSE);

    return pm;
  }

  @Bean
  public JpaTransactionManager transactionManager()
  {
    JpaTransactionManager tm = new JpaTransactionManager();
    LocalContainerEntityManagerFactoryBean emf = entityManagerFactory();

    if (emf != null)
    {
      tm.setEntityManagerFactory(emf.getObject());
    } else
    {
      log.error("LocalContainerEntityManagerFactoryBean was [{}], unable to set JpaTransactionManager.setEntityManagerFactory!", emf);
    }

    return tm;
  }
}
