/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import com.getinsured.hix.platform.location.service.AddressValidatorService;
import com.getinsured.hix.platform.location.service.jpa.AddressValidatorDefault;
import com.getinsured.hix.platform.location.service.jpa.AddressValidatorOEDQ;
import com.getinsured.hix.platform.location.service.jpa.AddressValidatorServiceImpl;
import com.getinsured.hix.platform.location.service.jpa.AddressValidatorSmartyStreet;
import com.getinsured.hix.platform.location.service.jpa.AddressValidatorUSPS;

/**
 * Address validation configuration.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class AddressValidationConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(AddressValidationConfiguration.class);
  
  @Autowired
  private WebHTTPConfiguration webHTTPConfiguration;
  
  @Value("#{configProp['smartystreet.url']}")
  private String smartyStreetUrl;
  
  @Value("#{configProp['smartystreet.authid']}")
  private String smartyStreetAuthId;
  
  @Value("#{configProp['smartystreet.authtoken']}")
  private String smartyStreetAuthToken;
  
  @Value("#{configProp['smartystreet.candidates']}")
  private String smartyStreetCandidates;
  
  public AddressValidationConfiguration()
  {
    if(log.isInfoEnabled())
    {
      log.info("Entering <AddressValidationConfiguration> bootstrap phase.");
    }
  }
  
  @Bean
  @Lazy
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public AddressValidatorService addressValidator()
  {
    // Looks like inside it's autowiring it's dependencies, so not sure why in XML it was trying
    // to set them explicitely. 
    return new AddressValidatorServiceImpl();
  }
  
  @Bean
  @Lazy
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public AddressValidatorSmartyStreet addressValidatorSmartyStreet()
  {
    final AddressValidatorSmartyStreet ss = new AddressValidatorSmartyStreet();
    
    ss.setSs_url(smartyStreetUrl);
    ss.setSs_authid(smartyStreetAuthId);
    ss.setSs_authkey(smartyStreetAuthToken);
    ss.setSs_candidates(smartyStreetCandidates);
    ss.sethIXHTTPClient(webHTTPConfiguration.hIXHTTPClient());
    
    return ss;
  }
  
  @Bean
  @Lazy
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public AddressValidatorDefault addressValidatorDefault()
  {
    return new AddressValidatorDefault();
  }
  
  @Bean
  @Lazy
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public AddressValidatorUSPS addressValidatorUSPS()
  {
    return new AddressValidatorUSPS();
  }
  
  @Bean
  @Lazy
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public AddressValidatorOEDQ addressValidatorOEDQ()
  {
    return new AddressValidatorOEDQ();
  }
}
