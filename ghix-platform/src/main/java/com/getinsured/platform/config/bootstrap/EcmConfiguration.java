/**
 *
 */
package com.getinsured.platform.config.bootstrap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.couchbase.CouchEcmService;
import com.getinsured.hix.platform.ecm.document.ContentService;
import com.getinsured.hix.platform.ecm.oracle.OracleContentManagementService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * ECM Configuration.
 *
 * @author Yevgen Golubenko
 */
@Configuration
public class EcmConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(EcmConfiguration.class);

  @Value("#{configProp['ecm.username']}")
  private String username;

  @Value("#{configProp['ecm.password']}")
  private String password;

  @Value("#{configProp['ecm.type']}")
  private String ecmType;

  @Autowired
  private CouchEcmService couchEcmService;

  @Autowired
  private ContentService contentService;

  @PostConstruct
  public void postConstruct()
  {
    if (log.isInfoEnabled())
    {
      log.info("Entering <EcmConfiguration> bootstrap phase.");
    }
  }

  @Bean(autowire = Autowire.BY_TYPE)
  @DependsOn({"couchEcmService", "gHIXApplicationContext", "contentService"})
  public ContentManagementService ecmService()
  {
    ContentManagementService service = null;

    if ("COUCH".equals(ecmType))
    {
      service = couchEcmService;
    } else if ("ORACLE".equals(ecmType))
    {
      service = new OracleContentManagementService();
    } else if("MSCONTENT".equals(ecmType))
    {
      service = contentService;
    }
    else
    {
      throw new GIRuntimeException("Invalid ecm.type specified in configuration.properties, " +
          "should be: COUCH, ORACLE or MSCONTENT, but [" + ecmType + "] was passed");
    }

    return service;
  }
}
