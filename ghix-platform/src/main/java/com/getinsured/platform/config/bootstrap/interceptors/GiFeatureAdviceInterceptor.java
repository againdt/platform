/*
 * Created by golubenko_y on 8/19/16.
 */
package com.getinsured.platform.config.bootstrap.interceptors;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.enums.YesNoEnum;
import com.getinsured.hix.platform.feature.GiFeature;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.TimeZone;

/**
 * Interceptor for components marked with {@link com.getinsured.hix.platform.feature.GiFeature} annotation.
 * <p>
 *   This is replacement for feature-aop.xml
 * </p>
 */
@Aspect
@Component
@Order(1000)
public class GiFeatureAdviceInterceptor
{
  private static Logger log = LoggerFactory.getLogger(GiFeatureAdviceInterceptor.class);
  private static final String YES = "Y";

  @Autowired
  private DynamicPropertiesUtil dynamicPropertiesUtil;

  // @GiFeature("ahbx.planmgmt.ind04.enabled")

  @PostConstruct
  public void postConstruct()
  {
    if(log.isInfoEnabled())
    {
      log.info("Initialized <GiFeatureAdviceInterceptor>");
    }
  }

  @Around("@within(com.getinsured.hix.platform.feature.GiFeature) || @annotation(com.getinsured.hix.platform.feature.GiFeature)")
  public Object giFeatureExecutionCheck(ProceedingJoinPoint jp) throws Throwable
  {
    try
    {
      MethodSignature signature = (MethodSignature) jp.getSignature();
      Method method = signature.getMethod();

      java.lang.reflect.Parameter[] params = method.getParameters();

      GiFeature giFeature = method.getAnnotation(GiFeature.class);

      String isFeatureEnabled = dynamicPropertiesUtil.getPropertyValue(giFeature.value());

      if (YES.equals(isFeatureEnabled) || YesNoEnum.YES.toString().equals(isFeatureEnabled))
      {
        if(log.isInfoEnabled())
        {
          log.info("Method [{}] annotated with @GiFeature, feature is enabled; allowing method to go through", method.getName());
        }

        return jp.proceed();
      }

      if(log.isInfoEnabled())
      {
        log.info("Method {} annotated with @GiFeature, but feature is not enabled in gi_app_config table: [{}], " +
            "not executing method and returning null", method.getName(), isFeatureEnabled);
      }

      return null;
    }
    catch (Exception e)
    {
      log.error("Error while intercepting method with @GiFeature annotation, " +
          "returning null because unable to determinate if feature is enabled or not", e);
    }

    return null;
  }
}
