package com.getinsured.platform.config.bootstrap;


import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.DelegatingFilterProxy;

import com.getinsured.platform.config.bootstrap.filters.StaticResourceFilter;

/**
 * Replacement for web.xml configuration.
 * <p>
 *   Currently it's only purpose is to create <code>springSessionRepositoryFilter</code> filter and
 *   register it with application server. Later once I checkin rest of annotation-based configuration
 *   this class will contain everything that used to be part of web.xml configuration.
 * </p>
 *
 * Created by golubenko_y on 9/2/16.
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class WebAppInitializer implements WebApplicationInitializer
{
  private static final Logger log = LoggerFactory.getLogger(WebAppInitializer.class);

  @PostConstruct
  private void postConstruct()
  {
    if(log.isInfoEnabled())
    {
      log.info("<WebAppInitialize> initialized");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void onStartup(ServletContext container) throws ServletException
  {
    final String useStaticResourceFilter = container.getInitParameter("useStaticResourceFilter");

    /*
     * Adding Static Resource Filter if useStaticResourceFilter context parameter is set to true.
     */
    if("true".equals(useStaticResourceFilter))
    {
      container.addFilter("staticResourceFilter", StaticResourceFilter.class).addMappingForUrlPatterns(null, false,
          StaticResourceFilter.URL_PATTERNS);

      // container.getContextPath(); // ''||/, hix, ghix-eapp, ghix-consumer-svc, etc.
    if(log.isInfoEnabled())
    {
        log.info("<WebAppInitializer> <filter> Static Resource Filter");
      }
    }

    /*
     * Adding Spring Session filter only if redis and spring session are enabled.
     */
    if ("on".equalsIgnoreCase(System.getProperty("redis.enabled")) &&
        "on".equalsIgnoreCase(System.getProperty("spring.session")))
    {
      container.addFilter("springSessionRepositoryFilter", DelegatingFilterProxy.class).addMappingForUrlPatterns(null, false, "/*");

      if(log.isInfoEnabled())
      {
        log.info("<WebAppInitializer> <filter> Spring Session Repository Filter");
      }
    }
  }
}
