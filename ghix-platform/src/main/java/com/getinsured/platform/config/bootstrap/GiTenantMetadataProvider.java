package com.getinsured.platform.config.bootstrap;


import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.provider.AbstractReloadingMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.ws_trust.saml.TenantSpMetadata;


public class GiTenantMetadataProvider extends AbstractReloadingMetadataProvider {
	private long tenantId = -1l;
	private TenantSpMetadata spMetadata;
	private Logger log = LoggerFactory.getLogger(GiTenantMetadataProvider.class);
	
	public GiTenantMetadataProvider(long tenantId, int spMetadataRefreshTime){
		super();
		this.tenantId = tenantId;
		this.setMinRefreshDelay(spMetadataRefreshTime);
		this.setMaxRefreshDelay(spMetadataRefreshTime+2*60*1000);
	}
	
	public void setTenantSpMetadata(TenantSpMetadata spMetadata){
		this.spMetadata = spMetadata;
	}
	
	public EntityDescriptor getEntityDescriptor(String name) throws MetadataProviderException{
		EntityDescriptor desc = super.getEntityDescriptor(name);
		if(desc != null){
			log.info("Returnin {}",desc.getEntityID());
			return desc;
		}
		return desc;
	}
	
	@Override
	protected String getMetadataIdentifier() {
		return this.spMetadata.getTenantCode(tenantId);
	}

	@Override
	protected byte[] fetchMetadata() throws MetadataProviderException {
		log.info("Tenant Id {} metadata cache is expired, initiating refresh", this.tenantId);
		this.spMetadata.refreshTenantConfiguration(this.tenantId);
		return this.spMetadata.getTenantSPMetadata(tenantId).getBytes();
	}
}