/**
 *
 */
package com.getinsured.platform.config.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

import com.getinsured.hix.platform.security.CustomPermissionEvaluator;
import com.getinsured.hix.platform.security.CustomWebSecurityExpressionHandler;

/**
 * @author Yevgen Golubenko
 */
//@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CustomPermissionEvaluatorWebSecurityConfig extends GlobalMethodSecurityConfiguration
{
  private static final Logger logger = LoggerFactory.getLogger(CustomPermissionEvaluatorWebSecurityConfig.class);

  public CustomPermissionEvaluatorWebSecurityConfig()
  {
    if (logger.isInfoEnabled())
    {
      logger.info("Entering <CustomPermissionEvaluatorWebSecurityConfig> bootstrap phase.");
    }
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception
  {

  }

  @Override
  protected MethodSecurityExpressionHandler createExpressionHandler()
  {
    DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
    expressionHandler.setPermissionEvaluator(new CustomPermissionEvaluator());

    return expressionHandler;
  }

  @Bean
  public CustomWebSecurityExpressionHandler customExpressionHandler()
  {
    return new CustomWebSecurityExpressionHandler();
  }
}
