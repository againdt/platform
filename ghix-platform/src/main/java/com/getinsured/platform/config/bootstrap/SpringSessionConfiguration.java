package com.getinsured.platform.config.bootstrap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.RedisHttpSessionConfiguration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * Configures Redis-based Spring Session.
 *
 * <p>
 *   If this is configured, it will require {@link JedisConnectionFactory}
 *   to be autowired, either custom or if custom not present, it will use
 *   default spring auto-configured factory.
 * </p>
 * <p>
 *   Initialization of this configuration only will take place if JVM attribute
 *   <code>-Dspring.session=on</code> is present.
 * </p>
 *
 * Created by golubenko_y on 8/24/16.
 */
@Configuration
@ConditionalOnProperty(name = "spring.session", havingValue = "on", matchIfMissing = false)
@DependsOn(value = {"jedisConnectionFactory"})
@EnableRedisHttpSession
public class SpringSessionConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(SpringSessionConfiguration.class);

  @PostConstruct
  public void postConstruct()
  {
    if(log.isInfoEnabled())
    {
      log.info("Initialized <SpringSessionConfiguration>");
    }
  }

  /**
   * Creates default implementation of {@link RedisHttpSessionConfiguration}
   * @return {@link RedisHttpSessionConfiguration}
   */
  @Bean
  public RedisHttpSessionConfiguration redisHttpSessionConfiguration()
  {
    return new RedisHttpSessionConfiguration();
  }

  /**
   * Configures cookie serialization strategy.
   *
   * @return configured {@link CookieSerializer}
   */
  @Bean
  @Primary
  public CookieSerializer defaultCookieSerializer()
  {
    DefaultCookieSerializer dcs = new DefaultCookieSerializer();
    dcs.setCookieName("SESSION");
    dcs.setCookiePath("/");

    return dcs;
  }
}
