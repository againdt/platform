package com.getinsured.platform.config.bootstrap;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.core.type.filter.RegexPatternTypeFilter;

/**
 * @author Yevgen Golubenko
 */
public class GhixComponentScanFilter extends RegexPatternTypeFilter
{
  private static final String PACKAGE_PATTERN = "[com.getinsured.hix|com.getinsured.hix.platform|org.springframework.security.saml]*\\." +
      "(model|serff|broker|repository|ws_trust|service|jpa|couchbase|helper|notification|security|util|notifs|saml|history|planmgmt|entity|iex|batch|account|inboxnotification)\\..*";

  public GhixComponentScanFilter()
  {
    super(
        Pattern.compile(
            PACKAGE_PATTERN
        )
    );
  }

  /**
   * TODO: Write Unit test for this.
   *
   * @param args
   */
  public static void main(String[] args)
  {
    Pattern p = Pattern.compile(
        PACKAGE_PATTERN
    );

    List<String> pkg = new ArrayList<>();

    pkg.add("com.getinsured.hix.platform.test.service.package.jpa.Class");
    pkg.add("com.getinsured.hix.platform.package.jpa.service.Test");
    pkg.add("com.getinsured.hix.platform.batch.helper.Class3");
    pkg.add("com.getinsured.hix.package.not.inlist.that.Class");
    pkg.add("com.getinsured.hix.platform.sample.couchbase.SampleClass");
    pkg.add("com.getinsured.not.scanned.Class");
    pkg.add("com.getinsured.hix.couchbase.VisiblePackageClass");
    pkg.add("com.getinsured.hix.platform.one.two.three.history.HistoryService");
    pkg.add("com.getinsured.entity.Some.OKEntity");
    pkg.add("org.springframework.security.saml.some.OKFile");
    pkg.add("com.getinsured.hix.planmgmt.OKFile");
    pkg.add("com.getinsured.hix.broker.history.OKFile");
    pkg.add("com.getinsured.iex.package.OKFile");
    pkg.add("com.getinsured.hix.serff.repository");
    pkg.add("com.getinsured.hix.platform.security.repository");
    pkg.add("com.getinsured.hix.model.ModelClass.ok");
    pkg.add("com.getinsured.hix.platform.account.inboxnotification");

    for (String s : pkg)
    {
      Matcher m = p.matcher(s);
      System.out.println(s + " => " + m.matches());
    }
    
    
    
    /*
     *
     *  component scans from xml config
    <context:component-scan base-package="org.springframework.security.saml" />
    <context:component-scan base-package="com.getinsured.hix.model" />
    <context:component-scan base-package="com.getinsured.hix.planmgmt" />
    <context:component-scan base-package="com.getinsured.hix.*" />
    <context:component-scan base-package="com.getinsured.entity.*" />
    <context:component-scan base-package="com.getinsured.hix.broker.history" />
    <context:component-scan base-package="com.getinsured.iex.*" />
     */
    
    /*
     *  "com.getinsured.hix.serff.repository",
        "com.getinsured.hix.serff.repository.templates",
        "com.getinsured.hix.repository",
        "com.getinsured.hix.*.repository",
        "com.getinsured.hix.*.*.repository",
        "com.getinsured.hix.*.*.*.repository",
        "com.getinsured.hix.platform.repository",
        "com.getinsured.hix.entity.repository",
        "com.getinsured.hix.platform.security.repository"
     */
  }
}
