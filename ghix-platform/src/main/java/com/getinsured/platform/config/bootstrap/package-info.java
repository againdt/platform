/**
 * Contains application annotation based configuration.
 * <p>
 *   This is replacement for all XML based configuration files found
 *   in the platform, as of this writing application is configured using one or many of these XML files:
 *  <ul>
 *    <li>applicationDB.xml</li>
 *    <li>applicationPlatform.xml</li>
 *    <li>applicationPlatform-default.xml</li>
 *    <li>applicationPlatform-java.xml</li>
 *    <li>applicationPlatform-test.xml</li>
 *    <li>applicationSecurity.xml</li>
 *    <li>applicationSecurity-cas.xml</li>
 *    <li>applicationSecurity-db.xml</li>
 *    <li>applicationSecurity-ghix.xml</li>
 *    <li>applicationSecurity-oam.xml</li>
 *    <li>applicationSecurity-phix-db.xml</li>
 *    <li>applicationSecurity-saml.xml</li>
 *    <li>applicationSecurity-saml_local.xml</li>
 *    <li>audit-aop-off.xml</li>
 *    <li>audit-aop-on.xml</li>
 *    <li>commonModuleDefinitions.xml</li>
 *    <li>feature-aop.xml</li>
 *    <li>platform-cache-context.xml</li>
 *    <li>platform-cache-off.xml</li>
 *    <li>platform-cache-on.xml</li>
 *    <li>redis.xml</li>
 *    <li>redis-off.xml</li>
 *    <li>redis-on.xml</li>
 *    <li>tenant-interceptor-off.xml</li>
 *    <li>tenant-interceptor-on.xml</li>
 *  </ul>
 * </p>
 * Created by golubenko_y on 9/1/16.
 */
package com.getinsured.platform.config.bootstrap;