/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.sendgrid.SendGrid;

/**
 * Email configuration.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class MailConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(MailConfiguration.class);
  
  @Value("#{configProp['mail.server.host']}")
  private String host;
  
  @Value("#{configProp['mail.server.port']}")
  private Integer port;
  
  @Value("#{configProp['mail.server.protocol']}")
  private String protocol;
  
  @Value("#{configProp['mail.server.username']}")
  private String username;
  
  @Value("#{configProp['mail.server.password']}")
  private String password;
  
  public MailConfiguration()
  {
    if(log.isInfoEnabled())
    {
      log.info("Entering <MailConfiguration> bootstrap phase.");
    }
  }
  
  @Bean
  public JavaMailSenderImpl mailSender()
  {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    
    mailSender.setHost(host);
    mailSender.setPort(port);
    mailSender.setProtocol(protocol);
    mailSender.setUsername(username);
    mailSender.setPassword(password);
    mailSender.setJavaMailProperties(javaMailProperties());
    
    return mailSender;
  }
 
  @Bean
  public SendGrid sendGridAPI()
  {
    final SendGrid sg = new SendGrid(username, password);
    return sg;
  }

  ///-@Bean
  public Properties javaMailProperties()
  {
    final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
    final Resource r = new ClassPathResource("javamail.properties");
        
    propertiesFactoryBean.setLocation(r);
    propertiesFactoryBean.setIgnoreResourceNotFound(false);
    
    Properties p = null;
    
    try
    {
      propertiesFactoryBean.afterPropertiesSet();
      p = propertiesFactoryBean.getObject();
    }
    catch (IOException e)
    {
      log.error("Error while initializing Java Mail Properties", e);
    }
    
    return p;
  }
}
