/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

//
//import org.opensaml.util.resource.ClasspathResource;
//import org.opensaml.util.resource.ResourceException;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;

import com.getinsured.hix.model.VimoEncryptor;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.google.code.springcryptoutils.core.cipher.Mode;
import com.google.code.springcryptoutils.core.cipher.asymmetric.Base64EncodedCipherer;
import com.google.code.springcryptoutils.core.cipher.asymmetric.Base64EncodedCiphererImpl;
import com.google.code.springcryptoutils.core.digest.Digester;
import com.google.code.springcryptoutils.core.digest.DigesterImpl;

/**
 * Encryption configuration. Should be drop-in replacement of
 * various &lt;crypt:[keystore|b64AsymmetricCiphere|digester|privateKey|publicKey] ... /&gt; XML configuration entries.
 * 
 * Also defines additional encryption/decryption stuff that was in XML file. Some of it is deprecated.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class EncryptionConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(EncryptionConfiguration.class);

  // TODO: Get these from properties
  private static final String KEYSTORE_FILE = "keystore-vimo.jks";
  private static final String KEYSTORE_PASS = "vimo123"; 
  private static final String KEY_ALIAS = "vimo";
  private static final String KEY_PASSWORD = "vimo123";
  
  @PostConstruct
  public void init()
  {
    if (log.isInfoEnabled())
    {
      log.info("Entering <EncryptionConfiguration> bootstrap phase.");
    }
  }
  
  @Bean(name = {"vimoEncryptor", "vimoencryptor"} )
  @Lazy
  public VimoEncryptor vimoEncryptor()
  {
    return new VimoEncryptor();
  }
  
  /**
   * I've kept this in place because it was still defined in XML. Abhai
   * said that Jasypt was removed from everywhere and not being used. However 
   * in some migration tools it's still referenced, so I've kept it until complete 
   * cleanup. 
   * 
   * @deprecated Don't use Jasypt enc/dec. Contact Abhai for details. 
   * @return {@link GhixJasyptEncrytorUtil}
   */
  @Bean
  @Deprecated
  @Lazy ///-
  public GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil()
  {
    return new GhixJasyptEncrytorUtil();
  }
  
  @Bean
  @Lazy
  public GhixEncryptorUtil ghixEncryptorUtil()
  {
    return new GhixEncryptorUtil();
  }

  @Bean(name = "vimoKeystore")
  public KeyStore key()
  {
    KeyStore ks = null;
    
    try
    {
       ks = KeyStore.getInstance("JKS");
       
       ClassPathResource cpResource = new ClassPathResource(KEYSTORE_FILE);
       
       if(cpResource.exists())
       {
         ks.load(cpResource.getInputStream(), KEYSTORE_PASS.toCharArray()); 
       }
       else
       {
         log.error("Error initializing KeyStore with JKS file [classpath:" + KEYSTORE_FILE + "] file not found!");
       }
    }
    catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e)
    {
      log.error("Unable to create KeyStore instance", e);
    }
    
    return ks;
  }

  @Bean(name = "digester1")
  public Digester digester()
  {
    DigesterImpl d = new DigesterImpl();
    d.setOutputMode(DigesterImpl.OutputMode.BASE64);
    d.setAlgorithm("SHA1");
    
    return d;
  }
  
  ///-@Bean
  public PublicKey pubKey()
  {
    KeyStore keystore = key();
    Certificate certificate = null;
    PublicKey pk = null;
    
    try
    {
      certificate = keystore.getCertificate(KEY_ALIAS);
      pk = certificate.getPublicKey();
    }
    catch (KeyStoreException e)
    {
      log.error("Unable to retrieve PublicKey from Certificate [" + certificate + "]", e);
    }
    
    return pk;
  }

  ///-@Bean
  public PrivateKey privKey()
  {
    final KeyStore keystore = key();
    PrivateKey key = null;
    
    try
    {
      key = (PrivateKey) keystore.getKey(KEY_ALIAS, KEY_PASSWORD.toCharArray());
    }
    catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e)
    {
      log.error("Unable to retrieve PrivateKey from configured KeyStore file", e);
    }
    
    return key;
  }

  @Bean
  public Base64EncodedCipherer encrypter()
  {
    Base64EncodedCiphererImpl cip = new Base64EncodedCiphererImpl();
    cip.setMode(Mode.ENCRYPT);
    cip.setKey(privKey());
    
    return cip;
  }

  @Bean
  public Base64EncodedCipherer decrypter()
  {
    Base64EncodedCiphererImpl cip = new Base64EncodedCiphererImpl();
    cip.setMode(Mode.DECRYPT);
    cip.setKey(pubKey());
    return cip;
  }
}
