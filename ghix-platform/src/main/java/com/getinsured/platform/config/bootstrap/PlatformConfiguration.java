/**
 *
 */
package com.getinsured.platform.config.bootstrap;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.getinsured.hix.platform.config.CacheConfiguration;
import com.getinsured.hix.platform.couchbase.helper.DocumentHelper;
import com.getinsured.hix.platform.file.manager.FileManagerService;
import com.getinsured.hix.platform.file.manager.factory.FileManagerFactory;
import com.getinsured.hix.platform.file.manager.factory.FileManagerFactory.FileManager;
import com.getinsured.hix.platform.service.GIAppConfigService;
import com.getinsured.hix.platform.service.jpa.GIAppConfigServiceImpl;
import com.getinsured.hix.platform.util.ValidationUtility;
import com.getinsured.hix.platform.util.GsonFactory;
import com.getinsured.platform.config.bootstrap.interceptors.GiFeatureAdviceInterceptor;
import com.getinsured.platform.config.bootstrap.interceptors.TenantDBInterceptor;
import com.google.gson.Gson;

/**
 * Global platform-wide configuration. 
 * This is a replacement of XML configuration.
 *
 * @author Yevgen Golubenko
 */
@Configuration
@Import({
    PropertiesConfiguration.class,
    ComponentScanConfiguration.class,
    JpaConfiguration.class,
    SamlWebSecurityConfiguration.class,
    DaoWebSecurityConfiguration.class,
    WebHTTPConfiguration.class,
    AddressValidationConfiguration.class,
    EncryptionConfiguration.class,
    EcmConfiguration.class,
    MailConfiguration.class,
    FTPConfiguration.class,
    SMSConfiguration.class,
    SecurityWebApplicationInitializer.class,
    TenantDBInterceptor.class,
    GiFeatureAdviceInterceptor.class,
    RedisConfiguration.class,
    SpringSessionConfiguration.class,
    CacheConfiguration.class,
    JsonConfiguration.class
})
@EnableAspectJAutoProxy
public class PlatformConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(PlatformConfiguration.class);

  @PostConstruct
  public void postConstruct()
  {
    if (log.isInfoEnabled())
    {
      log.info("<PlatformConfiguration> bootstrap phase: postConstruct()");
    }
  }
 
  @Bean(name= "couchDocumentHelper")
  public DocumentHelper documentHelper()
  {
    return new DocumentHelper();
  }

  @Bean
  public ValidationUtility validationUtility()
  {
    return new ValidationUtility();
  }

  @Bean(name = "jsonConverter")
  public MappingJackson2HttpMessageConverter jsonConverter()
  {
    final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));

    return converter;
  }
  
  @Bean(name="gsonFactory")
  public GsonFactory gsonFactory()
  {
	  return new GsonFactory();
  }
  
  @Bean(name="platformGson")
  public Gson getGson(){
	  return gsonFactory().getObject();
  }

  @Bean
  public PagedResourcesAssembler<?> pagedResourcesAssembler()
  {
    final PagedResourcesAssembler<HateoasPageableHandlerMethodArgumentResolver> pra = new PagedResourcesAssembler<>(new HateoasPageableHandlerMethodArgumentResolver(), null);
    return pra;
  }

  @Bean
  public FileManagerService fileManagerService()
  {
    final FileManagerService fms = FileManagerFactory.createFileManagerService(FileManager.FILE.name());
    return fms;
  }

  // TODO: This bean also defined in CacheConfiguration when cache.enabled=couch, remove it from there when adding this to repository
  @Bean
  public GIAppConfigService giAppConfigService()
  {
    return new GIAppConfigServiceImpl();
  }
}
