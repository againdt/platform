/**
 * 
 */
package com.getinsured.platform.config.bootstrap.interceptors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepositoryImpl;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

/**
 * Interceptor for Database calls when Tenant information is available.
 * <p>
 * This is replacement for tenant-interceptor-on.xml file configuration.
 * </p>
 *
 * @author Yevgen Golubenko
 */
@Aspect
@Component
public class TenantDBInterceptor
{
  private static final String TENANT_ID = "tenantId";
  private static final String TENANT_ID_FILTER = "tenantIdFilter";
  private static Logger log = LoggerFactory.getLogger(TenantDBInterceptor.class);

  @Around("execution(* com.getinsured.hix.platform.repository.TenantAwareRepository+.*(..))")
  public Object enableTenantFilter(ProceedingJoinPoint jp) throws Throwable
  {
    try
    {
      if (TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null)
      {
        Object repositoryObject = ((Advised) jp.getTarget()).getTargetSource().getTarget();
        if (repositoryObject instanceof EnversRevisionRepositoryImpl)
        {
          @SuppressWarnings("rawtypes")
          EnversRevisionRepositoryImpl enversRevisionRepositoryImpl = (EnversRevisionRepositoryImpl) repositoryObject;

          Session session = (Session) enversRevisionRepositoryImpl.getEntityManager().getDelegate();

          if (session.isOpen())
          {
            Filter filter = session.enableFilter(TENANT_ID_FILTER);
            filter.setParameter(TENANT_ID, TenantContextHolder.getTenant().getId());
          }
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error enabling tenant filter", e);
    }
    return jp.proceed();
  }

  @Around("execution(* com.getinsured.hix.platform.repository.TenantUnawareRepository+.*(..))")
  public Object disableTenantFilter(ProceedingJoinPoint jp) throws Throwable
  {
    try
    {
      Object repositoryObject = ((Advised) jp.getTarget()).getTargetSource().getTarget();
      
      if (repositoryObject instanceof EnversRevisionRepositoryImpl)
      {
        @SuppressWarnings("rawtypes")
        EnversRevisionRepositoryImpl enversRevisionRepositoryImpl = (EnversRevisionRepositoryImpl) repositoryObject;
        Session session = (Session) enversRevisionRepositoryImpl.getEntityManager().getDelegate();
        if (session.isOpen())
        {
          session.disableFilter(TENANT_ID_FILTER);
        }
      }
    } 
    catch (Exception e)
    {
      log.error("Error disabling tenant filter", e);
    }
    
    return jp.proceed();
  }
}
