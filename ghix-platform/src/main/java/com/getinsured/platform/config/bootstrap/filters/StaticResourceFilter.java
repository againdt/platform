package com.getinsured.platform.config.bootstrap.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StaticResourceFilter implements Filter
{
  // ghix-web.war/resources:
  // ls -R | egrep  '(\.\w+)$' -o | sort | uniq -c | sort -r
  public static final String[] URL_PATTERNS = new String[] {
      "*.js", "*.json", "*.css", "*.png", "*.jpg", "*.gif", "*.flv", "*.woff", "*.woff2", "*.map", "*.ttf",
      "*.svg", "*.swf", "*.psd", "*.ps1", "*.scss", "*.sh", "*.tmpl", "*.txt", "*.yml", "*.st", "*.srl",
      "*.otf", "*.markdown", "*.less", "*.jpeg", "*.ico", "*.htc", "*.eps", "*.csv", "*.csr",
      "*.conf", "*.cfg", "*.as", "*.ai", "*.cur", "*.html"
  };

  private RequestDispatcher defaultRequestDispatcher;
  private static final Logger log = LoggerFactory.getLogger(StaticResourceFilter.class);

  @Override
  public void destroy() {}

  /*
   * Static Resource Filter (applicable for /resources/**)
   * Skips All the Other filters defined in the application and serves the handles the control to application's default dispatcher servlet.
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException
  {
    if(log.isDebugEnabled())
    {
      log.debug("static resource: {} query str: {}", ((HttpServletRequest) request).getRequestURL(),
          ((HttpServletRequest) request).getQueryString());
    }

    defaultRequestDispatcher.forward(request, response);
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {
    this.defaultRequestDispatcher = filterConfig.getServletContext().getNamedDispatcher("appServlet");

    if(this.defaultRequestDispatcher == null)
    {
      log.error("Static Resource Filter could not be initialized, check dispatcher initialization inside of init()!");
    }
  }
}
