/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import java.util.Collections;

import javax.annotation.PostConstruct;

import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.getinsured.hix.platform.security.RestHeaderInterceptor;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.HttpErrorHandler;
import com.getinsured.hix.platform.util.SecureHttpClient;

/**
 * Web/HTTP related configuration.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class WebHTTPConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(WebHTTPConfiguration.class);

  @Value("#{configProp['httpClient.defaultReadTimeout']}")
  private int httpClientDefaultReadTimeout;

  @PostConstruct
  public void init()
  {
    if (log.isInfoEnabled())
    {
      log.info("Entering <WebHTTPConfiguration> bootstrap phase.");
    }
  }

  @Bean
  @Lazy
  public SaajSoapMessageFactory messageFactory()
  {
    return new SaajSoapMessageFactory();
  }

  ///-@Bean
  public HttpComponentsMessageSender httpComponentsMessageSender()
  {
    final HttpComponentsMessageSender ms = new HttpComponentsMessageSender();
    ms.setHttpClient(restHttpClient());
    //ms.setConnectionTimeout(120000); // 2 minutes
    //ms.setReadTimeout(60000); // 1 minute
    //ms.setMaxTotalConnections(10);
    return ms;
  }

  @Bean
  @Lazy
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public WebServiceTemplate webServiceTemplate()
  {
    final WebServiceTemplate t = new WebServiceTemplate(messageFactory());
    t.setMessageSender(httpComponentsMessageSender());
    return t;
  }

  @Bean
  @Lazy
  public HIXHTTPClient hIXHTTPClient()
  {
    final HIXHTTPClient c = new HIXHTTPClient();
    c.setWebServiceTemplate(webServiceTemplate());
    return c;
  }
  
  /**
   * Returns custom cloaseable HttpClient. This bean initialized only after
   * {@link GhixPlatformConstants} class.
   * 
   * @return cloaseable secure http client implementation.
   */
  @Bean(name = "restHttpClient")
  @DependsOn("platformConstants")
  @Lazy
  public HttpClient restHttpClient()
  {
    return SecureHttpClient.getHttpClient();
  }

  @Bean
  @Lazy
  public RestTemplate restTemplate()
  {
    final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    requestFactory.setReadTimeout(httpClientDefaultReadTimeout);
    requestFactory.setHttpClient(restHttpClient());
    
    final RestTemplate template = new RestTemplate(requestFactory);

   // template.setInterceptors(Collections.singletonList(new RestHeaderInterceptor()));
    template.setErrorHandler(new HttpErrorHandler());
    return template;
  }
}
