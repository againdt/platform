/**
 * 
 */
package com.getinsured.platform.config.bootstrap;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.getinsured.hix.platform.sms.service.SmsService;
import com.getinsured.hix.platform.sms.service.factory.SmsFactory;

/**
 * SMS Service Configuration.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class SMSConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(SMSConfiguration.class);
  
  @Value("#{configProp['sms.provider']}")
  private String provider;
  
  @Value("#{configProp['sms.provider.accountsid']}")
  private String accountSid;
  
  @Value("#{configProp['sms.provider.authtoken']}")
  private String authToken;
  
  @Value("#{configProp['sms.provider.fromphone']}")
  private String fromPhone;
  
  @Value("#{configProp['sms.provider.host']}")
  private String host;
  
  @Value("#{configProp['sms.provider.integrationKey']}")
  private String integrationKey;
  
  @Value("#{configProp['sms.provider.secretKey']}")
  private String secretKey;
  
  public SMSConfiguration()
  {
    if(log.isInfoEnabled())
    {
      log.info("Entering <SMSConfiguration> bootstrap phase.");
    }
  }
  
  @Bean
  @Lazy
  public SmsService smsService()
  {
    final SmsService service = SmsFactory.createSmsService(smsProvider());
    
    return service;
  }

  private Properties smsProvider()
  {
    final Properties p = new Properties();
    
    p.put("provider", provider);
    p.put("accountSid", accountSid);
    p.put("authToken", authToken);
    p.put("fromPhone", fromPhone);
    p.put("host", host);
    p.put("integrationKey", integrationKey);
    p.put("secretKey", secretKey);
    
    return p;
  }
}
