package com.getinsured.platform.config.bootstrap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepositoryFactoryBean;

/**
 * Created by golubenko_y on 1/10/17.
 */
@Configuration
@EnableJpaRepositories(
    repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class,
    basePackages = {
        // @Repository
        "com.getinsured.hix.platform.accountactivation.repository",
        "com.getinsured.hix.platform.accountactivation.service.jpa",
        "com.getinsured.hix.platform.comment.service.jpa",
        "com.getinsured.hix.platform.gimonitor.repository",
        "com.getinsured.hix.platform.giwspayload.repository",
        "com.getinsured.hix.platform.location.service.jpa",
        "com.getinsured.hix.platform.repository",
        "com.getinsured.hix.platform.security.repository",
        "com.getinsured.hix.platform.security.service.jpa",
        "com.getinsured.hix.platform.service.jpa",
        "com.getinsured.hix.platform.useraud.repository",
        "com.getinsured.hix.platform.security.service.impl", // PasswordServiceImpl


        // JpaRepository
        "com.getinsured.hix.platform.account.repository",
        "com.getinsured.hix.platform.audit.envers.repository.support",
        "com.getinsured.hix.platform.comment.repository",
        "com.getinsured.hix.platform.emailstat.repository",
        "com.getinsured.hix.platform.eventlog.repository",
        "com.getinsured.hix.platform.gimonitor.repository",
        "com.getinsured.hix.platform.lookup.repository",
        "com.getinsured.hix.platform.payments.repository",

        // CrudRepository
        "com.getinsured.hix.platform.cache.repository",

        // EnversRevisionRepository
        "com.getinsured.hix.platform.audit.envers.repository.support",
        "com.getinsured.hix.pmms.repository",
        "com.getinsured.affiliate.repository"
    }
)
public class JpaScanConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(JpaScanConfiguration.class);

  @PostConstruct
  public void init()
  {
    if(log.isInfoEnabled())
    {
      log.info("<JpaScanConfiguration> initialization complete");
    }
  }
}
