package com.getinsured.platform.config.bootstrap;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixEncryptablePropertyConfigurer;
import com.getinsured.platform.util.GhixHome;

/**
 * Configures all properties used by the platform. For example
 * makes 'build.properties' and 'configuration.properties' available to the application.
 * 
 * @author Yevgen Golubenko
 */
@Configuration
public class PropertiesConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(PropertiesConfiguration.class);

  private static final String GHIX_HOME = GhixHome.getGhixHome();
  private static final String GLOBAL_CONFIGURATION_FILE_PATH = "/ghix-setup/conf/configuration.properties";
  private static final String GLOBAL_SAML_CONFIGURATION_FILE_PATH = "/ghix-setup/conf/saml/saml.properties";
  
  private static final FileSystemResource CONFIGURATION_PROPERTIES_RESOURCE = new FileSystemResource(GHIX_HOME + GLOBAL_CONFIGURATION_FILE_PATH);
  private static final FileSystemResource CONFIGURATION_SAML_PROPERTIES_RESOURCE = new FileSystemResource(GHIX_HOME + GLOBAL_SAML_CONFIGURATION_FILE_PATH);

  @PostConstruct
  public void init()
  {
    if(log.isInfoEnabled())
    {
      log.info("Entering <PropertiesConfiguration> bootstrap phase.");
    }
  }
  
  /**
   * Configured instance of {@link DynamicPropertiesUtil}. This is database 
   * driven instance.
   *  
   * @return configured instance of {@link DynamicPropertiesUtil}
   */
  @Bean
  @Lazy
  public DynamicPropertiesUtil dynamicPropertiesUtil()
  {
    final DynamicPropertiesUtil dpu = new DynamicPropertiesUtil();
    dpu.customInit();
    
    return dpu;
  }

  /**
   * Instance of Properties for saml.properties.
   *
   * @return {@link PropertiesFactoryBean} which would be instanciated by Spring.
   */
  @Bean
  public PropertiesFactoryBean samlProp1()
  {
    final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();

    propertiesFactoryBean.setIgnoreResourceNotFound(true);
    propertiesFactoryBean.setLocalOverride(true);
    propertiesFactoryBean.setLocation(CONFIGURATION_SAML_PROPERTIES_RESOURCE);

    if(!CONFIGURATION_SAML_PROPERTIES_RESOURCE.exists())
    {
      if(log.isWarnEnabled())
      {
        log.warn("Unable to find: [" + CONFIGURATION_SAML_PROPERTIES_RESOURCE.getFilename() + "]");
        log.warn("[GHIX_HOME] => [" + GHIX_HOME + "], SAML Properties File Path Relative to GHIX_HOME: " + CONFIGURATION_SAML_PROPERTIES_RESOURCE);
        log.warn("Will be using defaults in SamlWebSecurityConfiguration instead of saml.properties");
      }
    }

    return propertiesFactoryBean;
  }
  
  /**
   * Instance of Properties for configuration.properties.
   * 
   * @return {@link PropertiesFactoryBean} which would be instanciated by Spring.
   */
  @Bean(name = {"configProp","samlProp"})
  public PropertiesFactoryBean configProp()
  {
    final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
    
    if(!CONFIGURATION_PROPERTIES_RESOURCE.exists())
    {
      log.error("Unable to find: [" + 
          CONFIGURATION_PROPERTIES_RESOURCE.getFilename() + "]");
      log.error("Application startup will fail. Debug: [GHIX_HOME] => [" 
          + GHIX_HOME + "], File Path Relative to GHIX_HOME: " 
          + GLOBAL_CONFIGURATION_FILE_PATH);
    }
    
    propertiesFactoryBean.setLocation(CONFIGURATION_PROPERTIES_RESOURCE);
    propertiesFactoryBean.setIgnoreResourceNotFound(false);
    propertiesFactoryBean.setLocalOverride(true);

    if (log.isInfoEnabled())
    {
      if (CONFIGURATION_PROPERTIES_RESOURCE.exists())
      {
        log.info("=> loaded " + GHIX_HOME + GLOBAL_CONFIGURATION_FILE_PATH + " file for properties");
      }
    }

    if (!CONFIGURATION_PROPERTIES_RESOURCE.exists())
    {
      log.error("Unable to load: " + GHIX_HOME + GLOBAL_CONFIGURATION_FILE_PATH + " file for properties, application startup will fail.");
    }

    return propertiesFactoryBean;
  }

  /**
   * Instance of Properties for build-number.properties.
   *
   * @return {@link PropertiesFactoryBean} which would be instanciated by Spring.
   */
  @Bean
  public Properties buildProp() throws IOException
  {
    final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
    final Resource r = new ClassPathResource("/build-number.properties");
        
    propertiesFactoryBean.setLocation(r);
    propertiesFactoryBean.setIgnoreResourceNotFound(false);
    propertiesFactoryBean.afterPropertiesSet();
    
    Properties p = propertiesFactoryBean.getObject();
    
    if(log.isDebugEnabled())
    {
      log.debug("=> buildProperties: " + p);
    }

    return p;
  }
  
  @Bean
  public static GhixEncryptablePropertyConfigurer propertyConfigurer()
  {
    final GhixEncryptablePropertyConfigurer pc = new GhixEncryptablePropertyConfigurer();
    pc.setLocation(CONFIGURATION_PROPERTIES_RESOURCE);
    
    return pc;
  }

  @Bean
  @ConditionalOnMissingBean(MessageSource.class)
  public MessageSource messageSource()
  {
    final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:/i18n/Account_exceptions");
    messageSource.setDefaultEncoding("UTF-8");

    return messageSource;
  }
}
