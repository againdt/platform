package com.getinsured.platform.config.bootstrap;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

/**
 * Configuration for XML-based file configurations. This is port of XML config from applicationContext.xml files
 * to support multi-state environments.
 *
 * @author Yevgen Golubenko
 * @since 4/11/19
 */
@Component
public class XmlConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(XmlConfiguration.class);

  private final ApplicationContext appContext;
  private String stateCode;

  @Autowired
  public XmlConfiguration(ApplicationContext appContext) {
    this.appContext = appContext;
  }

  @PostConstruct
  public void postConstruct() {
    this.stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
  }

  @Bean
  public DefaultConfigurationBuilder xmlConfig() {
    DefaultConfigurationBuilder dcb = new DefaultConfigurationBuilder();

    log.info("Loading XML configurations for: {}", this.stateCode);

    switch(this.stateCode) {
      case "CA":
        setConfigurationFileFromResource(dcb, appContext.getResource("classpath:appconf/ca/ghix-configuration.xml"));
        break;
      case "ID":
        setConfigurationFileFromResource(dcb, appContext.getResource("classpath:appconf/id/planCostMetaData.xml"));
        break;
      default:
        log.info("There is no state specific configuration for state: {}, using root level configuration.", this.stateCode);
        setConfigurationFileFromResource(dcb, appContext.getResource("classpath:appconf/ghix-configuration.xml"));
        break;
    }

    return dcb;
  }

  /**
   * Tries to set XML configuration file for {@link DefaultConfigurationBuilder} from given {@link Resource}.
   * @param dcb {@link DefaultConfigurationBuilder} object
   * @param resource {@link Resource} object that identifies resource location (in JAR, path, etc.)
   */
  private void setConfigurationFileFromResource(final DefaultConfigurationBuilder dcb, final Resource resource)
  {
    if(resource.exists() && resource.isReadable())
    {
      try
      {
        dcb.setURL(resource.getURL());
        log.info("Loaded XML configuration from: {}", resource.getURL());
      }
      catch (IOException e)
      {
        log.error("Unable to read XML configuration resource: {}, error: {}", resource.getFilename(), e.getMessage());
        log.error("Exception occurred while reading XML configuration resource", e);
      }
    }
    else {
      log.error("Unable to read XML configuration resource: {}, file exists: {}, readable: {}",
          resource.getFilename(), resource.exists(), resource.isReadable());
    }
  }
}
