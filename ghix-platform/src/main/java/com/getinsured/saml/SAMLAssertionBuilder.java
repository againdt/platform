package com.getinsured.saml;

import java.util.Map;

import org.joda.time.DateTime;
import org.opensaml.saml2.core.Assertion;

public interface SAMLAssertionBuilder {

    public void init() throws Exception;

    /**
     * Encrypt the SAML assertion
     *
     * @param authReqDTO   SAML assertion to be encrypted
     * @param notOnOrAfter Encrypting credential
     * @param sessionId    Certificate alias against which use to Encrypt the assertion.
     * @return Assertion
     * @throws IdentityException
     */

    public Assertion buildAssertion(Map<String,Object> context, Map<String,String> assertions, DateTime notBefore, DateTime notOnOrAfter,
                                    String sessionId) throws Exception;

}
