package com.getinsured.saml;

import java.io.ByteArrayOutputStream;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.EncryptedAssertion;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.StatusMessage;
import org.opensaml.saml2.core.impl.ResponseMarshaller;
import org.opensaml.saml2.core.impl.StatusBuilder;
import org.opensaml.saml2.core.impl.StatusCodeBuilder;
import org.opensaml.saml2.core.impl.StatusMessageBuilder;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.encryption.EncryptionConstants;
import org.opensaml.xml.signature.SignatureConstants;
import org.opensaml.xml.util.XMLHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import com.getinsured.hix.platform.util.GhixPlatformKeystore;
import com.getinsured.saml.payment.IssuerConfig;

@Component
public class SAMLResponseBuilder {
	private static Logger logger = LoggerFactory.getLogger(SAMLResponseBuilder.class);
	@Autowired
	private MetadataLoader metadaLoader;
	@Autowired
	private GhixPlatformKeystore pks;
	
	private static boolean init = false;
	
	@PostConstruct
	public void init() {
		try {
			if(!init) {
				DefaultBootstrap.bootstrap();
				org.apache.xml.security.Init.init();
				if(this.metadaLoader == null) {
					this.metadaLoader = new MetadataLoader();
					this.pks = new GhixPlatformKeystore();
				}
				init = true;
			}
		} catch (ConfigurationException e) {
			logger.error("Error bootstrapping the SAML",e);
		}
	}
	
	public String getPaymentAuthURL(String partnerEntityId) {
		return this.metadaLoader.getSPAssertionConsumerUrl(partnerEntityId);
	}
	
	public String buildSAMLResponse(
			boolean encryptAssertions,
			String authAddress,
			String authDNS,
			String partnerEntityId,
			String paymentDestinationUrl,
			String exchangeEntityId,
			String exchangeSigningKey,
			Map<String, String> claims, String signatureAlgoId, String digestAlgoId) throws Exception {
		init();
		String sessionId = SAMLUtils.generateSessionId();
		HashMap<String,Object> context = new HashMap<>();
		context.put(SAMLConstants.RESPONSE_ID_PREFIX,exchangeEntityId+"_SAML2_Response_");
		if(authAddress != null) {
			context.put(SAMLConstants.AUTH_ADDRESS,authAddress);
		}
		if(authDNS != null) {
			context.put(SAMLConstants.AUTH_DNS,authDNS);
		}
		context.put(SAMLConstants.ASSERTION_ID_PREFIX,exchangeEntityId+"_SAML2_Assertion_");
		context.put(SAMLConstants.ISSUSER_ENTITY_ID, exchangeEntityId);
		context.put(SAMLConstants.DESTINATION, paymentDestinationUrl);
		context.put(SAMLConstants.PARTNER_ENTITY_ID, partnerEntityId);
		context.put(SAMLConstants.SIGNING_KEY_ALIAS, exchangeSigningKey);
		Response resp = buildResponse(sessionId,encryptAssertions,false, context, claims,signatureAlgoId,digestAlgoId);
		ResponseMarshaller marshaller = new ResponseMarshaller();
		Element elem = marshaller.marshall(resp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    XMLHelper.writeNode(elem, baos);
	    return baos.toString();
	}
	
	/**
	 * 
	 * @param encryptAssertions		// If we need to encrypt the assertions
	 * @param b 
	 * @param authAddress			// Address of the authenticated subject
	 * @param authDNS				// DNS address of the authenticated subject
	 * @param partnerEntityId		// Payment service provider ID, should come from the metadata received from the payment partner
	 * @param alias					// entityId of the exchange, should be there in IDP metadata for the exchange
	 * @param exchangeSigningKey	// Signing key of the exchange, corresponding certificate should be there in IDP metdata for the exchange
	 * @param claims				// Claims we want to include as assertions
	 * @return
	 * @throws Exception
	 */
	public String buildSAMLResponseFromSPMetadata(
			boolean encryptAssertions,
			boolean useHostAsDestination,
			String authAddress,
			String authDNS,
			String partnerEntityId,
			String alias,
			String exchangeSigningKey,
			Map<String, String> claims, String signatureAlgoId, String digestAlgoId) throws Exception {
		init();
		String sessionId = SAMLUtils.generateSessionId();
		HashMap<String,Object> context = new HashMap<>();
		context.put(SAMLConstants.RESPONSE_ID_PREFIX,alias+"_SAML2_Response_");
		if(authAddress != null) {
			context.put(SAMLConstants.AUTH_ADDRESS,authAddress);
		}
		if(authDNS != null) {
			context.put(SAMLConstants.AUTH_DNS,authDNS);
		}
		context.put(SAMLConstants.ASSERTION_ID_PREFIX,alias+"_SAML2_Assertion_");
		context.put(SAMLConstants.ISSUSER_ENTITY_ID, alias);
		if(useHostAsDestination) {
			context.put(SAMLConstants.DESTINATION, partnerEntityId);
		}else {
			context.put(SAMLConstants.DESTINATION, this.metadaLoader.getSPAssertionConsumerUrl(partnerEntityId));
		}
		context.put(SAMLConstants.AUDIENCE, partnerEntityId);
		context.put(SAMLConstants.SIGNING_KEY_ALIAS, exchangeSigningKey);
		context.put(SAMLConstants.PARTNER_ENTITY_ID, partnerEntityId);
		Response resp = buildResponse(sessionId,encryptAssertions,true,context, claims,signatureAlgoId, digestAlgoId);
		ResponseMarshaller marshaller = new ResponseMarshaller();
		Element elem = marshaller.marshall(resp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    XMLHelper.writeNode(elem, baos);
	    return baos.toString();
	}
	

	private Response buildResponse(
			String sessionId, 
			boolean encryptAssertions,
			boolean useMetadataForPartnerCertificate,
			Map<String,Object> context,
			Map<String, String> claims, 
			String signatureAlgoId, 
			String digestAlgoId) throws Exception {
		Response response = new org.opensaml.saml2.core.impl.ResponseBuilder().buildObject();
		DefaultSAMLAssertionBuilder assertionBuilder = new DefaultSAMLAssertionBuilder(pks);
		response.setID(SAMLUtils.createID((String)context.get(SAMLConstants.RESPONSE_ID_PREFIX), sessionId));
		response.setDestination((String)context.get(SAMLConstants.DESTINATION));
		response.setStatus(buildStatus(StatusCodes.SUCCESS_CODE, null));
		response.setVersion(SAMLVersion.VERSION_20);
		DateTime issueInstant = new DateTime();
		DateTime notOnOrAfter = new DateTime(
				issueInstant.getMillis() + 30*60*1000L); // 2 days
		response.setIssueInstant(issueInstant);
		Issuer newIssuer = new org.opensaml.saml2.core.impl.IssuerBuilder().buildObject();
		newIssuer.setValue((String)context.get(SAMLConstants.ISSUSER_ENTITY_ID));
		response.setIssuer(newIssuer);
		Assertion assertion = assertionBuilder.buildAssertion(context, claims, issueInstant, notOnOrAfter, sessionId);
		if (encryptAssertions) {
			X509Certificate partnerCert = null;
			String partnerEntityId = (String)context.get(SAMLConstants.PARTNER_ENTITY_ID);
			if(useMetadataForPartnerCertificate) {
				partnerCert = this.metadaLoader.getEntityCertificate(partnerEntityId);
			}else {
				partnerCert = (X509Certificate) pks.getCertificates(partnerEntityId);
			}
			if (partnerCert != null) {
				EncryptedAssertion encryptedAssertion = SAMLSigner.setEncryptedAssertion(assertion,
						EncryptionConstants.ALGO_ID_BLOCKCIPHER_AES256, partnerCert);
				response.getEncryptedAssertions().add(encryptedAssertion);
			}else {
				logger.error("Assertions can not be encrypted without the partner [{}] certificate, using metadata {}",partnerEntityId,useMetadataForPartnerCertificate);
				throw new RuntimeException("Assertions can not be encrypted without partner certificate, Using metadata");
			}
		} else {
			response.getAssertions().add(assertion);
		}
		SignKeyDataHolder x509Creds = new SignKeyDataHolder(pks, (String)context.get(SAMLConstants.SIGNING_KEY_ALIAS));
		x509Creds.setSignatureAlgorithm(signatureAlgoId);
		SAMLSigner.setSignature(response, signatureAlgoId, digestAlgoId,
				x509Creds);
		return response;
	}

	private static Status buildStatus(String status, String statMsg) {
		Status stat = new StatusBuilder().buildObject();
		StatusCode statCode = new StatusCodeBuilder().buildObject();
		statCode.setValue(status);
		stat.setStatusCode(statCode);
		if (statMsg != null) {
			StatusMessage statMesssage = new StatusMessageBuilder().buildObject();
			statMesssage.setMessage(statMsg);
			stat.setStatusMessage(statMesssage);
		}
		return stat;
	}
}
