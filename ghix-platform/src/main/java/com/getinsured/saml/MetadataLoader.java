package com.getinsured.saml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.security.auth.x500.X500Principal;

import org.opensaml.DefaultBootstrap;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.AttributeConsumingService;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml2.metadata.RequestedAttribute;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml2.metadata.provider.FilesystemMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.X509Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.saml.metadata.CachingMetadataManager;
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate;
import org.springframework.security.saml.parser.ParserPoolHolder;
import org.springframework.stereotype.Component;
import com.getinsured.hix.platform.util.GhixPlatformKeystore;

@Component
public class MetadataLoader {
	@Autowired
	private GhixPlatformKeystore platformKeystore;
	private static ParserPoolHolder holder;
	private static Logger logger = LoggerFactory.getLogger(MetadataLoader.class);
	
	private static CachingMetadataManager metadataManager = null;
	private static boolean isBootStrapped=false;
	private static String PAYMENT_CONFIG_HOME;
	static {
		PAYMENT_CONFIG_HOME = System.getProperty("GHIX_HOME")+File.separatorChar+
				"ghix-setup"+File.separatorChar+
				"conf"+File.separatorChar+
				"saml";
		
	}
	public static void doBootstrap() {
        if (!isBootStrapped) {
            try {
                DefaultBootstrap.bootstrap();
                isBootStrapped = true;
            } catch (ConfigurationException e) {
                //log.error("Error in bootstrapping the OpenSAML2 library", e);
            }
           
        }
    }
	
	@PostConstruct	
	public void loadIssuerMetadata() {
		doBootstrap();
		List<MetadataProvider> providerList =  new ArrayList<>();
		logger.info("Loading issuer metadata from location {}", PAYMENT_CONFIG_HOME);
		File metadataDir = new File(PAYMENT_CONFIG_HOME);
		if(!metadataDir.isDirectory() || !metadataDir.canRead()) {
			logger.info("Error reading issuer metadata from {}, location is either not a directory or its not readable", PAYMENT_CONFIG_HOME);
			return;
		}
		File[] metadataFiles = metadataDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if(pathname.getName().endsWith(".xml")) {
					return true;
				}else {
					logger.info("Ignoring file {} only xml files are accepted ",pathname.getName());
					return false;
				}
			}
		});
		logger.info("Found {} XML files",metadataFiles.length);
		for(File metadataFile:metadataFiles) {
			logger.info("Attemting to load {} ",metadataFile.getAbsolutePath());
			MetadataProvider p = getMetadataDelegate(metadataFile);
			if(p == null) {
				logger.info("Failed to get the metadata from "+metadataFile.getAbsolutePath());
				continue;
			}
			logger.info("Adding metadata provider");
			providerList.add(p);
		}
		int loaded = providerList.size();
		logger.info("Done loading Issuer payment provider metadata, loaded {} metadata provider configurations ", loaded);
		if(loaded > 0) {
			try {
				metadataManager = new CachingMetadataManager(providerList);
			} catch (MetadataProviderException e) {
				logger.warn("error loading the metadata, failed", e);
			}
		}
	}
	
	public EntityDescriptor getEntityDescriptor(String entityId){
		EntityDescriptor ed = null;
		if(metadataManager == null) {
			logger.info("Metadata loader not yet initialized, initializing");
			loadIssuerMetadata();
		}
		List<ExtendedMetadataDelegate> providers = metadataManager.getAvailableProviders();
		if(providers == null) {
			logger.info("No providers availble in the metadata");
		}
		for(ExtendedMetadataDelegate delegate: providers) {
			try {
				ed = delegate.getEntityDescriptor(entityId);
			} catch (MetadataProviderException e) {
				logger.error("Invalid metadata {}", e.getMessage());
			}
			if(ed != null) {
				break;
			}
		}
		if(ed == null) {
			logger.info("Checked {} providers available, could not find {}",providers.size(), entityId);
		}
		return ed;
	}
	
	public synchronized String getSPAssertionConsumerUrl(String entityId) {
		if(entityId == null) {
			logger.info("No entity id provided");
			return null;
		}
		EntityDescriptor ed = this.getEntityDescriptor(entityId.trim());
		if (ed == null) {
			logger.info("No descriptor available for entity Id {} can not find the Consumer URL", entityId);
			return null;
		}
		SPSSODescriptor sp = ed.getSPSSODescriptor(SAMLConstants.SAML20P_NS);
		if (sp != null) {
			List<AssertionConsumerService> ascList = sp.getAssertionConsumerServices();
			logger.info("Retrieved ACS {}", ascList.size());
			if (ascList != null && ascList.size() > 0) {
				for (AssertionConsumerService acs : ascList) {
					if (acs.getBinding().contains("HTTP-POST")) {
						return acs.getLocation();
					}
				}
			}
		} else {
			logger.error("No SP descriptor available for {}", entityId);
		}
		logger.error("No HTTP POST binding available for {}", entityId);
		return null;
	}
	
	public  synchronized List<String> getSPRequestedAttributeConsumingServiceAttributes(String entityId) {
		ArrayList<String> attributes = new ArrayList<>();
		EntityDescriptor ed = this.getEntityDescriptor(entityId);
		if(ed == null) {
			logger.info("No descriptor available for {}",entityId);
			return attributes;
		}
		SPSSODescriptor sp = ed.getSPSSODescriptor(SAMLConstants.SAML20P_NS);
		if(sp != null) {
			List<AssertionConsumerService> ascList = sp.getAssertionConsumerServices();
			if(ascList != null && ascList.size() > 0) {
				if(ascList != null && ascList.size() > 0) {
					for(AssertionConsumerService acs:ascList) {
						if(acs.getBinding().contains("HTTP-POST")) {
							AttributeConsumingService attrService = sp.getDefaultAttributeConsumingService();
							if(attrService != null) {
								if(logger.isInfoEnabled()) {
									logger.info("Found attribute consuming service {}",attrService.getNames());
								}
								List<RequestedAttribute> atts = attrService.getRequestAttributes();
								for(RequestedAttribute attr: atts) {
									attributes.add(attr.getName());
								}
							}
						}
					}
				}else {
					logger.info("No service attributes available for {}",entityId);
				}
			}
		}else {
			logger.error("No SP descriptor available for {}",entityId);
		}
		logger.error("No HTTP POST binding available for {}",entityId);
		return attributes;
	}
	
	public  synchronized String getIDPSubjectName(String id) {
		try {
			EntityDescriptor desc = metadataManager.getEntityDescriptor(id);
			IDPSSODescriptor idpDesc = desc.getIDPSSODescriptor(SAMLConstants.SAML20P_NS);
			if(idpDesc != null) {
				X509Certificate cert = (X509Certificate) this.platformKeystore.getCertificates(id);
				X500Principal prin = cert.getSubjectX500Principal();
				return prin.getName("RFC1779");
			}
		} catch (MetadataProviderException e) {
			logger.error("Error finding Subject name for {}",id, e);
		}
		return null;
	}
	
	public  synchronized X509Certificate getEntityCertificate(String entityId) throws CertificateException {
		EntityDescriptor desc = this.getEntityDescriptor(entityId);
		if(desc == null) {
			logger.error("Error finding certificate for {}, no metadata available", entityId);
		}
		Signature signature = desc.getSignature();
		if(signature != null) {
			 KeyInfo keyDesc = signature.getKeyInfo();
			 List<X509Data> xData = keyDesc.getX509Datas();
			 if(xData != null && xData.size() > 0) {
				 X509Data xCerts = xData.get(0);
				 List<org.opensaml.xml.signature.X509Certificate> certList = xCerts.getX509Certificates();
				 if(certList != null && certList.size() > 0) {
					 String x = certList.get(0).getValue();
					 StringBuilder sb = new StringBuilder();
					 sb.append("-----BEGIN CERTIFICATE-----\n");
					 sb.append(x);
					 sb.append("\n-----END CERTIFICATE-----");
					 sb.trimToSize();
					 CertificateFactory cf = CertificateFactory.getInstance("X.509");
					 return (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(sb.toString().getBytes()));
				 }else {
					 logger.error("Error finding certificate for {}, no certificate available within X509 data, expected at least 1",entityId); 
				 }
			}else {
				logger.error("Error finding certificate for {}, X509 Data element is not available",entityId); 
			}
		 }else {
			 logger.error("Error finding certificate for {}, no signature found",entityId); 
		 }
		return null;
	}
	
	public static FilesystemMetadataProvider getMetadataDelegate(File metadata) {
		FilesystemMetadataProvider provider = null;
		try {
			provider = new FilesystemMetadataProvider(metadata);
			provider.setParserPool(getParserPool());
			provider.initialize();
		} catch (MetadataProviderException e) {
			logger.error("Error initializing the provider ", e);
		}
		return  provider;
		
	}
	
	public static ParserPool getParserPool() {
		if(holder == null) {
			StaticBasicParserPool pool = new StaticBasicParserPool();
			pool.setNamespaceAware(true);
			pool.setExpandEntityReferences(false);
			//pool.set
			try {
				pool.initialize();
				holder = new ParserPoolHolder();
				holder.setParserPool(pool);
			} catch (XMLParserException e) {
				logger.error("Failed to initialize the parser pool", e);
			}
		}
		return ParserPoolHolder.getPool();
	}
}
