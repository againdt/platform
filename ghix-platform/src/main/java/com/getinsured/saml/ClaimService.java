package com.getinsured.saml;

import java.util.Map;
import java.util.Set;

public interface ClaimService {
	
	public Map<String,String> getClaims(String entityId, Set<String> requestedClaims);

}
