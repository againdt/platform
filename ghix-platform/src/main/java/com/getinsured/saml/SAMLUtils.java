package com.getinsured.saml;

import java.security.SecureRandom;

import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.impl.IssuerBuilder;

public class SAMLUtils {
	private static final char[] charMapping = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k', 'l', 'm', 'n', 'o', 'p'};
	private static SecureRandom random = null;
	static {
		random = new SecureRandom();
	}
	public static String createID(String prefix) {
        byte[] bytes = new byte[20]; // 160 bits
        random.nextBytes(bytes);
        char[] chars = new char[40];
        for (int i = 0; i < bytes.length; i++) {
            int left = (bytes[i] >> 4) & 0x0f;
            int right = bytes[i] & 0x0f;
            chars[i * 2] = charMapping[left];
            chars[i * 2 + 1] = charMapping[right];
        }
        StringBuilder sb = new StringBuilder();
        if(prefix != null) {
        	sb.append(prefix);
        }
        sb.append(String.valueOf(chars));
        sb.trimToSize();
        return sb.toString(); 
    }
	
	public static String generateSessionId() {
		byte[] bytes = new byte[20]; // 160 bits
        random.nextBytes(bytes);
        char[] chars = new char[40];
        for (int i = 0; i < bytes.length; i++) {
            int left = (bytes[i] >> 4) & 0x0f;
            int right = bytes[i] & 0x0f;
            chars[i * 2] = charMapping[left];
            chars[i * 2 + 1] = charMapping[right];
        }
        StringBuilder sb = new StringBuilder();
        sb.append(chars);
        return sb.toString();
	}
	
	public static String createID(String prefix, String sessionId) {
		StringBuilder sb = new StringBuilder();
        if(prefix != null) {
        	sb.append(prefix);
        }
		if(sessionId == null) {
	        byte[] bytes = new byte[20]; // 160 bits
	        random.nextBytes(bytes);
	        char[] chars = new char[40];
	        for (int i = 0; i < bytes.length; i++) {
	            int left = (bytes[i] >> 4) & 0x0f;
	            int right = bytes[i] & 0x0f;
	            chars[i * 2] = charMapping[left];
	            chars[i * 2 + 1] = charMapping[right];
	        }
	        sb.append(String.valueOf(chars));
		}else {
			sb.append(sessionId);
		}
        sb.trimToSize();
        return sb.toString(); 
    }
	
	public static Issuer getIssuer(String format, String name) {
		Issuer issuer = new IssuerBuilder().buildObject();
		issuer.setFormat(format);
		issuer.setValue(name);
		return issuer;
	}
}
