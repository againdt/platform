package com.getinsured.saml;

public class SAMLConstants {

    public static final String NAME_ID_POLICY_ENTITY = "urn:oasis:names:tc:SAML:2.0:nameid-format:entity";
    public static final String SUBJECT_CONFIRM_BEARER = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
    public static final String NAME_FORMAT_BASIC = "urn:oasis:names:tc:SAML:2.0:attrname-format:basic";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String FEDERATED_IDP = "federated-idp-domain";
    public static final String ISSUER = "issuer";
    public static final String SAML_REQUEST = "SAMLRequest";
    public static final String AUTH_MODE = "authMode";
    public static final String ASSRTN_CONSUMER_URL = "ACSUrl";
    public static final String REQ_ID = "id";
    public static final String SUBJECT = "subject";
    public static final String RP_SESSION_ID = "relyingPartySessionId";
    public static final String REQ_MSG_STR = "requestMessageString";
    public static final String DESTINATION = "destination";
    public static final String RELAY_STATE = "RelayState";
    public static final String AUTH_REQ_SAML_ASSRTN = "SAMLRequest";
    public static final String SAML_RESP = "SAMLResponse";
    public static final String SIG_ALG = "SigAlg";
    public static final String SIGNATURE = "Signature";
    public static final String HTTP_QUERY_STRING = "HttpQuerryString";
    public static final String TARGET_ASSRTN_CONSUMER_URL = "targetedAssrtnConsumerURL";
    public static final String kEEP_SESSION_ALIVE = "keepSessionAlive";
    public static final String LOGOUT_RESP = "logoutResponse";
    public static final String STATUS = "status";
    public static final String STATUS_MSG = "statusMsg";
    public static final String SSO_TOKEN_ID = "ssoTokenId";
    public static final String FE_SESSION_KEY = "authSession";
    public static final String AUTH_FAILURE = "authFailure";
    public static final String AUTH_FAILURE_MSG = "authFailureMsg";
    public static final String SAMLSSOServiceClient = "ssoServiceClient";
    public static final String SESSION_DATA_KEY = "sessionDataKey";
    public static final String AUTHENTICATION_RESULT = "AuthenticationResult";
    public static final String LOGIN_PAGE = "customLoginPage";
    public static final String CLAIM_DIALECT_URL = "http://wso2.org/claims";
    public static final String SAML_ENDPOINT = "samlsso/carbon/";
    public static final String DEFAULT_LOGOUT_ENDPOINT = "/authenticationendpoint/samlsso_logout.do";
    public static final String SAMLSSO_URL = "/samlsso";
    public static final String NOTIFICATION_ENDPOINT ="/authenticationendpoint/samlsso_notification.do";
    public static final String SLO_SAML_SOAP_BINDING_ENABLED = "SSOService.SLOSAMLSOAPBindingEnabled";
    public static final String START_SOAP_BINDING = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP-ENV:Body>";
    public static final String END_SOAP_BINDING = "</SOAP-ENV:Body>" +
            "</SOAP-ENV:Envelope>";
    public static final String SOAP_ACTION = "http://www.oasis-open.org/committees/security";
    public static final String XML_TAG_REGEX = "\\<\\?xml(.+?)\\?\\>";
    public static final String SAML_REQUEST_PARAM_KEY = "SAMLRequest";
    public static final String SOAP_ACTION_PARAM_KEY = "SOAPAction";
    public static final String COOKIE_PARAM_KEY = "Cookie";
    public static final String SESSION_ID_PARAM_KEY = "JSESSIONID=";
    public static final String ENCODING_FORMAT = "UTF-8";
    public static final String COM_PROTOCOL = "https";
    public static final String CRYPTO_PROTOCOL = "TLS";
    public static final String ASSERTION_ID_PREFIX="ASSERTION_ID_PREFIX";
	public static final String ISSUSER_ENTITY_ID = "ISSUSER_ENTITY_ID";
	public static final String RESPONSE_ID_PREFIX = "RESPONSE_ID_PREFIX";
	public static final String SIGNING_KEY_ALIAS = "SIGNING_KEY_ALIAS";
	public static final String PARTNER_KEY_ALIAS = "PARTNER_KEY_ALIAS";
	public static final String AUTH_ADDRESS = "AUTH_ADDRESS";
	public static final String AUTH_DNS = "AUTH_DNS";
	public static final String ENCRYPTION_KEY_ALIAS = "ENCRYPTION_KEY";
	public static final String PARTNER_ENTITY_ID = "PARTNER_ENTITY_ID";
	public static final String AUDIENCE = "AUDIENCE";

	private SAMLConstants() {
    }

}
