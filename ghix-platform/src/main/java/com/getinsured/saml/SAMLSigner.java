package com.getinsured.saml;

import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.utils.EncryptionConstants;
import org.opensaml.common.impl.SAMLObjectContentReference;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.EncryptedAssertion;
import org.opensaml.saml2.core.RequestAbstractType;
import org.opensaml.saml2.encryption.Encrypter;
import org.opensaml.saml2.encryption.Encrypter.KeyPlacement;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.encryption.EncryptionException;
import org.opensaml.xml.encryption.EncryptionParameters;
import org.opensaml.xml.encryption.KeyEncryptionParameters;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.SignableXMLObject;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.signature.X509Certificate;
import org.opensaml.xml.signature.X509Data;
import org.opensaml.xml.signature.X509SubjectName;
import org.opensaml.xml.signature.impl.X509SubjectNameBuilder;
import org.opensaml.xml.validation.ValidationException;

public class SAMLSigner {

    public boolean validateXMLSignature(RequestAbstractType request, X509Credential cred,
                                        String alias) throws Exception {

        boolean isSignatureValid = false;

        if (request.getSignature() != null) {
            try {
                SignatureValidator validator = new SignatureValidator(cred);
                validator.validate(request.getSignature());
                isSignatureValid = true;
            } catch (ValidationException e) {
                throw new RuntimeException("Signature Validation Failed for the SAML Assertion : Signature is " +
                                            "invalid.", e);
            }
        }
        return isSignatureValid;
    }

    public static SignableXMLObject setSignature(SignableXMLObject signableXMLObject, String signatureAlgorithm, String
            digestAlgorithm, X509Credential cred) throws Exception {
    	
        Signature signature = (Signature) buildXMLObject(Signature.DEFAULT_ELEMENT_NAME);
        
        signature.setSigningCredential(cred);
        signature.setSignatureAlgorithm(signatureAlgorithm);
        signature.setCanonicalizationAlgorithm(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
        KeyInfo keyInfo = (KeyInfo) buildXMLObject(KeyInfo.DEFAULT_ELEMENT_NAME);
        X509Data data = (X509Data) buildXMLObject(X509Data.DEFAULT_ELEMENT_NAME);
        X509Certificate cert = (X509Certificate) buildXMLObject(X509Certificate.DEFAULT_ELEMENT_NAME);

        String value;
        try {
            value = org.apache.xml.security.utils.Base64.encode(cred.getEntityCertificate().getEncoded());
        } catch (CertificateEncodingException e) {
            throw new RuntimeException("Error occurred while retrieving encoded cert", e);
        }

        cert.setValue(value);
        data.getX509Certificates().add(cert);
        String name = cred.getEntityCertificate().getSubjectX500Principal().getName();
        X509SubjectNameBuilder builder = new X509SubjectNameBuilder();
        X509SubjectName xName = builder.buildObject();
        xName.setValue(name);
        data.getX509SubjectNames().add(xName);
        keyInfo.getX509Datas().add(data);
        signature.setKeyInfo(keyInfo);

        signableXMLObject.setSignature(signature);
        ((SAMLObjectContentReference) signature.getContentReferences().get(0)).setDigestAlgorithm(digestAlgorithm);

        List<Signature> signatureList = new ArrayList<Signature>();
        signatureList.add(signature);

        MarshallerFactory marshallerFactory = org.opensaml.xml.Configuration.getMarshallerFactory();
        Marshaller marshaller = marshallerFactory.getMarshaller(signableXMLObject);

        try {
            marshaller.marshall(signableXMLObject);
        } catch (MarshallingException e) {
            throw new RuntimeException("Unable to marshall the request", e);
        }

        
        try {
            Signer.signObjects(signatureList);
        } catch (SignatureException e) {
            throw new RuntimeException("Error occurred while signing request", e);
        }

        return signableXMLObject;
    }

    /**
     * Builds SAML Elements
     *
     * @param objectQName
     * @return
     * @throws Exception
     */
    private static XMLObject buildXMLObject(QName objectQName) throws Exception {
        @SuppressWarnings("rawtypes")
		XMLObjectBuilder builder =
                org.opensaml.xml.Configuration.getBuilderFactory()
                        .getBuilder(objectQName);
        if (builder == null) {
            throw new RuntimeException("Unable to retrieve builder for object QName " +
                                        objectQName);
        }
        return builder.buildObject(objectQName.getNamespaceURI(), objectQName.getLocalPart(),
                                   objectQName.getPrefix());
    }

	public static EncryptedAssertion setEncryptedAssertion(Assertion assertion, String algoIdBlockcipherAes256,
			java.security.cert.Certificate cert) throws EncryptionException, ConfigurationException {
		EncryptionParameters encParams = new EncryptionParameters();
		encParams.setAlgorithm(EncryptionConstants.ALGO_ID_BLOCKCIPHER_AES128);
		
		BasicX509Credential cred = new BasicX509Credential();
		PublicKey pKey = cert.getPublicKey();
		cred.setPublicKey(pKey);
		cred.setPrivateKey(null);
		KeyEncryptionParameters kek = new KeyEncryptionParameters();
		kek.setAlgorithm(EncryptionConstants.ALGO_ID_KEYTRANSPORT_RSAOAEP);
		kek.setEncryptionCredential(cred);
		Encrypter encrypter = new Encrypter(encParams, kek);
		encrypter.setKeyPlacement(KeyPlacement.INLINE);
		EncryptedAssertion encrypted = encrypter.encrypt(assertion);
		return encrypted;
	}
}
