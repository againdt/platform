package com.getinsured.saml;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml1.core.NameIdentifier;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.SubjectLocality;
import org.opensaml.saml2.core.impl.AssertionBuilder;
import org.opensaml.saml2.core.impl.AudienceBuilder;
import org.opensaml.saml2.core.impl.AudienceRestrictionBuilder;
import org.opensaml.saml2.core.impl.AuthnContextBuilder;
import org.opensaml.saml2.core.impl.AuthnContextClassRefBuilder;
import org.opensaml.saml2.core.impl.AuthnStatementBuilder;
import org.opensaml.saml2.core.impl.ConditionsBuilder;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SubjectBuilder;
import org.opensaml.saml2.core.impl.SubjectConfirmationBuilder;
import org.opensaml.saml2.core.impl.SubjectConfirmationDataBuilder;
import org.opensaml.saml2.core.impl.SubjectLocalityBuilder;
import org.opensaml.xml.schema.XSString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.GhixPlatformKeystore;

public class DefaultSAMLAssertionBuilder implements SAMLAssertionBuilder {

	private static Log log = LogFactory.getLog(DefaultSAMLAssertionBuilder.class);
	private GhixPlatformKeystore keyStore;
	private Logger logger = LoggerFactory.getLogger(DefaultSAMLAssertionBuilder.class);
	
	public DefaultSAMLAssertionBuilder(GhixPlatformKeystore keyStore) {
		this.keyStore = keyStore;
	}
	public void init() throws Exception {
		// Overridden method, no need to implement the body
	}

	 public Assertion buildAssertion(
			 Map<String,Object> context, 
			 Map<String,String> assertions, 
			 DateTime notBefore, 
			 DateTime notOnOrAfter,
             String sessionId) throws Exception {
		try {
			logger.info("Building assertions");
			Issuer newIssuer = new org.opensaml.saml2.core.impl.IssuerBuilder().buildObject();
			newIssuer.setValue((String)context.get(SAMLConstants.ISSUSER_ENTITY_ID));
			Assertion samlAssertion = new AssertionBuilder().buildObject();
			samlAssertion.setID(SAMLUtils.createID((String)context.get(SAMLConstants.ASSERTION_ID_PREFIX), sessionId));
			samlAssertion.setVersion(SAMLVersion.VERSION_20);
			samlAssertion.setIssuer(SAMLUtils.getIssuer("urn:oasis:names:tc:SAML:2.0:nameid-format:entity".intern(), (String)context.get(SAMLConstants.ISSUSER_ENTITY_ID)));
			samlAssertion.setIssueInstant(notBefore);
			samlAssertion.setIssuer(newIssuer);
			Subject subject = new SubjectBuilder().buildObject();
			NameID nameId = new NameIDBuilder().buildObject();
			nameId.setNameQualifier("");
			nameId.setFormat(NameIdentifier.UNSPECIFIED);
//          nameId.setValue(authReqDTO.getUser().getAuthenticatedSubjectIdentifier());
//          if (authReqDTO.getNameIDFormat() != null) {
//                nameId.setFormat(authReqDTO.getNameIDFormat());
//          } else {
//              nameId.setFormat(NameIdentifier.EMAIL);
//          }
			//Destination - > Issuer Payment Info
			String destinationUrl = (String)context.get(SAMLConstants.DESTINATION);
			subject.setNameID(nameId);
			SubjectConfirmation subjectConfirmation = new SubjectConfirmationBuilder().buildObject();
			subjectConfirmation.setMethod(SAMLConstants.SUBJECT_CONFIRM_BEARER);
			NameID scdNameId = new NameIDBuilder().buildObject();
			scdNameId.setNameQualifier("");
			scdNameId.setFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName".intern());
			subjectConfirmation.setNameID(scdNameId);
			SubjectConfirmationData scData = new SubjectConfirmationDataBuilder().buildObject();
			scData.setRecipient(destinationUrl);
			scData.setNotOnOrAfter(notOnOrAfter);
			subjectConfirmation.setSubjectConfirmationData(scData);
			subject.getSubjectConfirmations().add(subjectConfirmation);
			
			samlAssertion.setSubject(subject);

			AuthnStatement authStmt = new AuthnStatementBuilder().buildObject();
			authStmt.setAuthnInstant(notBefore);
			
			//Locality
			String address = (String)context.get(SAMLConstants.AUTH_ADDRESS);
			String dnsName = (String)context.get(SAMLConstants.AUTH_DNS);
			SubjectLocality locality = null;
			if(address != null) {
				locality = new SubjectLocalityBuilder().buildObject();
				locality.setAddress(address);
			}
			if(dnsName != null) {
				if(locality == null ) {
					locality = new SubjectLocalityBuilder().buildObject();
				}
				locality.setDNSName(dnsName);	
			}
			if(locality != null) {
				authStmt.setSubjectLocality(locality);
			}
			
			// Auth Context
			AuthnContext authContext = new AuthnContextBuilder().buildObject();
			AuthnContextClassRef authCtxClassRef = new AuthnContextClassRefBuilder().buildObject();
			authCtxClassRef.setAuthnContextClassRef(AuthnContext.PASSWORD_AUTHN_CTX);
			authContext.setAuthnContextClassRef(authCtxClassRef);
			authStmt.setAuthnContext(authContext);
			authStmt.setSessionNotOnOrAfter(notOnOrAfter);
			samlAssertion.getAuthnStatements().add(authStmt);
			
			String signingKey = (String)context.get(SAMLConstants.SIGNING_KEY_ALIAS);
			AttributeStatement attrStmt = buildAttributeStatement(context, assertions);
			if (attrStmt != null) {
				samlAssertion.getAttributeStatements().add(attrStmt);
			}

			AudienceRestriction audienceRestriction = new AudienceRestrictionBuilder().buildObject();
			Audience issuerAudience = new AudienceBuilder().buildObject();
			issuerAudience.setAudienceURI((String)context.get(SAMLConstants.PARTNER_ENTITY_ID));
			audienceRestriction.getAudiences().add(issuerAudience);

			Conditions conditions = new ConditionsBuilder().buildObject();
			conditions.setNotBefore(notBefore);
			conditions.setNotOnOrAfter(notOnOrAfter);
			conditions.getAudienceRestrictions().add(audienceRestriction);
			samlAssertion.setConditions(conditions);
			SAMLSigner.setSignature(samlAssertion, 
					"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256".intern(), 	// Signature Algorithm
					"http://www.w3.org/2001/04/xmlenc#sha256".intern(),				// Digest Algorithm
					new SignKeyDataHolder(this.keyStore,signingKey));
			return samlAssertion;
		} catch (Exception e) {
			log.error("Error when reading claim values for generating SAML Response", e);
			throw new RuntimeException("Error when reading claim values for generating SAML Response", e);
		}
	}

	private AttributeStatement buildAttributeStatement(Map<String, Object> context, Map<String, String> claims) {
		AttributeStatement attStmt = SAMLObjectUtils.buildXMLObject(AttributeStatement.class);
		Iterator<Map.Entry<String, String>> iterator = claims.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			Attribute attr = SAMLObjectUtils.createAttribute(entry.getKey());
			XSString attrVal = SAMLObjectUtils.createAttributeValue(entry.getValue());
			attr.getAttributeValues().add(attrVal);
			attStmt.getAttributes().add(attr);
		}
		return attStmt;
	}
}
