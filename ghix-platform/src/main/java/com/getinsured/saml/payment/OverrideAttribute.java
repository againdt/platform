package com.getinsured.saml.payment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class OverrideAttribute {
	
	
	private String source = null;
	private String target = null;
	
	@XmlAttribute(name="source")
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	@XmlAttribute(name="target")
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	

}
