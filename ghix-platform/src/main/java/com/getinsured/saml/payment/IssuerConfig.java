package com.getinsured.saml.payment;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="IssuerConfig")
public class IssuerConfig {

	private String hiosId;
	
	private String entityId = null;
	
	private String enableEncryption = null;
	
	private String haveMetadata = null;
	
	private String useRelayState = null;
	
	private String useHostAsDestination = null;
	
	private String samlRedirectSupported = null;
	
	private List<OverrideAttribute> overrideAttributes;
	
	private String signatureAlgoId = null;

	@XmlAttribute(name="hiosId")
	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	@XmlAttribute(name="entityId")
	public String getEntityId() {
		return entityId;
	}
	
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@XmlAttribute(name="enableEncryption")
	public String getEnableEncryption() {
		return enableEncryption;
	}

	public void setEnableEncryption(String enableEncryption) {
		this.enableEncryption = enableEncryption;
	}

	@XmlAttribute(name="haveMetadata")
	public String getHaveMetadata() {
		return haveMetadata;
	}

	public void setHaveMetadata(String haveMetadata) {
		this.haveMetadata = haveMetadata;
	}

	@XmlAttribute(name="useRelayState")
	public String getUseRelayState() {
		return useRelayState;
	}

	public void setUseRelayState(String useRelayState) {
		this.useRelayState = useRelayState;
	}
	
	@XmlAttribute(name="useHostAsDestination")
	public String getUseHostAsDestination() {
		return useHostAsDestination;
	}

	public void setUseHostAsDestination(String useHostAsDestination) {
		this.useHostAsDestination = useHostAsDestination;
	}
	
	@XmlAttribute(name="samlRedirectSupported")
	public String getSamlRedirectSupported() {
		return samlRedirectSupported;
	}
	
	public void setSamlRedirectSupported(String samlRedirectSupported) {
		this.samlRedirectSupported = samlRedirectSupported;
	}

	@XmlAttribute(name="signatureAlgoId")
	public String getSignatureAlgoId() {
		return signatureAlgoId;
	}

	public void setSignatureAlgoId(String signatureAlgoId) {
		this.signatureAlgoId = signatureAlgoId;
	}

	public List<OverrideAttribute> getOverrideAttributes() {
		return overrideAttributes;
	}
	@XmlElement(name="OverrideAttribute")
	public void setOverrideAttributes(List<OverrideAttribute> overrideAttributes) {
		this.overrideAttributes = overrideAttributes;
	}
}
