package com.getinsured.saml.payment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement(name = "IssuerPaymentConfig")
public class IssuerPaymentConfig {
	private static IssuerPaymentConfig issuerPaymentConfig = null;
	private static Logger logger = LoggerFactory.getLogger(IssuerPaymentConfig.class);
	private static String configLocation = null;
	
	private HashMap<String, IssuerConfig> issuerConfigMap = null;
	
	@XmlElement(name="IssuerConfig")
	private List<IssuerConfig> issuerConfigs;
	
	public IssuerConfig getIssuerConfig(String hiodId){
		if(this.issuerConfigMap == null) {
			this.issuerConfigMap = new HashMap<>();
			for(IssuerConfig ic: this.issuerConfigs) {
				String hiosId = ic.getHiosId();
				logger.info("Caching extended issuer payment config for HIOS ID:{}", hiosId);
				if(hiosId != null) {
					this.issuerConfigMap.put(hiosId, ic);
				}
			}
		}
		return this.issuerConfigMap.get(hiodId);
	}
	
	
	public List<IssuerConfig> getPartnerMap() {
		return issuerConfigs;
	}
	
	
	public void setIssuerConfig(List<IssuerConfig> configList) {
		this.issuerConfigs = configList;
	}
	
	@SuppressWarnings("serial")
	public static IssuerPaymentConfig getIssuerConfigs(String location) {
		 InputStream resource = null;
		 logger.info("Reading Issuer Payment Config from {}", location);
		 if(issuerPaymentConfig == null){
		        try {
		            JAXBContext jc = JAXBContext.newInstance(IssuerPaymentConfig.class,IssuerConfig.class, OverrideAttribute.class);
		            Unmarshaller u = jc.createUnmarshaller();
		            File mappingFile = new File(location,"IssuerPaymentConfig.xml");
		            if(!mappingFile.exists()){
		            	throw new FileNotFoundException("IssuerPaymentConfig file not available [Path:]"+mappingFile.getAbsolutePath()){
							public synchronized Throwable fillInStackTrace(){
		            			return null;
		            		}
		            	};
		            }
		            resource = new FileInputStream(mappingFile);
		            issuerPaymentConfig = (IssuerPaymentConfig) u.unmarshal( resource );   
		        } catch(Exception e) {
		            logger.error("Error initializing the partner maping information",e);
		        }finally{
		        	if(resource != null){
		        		try {
							resource.close();
						} catch (IOException e) {
							//Ignored
						}
		        	}
		        }
		       configLocation = location;
		 }else {
			 logger.info("Configiration already initialized from {}, not initializing again",configLocation);
		 }
		 return issuerPaymentConfig;
	 }
	
	 public static IssuerPaymentConfig getIssuerConfigs() {
		 InputStream resource = null;
		 if(issuerPaymentConfig == null){
		        try {
		        	JAXBContext jc = JAXBContext.newInstance(IssuerPaymentConfig.class,IssuerConfig.class, OverrideAttribute.class);
		            Unmarshaller u = jc.createUnmarshaller();
		            resource = IssuerPaymentConfig.class.getClassLoader().getResourceAsStream("saml/payment/IssuerPaymentConfig.xml");
		            issuerPaymentConfig = (IssuerPaymentConfig) u.unmarshal(new InputStreamReader(resource,"UTF-8") );   
		        } catch(Exception e) {
		            logger.error("Error initializing the IssuerPaymentConfig information",e);
		        }finally{
		        	if(resource != null){
		        		try {
							resource.close();
						} catch (IOException e) {
							//Ignored
						}
		        	}
		        }
		 }
		 return issuerPaymentConfig;
	 }
	 
	 public static void main(String[] args) {
		 IssuerPaymentConfig ic = getIssuerConfigs();
		 IssuerConfig icfg = ic.getIssuerConfig("xxxx");
		 System.out.println(icfg.getEnableEncryption());
	 }
}
