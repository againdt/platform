package com.getinsured.saml;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.crypto.SecretKey;

import org.apache.xml.security.signature.XMLSignature;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.CredentialContextSet;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.x509.X509Credential;

import com.getinsured.hix.platform.util.GhixPlatformKeystore;

public class SignKeyDataHolder implements X509Credential {

    private static final String DSA_ENCRYPTION_ALGORITHM = "DSA";
    public static final String SECURITY_KEY_STORE_KEY_ALIAS = "Security.KeyStore.KeyAlias";

    private String signatureAlgorithm = null;
    private X509Certificate[] issuerCerts = null;

    private PrivateKey issuerPK = null;
    private GhixPlatformKeystore keyStore = null;

    public SignKeyDataHolder(GhixPlatformKeystore keystore, String alias) throws Exception {
    	this.keyStore = keystore;
        Certificate[] certificates;
        issuerPK = (PrivateKey) this.keyStore.getPrivateKey(alias,null);
        if(issuerPK == null) {
        	throw new RuntimeException("No key entry available for "+ alias);
        }
        certificates = this.keyStore.getCertificateChain(alias);
        issuerCerts = new X509Certificate[certificates.length];
        int i = 0;
        for (Certificate certificate : certificates) {
            issuerCerts[i++] = (X509Certificate) certificate;
        }
        signatureAlgorithm = XMLSignature.ALGO_ID_SIGNATURE_RSA;
        String pubKeyAlgo = issuerCerts[0].getPublicKey().getAlgorithm();
        if (DSA_ENCRYPTION_ALGORITHM.equalsIgnoreCase(pubKeyAlgo)) {
            signatureAlgorithm = XMLSignature.ALGO_ID_SIGNATURE_DSA;
        }
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }
    
    @Override
    public Collection<X509CRL> getCRLs() {
        return Collections.emptyList();
    }

    @Override
    public X509Certificate getEntityCertificate() {
        return issuerCerts[0];
    }

    @Override
    public Collection<X509Certificate> getEntityCertificateChain() {
        return Arrays.asList(issuerCerts);
    }

    @Override
    public CredentialContextSet getCredentalContextSet() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Class<? extends Credential> getCredentialType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getEntityId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<String> getKeyNames() {
        // TODO Auto-generated method stub
        return Collections.emptyList();
    }

    @Override
    public PrivateKey getPrivateKey() {
        return issuerPK;
    }

    @Override
    public PublicKey getPublicKey() {
        return issuerCerts[0].getPublicKey();
    }

    @Override
    public SecretKey getSecretKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UsageType getUsageType() {
        // TODO Auto-generated method stub
        return null;
    }


}