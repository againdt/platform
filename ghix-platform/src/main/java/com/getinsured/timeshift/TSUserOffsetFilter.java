package com.getinsured.timeshift;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.config.FileBasedConfiguration;
import com.getinsured.hix.platform.util.GhixPlatformConstants;


/**
 * Filter that receives X-USER header information
 */
public class TSUserOffsetFilter implements Filter
{
  private static final String X_USER_HEADER = "X-USER";
  private Logger log = LoggerFactory.getLogger(TSUserOffsetFilter.class);
  /**
   * {@inheritDoc}
   */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void destroy()
  {
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
  {
	log.info("TSUserOffsetFilter filter invoked");
	if(FileBasedConfiguration.getConfiguration().getBoolean("platform.tsFilterEnabled", false)) {
		final HttpServletRequest httpReq = (HttpServletRequest) request;
	    if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
	    	String userInfo = httpReq.getHeader(X_USER_HEADER);
	    	log.info("TSUserOffsetFilter - user {}",userInfo );
	        if(userInfo != null) {
	            TimeShifterUtil.setUserOffset(userInfo);
	    		long os = TimeShifterUtil.setUserOffset(userInfo);
	    		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
	    		log.info("TSUserOffsetFilter - Applied offset {} for user {} with effective date {}", os, userInfo, sdf.format(new Date(System.currentTimeMillis()+ os)));
	     	}else{
	            log.error("Timeshift enabled but no user info found in the header, Ignoring TS directive");
	        }
	    }
	}else {
		log.info("TS Filter not enabled");
	}
    chain.doFilter(request,response);
  }

}