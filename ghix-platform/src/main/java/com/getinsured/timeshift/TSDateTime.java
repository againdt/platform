package com.getinsured.timeshift;

import org.joda.time.DateTime;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class TSDateTime {

	public static DateTime getInstance() {
		DateTime dt = new DateTime();
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			dt = dt.withMillis(TimeShifterUtil.currentTimeMillis());
		}
		return dt;
	}
}
