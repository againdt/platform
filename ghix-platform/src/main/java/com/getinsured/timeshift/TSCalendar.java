package com.getinsured.timeshift;

import java.util.Calendar;
import java.util.TimeZone;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class TSCalendar {
	
	public static Calendar getInstance() {
		Calendar instance = Calendar.getInstance();
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			instance.setTimeInMillis(TimeShifterUtil.currentTimeMillis());
		}
		return instance;
	}
	
	public static Calendar getInstance(TimeZone tz) {
		Calendar instance = Calendar.getInstance(tz);
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			instance.setTimeInMillis(TimeShifterUtil.currentTimeMillis());
		}
		return instance;
	}

}
