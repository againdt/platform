package com.getinsured.timeshift;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.config.FileBasedConfiguration;


/**
 * Filter that receives X-USER header information
 */
public class HeaderFilter implements Filter
{
  private static Logger log = LoggerFactory.getLogger(HeaderFilter.class);
  /**
   * {@inheritDoc}
   */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void destroy()
  {
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
  {
	log.info("HeaderFilter invoked");
    String headerDebugEnabled= FileBasedConfiguration.getConfiguration().getProperty("platform.headerDebug");
    if(headerDebugEnabled != null && headerDebugEnabled.equalsIgnoreCase("true")){
      final HttpServletRequest httpReq = (HttpServletRequest) request;
      Enumeration<String> headerNames = httpReq.getHeaderNames();
      while(headerNames.hasMoreElements()){
        String hdr = headerNames.nextElement();
        log.info("Header Name:"+hdr+" Header Value:"+httpReq.getHeader(hdr));
      }
    }
    chain.doFilter(request,response);
  }

}