package com.getinsured.timeshift;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecureHttpClient;

public class TimeShifterUtil {
	private static Logger logger = LoggerFactory.getLogger(TimeShifterUtil.class);
	private static Map<String, Long>  sessionOffsets = Collections.synchronizedMap(new HashMap<String, Long>());
	private static JSONParser parser;
	private static HashMap<String, HttpSession> sessions = new HashMap<>();
	private static SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z".intern());
	
	public static void cleanup(String sessionId) {
		sessionOffsets.remove(sessionId);
	}
	public static long currentTimeMillis() {
		return System.currentTimeMillis() + getUserOffset();
	}
	
	public static void setUserOffset(String username, String offset) {
		try {
			if(offset != null) {
				TimeshiftContext.getCurrent().setUserName(username);
				TimeshiftContext.getCurrent().setTimeOffset(offset);
				if(logger.isInfoEnabled()) {
					SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
					logger.info("Registered offset {} for user {}, effective Date {}".intern(),offset,username,sdf.format(System.currentTimeMillis()+Long.parseLong(offset)));
				}
			}
		}catch(NumberFormatException ne) {
			if(logger.isInfoEnabled()) {
				logger.info("Invalid offset provied for {}, provided offset {}, ignoring".intern(), username, offset);
			}
		}
	}
	
	public static long fetchUserOffset(String username) {
		long offset = 0;
		if(parser == null) {
			parser = new JSONParser();
		}
		CloseableHttpClient httpClient = SecureHttpClient.getHttpClient();
		CloseableHttpResponse response = null;
		HttpGet offsetGet = new HttpGet(GhixPlatformConstants.TIMESHIFT_URL+"/"+username);
		try {
			response = httpClient.execute(offsetGet);
			StatusLine status = response.getStatusLine();
			int statusCode = status.getStatusCode();
			logger.debug("statusCode {}",statusCode);
			if(statusCode == 204) {
				logger.debug("No time offset available for the user {}",username);
			}else if(statusCode == 200) {
				HttpEntity entity = response.getEntity();
				if(entity != null) {
					String jsonResponse = EntityUtils.toString(entity);
					if(jsonResponse == null) {
						logger.debug("No TS response received while status received 200");
					}
					JSONObject obj = (JSONObject) parser.parse(jsonResponse);
					offset = (long) obj.get("timeshifterOffsetMillis".intern());
					logger.info("Retrieved time offset {} for user {} using TS microservice, effective date  {}", offset, username,sdf.format(new Date(System.currentTimeMillis()+offset)));
				}
			}
			response.close();
		} catch (Exception e) {
			logger.error("Error fetching time offset for user:{}, assuming Timeshift not enabled",username, e);
		} finally {
			if(offsetGet != null) {
				offsetGet.abort();
				offsetGet.releaseConnection();
			}
			
		}
		return offset;
	}
	public static long setUserOffset(String username) {
		long offset = fetchUserOffset(username);
		sessionOffsets.put(getUserSession(), offset);
		TimeShifterUtil.setUserOffset(username, Long.toString(offset));
		return offset;
	}
	
	public static long getSessionOffsetIfAvailable() {
		Long offset = sessionOffsets.get(getUserSession());
		if(offset != null) {
			return offset;
		}
		return 0l;
	}
	
	public static synchronized long getUserOffset() {
		Long offset = 0l;
		if(!GhixPlatformConstants.TIMESHIFT_ENABLED) {
			return offset;
		}
		String userName=null;
		userName = getUserName();
		// No TS support for Anoymous users
		if(userName == null || userName.equalsIgnoreCase("Anonymous".intern())) {
			return offset;
		}
		// Check the context
		String tmp = TimeshiftContext.getCurrent().getTimeOffset();
		if(tmp != null) {
			offset = Long.parseLong(tmp);
			logger.info("Found user offset {} in the TS Context for user {}, effective date {}", offset, userName, sdf.format(new Date(System.currentTimeMillis()+offset)));
			return offset;
		}
		String sessionId = getUserSession(); 
		if(sessionId != null) {
			offset = sessionOffsets.get(sessionId);// Check by session
			if(offset != null) {
				if(logger.isInfoEnabled()) {
					logger.info("Offset {} available in session cache for user: {}, effective date {}",offset, userName,sdf.format(new Date(System.currentTimeMillis()+offset)));
				}
				return offset;
			}
		}
		// Now check the RequestContext, in case we have child threads 
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		if(ra != null) {
			tmp = (String) ra.getAttribute("X-USER-OFFSET".intern(), RequestAttributes.SCOPE_SESSION);
			if(tmp != null) {
				offset = Long.parseLong(tmp);
				logger.info("Found the offset from RequestAttributes for user {} as {} by thread {}, effective date {}", userName, offset, Thread.currentThread().getName(),sdf.format(new Date(System.currentTimeMillis()+offset)));
				TimeShifterUtil.setUserOffset(userName, tmp);
				return offset;
			}
		}else {
			logger.debug("Attempted Request attributes for offset information, not available".intern());
		}
		
		if(parser == null) {
			parser = new JSONParser();
		}
		if(logger.isDebugEnabled()) {
			logger.debug("No Offset available for {}, trying to reach the external service", userName);
		}
		offset = fetchUserOffset(userName);
		sessionOffsets.put(sessionId, offset);
		TimeShifterUtil.setUserOffset(userName, Long.toString(offset));
		return offset;
	}
	
	public static String getUserName() {
		String userName = null;
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if(auth != null) {
			Object principal = auth.getPrincipal();
			if(principal != null && principal instanceof AccountUser) {
				AccountUser user = (AccountUser)principal;
				if(!user.getUsername().equalsIgnoreCase("Anonymous")) {
					userName= user.getUsername();
					logger.info("Found username {} from security context".intern(), userName);
				}
			}
		}
		// Check if the thread local context has user information available
		if(userName == null) {
			userName = TimeshiftContext.getCurrent().getUserName();
			if(userName == null) {
				logger.debug("Checked Timeshift Context for username availability, not found".intern());
			}
		}
		// Now check the request ContextHolder
		if(userName == null) {
			logger.debug("Checking request attributes for offset availability".intern());
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			if(ra != null) {
				userName = (String) ra.getAttribute("X-USER".intern(), RequestAttributes.SCOPE_SESSION);
				if(userName == null) {
					logger.debug("Checked RequestAttributes Context for username availability, not found".intern());
				}else {
					logger.info("Checked RequestAttributes Context for username availability, found user {}".intern(),userName);
				}
			}else {
				logger.debug("Attempted Request attributes for user information, not available".intern());
			}
			
		}
		if(logger.isDebugEnabled() && userName == null) {
			logger.debug("No user found in the session, assuming Anonymous");
		}
		return userName;
	}
	
	public static String getUserSession() {
		String id = null;
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra != null){
			try {
				HttpSession session = sra.getRequest().getSession(false);
				if(session != null) {
					id= session.getId();
					sessions.put(id, session);
				}
			}catch(Exception e) {
				//ignored
			}
		}
		return id;
	}
	
	public static void initializeTimeContext(TimeshiftSupport tsSupportedObject) {
		String user = tsSupportedObject.getUser();
		String offset = tsSupportedObject.getTsOffset();
		if(user.equalsIgnoreCase("na".intern())) {
			logger.info("No user context available from the DTO, checking the request attributes if any".intern());
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if (sra!= null){
				HttpServletRequest req = sra.getRequest();
				user = req.getHeader("X-USER".intern());
				offset = req.getHeader("X-USER-OFFSET".intern());
				if(logger.isInfoEnabled()) {
					logger.info("Checked the request context for TS context for user {} with offset {}", user, offset);
				}
				TimeShifterUtil.setUserOffset(user, offset);
				return;
			}
		}
		if(logger.isInfoEnabled()) {
			logger.info("TS Context available for user {} with offset {}",user,offset);
		}
		TimeShifterUtil.setUserOffset(user, offset);
	}

}
