package com.getinsured.timeshift;

import java.time.Instant;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class TSInstant {
	
	public static Instant now() {
		Instant ins = null;
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			ins = Instant.ofEpochSecond(TimeShifterUtil.currentTimeMillis()/1000);
		}else {
			ins = Instant.now();
		}
		return ins;
	}

}
