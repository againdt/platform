package com.getinsured.timeshift;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class TimeshiftContext {

	private static final ThreadLocal<TimeshiftContext> CONTEXT = new InheritableThreadLocal<TimeshiftContext>();
	private String userName;
	private String timeOffset;
	private static int count = 0;
	private static Map<String,String> allContexts = Collections.synchronizedMap(new HashMap<String,String>());
	private static Logger logger = LoggerFactory.getLogger(TimeshiftContext.class);
	protected TimeshiftContext() {
	}

	public static TimeshiftContext getCurrent() {
		TimeshiftContext context = CONTEXT.get();
		if (context == null) {
			context = new TimeshiftContext();
			CONTEXT.set(context);
		}
		return context;
	}

	public static void clearCurrent() {
		if(logger.isInfoEnabled()) {
			TimeshiftContext ctx = CONTEXT.get();
			if(ctx != null) {
				logger.info("Context cleared for user {} and offset {}", ctx.getUserName(), ctx.getTimeOffset());
			}
		}
		CONTEXT.remove();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
		if(this.timeOffset != null) {
			allContexts.put(Thread.currentThread().getName()+userName, timeOffset);
			if(allContexts.size() > count) {
				dumpContext();
			}
			count = allContexts.size();
		}
	}

	private void dumpContext() {
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			for(Map.Entry<String, String> tmp: allContexts.entrySet()) {
				logger.info("User {}, Offset {}",tmp.getKey(), tmp.getValue());
			}
		}
	}
	public String getTimeOffset() {
		return timeOffset;
	}
	public void setTimeOffset(String timeOffset) {
		if(timeOffset == null || timeOffset.length() == 0) {
			this.timeOffset = "0";
			return;
		}
		try {
			Long.parseLong(timeOffset);
			this.timeOffset = timeOffset;
			if(this.userName != null) {
				allContexts.put(Thread.currentThread().getName()+userName, timeOffset);
			}
			if(allContexts.size() > count) {
				dumpContext();
			}
			count = allContexts.size();
		}catch (NumberFormatException ne) {
			logger.info("Invalid offset received, {}, offset for user {}, assuming no timeshift required",timeOffset , this.userName);
			this.timeOffset = "0".intern();
		}
	}
}
