package com.getinsured.timeshift.util;

import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.timeshift.TimeShifterUtil;

public class TSDate extends java.util.Date {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TSDate() {
		super();
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			long offset = TimeShifterUtil.currentTimeMillis();
			super.setTime(offset);
		}
	}

	public TSDate(long date) {
		super(date);
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			super.setTime(date+TimeShifterUtil.getUserOffset());
		}
	}
	
	public static TSDate getNoOffsetTSDate(java.util.Date date) {
		TSDate dt = new TSDate();
		dt.setTime(date.getTime());
		return dt;
	}
	
	public static TSDate getNoOffsetTSDate(long time) {
		TSDate dt = new TSDate();
		dt.setTime(time);
		return dt;
	}

}
