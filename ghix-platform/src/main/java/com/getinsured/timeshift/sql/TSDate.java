package com.getinsured.timeshift.sql;

import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.timeshift.TimeShifterUtil;

public class TSDate extends java.sql.Date {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TSDate(long date) {
		super(date);
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			super.setTime(date+TimeShifterUtil.getUserOffset());
		}
	}
	
	public TSDate(long date, boolean applyTimeshift) {
		super(date);
		if(GhixPlatformConstants.TIMESHIFT_ENABLED && applyTimeshift) {
			super.setTime(date+TimeShifterUtil.getUserOffset());
		}
	}

}
