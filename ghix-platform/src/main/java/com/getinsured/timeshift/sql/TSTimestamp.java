package com.getinsured.timeshift.sql;

import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.timeshift.TimeShifterUtil;

public class TSTimestamp extends java.sql.Timestamp {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TSTimestamp() {
		super(System.currentTimeMillis());
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			super.setTime(TimeShifterUtil.currentTimeMillis());
		}
	}
	
	public TSTimestamp(long time) {
		super(System.currentTimeMillis());
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			super.setTime(time + TimeShifterUtil.getUserOffset());
		}
	}
	
	public TSTimestamp(long time, boolean offset) {
		super(time);
		if(offset) {
			super.setTime(time + TimeShifterUtil.getUserOffset());
		}
	}
	
}
