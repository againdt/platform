package com.getinsured.timeshift;


import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.getinsured.hix.platform.util.GhixPlatformConstants;

public class TSLocalTime {
	
	public static LocalTime getLocalTimeInstance() {
		LocalTime dt = null;
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			dt = new LocalTime(TimeShifterUtil.currentTimeMillis());
		}else {
			dt = new LocalTime();
		}
		return dt;
	}
	
	public static LocalDate getLocalDateInstance() {
		LocalDate dt = null;
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			dt = new LocalDate(TimeShifterUtil.currentTimeMillis());
		}else {
			dt = new LocalDate();
		}
		return dt;
	}
}
