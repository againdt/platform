package com.getinsured.timeshift;

public interface TimeshiftSupport {
	String getUser();
	String getTsOffset();
}
