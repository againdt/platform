package com.getinsured.affiliate.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.getinsured.hix.model.ActiveFlag;
import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.String2ConfigurationDTO;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.service.AffiliateRedisService;
import com.getinsured.hix.platform.util.ModuleContextProvider;
import com.getinsured.hix.platform.util.SpringApplicationContext;

/**
 * Affiliate model class.
 *
 * @author Ekram Ali Kazi
 */
@Entity
@Table(name = "AFF_AFFILIATE")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
public class Affiliate implements Serializable{

	private static final long serialVersionUID = 908729144240801990L;
	
	private static final Logger LOGGER = Logger.getLogger(Affiliate.class);
	
	private static final boolean isRedisEnabled = System.getProperty("redis.enabled") == null ? false : "on".equalsIgnoreCase(System.getProperty("redis.enabled"));
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AFF_AFFILIATE_SEQ")
	@SequenceGenerator(name = "AFF_AFFILIATE_SEQ", sequenceName = "AFF_AFFILIATE_SEQ", allocationSize = 1)
	private Long affiliateId;

	@NotEmpty
	@Size(min=1, max=64)
	@Column(name = "COMPANY_NAME")
	private String companyName;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "LOCATION_ID")
	private Location location;
   
	
	@Column(name = "WEBSITE")
	private String website;
    
	@NotEmpty
	@Size(min=1, max=100)
	@Column(name = "ACCOUNT_EXECUTIVE")
	private String accountExecutive;

	//Pattern is required, because original @Email validation doesn't work as expected
	
	@NotEmpty
	@Pattern(regexp="^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$",message="Invalid Email format")
	@Size(min=1, max=100)
	@Column(name = "EMAIL")
	private String email;

	
	@NotEmpty
	@Size(min=1, max=25)
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="Invalid Phone Number format")
	@Column(name = "PHONE")
	private String phone;
	
	@Column(name = "MANAGER_NAME")
	private String managerName;

	@JsonManagedReference("affiliate-affiliateflow")
	@OneToMany(targetEntity = AffiliateFlow.class, cascade = CascadeType.ALL, mappedBy = "affiliate", fetch = FetchType.LAZY)
	private List<AffiliateFlow>	affiliateFlowList = new ArrayList<AffiliateFlow>();

	@Embedded
	private Audit audit;
	
	@Column(name="API_KEY", insertable=true)
	private String apiKey;
	
	@Column(name = "AFFILIATE_STATUS")
	private String affiliateStatus;
	
	@Column(name = "URL")
	private String url;

	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	@Column(name = "CONFIGURATION")
	private String configuration;

	@Column(name = "HAS_EMPLOYER")
	@Enumerated(EnumType.STRING)
	private ActiveFlag hasEmployer;
	
	@Transient
	private ConfigurationDTO affiliateConfig;

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAccountExecutive() {
		return accountExecutive;
	}

	public void setAccountExecutive(String accountExecutive) {
		this.accountExecutive = accountExecutive;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public List<AffiliateFlow> getAffiliateFlowList(){
		return affiliateFlowList;
	}

	public void setAffiliateFlowList(List<AffiliateFlow> affiliateFlowList){
		this.affiliateFlowList = affiliateFlowList;
	}

	public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public String getAffiliateStatus() {
		return affiliateStatus;
	}

	public void setAffiliateStatus(String affiliateStatus) {
		this.affiliateStatus = affiliateStatus;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}
	
	public ConfigurationDTO getAffiliateConfig() {
		return affiliateConfig;
	}

	public void setAffiliateConfig(ConfigurationDTO affiliateConfig) {
		this.affiliateConfig = affiliateConfig;
	}

	public ActiveFlag getHasEmployer() {
		return hasEmployer;
	}
	
	public void setHasEmployer(ActiveFlag hasEmployer) {
		this.hasEmployer = hasEmployer;
	}
	
	@Override
	public String toString() {
		return "Affiliate [affiliateId=" + affiliateId + ", companyName="
				+ companyName + ", location=" + location + ", website="
				+ website + ", accountExecutive=" + accountExecutive
				+ ", email=" + email + ", phone=" + phone + ", managerName="
				+ managerName + ", audit=" + audit + ", apiKey=" + apiKey
				+ ", affiliateStatus=" + affiliateStatus + "]";
	}

	/*@Override
	public String toString(){
		final ReflectionToStringBuilder reflectionToStringBuilder = new ReflectionToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		reflectionToStringBuilder.setExcludeFieldNames(new String[] {"affiliateFlowList"});
		return reflectionToStringBuilder.toString();
	}*/
	@Override
	public int hashCode() {
		if(this.affiliateId!=null){
			return affiliateId.intValue();
		}else{
			return super.hashCode();
		}
	}
	    
    @Override
    public boolean equals(Object obj) {
    	
    	if(this.affiliateId!=null  && obj!=null){
    		Affiliate afl = (Affiliate) obj;
    		return  afl.affiliateId==this.affiliateId;
    	}else{
    		return super.equals(obj);
    	}
    }
    
	@PrePersist
	public void onPrePersist() {
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        this.setTenantId(TenantContextHolder.getTenant().getId());
		}
	}
	
    @PostLoad
    public void populateConfigurationData() {
    	this.setAffiliateConfig(String2ConfigurationDTO.Current.convert(this.getConfiguration()));
    }
    
    public void updateConfigurationData() {
		this.setConfiguration(String2ConfigurationDTO.Current.reverseConvert(this.affiliateConfig));
	}
    
    @PostUpdate
    @PostPersist
    public void onPostPersist() {
    	if(isRedisEnabled && this.getUrl() != null) {
    		try {
    			AffiliateRedisService affiliateRedisService = (AffiliateRedisService) GHIXApplicationContext.getBean("affiliateRedisService");
    			affiliateRedisService.set(this.getUrl(), this);
    		} catch (Exception e) {
    			LOGGER.error("Error caching affiliate in redis", e);
    		}
    	}
    }
}
