package com.getinsured.affiliate.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Affiliate model class.
 *
 * @author Nitin Makhija
 */
@Entity
@Table(name = "AFF_CLICK")
public class AffiliateClick implements Serializable{

	private static final long serialVersionUID = 908729144240801990L;
	public static enum ProductType { HEALTH, DENTAL, AME, VISION, STM, MEDICARE;}

	@Id
	@Column(name = "ID")  
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AFF_CLICK_SEQ")
	@SequenceGenerator(name = "AFF_CLICK_SEQ", sequenceName = "AFF_CLICK_SEQ", allocationSize = 1)
	private Long clickId;
	

	@Column(name = "AFFILIATE_ID")
	private Long affiliateId;
	
	@Column(name = "FLOW_ID")
	private int affiliateFlowId;
	
	
	@Column(name = "AFFILIATE_HOUSEHOLD_ID")
	private String affiliateHouseholdId;
	
	@Column(name = "LEAD_ID")
	private Long LeadId;
	
	@XmlElement(name="zip")
	@Column(name = "zip")
	private String zip;
	
	
	@XmlElement(name="county")
	@Column(name = "county")
	private String county;
	
	@Column(name = "FAMILY_SIZE")
	private Long familySize;
	
	@Column(name="MEMBER_DATA" ,length=4000)
	private String memberData;
	
	@Column(name = "ANNUAL_HOUSEHOLD_INCOME")
	private Double annualHouseholdIncome;


	@Column(name = "EMAIL")
	private String email;

	@Column(name = "PHONE")
	private String phone;
	
	@Column(name = "PLAN_ID")
	private String planId;
	
	@Column(name = "CAMPAIGN_REF_ID")
	private String campaignRefId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Column(name = "REDIRECTION_URL")
	private String redirectionUrl;
	
	@Column(name = "PRODUCT_TYPE")
	@Enumerated(EnumType.STRING)
	private ProductType productType;
	
	public Long getClickId() {
		return clickId;
	}

	public void setClickId(Long clickId) {
		this.clickId = clickId;
	}

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public int getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(int affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	public String getAffiliateHouseholdId() {
		return affiliateHouseholdId;
	}

	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.affiliateHouseholdId = affiliateHouseholdId;
	}

	public Long getLeadId() {
		return LeadId;
	}

	public void setLeadId(Long leadId) {
		LeadId = leadId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Long getFamilySize() {
		return familySize;
	}

	public void setFamilySize(Long familySize) {
		this.familySize = familySize;
	}

	public String getMemberData() {
		return memberData;
	}

	public void setMemberData(String memberData) {
		this.memberData = memberData;
	}

	public Double getAnnualHouseholdIncome() {
		return annualHouseholdIncome;
	}

	public void setAnnualHouseholdIncome(Double annualHouseholdIncome) {
		this.annualHouseholdIncome = annualHouseholdIncome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getCampaignRefId() {
		return campaignRefId;
	}

	public void setCampaignRefId(String campaignRefId) {
		this.campaignRefId = campaignRefId;
	}
	
}
