package com.getinsured.affiliate.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * Campaign model class.
 *
 * @author Ekram Ali Kazi
 */
@Entity
@Table(name = "AFF_CAMPAIGN")
public class Campaign implements Serializable {

	private static final long serialVersionUID = 7217509873879514887L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AFF_CAMPAIGN_SEQ")
	@SequenceGenerator(name = "AFF_CAMPAIGN_SEQ", sequenceName = "AFF_CAMPAIGN_SEQ", allocationSize = 1)
	private Long campaignId;

	/*@JsonBackReference("affiliate-campaign")
	@OneToOne(targetEntity = Affiliate.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "AFFILIATE_ID", referencedColumnName = "ID")
	private Affiliate affiliate;*/

	@JsonBackReference("affiliateflow-campaign")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FLOW_ID")
	private AffiliateFlow affiliateFlow;

	@Column(name = "CAMPAIGN_NAME")
	private String campaignName;

	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "START_DATE")
	private Date startDate;

	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "STOP_DATE")
	private Date endDate;

	
	@Column(name = "AMOUNT")
	@Min(value=0,message="Amount should not be a negative value ")
	private double amount;

	@Embedded
	private Audit audit;

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	/*public Affiliate getAffiliate(){
		return affiliate;
	}

	public void setAffiliate(Affiliate affiliate){
		this.affiliate = affiliate;
	}*/

	public String getCampaignName(){
		return campaignName;
	}

	public void setCampaignName(String campaignName){
		this.campaignName = campaignName;
	}

	public AffiliateFlow getAffiliateFlow(){
		return affiliateFlow;
	}

	public void setAffiliateFlow(AffiliateFlow affiliateFlow){
		this.affiliateFlow = affiliateFlow;
	}

	public Date getStartDate(){
		return startDate;
	}

	public void setStartDate(Date startDate){
		this.startDate = startDate;
	}

	public Date getEndDate(){
		return endDate;
	}

	public void setEndDate(Date endDate){
		this.endDate = endDate;
	}

	public double getAmount(){
		return amount;
	}

	public void setAmount(double amount){
		this.amount = amount;
	}

	public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	@Override
	public String toString() {
		return "Campaign [campaignId=" + campaignId + ", campaignName="
				+ campaignName + ", startDate=" + startDate + ", endDate="
				+ endDate + ", amount=" + amount + ", audit=" + audit + "]";
	}

/*	@Override
	public String toString(){
		final ReflectionToStringBuilder reflectionToStringBuilder = new ReflectionToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		reflectionToStringBuilder.setExcludeFieldNames(new String[] {"affiliate", "affiliateFlow" });
		return reflectionToStringBuilder.toString();
	}
*/

}