package com.getinsured.affiliate.model;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Affiliate Flow Access model class.
 *
 * @author Suresh Kancherla
 */
@Entity
@Table(name = "AFF_FLOW_ACCESS")
//@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
//@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
//@EntityListeners(TenantIdPrePersistListener.class)
public class AffiliateFlowAccess implements Serializable{

	private static final long serialVersionUID = 908729144240801990L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AFF_FLOW_ACCESS_SEQ")
	@SequenceGenerator(name = "AFF_FLOW_ACCESS_SEQ", sequenceName = "AFF_FLOW_ACCESS_SEQ", allocationSize = 1)
	private Long affiliateFlowAccessId;

	@NotEmpty
	@Size(min=1, max=5)
	@Column(name = "AFFILIATE_ID")
	private Long affiliateId;

	@NotEmpty
	@Size(min=1, max=5)
	@Column(name = "AFFILIATE_FLOW_ID")
	private Long affiliateFlowId;
	
	@NotEmpty
	@Size(min=1, max=50)
	@Column(name = "ACCESS_CODE")
	private String accessCode;
	
	@Pattern(regexp ="|[NY]{1}",message="Invalid Value for IS_ACTIVE in Affiliate Flow Access" )
	@Column(name="IS_ACTIVE")
    private char isActive = 'Y';
	
	
	@Override
	public String toString() {
		return "Affiliate Flow Access [affiliateId=" + affiliateId + ", affiliateFlowId=" + affiliateFlowId + ", accessCode=" + accessCode
				+ ", affiliateFlowAccessId=" + affiliateFlowAccessId + "]";
	}

	
	@Override
	public int hashCode() {
		if(this.affiliateFlowAccessId!=null){
			return affiliateFlowAccessId.intValue();
		}else{
			return super.hashCode();
		}
	}
	    
    @Override
    public boolean equals(Object obj) {
    	
    	if(this.affiliateFlowAccessId!=null  && obj!=null){
    		AffiliateFlowAccess afa = (AffiliateFlowAccess) obj;
    		return  afa.affiliateFlowAccessId==this.affiliateFlowAccessId;
    	}else{
    		return super.equals(obj);
    	}
    }


	public Long getAffiliateFlowAccessId() {
		return affiliateFlowAccessId;
	}


	public void setAffiliateFlowAccessId(Long affiliateFlowAccessId) {
		this.affiliateFlowAccessId = affiliateFlowAccessId;
	}

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Long getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Long affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public char getIsActive() {
		return isActive;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	
}
