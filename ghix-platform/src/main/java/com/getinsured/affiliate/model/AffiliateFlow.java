package com.getinsured.affiliate.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.getinsured.affiliate.enums.AffiliateAncillaryStatus;
import com.getinsured.affiliate.enums.AffiliateFlowStatus;
import com.getinsured.affiliate.enums.AffiliateTrafficType;
import com.getinsured.affiliate.enums.CallPayoutType;
import com.getinsured.affiliate.enums.CallPricingModel;
import com.getinsured.affiliate.enums.WebPayoutType;
import com.getinsured.affiliate.enums.WebPricingModel;
import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.model.String2ConfigurationDTO;
import com.getinsured.hix.model.TenantIdPrePersistListener;

/**
 * AffiliateFlow model class.
 *
 * @author Ekram Ali Kazi
 */
@Audited
@Entity
@Table(name = "AFF_FLOW")
@FilterDef(name = "tenantIdFilter", parameters = {@ParamDef(name = "tenantId", type = "long")})
@Filters({@Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId")})
@EntityListeners({TenantIdPrePersistListener.class})
public class AffiliateFlow implements Serializable
{
  private static final long serialVersionUID = 8886140504004627925L;
  private static final String NULL_CLOB_STR = "(null)";

  public enum WindowConfig
  {
    _self,
    _blank,
    _parent
  }

  public enum OnOffConfig
  {
    ON("ON"), OFF("OFF");

    private String description;

    OnOffConfig(String description)
    {
      this.description = description;
    }

    public String getDescription()
    {
      return description;
    }
  }

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AFF_FLOW_SEQ")
  @SequenceGenerator(name = "AFF_FLOW_SEQ", sequenceName = "AFF_FLOW_SEQ", allocationSize = 1)
  private Integer affiliateflowId;

  @NotAudited
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  @JsonBackReference("affiliate-affiliateflow")
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "AFFILIATE_ID")
  private Affiliate affiliate;

  @Column(name = "WEB_PRICING_MODEL")
  @Enumerated(EnumType.STRING)
  private WebPricingModel webPricingModel;

  @Column(name = "WEB_PAYOUT_TYPE")
  @Enumerated(EnumType.STRING)
  private WebPayoutType webPayoutType;

  @Column(name = "WEB_PAYOUT")
  @Min(value = 0, message = "webPayout value should be non negative value")
  private double webPayoutvalue;

  @Column(name = "CALL_PRICING_MODEL")
  @Enumerated(EnumType.STRING)
  private CallPricingModel callPricingModel;

  @Column(name = "CALL_PAYOUT_TYPE")
  @Enumerated(EnumType.STRING)
  private CallPayoutType callPayoutType;

  @Column(name = "CALL_PAYOUT")
  @Min(value = 0, message = "callPayout value should be non negative value")
  private double callPayoutvalue;

  @NotAudited
  @NotEmpty
  @Size(min = 1, max = 200)
  @Column(name = "REDIRECTION_URL")
  private String redirectionUrl;

  /**
   * https://hibernate.atlassian.net/browse/HHH-5255
   * Byte array marked with lazy load are not lazy loaded due to the above bug which is still an issue in ORM 4.
   * Hence Byte array has to be wrapped by an object of type java.sql.blob
   */
  @NotAudited
  @Type(type = "org.hibernate.type.BinaryType")
  @Basic(fetch = FetchType.LAZY)
  @Column(length = 100000, name = "LOGO")
  private byte[] logo;

  @NotAudited
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  @JsonManagedReference("affiliateflow-campaign")
  @OneToOne(targetEntity = Campaign.class, cascade = CascadeType.ALL, mappedBy = "affiliateFlow", fetch = FetchType.LAZY)
  private Campaign campaign;

  @NotEmpty
  @Size(min = 1, max = 100)
  @Column(name = "FLOW_NAME")
  private String flowName;

  @Column(name = "STATUS")
  @Enumerated(EnumType.STRING)
  private AffiliateFlowStatus flowStatus;

  @NotAudited
  @Column(name = "TRAFFIC_TYPE")
  @Enumerated(EnumType.STRING)
  private AffiliateTrafficType trafficType;

  @NotAudited
  @Column(name = "IVR_NUMBER")
  private Long ivrNumber;

  @NotAudited
  @Column(name = "CUSTOM_FIELD_1")
  private String customField1;

  @NotAudited
  @Column(name = "CUSTOM_FIELD_2")
  private String customField2;

  @NotAudited
  @Column(name = "CUSTOM_FIELD_3")
  private String customField3;

  @NotAudited
  @Column(name = "CUSTOM_FIELD_4")
  private String customField4;

  @NotAudited
  @Column(name = "INBOUND_GREETING")
  private String inboundGreeting;

  @NotAudited
  @Column(name = "OUTBOUND_GREETING")
  private String outboundGreeting;

  @NotAudited
  @Column(name = "BUSINESS_TERMS_CONDITIONS")
  private String businessTermsConditons;

  @NotAudited
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  @Embedded
  private Audit audit;

  @NotAudited
  @Column(name = "ON_EXCHANGE_REDIRECT_URL")
  private String onExchgRedirectionUrl;

  @NotAudited
  @Column(name = "OFF_EXCHANGE_REDIRECT_URL")
  private String offExchgRedirectionUrl;

  @NotAudited
  @Column(name = "MEDICAID_REDIRECT_URL")
  private String medicaidRedirectionUrl;

  @NotAudited
  @Column(name = "MEDICARE_REDIRECT_URL")
  private String medicareRedirectionUrl;

  @NotAudited
  @Column(name = "STM_REDIRECT_URL")
  private String shortTermMedicalRedirectionUrl;

  @NotAudited
  @Column(name = "ANCILLARY_STM")
  @Enumerated(EnumType.STRING)
  private AffiliateAncillaryStatus ancillarySTM;

  @NotAudited
  @Column(name = "ANCILLARY_DENTAL")
  @Enumerated(EnumType.STRING)
  private AffiliateAncillaryStatus ancillaryDental;

  @NotAudited
  @Column(name = "ANCILLARY_AME")
  @Enumerated(EnumType.STRING)
  private AffiliateAncillaryStatus ancillaryAME;

  @NotAudited
  @Column(name = "ANCILLARY_VISION")
  @Enumerated(EnumType.STRING)
  private AffiliateAncillaryStatus ancillaryVision;

  @NotAudited
  @Column(name = "GI_MEDICARE")
  @Enumerated(EnumType.STRING)
  private AffiliateAncillaryStatus affiliateMedicare;

  @NotAudited
  @Column(name = "ON_EXCHANGE_SELF_WINDOW")
  @Enumerated(EnumType.STRING)
  private WindowConfig onExchSelfWindow;

  @NotAudited
  @Column(name = "OFF_EXCHANGE_SELF_WINDOW")
  @Enumerated(EnumType.STRING)
  private WindowConfig offExchSelfWindow;

  @NotAudited
  @Column(name = "MEDICAID_SELF_WINDOW")
  @Enumerated(EnumType.STRING)
  private WindowConfig medicaidSelfWindow;

  @NotAudited
  @Column(name = "MEDICARE_SELF_WINDOW")
  @Enumerated(EnumType.STRING)
  private WindowConfig medicareSelfWindow;

  @NotAudited
  @Column(name = "STM_SELF_WINDOW")
  @Enumerated(EnumType.STRING)
  private WindowConfig stmSelfWindow;

  @NotAudited
  @Column(name = "LOGO_URL")
  private String logoURL;

  @NotAudited
  @Column(name = "GPS_PREF_POP_UP")
  @Enumerated(EnumType.STRING)
  private OnOffConfig gpsPreferencePopUp;

  @NotAudited
  @Column(name = "TENANT_ID")
  private Long tenantId;

  @NotAudited
  @Column(name = "IS_DEFAULT")
  private String isDefault;

  @NotAudited
  @Basic(fetch = FetchType.LAZY)
  @Column(name = "CONFIGURATIONS")
  private String configurations;

  @NotAudited
  @Column(name = "URL")
  private String url;

  @NotAudited
  @Column(name = "IVR_NUMBER2")
  private Long ivrNumberTwo;

  @Transient
  private ConfigurationDTO affiliateFlowConfig;

  @Transient
  private boolean isFlowLevelReadOnlyFlag;

  public Long getTenantId()
  {
    return tenantId;
  }

  public void setTenantId(Long tenantId)
  {
    this.tenantId = tenantId;
  }

  public Integer getAffiliateflowId()
  {
    return affiliateflowId;
  }

  public void setAffiliateflowId(Integer affiliateflowId)
  {
    this.affiliateflowId = affiliateflowId;
  }

  public Affiliate getAffiliate()
  {
    return affiliate;
  }

  public void setAffiliate(Affiliate affiliate)
  {
    this.affiliate = affiliate;
  }

  public String getFlowName()
  {
    return flowName;
  }

  public void setFlowName(String flowName)
  {
    this.flowName = flowName;
  }

  public AffiliateFlowStatus getFlowStatus()
  {
    return flowStatus;
  }

  public void setFlowStatus(AffiliateFlowStatus flowStatus)
  {
    this.flowStatus = flowStatus;
  }

  public AffiliateTrafficType getTrafficType()
  {
    return trafficType;
  }

  public void setTrafficType(AffiliateTrafficType trafficType)
  {
    this.trafficType = trafficType;
  }

  public WebPricingModel getWebPricingModel()
  {
    return webPricingModel;
  }

  public void setWebPricingModel(WebPricingModel webPricingModel)
  {
    this.webPricingModel = webPricingModel;
  }

  public WebPayoutType getWebPayoutType()
  {
    return webPayoutType;
  }

  public void setWebPayoutType(WebPayoutType webPayoutType)
  {
    this.webPayoutType = webPayoutType;
  }

  public double getWebPayoutvalue()
  {
    return webPayoutvalue;
  }

  public void setWebPayoutvalue(double webPayoutvalue)
  {
    this.webPayoutvalue = webPayoutvalue;
  }

  public CallPricingModel getCallPricingModel()
  {
    return callPricingModel;
  }

  public void setCallPricingModel(CallPricingModel callPricingModel)
  {
    this.callPricingModel = callPricingModel;
  }

  public CallPayoutType getCallPayoutType()
  {
    return callPayoutType;
  }

  public void setCallPayoutType(CallPayoutType callPayoutType)
  {
    this.callPayoutType = callPayoutType;
  }

  public double getCallPayoutvalue()
  {
    return callPayoutvalue;
  }

  public void setCallPayoutvalue(double callPayoutvalue)
  {
    this.callPayoutvalue = callPayoutvalue;
  }

  public Long getIvrNumber()
  {
    return ivrNumber;
  }

  public void setIvrNumber(Long ivrNumber)
  {
    this.ivrNumber = ivrNumber;
  }

  public String getCustomField1()
  {
    return customField1;
  }

  public void setCustomField1(String customField1)
  {
    this.customField1 = customField1;
  }

  public String getCustomField2()
  {
    return customField2;
  }

  public void setCustomField2(String customField2)
  {
    this.customField2 = customField2;
  }

  public String getCustomField3()
  {
    return customField3;
  }

  public void setCustomField3(String customField3)
  {
    this.customField3 = customField3;
  }

  public String getCustomField4()
  {
    return customField4;
  }

  public void setCustomField4(String customField4)
  {
    this.customField4 = customField4;
  }

  public String getInboundGreeting()
  {
    return inboundGreeting;
  }

  public void setInboundGreeting(String inboundGreeting)
  {
    this.inboundGreeting = inboundGreeting;
  }

  public String getOutboundGreeting()
  {
    return outboundGreeting;
  }

  public void setOutboundGreeting(String outboundGreeting)
  {
    this.outboundGreeting = outboundGreeting;
  }

  public String getBusinessTermsConditons()
  {
    return businessTermsConditons;
  }

  public void setBusinessTermsConditons(String businessTermsConditons)
  {
    this.businessTermsConditons = businessTermsConditons;
  }

  public Campaign getCampaign()
  {
    return campaign;
  }

  public void setCampaign(Campaign campaign)
  {
    this.campaign = campaign;
  }

  public Audit getAudit()
  {
    return audit;
  }

  public void setAudit(Audit audit)
  {
    this.audit = audit;
  }


  public boolean isFlowLevelReadOnlyFlag()
  {
    return isFlowLevelReadOnlyFlag;
  }

  public void setFlowLevelReadOnlyFlag(boolean isFlowLevelReadOnlyFlag)
  {
    this.isFlowLevelReadOnlyFlag = isFlowLevelReadOnlyFlag;
  }

  public String getRedirectionUrl()
  {
    return redirectionUrl;
  }

  public void setRedirectionUrl(String redirectionUrl)
  {
    this.redirectionUrl = redirectionUrl;
  }

  public byte[] getLogo()
  {
    return logo;
  }

  public void setLogo(byte[] logo)
  {
    this.logo = logo;
  }

  public String getOnExchgRedirectionUrl()
  {
    return onExchgRedirectionUrl;
  }

  public void setOnExchgRedirectionUrl(String onExchgRedirectionUrl)
  {
    this.onExchgRedirectionUrl = onExchgRedirectionUrl;
  }

  public String getOffExchgRedirectionUrl()
  {
    return offExchgRedirectionUrl;
  }

  public void setOffExchgRedirectionUrl(String offExchgRedirectionUrl)
  {
    this.offExchgRedirectionUrl = offExchgRedirectionUrl;
  }

  public String getMedicaidRedirectionUrl()
  {
    return medicaidRedirectionUrl;
  }

  public void setMedicaidRedirectionUrl(String medicaidRedirectionUrl)
  {
    this.medicaidRedirectionUrl = medicaidRedirectionUrl;
  }

  public String getMedicareRedirectionUrl()
  {
    return medicareRedirectionUrl;
  }

  public void setMedicareRedirectionUrl(String medicareRedirectionUrl)
  {
    this.medicareRedirectionUrl = medicareRedirectionUrl;
  }

  public String getShortTermMedicalRedirectionUrl()
  {
    return shortTermMedicalRedirectionUrl;
  }

  public void setShortTermMedicalRedirectionUrl(
      String shortTermMedicalRedirectionUrl)
  {
    this.shortTermMedicalRedirectionUrl = shortTermMedicalRedirectionUrl;
  }


  public AffiliateAncillaryStatus getAncillarySTM()
  {
    return ancillarySTM;
  }

  public void setAncillarySTM(AffiliateAncillaryStatus ancillarySTM)
  {
    this.ancillarySTM = ancillarySTM;
  }

  public AffiliateAncillaryStatus getAncillaryDental()
  {
    return ancillaryDental;
  }

  public void setAncillaryDental(AffiliateAncillaryStatus ancillaryDental)
  {
    this.ancillaryDental = ancillaryDental;
  }

  public AffiliateAncillaryStatus getAncillaryAME()
  {
    return ancillaryAME;
  }

  public void setAncillaryAME(AffiliateAncillaryStatus ancillaryAME)
  {
    this.ancillaryAME = ancillaryAME;
  }

  public AffiliateAncillaryStatus getAncillaryVision()
  {
    return ancillaryVision;
  }

  public void setAncillaryVision(AffiliateAncillaryStatus ancillaryVision)
  {
    this.ancillaryVision = ancillaryVision;
  }

  public WindowConfig getOnExchSelfWindow()
  {
    return onExchSelfWindow;
  }

  public void setOnExchSelfWindow(WindowConfig onExchSelfWindow)
  {
    this.onExchSelfWindow = onExchSelfWindow;
  }

  public WindowConfig getOffExchSelfWindow()
  {
    return offExchSelfWindow;
  }

  public void setOffExchSelfWindow(WindowConfig offExchSelfWindow)
  {
    this.offExchSelfWindow = offExchSelfWindow;
  }

  public WindowConfig getMedicaidSelfWindow()
  {
    return medicaidSelfWindow;
  }

  public void setMedicaidSelfWindow(WindowConfig medicaidSelfWindow)
  {
    this.medicaidSelfWindow = medicaidSelfWindow;
  }

  public WindowConfig getMedicareSelfWindow()
  {
    return medicareSelfWindow;
  }

  public void setMedicareSelfWindow(WindowConfig medicareSelfWindow)
  {
    this.medicareSelfWindow = medicareSelfWindow;
  }

  public WindowConfig getStmSelfWindow()
  {
    return stmSelfWindow;
  }

  public void setStmSelfWindow(WindowConfig stmSelfWindow)
  {
    this.stmSelfWindow = stmSelfWindow;
  }

  public String getLogoURL()
  {
    return logoURL;
  }

  public void setLogoURL(String logoURL)
  {
    this.logoURL = logoURL;
  }

  public AffiliateAncillaryStatus getAffiliateMedicare()
  {
    return affiliateMedicare;
  }

  public void setAffiliateMedicare(AffiliateAncillaryStatus affiliateMedicare)
  {
    this.affiliateMedicare = affiliateMedicare;
  }

  public OnOffConfig getGpsPreferencePopUp()
  {
    return gpsPreferencePopUp;
  }

  public void setGpsPreferencePopUp(OnOffConfig gpsPreferencePopUp)
  {
    this.gpsPreferencePopUp = gpsPreferencePopUp;
  }

  public String getIsDefault()
  {
    return isDefault;
  }

  public void setIsDefault(String isDefault)
  {
    this.isDefault = isDefault;
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof AffiliateFlow)
    {
      final AffiliateFlow other = (AffiliateFlow) obj;
      return new EqualsBuilder().append(flowName, other.flowName).isEquals();
    } else
    {
      return false;
    }
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder().append(flowName).toHashCode();
  }

  public String getConfigurations()
  {
    return configurations;
  }

  public void setConfigurations(String configurations)
  {
    this.configurations = configurations;
  }

  @PostLoad
  public void populateConfigurationData()
  {
    // TODO - Get a cleaner fix for this
    if (NULL_CLOB_STR.equals(this.configurations))
    {
      this.configurations = null;
    }
    this.affiliateFlowConfig = String2ConfigurationDTO.Current.convert(this.configurations);
  }

  public ConfigurationDTO getAffiliateFlowConfig()
  {
    return affiliateFlowConfig;
  }

  public void setAffiliateFlowConfig(ConfigurationDTO affiliateFlowConfig)
  {
    this.affiliateFlowConfig = affiliateFlowConfig;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public Long getIvrNumberTwo()
  {
    return ivrNumberTwo;
  }

  public void setIvrNumberTwo(Long ivrNumberTwo)
  {
    this.ivrNumberTwo = ivrNumberTwo;
  }

  @Override
  public String toString() {
    return "AffiliateFlow [affiliateflowId=" + affiliateflowId
        + ", webPricingModel=" + webPricingModel + ", webPayoutType="
        + webPayoutType + ", webPayoutvalue=" + webPayoutvalue
        + ", callPricingModel=" + callPricingModel
        + ", callPayoutType=" + callPayoutType + ", callPayoutvalue="
        + callPayoutvalue + ", redirectionUrl=" + redirectionUrl
        + ", flowName=" + flowName + ", flowStatus=" + flowStatus
        + ", trafficType=" + trafficType + ", ivrNumber=" + ivrNumber
        + ", customField1=" + customField1 + ", customField2="
        + customField2 + ", customField3=" + customField3
        + ", customField4=" + customField4 + ", inboundGreeting="
        + inboundGreeting + ", outboundGreeting=" + outboundGreeting
        + ", businessTermsConditons=" + businessTermsConditons
        + ", audit=" + audit + ", isFlowLevelReadOnlyFlag="
        + isFlowLevelReadOnlyFlag + "]";
  }
}