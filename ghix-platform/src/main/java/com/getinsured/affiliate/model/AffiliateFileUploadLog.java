package com.getinsured.affiliate.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * This is the model class for AFF_FILE_UPLOAD_LOG table
 * @author Kunal Dav
 * @since 04 August, 2014
 */
@Entity
@Table(name="AFF_FILE_UPLOAD_LOG")
@DynamicUpdate
@DynamicInsert
public class AffiliateFileUploadLog implements Serializable {
	
	private static final long serialVersionUID = 4145411109923886242L;
	
	public enum ISOKTOCALL {
		/** Is OK to call?  Value: Yes or NO */
		YES, NO;
	}
	
	public enum STATUS {
		/** Status for the upload process. Valid values: NEW, IN_PROGRESS, TRIGGERED_FOR_UPLOAD, PROCESSED */
		NEW, IN_PROGRESS, TRIGGERED_FOR_UPLOAD, PROCESSED;
	}
		
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AFF_FILE_UPLOAD_LOG_SEQ")
	@SequenceGenerator(name = "AFF_FILE_UPLOAD_LOG_SEQ", sequenceName = "AFF_FILE_UPLOAD_LOG_SEQ", allocationSize = 1)
	private Long id;
	
	@Column(name = "FTP_FILE_PATH")
	private String ftpFilePath;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private STATUS status;
	
	@Column(name = "TCPA_COMPLIANCE_LINK")
	private String tcpaComplianceLink;
	
	@Column(name = "IS_OK_TO_CALL")
	@Enumerated(EnumType.STRING)
	private ISOKTOCALL isOkToCall;
	
	@Column(name = "TOTAL_NO_OF_RECORDS")
	private Integer totalNoOfRecords;
	
	@Column(name = "SUCCESSFUL_COUNT")
	private Integer successfulCount;
	
	@Column(name = "DUPLICATE_COUNT")
	private Integer duplicateCount;
	
	@Column(name = "FAILED_COUNT")
	private Integer failedCount;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false, updatable=false)
	private Date creationTimestamp;
	
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "PROCESSING_TIME")
	private Long processingTime;
	
	@Column(name = "BATCH_JOB_ID")
	private Integer batchJobId;
	
	@Column(name = "AFF_AFFILIATE_ID")
	private Long affiliateId;
	
	@Column(name = "AFF_FLOW_ID")
	private Integer affiliateFlowId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFtpFilePath() {
		return ftpFilePath;
	}

	public void setFtpFilePath(String ftpFilePath) {
		this.ftpFilePath = ftpFilePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getTcpaComplianceLink() {
		return tcpaComplianceLink;
	}

	public void setTcpaComplianceLink(String tcpaComplianceLink) {
		this.tcpaComplianceLink = tcpaComplianceLink;
	}

	public ISOKTOCALL getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(ISOKTOCALL isOkToCall) {
		this.isOkToCall = isOkToCall;
	}

	public Integer getTotalNoOfRecords() {
		return totalNoOfRecords;
	}

	public void setTotalNoOfRecords(Integer totalNoOfRecords) {
		this.totalNoOfRecords = totalNoOfRecords;
	}

	public Integer getSuccessfulCount() {
		return successfulCount;
	}

	public void setSuccessfulCount(Integer successfulCount) {
		this.successfulCount = successfulCount;
	}

	public Integer getDuplicateCount() {
		return duplicateCount;
	}

	public void setDuplicateCount(Integer duplicateCount) {
		this.duplicateCount = duplicateCount;
	}

	public Integer getFailedCount() {
		return failedCount;
	}

	public void setFailedCount(Integer failedCount) {
		this.failedCount = failedCount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(Long processingTime) {
		this.processingTime = processingTime;
	}

	public Integer getBatchJobId() {
		return batchJobId;
	}

	public void setBatchJobId(Integer batchJobId) {
		this.batchJobId = batchJobId;
	}

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	@PrePersist
	public void prePersist(){
		this.setCreationTimestamp(new TSDate());
	}

	@Override
	public String toString() {
		return "AffiliateFileUploadLog [id=" + id + ", ftpFilePath="
				+ ftpFilePath + ", fileName=" + fileName + ", status=" + status
				+ ", tcpaComplianceLink=" + tcpaComplianceLink
				+ ", isOkToCall=" + isOkToCall + ", totalNoOfRecords="
				+ totalNoOfRecords + ", successfulCount=" + successfulCount
				+ ", duplicateCount=" + duplicateCount + ", failedCount="
				+ failedCount + ", description=" + description
				+ ", creationTimestamp=" + creationTimestamp + ", createdBy="
				+ createdBy + ", processingTime=" + processingTime
				+ ", batchJobId=" + batchJobId + ", affiliateId=" + affiliateId
				+ ", affiliateFlowId=" + affiliateFlowId + "]";
	}

}
