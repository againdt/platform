package com.getinsured.affiliate.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * AffiliateFlow PricingModel model class used as an {@link Embeddable} object
 * with persistence in the AffiliateFlow table.
 *
 * @author Ekram Ali Kazi
 */

@Embeddable
public class Audit implements Serializable {

	private static final long serialVersionUID = 3429646637147161962L;

	@Column(name = "CREATED_BY")
	private Long createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;

	@Column(name = "LAST_UPDATED_BY")
	private Long lastUpdatedBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdatedOn;

	public Long getCreatedBy(){
		return createdBy;
	}

	public void setCreatedBy(Long createdBy){
		this.createdBy = createdBy;
	}

	public Date getCreatedOn(){
		return createdOn;
	}

	public void setCreatedOn(Date createdOn){
		this.createdOn = createdOn;
	}

	public Long getLastUpdatedBy(){
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy){
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedOn(){
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn){
		this.lastUpdatedOn = lastUpdatedOn;
	}

	// To AutoUpdate created and updated dates while persisting object
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setLastUpdatedOn(new TSDate());
	}

	// To AutoUpdate updated dates while updating object
	@PreUpdate
	public void preUpdate(){
		this.setLastUpdatedOn(new TSDate());
	}

	@Override
	public String toString() {
		return "Audit [createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedOn="
				+ lastUpdatedOn + "]";
	}

	/*@Override
	public String toString()	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}*/
	
	
}
