package com.getinsured.affiliate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * Spring-JPA repository that encapsulates methods that interact with the
 * Affiliate entity.
 *
 * @author Ekram Ali Kazi
 *
 */
@Repository
@Transactional
public interface PlatformAffiliateRepository extends TenantAwareRepository<Affiliate, Long>
{
  /**
   * HQL-JPA query to return List<Affiliate> for affiliateId.
   * This helps in leveraging LAZY behavior as default for Affiliate entity.
   *
   * @param affiliateId
   * @return List<Affiliate>
   */
  @Query("Select aff " +
      " FROM Affiliate as aff " +
      " inner join fetch aff.affiliateFlowList as affFlowList" +
      " where aff.affiliateId = :affiliateId ")
  List<Affiliate> getAffiliates(@Param("affiliateId") Long affiliateId);

  @Query("select e from Affiliate e where e.companyName = :companyName")
  Affiliate findByCompanyName(@Param("companyName") String companyName);

  @Query("select e from Affiliate e where e.affiliateStatus = 'ACTIVE' and e.apiKey = :apiKey")
  Affiliate getAffiliateByApiKey(@Param("apiKey") String apiKey);

  @Query("SELECT a FROM Affiliate a WHERE a.tenantId = :tenantId order by a.affiliateId desc")
  List<Affiliate> getAffiliatesByTenantId(@Param("tenantId") Long tenantId);
}
