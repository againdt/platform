package com.getinsured.affiliate.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.platform.repository.TenantAwareRepository;


@Repository
public interface PlatformAffiliateFlowRepository extends RevisionRepository<AffiliateFlow, Integer, Integer>, TenantAwareRepository<AffiliateFlow, Integer>{

	@Query("select flow from AffiliateFlow flow where flow.affiliate.affiliateId = :affiliateId")
	List<AffiliateFlow> findByAffiliateId(@Param("affiliateId") Long affiliateId);

}