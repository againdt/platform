package com.getinsured.affiliate.dto;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.affiliate.model.AffiliateFileUploadLog.ISOKTOCALL;

public class AffiliateFileUploadLogDTO implements Serializable {
	

	private static final long serialVersionUID = 1L;

	public String getFtpFilePath() {
		return ftpFilePath;
	}

	public void setFtpFilePath(String ftpFilePath) {
		this.ftpFilePath = ftpFilePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTcpaComplianceLink() {
		return tcpaComplianceLink;
	}

	public void setTcpaComplianceLink(String tcpaComplianceLink) {
		this.tcpaComplianceLink = tcpaComplianceLink;
	}

	public ISOKTOCALL getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(ISOKTOCALL isOkToCall) {
		this.isOkToCall = isOkToCall;
	}

	public Integer getSuccessfulCount() {
		return successfulCount;
	}

	public void setSuccessfulCount(Integer successfulCount) {
		this.successfulCount = successfulCount;
	}

	public Integer getDuplicateCount() {
		return duplicateCount;
	}

	public void setDuplicateCount(Integer duplicateCount) {
		this.duplicateCount = duplicateCount;
	}

	public Integer getFailedCount() {
		return failedCount;
	}

	public void setFailedCount(Integer failedCount) {
		this.failedCount = failedCount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(Long processingTime) {
		this.processingTime = processingTime;
	}

	public Integer getBatchJobId() {
		return batchJobId;
	}

	public void setBatchJobId(Integer batchJobId) {
		this.batchJobId = batchJobId;
	}

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Long getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Long affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public Long getAffiliateFileUploadLogId() {
		return affiliateFileUploadLogId;
	}

	public void setAffiliateFileUploadLogId(Long affiliateFileUploadLogId) {
		this.affiliateFileUploadLogId = affiliateFileUploadLogId;
	}

	public Integer getTotalNoOfRecords() {
		return totalNoOfRecords;
	}

	public void setTotalNoOfRecords(Integer totalNoOfRecords) {
		this.totalNoOfRecords = totalNoOfRecords;
	}

	public String getAllowSelectAction() {
		return allowSelectAction;
	}

	public void setAllowSelectAction(String allowSelectAction) {
		this.allowSelectAction = allowSelectAction;
	}

	public String getAllowResultAction() {
		return allowResultAction;
	}

	public void setAllowResultAction(String allowResultAction) {
		this.allowResultAction = allowResultAction;
	}

	public Integer getFailedMissed() {
		return failedMissed;
	}

	public void setFailedMissed(Integer failedMissed) {
		this.failedMissed = failedMissed;
	}

	public Integer getFailedOther() {
		return failedOther;
	}

	public void setFailedOther(Integer failedOther) {
		this.failedOther = failedOther;
	}

	private String ftpFilePath;
	
	private String fileName;
	
	private String companyName;
	
	private String flowName;
	
	private String status;
	
	private String tcpaComplianceLink;
	
	private ISOKTOCALL isOkToCall;
	
	private Integer successfulCount;
	
	private Integer duplicateCount;
	
	private Integer failedCount;
	
	private Integer totalNoOfRecords;
	
	private String description;
	
	private Date creationTimestamp;
	
	private Integer createdBy;
	
	private Long processingTime;
	
	private Integer batchJobId;
	
	private Long affiliateId;
	
	private Long affiliateFlowId;

	private Long affiliateFileUploadLogId;

	private String allowSelectAction;
	
	private String allowResultAction;

	private Integer failedMissed;
	
	private Integer failedOther;
}
