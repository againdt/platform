package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Affinity.
 *
 * @author Vaibhav
 */
public enum AffiliateAffinity {

	NO("No"), NON_ATL("NonATL"),YES("Yes");

	private String description;

	private AffiliateAffinity(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}