package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Call Payout.
 *
 * @author Vaibhav
 */
public enum CallPayoutType{
	FIXED_PAYOUT("Fixed Payout"), PERCENTAGE("Percentage");

	private String	description;

	private CallPayoutType(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}