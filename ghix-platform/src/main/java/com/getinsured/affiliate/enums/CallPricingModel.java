package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Call Pricing Model.
 *
 * @author Vaibhav
 */
public enum CallPricingModel{

	CP90("CP90"), CPO("CPO"),	CPRC("CPRC");

	private String	description;

	private CallPricingModel(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}