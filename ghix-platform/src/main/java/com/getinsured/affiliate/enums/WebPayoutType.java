package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Web Pricing Model.
 *
 * @author Vaibhav
 */
public enum WebPayoutType{

	FIXED("Fixed"),	PERCENTAGE("Percentage");

	private String	description;

	private WebPayoutType(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}