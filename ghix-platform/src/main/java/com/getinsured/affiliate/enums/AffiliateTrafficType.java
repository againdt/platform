package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Traffic Type.
 *
 * @author Vaibhav
 */
public enum AffiliateTrafficType{

	AFFILIATE_NETWORK("Affiliate Network"),	EMAIL_NETWORK("Email Network"),ORGANIC("Organic"), OUTSOURCING("OutSourcing"), PAID_SEARCH("Paid Search"),
	PARTNERSHIP("Partnership"), POSTED_LEADS("Posted Leads"), RADIO("Radio"),TV("TV"), VIMO_LIVE("VimoLive"), WEB_EMAIL_PUB("Web & Email Pub"),DEFAULT("");

	private String	description;

	private AffiliateTrafficType(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}