package com.getinsured.affiliate.enums;

public enum AffiliateStatus {
	
	ACTIVE("Active"), INACTIVE("Inactive"),REGISTERED("Registered"), SUSPENDED("Suspended"), MULTIPLE("Multiple") ;

	private String	description;

	private AffiliateStatus(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

}
