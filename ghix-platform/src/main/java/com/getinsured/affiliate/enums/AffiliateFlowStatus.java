package com.getinsured.affiliate.enums;

/**
 * Enum to represent Flow Status.
 *
 * @author Ekram Ali Kazi
 */
public enum AffiliateFlowStatus{

	ACTIVE("Active"), INACTIVE("Inactive"),REGISTERED("Registered"), SUSPENDED("Suspended");

	private String	description;

	private AffiliateFlowStatus(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}