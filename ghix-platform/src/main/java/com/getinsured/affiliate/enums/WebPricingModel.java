package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Web Payout.
 *
 * @author Vaibhav
 */
public enum WebPricingModel{

	CPC("CPC"),	CPL("CPL"),	CPO("CPO");

	private String	description;

	private WebPricingModel(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}