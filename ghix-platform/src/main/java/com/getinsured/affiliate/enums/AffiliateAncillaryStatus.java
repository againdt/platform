package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Ancillary status.
 *
 * @author Suresh Kancherla
 */
public enum AffiliateAncillaryStatus {

	ON("ON"), OFF("OFF");

	private String description;

	private AffiliateAncillaryStatus(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}