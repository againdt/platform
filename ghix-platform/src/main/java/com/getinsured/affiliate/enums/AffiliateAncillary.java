package com.getinsured.affiliate.enums;

/**
 * Enum to represent Affiliate Ancillary.
 *
 * @author Suresh Kancherla
 */
public enum AffiliateAncillary {

	STM("STM"), AME("AME"),DENTAL("DENTAL"),VISION("VISION"), HEALTH("HLT");

	private String description;

	private AffiliateAncillary(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}