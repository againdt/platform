package com.getinsured.integration;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.httpclient.api.HttpStatus;
import com.getinsured.hix.platform.config.FileBasedConfiguration;
import com.getinsured.hix.platform.util.SecureHttpClient;

@Service
public class FDSHIntegrationHealth {
	@Autowired
	private EntityManagerFactory emf;
	private CloseableHttpClient httpClient = SecureHttpClient.getHttpClient();
	private Query lockQuery;
	private Query unLockQuery;
	boolean healthCheckEnabled = true;
	private String healthCheckUrl= null;
	private Boolean clusterHealthCheckEnabled;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private Timer timer = null;
	
	
	
	@PostConstruct
	public void startHealthCheck() {
		clusterHealthCheckEnabled = FileBasedConfiguration.getConfiguration().getBoolean("fdh.hubClusterHealthCheckEnabled", false);
		if(!clusterHealthCheckEnabled) {
			logger.info("Health check not enabled for HUB integration");
			return;
		}
		String url = FileBasedConfiguration.getConfiguration().getProperty("ghixHubIntegrationURL");
		if(url == null) {
			this.healthCheckEnabled = false;
		}else {
			if(!url.endsWith("/")) {
				url +="/getClusterStatus";
			}else {
				url += "getClusterStatus";
			}
			this.healthCheckUrl = url;
		}
		if(timer == null) {
			timer = new Timer();
			timer.schedule(new HealthCheckHook(), 60000,60000); // every min
		}
	}
	@PreDestroy
	public void cleanup() {
		if(timer != null) {
			logger.info("Shutting down the HUB Cluster health check");
			timer.cancel();
		}
	}
	
	private class HealthCheckHook extends TimerTask {
		private static final String PATTERN = "dd-MM-yyyy hh:mm:ss";
		private Logger logger = LoggerFactory.getLogger(getClass());
		private JSONParser parser = new JSONParser();
		@Override
		public void run() {
			long now = System.currentTimeMillis();
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			HttpGet healthGet = null;
			try {
				lockQuery = em.createNativeQuery("UPDATE GI_WS_PAYLOAD "
						+ "SET "
						+ 	"status='LOCKED' "
						+ "WHERE "
						+ 	"status = 'UNLOCKED' "
						+ "AND "
						+ 	"endpoint_function='HUB' "
						+ "AND "
						+ 	"endpoint_operation_name='HEALTH'"
						+ "AND "
						+ 	"(date_part('epoch', now()) - date_part('epoch',created_timestamp) > 1)");
				unLockQuery = em.createNativeQuery("UPDATE GI_WS_PAYLOAD "
						+ "SET "
						+ 	"status='UNLOCKED', "
						+	"created_timestamp = now() "
						+ "WHERE "
						+ 	"status = 'LOCKED' "
						+ "AND "
						+ 	"endpoint_function='HUB' "
						+ "AND "
						+ 	"endpoint_operation_name='HEALTH'");
				logger.info("Trying to acquire the cluster lock");
				int locked = lockQuery.executeUpdate();
				em.getTransaction().commit();
				if(locked == 1) {
					logger.info("Acquired lock for checking the cluster health");
					healthGet = new HttpGet(healthCheckUrl);
					CloseableHttpResponse response = httpClient.execute(healthGet);
					int code = response.getStatusLine().getStatusCode();
					if(code == HttpStatus.OK.code) {
						InputStream is = response.getEntity().getContent();
						try {
							JSONObject cluster = (JSONObject) parser.parse(new InputStreamReader(is));
							JSONArray array = (JSONArray) cluster.get("cluster");
							printClusterHealth(array);
						} catch (ParseException e) {
							logger.error("Error extracting the node information",e);
						}
							
					}else {
						logger.error("Received {} code for health check", code);
						InputStream is = response.getEntity().getContent();
						BufferedReader br = new BufferedReader(new InputStreamReader(is));
						String str = null;
						StringBuilder sb = new StringBuilder();
						while((str = br.readLine()) != null) {
							sb.append(str);
						}
						logger.error(sb.toString());
						if(code == 502) {
							logger.error("HUB Integration point seems to be down, please check the Cluster health manually");
						}
					}
					response.close();
					em.getTransaction().begin();
					int unlocked = unLockQuery.executeUpdate();
					if(unlocked != 1) {
						logger.error("Help.. failed to unlock the record, manual intervention required");
					}else {
						logger.info("Released lock for checking the cluster health");
					}
				}else {
					logger.info("Failed to acquire the lock record, not initiating AWS health check");
				}
			}catch (Exception e) {
				logger.error("Error communicating with cluster",e);
			} finally {
				if(em.getTransaction().isActive()) {
					em.getTransaction().commit();
				}
				em.close();
				healthGet.releaseConnection();
			}
			logger.info("Cluster health check took {} ms", (System.currentTimeMillis()- now));
		}
		
		private String getRedableDuration(long millis) {
			long days = TimeUnit.MILLISECONDS.toDays(millis);
	        millis -= TimeUnit.DAYS.toMillis(days);
	        
	        long hours = TimeUnit.MILLISECONDS.toHours(millis);
	        millis -= TimeUnit.HOURS.toMillis(hours);
	        
	        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
	        millis -= TimeUnit.MINUTES.toMillis(minutes);
	        
	        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

	        StringBuilder sb = new StringBuilder(64);
	        sb.append(days);
	        sb.append(" Days ");
	        sb.append(hours);
	        sb.append(" Hours ");
	        sb.append(minutes);
	        sb.append(" Minutes ");
	        sb.append(seconds);
	        sb.append(" Seconds");
	        sb.trimToSize();
	        return sb.toString();
		}
		
		private void printClusterHealth(JSONArray array) {
			SimpleDateFormat sdf = new SimpleDateFormat(PATTERN);
			int len = array.size();
			logger.info("Received information on {} nodes", len);
			JSONObject node = null;
			Date clusterTime = null;
			Date lastHeartBeat = null;
			String nodeIdentifier = null;
			int suspect = 0;
			for(int i = 0; i < len; i++) {
				try {
					node = (JSONObject) array.get(i);
					String tmp = (String)node.get("currentSystemTime");
					if(tmp == null) {
						logger.info("Cluster local time not available, can not determine if the odeis alive");
					}else {
						clusterTime = sdf.parse(tmp);
					}
					tmp = (String)node.get("hostName");
					if(tmp != null) {
						nodeIdentifier = tmp;
					}
					tmp = (String)node.get("lastHeartBeat");
					if(tmp != null) {
						lastHeartBeat = sdf.parse(tmp);
						long diff = clusterTime.getTime()- lastHeartBeat.getTime();
						if(diff > 30*1000) {
							suspect++;
							logger.error("Node:{} appears to be down, Cluster time {} with Last heart beat registered at {} , {} ago",
									nodeIdentifier, sdf.format(clusterTime), sdf.format(lastHeartBeat), getRedableDuration(diff));
						}
					}
					float totalMemory = (Long)node.get("totalMemory");
					float freeMemory = (Long)node.get("availableMemory");
					float freeMemoryPercetage = (freeMemory/totalMemory)*100;
					if(freeMemoryPercetage < 30.0 && freeMemoryPercetage > 20.0) {
						suspect++;
						logger.warn("node {} running low on memory with {}%, [{}/{}] at {}", nodeIdentifier,freeMemoryPercetage, freeMemory, totalMemory,sdf.format(clusterTime));
					}
					if(freeMemoryPercetage < 20.0) {
						suspect++;
						logger.error("node {} running low on memory with {}%, [{}/{}] at {}", nodeIdentifier,freeMemoryPercetage, freeMemory, totalMemory, sdf.format(clusterTime));
					}
				}catch(Exception e) {
					logger.error("Error parsing the node information received {}",array,e);
				}
				
			}
			logger.info("Received 2 nodes, bad health suspected markers count {}, check errors and warning for details", suspect);
		}
	}
}
