package com.getinsured;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;

import com.getinsured.hix.filter.xss.CrossScriptingFilterTest;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantFilterTest;
import com.getinsured.hix.platform.security.session.SessionTrackerServiceImplTest;
import com.getinsured.hix.platform.util.GhixAESCipherPoolTest;
import com.getinsured.hix.platform.util.GhixUtilsTest;
import com.getinsured.hix.ws_trust.saml.GhixSamlUserDetailsServiceTest;
import com.getinsured.platform.couchbase.CouchbaseMultiNodeSpringTest;
import com.getinsured.platform.couchbase.CouchbaseRawTest;
import com.getinsured.platform.couchbase.CouchbaseSpringTest;
import com.getinsured.platform.util.HIXHTTPClientTest;
import com.getinsured.platform.util.Wso2XmlUtilTest;
import com.getinsured.platform.util.security.service.UserServiceTest;

/**
 * Created by golubenko_y on 1/17/17.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    UserServiceTest.class,
    HIXHTTPClientTest.class,
    CrossScriptingFilterTest.class,
    TenantFilterTest.class,
    SessionTrackerServiceImplTest.class,
    Wso2XmlUtilTest.class,
    GhixSamlUserDetailsServiceTest.class,
    CouchbaseMultiNodeSpringTest.class,
    CouchbaseRawTest.class,
    CouchbaseSpringTest.class,
    GhixUtilsTest.class,
    GhixAESCipherPoolTest.class
})
//@Ignore("This is test suite declaration, doesn't contain any tests.")
public class PlatformTestSuite
{
  public static void main(String[] args)
  {
    Result result = JUnitCore.runClasses(PlatformTestSuite.class);
    for(Failure fail : result.getFailures())
    {
      System.out.println(fail.toString());
    }

    if(result.wasSuccessful())
    {
      System.out.println("All tests finished successfully.");
    }
  }
}
