package com.getinsured.junit;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * A class that "blitzes" an object by calling it many times, from
 * multiple threads.  Used for stress-testing synchronisation.
 *
 * @author nat https://github.com/npryce/goos-code-examples/
 */
public class MultiThreadedStressTester
{
  private static final Logger log = LoggerFactory.getLogger(MultiThreadedStressTester.class);
  /**
   * The default number of threads to run concurrently.
   */
  public static final int DEFAULT_THREAD_COUNT = 2;

  private final ExecutorService executor;
  private final int threadCount;
  private final int iterationCount;


  public MultiThreadedStressTester(int iterationCount)
  {
    this(DEFAULT_THREAD_COUNT, iterationCount);
  }

  public MultiThreadedStressTester(int threadCount, int iterationCount)
  {
    this.threadCount = threadCount;
    this.iterationCount = iterationCount;
    this.executor = Executors.newCachedThreadPool();
  }

  public MultiThreadedStressTester(int threadCount, int iterationCount, ThreadFactory threadFactory)
  {
    this.threadCount = threadCount;
    this.iterationCount = iterationCount;
    this.executor = Executors.newCachedThreadPool(threadFactory);
  }

  public void stress(final Runnable action) throws InterruptedException
  {
    spawnThreads(action).await();
  }

  public void blitz(long timeoutMs, final Runnable action) throws InterruptedException, TimeoutException
  {
    if (!spawnThreads(action).await(timeoutMs, MILLISECONDS))
    {
      throw new TimeoutException("timed out waiting for blitzed actions to complete successfully");
    }
  }

  private CountDownLatch spawnThreads(final Runnable action)
  {
    final CountDownLatch finished = new CountDownLatch(threadCount);

    for (int i = 0; i < threadCount; i++)
    {
      int finalI = i;
      executor.execute(() -> {
        try
        {
          repeat(action, threadCount, finalI);
        } finally
        {
          finished.countDown();
        }
      });
    }
    return finished;
  }

  private void repeat(Runnable action, int threadCount, int threadNum)
  {
    for (int i = 0; i < iterationCount; i++)
    {
      log.info("Starting: {}/{} thread; iteration: #{}", threadNum, threadCount, (i+1));
      action.run();
    }
  }

  public void shutdown()
  {
    executor.shutdown();
  }
}