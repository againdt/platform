package com.getinsured.junit;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.getinsured.platform.config.bootstrap.AddressValidationConfiguration;
import com.getinsured.platform.config.bootstrap.ComponentScanConfiguration;
import com.getinsured.platform.config.bootstrap.EncryptionConfiguration;
import com.getinsured.platform.config.bootstrap.JpaScanConfiguration;
import com.getinsured.platform.config.bootstrap.MailConfiguration;
import com.getinsured.platform.config.bootstrap.PropertiesConfiguration;
import com.getinsured.platform.config.bootstrap.SMSConfiguration;
import com.getinsured.platform.config.bootstrap.WebHTTPConfiguration;

/**
 * Created by golubenko_y on 1/13/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
    JpaTestConfig.class,
    ComponentScanConfiguration.class,
    JpaScanConfiguration.class,
    EncryptionConfiguration.class,
    MailConfiguration.class,
    SMSConfiguration.class,
    AddressValidationConfiguration.class,
    PropertiesConfiguration.class,
    WebHTTPConfiguration.class
})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
@Ignore("This is a parent class for tests, no tests are defined here.")
public class FunctionalTest
{
  private static final Map<String, String> usersGhixHome = new HashMap<>();
  private static final Logger log = LoggerFactory.getLogger(FunctionalTest.class);

  // Add your GHIX_HOME if when running tests GHIX_HOME is not present
  static
  {
    usersGhixHome.put("golubenko_y", "/Users/golubenko_y/projects/branches/MAIN_NEW/phix");
    usersGhixHome.put("jeneag", "/home/jeneag/DEVELOPMENT/getinsured/branches/MAIN/ghix");
  }

  @BeforeClass
  public static void setGhixHome()
  {
    // slf4j configuration for JUnit tests.
//    System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "INFO");
//    System.setProperty("org.slf4j.simpleLogger.showDateTime", "true");

    // Global configuration parameters independent of each environment
    System.setProperty("GHIX_SECNTX_TYPE", "db");
    System.setProperty("PASSWORD_KEY", "ghix123");

    String ghixHome = System.getProperty("GHIX_HOME") != null && !"".equals(System.getProperty("GHIX_HOME")) ? System.getProperty("GHIX_HOME") : System.getenv("GHIX_HOME");
    String userName = System.getProperty("user.name");
    log.info("GHIX_HOME currently set to: {}", ghixHome);

    if((ghixHome == null || "".equals(ghixHome)) && usersGhixHome.containsKey(userName))
    {
      log.info("Found mapping of GHIX_HOME for current user: {} => {}", userName, usersGhixHome.get(userName));
      System.setProperty("GHIX_HOME", usersGhixHome.get(System.getProperty("user.name")));
    }

    if("".equals(System.getProperty("GHIX_HOME")))
    {
      log.info("Environment variable GHIX_HOME is not set and current user '{}' doesn't have any mappings defined in {}, JUnit tests will may fail.",
          System.getProperty("user.name"), FunctionalTest.class.getName());
    }
  }

/*
 * For reference:
 *
 *
-Dorg.owasp.esapi.resources=/Users/golubenko_y/projects/branches/MAIN/ghix/ghix-setup/esapi
-DGHIX_HOME=/Users/golubenko_y/projects/branches/MAIN_NEW/phix
-DGHIX_SECNTX_TYPE=db
-DD2C_HOME=/Users/golubenko_y/projects/branches/MAIN_NEW/phix
-DPASSWORD_KEY=ghix123
-Dtenant.enabled=on
-Dinit.type=java
-Dredis.enabled=on
-Dspring.session=on
-Dcache.enabled=couch
-Dsolr.velocity.enabled=false
-Duser.language=en
-Duser.country=US
-ea
-Dea
*/
}
