package com.getinsured.junit.mock;

import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.model.TenantDTO;

/**
 * Created by golubenko_y on 1/18/17.
 */
public class TenantDTOMock
{
  /**
   * Returns {@link TenantDTO} mocked object.
   *
   * @return TenantDTO object.
   */
  public static TenantDTO getTenantDTO()
  {
    TenantDTO tenant = new TenantDTO();
    tenant.setCode("JUNIT");
    tenant.setConfiguration(new ConfigurationDTO());
    tenant.setId(1L);
    tenant.setIsActive("Y");
    tenant.setName("Junit Test Tenant Name");
    tenant.setTestTenant("N");
    tenant.setUrl("www.getinsured.com");

    return tenant;
  }

  /**
   * Returns {@link TenantDTO} with specified url.
   *
   * @param url tenant url to pre-set.
   * @return TenantDTO object.
   */
  public static TenantDTO getTenantDTO(final String url)
  {
    TenantDTO tenant = getTenantDTO();
    tenant.setUrl(url);

    return tenant;
  }

  /**
   * Returns {@link TenantDTO} with specified url and code.
   *
   * @param url tenant url
   * @param code code for this tenant
   * @return TenantDTO object.
   */
  public static TenantDTO getTenantDTO(final String url, final String code)
  {
    TenantDTO tenant = getTenantDTO();
    tenant.setUrl(url);
    tenant.setCode(code);

    return tenant;
  }
}
