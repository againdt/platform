package com.getinsured.junit.mock;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Permission;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RolePermission;
import com.getinsured.hix.model.UserRole;

/**
 * Created by jeneag on 1/13/17.
 */
@Ignore("AccountUserMock doesn't contain any JUnit tests")
public class AccountUserMock
{
  /**
   * Returns sample {@link AccountUser} object.
   *
   * @return sample AccountUser object.
   */
  public static AccountUser getAccountUser()
  {
    AccountUser testUser = new AccountUser();
    testUser.setEmail("test@junit.org");
    testUser.setAuthenticated(false);

    ModuleUser moduleUser = new ModuleUser();
    moduleUser.setCreated(new TSDate());
    moduleUser.setId(12345);
    moduleUser.setModuleId(1);
    moduleUser.setModuleName("MODULE_NAME");
    moduleUser.setUpdated(new TSDate());

    testUser.setActiveModule(moduleUser);
    testUser.setUpdated(new TSDate());
    testUser.setAuthenticated(true);
    testUser.setId(123);
    testUser.setActiveModuleId(1);
    testUser.setActiveModuleName("MODULE_NAME");
    testUser.setCommunicationPref("EMAIL");
    testUser.setConfirmed(1);
    testUser.setCreated(new TSDate());
    testUser.setCurrentUserRole("ROLE_ADMIN");

    Role role = new Role();
    role.setCreated(new TSDate());
    role.setAccountIdlePeriod(3600);
    role.setDescription("Role Description");
    role.setId(1);
    role.setLandingPage("/landing");
    role.setLoginRestricted(0);
    role.setMaxRetryCount(3);
    role.setMfaEnabled("false");
    role.setLabel("Role Label");
    role.setName("ROLE_ADMIN");
    role.setPasswordExpiryPeriod(360);
    role.setPostRegistrationUrl("/postregurl");
    role.setPrivileged(1);

    Set<RolePermission> rolePermissions = new HashSet<>();

    RolePermission perm = new RolePermission();
    perm.setCreated(new TSDate());
    perm.setId(1);

    Permission permission = new Permission();
    permission.setId(1);
    permission.setDescription("JUnit Permission Description");
    permission.setIsDefault(Permission.DefaultFlag.Y);
    permission.setName("Junit Permission name");
    perm.setPermission(permission);
    rolePermissions.add(perm);

    role.setRolePermissions(rolePermissions);
    role.setPwdConstraintsEnabled('N');
    testUser.setDefRole(role);
    testUser.setFirstName("Spring");
    testUser.setLastName("JUnit");
    testUser.setPassword("ghix123#");
    testUser.setPhone("180012345678");
    testUser.setPin("1234");
    testUser.setRetryCount(3);
    testUser.setSecQueRetryCount(3);
    testUser.setSecurityAnswer1("answer1");
    testUser.setSecurityAnswer2("answer2");
    testUser.setSecurityAnswer3("answer3");
    testUser.setSecurityQuestion1("question1");
    testUser.setSecurityQuestion2("question2");
    testUser.setSecurityQuestion3("question3");
    testUser.setStartDate(new TSDate());
    testUser.setStatus("ACTIVE");
    testUser.setTenantId(1L);
    testUser.setTitle("User Title");
    testUser.setUserName("test@junit.org");
    testUser.setUuid("UUID1234567890");

    UserRole userRole = new UserRole();
    userRole.setCreated(new TSDate());
    userRole.setId(1);
    userRole.setIsActive('Y');
    userRole.setRole(role);
    Set<UserRole> userRoles = new HashSet<>();
    userRoles.add(userRole);

    testUser.setUserRole(userRoles);

    return testUser;
  }
}
