package com.getinsured.junit.mock;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.affiliate.model.AffiliateFlow;
import com.getinsured.hix.model.ConfigurationDTO;

/**
 * Created by golubenko_y on 1/18/17.
 */
public class AffiliateFlowMock
{
  public static AffiliateFlow getAffiliateFlow()
  {
    AffiliateFlow flow = new AffiliateFlow();
    flow.setAffiliateflowId(1);
    flow.setTenantId(TenantDTOMock.getTenantDTO().getId());
    flow.setFlowName("Junit Flow Name");
    flow.setAffiliateFlowConfig(new ConfigurationDTO());
    flow.setAffiliate(AffiliateMock.getAffiliate());

    return flow;
  }

  public static AffiliateFlow getAffiliateFlow(String url)
  {
    AffiliateFlow flow = getAffiliateFlow();
    flow.setUrl(url);
    flow.setAffiliate(AffiliateMock.getAffiliate(url));

    return flow;
  }
}
