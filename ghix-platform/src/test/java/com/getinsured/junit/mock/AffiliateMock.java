package com.getinsured.junit.mock;

import com.getinsured.affiliate.enums.AffiliateStatus;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.model.ActiveFlag;
import com.getinsured.hix.model.ConfigurationDTO;
import com.getinsured.hix.platform.multitenant.resolver.Tenant;

/**
 * Created by golubenko_y on 1/18/17.
 */
public class AffiliateMock
{
  public static Affiliate getAffiliate()
  {
    Affiliate a = new Affiliate();
    a.setTenantId(TenantDTOMock.getTenantDTO().getId());
    a.setAffiliateConfig(new ConfigurationDTO());
    a.setAffiliateId(1L);
    a.setAffiliateStatus(AffiliateStatus.ACTIVE.name());
    a.setCompanyName("Getinsured Junit Company");
    a.setEmail("junit@getinsured.com");
    a.setPhone("8002661974");
    a.setWebsite("http://junit.getinsured.com");
    a.setApiKey("apikey123#");
    a.setHasEmployer(ActiveFlag.Y);

    return a;
  }

  public static Affiliate getAffiliate(String url)
  {
    Affiliate a = getAffiliate();

    a.setUrl(url);
    a.setTenantId(TenantDTOMock.getTenantDTO(url).getId());

    return a;
  }
}
