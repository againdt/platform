package com.getinsured.junit.mock;

import com.getinsured.hix.model.UserSession;
import com.getinsured.hix.model.UserSessionStatus;

/**
 * Created by golubenko_y on 1/23/17.
 */
public class UserSessionMock
{
  public static UserSession getUserSession()
  {
    UserSession us = new UserSession();
    us.setBrowserIp("24.4.116.5");
    us.setId(12345L);
    us.setLoginTime();
    us.setNodeId("1");
    us.setSessionId("session_id-" + us.getId());
    us.setSessionVal("session_val" + us.getId());
    us.setStatus(UserSessionStatus.INIT.toString());
   // us.setUserId(1L);

    return us;
  }

  public static UserSession getUserSession(final Long id)
  {
    UserSession us = getUserSession();
    us.setId(id);
    us.setSessionId("session_id-" + id);
    us.setSessionVal("session_val-" + id);

    return us;
  }

  public static UserSession getUserSession(final UserSessionStatus status)
  {
    UserSession us = getUserSession();
    us.setStatus(status.toString());

    return us;
  }
}
