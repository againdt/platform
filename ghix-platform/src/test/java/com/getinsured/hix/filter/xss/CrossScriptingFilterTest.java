package com.getinsured.hix.filter.xss;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.getinsured.hix.platform.security.tokens.CSRFTokenTag;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

import static org.mockito.Mockito.when;

/**
 * Created by golubenko_y on 1/17/17.
 */
public class CrossScriptingFilterTest
{
  private static final Logger log = LoggerFactory.getLogger(CrossScriptingFilterTest.class);

  @Mock
  private HttpServletRequest request;
  @Mock
  private HttpServletResponse response;
  @Mock
  private HttpSession session;
  @Mock
  private FilterChain filterChain;
  @Mock
  private FilterConfig filterConfig;
  @Mock
  private WebApplicationContext webApplicationContext;
  @Mock
  private ServletContext servletContext;

  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);

    // Some common rules applicable for most tests
    when(filterConfig.getServletContext()).thenReturn(servletContext);
    when(request.getAttribute(DispatcherServlet.WEB_APPLICATION_CONTEXT_ATTRIBUTE)).thenReturn(webApplicationContext);
    when(webApplicationContext.getBean("ghixJasyptEncryptorUtil")).thenReturn(new GhixJasyptEncrytorUtil());
  }

  @Test
  public void testGETRequestWithoutCSRFToken() throws Exception
  {
    log.info("Testing GET without CSRF Token");

    StringBuffer requestURL = new StringBuffer("/somepage.jsp");

    when(request.getRequestURL()).thenReturn(requestURL);
    when(request.getMethod()).thenReturn("GET");
    when(request.getSession()).thenReturn(session);

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);

    filter.doFilter(request, response, filterChain);

    log.info("XSS Filter completed successfully for GET Request Without CSRF Token");
  }


  @Test(expected = ServletException.class)
  public void testGETRequestWithCSRFToken() throws Exception
  {
    log.info("Testing GET with invalid CSRF Token parameter");

    StringBuffer requestURL = new StringBuffer("/somepage.jsp");

    when(request.getRequestURL()).thenReturn(requestURL);
    when(request.getMethod()).thenReturn("GET");
    when(request.getSession()).thenReturn(session);
    when(request.getParameter(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn("mycsrftoken-that-shouldnt-be-here");

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);

    filter.doFilter(request, response, filterChain);
  }

  @Test
  public void testPOSTWithInvalidCSRFToken() throws Exception
  {
    log.info("Testing POST with invalid CSRF Token parameter");

    // When we ask for token parameter, return something invalid
    when(request.getParameter(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn("someinvalidtoken");
    when(request.getMethod()).thenReturn("POST");
    when(request.getSession()).thenReturn(session);
    when(request.getRequestURL()).thenReturn(new StringBuffer("/somepage.jsp"));

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);

    filter.doFilter(request, response, filterChain);
  }

  @Test
  public void testPOSTWithMismatchedParamAndSessionTokens() throws Exception
  {
    log.info("Testing POST with mismatched tokens in session and parameter.");

    // Place some token in session that will differ from CSRF parameter
    when(session.getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn("someTokenInSession");

    // Place something for CSRF parameter that is different from the session attribute
    when(request.getParameter(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn("someinvalidtoken");

    when(request.getMethod()).thenReturn("POST");
    when(request.getSession()).thenReturn(session);
    when(request.getRequestURL()).thenReturn(new StringBuffer("/somepage.jsp"));

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);

    filter.doFilter(request, response, filterChain);
  }

  /**
   * Since we are ignoring CSRF token when certain path is in request, test it.
   *
   * @throws Exception if exception happened.
   */
  @Test
  public void testPOSTForIgnoredSAMLPath() throws Exception
  {
    log.info("Testing POST with invalid tokens for ignored path");

    // Place mismatched tokens in the session and in parameters
    when(session.getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn("someTokenInSession");
    when(request.getParameter(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn("someinvalidtoken");

    // But set URL to contain some path that will ignore invalid tokens
    when(request.getRequestURL()).thenReturn(new StringBuffer("/saml/user/account"));

    when(request.getMethod()).thenReturn("POST");
    when(request.getSession()).thenReturn(session);

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);
    filter.doFilter(request, response, filterChain);

    log.info("XSS Filter ignored /saml/user/account path with invalid CSRF tokens: Test Passed");
  }

  /**
   * Testing with custom Header name for CSRF token.
   *
   * @throws Exception if exception happened.
   */
  @Test
  public void testPOSTWithValidSessionAndHeaderCSRFToken() throws Exception
  {
    log.info("Testing POST valid CSRF Token in Session and custom 'csrftoken' Header");

    String validToken = "validtoken";

    // Place some token in session
    when(session.getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn(validToken);
    // Place something for CSRF header
    when(request.getHeader(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn(validToken);

    when(request.getMethod()).thenReturn("POST");
    when(request.getSession()).thenReturn(session);
    when(request.getRequestURL()).thenReturn(new StringBuffer("/somepage.jsp"));

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);

    filter.doFilter(request, response, filterChain);
  }

  /**
   * Testing standard Header name for CSRF token.
   *
   * @throws Exception if exception happened.
   */
  @Test
  public void testPOSTWithValidSessionAndStandardHeaderCSRFToken() throws Exception
  {
    log.info("Testing POST valid CSRF Token in Session and Standard 'X-CSRF-TOKEN' Header");

    String validToken = "validtoken";

    // Place some token in session
    when(session.getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME)).thenReturn(validToken);
    // Place something for standard CSRF header
    when(request.getHeader("X-CSRF-TOKEN")).thenReturn(validToken);

    when(request.getMethod()).thenReturn("POST");
    when(request.getSession()).thenReturn(session);
    when(request.getRequestURL()).thenReturn(new StringBuffer("/somepage.jsp"));

    CrossScriptingFilter filter = new CrossScriptingFilter();
    filter.init(filterConfig);

    filter.doFilter(request, response, filterChain);
  }
}