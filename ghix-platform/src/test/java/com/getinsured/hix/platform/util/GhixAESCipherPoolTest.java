package com.getinsured.hix.platform.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * Test for encryption/decryption.
 *
 * @author Yevgen Golubenko
 * @since 6/28/18
 */
public class GhixAESCipherPoolTest
{
  private static final Logger log = LoggerFactory.getLogger(GhixAESCipherPool.class);

  @Test
  public void encryptDecrypt() throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException
  {
    String password = "password";
    String encrypted = GhixAESCipherPool.encrypt(password);
    log.warn("Encrypted '{}' => '{}'", password, encrypted);
    Assert.assertNotNull(encrypted);

    String decrypted = GhixAESCipherPool.decrypt(encrypted);
    log.warn("Decrypted: '{}' => '{}'", encrypted, decrypted);
    Assert.assertNotNull(decrypted);
    Assert.assertEquals(password, decrypted);
  }
}
