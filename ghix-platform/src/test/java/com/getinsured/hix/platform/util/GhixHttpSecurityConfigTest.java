package com.getinsured.hix.platform.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for {@link GhixHttpSecurityConfig}
 *
 * @author Yevgen Golubenko
 * @since 8/31/17
 */
public class GhixHttpSecurityConfigTest
{
  @Test
  public void testProtectedUrls() throws Exception
  {
    String[] secureUrls = new String[] {
        "https://idssodev.ghixqa.com/hix/account/user/switchUserRole",
        "/admin/planmgmt/managevisionplans",
        "https://www.getinsured.com/cap/consumer/view/1",
        "/admin/planmgmt/editqdprates/abc1223encodedPlanId",
        "/hix/somenew/page/not/added/to/unsecure/list",
        "somenew/page/not/added/to/unsecure/list"
    };

    GhixHttpSecurityConfig ghixHttpSecurityConfig = new GhixHttpSecurityConfig();

    for(String u : secureUrls)
    {
      Assert.assertTrue("URL should be secure: " + u, ghixHttpSecurityConfig.isUrlProtected(u));
    }
  }

  @Test
  public void testUnprotectedUrls() throws Exception
  {
    GhixHttpSecurityConfig ghixHttpSecurityConfig = new GhixHttpSecurityConfig();

    String[] unsecureUrls = new String[] {
        "https://www.getinsured.com/validateZip",
        "http://mercer.getinsured.com/j_spring_security_check",
        "https://phix3dev.ghixqa.com/hix/actuator/info",
        "https://phix8qa.ghixqa.com/actuator/info",
        "http://phix5dev.ghixqa.com/ancillaryquote/vision",
        "https://phix8qa.ghixqa.com/private/planselection?flow=vision&exchangeType=OFF",
        "/memberportal/universalCart",
        "/iex/validateZip",
        "/validateZipCode",
        "/webapi/someapi/someMethod?param1=value1",
        "/"
    };

    for(String u : unsecureUrls)
    {
      Assert.assertFalse("URL should NOT be secure: " + u, ghixHttpSecurityConfig.isUrlProtected(u));
    }
  }
}