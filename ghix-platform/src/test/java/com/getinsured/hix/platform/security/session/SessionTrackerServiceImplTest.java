package com.getinsured.hix.platform.security.session;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.model.UserSession;
import com.getinsured.hix.model.UserSessionStatus;
import com.getinsured.hix.platform.security.repository.IUserSessionRepository;
import com.getinsured.junit.MultiThreadedStressTester;
import com.getinsured.junit.mock.UserSessionMock;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

/**
 * Created by golubenko_y on 1/23/17.
 */
public class SessionTrackerServiceImplTest
{
  private static final Logger log = LoggerFactory.getLogger(SessionTrackerServiceImplTest.class);

  @Mock
  private IUserSessionRepository userSessionRepository;

  @Mock
  private HttpServletRequest request;

  @Mock
  private HttpSession session;

  @InjectMocks
  private SessionTrackerService trackerService = new SessionTrackerServiceImpl();


  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);

    // When we are saving, mimick db operation by invoking @PrePresist and setting some stuff.
    when(userSessionRepository.saveAndFlush(any(UserSession.class))).thenAnswer(invocation ->
    {
      UserSession us = (UserSession) invocation.getArguments()[0];
      Random random = new Random();
      Long id = random.nextLong();
      if (id < 0)
      {
        id = -id;
      }

      us.setId(id);

      // Invoke @PrePresist manually
      us.setLoginTime();

      InetAddress addr = InetAddress.getLocalHost();
      us.setNodeId(addr.getCanonicalHostName());

      return us;
    });

    when(session.getId()).thenAnswer(invocation -> UUID.randomUUID().toString());
  }

  @After
  public void tearDown() throws Exception
  {

  }

  @Test
  public void createUserSession() throws Exception
  {
    when(request.getRemoteAddr()).thenReturn("10.10.0.1");
    ServletRequestAttributes attributes = new ServletRequestAttributes(request);
    RequestContextHolder.setRequestAttributes(attributes);

    boolean created = trackerService.createUserSession(session, "192.168.0.1", 2L, false);
    Assert.assertTrue("User session creation was not successfull", created);
  }

  @Test
  public void createUserSessionWithXForwardedForHeader() throws Exception
  {
    when(request.getHeader("X-FORWARDED-FOR")).thenReturn(randomIp()); // Original Client IP
    when(request.getRemoteAddr()).thenReturn("192.168.0.1"); // IP of the proxy server

    ServletRequestAttributes attributes = new ServletRequestAttributes(request);
    RequestContextHolder.setRequestAttributes(attributes);

    boolean created = trackerService.createUserSession(session, "192.168.0.1", 3L, false);
    Assert.assertTrue("User session creation was not successfull", created);
  }

  @Test
  public void createManySessions() throws Exception
  {
    Random random = new Random();

    for (int i = 0; i < 10; i++)
    {
      when(request.getHeader("X-FORWARDED-FOR")).thenReturn(randomIp()); // Randomize Client IPs
      ServletRequestAttributes attributes = new ServletRequestAttributes(request);
      RequestContextHolder.setRequestAttributes(attributes);
      Long userId = random.nextLong();
      if (userId < 0) { userId = -userId; }
      boolean created = trackerService.createUserSession(session, randomIp(), userId, random.nextInt() % 2 == 0);
      Assert.assertTrue("User session creation was not successfull for iteration: " + i, created);
    }
  }

  @Test
  public void createSessionsMultiThreaded() throws Exception
  {
    MultiThreadedStressTester stressTester = new MultiThreadedStressTester(5, 1);
    Random random = new Random();

    stressTester.stress(() ->
    {
      when(request.getHeader("X-FORWARDED-FOR")).thenReturn(randomIp()); // Randomize Client IPs
      ServletRequestAttributes attributes = new ServletRequestAttributes(request);
      RequestContextHolder.setRequestAttributes(attributes);
      Long userId = random.nextLong();
      if (userId < 0) { userId = -userId; }

      boolean created = trackerService.createUserSession(session, randomIp(), userId, random.nextInt() % 2 == 0);

      log.info("Thread: {}; created: {}", Thread.currentThread().getName(), created);

      Assert.assertTrue("User session creation was not successfull for thread: " + Thread.currentThread().getName(), created);
    });

    stressTester.shutdown();
  }

  @Test
  public void fetchUserSession()
  {
    when(userSessionRepository.findRegisteredUserSession(anyInt())).thenAnswer(invocation ->
    {

      Long userId = (Long) invocation.getArguments()[0];
      List<UserSession> sessions = new ArrayList<>();

      // Only if userId == 1, we return something from DB, otherwise nothing.
      if (userId == 1L)
      {
        UserSession session = UserSessionMock.getUserSession(UserSessionStatus.INIT);
        sessions.add(session);
      }

      return sessions;
    });

    UserSession userSession = trackerService.fetchUserSession(1);
    UserSession nullSession = trackerService.fetchUserSession(2); // This should be null

    Assert.assertNotNull("Didn't get user session for userId 1", userSession);
    Assert.assertNull("Got user session which should be null", nullSession);
  }

  private static int randomIpPart()
  {
    Random r = new Random();
    int low = 0;
    int high = 255;
    return r.nextInt(high - low) + low;
  }

  private static String randomIp()
  {
    return randomIpPart() + "." + randomIpPart() + "." + randomIpPart() + "." + randomIpPart();
  }
}