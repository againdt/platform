package com.getinsured.hix.platform.multitenant.resolver.filter;

import java.util.Base64;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.affiliate.service.AffiliateFlowConfigService;
import com.getinsured.hix.platform.multitenant.resolver.exception.TenantRuntimeException;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.junit.mock.AffiliateFlowMock;
import com.getinsured.junit.mock.AffiliateMock;
import com.getinsured.junit.mock.TenantDTOMock;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link TenantFilter}.
 *
 * Created by golubenko_y on 1/18/17.
 */
public class TenantFilterTest
{
  private static final Logger log = LoggerFactory.getLogger(TenantFilterTest.class);

  @Mock
  private TenantService tenantService;

  @Mock
  private AffiliateFlowConfigService affiliateFlowConfigService;

  @Mock
  private GIExceptionHandler giExceptionHandler;

  @Mock
  private HttpServletRequest request;

  @Mock
  private HttpServletResponse response;

  @Mock
  private HttpSession session;

  @Mock
  private FilterChain filterChain;

  @Mock
  private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);

    when(tenantService.getTenantForUrl(anyString())).thenAnswer(invocation ->
    {
      if(invocation.getArguments() != null)
      {
        String host = (String) invocation.getArguments()[0];

        if(host != null)
        {
          // If it's not invalid, return it.
          // Affiliate flow will match against 'valid' and 'affiliate' strings
          if(!host.contains("invalid"))
          {
            return TenantDTOMock.getTenantDTO(host);
          }
        }
      }

      return null;
    });
  }

  private TenantUtil getTenantUtil()
  {
    TenantUtil tenantUtil = new TenantUtil();
    tenantUtil.setGhixJasyptEncrytorUtil(ghixJasyptEncrytorUtil);
    tenantUtil.setAffiliateFlowConfigService(affiliateFlowConfigService);
    tenantUtil.setTenantService(tenantService);

    return tenantUtil;
  }

  /**
   * Tests when tenant is not enabled. This should pass
   * with invalid hostnames.
   *
   * @throws Exception if excepton occurred.
   */
  @Test
  public void testTenantNotEnabled() throws Exception
  {
    log.info("Testing TenantFilter when tenant.enabled=off");

    System.setProperty("tenant.enabled", "off");

    StringBuffer requestURL = new StringBuffer("http://invalid.tenant.com/somepage.jsp");
    TenantFilter filter = new TenantFilter();

    filter.setTenantUtil(getTenantUtil());

    filter.isTenantEnabled = false;

    when(request.getRequestURL()).thenReturn(requestURL);
    when(request.getHeader("TENANT_KEY")).thenReturn("invalid.tenant.com");

    filter.doFilter(request, response, filterChain);

    log.info("Test passed");
  }

  /**
   * Tests for invalid hostname.
   *
   * @throws Exception if exception occurred.
   */
  @Test(expected = TenantRuntimeException.class)
  public void testInvalidTenantHostname() throws Exception
  {
    log.info("Testing TenantFilter with invalid hostname");

    StringBuffer requestURL = new StringBuffer("http://invalid.tenant.com/somepage.jsp");
    when(request.getRequestURL()).thenReturn(requestURL);

    System.setProperty("tenant.enabled", "on");

    TenantFilter filter = new TenantFilter();
    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    filter.doFilter(request, response, filterChain);
  }

  /**
   * Testing flow when there is no Tenant for specified URL, but there is an Affiliate
   * for given URL and tenant associated affiliate flow.
   *
   * @throws Exception if exception occurs.
   */
  @Test
  public void testInvalidTenantAndValidAffiliateUrl() throws Exception
  {
    log.info("Testing invalid tenant URL with VALID Affiliate URL");

    StringBuffer requestURL = new StringBuffer("http://invalid.affiliate.tenant.com/somepage.jsp");
    when(request.getRequestURL()).thenReturn(requestURL);
    System.setProperty("tenant.enabled", "on");

    TenantFilter filter = new TenantFilter();
    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    when(tenantService.getAffiliateForUrl(anyString())).thenAnswer(invocation -> {
      String url = (String) invocation.getArguments()[0];

      // if affiliate and valid present
      if(url.contains("affiliate") && url.contains("valid"))
      {
        return AffiliateMock.getAffiliate(url);
      }

      return null;
    });

    when(tenantService.getTenant(anyLong())).thenReturn(TenantDTOMock.getTenantDTO("invalid.affiliate.tenant.com"));
    when(request.getSession()).thenReturn(session);

    filter.doFilter(request, response, filterChain);

    log.info("Test passed");
  }

  /**
   * Testing flow when there is no Tenant for specified URL, there is no Affiliate for url, but
   * there is Affiliate Flow for given URL
   *
   * @throws Exception if exception occurs.
   */
  @Test
  public void testInvalidTenantAndValidAffiliateFlowUr() throws Exception
  {
    log.info("Testing invalid tenant URL, invalid Affiliate and Valid Affiliate Flow URL");

    StringBuffer requestURL = new StringBuffer("http://invalid.affiliateflow.tenant.com/somepage.jsp");
    when(request.getRequestURL()).thenReturn(requestURL);
    System.setProperty("tenant.enabled", "on");

    TenantFilter filter = new TenantFilter();
    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    when(tenantService.getAffiliateForUrl(anyString())).thenReturn(null);
    when(tenantService.getAffiliateFlowForUrl(anyString())).thenAnswer(invocation -> {
      String url = (String) invocation.getArguments()[0];

      if(url.contains("affiliateflow") && url.contains("valid"))
      {
        return AffiliateFlowMock.getAffiliateFlow(url);
      }

      return null;
    });

    when(tenantService.getTenant(anyLong())).thenReturn(TenantDTOMock.getTenantDTO("invalid.affiliateflow.tenant.com"));
    when(request.getSession()).thenReturn(session);

    filter.doFilter(request, response, filterChain);

    log.info("Test passed");
  }

  /**
   * Tests when hostname is valid.
   *
   * @throws Exception if exception occurred.
   */
  @Test
  public void testValidTenantHostname() throws Exception
  {
    log.info("Testing TenantFilter with valid hostname");

    StringBuffer requestURL = new StringBuffer("http://valid.tenant.com/somepage.jsp");
    when(request.getRequestURL()).thenReturn(requestURL);
    when(request.getSession()).thenReturn(session);

    System.setProperty("tenant.enabled", "on");

    TenantFilter filter = new TenantFilter();
    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    filter.doFilter(request, response, filterChain);
  }

  /**
   * Tests condition when hostname from request resolves to invalid tenant, but HTTP
   * header (TENANT_KEY) contains valid hostname.
   *
   * @throws Exception if exception occurred
   */
  @Test
  public void testValidTenantHostnameInHeader() throws Exception
  {
    log.info("Testing TenantFilter with valid hostname");

    StringBuffer requestURL = new StringBuffer("http://invalid.tenant.com/somepage.jsp");

    // Request URL will contain invalid tenant host, but header will be valid.
    when(request.getRequestURL()).thenReturn(requestURL);
    when(request.getHeader("TENANT_KEY")).thenReturn("valid.tenant.com");
    when(request.getSession()).thenReturn(session);

    System.setProperty("tenant.enabled", "on");

    TenantFilter filter = new TenantFilter();
    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    filter.doFilter(request, response, filterChain);
    filter.destroy();
  }

  /**
   * Tests condition when incoming request contains encrypted affiliate/flow information.
   *
   * @throws Exception if exception occurred.
   */
  @Test
  public void testSetTenantFromCookies() throws Exception
  {
    System.setProperty("tenant.enabled", "on");

    when(request.getRequestURL()).thenReturn(new StringBuffer("http://www.getinsured.com/home"));
    when(request.getHeader("TENANT_KEY")).thenReturn("valid.tenant.com");
    when(request.getSession()).thenReturn(session);

    when(ghixJasyptEncrytorUtil.decryptStringByJasypt(anyString())).thenAnswer(invocation ->
    {
      String hashed = (String) invocation.getArguments()[0];
      return decode(hashed);
    });

    when(ghixJasyptEncrytorUtil.encryptStringByJasypt(anyString())).thenAnswer(invocation -> {
      String str = (String) invocation.getArguments()[0];
      return encode(str);
    });

    when(request.getCookies()).thenReturn(getAffiliateIdAndFlowIdCookies(1L, 2));

    when(tenantService.getTenant(anyLong())).thenAnswer(invocation -> {
      Long tenantId = (Long)invocation.getArguments()[0];

      if(tenantId == 1L)
      {
        return TenantDTOMock.getTenantDTO("valid.tenant.url");
      }

      return null;
    });

    when(tenantService.getAffiliateById(anyLong())).thenAnswer(invocation -> {
      Long affiliateId = (Long) invocation.getArguments()[0];

      // Only for affiliateId == 1 we return valid affiliate.
      if(affiliateId == 1)
      {
        return AffiliateMock.getAffiliate("valid.affiliate.url");
      }

      // By default we cannot find affiliate with given id in database
      return null;
    });

    when(tenantService.getAffiliateFlowById(anyInt())).thenAnswer(invocation -> {
      Integer flowId = (Integer) invocation.getArguments()[0];

      // Only for flow id 2, we return valid flow.
      if(flowId == 2)
      {
        return AffiliateFlowMock.getAffiliateFlow("valid.flow.url");
      }

      // Otherwise we return null, meaning it's not found in database.
      return null;
    });

    when(tenantService.getDefaultFlowForAffiliate(anyLong())).thenAnswer(invocation -> {
      Long affiliateId = (Long)invocation.getArguments()[0];

      if(affiliateId == 1)
      {
        return AffiliateFlowMock.getAffiliateFlow("valid.flow.url");
      }

      return null;
    });


    TenantFilter filter = new TenantFilter();

    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    filter.doFilter(request, response, filterChain);

    TenantDTO afterFilterTenantDto = TenantContextHolder.getTenant();
    Assert.assertNotNull("Unable to get Tenant from TenantContextHolder", afterFilterTenantDto);
    Assert.assertTrue("invalid affiliate id stored in TenantContextHolder", 1L == TenantContextHolder.getAffiliateId());
    Assert.assertTrue("invalid flow id stored in TenantContextHolder", 1==TenantContextHolder.getFlowId());

    filter.destroy();
  }

  @Test
  public void testGetHostnameFromRequest() throws Exception
  {
    System.setProperty("tenant.enabled", "on");

    when(request.getRequestURL()).thenReturn(new StringBuffer("http://phix8qa-app1.getinsured.com/home"));
    //when(request.getHeader("TENANT_KEY")).thenReturn("www-app1.getinsured.com");
    when(request.getSession()).thenReturn(session);

    TenantFilter filter = new TenantFilter();

    filter.setTenantUtil(getTenantUtil());
    filter.setAffiliateFlowConfigService(affiliateFlowConfigService);
    filter.setTenantService(tenantService);
    filter.setGiExceptionHandler(giExceptionHandler);

    filter.doFilter(request, response, filterChain);

    TenantDTO afterFilterTenantDto = TenantContextHolder.getTenant();
    Assert.assertNotNull("Unable to get Tenant from TenantContextHolder", afterFilterTenantDto);

    String hostname = filter.getHostNameFromRequest(request);
    Assert.assertNotNull("Unable to get hostname from the request", hostname);
    Assert.assertEquals("Could not get hostname with -app1 postfix", "phix8qa-app1.getinsured.com", hostname);
  }

  private String encode(String s)
  {
    return new String(Base64.getEncoder().encode(s.getBytes()));
  }

  private String decode(String s)
  {
    return new String(Base64.getDecoder().decode(s.getBytes()));
  }

  private Cookie[] getAffiliateIdAndFlowIdCookies(Long affiliateId, Integer flowId)
  {
    final Cookie[] cookies = new Cookie[1];
    Cookie afCookie = new Cookie("AF", encode(affiliateId + "," + flowId));
    afCookie.setDomain(".getinsured.com");
    afCookie.setHttpOnly(true);
    afCookie.setPath("/");
    afCookie.setSecure(true);
    afCookie.setMaxAge(315_360_000);
    cookies[0] = afCookie;
    return cookies;
  }
}