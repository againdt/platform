package com.getinsured.hix.platform.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link GhixUtils} methods.
 *
 * @author Yevgen Golubenko
 * @since 10/4/17
 */
public class GhixUtilsTest
{
  private static final String VALID_HTML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<!DOCTYPE html \n" +
      "     PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
      "    \"DTD/xhtml1-strict.dtd\">\n" +
      "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n" +
      "  <head>\n" +
      "    <title>Minimal XHTML 1.0 Document</title>\n" +
      "  </head>\n" +
      "  <body>\n" +
      "    <p>This is a minimal <a href=\"http://www.w3.org/TR/xhtml1/\">XHTML 1.0</a> \n" +
      "    document.</p>\n" +
      "  </body>\n" +
      "</html>";

  private static final String INVALID_HTML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<!DOCTYPE html \n" +
      "     PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
      "    \"DTD/xhtml1-strict.dtd\">\n" +
      "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n" +
      "  <head>\n" +
      "    <title>Minimal XHTML 1.0 Document</title>\n" +
      "<body />" +    // <- INVALID tag placement
      "  </head>\n" +
      "  <body data-css=\"invalid-attribute\">\n" + // <- INVALID attribute
      "    <p>This is a minimal <a href=\"http://www.w3.org/TR/xhtml1/\">XHTML 1.0</a> \n" +
      "    document.</p>\n" +
      "  </body>\n" +
      "</html>";

  @Test
  public void parseValidHtmlData() throws IOException
  {
    String result = GhixUtils.parseHtmlData(VALID_HTML);
    Assert.assertNotNull(result);
  }

  @Test
  public void parseInvalidHtmlData() throws IOException
  {
    String result = GhixUtils.parseHtmlData(INVALID_HTML);
    Assert.assertNotNull(result);
  }

  @Test
  public void parseValidHtmlDataWithSummary() throws IOException
  {
    StringWriter errors = new StringWriter();
    PrintWriter out = new PrintWriter(errors);

    String result = GhixUtils.parseHtmlData(VALID_HTML, out);

    Assert.assertNotNull(result);
    Assert.assertNotEquals("Output did not contain summary but it should", "", errors.toString());
    Assert.assertTrue("Errors/Warnings were reported for valid document", errors.toString().toLowerCase().contains("no warnings or errors were found"));
  }

  @Test
  public void parseInvalidHtmlDataWithSummary() throws IOException
  {
    StringWriter errors = new StringWriter();
    PrintWriter out = new PrintWriter(errors);

    String result2 = GhixUtils.parseHtmlData(INVALID_HTML, out);

    Assert.assertNotNull(result2);
    Assert.assertNotEquals("Output did not contain warnings/errors/summary but it should", "", errors.toString());
    Assert.assertTrue("Errors were not reported for invalid document", errors.toString().toLowerCase().contains("error"));
    Assert.assertTrue("Warnings were not reported for invalid document", errors.toString().toLowerCase().contains("warning"));
    Assert.assertTrue("Summary was not reported for invalid document", errors.toString().toLowerCase().contains("document content looks like"));
  }
}