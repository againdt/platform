package com.getinsured.hix.ws_trust.saml;

import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.opensaml.common.binding.security.IssueInstantRule;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.impl.AssertionBuilder;
import org.opensaml.saml2.core.impl.AttributeBuilder;
import org.opensaml.saml2.core.impl.AuthnContextBuilder;
import org.opensaml.saml2.core.impl.AuthnStatementBuilder;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SubjectBuilder;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.schema.impl.XSStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.saml.SAMLCredential;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.TenantSSOConfiguration;
import com.getinsured.hix.model.sso.XtraConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.repository.IRoleRepository;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRoleRepository;
import com.getinsured.hix.platform.security.repository.IUserSessionRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.junit.mock.TenantDTOMock;
import com.getinsured.timeshift.TSDateTime;

/**
 * Unit tests for {@link GhixSamlUserDetailsService}
 * @author Yevgen Golubenko
 * @since 6/21/17
 */
public class GhixSamlUserDetailsServiceTest
{
  private static Logger log = LoggerFactory.getLogger(GhixSamlUserDetailsService.class);

  @Mock
  private IUserRepository userRepository;

  @Mock
  private IRoleRepository roleRepository;

  @Mock
  private IUserRoleRepository userRoleRepo;

  @Mock
  IUserSessionRepository sessionRepo;

  @Mock
  SessionTrackerService sessionTracker;

  @Mock
  private SCIMUserManager scimUserManager;

  @InjectMocks
  private GhixSamlUserDetailsService ghixSamlUserDetailsService;

  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);
  }

  @BeforeClass
  public static void beforeClass() throws Exception
  {
    System.setProperty("tenant.enabled", "on");
    System.setProperty("wso2", "true");

    TenantDTO tenantDTO = TenantDTOMock.getTenantDTO("http://www.getinsured.com", "GINS");

    StringBuilder claimToAccountUserMap = new StringBuilder();

    claimToAccountUserMap.append("userName=http://wso2.org/claims/username\n" +
        "firstName=http://wso2.org/claims/givenname\n" +
        "lastName=http://wso2.org/claims/lastname\n" +
        "securityQuestion1=http://wso2.org/claims/challengeQuestion1\n" +
        "securityQuestion2=http://wso2.org/claims/challengeQuestion2\n" +
        "securityAnswer1=http://wso2.org/claims/secanswer1\n" +
        "securityAnswer2=http://wso2.org/claims/secanswer2\n" +
        "preferredLanguage=http://wso2.org/claims/preferredLanguage\n" +
        "email=http://wso2.org/claims/emailaddress\n" +
        "communicationPref=http://wso2.org/claims/prefMethComm\n" +
        "phone=http://wso2.org/claims/telephone\n" +
        "extnAppUserId=http://wso2.org/claims/userid\n" +
        "samlPasswdTimeStamp=http://wso2.org/claims/identity/lastPasswordUpdateTime");

    TenantSSOConfiguration tenantSSOConfiguration = new TenantSSOConfiguration();

    XtraConfiguration xtraConfiguration = tenantSSOConfiguration.getXtraConfiguration();
    xtraConfiguration.setClaimToAccountUserMap(new String(Base64.getEncoder().encode(claimToAccountUserMap.toString().getBytes())));

    tenantSSOConfiguration.setXtraConfiguration(xtraConfiguration);
    tenantDTO.setTenantSSOConfiguration(tenantSSOConfiguration);

    TenantContextHolder.setTenant(tenantDTO);
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void loadUserBySAMLWithValidRole() throws Exception
  {
    Mockito.when(roleRepository.findIdByName(anyString())).thenAnswer(invocation -> {
      String roleName = (String) invocation.getArguments()[0];

      if(roleName.equals("CONSUMER"))
      {
        Role role = new Role();
        role.setId(1);
        role.setName("CONSUMER");
        role.setDescription("Consumer role");

        return role;
      }

      return null;
    });
    GhixPlatformConstants constants = new GhixPlatformConstants();
    constants.setDefauleProvisioningRole("CONSUMER");
    constants.setIdentityLookupField("ExternalId");
    SAMLCredential credential = buildSamlCredential();
    AccountUser accountUser = (AccountUser) ghixSamlUserDetailsService.loadUserBySAML(credential);
    Assert.assertNotNull("AccountUser returned null", accountUser);
    Assert.assertEquals("Role for the user didn't match expected", "CONSUMER", accountUser.getDefRole().getName());
  }

  @Test(expected = GIRuntimeException.class)
  public void loadUserBySAMLWithInValidRole() throws Exception
  {
    Mockito.when(roleRepository.findIdByName(anyString())).thenAnswer(invocation -> {
      String roleName = (String) invocation.getArguments()[0];
      log.info("Querying repository for role with name {} and returning NULL", roleName);
      return null;
    });
    GhixPlatformConstants constants = new GhixPlatformConstants();
    constants.setJustInTimeCreateEnabled("true");
    constants.setJustInTimeSyncEnabled("true");
    constants.setIdentityLookupField("ExternalId");
    SAMLCredential credential = buildSamlCredential();
    AccountUser accountUser = (AccountUser) ghixSamlUserDetailsService.loadUserBySAML(credential);
  }


  private SAMLCredential buildSamlCredential()
  {
    NameID nameID = new NameIDBuilder().buildObject();
    nameID.setSPProvidedID("GINS");
    nameID.setValue("GINS");

    Subject subject = new SubjectBuilder().buildObject();
    subject.setNameID(nameID);

    Assertion assertion = new AssertionBuilder().buildObject();
    assertion.setID("sso.ghixqa.com");
    assertion.setSubject(subject);
    assertion.getAuthnStatements().add(buildAuthnStatement());

    Issuer issuer = new IssuerBuilder().buildObject();
    issuer.setValue("sso.ghixqa.com");
    assertion.setIssuer(issuer);

    IssueInstantRule issueInstantRule = new IssueInstantRule(10, 600);
    assertion.setIssueInstant(TSDateTime.getInstance());

    List<Attribute> attributes = new ArrayList<>();
    Attribute giTenant = buildAttribute("http://wso2.org/claims/giTenantId", "giTenantId", TenantContextHolder.getTenant().getCode());
    Attribute username = buildAttribute("http://wso2.org/claims/username", "username", "me@me.com");
    Attribute externAppId = buildAttribute("http://wso2.org/claims/userid", "userid", UUID.randomUUID().toString());
    Attribute firstName = buildAttribute("http://wso2.org/claims/givenname", "firstName", "Yevgen");
    Attribute lastName = buildAttribute("http://wso2.org/claims/lastname", "lastName", "Golubenko");
    Attribute emailAddress =buildAttribute("http://wso2.org/claims/emailaddress", "email", "me@me.com");

    attributes.add(giTenant);
    attributes.add(username);
    attributes.add(externAppId);
    attributes.add(firstName);
    attributes.add(lastName);
    attributes.add(emailAddress);

    SAMLCredential credential = new SAMLCredential(nameID, assertion, "sso.ghixqa.com", "#tid=1,#aid=1,#fid=2", attributes, "GINS");

    return credential;
  }

  private Attribute buildAttribute(String tag, String friendlyName, String value)
  {
    Attribute attribute = new AttributeBuilder().buildObject();
    attribute.setName(tag);
    attribute.setFriendlyName(friendlyName);

    XSStringBuilder stringBuilder = new XSStringBuilder();
    XSString attributeValue = stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
    attributeValue.setValue(value);
    attribute.getAttributeValues().add(attributeValue);

    return attribute;
  }

  private AuthnStatement buildAuthnStatement()
  {
    AuthnContext authnContext = new AuthnContextBuilder().buildObject();

    AuthnStatement statement = new AuthnStatementBuilder().buildObject();
    statement.setAuthnInstant(TSDateTime.getInstance());
    statement.setAuthnContext(authnContext);

    return statement;
  }
}
