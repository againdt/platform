package com.getinsured.platform.util.security.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletRequest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.handler.RequestHandler;
import com.getinsured.hix.platform.handler.RequestHandlerFactory;
import com.getinsured.hix.platform.security.RoleAccessControl;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.PasswordService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;
import com.getinsured.hix.platform.um.handlers.PreUserCreateHandler;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.junit.FunctionalTest;
import com.getinsured.junit.mock.AccountUserMock;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

/**
 * UserService tests.
 *
 * Created by Yevgen Golubenko on 1/13/17.
 * @author Yevgen Golubenko
 */
public class UserServiceTest
{
  private static final Logger log = LoggerFactory.getLogger(UserServiceTest.class);

  @Mock
  private IUserRepository userRepository;

  @Mock
  private PasswordService passwordService;

  @Mock
  private RoleService roleService;

  @Mock
  private RoleAccessControl roleAccessControl;

  @Mock
  private SCIMUserManager scimUserManager;

  @Mock
  private UserRoleService userRoleService;

  @Mock
  private RequestHandlerFactory handlerFactory;

  @InjectMocks
  private UserServiceImpl userService = new UserServiceImpl();

  private AccountUser testUser;

  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);
    testUser = AccountUserMock.getAccountUser();
  }

  @After
  public void tearDown() throws Exception
  {

  }

  @Test
  public void getUserByEmail()
  {
    log.info("Mocking userRepository.findByEmail()");

    when(userRepository.findByEmail(anyString())).thenReturn(testUser);

    AccountUser accountUser = userService.findByEmail("test@junit.org");

    Assert.assertNotNull("AccountUser.findByEmail returned null", accountUser);
    log.info("Got accountUser from userService.findByEmail: {}", accountUser.getEmail());

    Assert.assertEquals("Email addresses do not match", "test@junit.org", accountUser.getEmail());
  }

  @Test
  public void findAccountUsersForRoleNames()
  {
    List<AccountUser> ret = new ArrayList<>();
    ret.add(testUser);

    log.info("Mocking userRepository.findAccountUsersForRoleNames()");
    when(userRepository.findAccountUsersForRoleNames(any(List.class))).thenReturn(ret);

    List<String> roleNames = new ArrayList<>();
    roleNames.add("ROLE_ADMIN");
    List<AccountUser> users = userService.findAccountUsersForRoleNames(roleNames);

    Assert.assertNotNull("No AccountUsers returned for given roles", users);
    Assert.assertTrue("List of AccountUsers is != 1", users.size() == 1);

    AccountUser u = users.get(0);
    log.info("Got List<AccountUser> from userService.findAccountUsersForRoleNames: " + u.getFirstName());
    Assert.assertEquals("AccountUser's role name doesn't match", roleNames.get(0), u.getDefRole().getName());
  }

  @Test
  public void createUser() throws GIException
  {
    // When we are searching for role, return test role.
    when(roleService.findRoleByName(anyString())).thenReturn(testUser.getDefRole());

    // Allow creation of user when checking access control.
    when(roleAccessControl.canCreateUserForRole(anyString())).thenReturn(true);

    // Return account user when searching for UUID to invoke retry logic
    List<AccountUser> nonUniqueUUID = new ArrayList<>();
    nonUniqueUUID.add(testUser);
    when(userRepository.findByUuid(anyString())).thenReturn(nonUniqueUUID).thenReturn(new ArrayList<AccountUser>());

    // Our createUser method also calls passwordService->updatePassword, so let's mock that by returning what has been passed
    when(passwordService.updatePassword(any(AccountUser.class), anyString(), anyBoolean())).thenAnswer(invocation -> invocation.getArguments()[0]);

    // Before saving it, we make a request to handlerFactory instanse
    when(handlerFactory.getHandlerByName(any(String.class))).thenReturn(new RequestHandler()
    {
      @Override
      public String getName()
      {
        return PreUserCreateHandler.NAME;
      }

      @Override
      public List<String> getHandlerPath()
      {
        return null;
      }

      @Override
      public boolean handleRequest(HashMap<String, Object> context, ServletRequest request, boolean async)
      {
        // Assume that everything is ok
        return true;
      }
    });

    // When we are saving something to DB, return that exact instance that passed
    when(userRepository.save(any(AccountUser.class))).thenAnswer(invocation -> invocation.getArguments()[0]);

    try
    {
      AccountUser newUser = userService.createUser(testUser, testUser.getDefRole().getName());
      Assert.assertNotNull("Could not save user, save returned null object", newUser);
    } catch (GIException e)
    {
      e.printStackTrace();
      Assert.fail("Exception while saving user: " + e.getMessage());
    }
  }

  @Test(expected = GIRuntimeException.class)
  public void loggedInUserHasNoPermissionsToCreateUser() throws GIException
  {
    // When we are searching for role, return test role.
    when(roleService.findRoleByName(anyString())).thenReturn(testUser.getDefRole());

    // Return account user when searching for UUID to invoke retry logic
    List<AccountUser> nonUniqueUUID = new ArrayList<>();
    nonUniqueUUID.add(testUser);
    when(userRepository.findByUuid(anyString())).thenReturn(nonUniqueUUID).thenReturn(new ArrayList<AccountUser>());

    // DO NOT allow current user to create any users.
    when(roleAccessControl.canCreateUserForRole(anyString())).thenReturn(false);

    AccountUser newUser = userService.createUser(testUser, testUser.getDefRole().getName());
    Assert.assertNull(newUser);
  }
}
