package com.getinsured.platform.util;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * JUnit tests for {@link HIXHTTPClient}
 *
 * Created by golubenko_y on 1/13/17.
 */
@Ignore("Skipping http tests, because httpbin.org is down")
public class HIXHTTPClientTest
{
  private static final Logger log = LoggerFactory.getLogger(HIXHTTPClientTest.class);

  @Mock
  private HIXHTTPClient hIXHTTPClient;

  @Mock
  private HttpServletRequest request;

  @Mock
  private HttpSession session;

  private static final String HOST = "https://httpbin.org";

  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);
    when(request.getSession()).thenReturn(session);

    ServletRequestAttributes attributes = new ServletRequestAttributes(request);
    RequestContextHolder.setRequestAttributes(attributes);

    // Modifying local SSL context to accept all certificates.
    SSLContext ctx = SSLContext.getInstance("TLS");
    ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
    SSLContext.setDefault(ctx);

    CloseableHttpClient closeableHttpClient = HttpClients.createSystem();

    when(hIXHTTPClient.getHttpClient()).thenReturn(closeableHttpClient);
  }

  @After
  public void tearDown() throws Exception
  {

  }

  @Test
  public void testGetWithHeaders()
  {
    final String url = HOST + "/headers";

    final HashMap<String, String> headers = new HashMap<>(3);
    headers.put("Tenant-Key", "test-tenant");
    headers.put("Authentication-Method", "Simple");
    headers.put("Test-Header", "test-value");

    String data = null;

    try
    {
      when(hIXHTTPClient.getDataWithHeaders(anyString(), any(HashMap.class))).thenCallRealMethod();

      data = hIXHTTPClient.getDataWithHeaders(url, headers);

      Assert.assertNotNull("No data returned for GET wto url: " + url, data);

      log.info("Got data for url: {} => {}", url, data);

      Gson gson = GhixUtils.platformGson();

      LinkedTreeMap<String, Object> linkedTreeMap = getResponseTreeMap(data);
      LinkedTreeMap<String, String> retHeaders = (LinkedTreeMap) linkedTreeMap.get("headers");
      Assert.assertNotNull("There is no 'headers' key in returned JSON", retHeaders);

      headers.forEach((header, value) -> {
        Assert.assertEquals("Header value doesn't match passed value", value, retHeaders.get(header));
        //System.out.println(String.format("validated: %s => %s", value, retHeaders.get(header)));
        log.info("validated header=>value: {}=>{}", value, retHeaders.get(header));
      });
    } catch (Exception e)
    {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void getDataWithWaitTime()
  {
    final String url = HOST + "/delay/3";

    String data = null;

    try
    {
      when(hIXHTTPClient.getDataWithWaitTime(anyString(), any(Integer.class))).thenCallRealMethod();

      data = hIXHTTPClient.getDataWithWaitTime(url, 5000);
      Assert.assertNotNull("No data returned for GET to url: " + url, data);
      log.info("Got data for url: {} => {}", url, data);
    } catch (Exception e)
    {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void getPOSTDataWithWaitTime() throws Exception
  {
    final String url = HOST + "/status/200";

    String data = null;

    try
    {
      when(hIXHTTPClient.getPOSTDataWithWaitTime(anyString(), anyString(), anyString(), any(Integer.class))).thenCallRealMethod();
      data = hIXHTTPClient.getPOSTDataWithWaitTime(url, "{\"param1\"=\"value1\",\"param2\":\"value2\"}", "application/json", 3000);
      Assert.assertNotNull("No data returned for post to url: " + url, data);
      log.info("Got data for url: {} => {}", url, data);
    } catch (Exception e)
    {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void getPOSTData() throws Exception
  {
    final String url = HOST + "/post";

    String data = null;

    when(hIXHTTPClient.getPOSTData(anyString(), anyString(), anyString())).thenCallRealMethod();

    try
    {
      data = hIXHTTPClient.getPOSTData(url, "{\"param1\"=\"value1\",\"param2\":\"value2\"}", "application/json");
      Assert.assertNotNull("No data returned for post to url: " + url, data);

      log.info("Got data for url: {} => {}", url, data);
      LinkedTreeMap<String, Object> resp = getResponseTreeMap(data);
      Assert.assertNotNull("Could not get tree map from response", resp);
      String respData = (String)resp.get("data");
      Assert.assertTrue("Response data didn't contain passed parameter", respData.contains("value1"));
      Assert.assertTrue("Response data didn't contain passed parameter", respData.contains("value2"));
    } catch (Exception e)
    {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void postFormData() throws Exception
  {
    final String url = HOST + "/post";
    String data = null;

    HashMap<String, String> postParams = new HashMap<>();
    postParams.put("param1", "value1");
    postParams.put("param2", "value2");

    final HashMap<String, String> headers = new HashMap<>(3);
    headers.put("Tenant-Key", "test-tenant");
    headers.put("Authentication-Method", "Simple");
    headers.put("Test-Header", "test-value");

    try
    {
      when(hIXHTTPClient.postFormData(anyString(), any(HashMap.class), any(HashMap.class))).thenCallRealMethod();

      data = hIXHTTPClient.postFormData(url, headers, postParams);
      Assert.assertNotNull("No data returned for post to url: " + url, data);
      log.info("Got data for url: {} => {}", url, data);
      LinkedTreeMap<String, Object> resp = getResponseTreeMap(data);
      Assert.assertNotNull("Could not get tree map from response", resp);
      LinkedTreeMap<String, Object> form = (LinkedTreeMap<String, Object>) resp.get("form");
      postParams.forEach((k,v) -> {
        Assert.assertEquals("Passed form key/value didn't match response", v, form.get(k));
        log.info("Validated form response for passed key=>value: {} => {}", k, form.get(k));
      });
    } catch (Exception e)
    {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void getGETData() throws Exception
  {
    final String url = HOST + "/get?param1=value1&param2=value2";

    when(hIXHTTPClient.getGETData(anyString())).thenCallRealMethod();
    final String data = hIXHTTPClient.getGETData(url);
    Assert.assertNotNull("Nothing returned from " + url, data);

    log.info("Got data from {}: {}", url, data);

    LinkedTreeMap<String, Object> resp = getResponseTreeMap(data);
    Assert.assertNotNull("Could not get tree map from response", resp);
    LinkedTreeMap<String, Object> args = (LinkedTreeMap<String, Object>)resp.get("args");

    Assert.assertEquals("Returned argument list doesn't match passed argument list", "value1", args.get("param1"));
    Assert.assertEquals("Returned argument list doesn't match passed argument list", "value2", args.get("param2"));
  }

  private LinkedTreeMap<String, Object> getResponseTreeMap(final String data)
  {
    Gson gson = GhixUtils.platformGson();
    LinkedTreeMap<String, Object> linkedTreeMap = (LinkedTreeMap) gson.fromJson(data, Object.class);

    return linkedTreeMap;
  }

  /**
   * Accept any certificate for unit tests...
   */
  private static class DefaultTrustManager implements X509TrustManager
  {
    @Override
    public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

    @Override
    public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

    @Override
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }
  }
}
