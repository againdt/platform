package com.getinsured.platform.couchbase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.ReplicaMode;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.getinsured.hix.platform.config.CBEnvironment;

import rx.plugins.RxJavaErrorHandler;
import rx.plugins.RxJavaPlugins;

/**
 * Testing multi-node couchbase server setup.
 *
 * @author Yevgen Golubenko
 * @since 10/3/17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {CBEnvironment.class})
@Ignore
public class CouchbaseMultiNodeSpringTest
{
  private static final Logger log = LoggerFactory.getLogger(CouchbaseMultiNodeSpringTest.class);

  @Autowired
  private CBEnvironment cbEnvironment;

  private static CouchbaseEnvironment env = null;
  private Cluster cluster = null;
  private Bucket bucket = null;

  private static final String DOCUMENT_ID ="TK::ATTACH::bb6dd123-6edc-462a-ab34-150977273bd1";
  // "TK::ATTACH::d0d3695e-5499-413b-a901-509392196df5";

  /* Which node + bucket from array to use (defined below) */
  private static final int CB_NODE = 0;

  private static final String[] nodes = new String[]{
      /* 0 */ "http://sfo-cbqa1.ghixqa.com"
  };

  private static final String[] bucketName = {
      /* 0 */ "phix8qa_nonbin"
  };

  private static final String password = "ghix123#";

  private static Map<String, Throwable> RX_ERRORS = null;

  @BeforeClass
  public static void beforeClass()
  {
    RxJavaPlugins.getInstance().registerErrorHandler(new RxJavaErrorHandler()
    {
      @Override
      public void handleError(Throwable e)
      {
        super.handleError(e);
        RX_ERRORS.put(e.getMessage(), e);
      }

      @Override
      protected String render(Object item) throws InterruptedException
      {
        return super.render(item);
      }
    });
  }

  @Before
  public void beforeTest()
  {
    RX_ERRORS = new HashMap<>();
    if (env == null)
    {
      env = cbEnvironment.environment();
    }

    if (cluster == null)
    {
      cluster = CouchbaseCluster.create(env, nodes[CB_NODE]);
    }

    if (bucket == null)
    {
      bucket = cluster.openBucket(bucketName[CB_NODE], password);
    }
  }

  @After
  public void afterTest()
  {
    bucket.close();
  }

  @AfterClass
  public static void afterClass()
  {
    RxJavaPlugins.getInstance().reset();
  }

  @Test
  public void testCBConnection()
  {
    RX_ERRORS = new HashMap<>();
    try
    {
      Cluster cluster = CouchbaseCluster.create(env, nodes[CB_NODE]);
      Bucket bucket = cluster.openBucket(bucketName[CB_NODE], password);
      cluster.disconnect();
    }
    catch (Exception e)
    {
      Assert.fail("Exception:" + e.getMessage());
    }

    if (RX_ERRORS != null && !RX_ERRORS.isEmpty())
    {
      printRxExceptions(RX_ERRORS);
      Assert.fail("Exceptions occurred in RX: " + RX_ERRORS.size());
    }
  }


  @Test
  public void testCBConnectionLoop() throws Exception
  {
    RX_ERRORS = new HashMap<>();
    int totalLoops = 10;

    for (int i = 0; i < totalLoops; i++)
    {
      log.info("Connection test: {}/{}", i, totalLoops);
      connect();
      log.info("complete");
    }

    if (RX_ERRORS != null && !RX_ERRORS.isEmpty())
    {
      printRxExceptions(RX_ERRORS);
      Assert.fail("Exceptions occurred in RX: " + RX_ERRORS.size());
    }
  }

  private void connect() throws Exception
  {
    log.info("Connecting to couchbase: {}; bucket = {}; pass = {}", nodes[CB_NODE], bucketName[CB_NODE], password);

    if (!bucket.isClosed() && bucket.exists(DOCUMENT_ID))
    {
      JsonDocument doc = bucket.get(DOCUMENT_ID);
      log.info("got document: {}", doc.toString());

      Assert.assertNotNull(doc);

      List<JsonDocument> documents = this.bucket.async()
          .getFromReplica(DOCUMENT_ID, ReplicaMode.ALL, JsonDocument.class)
          .toList().toBlocking().single();

      Assert.assertNotNull(documents);
      log.info("Got List with one document: {}", documents);
      Assert.assertTrue(documents.size() > 0);

      final JsonDocument[] singleDocument = {null};

      if(documents.size() > 1)
      {
        documents.forEach(d -> {
          if (singleDocument[0] == null)
          {
            singleDocument[0] = d;
          }
          if (singleDocument[0].content() != null)
          {
            JsonObject jsonMetadata = (JsonObject) singleDocument[0].content().get("metaData");
            Long modifiedDate = Long.valueOf(String.valueOf(jsonMetadata.get("modifiedDate")));

            JsonObject jsonMetadataNext = (JsonObject) d.content().get("metaData");
            Long newModifiedDate = Long.valueOf(String.valueOf(jsonMetadataNext.get("modifiedDate")));

            if (newModifiedDate > modifiedDate)
            {
              singleDocument[0] = d;
            }
          }
        });
      }
      else if(documents.size() == 1)
      {
        singleDocument[0] = documents.get(0);
      }

      Assert.assertNotNull(singleDocument[0]);
      log.info("Got one document from multi-node Couchbase server: {}", singleDocument[0]);
    } else
    {
      log.error("Bucket is closed [{}]; bucket document exists = {}", bucket.isClosed(), bucket.exists(DOCUMENT_ID));
    }

    log.info("Disconnected ok");
  }

  /**
   * This unit test uses old code block that was causing the failure.
   * <p>
   *   As it can be seen, this unit test expected to fail with {@link IllegalArgumentException}.
   * </p>
   */
  @Test(expected = java.lang.IllegalArgumentException.class)
  public void testMultiNodeDocumentRetrievalWithFailure()
  {
    log.info("Connecting to couchbase: {}; bucket = {}; pass = {}", nodes[CB_NODE], bucketName[CB_NODE], password);

    if (!bucket.isClosed() && bucket.exists(DOCUMENT_ID))
    {
      JsonDocument doc = bucket.get(DOCUMENT_ID);
      log.info("got document: {}", doc.toString());

      JsonDocument document = this.bucket
          .async()
          .getFromReplica(DOCUMENT_ID, ReplicaMode.ALL, JsonDocument.class)
          .toBlocking()
          .singleOrDefault(null);

      Assert.assertNull("Document should be null because exception should be thrown with this kind of access", document);
    } else
    {
      log.error("Bucket is closed [{}]; bucket document exists = {}", bucket.isClosed(), bucket.exists(DOCUMENT_ID));
    }

    log.info("Disconnected ok");
  }

  private void printRxExceptions(Map<String, Throwable> rxExceptions)
  {
    if (rxExceptions != null && !rxExceptions.isEmpty())
    {
      rxExceptions.forEach((k, v) -> {
        log.error("Exception: " + k);
        v.printStackTrace(System.err);
      });
    }
  }
}
