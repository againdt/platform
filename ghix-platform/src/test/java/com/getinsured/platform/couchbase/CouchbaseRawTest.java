package com.getinsured.platform.couchbase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.getinsured.junit.MultiThreadedStressTester;

import rx.plugins.RxJavaErrorHandler;
import rx.plugins.RxJavaPlugins;

/**
 * Couchbase connection tests.
 *
 * @author Yevgen Golubenko
 * @since 9/11/17
 */
@Ignore
public class CouchbaseRawTest
{
  private static final Logger log = LoggerFactory.getLogger(CouchbaseRawTest.class);

  private static final CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
      .connectTimeout(100_000) // default is 5s
      .socketConnectTimeout(1000 * 60)
      .tcpNodelayEnabled(true)
      .userAgent("yevgen-junit-test-client")
      .kvTimeout(30_000) // default 2500ms
      .build();

  private Cluster cluster = null;
  private Bucket bucket = null;

  /* Which node + bucket from array to use (defined below) */
  private static final int CB_NODE = 2;

  private static final String[] nodes = new String[]{
      /* 0 */ "http://cbnyc6.ghixqa.com",    // 4.1.0-5005 compaction
      /* 1 */ "http://cbnyc5.ghixqa.com",    // 4.1.0-5005 no compaction
      /* 2 */ "http://cbsfo4.ghixqa.com",    // 4.1.0-5005 compaction
      /* 3 */ "http://sfo-cbdev1.ghixqa.com" // 4.6.1      compaction
  };

  private static final String[] bucketName = {
      /* 0 */ "id2auto_nonbin",
      /* 1 */ "idmainqa_nonbin",
      /* 2 */ "phix2dev_nonbin",
      /* 3 */ "phix1dev_nonbin"
  };

  private static final String password = "ghix123#";

  private static Map<String, Throwable> RX_ERRORS = null;

  @BeforeClass
  public static void beforeClass()
  {
    RxJavaPlugins.getInstance().registerErrorHandler(new RxJavaErrorHandler()
    {
      @Override
      public void handleError(Throwable e)
      {
        super.handleError(e);
        RX_ERRORS.put(e.getMessage(), e);
      }

      @Override
      protected String render(Object item) throws InterruptedException
      {
        return super.render(item);
      }
    });
  }

  @Before
  public void beforeTest()
  {
    RX_ERRORS = new HashMap<>();
    cluster = CouchbaseCluster.create(env, nodes[CB_NODE]);
    bucket = cluster.openBucket(bucketName[CB_NODE], password);
  }

  @After
  public void afterTest()
  {
    bucket.close();
    cluster.disconnect();
  }

  @AfterClass
  public static void afterClass()
  {
    RxJavaPlugins.getInstance().reset();
  }

  @Test
  public void testCBConnection()
  {
    RX_ERRORS = new HashMap<>();
    try
    {
      Cluster cluster = CouchbaseCluster.create(env, nodes[CB_NODE]);
      Bucket bucket = cluster.openBucket(bucketName[CB_NODE], password);
      cluster.disconnect();
    }
    catch (Exception e)
    {
      Assert.fail("Exception:" + e.getMessage());
    }

    if(RX_ERRORS != null && !RX_ERRORS.isEmpty())
    {
      printRxExceptions(RX_ERRORS);
      Assert.fail("Exceptions occurred in RX: " + RX_ERRORS.size());
    }
  }

  @Test
  public void testCBConnectionMultiThreaded() throws Exception
  {
    RX_ERRORS = new HashMap<>();

    MultiThreadedStressTester stressTester = new MultiThreadedStressTester(500, 10);

    List<String> exceptions = new ArrayList<>();

    stressTester.stress(() ->
    {
      try
      {
       connect();
      }
      catch(RuntimeException ex)
      {
        exceptions.add(ex.getMessage());
        ex.printStackTrace();
      }
      catch(Exception e)
      {
        exceptions.add(e.getMessage());
        e.printStackTrace();
      }
    });

    stressTester.shutdown();

    if(exceptions.size() > 0)
    {
      for(String msg : exceptions)
      {
        System.err.println("Exception: " + msg);
      }
      Assert.fail("Number of exceptions occurred > 0: " + exceptions.size());
    }

    if(RX_ERRORS != null && !RX_ERRORS.isEmpty())
    {
      printRxExceptions(RX_ERRORS);
      Assert.fail("Exceptions occurred in RX: " + RX_ERRORS.size());
    }
  }

  @Test
  public void testCBConnectionLoop()
  {
    RX_ERRORS = new HashMap<>();
    int totalLoops = 100;

    for(int i = 0; i < totalLoops; i++)
    {
      log.info("Connection test: {}/{}", i, totalLoops);
      connect();
      log.info("complete");
    }

    if(RX_ERRORS != null && !RX_ERRORS.isEmpty())
    {
      printRxExceptions(RX_ERRORS);
      Assert.fail("Exceptions occurred in RX: " + RX_ERRORS.size());
    }
  }

  private void connect()
  {
    log.info("Connecting to couchbase: {}; bucket = {}; pass = {}", nodes[CB_NODE] , bucketName[CB_NODE], password);

    if(!bucket.isClosed() && bucket.exists("test"))
    {
      JsonDocument doc = bucket.get("test");
      log.info("got document: " + doc.toString());
    }
    else
    {
      log.error("Bucket is closed [{}]; bucket test exists = {}", bucket.isClosed(), bucket.exists("test"));
    }

    log.info("Disconnected ok");
  }

  private void printRxExceptions(Map<String, Throwable> rxExceptions)
  {
    if(rxExceptions != null && !rxExceptions.isEmpty())
    {
      rxExceptions.forEach((k, v) -> {
        log.error("Exception: " + k);
        v.printStackTrace(System.err);
      });
    }
  }
}
