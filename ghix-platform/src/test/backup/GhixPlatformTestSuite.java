/**
 * 
 */
package resources_backup;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test suite for one or many junit tests. 
 * Add yours here if you want to run them together 
 * as a single junit suite.
 * 
 * @author Yevgen Golubenko
 */
@RunWith(Suite.class)
@SuiteClasses({
	GhixPlatformCacheTest.class,
        DynamicPropertiesUtilTest.class,
        DemoTest.class
})
public class GhixPlatformTestSuite
{
	/**
	 * Main execution point.
	 * @param args command line arguments.
	 */
	public static void main(final String[] args)
	{
		System.out.println("Launching GhixPlatformTestSuite");
		JUnitCore.runClasses(GhixPlatformTestSuite.class);
	}
}
