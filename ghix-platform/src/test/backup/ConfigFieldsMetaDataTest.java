package resources_backup;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.junit.Test;
import org.xml.sax.SAXException;

import com.getinsured.hix.config.ConfigFieldsMetaData;
import com.getinsured.hix.config.ConfigurationFieldMetadata;
import com.getinsured.hix.config.InvalidOperationException;

/**
 * Created by golubenko_y on 10/13/16.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
public class ConfigFieldsMetaDataTest
{
  public static int i = 0;

  @Test
  public void testGetMetadataMap()
  {
    try
    {
      System.setProperty("GHIX_HOME", "/Users/golubenko_y/projects/branches/MAIN/ghix");

      for(i = 0; i < 3; i++)
      {
        long st = TimeShifterUtil.currentTimeMillis();

        ConfigFieldsMetaData cfmd = ConfigFieldsMetaData.getMetadataMap();
        cfmd.keySet().forEach(k -> {
          if(i != 0)
            System.out.println("Key: " + k + " => " + cfmd.get(k));
        });

        if(i != 0)
          System.out.println(TimeShifterUtil.currentTimeMillis() - st + "ms\n====");

        st = TimeShifterUtil.currentTimeMillis();
        ConfigurationFieldMetadata cf = ConfigurationFieldMetadata.getMetadataMap();
        cf.keySet().forEach(k -> {
          if(i != 0)
          System.out.println("Key: " + k + " => " + cf.get(k));
        });

        if(i != 0)
          System.out.println(TimeShifterUtil.currentTimeMillis() - st + "ms");
      }
    } catch (InvalidOperationException e)
    {
      e.printStackTrace();
    } catch (ParserConfigurationException e)
    {
      e.printStackTrace();
    } catch (SAXException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    } catch (XMLStreamException e)
    {
      e.printStackTrace();
    }
  }
}
