package resources_backup;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.naming.NamingException;

import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp.datasources.SharedPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

/**
 * Created by golubenko_y on 8/30/16.
 */
@Configuration
public class DatabaseContextCreator
{
  private static final Logger log = LoggerFactory.getLogger(DatabaseContextCreator.class);

  private static final String DATABASE_DEFINITION_FILE_NAME = "database.txt";
  private static final String JNDI_NAMING_CONTEXT = "java:comp/env/jdbc/ghixDS";

  private static final Pattern newLinePattern = Pattern.compile("[\\r\\n;]+");
  private static URL databaseConfigURL = null;

  @PostConstruct
  public void init()
  {
    log.info("Used {} file for JNDI DB configuration", databaseConfigURL);
  }

  public DatabaseContextCreator()
  {
    databaseConfigURL = DatabaseContextCreator.class.getClassLoader().getResource(DATABASE_DEFINITION_FILE_NAME);

    Scanner scanner = null;

    try
    {
      scanner = new Scanner(databaseConfigURL.openStream());
      scanner.useDelimiter(newLinePattern);
    }
    catch (IOException e)
    {
      log.error("Exception while reading DB information from given file: {}", e.getMessage());
      log.error("Exception occurred", e);
    }

    if(scanner != null)
    {
      DriverAdapterCPDS cpds = new DriverAdapterCPDS();
      final Map<String, String> databaseUrlMap =  new HashMap<>();

      scanner.forEachRemaining(line -> {
        if(!"".equals(line.trim()))
        {
          String value = line.split("=")[1].trim();

          if (line.startsWith("Driver"))
          {
            try
            {
              cpds.setDriver(value);
            } catch (ClassNotFoundException cne)
            {
              log.error("JDBC Driver defined in {} not found: {}", DATABASE_DEFINITION_FILE_NAME, cne.getMessage());
            }
          } else if (line.startsWith("Server"))
          {
            databaseUrlMap.put("SERVER", value);
          } else if (line.startsWith("Port"))
          {
            databaseUrlMap.put("PORT", value);
          } else if (line.startsWith("Schema"))
          {
            databaseUrlMap.put("SCHEMA", value);
          } else if (line.startsWith("Username"))
          {
            cpds.setUser(value);
          } else if (line.startsWith("Password"))
          {
            if(cpds.getPassword() == null || "".equals(cpds.getPassword()))
            {
              cpds.setPassword(value);
            }
          }
        }
      });

      // hibernate.temp.use_jdbc_metadata_defaults
      cpds.setUrl("jdbc:postgresql://" + databaseUrlMap.get("SERVER") + ":" + databaseUrlMap.get("PORT") + "/" + databaseUrlMap.get("SCHEMA"));

      try
      {
        SharedPoolDataSource dataSource = new SharedPoolDataSource();
        dataSource.setConnectionPoolDataSource(cpds);
        dataSource.setMaxActive(10);
        dataSource.setMaxWait(50);

        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
        builder.bind(JNDI_NAMING_CONTEXT, dataSource);
        builder.activate();
      }
      catch (NamingException ex)
      {
        log.error("Unable to activate {} context: {}", JNDI_NAMING_CONTEXT, ex.getMessage());
        log.error("Exception occurred", ex);
      }
    }
  }
}
