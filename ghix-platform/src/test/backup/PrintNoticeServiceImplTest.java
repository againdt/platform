package resources_backup;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.getinsured.hix.model.Notice;
import resources_backup.GhixBaseTest;
import com.getinsured.hix.platform.paper.PrintNoticeService;
import com.getinsured.hix.platform.util.DateUtil;

/**
 * The class <code>PrintNoticeServiceImplTest</code> contains tests for the
 * class {@link <code>PrintNoticeServiceImpl</code>}
 *
 * @pattern JUnit Test Case
 *
 * @generatedBy CodePro at 8/15/14 12:33 PM
 *
 * @author nayak_b
 *
 * @version $Revision$
 */
public class PrintNoticeServiceImplTest extends GhixBaseTest
{

	@Autowired private PrintNoticeService printNoticeService;
	
	/**
	 * Run the List<Notice> paperNoticeToPrintQueue(Date, Date) method test
	 */
	@Test
	public void testPaperNoticeToPrintQueue()
	{
		Date startDate = DateUtil.StringToDate("20140805000000", "yyyyMMddHHmmss");
		Date endDate = new TSDate();
		List<Notice> result = printNoticeService.paperNoticeToPrintQueue(startDate, endDate);
		
		Assert.assertNotNull(result);
	}
}

