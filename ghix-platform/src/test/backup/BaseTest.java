/**
 * 
 */
package resources_backup;

import org.apache.log4j.xml.DOMConfigurator;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author panda_p
 * @Created 11-04-2012 It is the base Test class for initializing application
 *          Context
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
@Ignore("This class does not contain any test methods")
public abstract class BaseTest {

	private static final Logger logger = LoggerFactory
			.getLogger(BaseTest.class);

	public static ApplicationContext context = null;

	@BeforeClass
	public static void setUp() {
		try{
		//Don't Check this setting. use this while testing from Eclipse.
		//System.setProperty("GHIX_HOME", "C:/workspace_ghix/");
		
		//This helps to load application context.
		context = new ClassPathXmlApplicationContext("/applicationContext.xml");
		
		//This helps to load log4j file
		//String rootPath = System.getProperty("GHIX_HOME");
		String rootPath = System.getenv("GHIX_HOME");
		
		DOMConfigurator.configure(rootPath + "/ghix-setup/conf/ghix-log4j.xml");
		}catch(Exception ex)
		{
			logger.error("",ex);
		}
	}

}
