package resources_backup;

import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Base test for spring-enabled junit tests.
 * 
 * <p>
 * JUnit tests should extend this class, to have ability to {@code @Autowire}
 * Spring's beans into their test cases as well as access database.
 * </p>
 * 
 * <p>
 * Child test classes should also be annotated with
 * {@code @TransactionConfiguration(defaultRollback = true)} so that there is no
 * actual changes to database performed during tests.
 * </p>
 * 
 * <p>
 * Some methods if they are connecting to database should be annotated with
 * {@code @Transactional(readOnly = true)} if they perform read-only queries or
 * {@code @Transactional} if they do create/update/delete queries on the
 * database.
 * </p>
 * 
 * <p>
 * This class tries to parse $CATALINA_HOME/conf/context.xml file to get
 * currently used JNDI Data Source properties, such as Database connection url,
 * username, password, etc.
 * </p>
 * <p>
 * Set your $CATALINA_HOME environment variable to a location under which
 * correct <code>conf/context.xml</code> file could be found and parsed. <br/>
 * If you are deploying to stand-alone Tomcat, then it should point to Tomcats
 * installation directory, such as C:\apache-tomcat-7.0.24 or
 * /home/user/apps/tomcat7 <br/>
 * If you are using Eclipse's VFabric Server, then CATALINA_HOME should point to
 * something similar to
 * <code>/home/jeneag/projects/workspaces/sts-3.8.2/Servers/VMware vFabric tc Server Developer Edition v2.9-config/context.xml</code>
 * To find out where context.xml is stored, in Eclipse project explorer view,
 * find 'Servers', expand VMware*vFabric/Tomcat (whichever you are deploying
 * to), right click context.xml -&gt; properties and look at the
 * <code>Location</code> property. Usually it sits somewhere within your
 * workspace.
 * </p>
 * 
 * <p>
 * When this class is bootstrapped, if $CATALINA_HOME variable contains string
 * vFabric, it will no longer prepend 'config/' directory to that path, but will
 * look for context.xml in supplied CATALINA_HOME directory directly.
 * </p>
 * 
 * @author Yevgen Golubenko
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationPlatform-test.xml" })
@Ignore("This is a base test, it doesn't have any test methods")
public class GhixBaseTest 
{
	private static Logger             log     = null;
	private static ApplicationContext context = null;

	/**
	 * Sets up JNDI datasource to be available to JUnit tests that
	 * extend this base test. If your unit tests initialize
	 * applicationContext.xml for example,
	 * JNDI datasource should be available to the beans defined in it.
	 */
	@BeforeClass
	public static void setup()
	{
		if (log == null)
		{
			log = LoggerFactory.getLogger(GhixBaseTest.class);
		}

		GhixJNDITestSetup.initJNDI();

		if (context == null)
		{
			System.out.println("Initializing Spring application context");

			try
			{
				context = new ClassPathXmlApplicationContext(
						new String[]
								{
								"classpath*:/applicationDB.xml"
								}, true);
			}
			catch (final Exception e)
			{
				System.err.println("Caught exception while initializing Spring Application Context: " + e.getMessage());
			}

			if (context != null)
			{
				System.out.format("Loaded %d spring beans", context.getBeanDefinitionCount());
				final String[] beanNames = context.getBeanDefinitionNames();

				for (final String s : beanNames)
				{
					System.out.println("Bean: " + s);
				}

				System.out.println("Spring application context initialized");
			}
			else
			{
				System.out.println("Spring application context FAILED to initialize");
			}
		}
	}

	/**
	 * Gets size of interable object.
	 * 
	 * @param it java.util.Iterable.
	 * @return number of elements in given iterable
	 */
	protected int iterableSize(final Iterable<?> it)
	{
		if (it instanceof Collection)
		{
			return ((Collection<?>) it).size();
		}
		// else iterate
		int i = 0;
		for (@SuppressWarnings("unused")
		final Object obj : it)
		{
			i++;
		}
		return i;
	}

	public Logger getLog()
	{
		return log;
	}

	@SuppressWarnings("rawtypes")
	public void setLogClass(final Class clazz)
	{
		log = LoggerFactory.getLogger(clazz);
	}

	/**
	 * Returns application context.
	 * 
	 * @return spring application context.
	 */
	public static ApplicationContext getApplicationContext()
	{
		return context;
	}
}
