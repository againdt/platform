/**
 * 
 */
package resources_backup;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp.datasources.SharedPoolDataSource;
import org.apache.xerces.parsers.DOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Contains dynamic JNDI setup methods that are used by the JUnit tests.
 * 
 * @author Yevgen Golubenko
 */
public class GhixJNDITestSetup
{
	public static final Logger	 log	        = LoggerFactory.getLogger(GhixJNDITestSetup.class);
	public static InitialContext	ic	        = null;
	public static String         DB_CONNECT_URL = "jdbc:oracle:thin:@ghixdevdb.com:1521:ghixdb";
	public static String	     DB_USERNAME	= "MS1_QA";
	public static String	     DB_PASSWORD	= "MS1_QA";
	public static String	     DB_JNDI_NAME	= "jdbc/ghixDS";
	public static String	     DB_JDBC_DRIVER	= "oracle.jdbc.OracleDriver";

	/*
	 * This will hold properties from context.xml file found under
	 * $CATALINA_HOME/$VFABRIC_HOME environment variable.
	 * If this will be resolved, these connection properties will be
	 * used instead of hard-coded values.
	 */
	public static Map<String, String> connInfo       = null;

	/**
	 * Finds context.xml and binds JNDI data source.
	 * Call this in order to setup <code>jdbc/ghixDS</code> data source
	 * that will be used by spring application context.
	 */
	public static void initJNDI()
	{
		connInfo = getCatalinaContextXml();

		bindJNDIDataSourceViaSpringContextBuilder();
		printJndiSetup();
	}


	public static void printJndiSetup()
	{
		final Map<String, String> defaultConnectionMap = new HashMap<>(5);
		defaultConnectionMap.put("CATALINA_HOME", System.getenv("CATALINA_HOME"));
		defaultConnectionMap.put("VFABRIC_HOME", System.getenv("VFABRIC_HOME"));
		defaultConnectionMap.put("DB CONNECTION URL", getDbConnectUrl());
		defaultConnectionMap.put("DB USERNAME", getDbUsername());
		defaultConnectionMap.put("DB PASSWORD", getDbPassword());
		defaultConnectionMap.put("DB JNDI NAME", getDbJndiName());
		defaultConnectionMap.put("DB JDBC DRIVER", getDbJdbcDriver());
		defaultConnectionMap.put("TOMCAT_HOME", System.getenv("TOMCAT_HOME"));
		defaultConnectionMap.put("GHIX_HOME", System.getenv("GHIX_HOME"));

		System.out.println("\n\n");
		System.out.println(" Unit Test JNDI Configuration");
		System.out.format("%-20.25s%-40.90s%n",
				"+----------------------+",
				"-----------------------------------------------------------------------------+");
		for (final String key : defaultConnectionMap.keySet())
		{
			System.out.format("| %-20.25s | %-40.90s%n", key, defaultConnectionMap.get(key));
		}

		System.out.format("%-20.25s%-40.90s%n",
				"+----------------------+",
				"-----------------------------------------------------------------------------+");
		System.out.println("\n\n");
	}

	/**
	 * Creates new JNDI Data Source and binds it to the system, so that
	 * inside of applicationContext.xml whenever we refer to jdbc/ghixDS
	 * it will be working.
	 */
	public static void bindJNDIDataSourceViaSpringContextBuilder()
	{
		try
		{
			final SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
			final OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();

			ds.setURL(getDbConnectUrl());
			ds.setUser(getDbUsername());
			ds.setPassword(getDbPassword());

			builder.bind(DB_JNDI_NAME, ds);
			builder.activate();
		}
		catch (final NamingException | SQLException e)
		{
			e.printStackTrace();

			log.error("bindJNDIDataSourceViaSpringContextBuilder() " +
					"NamingException occurred while registring JNDI " +
					getDbJndiName(), e);
		}
	}

	/**
	 * Returns key/value pairs from context.xml for jndi data bind
	 * 
	 * @return Map or null.
	 */
	public static Map<String, String> getCatalinaContextXml()
	{
		final String catalinaHome = System.getenv("CATALINA_HOME");
		final String vfabricHome = System.getenv("VFABRIC_HOME");
		final String tomcatHome = System.getenv("TOMCAT_HOME");

		// /home/jeneag/projects/workspaces/sts-3.8.2/Servers/VMware vFabric tc Server Developer Edition v2.9-config/context.xml

		final String xmlFileName = "context.xml";
		String xmlFilePath = "";

		System.out.println("  Using CATALINA_HOME: " + catalinaHome);
		System.out.println("         VFABRIC_HOME: " + vfabricHome);
		System.out.println("          TOMCAT_HOME: " + tomcatHome);

		// If we have VFABRIC_HOME, lets check it first...
		if (vfabricHome != null)
		{
			System.out.println("VFABRIC_HOME environment variable is set, checking for base-instance/conf/" + xmlFileName);
			xmlFilePath = vfabricHome + File.separatorChar + "base-instance" +
					File.separatorChar + "conf" + File.separatorChar + xmlFileName;
			final File f = new File(xmlFilePath);

			if (f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				final Document doc = parseXmlFile(xmlFilePath);

				if (doc != null)
				{
					final Map<String, String> kv = getMapFromDocument(doc);

					if (kv != null)
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}

		// are we supplied vfabric path?
		if (catalinaHome != null && catalinaHome.toLowerCase().contains("vfabric"))
		{
			System.out.println("CATALINA_HOME is pointing to Eclipse's vFabric instance: " + catalinaHome);
			xmlFilePath = catalinaHome + File.separatorChar + xmlFileName;

			final File f = new File(xmlFilePath);

			if (f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				final Document doc = parseXmlFile(xmlFilePath);

				if (doc != null)
				{
					final Map<String, String> kv = getMapFromDocument(doc);

					if (kv != null)
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}
		else if (catalinaHome != null && !catalinaHome.trim().equals(""))
		{
			System.out.println("CATALINA_HOME is pointing to Tomcat's instance: " + catalinaHome);
			xmlFilePath = catalinaHome + File.separatorChar + "conf" + File.separatorChar + xmlFileName;

			final File f = new File(xmlFilePath);

			if (f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				final Document doc = parseXmlFile(xmlFilePath);

				if (doc != null)
				{
					final Map<String, String> kv = getMapFromDocument(doc);

					if (kv != null)
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}
		else if (tomcatHome != null && !tomcatHome.trim().equals(""))
		{
			System.out.println("TOMCAT_HOME is pointing to directory with tomcat installation(s): " + tomcatHome);
			xmlFilePath = tomcatHome + File.separatorChar + "conf" + File.separatorChar + xmlFileName;

			final File f = new File(xmlFilePath);

			if (f.exists() && f.canRead())
			{
				System.out.println("Found configuration: " + xmlFilePath);
				final Document doc = parseXmlFile(xmlFilePath);

				if (doc != null)
				{
					final Map<String, String> kv = getMapFromDocument(doc);

					if (kv != null)
					{
						System.out.println("Using file for tests: " + xmlFilePath);
						return kv;
					}
				}
			}
		}
		else
		{
			System.out.println("Unable to find CATALINA_HOME, TOMCAT_HOME or VFABRIC_HOME environment variables!\n"
					+ "Falling back to default DB connection properties.");
		}

		return null;
	}

	/**
	 * Parses given XML file and returns Document object.
	 * 
	 * @param xmlFilePath absolute xml file path.
	 * @return Document object or null if unable to parse or exeption occurrs.
	 */
	private static Document parseXmlFile(final String xmlFilePath)
	{
		final DOMParser parser = new DOMParser();
		Document doc = null;

		try
		{
			parser.parse(xmlFilePath);

			doc = parser.getDocument();
		}
		catch (final Exception e)
		{
			log.error("Could not get Document from given XML file: " + xmlFilePath, e);
		}

		return doc;
	}

	/**
	 * Gets Map of XML attribute names/values from context.xml file.
	 * 
	 * @return {@link Map} object or null if context.xml file not found or
	 *         doesn't have &lt;Resource/&gt;
	 *         child defined.
	 */
	public static Map<String, String> getMapFromDocument(final Document doc)
	{
		if (doc != null)
		{
			System.out.println("Reading context.xml file and parsing relavent data into Map<String,String> ...");

			final NodeList root = doc.getChildNodes();
			final Node context = getNode("Context", root);

			if (context == null)
			{
				System.out.println("No <Context> node found, ignoring this context.xml");
				return null;
			}

			final Node resource = getNode("Resource", context.getChildNodes());

			if (resource == null)
			{
				System.out.println("No <Resource> node found, ignoring this context.xml");
				return null;
			}

			final Map<String, String> connectionMap = new HashMap<String, String>();

			connectionMap.put("auth", getNodeAttr("auth", resource));
			connectionMap.put("type", getNodeAttr("type", resource));
			connectionMap.put("url", getNodeAttr("url", resource));
			connectionMap.put("driver", getNodeAttr("driverClassName", resource));
			connectionMap.put("name", getNodeAttr("name", resource));
			connectionMap.put("username", getNodeAttr("username", resource));
			connectionMap.put("password", getNodeAttr("password", resource));
			connectionMap.put("removeAbandoned", getNodeAttr("removeAbandoned", resource));
			connectionMap.put("removeAbandonedTimeout", getNodeAttr("removeAbandonedTimeout", resource));
			connectionMap.put("logAbandoned", getNodeAttr("logAbandoned", resource));
			connectionMap.put("maxActive", getNodeAttr("maxActive", resource));
			connectionMap.put("maxIdle", getNodeAttr("maxIdle", resource));
			connectionMap.put("minIdle", getNodeAttr("minIdle", resource));
			connectionMap.put("maxWait", getNodeAttr("maxWait", resource));

			if (connectionMap.containsKey("driver") && connectionMap.containsKey("type") &&
					connectionMap.containsKey("name") && connectionMap.containsKey("url"))
			{
				return connectionMap;
			}
		}

		return null;
	}

	/**
	 * Builds and activates JNDI datasource namespace using catalina's url
	 * context factory.
	 * 
	 * @deprecated Use {@link #bindJNDIDataSourceViaSpringContextBuilder()}
	 */
	@Deprecated
	protected static void bindJNDIDataSource()
	{
		try
		{
			System.out.println("GetinsuredSpringTest.setup() Binding JNDI DataSource");
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");

			ic = new InitialContext();
			ic.createSubcontext("jdbc");
			ic.createSubcontext("jdbc/ghixDS");

			final OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
			ds.setURL(getDbConnectUrl());
			ds.setUser(getDbUsername());
			ds.setPassword(getDbPassword());

			//ic.bind(DB_JNDI_NAME, ds);
			ic.rebind(getDbJndiName(), ds);
			log.info("bindJNDIDataSource() Finished bying JNDI data source: " + ic.getNameInNamespace());
		}
		catch (final NamingException ne)
		{
			ne.printStackTrace();
			log.error("NamingException occurred while registring JNDI jdbc/ghixDS", ne);
		}
		catch (final SQLException sex)
		{
			sex.printStackTrace();
			log.error("SQLException occurred while creating OracleConnectionPoolDataSource()", sex);
		}
	}

	/**
	 * Uses driver adapter to construct new JNDI datasource.
	 * 
	 * @throws ClassNotFoundException
	 */
	protected static void bindJNDIDataSourceViaDriverAdapter()
	{
		try
		{
			final SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
			final DriverAdapterCPDS cpds = new DriverAdapterCPDS();
			cpds.setDriver(getDbJdbcDriver());
			cpds.setUser(getDbUsername());
			cpds.setPassword(getDbPassword());
			cpds.setUrl(getDbConnectUrl());

			final SharedPoolDataSource dataSource = new SharedPoolDataSource();
			dataSource.setConnectionPoolDataSource(cpds);
			dataSource.setMaxActive(2);
			dataSource.setMaxWait(60);

			// Activate this DataSource
			builder.bind(getDbJndiName(), dataSource);
			builder.activate();
		}
		catch (final NamingException ne)
		{
			ne.printStackTrace();
			log.error("bindJNDIDataSourceViaDriverAdapter() NamingException occurred while registring JNDI jdbc/ghixDS", ne);
		}
		catch (final ClassNotFoundException cnf)
		{
			cnf.printStackTrace();
			log.error("bindJNDIDataSourceViaDriverAdapter() Cannot create oracle.jdbc.OracleDriver (with Class.forName())", cnf);
		}
	}

	/**
	 * Gets attribute from given node.
	 * 
	 * @param attrName name of the attribute.
	 * @param node node to read attribute from.
	 * @return node value or empty string.
	 */
	public static String getNodeAttr(final String attrName, final Node node)
	{
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			final NamedNodeMap attrs = node.getAttributes();

			for (int y = 0; y < attrs.getLength(); y++)
			{
				final Node attr = attrs.item(y);

				if (attr.getNodeName().equalsIgnoreCase(attrName))
				{
					return attr.getNodeValue();
				}
			}
		}

		return "";
	}

	/**
	 * Gets attribute from given node list.
	 * 
	 * @param tagName name of the tag.
	 * @param attrName attribute name.
	 * @param nodes list of nodes.
	 * @return node value or empty string.
	 */
	public static String getNodeAttr(final String tagName, final String attrName, final NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			final Node node = nodes.item(x);

			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				final NodeList childNodes = node.getChildNodes();

				for (int y = 0; y < childNodes.getLength(); y++)
				{
					final Node data = childNodes.item(y);

					if (data.getNodeType() == Node.ATTRIBUTE_NODE)
					{
						if (data.getNodeName().equalsIgnoreCase(attrName))
						{
							return data.getNodeValue();
						}
					}
				}
			}
		}

		return "";
	}

	/**
	 * Returns node from given node list.
	 * 
	 * @param tagName tag name of the node thats needed.
	 * @param nodes list of nodes where this node is placed
	 * @return Node or null.
	 */
	public static Node getNode(final String tagName, final NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			final Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				return node;
			}
		}

		return null;
	}

	/**
	 * Returns node text value from given node.
	 * 
	 * @param node node that contains text, e.g. &lt;name&gt;Cousin
	 *            Vinny&lt;/name&gt;
	 * @return Cousin Vinny
	 */
	public static String getNodeValue(final Node node)
	{
		final NodeList childNodes = node.getChildNodes();
		for (int x = 0; x < childNodes.getLength(); x++)
		{
			final Node data = childNodes.item(x);
			if (data.getNodeType() == Node.TEXT_NODE)
			{
				return data.getNodeValue();
			}
		}
		return "";
	}

	/**
	 * Returns text node value.
	 * 
	 * @param tagName tag name of the node.
	 * @param nodes list of nodes.
	 * @return text node value or empty string.
	 */
	public static String getNodeValue(final String tagName, final NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			final Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				final NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++)
				{
					final Node data = childNodes.item(y);
					if (data.getNodeType() == Node.TEXT_NODE)
					{
						return data.getNodeValue();
					}
				}
			}
		}
		return "";
	}

	/**
	 * Returns database connection url.
	 * 
	 * @return database connection url.
	 */
	public static String getDbConnectUrl()
	{
		if (connInfo != null && connInfo.containsKey("url"))
		{
			return connInfo.get("url");
		}

		return DB_CONNECT_URL;
	}

	/**
	 * Returns database username.
	 * 
	 * @return username to connect to database.
	 */
	public static String getDbUsername()
	{
		if (connInfo != null && connInfo.containsKey("username"))
		{
			return connInfo.get("username");
		}

		return DB_USERNAME;
	}

	/**
	 * Returns database password.
	 * 
	 * @return password to be used to connect to database.
	 */
	public static String getDbPassword()
	{
		if (connInfo != null && connInfo.containsKey("password"))
		{
			return connInfo.get("password");
		}

		return DB_PASSWORD;
	}

	/**
	 * Returns JNDI name to bind to the system for database access.
	 * 
	 * @return JNDI name.
	 */
	public static String getDbJndiName()
	{
		if (connInfo != null && connInfo.containsKey("name"))
		{
			return connInfo.get("name");
		}

		return DB_JNDI_NAME;
	}

	/**
	 * Returns JDBC driver used for database connection.
	 * 
	 * @return jdbc driver class name (as string) used for database connection.
	 */
	public static String getDbJdbcDriver()
	{
		if (connInfo != null && connInfo.containsKey("driver"))
		{
			return connInfo.get("driver");
		}

		return DB_JDBC_DRIVER;
	}

	/**
	 * For testing pursposes.
	 * 
	 * @param args command line arguments.
	 */
	public static void main(final String[] args)
	{
		printJndiSetup();
	}
}
