/**
 * 
 */
package resources_backup;

import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.getinsured.hix.model.LookupValue;
import resources_backup.GhixBaseTest;

import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.lookup.service.jpa.LookupServiceImpl;

/**
 * Class to unit test LookupService methods.
 * @author chalse_v
 *
 */
 @Ignore (value = "to avoid stop running the tests due to failed junit")
public final class LookupServiceTest extends GhixBaseTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(LookupServiceImpl.class);

	@Autowired
	private LookupService lookupService;
	
	/**
	 * Method to unit test 'getLookupValueList()' method.
	 */
	@Test
	@Transactional(readOnly = true)
	public void getLookupValueListTest() {
		LOGGER.info("getLookupValueListTest : START");
		
		Assert.notNull(lookupService, "LookupService is not bound.");
		
		List<LookupValue> educationList = lookupService.getLookupValueList("AGENT_EDUCATION");
		
		Assert.notNull(educationList, "No education list collection found for Agent.");
		Assert.notEmpty(educationList, "No education data found for Agent");

		LOGGER.info("getLookupValueListTest : END");
	}
	
	@Test
	@Transactional(readOnly = true)
	public void populateLanguageNamesTest() {
		LOGGER.info("populateLanguageNamesTest : START");
		
		Assert.notNull(lookupService, "LookupService is not bound.");
		
		List<String> languageNameList = lookupService.populateLanguageNames(null);
		
		Assert.notNull(languageNameList, "No language name list collection found for Agent.");
		Assert.notEmpty(languageNameList, "No language name data found for Agent");

		LOGGER.info("populateLanguageNamesTest : END");
	}
}
