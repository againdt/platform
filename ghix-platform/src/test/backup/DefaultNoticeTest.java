package resources_backup;

import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Notice;
import resources_backup.GhixBaseTest;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.UserService;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * The class <code>DefaultNoticeTest</code> contains tests for the class {@link
 * <code>DefaultNotice</code>}
 *
 * @pattern JUnit Test Case
 *
 * @generatedBy CodePro at 5/27/14 9:31 PM
 *
 * @author nayak_b
 *
 * @version $Revision$
 */
public class DefaultNoticeTest extends GhixBaseTest {

	private static final String EMAIL = "exadmin@ghix.com";
	private static final String ECMPATH = "1";
	@Autowired private NoticeService noticeService;
	@Autowired private UserService userService;
	@Autowired private  ApplicationContext appContext;
	private final String fileName = "Notice-Test-BK"+new TSDate().getTime()+".pdf";
	
	//@Rule public ExpectedException thrown= ExpectedException.none();
	
	@Test
	public void testCreateNotice() {
		
		AccountUser user = userService.findByEmail(EMAIL);
		String ecmPath = ECMPATH;
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ecmPath , fileName, user);
			System.out.println("Sucess-"+notice);
			assertNotNull(notice);	
			
		} catch (Exception e) {
			Assert.assertTrue(true);
			e.printStackTrace();
		}
			
	}
	
	
	
	@Test
	public void testCreateNoticeWithLocationAndMailPreff() {
		
		AccountUser user = userService.findByEmail(EMAIL);
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		Location location = populateLocation();
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ECMPATH ,
					fileName, user,location,GhixNoticeCommunicationMethod.Mail);
			System.out.println(notice);
			assertNotNull(notice);
		
		} catch (Exception e) {
			Assert.assertTrue(true);
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test(expected=IllegalArgumentException.class)
	public void testCreateNoticeWithLocationAndMailPreffNoUser() {
		AccountUser user = null;
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		Location location = populateLocation();
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ECMPATH ,
					fileName, user,location,GhixNoticeCommunicationMethod.Mail);
			System.out.println(notice);
			assertNotNull(notice);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateNoticeWithLocationAndEMailPreff() {
		
		AccountUser user = userService.findByEmail(EMAIL);
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		Location location = populateLocation();
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ECMPATH ,
					fileName, user,location,GhixNoticeCommunicationMethod.Email);
			System.out.println(notice);
			assertNotNull(notice);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testCreateNoticeWithLocationAndEMailPreffNoUser() {
		
		AccountUser user = null;
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		Location location = populateLocation();
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ECMPATH ,
					fileName, user,location,GhixNoticeCommunicationMethod.Email);
			System.out.println(notice);
			assertNotNull(notice);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testCreateNoticeWithLocationNoPreff() {
		
		AccountUser user = userService.findByEmail(EMAIL);
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		Location location = populateLocation();
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ECMPATH ,
					fileName, user,location,null);
			System.out.println(notice);
			assertNotNull(notice);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testCreateNoticeWithLocationNoPreffNoUser() {
		
		AccountUser user = null;
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		Location location = populateLocation();
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Notice notice = noticeService.createNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject, ECMPATH ,
					fileName, user,location,null);
			System.out.println(notice);
			assertNotNull(notice);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testCreateModuleNotice() {
		
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			List<ModuleUser> mList = userService.getModuleUsers(1, "employer");
			ModuleUser moduleUser = mList.get(0);
			Notice notice = noticeService.createModuleNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject,
					ECMPATH , fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					"toFullName");
			System.out.println(notice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testCreateModuleNoticeWithLocationAndMailPreffNoValidUser() {
		
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Location location = populateLocation();
			Notice notice = noticeService.createModuleNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject,
					ECMPATH , fileName, null, -00000, email, 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					"toFullName",location,GhixNoticeCommunicationMethod.Mail);
			System.out.println(notice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateModuleNoticeWithLocationAndMailPreff() {
		
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Location location = populateLocation();
			List<ModuleUser> mList = userService.getModuleUsers(1, "employer");
			ModuleUser moduleUser = mList.get(0);
			Notice notice = noticeService.createModuleNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject,
					ECMPATH , fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					"toFullName",location,GhixNoticeCommunicationMethod.Mail);
			System.out.println(notice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateModuleNoticeWithLocationAndEmailPreff() {
		
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Location location = populateLocation();
			List<ModuleUser> mList = userService.getModuleUsers(1, "employer");
			ModuleUser moduleUser = mList.get(0);
			Notice notice = noticeService.createModuleNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject,
					ECMPATH , fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					"toFullName",location,GhixNoticeCommunicationMethod.Email);
			System.out.println(notice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateModuleNoticeWithLocationAndEmptyPreff() {
		
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Location location = populateLocation();
			List<ModuleUser> mList = userService.getModuleUsers(1, "employer");
			ModuleUser moduleUser = mList.get(0);
			Notice notice = noticeService.createModuleNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject,
					ECMPATH , fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					"toFullName",location,null);
			System.out.println(notice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateModuleNoticeWithLocationAndEmptyPreffNoValidUser() {
		
		List<String> email = new ArrayList<String>();
		email.add(EMAIL);
		try {
			Map<String, Object> replaceableObject = populateReplacable();
			Location location = populateLocation();
			Notice notice = noticeService.createModuleNotice("NMBrokerCertificationNotification", GhixLanguage.US_EN, replaceableObject,
					ECMPATH , fileName, null, -00000, null, 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					"toFullName",location,null);
			System.out.println(notice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	private Location populateLocation() {
		Location location = new Location();
		location.setAddress1("118 college drive");
		//location.setAddress2("bbbbbb");
		location.setCity("hattiesburg");
		location.setZip("39201");
		return location;
	}
	
	private Map<String, Object> populateReplacable() throws Exception {
		Map<String, Object> replaceableObject = new HashMap<String, Object> ();
		
		replaceableObject.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		replaceableObject.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		replaceableObject.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
		replaceableObject.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		//replaceableObject.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		//replaceableObject.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		//replaceableObject.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		//replaceableObject.put(TemplateTokens.HEADER_CONTENT, getTemplateHeaderOrFooter("notificationTemplate/emailTemplateHeader.html"));
		//replaceableObject.put(TemplateTokens.FOOTER_CONTENT, getTemplateHeaderOrFooter("notificationTemplate/emailTemplateFooter.html"));
		replaceableObject.put("notificationDate","23-may-2014");
		replaceableObject.put("BrokerName","BrokerNameExp");
		replaceableObject.put("BrokerCompanyAddress","address");
		replaceableObject.put("priorCertificationStatus","pending");
		replaceableObject.put("newCertificationStatus","success");
		replaceableObject.put("StatusChangeComment","weldone");
		
	
		
		return replaceableObject;
	}
	
	private String getTemplateHeaderOrFooter(String templateLocation) throws  NoticeServiceException {
		InputStream inputStreamUrl = null;
		try {
			Resource resource = 
			           appContext.getResource("classpath:"+templateLocation);
			inputStreamUrl = resource.getInputStream();			
			return populatreHeaderFooterTemplate(IOUtils.toString(inputStreamUrl, "UTF-8"));			
		} catch (FileNotFoundException fnfe) {
			throw new NoticeServiceException(fnfe);
		} catch (IOException ioe) {
			throw new NoticeServiceException(ioe);
		}
		catch (Exception e) {
			throw new NoticeServiceException(e);
		}
		finally
		{
			IOUtils.closeQuietly(inputStreamUrl);			
		}	
	}
	
	private String populatreHeaderFooterTemplate(String templateContent) throws NoticeServiceException
	{
		Map<String, Object> replaceableObj = new HashMap<String, Object> ();		
		replaceableObj.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));		
		replaceableObj.put(TemplateTokens.HOST, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		replaceableObj.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));	
		replaceableObj.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		replaceableObj.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		replaceableObj.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		replaceableObj.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
		replaceableObj.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		replaceableObj.put(TemplateTokens.STATE_CODE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		replaceableObj.put(TemplateTokens.CITY_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		replaceableObj.put(TemplateTokens.PIN_CODE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		replaceableObj.put(TemplateTokens.EXCHANGE_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		
		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			tmpl.process(replaceableObj, sw);
			
		} catch (Exception e) {
			throw new NoticeServiceException(e);
		}
		finally{
			IOUtils.closeQuietly(sw);
		}		
		return sw.toString();
	}	
}

/*$CPS$ This comment was generated by CodePro. Do not edit it.
 * patternId = com.instantiations.assist.eclipse.pattern.testCasePattern
 * strategyId = com.instantiations.assist.eclipse.pattern.testCasePattern.junitTestCase
 * additionalTestNames = 
 * assertTrue = false
 * callTestMethod = true
 * createMain = false
 * createSetUp = false
 * createTearDown = false
 * createTestFixture = false
 * createTestStubs = false
 * methods = 
 * package = com.getinsured.hix.platform.notices.jpa
 * package.sourceFolder = ghix-platform/src/main/java
 * superclassType = resources_backup.GhixBaseTest
 * testCase = DefaultNoticeTest
 * testClassType = com.getinsured.hix.platform.notices.jpa.DefaultNotice
 */
