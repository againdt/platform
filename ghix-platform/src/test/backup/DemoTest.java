/**
 * 
 */
package resources_backup;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
//import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import resources_backup.GhixBaseTest;

/**
 * Demo test, minimal configuration needed for the test. 
 * Each sample test method is documented, you can pick any 
 * as your starting point.
 * 
 * Comment out @Ignore annotation, so you can launch this test with JUnit (but don't commit this change)
 * The reason for @Ignore here, is when we do mvn test, it will report it as unsuccessfull 
 * build because some methods here explicitely made to FAIL.
 * 
 * @author Yevgen Golubenko
 */
//@Ignore("I'm going to be ignored, because I'm just an example.")
@TransactionConfiguration(defaultRollback = true)
public class DemoTest extends GhixBaseTest
{
	/**
	 * Executed before each test method. 
	 * This is a good place to pre-initialize anything that your subsequent 
	 * methods may need.
	 */
	@Before
	public void beforeEachTestMethod()
	{
		System.out.println("@I'm executed BEFORE each test in this class.\n{");
	}

	/**
	 * This executed after each test method. 
	 * Good place to put your 'clean-up' code, to release/close/cleanup/reset
	 * anything that maybe have been initialized in {@link #beforeEachTestMethod()} 
	 * or in your tests.
	 */
	@After
	public void afterEachTestMethod()
	{
		System.out.println("}\n@I'm executed AFTER each test in this method.\n\n");
	}

	/**
	 * Simpliest method.
	 */
	@Test
	public void successTest()
	{
		System.out.println("I'm a simpliest test ever.");
		Assert.assertEquals(1,1);
	}

	/**
	 * Simple test with timeout property added.
	 * 
	 * Test will be marked as failed if it takes longer then 2 seconds to run.
	 */
	@Test(timeout = 1000 * 2)
	public void testWithTimeout()
	{
		System.out.println("I will be executed because I'm quick and @Test(timeout=...) will no kick in!");
		Assert.assertEquals(1, 1);
	}

	/**
	 * Test that is given 1 ms to execute, it may throw InterruptedException
	 * (this exception can be &quot;expected&quot; by the test if expected = InterruptedException added
	 * to the @Test annotation.
	 */
	// uncomment to fail @Test(timeout = 1)
	public void testWithTimeoutFail() throws InterruptedException
	{
		System.out.println("I'm going to sleep for 2ms but will fail because timeout = 1ms");
		Thread.sleep(2);
	}

	/**
	 * This method will be executed by JUnit and will be ignored
	 * and logged with specified reason message (if available).
	 * 
	 * Supply reason why it's ignored, for example if you are waiting
	 * to implement some other things before making this test a part 
	 * of your test plan, etc.
	 */
	@Test
	//@Ignore("Reason for ignoring it: because it's just a demo")
	public void testWillBeIgnoredByJUnit()
	{
		System.out.println("I will not be executed at all.");
		//Assert.fail("I will not fail, because I'm ignored ;)");
	}

	/**
	 * Test that queries Database.
	 * <p> 
	 * Total time for this test is limited by 2 seconds,
	 * this test will query DB with as read-only and allowed 1 second
	 * for transaction to complete.
	 * </p>
	 */
	@Test(timeout = 2000)
	@Transactional(readOnly = true, timeout = 1000)
	public void testMethodThatQueriesDatabase()
	{
		System.out.println("I will be quering database, so I mark myself with @Transactional annotation and readOnly = true flag");
		System.out.println("I'm also thinking to be done with my transaction within 1 second, so I mark myself with timeout=1000 as well.");
		Assert.assertEquals(1,1);
	}

	/**
	 * Test that updates database. 
	 * 
	 * <p>
	 * Use this test when you testing something that updates database. Transactions
	 * will be rolled back, so no actual modifications in the database will be saved.
	 * </p>
	 * <p>
	 * Even with rollback your tests will fail if SQL exception is thrown, SQL is invalid, etc.,
	 * so it's a good starting point for the test cases like this.
	 * </p>
	 */
	@Test(timeout = 1500)
	@Transactional(readOnly = false, timeout = 1000)
	public void testMethodThatUpdatesDatabase()
	{
		System.out.println("I'm going to create/update/delete records from the database, " +
				"but it will do no harm, because my actions are rolled back when this test is finished");

		Assert.assertEquals(1,1);
	}
}











