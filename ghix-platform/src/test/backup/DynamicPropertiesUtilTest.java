/**
 * 
 */
package resources_backup;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration.SecurityConfigurationEnum;

/**
 * Test case for {@DynamicPropertiesUtil}
 * 
 * @author Yevgen Golubenko
 */
@TransactionConfiguration(defaultRollback = true)
public class DynamicPropertiesUtilTest extends GhixBaseTest 
{
	private static Log log = LogFactory.getLog(DynamicPropertiesUtilTest.class);
	private static final String PROP_GLOBAL_STATECODE = "global.StateCode";

	@Test
	public void getPropertyValue()
	{
		log.info("getting property value from DynamicPropertiesUtil: " + PROP_GLOBAL_STATECODE);
		final String stateCodeValue = DynamicPropertiesUtil.getPropertyValue(PROP_GLOBAL_STATECODE);
		assertNotNull("Could not get dynamic property", stateCodeValue);
	}

	@Test
	public void getPropertyValueByEnum()
	{
		log.info("getting property value from DynamicPropertiesUtil by Enum (subclass of PropertiesEnumMarker)");
		final String issuer = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ISSUER);
		assertNotNull("Could not get property with enum SecurityConfiguration.SecurityConfigurationEnum.ISSUER", issuer);
	}

	/**
	 * TODO: ygolubenko: while this test should REALLY fail, because not all keys are defined in the database
	 * for {@link SecurityConfigurationEnum}, but since we return '' (blank) values for NULL records
	 * and throw soft exception, I hope that would be enough for developers to go back and fix it if needed.
	 */
	@Test
	@Transactional(readOnly = true)
	public void getPropertyValuesForAllSecurityConfigurationEnums()
	{
		log.info("Getting all property values for SecurityConfiguration.SecurityConfigurationEnum.values()");

		for(final SecurityConfiguration.SecurityConfigurationEnum e : SecurityConfiguration.SecurityConfigurationEnum.values())
		{
			final String value = DynamicPropertiesUtil.getPropertyValue(e);

			if(value == null)
			{
				log.error("Property value for enum: " + e + " was <NULL>");
				assertNotNull("Property value for enum: " + e + " was <NULL>", value);
			}
		}
	}
}
