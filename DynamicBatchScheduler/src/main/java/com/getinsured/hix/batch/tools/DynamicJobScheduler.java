package com.getinsured.hix.batch.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;

import net.redhogs.cronparser.CronExpressionDescriptor;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

public class DynamicJobScheduler {
	private static String job_name_str = "Name of the job to be rescheduled:";
	private static String job_cron_exp_str = "Cron Expression to schedule the job:"; 
	private static String user_name_str = "\nUserName:";
	private static String user_pwd_str = "\nPassword:";
	private static String key_str = "\nKeystore password:";
	private static String server_url_str = "\nServer URL:";
	
	private static String continue_scheduling ="Job Scheduled successfully. Schedule another job?[y/n]";
	private static String cron_ok = "Is Cron expression ok?[y/n]";
	private static CloseableHttpClient httpclient = null;
	private static String hostName = null;
	private static String serverURL = null;
	private static BasicCookieStore cookieStore = null;
	private static String getUserInput(String paramName, String defaultVal)
			throws IOException {
		boolean inputProvided = false;
		String val = null;
		while (!inputProvided) {
			if (paramName != null) {
				System.out.print(paramName
						+ "::"
						+ ((defaultVal == null) ? ("   ") : ("[" + defaultVal)
								+ "]   "));
			}
			val = new BufferedReader(new InputStreamReader(System.in))
			.readLine();
			if (val == null || val.length() == 0) {
				if (defaultVal != null) {
					System.out.println("");
					return defaultVal;
				} else {
					continue;
				}
			}
			inputProvided = true;
			//	System.out.println(paramName + " = " + val);
		}
		if (val.equalsIgnoreCase("\\q")) {
			System.out.println("Exiting DynamicJobScheduler.......Bye");
			if(httpclient != null){
				try {
					httpclient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.exit(0);
		}
		return val;
	}
	public static void main(String[] args) throws IOException {

		String userName = getUserInput(user_name_str, null);
		String password = getUserInput(user_pwd_str,null);
		String key = getUserInput(key_str, null);
		serverURL = getUserInput(server_url_str, null);
		Pattern p = Pattern.compile("(http://|https://)(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?");
	    Matcher matcher = p.matcher(serverURL);
	    
		while(serverURL == null || !matcher.matches()){
			System.out.println("Please provide a valid server URL (Example: https://nm1dev.ghixqa.com)");
			serverURL = getUserInput(server_url_str, null);
			matcher = p.matcher(serverURL);
		}
		
		System.out.println("Authenticating user :  " + userName);
		
		while(!initHttpClient(key)){
			System.out.println("Invalid key. Please provide a valid keystore password");
			key = getUserInput(key_str, null);
		}
		
		while(!authenticateUser(userName, password)){
			System.out.println("Authentication failed. Please provide the valid user credentials again:");
			userName = getUserInput(user_name_str, null);
			password = getUserInput(user_pwd_str,null);
			
			
			if(httpclient != null){
				try {
					httpclient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				httpclient = null;
			}
			initHttpClient(key);
		}
		
		System.out.println(userName + " authenticated.");
		System.out.println("\nGetting the list of jobs configured on server: ");
		
		List<String> jobList = getBatchJobList();
		String continueSchedule = "y";
		
		
		while(continueSchedule.compareToIgnoreCase("y") == 0){
			String jobName = getUserInput(job_name_str, null);
			
			if(jobList.indexOf(jobName.trim()) < 0){
				System.out.println("Invalid job name, Enter the name again.");
				continue;
			}
			
			
			if(!getBatchJobSchedule(jobName)){
				System.out.println("The schedule for this job is not availabale. Try another job.");
				continue;
			}
			
			String cronExp = "";
			String cronCheck = "n";
			while(cronCheck.compareToIgnoreCase("n") == 0){
				cronExp = getUserInput(job_cron_exp_str, null);
				try {
					cronExp = cronExp.trim();
					System.out.println("The job will be scheduled " + CronExpressionDescriptor.getDescription(cronExp));
					cronCheck = getUserInput(cron_ok, "y");
				} catch (Exception e) {
					System.out.println("Invalid cron expression, Enter again.");
				}
			}
			if(scheduleJob(jobName, cronExp)){	
				continueSchedule = getUserInput(continue_scheduling, "n");
			}else{
				System.out.println("Error in scheduling the job");
				break;
			}
		}
		System.exit(0);
	}


	private static boolean getBatchJobSchedule(String jobName) {
		
		StringBuilder stUri = new StringBuilder();
		boolean bRet = true;
		if(hostName == null){
			stUri.append("http://localhost:8080/ghix-batch/getBatchJobSchedule?");
		}else{
			stUri.append(serverURL + "/ghix-batch/getBatchJobSchedule?");
		}
		jobName = jobName.trim();
		stUri.append("jobName=" + jobName);
		
		HttpResponse response = doGetRequest(stUri.toString());
		StringBuilder stJobSchedule = new StringBuilder();
		BufferedReader br;
		try {
			String output;
			br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((output = br.readLine()) != null) {
				stJobSchedule.append(output);
			}
			br.close();

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if(stJobSchedule.length() != 0){
				System.out.println("This job is presently scheduled at : " + stJobSchedule.toString() + "\t(" + CronExpressionDescriptor.getDescription(stJobSchedule.toString()) + ")");
			}else{
				bRet = false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return bRet;
	}
	private static boolean authenticateUser(String userName, String j_password){
		boolean bAuthenticated = false;
		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			hostName = localMachine.getCanonicalHostName();
			//System.out.println("Hostname of local machine: " + hostName);
		} catch (UnknownHostException e) {
			System.out.println("UnknownHostException occured" + e.getMessage());
		}
		
		StringBuilder stAuthUrl = new StringBuilder();
		
		if(hostName == null){
			stAuthUrl.append("http://localhost:8080/ghix-batch/j_spring_security_check");
		}else{
			stAuthUrl.append(serverURL + "/ghix-batch/j_spring_security_check");
		}
		
		ArrayList<NameValuePair> postParameters;
	    HttpPost httppost;

	    try {
			httppost = new HttpPost (stAuthUrl.toString());
			postParameters = new ArrayList<NameValuePair>();
		    postParameters.add(new BasicNameValuePair("j_username", userName));
		    postParameters.add(new BasicNameValuePair("j_password", j_password));

		    httppost.setEntity(new UrlEncodedFormEntity(postParameters));
		    HttpResponse postResponse = null;
			//postResponse = httpclient.execute(postRequest);
			 postResponse = httpclient.execute(httppost);

			if(postResponse != null){
				Header[] headers = postResponse.getHeaders("Location");
	
				if(headers != null && headers.length > 0){
					String header = headers[0].getValue();
					if(header.contains("loginSuccess")){
						bAuthenticated = true;
					}
				}
			}
		} catch (IOException  e1) {
			System.out.println("Exception caught in authenticateUser : " + e1.getMessage());
		} 		
		
		return bAuthenticated;
	}

	public static List<String> getBatchJobList() {
		StringBuilder stUri = new StringBuilder();
		
		if(hostName == null){
			stUri.append("http://localhost:8080/ghix-batch/getBatchJobList");
		}else{
			stUri.append(serverURL + "/ghix-batch/getBatchJobList");
		}
		HttpResponse response = doGetRequest(stUri.toString());
		StringBuilder stJobNames = new StringBuilder();
		BufferedReader br;
		try {
			String output;
			br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((output = br.readLine()) != null) {
				stJobNames.append(output);
			}
			br.close();

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String [] strings = stJobNames.toString().split(",");
		List<String> jobs = Arrays.asList(strings);
		System.out.println(jobs.toString().replaceAll(",", "\n"));
		return jobs;
	}

	public static boolean scheduleJob(String jobName, String cronEx) {
		String encodedCronExp = "";
		boolean status = false;
		
		try {
			encodedCronExp = URLEncoder.encode(cronEx, "UTF-8");
			StringBuilder stUri = new StringBuilder();
			
			if(hostName == null){
				stUri.append("http://localhost:8080/ghix-batch/scheduleJob?");
			}else{
				stUri.append(serverURL + "/ghix-batch/scheduleJob?");
			}
			
			
			stUri.append("jobName=" + jobName + "&");
			stUri.append("cronExp=" + encodedCronExp);

			
			HttpResponse response = doGetRequest(stUri.toString());
			System.out.println("URI: " + stUri.toString());

			if(response != null){
				try {
					new InputStreamReader(response.getEntity().getContent()).close();
					status = true;
				}  catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (UnsupportedEncodingException e1) {
		
			e1.printStackTrace();
		}
		
		
		return status;
	}

	private static boolean initHttpClient(String key){
		boolean initSuccess = false;
		try{
			if(httpclient == null){
				InputStream instream = null;

				KeyStore trustStore = null;
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

				String ghix_home = System.getenv("GHIX_HOME");
				
				if(ghix_home == null){
					ghix_home = "/home/tomcat/ghixhome";
				}
				instream = new FileInputStream(getKeyStoreFile(ghix_home + "/ghix-setup/conf", "platformKeyStore.jks"));
				
				trustStore.load(instream, key.toCharArray());
				System.out.println("Done creating trust store...");

				SSLContext sslContext = SSLContexts.custom()
						.useTLS()
						.loadTrustMaterial(trustStore)
						.build();
				SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
				httpclient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
				initSuccess = true;
			}
		}catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Exception caught in initHttpClient");
		}
		return initSuccess;
	}
	private static File getKeyStoreFile(String location,String file){
		System.out.println("Keystore Location:"+location);
		File keyStoreDir = new File(location);
		File keyStore = new File(keyStoreDir,file);
		if(!keyStore.exists() || !keyStore.isFile()){
			System.out.println("Key store ["+keyStore.getAbsolutePath()+"] does not exist Or its not a file");
			keyStore = null;
		}
		return keyStore;
	}
	private static HttpResponse doGetRequest(String uri){
		HttpGet request = new HttpGet(uri);
		HttpResponse response = null;
		try {
			response = httpclient.execute(request);
			// Printing the response
		/*	System.out.println("printing response :  ");
			System.out.println(response);
			System.out.println("error code :- "
					+ response.getStatusLine().getStatusCode());
*/
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

}

