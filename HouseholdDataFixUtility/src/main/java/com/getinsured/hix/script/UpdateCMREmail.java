package com.getinsured.hix.script;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CmrHouseholdRepository;
import com.getinsured.hix.model.Enrollee;
import com.getinsured.hix.model.EnrolleeAud;
import com.getinsured.hix.model.EnrolleeAudRepository;
import com.getinsured.hix.model.EnrolleeRepository;
import com.getinsured.hix.model.Enrollment;
import com.getinsured.hix.model.EnrollmentRepository;
import com.getinsured.hix.model.Household;
import com.getinsured.hix.model.SSAPAppRepository;
import com.getinsured.hix.model.SSAPApplicantsRepository;
import com.getinsured.hix.model.SsapApplicant;
import com.getinsured.hix.model.SsapApplication;

@SpringBootApplication
@EnableJpaRepositories(basePackages= {"com.getinsured.hix.model"})
@EntityScan(basePackages = {"com.getinsured.hix.model"})
@Configuration
public class UpdateCMREmail {
	private static final Logger log = LoggerFactory.getLogger(UpdateCMREmail.class);
    public static boolean updateDB = false;
	public static void main(String[] args) {
		if(args != null && args.length >= 2){
			if(args[0].equals("-update")){
				String input = args[1];
				UpdateCMREmail.updateDB = input.equalsIgnoreCase("true");
			}
		}
		if(!UpdateCMREmail.updateDB){
			log.info("Executing DB check in read only mode, please use \"-update true\" to make the changes");
		}
		SpringApplication.run(UpdateCMREmail.class);
	}
	
	@Bean
	public CommandLineRunner run(EntityManager em, CmrHouseholdRepository repository, EnrollmentRepository enrollmentRepo, SSAPAppRepository ssapRepo, SSAPApplicantsRepository ssapApplicantsRepo, EnrolleeRepository enrolleeRepo, EnrolleeAudRepository enrolleeAudRepo) {
		return (args) -> {
			String query = "select a.* from cmr_household A, cmr_household B where A.user_id = B.id and A.email_address=B.email_address and a.email_address not like '%zombie%'";
			Query q = em.createNativeQuery(query, Household.class);
			@SuppressWarnings("unchecked")
			List<Household> householdsUpdated = q.getResultList();//repository.findHouseholdByLastUpdateTime(sdf.parse(cutOffDate));
			//log.info("received:"+householdsUpdated.size());
			Iterator<Household> cursor = householdsUpdated.iterator();
			Household tmp = null;
			AccountUser user = null;
			String userEmail = null;
			String cmrEmail = null;
			while(cursor.hasNext()){
				 tmp = cursor.next();
				 cmrEmail = tmp.getEmail();
				 if(cmrEmail == null){
					 continue;
				 }
				 user = tmp.getUser();
				 if(user == null){
					 continue;
				 }
				 userEmail = user.getEmail();
				 log.info("Checking CMR record for user:"+userEmail);
				 // If user's email does not match with CMR email, we have a bad CMR Record
				 if(!userEmail.equalsIgnoreCase(cmrEmail)){
					 log.info("CMR HOUSEHOLD mismatch for:["+userEmail+"] CMR ID<CMR Email>"+tmp.getId()+"<"+cmrEmail+"> with AccountUser :"+user.getUserName()+"<"+user.getEmail()+">");
					 checkEnrollment(tmp.getId(), enrollmentRepo,  userEmail, enrolleeRepo, enrolleeAudRepo, cmrEmail);
					 checkSSAPApplications(tmp.getId(), ssapRepo, ssapApplicantsRepo, userEmail, cmrEmail);
					 if(updateDB){
						 tmp.setEmail(userEmail);
						 repository.save(tmp);
						 log.info("Updated CMR_Houshold for id: " + tmp.getId());
					 }
				 }
			}
		};
	}


	private void checkSSAPApplications(int id, SSAPAppRepository ssapRepo, SSAPApplicantsRepository ssapApplicantsRepo,String userEmail, String cmrEmail) {
		List<SsapApplication> applications = ssapRepo.findSSAPApplicationByHousehold(new BigDecimal(Integer.toString(id)));
		for(SsapApplication application: applications){
			List<SsapApplicant> applicants = application.getSsapApplicants();
			String applicantEmail = null;
			for(SsapApplicant applicant: applicants){
				applicantEmail = applicant.getEmailAddress();
				// CMR email is bad, if its available for applicant, its wrong
				if(applicantEmail != null && applicantEmail.equalsIgnoreCase(cmrEmail)){
					log.info("SSAP APPLICANT mismatch CMR_ID:"+id+" SSAP Application Id:"+application.getId()+" Applicant:"+applicant.getId()+"<"+applicantEmail+">");
					if(updateDB){
						applicant.setEmailAddress(userEmail);
						ssapApplicantsRepo.save(applicant);
						log.info("Updated SSAP_APPLICANTS for id: " + applicant.getId());
					}
				}
				
				try {
					modifySsapApplication( id,ssapRepo, application, userEmail, cmrEmail);
				} catch (JSONException e) {
					log.error("JSON Exception while processing household " + id, e);
				} 
			}
			
		}
	}


	private void modifySsapApplication(int id,  SSAPAppRepository ssapRepo, SsapApplication ssapApplication, String userEmail, String cmrEmail) throws JSONException {
		String jsonData = ssapApplication.getApplicationData();
		JSONObject jsonApplicationData = new JSONObject(jsonData);
		JSONObject jsonSingleStreamData = jsonApplicationData.getJSONObject("singleStreamlinedApplication");
		if(jsonSingleStreamData == null){
			return;
		}
		
		JSONArray jsonArray = (JSONArray) jsonSingleStreamData.get("taxHousehold");
		if (jsonArray == null || jsonArray.length() == 0) {
			return;
		}

		jsonArray = (JSONArray) ((JSONObject) jsonArray.get(0)).get("householdMember");
		if (jsonArray == null || jsonArray.length() == 0) {
			return;
		}
		final int size = jsonArray.length();
		JSONObject member;
		JSONObject houseHoldContact;
		for (int i = 0; i < size; i++) {
			member = (JSONObject) jsonArray.get(i);
			houseHoldContact =  member.getJSONObject("householdContact");
			
			if(houseHoldContact != null){
				JSONObject contactPref =  houseHoldContact.getJSONObject("contactPreferences");
				if(contactPref != null){
					if(!contactPref.isNull("emailAddress")){
						String emailSSAP = (String) contactPref.get("emailAddress"); 
						// CMR email is bad, if available, change it to user's email
						if(emailSSAP != null && !emailSSAP.isEmpty() && emailSSAP.equalsIgnoreCase(cmrEmail)){
							log.info("SSAP APPLICATION mismatch CMR_ID:"+id+" SSAP_Application_Id: "+ ssapApplication.getId()+"<"+emailSSAP + ">, userEmail: " + userEmail);
							if(updateDB){
								contactPref.remove("emailAddress");
								contactPref.put("emailAddress", userEmail);
							}
						}
					}
				}
			}
		}
		if(updateDB){
			ssapApplication.setApplicationData(jsonApplicationData.toString());
			ssapRepo.save(ssapApplication);
			log.info("Updated SSAP_APPLICATIONS for id: " + ssapApplication.getId());
		}
	}
	
	private void checkEnrollment(int id, EnrollmentRepository enrollmentRepo,  String userEmail, EnrolleeRepository enrolleeRepo, EnrolleeAudRepository enrolleeAudRepo, String cmrEmail) {
		List<Enrollment> enrollments = enrollmentRepo.findEnrollmentByHousehold(Integer.toString(id));
		for(Enrollment enrollment: enrollments){
			List<Enrollee> enrollees = enrollment.getEnrollees();
			String memberEmail = null;
			for(Enrollee member: enrollees){
				memberEmail = member.getPreferredEmail();
				if(memberEmail == null){
					continue;
				}
				// CMR email is bad
				if(memberEmail.equalsIgnoreCase(cmrEmail)){
					log.info("ENROLEE RECORD mismatch: CMR_ID:"+id+" USER_EMAIL:"+userEmail+" Enrollee Email: "+memberEmail);
					
					if(updateDB){
						member.setPreferredEmail(userEmail);
						enrolleeRepo.save(member);
						log.info("Updated ENROLLEE for id: " + member.getId());
					}
				}
			}
			List<EnrolleeAud> enroleeAud = enrolleeAudRepo.findEnrolleeAudEntrybyEnrollment(enrollment);
			for(EnrolleeAud audRec:enroleeAud){
				memberEmail = audRec.getPreferredEmail();
				if(memberEmail == null){
					continue;
				}
				if(memberEmail.equalsIgnoreCase(cmrEmail)){
					log.info("ENROLEE AUD RECORD mismatch: CMR_ID:"+id+" USER_EMAIL:"+userEmail+" Enrollee Aud Email: "+memberEmail);
					
					if(updateDB){
						audRec.setPreferredEmail(userEmail);
						enrolleeAudRepo.save(audRec);
						log.info("Updated ENROLLEE_AUD for id: " + audRec.getId());
					}					
				}
			}
		}
		
	}
}
