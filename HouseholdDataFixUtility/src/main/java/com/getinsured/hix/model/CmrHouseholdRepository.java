package com.getinsured.hix.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface CmrHouseholdRepository extends JpaRepository<Household, Integer>{
	//select a.* from cmr_household A, cmr_household B where A.created_by = B.id and A.email_address=B.email_address and a.email_address not like '%zombie%'
	//@Query("select hh.id FROM Household hh where hh.updated >= :updated")
	@Query("select hh FROM Household hh where hh.updated > :updated")
	List<Household> findHouseholdByLastUpdateTime(@Param("updated") Date date); 
	
	
}
