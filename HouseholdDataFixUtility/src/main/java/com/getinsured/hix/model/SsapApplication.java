package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the SSAP_APPLICATIONS database table.
 *
 */
@Entity
@Table(name="SSAP_APPLICATIONS")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="SsapApplication.findAll", query="SELECT s FROM SsapApplication s")
public class SsapApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_APPLICATIONS_ID_GENERATOR", sequenceName="SSAP_APPLICATIONS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICATIONS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="APPLICATION_DATA")
	private String applicationData;

	@Column(name="CMR_HOUSEOLD_ID", precision=22)
	private BigDecimal cmrHouseoldId;

	@Column(name="CREATED_BY", precision=10)
	private BigDecimal createdBy;

	@Column(name="CASE_NUMBER", length=100)
	private String caseNumber;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@Column(name="LAST_UPDATED_BY", precision=10)
	private BigDecimal lastUpdatedBy;
	

	@OneToMany(fetch = FetchType.EAGER, mappedBy="ssapApplication", cascade = CascadeType.PERSIST)
	private List<SsapApplicant> ssapApplicants;

	public SsapApplication() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplicationData() {
		return this.applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	
	public BigDecimal getCmrHouseoldId() {
		return this.cmrHouseoldId;
	}

	public void setCmrHouseoldId(BigDecimal cmrHouseoldId) {
		this.cmrHouseoldId = cmrHouseoldId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public String getCaseNumber() {
		return this.caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public List<SsapApplicant> getSsapApplicants() {
		return this.ssapApplicants;
	}

	public void setSsapApplicants(List<SsapApplicant> ssapApplicants) {
		this.ssapApplicants = ssapApplicants;
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new Timestamp(new Date().getTime()));
	}
}