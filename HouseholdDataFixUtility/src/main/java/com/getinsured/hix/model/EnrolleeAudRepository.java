package com.getinsured.hix.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
@Transactional
public interface EnrolleeAudRepository extends JpaRepository<EnrolleeAud, Integer>{

	@Query("select hh FROM EnrolleeAud hh where hh.enrollment = :enrollment")
	List<EnrolleeAud> findEnrolleeAudEntrybyEnrollment(@Param("enrollment") Enrollment enrollment); 
	
}
