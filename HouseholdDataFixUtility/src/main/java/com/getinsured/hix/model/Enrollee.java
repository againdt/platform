package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name = "ENROLLEE")
public class Enrollee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Enrollee_Seq")
	@SequenceGenerator(name = "Enrollee_Seq", sequenceName = "ENROLLEE_SEQ", allocationSize = 1)
	private int id;
	
	

	@ManyToOne
	@JoinColumn(name = "ENROLLMENT_ID")
	private Enrollment enrollment;

	@Column(name = "TAX_ID_NUMBER")
	private String taxIdNumber;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@XmlElement(name="middleName")
	@Column(name = "MIDDLE_NAME")
	private String middleName;

	@XmlElement(name="nameSuffix")
	@Column(name = "SUFFIX")
	private String suffix;

	@Column(name = "PREF_EMAIL")
	private String preferredEmail;

	public Enrollee() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getPreferredEmail() {
		return preferredEmail;
	}

	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}
}
