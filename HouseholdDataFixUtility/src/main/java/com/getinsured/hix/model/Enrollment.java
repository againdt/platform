package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name="ENROLLMENT")
public class Enrollment implements Serializable, Comparable<Enrollment> {
	public static enum Status{ SOLD, PENDING, ESIG_DONE }
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Enrollment_Seq")
	@SequenceGenerator(name = "Enrollment_Seq", sequenceName = "ENROLLMENT_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId ;
	
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@OneToOne
	@JoinColumn(name="created_by")
	private AccountUser createdBy;

	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;

	@OneToMany(mappedBy="enrollment", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Enrollee> enrollees;

	@Column(name = "SSAP_APPLICATION_ID")
	private Long ssapApplicationid;
	
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	

	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	public Integer getId() {
		return id;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new Date());
		this.setUpdatedOn(new Date());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new Date()); 
	}
	
	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setEnrollees(List<Enrollee> enrollees) {
		this.enrollees = enrollees;
	}
	
	public List<Enrollee> getEnrollees() {
		return enrollees;
	}
	
	
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public Long getSsapApplicationid() {
		return ssapApplicationid;
	}

	public void setSsapApplicationid(Long ssapApplicationid) {
		this.ssapApplicationid = ssapApplicationid;
	}
	
	@Override
	public int compareTo(Enrollment enrollment) {
		if(enrollment != null){
			return this.updatedOn.compareTo(enrollment.getUpdatedOn());
		}
		return 0;
	}

	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
