package com.getinsured.hix.model;


import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<AccountUser, Integer> {
}
