package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the roles database table.
 * 
 */

@Entity
@Table(name = "ENROLLEE_AUD")
public class EnrolleeAud implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "ENROLLMENT_ID")
	private Enrollment enrollment;

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	@Column(name = "pref_email")
	private String preferredEmail;
   	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPreferredEmail() {
		return preferredEmail;
	}

	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}

	
	
}
