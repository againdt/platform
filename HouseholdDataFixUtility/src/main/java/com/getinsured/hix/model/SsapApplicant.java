package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;



/**
 * The persistent class for the SSAP_APPLICANTS database table.
 *
 */
@Entity
@Table(name="SSAP_APPLICANTS")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="SsapApplicant.findAll", query="SELECT s FROM SsapApplicant s")
public class SsapApplicant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_APPLICANTS_ID_GENERATOR", sequenceName="SSAP_APPLICANTS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICANTS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	

	@Column(name="FIRST_NAME", length=100)
	private String firstName;


	@Column(name="LAST_NAME", length=100)
	private String lastName;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	
	@Column(name="MIDDLE_NAME", length=100)
	private String middleName;

	

	@Column(name="EMAIL_ADDRESS", length=100)
	private String emailAddress;

	

	@Column(name="APPLICANT_GUID", length=100)
	private String applicantGuid;

	
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@ManyToOne
	@JoinColumn(name="SSAP_APPLICATION_ID", nullable=false)
	private SsapApplication ssapApplication;


	public SsapApplicant() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	
	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public SsapApplication getSsapApplication() {
		return this.ssapApplication;
	}

	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new Timestamp(new Date().getTime()));
	}
}