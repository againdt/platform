package com.getinsured.hix.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public interface SSAPAppRepository extends JpaRepository<SsapApplication, Integer>{

	@Query("select hh FROM SsapApplication hh where hh.cmrHouseoldId = :cmrHouseoldId")
	List<SsapApplication> findSSAPApplicationByHousehold(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId); 
	
	
}