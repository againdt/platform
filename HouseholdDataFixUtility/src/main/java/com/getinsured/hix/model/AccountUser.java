package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import org.hibernate.annotations.TypeDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The persistent class for the users database table.
 *
 */
@Entity
@Table(name="users")
public class AccountUser implements Serializable,Cloneable{
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountUser.class);

	

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountUser_Seq")
	@SequenceGenerator(name = "AccountUser_Seq", sequenceName = "users_seq", allocationSize = 1)
	private int id;

	
	@Column(name="username",unique = true,nullable = false)
	private String userName;

	@Column(name="email")
	private String email;

	@Column(name="phone", length=10)
	private String phone;

	@Column(name="recovery",unique = true,nullable = false)
	private String recovery;

	@Column(name="communication_pref")
	private String communicationPref;

	@Column(name="confirmed")
	private int confirmed;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date created;

	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;


	@Column(name="pass_recovery_token" ,length=4000)
	private String passwordRecoveryToken;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pass_recovery_token_expiration")
	private Date passwordRecoveryTokenExpiration;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="lastLogin")
	private Date lastLogin;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="startDate")
	private Date startDate;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="endDate")
	private Date endDate;


	@Column(name="ISSUPERUSER")
	private Integer isSuperUser;

	/*
	 * HIX-42019 : Persists extn_app_userid
	 */
	@Column(name="EXTN_APP_USERID",unique = true)
	private String extnAppUserId;

	/*@Transient ModuleUser activeModule stores users active module details.
	 * 1. When the user has logged in then activeModule will be the ModuleUser record that
	 *    had created while user's post-registration process
	 * 2. When broker is switching from broker role to employer role then activeModule will be
	 *    the ModuleUser record that matches;
	 *       a. moduleId=<switch to module id>
	 *       b. moduleName=<switch to module name> (its EMPLOYER_MODULE)
	 *       c. userId=loggedin user id
	 */

	@Column(name = "TENANT_ID")
	private Long tenantId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	    
	@Column(name="status")
	//@Transient
	private String status;

	@Column(name = "user_npn")
	private String userNPN;

	public AccountUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail(){

		return this.email;
	}

	public void setEmail(String email){
		if(email != null){
			this.email = email.trim().toLowerCase();
		}
	}


	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getIsSuperUser() {
		return isSuperUser;
	}

	public void setIsSuperUser(Integer isSuperUser) {
		this.isSuperUser = isSuperUser;
	}
	
	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountUser other = (AccountUser) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}


}
