package com.getinsured.hix.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
@Transactional
public interface EnrollmentRepository extends JpaRepository<Enrollment, Integer>{

	@Query("select hh FROM Enrollment hh where hh.houseHoldCaseId = :id")
	List<Enrollment> findEnrollmentByHousehold(@Param("id") String id); 
	
}