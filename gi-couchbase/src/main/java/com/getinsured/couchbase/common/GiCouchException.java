package com.getinsured.couchbase.common;

public class GiCouchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1008925029461187345L;

	public GiCouchException() {
		super();
	}

	public GiCouchException(String message, Throwable cause) {
		super(message, cause);
	}

	public GiCouchException(String message) {
		super(message);
	}

	public GiCouchException(Throwable cause) {
		super(cause);
	}

}
