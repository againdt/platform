package com.getinsured.couchbase.model;

import java.util.Map;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;

@Document
public class CouchEcmDocument extends CouchDocument {
	@Field
	private String relativePath;
	@Field
	private String description;
	@Field
	private String originalFileName;
	@Field
	private Map<String, String> customMetaData;

	public CouchEcmDocument() {
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public Map<String, String> getCustomMetaData() {
		return customMetaData;
	}

	public void setCustomMetaData(Map<String, String> customMetaData) {
		this.customMetaData = customMetaData;
	}

}
