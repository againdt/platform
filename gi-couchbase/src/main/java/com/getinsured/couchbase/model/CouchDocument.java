package com.getinsured.couchbase.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document
public class CouchDocument {
	@Id
	private String metaId;
	@Field
	private String id;
	@Field
	private CouchMeta metaData = new CouchMeta();
	@Field
	private String content;
	@Field
	private CouchBinary binaryMetaData;

	public String getMetaId() {
		return metaId;
	}

	public void setMetaId(String metaId) {
		this.metaId = metaId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CouchMeta getMetaData() {
		return metaData;
	}

	public void setMetaData(CouchMeta metaData) {
		this.metaData = metaData;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public CouchBinary getBinaryMetaData() {
		return binaryMetaData;
	}

	public void setBinaryMetaData(CouchBinary binaryMetaData) {
		this.binaryMetaData = binaryMetaData;
	}
}
