package com.getinsured.couchbase.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.couchbase.model.CouchBinary;
import com.getinsured.couchbase.model.CouchEcmDocument;
import com.getinsured.couchbase.service.CouchEcmDocumentService;

@RestController
@RequestMapping("/test")
public class CouchEcmController {

	@Autowired
	@Qualifier("couchEcmDocumentService")
	private CouchEcmDocumentService couchEcmDocumentService;

	@RequestMapping("/nonBinaryCreate")
	String nonBinaryCreate() {
		String s = "This is a Test Content";
		String id = createContent("some/relativepath", "Test.txt", s, "Category", "SubCategory", "TXT");
		return id;
	}

	public String createContent(String relativePath, String fileName, String content, String category,
			String subCategory, String type) {

		CouchEcmDocument document = formDocument(relativePath, fileName, null, "text/plain", content, category,
				subCategory, type);

		return couchEcmDocumentService.createDocument(document);

	}

	@RequestMapping("/binaryCreate")
	String binaryCreate() throws Exception {
		File file = new File("D://test.pdf");
		FileInputStream fileInputStream = null;
		byte[] bFile = new byte[(int) file.length()];
		fileInputStream = new FileInputStream(file);
		fileInputStream.read(bFile);
		fileInputStream.close();

		CouchEcmDocument document = formDocument("some/relativepath", "Sample.pdf", bFile, "mimeType", null,
				"Category", "SubCategory", "type");

		final String documentId = couchEcmDocumentService.createDocument(document);
		final String[] documentContentIds = couchEcmDocumentService.createBinaryDocument(bFile,
				document.getBinaryMetaData().getContentLink());

		return documentId + " : " + document.getBinaryMetaData().getContentLink();
	}

	private CouchEcmDocument formDocument(String relativePath, String fileName, byte[] dataBytes, String mimeType,
			String content, String category, String subCategory, String type) {
		CouchEcmDocument document = new CouchEcmDocument();
		document.setRelativePath(relativePath);
		document.setContent(content);
		document.getMetaData().setCategory(category);
		document.getMetaData().setSubCategory(subCategory);
		document.getMetaData().setCreationDate(new Date());
		document.getMetaData().setModifiedDate(new Date());
		document.getMetaData().setCreatedBy("abc");
		document.getMetaData().setModifiedBy("abc");
		if (dataBytes != null) {
			CouchBinary couchBinary = new CouchBinary();
			couchBinary.setFileName(fileName);
			couchBinary.setFileSize(dataBytes.length);
			couchBinary.setMimeType(mimeType + "; charset=UTF-8");
			couchBinary.setHasContent(true);
			couchBinary.setDocType(type);
			document.setBinaryMetaData(couchBinary);
		}
		return document;
	}

	@RequestMapping(value = "/download/{uploadedFileId}", method = RequestMethod.GET)
	public void getFile(@PathVariable("uploadedFileId") String uploadedFileId, HttpServletResponse response)
			throws Exception {
		CouchEcmDocument cd = couchEcmDocumentService.getDocumentById(uploadedFileId);
		byte[] document = couchEcmDocumentService.getBinaryDocumentById(uploadedFileId);
		try {
			streamData(response, document, cd.getBinaryMetaData().getFileName());
		} catch (IOException e) {
			throw e;
		}
		System.out.println(cd.getMetaId() + " : " + cd.getId() + " : " + cd.getMetaData().getCreationDate());

	}

	@RequestMapping(value = "/testDelete/{id}", method = RequestMethod.GET)
	public @ResponseBody String testDelete(@PathVariable("id") String id) throws Exception {
		couchEcmDocumentService.deleteDocument(id);
		return "Success";
	}

	private void streamData(HttpServletResponse response, byte[] bytes, String fileName) throws IOException {
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
		response.setContentType("application/octet-stream");
		FileCopyUtils.copy(bytes, response.getOutputStream());
	}

}
