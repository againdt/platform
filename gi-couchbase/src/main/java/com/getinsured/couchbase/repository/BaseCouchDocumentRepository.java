package com.getinsured.couchbase.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.getinsured.couchbase.model.CouchDocument;

public interface BaseCouchDocumentRepository<T extends CouchDocument,ID extends Serializable> extends PagingAndSortingRepository <T, ID>{

}
