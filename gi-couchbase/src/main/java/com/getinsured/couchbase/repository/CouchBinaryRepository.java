package com.getinsured.couchbase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.couchbase.model.CouchBinaryDocument;

@Repository("couchBinaryRepository")
public interface CouchBinaryRepository extends CrudRepository<CouchBinaryDocument, String> {

}
	