package com.getinsured.couchbase.repository;

import org.springframework.stereotype.Repository;

import com.getinsured.couchbase.model.CouchEcmDocument;

@Repository("couchEcmDocumentRepository")
public interface CouchEcmDocumentRepository extends BaseCouchDocumentRepository<CouchEcmDocument, String> {

}
	