package com.getinsured.couchbase.conf;

import org.apache.commons.codec.binary.Base64;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

@Component
public class CustomConverters {
	/*
	@WritingConverter
	public static enum DateWriterConverter implements Converter<Date, String> {
		INSTANCE;
		@Override
		public String convert(Date source) {
			if (source != null) {
				DateFormat objFormat = new SimpleDateFormat(ISO_DATETIME_TIME_ZONE_FORMAT.getPattern());
				String strDate = objFormat.format(source);
				return strDate;
			} else {
				return null;
			}
		}
	}

	@ReadingConverter
	public static enum DateReaderConverter implements Converter<String, Date> {
		INSTANCE;
		@Override
		public Date convert(String source) {
			if (source != null) {
				Date objDate = null;
				final DateFormat objFormat = new SimpleDateFormat(ISO_DATETIME_TIME_ZONE_FORMAT.getPattern());
				final ParsePosition pos = new ParsePosition(0);
				objDate = objFormat.parse(source, pos);
				return objDate;
			} else {
				return null;
			}
		}

	}
	*/

	@WritingConverter
	public static enum ByteWriterConverter implements Converter<byte[], String> {
		INSTANCE;
		@Override
		public String convert(byte[] source) {
			if (source != null) {
				return Base64.encodeBase64String(source);
			} else {
				return null;
			}
		}
	}

	@ReadingConverter
	public static enum ByteReaderConverter implements Converter<String, byte[]> {
		INSTANCE;
		@Override
		public byte[] convert(String source) {
			if (source != null) {
				return Base64.decodeBase64(source);
			} else {
				return null;
			}
		}

	}
}
