package com.getinsured.couchbase.conf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.data.couchbase.core.convert.CustomConversions;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import org.springframework.data.couchbase.repository.config.RepositoryOperationsMapping;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.getinsured.couchbase.conf.CustomConverters.ByteReaderConverter;
import com.getinsured.couchbase.conf.CustomConverters.ByteWriterConverter;
import com.getinsured.couchbase.repository.CouchBinaryRepository;

@Configuration
@EnableCouchbaseRepositories({ "com.getinsured.couchbase.repository" })
public class ConnectionConfiguration extends AbstractCouchbaseConfiguration {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionConfiguration.class.getName());

	private static final String BINARY_BUCKET_NOT_AVAILABLE = "Binary Bucket not available";

	private static final String NON_BINARY_BUCKET_NOT_AVAILABLE = "Non-Binary Bucket not available";

	@Value("${couchbase.nodes}")
	private List<String> nodes = new ArrayList<String>();

	@Value("${couchbase.binary.bucketname}")
	private String binaryBucketName;

	@Value("${couchbase.nonbinary.bucketname}")
	private String nonbinaryBucketName;

	@Value("${couchbase.password}")
	private String password;

	@Override
	protected List<String> getBootstrapHosts() {
		return nodes;
	}

	@Override
	protected String getBucketName() {
		return nonbinaryBucketName;
	}

	@Override
	protected String getBucketPassword() {
		return password;
	}

	@Override
	public CouchbaseTemplate couchbaseTemplate() throws Exception {
		try {
			return super.couchbaseTemplate();
		} catch (Exception e) {
			LOGGER.error(NON_BINARY_BUCKET_NOT_AVAILABLE);
			LOGGER.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
	}

	@Bean
	public Bucket binaryBucket() throws Exception {
		if (StringUtils.isNotBlank(binaryBucketName)) {
			return couchbaseCluster().openBucket(binaryBucketName, password);
		}
		return null;
	}

	@Bean
	public CouchbaseTemplate binaryTemplate() {
		CouchbaseTemplate template = null;
		try {
			if (StringUtils.isNotBlank(binaryBucketName)) {
				template = new CouchbaseTemplate(couchbaseClusterInfo(), binaryBucket(), mappingCouchbaseConverter(),
						translationService());
				template.setDefaultConsistency(getDefaultConsistency());
			} else {
				LOGGER.info(BINARY_BUCKET_NOT_AVAILABLE);
			}
		} catch (Exception e) {
			LOGGER.error(BINARY_BUCKET_NOT_AVAILABLE);
			LOGGER.error(ExceptionUtils.getStackTrace(e));
		}
		return template;
	}

	@Override
	public void configureRepositoryOperationsMapping(RepositoryOperationsMapping baseMapping) {
		CouchbaseTemplate binaryTemplate = binaryTemplate();
		if (null != binaryTemplate) {
			baseMapping.map(CouchBinaryRepository.class, binaryTemplate);
		}
	}

	@Override
	public CustomConversions customConversions() {
		return new CustomConversions(Arrays.asList(ByteWriterConverter.INSTANCE, ByteReaderConverter.INSTANCE));
	}

	@Override
	protected CouchbaseEnvironment getEnvironment() {
		return DefaultCouchbaseEnvironment.builder()
				.connectTimeout(TimeUnit.SECONDS.toMillis(ConnectionProperties.CONNECTION_TIMEOUT))
				.kvTimeout(TimeUnit.SECONDS.toMillis(ConnectionProperties.OPERATION_TIMEOUT)).build();
	}

	private static class ConnectionProperties {
		private static final int CONNECTION_TIMEOUT = 100;
		private static final int OPERATION_TIMEOUT = 60;
	}
}
