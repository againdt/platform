package com.getinsured.couchbase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.couchbase.repository.CouchEcmDocumentRepository;

@Service("couchEcmDocumentService")
public class CouchEcmDocumentService extends CouchDocumentBaseService {

	@Autowired
	@Qualifier("couchEcmDocumentRepository")
	private CouchEcmDocumentRepository repository;

	@SuppressWarnings("unchecked")
	@Override
	public CouchEcmDocumentRepository getRepository() {
		return repository;
	}

}
