package com.getinsured.couchbase.service;

import java.io.Serializable;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;

import com.getinsured.couchbase.common.CouchBaseUtil;
import com.getinsured.couchbase.common.GiCouchException;
import com.getinsured.couchbase.model.CouchBinaryDocument;
import com.getinsured.couchbase.model.CouchDocument;
import com.getinsured.couchbase.model.CouchMeta;
import com.getinsured.couchbase.repository.CouchBinaryRepository;

public abstract class CouchDocumentBaseService {
	private static final String ERROR_WHILE_UPDATING_DOCUMENT = "Error while updating document - ";

	private static final String ERROR_WHILE_CREATING_DOCUMENT = "Error while creating Document - ";

	private static final String ERROR_WHILE_CREATING_BINARY_DOCUMENT = "Error while creating Binary Document - ";

	private static final String ERROR_WHILE_UPDATING_BINARY_DOCUMENT = "Error while updating Binary Document - ";

	private static final String SUB_CATEGORY_CANNOT_BE_EMPTY = "Sub-category Cannot be empty";

	private static final String CATEGORY_CANNOT_BE_EMPTY = "Category Cannot be empty";

	private static final String BIN = "BIN";

	private static final String CB_DOC_ID_SEPARATOR = "::";

	public abstract <T extends CouchDocument, ID extends Serializable> CrudRepository<T, ID> getRepository();

	@Autowired
	@Qualifier("couchBinaryRepository")
	private CouchBinaryRepository couchBinaryRepository;

	public static String documentId(CouchMeta meta) {

		StringBuilder id = new StringBuilder().append(meta.getCategory() + CB_DOC_ID_SEPARATOR);

		if (StringUtils.isNotBlank(meta.getSubCategory())){
			id.append(meta.getSubCategory() + CB_DOC_ID_SEPARATOR);
		}

		if (StringUtils.isNotBlank(meta.getCustomKey())){
			id.append(meta.getCustomKey());
		} else {
			id.append(UUID.randomUUID().toString());
		}

		return id.toString();
	}

	public static String binaryContentId(String documentId) {
		return documentId + CB_DOC_ID_SEPARATOR + BIN;
	}

	public static String binaryDocumentId(String documentId, int partNumber) {
		/**
		 * purposely not appended part number for first document to support
		 * already migrated document without split feature
		 */
		return ((partNumber == 0) ? documentId : documentId + CB_DOC_ID_SEPARATOR + partNumber);
	}

	public <T extends CouchDocument> String createDocument(T document) {
		validateAndSetId(document);
		try {
			final T createdDocument = getRepository().save(document);
			return createdDocument.getId();
		} catch (Exception e) {
			throw new GiCouchException(ERROR_WHILE_CREATING_DOCUMENT + e.getMessage(), e);
		}
	}

	private <T extends CouchDocument> void validateAndSetId(T document) {
		validateDocument(document.getMetaData());
		document.setId(documentId(document.getMetaData()));
		document.setMetaId(document.getId());
		if (document.getBinaryMetaData() != null && document.getBinaryMetaData().isHasContent()) {
			document.getBinaryMetaData()
					.setNumberOfParts(CouchBaseUtil.numberOfParts(document.getBinaryMetaData().getFileSize()));
			document.getBinaryMetaData().setContentLink(binaryContentId(documentId(document.getMetaData())));
		}
	}

	private void validateDocument(CouchMeta meta) {
		if (StringUtils.isEmpty(meta.getCategory())) {
			throw new IllegalArgumentException(CATEGORY_CANNOT_BE_EMPTY);
		}
	}

	public String[] createBinaryDocument(byte[] data, String id) {
		final byte[][] byteDataArray = CouchBaseUtil.splitBytes(data);
		final int numberOfDocs = byteDataArray.length;
		final String[] docIds = new String[numberOfDocs];
		for (int i = 0; i < numberOfDocs; i++) {
			docIds[i] = createBinary(byteDataArray[i], binaryDocumentId(id, i));
		}
		return docIds;
	}

	private String createBinary(byte[] data, String id) {
		try {
			CouchBinaryDocument binaryDocument = new CouchBinaryDocument();
			binaryDocument.setData(data);
			binaryDocument.setId(id);
			binaryDocument = couchBinaryRepository.save(binaryDocument);
			return binaryDocument.getId();
		} catch (Exception e) {
			throw new GiCouchException(ERROR_WHILE_CREATING_BINARY_DOCUMENT + e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends CouchDocument> T getDocumentById(String id) {
		return (T) getRepository().findOne(id);
	}

	public byte[] getBinaryDocumentById(String contentId) {
		byte[] data = null;
		CouchDocument document = getDocumentById(contentId);
		if (document != null) {
			final int numberOfParts = document.getBinaryMetaData().getNumberOfParts();
			byte[] partData = null;
			data = new byte[document.getBinaryMetaData().getFileSize()];
			int startData = 0;
			for (int i = 0; i < numberOfParts; i++) {
				partData = getBinaryData(binaryDocumentId(document.getBinaryMetaData().getContentLink(), i));
				System.arraycopy(partData, 0, data, startData, partData.length);
				startData += partData.length;
			}
		}
		return data;
	}

	private byte[] getBinaryData(String id) {
		CouchBinaryDocument binaryDocument = getBinaryDocumentObjectById(id);
		if (binaryDocument != null) {
			return binaryDocument.getData();
		} else {
			return null;
		}
	}

	public CouchBinaryDocument getBinaryDocumentObjectById(String id) {
		return couchBinaryRepository.findOne(id);
	}

	public <T extends CouchDocument> void deleteDocument(String id) {
		T document = getDocumentById(id);
		if (document != null) {
			final boolean hasBinary = hasBinary(document);
			if (hasBinary) {
				deleteBinaryDocument(document);
			}
			getRepository().delete(id);
		}

	}

	public <T extends CouchDocument> void deleteBinaryDocument(T document) {
		for (int i = 0; i < document.getBinaryMetaData().getNumberOfParts(); i++) {
			deleteBinaryDocumentObject(binaryDocumentId(document.getBinaryMetaData().getContentLink(), i));
		}
	}

	private <T extends CouchDocument> boolean hasBinary(T document) {
		return document.getBinaryMetaData() != null && document.getBinaryMetaData().isHasContent();
	}

	public void deleteBinaryDocumentObject(String id) {
		couchBinaryRepository.delete(id);
	}

	public <T extends CouchDocument> String updateDocument(T document) {
		try {
			final T updatedDocument = getRepository().save(document);
			return updatedDocument.getId();
		} catch (Exception e) {
			throw new GiCouchException(ERROR_WHILE_UPDATING_DOCUMENT + e.getMessage(), e);
		}
	}

	public <T extends CouchDocument> void updateBinaryDocument(T document, byte[] newContent) {
		validateDocument(document.getMetaData());
		deleteBinaryDocument(document);
		document.getBinaryMetaData().setFileSize(newContent.length);
		document.getBinaryMetaData()
				.setNumberOfParts(CouchBaseUtil.numberOfParts(document.getBinaryMetaData().getFileSize()));
		document.getBinaryMetaData().setContentLink(binaryContentId(documentId(document.getMetaData())));
		updateDocument(document);
		createBinaryDocument(newContent, document.getBinaryMetaData().getContentLink());
	}

	public String updateBinaryDocument(CouchBinaryDocument document) {
		try {
			final CouchBinaryDocument updatedDocument = couchBinaryRepository.save(document);
			return updatedDocument.getId();
		} catch (Exception e) {
			throw new GiCouchException(ERROR_WHILE_UPDATING_BINARY_DOCUMENT + e.getMessage(), e);
		}
	}
}
