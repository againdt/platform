package com.getinsured.test;

import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp.datasources.SharedPoolDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

/**
 * Created by song_s on 6/8/17.
 */
public class JndiInitializer {
    public static void initialize() throws Exception {
        DriverAdapterCPDS cpds = new DriverAdapterCPDS();
        cpds.setDriver("org.postgresql.Driver");
        cpds.setUrl("jdbc:postgresql://sfo-pg4.ghixqa.com:5444/id2dev");
        cpds.setUser("id2dev");
        cpds.setPassword("MGIyZDMwZjFkODRk");

        SharedPoolDataSource dataSource = new SharedPoolDataSource();
        dataSource.setConnectionPoolDataSource(cpds);
        dataSource.setMaxActive(10);
        dataSource.setMaxWait(50);

        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
        builder.bind("java:comp/env/jdbc/ghixDS", dataSource);
        builder.activate();
    }
}
