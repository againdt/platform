package com.getinsured.test;

public class PropertyInitializer {
    public static void init() {
        checkRequired("GHIX_HOME");
        checkRequired("GHIX_SECNTX_TYPE");
    }

    private static void checkRequired(String key) {
        String value = System.getProperty(key);
        if (value == null) {
            throw new RuntimeException("Property " + key + " must be set.");
        }
    }

    private static void replaceNullValue(String key, String defaultValue) {
        String value = System.getProperty(key);
        if (value == null) {
            System.setProperty(key, defaultValue);
        }
    }
}
