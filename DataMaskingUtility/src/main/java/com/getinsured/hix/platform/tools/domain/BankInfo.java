package com.getinsured.hix.platform.tools.domain;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the bankinfo database table.
 * 
 */


@Entity
@Table(name="bank_info")
public class BankInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BankInfo_Seq")
	@SequenceGenerator(name = "BankInfo_Seq", sequenceName = "bank_info_seq", allocationSize = 1)
	private int id;
	
	@Column(name="bank_name")
	private String bankName;
	
	@Column(name="name_on_account")
	private String nameOnAccount;
	
	@Column(name="routing_number",length=9)
	private String routingNumber;

	@Column(name="account_number")
	private String accountNumber;

	@Column(name="account_type", length=1)
	private String accountType;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date created;

    @Temporal( TemporalType.TIMESTAMP)
	private Date updated;
    
      
    @Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

    public BankInfo() {
    }
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getNameOnAccount() {
		return nameOnAccount;
	}

	public void setNameOnAccount(String nameOnAccount) {
		this.nameOnAccount = nameOnAccount;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new Date());
		this.setUpdated(new Date());
	}
	
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new Date()); 
	}
	
	public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("accountType=").append(accountType).append(", ");
        sb.append("bankName=").append(bankName).append(", ");
        sb.append("routingNumber=").append(routingNumber).append(", \n");
        sb.append("accountNumber=").append(accountNumber).append(", \n");
        sb.append("routingNumber=").append(routingNumber).append(" \n");
       
        return sb.toString();
    }
}