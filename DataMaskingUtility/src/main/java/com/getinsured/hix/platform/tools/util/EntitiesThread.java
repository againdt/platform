package com.getinsured.hix.platform.tools.util;

import java.io.PrintWriter;
import javax.persistence.EntityManager;

public class EntitiesThread implements Runnable {
	private EntityMaskerThread entityThread;
	private PrintWriter maskerLogger;
	
	public boolean buildWorker(String entityName, EntityManager manager, PrintWriter maskerLogger){
		return this. setLogger(maskerLogger)
				   . createEntityThread(entityName,  manager);
	}
	
	public void run() {
		System.out.println(this.entityThread.getClass().getSimpleName()+" Started. ");
		maskerLogger.println(this.entityThread.getClass().getSimpleName()+" Started. ");
		
		this.entityThread.saveMaskedData();
        
        System.out.println(this.entityThread.getClass().getSimpleName()+" Ended. ");
		maskerLogger.println(this.entityThread.getClass().getSimpleName()+" Ended. ");
	}
	
	private EntitiesThread setLogger(PrintWriter maskerLogger){
		this.maskerLogger = maskerLogger;
		return this;
	}
	
	private boolean createEntityThread(String entityName, EntityManager manager){
		try {
			this.entityThread = (EntityMaskerThread) EntitiesThread.class.getClassLoader() 
					           . loadClass("com.getinsured.hix.platform.tools.util."+entityName+"Thread")
					           . newInstance();
			
			this.entityThread
			. setEntityManager(manager)
			. setMaskerLogger(maskerLogger);
			
			maskerLogger.println("Worker thread for entity :"+this.entityThread.getClass().getSimpleName() +" created.");
			maskerLogger.flush();
			return true;
		} catch (InstantiationException e) {
			e.printStackTrace(maskerLogger);
		} catch (IllegalAccessException e) {
			e.printStackTrace(maskerLogger);
		} catch (ClassNotFoundException e) {
			e.printStackTrace(maskerLogger);
		}
		maskerLogger.flush();
		return false;
	}
}
