package com.getinsured.hix.platform.tools.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PostLoad;

import com.getinsured.hix.platform.tools.domain.PldPerson;


public class PldPersonThread extends EntityBaseThread {
	
	private static int MAXRESULT = 10;
	
	public List<PldPerson>  getData(int count){
		List<PldPerson> resultList = new ArrayList<PldPerson>();
	
		int offset = count * PldPersonThread.MAXRESULT; 
		
		try{
				maskerLogger.println("Fetching PldPerson data to Mask :- Offset :"+offset );
	
				resultList = this.manager.createQuery( "Select a From PldPerson a", PldPerson.class)
						.setFirstResult(offset) 
						.setMaxResults(PldPersonThread.MAXRESULT)
						.getResultList();
		}catch(Exception e){
			e.printStackTrace(maskerLogger);
			maskerLogger.flush();
		}
		
		return resultList;
	}


	@PostLoad
	public void maskData(PldPerson pldPersonObj) {
		pldPersonObj.setFirstName( scramble( pldPersonObj.getFirstName() ) );
		pldPersonObj.setLastName( scramble( pldPersonObj.getLastName() ) );
	}

}
