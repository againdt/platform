package com.getinsured.hix.platform.tools.util;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;


public abstract class EntityBaseThread implements EntityMaskerThread {
	
	protected EntityManager manager;
	protected PrintWriter maskerLogger;
	
	public EntityBaseThread setEntityManager(EntityManager manager) {
		this.manager = manager;
		return this;
	}
	
	public EntityBaseThread setMaskerLogger(PrintWriter maskerLogger) {
		this.maskerLogger = maskerLogger;
		return this;
	}
	
	public void saveMaskedData() {
		//ExecutorService entityMaskExecutor = Executors.newFixedThreadPool(2);
		int count= 0;
		while(true){
			maskerLogger.println(this.getClass().getName()+" :: COUNT :- "+ (count+1)+" STARTED.");
			
			//for testing
			//if (count == 1) {break;}
			
			List<?> records = this.getData(count);
			if(records.size() == 0) { break; }
			
			EntityTransaction tx = manager.getTransaction();
			try {
				for (Object record : records) {
					tx.begin();
					manager.merge(record);
					tx.commit();
		        }
			} catch (Exception e) {
				e.printStackTrace(maskerLogger);
				maskerLogger.flush();
			}
			
			maskerLogger.println(this.getClass().getName()+" :: COUNT :- "+ (count+1)+" ENDED.");
			count++;
		}
	}
	
	public List<?> getData(int count){
		return null;
	}

//-------------------------------UTILITY METHODS-------------------------------------------------------	
	protected String scramble(String word){
		
		if(word.length() <= 1) return word;
		
		List<Character> l = new ArrayList<>();
		for(char c :  word.toCharArray()) {
		    l.add(c); 
		}
		Collections.shuffle(l); 

		StringBuilder sb = new StringBuilder();
		for(char c : l)
		  sb.append(c);

		return sb.toString();
	}
	
	protected String maskEmail(String email){
		List<String> emailTokens = Arrays.asList(email.split("@"));
		List<Character> l = new ArrayList<>();
		for(char c :  emailTokens.get(0).toCharArray()) {
		    l.add(c); 
		}
		Collections.shuffle(l); 

		StringBuilder sb = new StringBuilder();
		for(char c : l)
		  sb.append(c);

		return sb.append("@yopmail.com").toString();
	}
}
