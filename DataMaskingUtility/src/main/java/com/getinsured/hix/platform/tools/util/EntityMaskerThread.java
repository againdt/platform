package com.getinsured.hix.platform.tools.util;

import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;


public interface EntityMaskerThread {
	void saveMaskedData ();
	
	EntityMaskerThread setEntityManager(EntityManager manager);
	
	EntityMaskerThread setMaskerLogger(PrintWriter maskerLogger);
	
	List<?> getData(int count);
}
