package com.getinsured.hix.platform.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.getinsured.hix.platform.tools.util.EntitiesThread;


public class DataMasker {

	private PrintWriter maskerLogger;
	private File outputFile;
	private EntityManager manager;
	private Map<String,String> entityMaps;
	
	public DataMasker (){
		
		this.setLogger(). 
		     createEntityManager(). 
		     createEntityList().
		     processMasking();
		
		this.maskerLogger.flush();
		this.maskerLogger.close();
		
		System.exit(0);
	}
	
	public static void main (String[] args ){
		new DataMasker();
	}
	
	private DataMasker setLogger(){
		try {
			outputFile = new File("DataMasker.log");
			if (outputFile.exists()) { outputFile.delete(); }
			this.maskerLogger = new PrintWriter(outputFile);
		} catch (FileNotFoundException e) {
			System.err.println("Error while setting up logger"+ e.getMessage());
			System.exit(0);
		}
		
		System.out.println("Logger file set to :"+ outputFile.getAbsolutePath() );
		return this;
	}
	
	private DataMasker createEntityManager() {
		HashMap<String, String> dbParameters = new HashMap<String, String>();
		try {
			/*
			dbParameters.put("javax.persistence.jdbc.driver",   "oracle.jdbc.OracleDriver"); 
			dbParameters.put("javax.persistence.jdbc.url",      System.getProperty("dbUrl")); 
			dbParameters.put("javax.persistence.jdbc.user",     System.getProperty("dbUser"));
			dbParameters.put("javax.persistence.jdbc.password", System.getProperty("dbPassword"));
			*/
			
			dbParameters.put("javax.persistence.jdbc.driver",   "oracle.jdbc.OracleDriver"); 
			dbParameters.put("javax.persistence.jdbc.url",      "jdbc:oracle:thin:@oracle1db.ghixqa.com:1521/ghixdb"); 
			dbParameters.put("javax.persistence.jdbc.user",     "NMSHOPQA");
			dbParameters.put("javax.persistence.jdbc.password", "MTMzNzdlOWUxMTA0");
			
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("maskerUnit", dbParameters);
			this.manager =  factory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Failed to create Entity manager, Message:" + e.getMessage());
			e.printStackTrace(maskerLogger);
			maskerLogger.flush();
			System.exit(0);
		}
		
		System.out.println("Database handel created ");
		return this;
	}
	
	private DataMasker createEntityList() {
		Properties prop = new Properties();
		InputStream input = null;
		this.entityMaps = new HashMap<String, String>();
		String filename = "TablesToMask.properties";
		
		maskerLogger.println( "Reading tables from ..." + filename);
		try {
			input = getClass().getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				System.err.println("Sorry, unable to find " + filename);
				maskerLogger.println( "Sorry, unable to find " + filename);
				System.exit(0);
			}
			prop.load(input);
			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String value = prop.getProperty(key);
				this.entityMaps.put(key, value);
			}
		} catch (IOException ex) {
			ex.printStackTrace(maskerLogger);
			System.err.println("Failed to start processing, Message:" + ex.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace(maskerLogger);
					System.err.println("Failed to close TablesToMask Property file, Message:" + e.getMessage());
					System.exit(0);
				}
			}
		}
		maskerLogger.println( "Entities loaded to memory ..."+entityMaps.toString());
		System.out.println("Entities loaded to memory ...");
		return this;
	}
	
	private void processMasking(){
		ExecutorService executor = Executors.newFixedThreadPool(10);

		for (Map.Entry<String, String> entry : this.entityMaps.entrySet()) {
			EntitiesThread worker = new EntitiesThread();
		    if ( worker.buildWorker(entry.getValue(), this.manager, this.maskerLogger) ){
		    	executor.execute(worker);
		    }else{
		    	maskerLogger.println("Worker thread for entity :"+entry.getValue() +" FAILED TO CREATE.");
		    }
		    maskerLogger.flush();
		}

		executor.shutdown();
        while (!executor.isTerminated()) {;}
        
        System.out.println("Data Masker Util ENDED.....Bye.");
        maskerLogger.println( "Data Masker Util ENDED.....Bye.");
	  }
	
}
