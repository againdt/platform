package com.getinsured.hix.platform.tools.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import com.getinsured.hix.platform.tools.util.PldPersonThread;

@Entity
@Table(name="pld_person")
@EntityListeners(PldPersonThread.class)
public class PldPerson implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public enum Eligible {
		YES, NO;
	}
	
	public enum ShopRelation {
		EMPLOYEE, SPOUSE, DEPENDENT;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Pld_Person_Seq")
	@SequenceGenerator(name = "Pld_Person_Seq", sequenceName = "pld_person_seq", allocationSize = 1)
	private int id;
    
  	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="BIRTH_DAY")
	private Date dob;
	
	@Column(name="external_id")
    private String externalId;
    
    @Lob
	@Column(name="person_data")
	private String personData;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getPersonData() {
		return personData;
	}

	public void setPersonData(String personData) {
		this.personData = personData;
	}
	

    

}

