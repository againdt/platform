/**
 * 
 */
package com.getinsured.wso2.identity.mgt.events.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.identity.core.persistence.JDBCPersistenceManager;
import org.wso2.carbon.identity.core.util.IdentityDatabaseUtil;
import org.wso2.carbon.user.core.UserStoreException;

import com.getinsured.wso2.identity.mgt.events.util.EncryptionUtil;


public class PasswordStoreManagerImpl  {

	private static Log log = LogFactory.getLog(PasswordStoreManagerImpl.class);
/*	private static final String UM_USER_PASSWORD = "UM_USER_PASSWORD";
	private static final String IS_PRIVILEGED = "isPrivileged";
	private static final String ATTR_PWD_TIMESTAMP = "passwordTimestamp";
	
	private static final String UM_ATTR_VALUE = "UM_ATTR_VALUE";
	
	*/
	private static final String ACCOUNT_TYPE_CODE = "accountTypeCode";
	private static int PRIVILEGED = 1;
	private static final String UM_ID = "UM_ID";
	
	private static final int TENANT_ID = -1234;
	private static String SELECT_PASS_QRY;
	private static final String INSERT_PASS_QRY = "insert into UM_PASSWORD_HISTORY(UM_ID, UM_USER_ID,UM_USER_PASSWORD,UM_TENANT_ID) values (UM_PASSWORD_HISTORY_SEQ.nextval, ?,?," + TENANT_ID + ")";
	private static final String MYSQL_SELECT_PASS_QRY = "select UM_USER_PASSWORD from UM_PASSWORD_HISTORY where UM_USER_ID = ? and UM_TENANT_ID = ? order by UM_TIME desc limit ";
	
	private static final String ORACLE_SELECT_PASS_QRY = "select * from (select UM_USER_PASSWORD from UM_PASSWORD_HISTORY where UM_USER_ID = ? and UM_TENANT_ID = ? order by UM_TIME desc) where ROWNUM <=";
	private static final String MYSQL_PRIVILEGED_USR_QRY = "select UM_USER_ATTRIBUTE.UM_ATTR_VALUE from UM_USER inner join UM_USER_ATTRIBUTE on UM_USER.UM_ID = UM_USER_ATTRIBUTE.UM_USER_ID where UM_USER.UM_USER_NAME = ? and UM_USER_ATTRIBUTE.UM_ATTR_NAME=? and UM_USER.UM_TENANT_ID = " + TENANT_ID;
	private static final String ORACLE_PRIVILEGED_USR_QRY = "select UM_USER_ATTRIBUTE.UM_ATTR_VALUE from UM_USER inner join UM_USER_ATTRIBUTE on UM_USER.UM_ID = UM_USER_ATTRIBUTE.UM_USER_ID where UM_USER.UM_USER_NAME = ? and UM_USER_ATTRIBUTE.UM_ATTR_NAME=? and UM_USER.UM_TENANT_ID = ?";

	private static String SELECT_PRIVILEGED_QRY;
	
	private static String SELECT_USER_ID_QRY;
	private static final String MYSQL_SELECT_USER_ID_QRY = "select UM_ID from UM_USER where UM_USER_NAME = ? and UM_TENANT_ID = " + TENANT_ID;
	private static final String ORACLE_SELECT_USER_ID_QRY = "select UM_ID from UM_USER where UM_USER_NAME = ? and UM_TENANT_ID = " + TENANT_ID ;

	
	public PasswordStoreManagerImpl() throws UserStoreException {
		super();
			SELECT_PRIVILEGED_QRY=ORACLE_PRIVILEGED_USR_QRY;
			SELECT_PASS_QRY = ORACLE_SELECT_PASS_QRY;
			SELECT_USER_ID_QRY = ORACLE_SELECT_USER_ID_QRY;
	}
	
	int getUserIdByName(String userName) throws Exception{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String query = SELECT_USER_ID_QRY;
		int userId = -1;
		try {
			conn = JDBCPersistenceManager.getInstance().getDBConnection();
			
			st = conn.prepareStatement(query);
			st.setString(1, userName);
			rs = st.executeQuery();
			
			
			if (null != rs && rs.next()) {
				userId = Integer.valueOf(rs.getString(UM_ID));
			}
			log.debug("getUserIdByName query executed, userId:" + userId);
			
		}catch (Exception ex) {
			log.error("getUserIdByName: ERR: WHILE PRFRMNG QRY: ", ex);
			throw ex;
		} finally {
			if (null != st) {
				IdentityDatabaseUtil.closeStatement(st);
			}
			if (null != conn) {
				IdentityDatabaseUtil.closeConnection(conn);
			}
		}
		return userId;
	}
	
	public void insertPasswordIntoDb(String userName, String password)
			throws Exception {
		String query2 = INSERT_PASS_QRY;
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			int userId = getUserIdByName(userName);
			if(userId != -1){
				conn = JDBCPersistenceManager.getInstance().getDBConnection();
				st = conn.prepareStatement(query2);
				st.setInt(1, userId);
				st.setString(2, EncryptionUtil.getInstance().encryptPasswordWithHash(password));
				st.execute();
				log.info("new user: timestamp updated");
				conn.commit();
					
			}
			
		} catch (Exception ex) {
			log.error("insertPasswordIntoDb: ERR: WHILE PRFRMNG QRY: ", ex);
			throw ex;
		} finally {
			if (null != st) {
				IdentityDatabaseUtil.closeStatement(st);
			}
			if (null != conn) {
				IdentityDatabaseUtil.closeConnection(conn);
			}
		}

	}
	
	/*
	 * (non-Javadoc)
	 * @see com.getinsured.wso2.identity.mgt.password.policy.dao.PasswordStoreManager#isPrivilegedUser(java.lang.String)
	 */
	public boolean isPrivilegedUser(String userName) throws Exception {
		boolean isPrivileged = false;
		String query = SELECT_PRIVILEGED_QRY;
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			conn = JDBCPersistenceManager.getInstance().getDBConnection();
			st = conn.prepareStatement(query);
			st.setString(1, userName);
			st.setString(2, ACCOUNT_TYPE_CODE);
			st.setInt(3,TENANT_ID );
			rs = st.executeQuery();
			while (null != rs && rs.next()) {
				final String isPrivilegedStr = rs.getString(1); // ("UM_USER_ATTRIBUTE.UM_ATTR_VALUE");
				if(null != isPrivilegedStr && !isPrivilegedStr.isEmpty()) {
					isPrivileged = Integer.valueOf(isPrivilegedStr) == PRIVILEGED;
				}
			}
		} catch (Exception ex) {
			log.error("isPrivilegedUser ERR: WHILE PRFRMNG QRY: ", ex);
			throw ex;
		} finally {
			if (null != rs) {
				IdentityDatabaseUtil.closeResultSet(rs);
			}
			if (null != st) {
				IdentityDatabaseUtil.closeStatement(st);
			}
			if (null != conn) {
				IdentityDatabaseUtil.closeConnection(conn);
			}
		}
		
		return isPrivileged;
	}

}