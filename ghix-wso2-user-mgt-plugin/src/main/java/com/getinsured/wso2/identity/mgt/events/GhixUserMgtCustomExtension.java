package com.getinsured.wso2.identity.mgt.events;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.identity.mgt.IdentityMgtConfig;
import org.wso2.carbon.user.core.UserStoreException;
import org.wso2.carbon.user.core.UserStoreManager;
import org.wso2.carbon.user.core.common.AbstractUserOperationEventListener;

import com.getinsured.wso2.identity.mgt.events.jdbc.PasswordStoreManagerImpl;

/**
 *
 */
public class GhixUserMgtCustomExtension extends AbstractUserOperationEventListener {

    private static Log log = LogFactory.getLog(GhixUserMgtCustomExtension.class);
    PasswordStoreManagerImpl passwordStoreManager = null;
    private IdentityMgtConfig identityMgtConfig;

    public GhixUserMgtCustomExtension(){
    	try {
			passwordStoreManager = new PasswordStoreManagerImpl();
		} catch (UserStoreException e) {
			log.error("Error in contruction of listener: " , e);
		}
		identityMgtConfig = IdentityMgtConfig.getInstance();
    }
    
    
    @Override
    public int getExecutionOrderId() {
        return 9883;
    }


    @Override
    public boolean doPreAuthenticate(String userName, Object credential,
                                     UserStoreManager userStoreManager) throws UserStoreException {

        // log.info("doPreAuthenticate method is called before authenticating with user store");
    	//TODO: Add if any implementation required
        return true;
    }

    @Override
    public boolean doPostAuthenticate(String userName, boolean authenticated, UserStoreManager userStoreManager) throws UserStoreException {

    	//log.info("doPreAuthenticate method is called after authenticating with user store");
    	
        if(authenticated){
        	//TODO: Add if any implementation required           
        }

        return true;

    }

	@Override
	public boolean doPreAddUser(String userName, Object credential, String[] roleList,
	                            Map<String, String> claims, String profile,
	                            UserStoreManager userStoreManager) throws UserStoreException {

		if (log.isInfoEnabled()) {
//			log.info("Pre add user is called in GhixUserMgtCustomExtension");
		}
		return true;
	}

	@Override
	public boolean doPostAddUser(String userName, Object credential, String[] roleList,
	                             Map<String, String> claims, String profile,
	                             UserStoreManager userStoreManager) throws UserStoreException {
		if (log.isInfoEnabled()) {
			log.info("GhixUserMgtCustomExtension : user added");
		}
		
		if (credential != null &&  credential.toString().trim().length() > 0) {
//				log.info("inside Post add user name = " + userName + " and password = " + credential);
			
			try {
					passwordStoreManager.insertPasswordIntoDb(userName, credential.toString());
					
			} catch (Exception e) {
				log.error("Exception creating history for new user", e);
			}
			
		}

		return true;	
	}
	
	@Override
    public boolean doPostUpdateCredential(String userName, UserStoreManager userStoreManager)
                                                                 throws UserStoreException {
		long currentTimestamp = new Date().getTime();
		try {
			userStoreManager.setUserClaimValue(userName, "http://wso2.org/claims/passwordTimestamp", String.valueOf(currentTimestamp), "default");
			return true;
		}catch (Exception e) {
			log.error("Exception updating the password time stamp", e);
		}
		return true;
    }
	
	@Override
    public boolean doPreUpdateCredentialByAdmin(String userName, Object newCredential,
                                    UserStoreManager userStoreManager) throws UserStoreException {
		long currentTimestamp = new Date().getTime();
		try {
			userStoreManager.setUserClaimValue(userName, "http://wso2.org/claims/passwordTimestamp", String.valueOf(currentTimestamp), "default");
			return true;
		}catch (Exception e) {
			log.error("Exception updating the password time stamp", e);
		}
		return true;
    }


}