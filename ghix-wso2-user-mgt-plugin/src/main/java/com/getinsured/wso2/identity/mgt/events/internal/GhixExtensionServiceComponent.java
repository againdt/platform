/*
 * Copyright (c)  WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.getinsured.wso2.identity.mgt.events.internal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.wso2.carbon.user.core.listener.UserOperationEventListener;

import com.getinsured.wso2.identity.mgt.events.GhixUserMgtCustomExtension;


/**
 * 
 * @scr.component name="com.getinsured.wso2.identity.mgt.events.internal.GhixExtensionServiceComponent"
 * immediate="true"
 */

public class GhixExtensionServiceComponent {

    private static Log log = LogFactory.getLog(GhixExtensionServiceComponent.class);

    @SuppressWarnings("rawtypes")
	private ServiceRegistration serviceRegistration = null;

    protected void activate(ComponentContext context) {

        GhixUserMgtCustomExtension listener = new GhixUserMgtCustomExtension();
        serviceRegistration =
                context.getBundleContext().registerService(UserOperationEventListener.class.getName(),
                        listener, null);

        log.debug("Ghix user mgmt bundle is activated");
    }


    protected void deactivate(ComponentContext context) {
        log.debug("Ghix user mgmt bundle is de-activated");
    }
}