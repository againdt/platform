package com.getinsured.hix.model.batch;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="BATCH_JOB_EXECUTION")
public class BatchJobExecution implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "JOB_EXECUTION_ID")
	private int id;
	
	@Column(name= "VERSION")
	private int version;
	
	@OneToOne
	@JoinColumn(name="JOB_INSTANCE_ID")
	private BatchJobInstance batchJobInstance;
	
	@Column(name="CREATE_TIME")
	private Date createdOn;
	
	@Column(name="START_TIME")
	private Date startTime;
	
	@Column(name="END_TIME")
	private Date endTime;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="EXIT_CODE")
	private String exitCode;
	
	@Column(name="EXIT_MESSAGE")
	private String exitmessage;
	
	@Column(name="LAST_UPDATED")
	private Date updatedOn;
	
	@Column(name="JOB_CONFIGURATION_LOCATION")
	private String jobConfigurationLocation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public BatchJobInstance getBatchJobInstance() {
		return batchJobInstance;
	}

	public void setBatchJobInstance(BatchJobInstance batchJobInstance) {
		this.batchJobInstance = batchJobInstance;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExitCode() {
		return exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	public String getExitmessage() {
		return exitmessage;
	}

	public void setExitmessage(String exitmessage) {
		this.exitmessage = exitmessage;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getJobConfigurationLocation() {
		return jobConfigurationLocation;
	}

	public void setJobConfigurationLocation(String jobConfigurationLocation) {
		this.jobConfigurationLocation = jobConfigurationLocation;
	}
}
