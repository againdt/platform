package com.getinsured.hix.model.batch;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the ACCOUNT_RECEIVEABLE oracle view.
 * @author parhi_s
 */
@Entity
@Table(name="BATCH_JOB_INSTANCE")
public class BatchJobInstance implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "JOB_INSTANCE_ID")
	private int id;

	@Column(name= "VERSION")
	private int version;
	
	@Column(name="JOB_NAME")
	private String jobName;
	
	@Column(name="JOB_KEY")
	private String jobKey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobKey() {
		return jobKey;
	}

	public void setJobKey(String jobKey) {
		this.jobKey = jobKey;
	}
	
	
}
