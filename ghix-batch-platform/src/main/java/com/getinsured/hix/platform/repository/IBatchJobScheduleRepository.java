package com.getinsured.hix.platform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.platform.batch.BatchJobSchedule;


public interface IBatchJobScheduleRepository extends JpaRepository<BatchJobSchedule, Long>{

	@Query("FROM  BatchJobSchedule as js where js.jobName=:jobName")
	BatchJobSchedule findByJobName(@Param("jobName") String jobName);
	
}