package com.getinsured.hix.platform.batch.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.getinsured.hix.model.batch.BatchJobExecution;
import org.springframework.batch.core.JobExecution;

public interface BatchJobExecutionService {

	Integer getHighestInstanceIDByJobName(String jobName,String status);
	
	Date getLastJobRunDate(String jobName);
	
	Date getLastJobRunDate(String jobName, String status);
	
	List<BatchJobExecution> findRunningJob(String jobname);

	Set<JobExecution> findStoppingJobs();
}
