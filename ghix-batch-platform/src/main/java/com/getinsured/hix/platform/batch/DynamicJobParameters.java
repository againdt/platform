package com.getinsured.hix.platform.batch;

import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;

public class DynamicJobParameters implements JobParametersIncrementer {
	private static String run_ID="runId";     
	public JobParameters getNext(JobParameters parameters){
		long id=0;        
		if (parameters==null || parameters.isEmpty()) {           
			id=1;         
		}
		else{ 
			id = parameters.getLong(run_ID,1L) + 1; 
        } 
		JobParametersBuilder builder = new JobParametersBuilder(parameters); 
		builder.addLong(run_ID, id).toJobParameters(); 
		builder.addLong("EXECUTION_DATE", new Date().getTime());  
		return builder.toJobParameters(); 
	}
}