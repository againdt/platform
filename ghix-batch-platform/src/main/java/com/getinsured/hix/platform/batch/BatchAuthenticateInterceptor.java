package com.getinsured.hix.platform.batch;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;

public class BatchAuthenticateInterceptor extends HandlerInterceptorAdapter{

	private static Log LOG = LogFactory.getLog(BatchAuthenticateInterceptor.class);
	
	private UserService userService;
	
	public UserService getUserService() {
		return userService;
	}

    public void setUserService(UserService userService) {
		this.userService = userService;
	}

    @Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
    	LOG.info("Received the request in BatchAuthenticateInterceptor");
		
		AccountUser user = null;
		try{
           user = userService.getLoggedInUser();
           if(!userService.isAdmin(user)){	
        	   response.sendRedirect(request.getContextPath()+"/account/user/login");
        	   return false;
           }
        }catch(InvalidUserException ex){
        	response.sendRedirect(request.getContextPath()+"/account/user/login");
        	return false;
        }
		return super.preHandle(request, response, handler);
	}
}
