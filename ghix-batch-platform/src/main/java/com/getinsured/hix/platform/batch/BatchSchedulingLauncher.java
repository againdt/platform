package com.getinsured.hix.platform.batch;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;

public class BatchSchedulingLauncher {
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchSchedulingLauncher.class);
	public static String RUN_ID="runId";     
	public static String EXECUTION_DATE="executionDate";
	private Job job;
	
	private JobLauncher jobLauncher;
	private HashMap<String,String> jobParameterMap;
	private JobRepository repo;
	private static SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z".intern());
	private Map<String, JobParameters> previousExecutions = Collections.synchronizedMap(new HashMap<String,JobParameters>());
	
	private static Map<String, BatchSchedulingLauncher>  launcherInstances = Collections.synchronizedMap(new HashMap<String, BatchSchedulingLauncher>());
	
	private BatchSchedulingLauncher(Job job,JobLauncher jobLauncher, JobRepository repo, HashMap<String, String> jobParameterMap) {
		this.job = job;
		this.jobLauncher = jobLauncher;
		this.repo = repo;
		this.jobParameterMap = jobParameterMap;
	}
	
	public static BatchSchedulingLauncher getBatchSchedulingLauncher(Job job, JobLauncher jobLauncher, JobRepository repo) {
		
		return getBatchSchedulingLauncher( job,  jobLauncher,  repo, null);
	}
	
	public static BatchSchedulingLauncher getBatchSchedulingLauncher(Job job, JobLauncher jobLauncher, JobRepository repo, HashMap<String, String> jobParameterMap) {
		String name = job.getName();
		BatchSchedulingLauncher launcher  = launcherInstances.get(name);
		if(launcher == null) {
			launcher = new BatchSchedulingLauncher(job, jobLauncher, repo, jobParameterMap);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Ceating a new scheduling launcher for job {}", job.getName());
			}
			launcherInstances.put(name, launcher);
		}else {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Scheduling launcher already created for job :"+job.getName());
			}
		}
		return launcher;
	}
	
	public synchronized void launch() throws JobExecutionException {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(Thread.currentThread().getName() +  ": launch called for job: {} ", this.job.getName());
		}
		JobParameters params = this.previousExecutions.get(this.job.getName());
		
		if(params != null) {
			JobExecution fromDB = this.repo.getLastJobExecution(this.job.getName(), params);
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info(Thread.currentThread().getName() +"last job instance id: {} ", fromDB.getJobInstance().getId());
			}
			if(fromDB != null) {
				long id = fromDB.getId();
				BatchStatus x = fromDB.getStatus();
				if(x == BatchStatus.STARTED || x == BatchStatus.STARTING) {
					LOGGER.error("Previous execution launched at {} with ID {} is still running, can not launch another", params.getString(EXECUTION_DATE), fromDB.getJobId());
					throw new JobExecutionException("Job with id "+id+" ["+this.job.getName()+"] launched at ["+params.getString(EXECUTION_DATE)+"] still running, tryafter some time");
				}else {
					Long lastexeTime = params.getLong("EXECUTION_DATE");
					if(lastexeTime != null){
						   long timediff = System.currentTimeMillis() - lastexeTime;
						   
						   if(timediff < 290000){
							   LOGGER.error(Thread.currentThread().getName() +": Previous execution launched at {} with ID {}, can not launch another", params.getString(EXECUTION_DATE), fromDB.getJobId());
								throw new JobExecutionException("Job with id "+id+" ["+this.job.getName()+"] launched at ["+params.getString(EXECUTION_DATE)+"] , tryafter some time");
						   }
					}
					this.previousExecutions.remove(this.job.getName());
				}
				
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug(Thread.currentThread().getName() +"Previous execution for {} founf witn status as {}", this.job.getName(), x);
				}
			}
		}
		JobParameters jobParams = createJobParameters();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(Thread.currentThread().getName() +"Launching Job {} on {}", job.getName()) ; //, sdf.format(new Date(System.currentTimeMillis()).toString()));
		}
		
		JobExecution execution = jobLauncher.run(job, jobParams);
		this.previousExecutions.put(this.job.getName(), jobParams);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(Thread.currentThread().getName() +"Finished launching Job {} , this.previousExecutions size =  {}", job.getName(), this.previousExecutions.size()); // + sdf.format(new Date(System.currentTimeMillis()).toString()));
		}
	}
	
	private JobParameters createJobParameters() {
		JobParametersBuilder builder = new JobParametersBuilder();
		if(jobParameterMap!=null){
			Iterator<String> it = jobParameterMap.keySet().iterator();
			while (it.hasNext()) {
		        String paramName = it.next();
		        builder.addString(paramName,jobParameterMap.get(paramName));
		    }
		}
		Date dt = new Date();
		//builder.addString("EXECUTION_DATE", sdf.format(dt.getTime()));  
		builder.addLong("EXECUTION_DATE", dt.getTime());  
		return builder.toJobParameters();
	}

	public HashMap<String, String> getJobParameterMap() {
		return jobParameterMap;
	}
	
	public Job getJob() {
		return job;
	}

	public void setJobParameterMap(HashMap<String, String> jobParameterMap) {
		this.jobParameterMap = jobParameterMap;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((job.getName() == null) ? 0 : job.getName().hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BatchSchedulingLauncher other = (BatchSchedulingLauncher) obj;
		if (job.getName() == null) {
			if (other.job.getName() != null)
				return false;
		} else if (!job.getName().equals(other.job.getName())) {
			return false;
		}
		return true;
	}
	
}