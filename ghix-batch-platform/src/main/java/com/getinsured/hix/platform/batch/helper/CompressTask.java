 package com.getinsured.hix.platform.batch.helper;
 
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

 
public class CompressTask implements Tasklet {
	  private static final Logger LOGGER = LoggerFactory.getLogger(CompressTask.class);
      
	  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    	  
    	  ExecutionContext jobexecutionContext = arg1.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    	  if (jobexecutionContext.containsKey("outputFile"))
  	      {
  	    	String filePath =  jobexecutionContext.getString("outputFile");
  	    	byte[] buffer = new byte[1024];
  	    	FileInputStream in = null;
  	    	ZipOutputStream zos = null;
  	    	FileOutputStream fos = null;
  	    	try{
  	    		int lastIndex = filePath.lastIndexOf('/');
  	    		
  	    		fos = new FileOutputStream(filePath.substring(5, filePath.lastIndexOf('.')+1)+"zip");
  	    		zos = new ZipOutputStream(fos);
  	    		ZipEntry ze= new ZipEntry(filePath.substring(lastIndex+1,filePath.length()));
  	    		zos.putNextEntry(ze);
  	    		
  	    		String newfilepath = filePath.substring(5, filePath.length());
  	    		
  	    		in = new FileInputStream(newfilepath);
  	 
  	    		int len;
  	    		while ((len = in.read(buffer)) > 0) {
  	    			zos.write(buffer, 0, len);
  	    		}
  	 
  	    		zos.closeEntry();
  	    		
  	    	}catch(IOException ex){
  	    	   LOGGER.error("IOException occurred while compressing file"+ ex);
            }finally {
            	IOUtils.closeQuietly(in);
            	IOUtils.closeQuietly(zos);
            	IOUtils.closeQuietly(fos);
        	}
          }
          return RepeatStatus.FINISHED;
      }
}
