package com.getinsured.hix.platform.batch.helper.xmlvalidator;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XmlErrorHandler implements ErrorHandler {
	
	private List<SAXParseException> warnings = new ArrayList<SAXParseException>();
	private List<SAXParseException> errors = new ArrayList<SAXParseException>();
	private List<SAXParseException> fatalErrors = new ArrayList<SAXParseException>();
	
	public void warning(SAXParseException saxParseException) throws SAXException{
		warnings.add(saxParseException);
	}
	
	public void error(SAXParseException saxParseException) throws SAXException{
		errors.add(saxParseException);
	}
	
	public void fatalError(SAXParseException saxParseException) throws SAXException{
		fatalErrors.add(saxParseException);
	}
	
	public boolean checkErrors(){
		if(errors.size()>0||fatalErrors.size()>0){
//			System.out.println(errors.get(0).toString());
//			System.out.println(errors.get(0).getMessage());
			return true;
		}
		else{
			return false;
		}
	}
	
	public String toString(){
		return "Warnings="+warnings.size()+"Errors="+errors.size()+"FatalErrors="+fatalErrors.size();
	}
}