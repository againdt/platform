package com.getinsured.hix.platform.batch.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

@Component
@Scope("prototype")
public class MonitoringExecutionListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringExecutionListener.class);
	@Autowired private EmailMonitoringNotifier monitoringNotifier;
	
	
	@AfterJob
	public void executeAfterJob(JobExecution jobExecution) {
		try{
			Notice noticeObj = null;
			monitoringNotifier.setJobExecution(jobExecution);
			noticeObj = monitoringNotifier.generateEmail();
			monitoringNotifier.sendEmail(noticeObj);
 		}catch (NotificationTypeNotFound e) {
			LOGGER.error("Notification type \"EmailMonitoringNotifier\" not found. Email can not be sent.",e);
		}
	}
}
