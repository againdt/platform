package com.getinsured.hix.platform.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.platform.batch.BatchJobSchedule;
import com.getinsured.hix.platform.repository.IBatchJobScheduleRepository;



@Service("BatchScheduleService")
@Transactional( propagation = Propagation.REQUIRES_NEW )
public class BatchScheduleService{
	@Autowired (required=true) private IBatchJobScheduleRepository batchJobScheduleRepository;
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchScheduleService.class);

		
	@Transactional
	public BatchJobSchedule saveSchedule( BatchJobSchedule batchSchedule) {
		LOGGER.debug("Saving job schedule in db");
		return batchJobScheduleRepository.saveAndFlush(batchSchedule);
	}
	
	@Transactional
	public BatchJobSchedule getScheduleByJobName( String jobName) {
		LOGGER.debug("Getting job schedule from db for job" + jobName);
		return (BatchJobSchedule) batchJobScheduleRepository.findByJobName(jobName);
	}
}
