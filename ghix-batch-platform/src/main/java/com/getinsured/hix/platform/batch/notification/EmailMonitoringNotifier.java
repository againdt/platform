package com.getinsured.hix.platform.batch.notification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;


@Component
@Scope("prototype")
public class EmailMonitoringNotifier extends NotificationAgent{
	private JobExecution jobExecution;
	private Map<String, String> singleData;
	
	public void setJobExecution(JobExecution jobExecution) {
		this.jobExecution = jobExecution;
	}
	
	@Override
	public Map<String, String> getSingleData() {
		String exceptionDetails = "";
		Map<String, String> data = new HashMap<String, String>();
		this.singleData = new HashMap<String, String>();
		DateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
		
		String subject = "Email Monitoring Notifier for : " + jobExecution.getJobInstance().getJobName() + " : Status :"+jobExecution.getStatus();
		singleData.put("Subject", subject);
		Collection<StepExecution> stepExecutions =  jobExecution.getStepExecutions();
		
		for (StepExecution stepExecution : stepExecutions) {
			exceptionDetails = stepExecution.getExitStatus().getExitDescription();
		}
		
		data.put("jobExecution", String.valueOf(jobExecution.getId()));
		data.put("jobInstance", String.valueOf(jobExecution.getJobInstance().getId()));
		data.put("jobStatus", jobExecution.getStatus().toString());
		data.put("jobExecutionURL", GhixPlatformEndPoints.APPSERVER_URL +"ghix-batch/jobs/jobs/executions/"+jobExecution.getId());
		data.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		
		exceptionDetails = (exceptionDetails.equals("")) ? "NA" : exceptionDetails;
		
		data.put("jobExceptions", exceptionDetails);
		data.put("systemDate",dateFormat.format(new Date()));
		setTokens(data);
		
		return singleData;
	}
}