package com.getinsured.hix.platform.batch.helper;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class FTPFileDownload{

	private static Log log = LogFactory.getLog(FTPFileDownload.class);
	
    public void downloadFile(String server, String userName, String password, String remoteFileName, String localFilePath) throws MalformedURLException,FileNotFoundException,IOException{
        
    	log.info("Connecting to FTP server...");    
    	BufferedInputStream in = null;
    	FileOutputStream out = null;
    	
        //Connection String
    	try{
	        URL url = new URL("ftp://"+userName+":"+password+"@"+server+"/"+remoteFileName+";type=i");
	        //URL url = new URL("ftp://"+userName+":"+password+"@"+server+"/"+fileName+";type=i");
	        URLConnection con = url.openConnection();
	     
	        in = new BufferedInputStream(con.getInputStream());
	     
	        log.info("Downloading file.");
	     
	        //Downloads the selected file to the C drive
	        out = new FileOutputStream(localFilePath);
	     
	        int i = 0;
	        byte[] bytesIn = new byte[1024];
	        while ((i = in.read(bytesIn)) >= 0) {
	            out.write(bytesIn, 0, i);
	        }
	      
	        log.info("File downloaded.");
	     
	    }
        catch(MalformedURLException urlExp){
        	log.error("Wrong Format of Url.", urlExp);
        	throw urlExp;
        }
    	catch(FileNotFoundException fileExp){
        	log.error("Local file path is wrong or remote file is not found.", fileExp);
        	throw fileExp;
        }
    	catch(IOException ioExp){
        	log.error("Not able to open connection with ftp server or not able to read from ftp", ioExp);
        	throw ioExp;
        }finally{
        	IOUtils.closeQuietly(in);
        	IOUtils.closeQuietly(out);     
        }
    	
     }
}