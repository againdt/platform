package com.getinsured.hix.platform.batch;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="BATCH_JOB_SCHEDULES")
public class BatchJobSchedule implements Serializable {

	private static final long serialVersionUID = 2453213624867956087L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Job_Schedule_Seq")
	@SequenceGenerator(name = "Job_Schedule_Seq", sequenceName = "JOB_SCHEDULE_SEQ", allocationSize = 1)
	private Long id;
	
	
	@Column(name="JOB_NAME")
	private String jobName;
	
		
	@Column(name="JOB_SCHEDULE")
	private String jobSchedule;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobSchedule() {
		return jobSchedule;
	}
	public void setJobSchedule(String jobSchedule) {
		this.jobSchedule = jobSchedule;
	}
	
	
}
