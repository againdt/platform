package com.getinsured.hix.platform.batch.service;

import java.util.Date;
import java.util.List;
import java.util.Set;


import com.getinsured.hix.platform.repository.BatchJobSearchDao;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;

import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.repository.IBatchJobExecutionRepository;

//import com.getinsured.hix.platform.batch.repository.IBatchJobExecutionRepository;

@Service("batchJobExecutionService")
@Transactional
public class BatchJobExecutionServiceImpl implements BatchJobExecutionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchJobExecutionServiceImpl.class);
	@Autowired
	private IBatchJobExecutionRepository batchJobExecutionRepository;

	@Autowired
	BatchJobSearchDao batchJobSearchDao;

	@Override
	public Integer getHighestInstanceIDByJobName(String jobName, String status) {
		Integer highestID = null;

		try {

			highestID = batchJobExecutionRepository.findHighestInstanceIDByJobName(status, jobName);
		} catch (Exception e) {
			LOGGER.error("Error in getHighestInstanceIDByJobName() "+ e.getMessage());
		}
		return highestID;
	}

	@Override
	public Date getLastJobRunDate(String jobName) {
		Date lastRunDate = null;
		
		try {
			lastRunDate = batchJobExecutionRepository.findLastRunDateByJobName(jobName);
		} catch (Exception e) {
			
			LOGGER.error("Error in getLastJobRunDate() " + e.getMessage());
		}
		return lastRunDate;
	}

	@Override
	public Date getLastJobRunDate(String jobName, String status) {
		Date lastRunDate = null;
		try {
			List<BatchJobExecution> executionList= batchJobExecutionRepository.findLastRunJob(status, jobName );
			if(executionList!=null && !executionList.isEmpty()){
				lastRunDate=executionList.get(0).getStartTime();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getLastJobRunDate() " + e.getMessage());
		}
		return lastRunDate;
	}

	@Override
	public List<BatchJobExecution> findRunningJob(String jobname) {
		return batchJobExecutionRepository.findRunningJob(jobname);
	}

	@Override
	public Set<JobExecution> findStoppingJobs() {
		return batchJobSearchDao.findStoppingJobExecutions();
	}
}
