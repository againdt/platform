package com.getinsured.hix.platform.batch.helper.xmlvalidator;

import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.util.GhixUtils;

public class XmlValidator implements Tasklet, ApplicationContextAware {
	
	private Resource xmlFile;
	private Resource xsdFile;
	private String xsdFilePath;
	private ApplicationContext applicationContext;
	 
	public void setApplicationContext(ApplicationContext applicationContext) 
    {        
		this.applicationContext = applicationContext;  
	} 
	
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
  	  
		ExecutionContext jobexecutionContext = arg1.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		
		if (jobexecutionContext.containsKey("outputFile"))
	    {
			xmlFile = applicationContext.getResource(jobexecutionContext.getString("outputFile")); 
			xsdFile = applicationContext.getResource(xsdFilePath); 
  	    	DocumentBuilderFactory dbf = GhixUtils.getDocumentBuilderFactoryInstance();
  	    	dbf.setNamespaceAware(true);
  	    	InputStream isXML = null;
  	    	InputStream isXSD = null;
  	    	try{
  	    		DocumentBuilder parser = dbf.newDocumentBuilder();
		  	    isXML = xmlFile.getInputStream();
		  	    Document document = parser.parse(isXML);
  	    	
  	    		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
  	    	
		  	    isXSD = xsdFile.getInputStream();
		  	   
		  	   	Source schemaFile = new StreamSource(isXSD);
	  	    	Schema schema = factory.newSchema(schemaFile);
	  	    	
	  	    	Validator validator = schema.newValidator();
	  	    	validator.setErrorHandler(new XmlErrorHandler());
	  	    	
	  	    	try{
	  	    		validator.validate(new DOMSource(document));
	  	    	}
	  	    	catch(SAXException ex){
	  	    		throw new RuntimeException(ex);
	  	    	}
	  	    	XmlErrorHandler myErrorHandler = (XmlErrorHandler)validator.getErrorHandler();
	  	    	if(myErrorHandler.checkErrors()){
	  	    		throw new RuntimeException("xml is not valid !");
		  	    	}
	  	    	}catch(Exception e){
	  	    		if(isXML != null){
	  	    			isXML.close();
	  	    		}
	  	    		if(isXSD != null){
	  	    			isXSD.close();
	  	    		}
	  	    		throw e;
	  	    	}finally{
	  	    		IOUtils.closeQuietly(isXML);
	  	    		IOUtils.closeQuietly(isXSD);
	  	    	}
  	    	return RepeatStatus.FINISHED;
		}
		else{
			throw new RuntimeException("No xml file available to validate");
		}
  	}

	public String getXsdFilePath() {
		return xsdFilePath;
	}

	public void setXsdFilePath(String xsdFilePath) {
		this.xsdFilePath = xsdFilePath;
	}
	
}