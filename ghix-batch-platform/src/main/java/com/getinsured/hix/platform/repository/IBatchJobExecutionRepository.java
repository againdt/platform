package com.getinsured.hix.platform.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.getinsured.hix.model.batch.BatchJobExecution;

@Transactional(readOnly = true)
public interface IBatchJobExecutionRepository extends JpaRepository<BatchJobExecution, Integer> {

	@Query("select MAX(be.batchJobInstance.id) From BatchJobExecution as be where be.status = :status and be.batchJobInstance.jobName = :jobName")
	Integer findHighestInstanceIDByJobName( @Param("status") String status,@Param("jobName") String jobName);
	
//	@Query("select be.startTime From BatchJobExecution as be where be.batchJobInstance.id = :id and  be.status = 'COMPLETED'")
//	Date findLastRunDateByJobID( @Param("id") int id);
	
	@Query(value = "SELECT START_TIME " +
			" FROM " +
			" (SELECT BE.START_TIME " +
			" FROM BATCH_JOB_EXECUTION BE, " +
			" BATCH_JOB_INSTANCE BI " +
			" WHERE BE.STATUS        = 'COMPLETED' " +
			" AND BE.JOB_INSTANCE_ID = BI.JOB_INSTANCE_ID " +
			" AND BI.JOB_NAME        = :jobName " +
			" ORDER BY BE.START_TIME DESC " +
			" ) WHERE ROWNUM = 1", nativeQuery = true)
	Date findLastRunDateByJobName(@Param("jobName") String jobName);
	
	@Query("FROM BatchJobExecution as be where be.batchJobInstance.jobName = :jobName and  be.status = :status and be.createdOn=(SELECT MAX(createdOn) FROM BatchJobExecution where batchJobInstance.jobName = :jobName and  status = :status ) ORDER BY be.createdOn DESC")
	 List<BatchJobExecution> findLastRunJob( @Param("status") String status,@Param("jobName") String jobName);
	
	@Query(" FROM BatchJobExecution as be where be.batchJobInstance.jobName = :jobName " +
			" AND  be.status IN ('STARTING','STARTED') " +			
			" ORDER BY be.createdOn DESC")
	 List<BatchJobExecution> findRunningJob(@Param("jobName") String jobName);
	
}
