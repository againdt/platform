/**
 * 
 */
package com.getinsured.wso2.identity.mgt.password.policy;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.identity.mgt.policy.AbstractPasswordPolicyEnforcer;

import com.getinsured.wso2.identity.mgt.password.policy.jdbc.PasswordJdbcStoreManagerImpl;
import com.getinsured.wso2.identity.mgt.password.policy.util.EncryptionUtil;

/**
 * @author Biswakesh
 * 
 */
public class GhixPasswordHistory extends AbstractPasswordPolicyEnforcer {

	private static final Log log = LogFactory.getLog(GhixPasswordHistory.class);
	private static Boolean isPasswordHistoryEnabled = true;
	private static Integer passwordHistoryLimit = 24;
	private static String errorCode = "30000";
	public static String dbType = "oracle";
	private PasswordJdbcStoreManagerImpl passwordStoreManager = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.wso2.carbon.identity.mgt.policy.PolicyEnforcer#init(java.util.Map)
	 */
	@Override
	public void init(Map<String, String> params) {

		try {
			isPasswordHistoryEnabled = Boolean.parseBoolean(params
					.get("history.enable"));
			GhixPasswordHistory.passwordHistoryLimit = Integer.parseInt(params
					.get("history.limit"));
			GhixPasswordHistory.errorCode = params.get("history.errorCode");
			GhixPasswordHistory.dbType = params.get("history.dbType");
			passwordStoreManager = new PasswordJdbcStoreManagerImpl();
		} catch (Exception ex) {
			log.error("ERR: WHILE READING PASS HSTRY PARAMS: ", ex);
		}

		if (log.isInfoEnabled()) {
			log.info("INIT CUSTOM PASS HISTORY POLICY: PARAMS: ISPASSWORDHISTORYENABLED: "
					+ GhixPasswordHistory.isPasswordHistoryEnabled
					+ " PASSWORDHISTORYLIMIT: "
					+ GhixPasswordHistory.passwordHistoryLimit
					+ " ERR CODE: "
					+ GhixPasswordHistory.errorCode
					+ " DB TYPE: "
					+ GhixPasswordHistory.dbType);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.wso2.carbon.identity.mgt.policy.PolicyEnforcer#enforce(java.lang.
	 * Object[])
	 */
	@Override
	public boolean enforce(Object... args) {
		boolean isPasswordNotAvailable = false;

		try {
			if (GhixPasswordHistory.isPasswordHistoryEnabled
					&& passwordStoreManager
							.isPrivilegedUser(args[1].toString())) {
				if (log.isInfoEnabled()) {
					log.info("INSIDE ENFORCE: USR AND PASS PICKED FRM REQ: ");
				}
				isPasswordNotAvailable = isPasswordNotAvailableInPasswordHistory(
						args[1].toString(), args[0].toString());
			} else {
				isPasswordNotAvailable = true;
			}
		} catch (Exception e) {
			log.error("ERR: WHILE ENFORCING PASS POLICY: ", e);
		}

		return isPasswordNotAvailable;

	}

	/**
	 * Validate password with regular expression
	 * 
	 * @param password
	 *            password for validation
	 * @return true valid password, false invalid password
	 */
	public boolean isPasswordNotAvailableInPasswordHistory(String userEmailId,
			String password) {
		boolean isPasswordNotAvailable = false;
		List<String> userHistoryList = null;

		try {
			if (GhixPasswordHistory.passwordHistoryLimit == 0) {
				isPasswordNotAvailable = true;
			} else {
				userHistoryList = passwordStoreManager.fetchPasswordsFromDb(
						userEmailId, GhixPasswordHistory.passwordHistoryLimit);
				if (null == userHistoryList
						|| userHistoryList.isEmpty()
						|| !userHistoryList.contains(EncryptionUtil
								.getInstance()
								.encryptPasswordWithHash(password))) {
					isPasswordNotAvailable = true;
				}
			}

			if (isPasswordNotAvailable) {
				passwordStoreManager
						.insertPasswordIntoDb(userEmailId, password);
			} else {
				errorMessage = GhixPasswordHistory.errorCode;
			}
		} catch (Exception e) {
			log.error("ERR: WHILE CHCKNG PASS HISTORY: ", e);
		}

		if (log.isInfoEnabled()) {
			log.info(" IS PASSWORD USED LAST "
					+ GhixPasswordHistory.passwordHistoryLimit + " TIMES: "
					+ !isPasswordNotAvailable);
		}

		return isPasswordNotAvailable;

	}

}
