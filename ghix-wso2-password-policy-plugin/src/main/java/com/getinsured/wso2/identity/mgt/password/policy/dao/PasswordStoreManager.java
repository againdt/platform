/**
 * 
 */
package com.getinsured.wso2.identity.mgt.password.policy.dao;

import java.util.List;

/**
 * Define methods to perform db queries wrt password polciy
 * @author Biswakesh
 *
 */
public interface PasswordStoreManager {
	/**
	 * Fetch the last n passwords from the um_password_history table
	 * 
	 * @param userName String containing the userName for which the check is to be done
	 * @param passwordLimit int containing the history limit to be checked for each password
	 * @return List containing last n passwords
	 * 
	 * @throws Exception
	 */
	public List<String> fetchPasswordsFromDb(String userName, int passwordLimit) throws Exception;
	
	/**
	 * Insert the password into db if it satisfies all policies
	 * 
	 * @param userName String containing the username for which password is to be stored
	 * @param password String containing the new password
	 * 
	 * @throws Exception
	 */
	public void insertPasswordIntoDb(String userName, String password) throws Exception;
	
	/**
	 * Method which queries the db to determine if user is privileged or not
	 * 
	 * @param userName String containing the username for which password is to be stored
	 * @return
	 * @throws Exception
	 */
	public boolean isPrivilegedUser(String userName) throws Exception;
}
