/**
 * 
 */
package com.getinsured.wso2.identity.mgt.password.policy.util;

import java.security.MessageDigest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Biswakesh
 * 
 */
public class EncryptionUtil extends CloneNotSupportedException{

	private static final String HASHING_ALG = "SHA-256";

	private static final long serialVersionUID = 1L;

	private static Log log = LogFactory.getLog(EncryptionUtil.class);

	private static final Object lock = new Object();

	private static EncryptionUtil instance = null;

	private EncryptionUtil() {

	}

	/**
	 * Singleton for this class
	 * 
	 * @return Object containning EncryptionUtil
	 */
	public static EncryptionUtil getInstance() {
		synchronized (lock) {
			if (null == instance) {
				instance = new EncryptionUtil();
			}
			return instance;
		}
	}

	/**
	 * Method to encrypt the password with hash
	 * 
	 * @param password
	 *            String containing the plain text password
	 * @return String containing the encrypted password
	 */
	public String encryptPasswordWithHash(String password) {
		String encrypted = null;
		MessageDigest digest = null;
		byte[] passwordBytes = null;
		byte[] message = null;

		try {
			digest = MessageDigest.getInstance(HASHING_ALG);
			passwordBytes = password.getBytes();

			digest.reset();
			digest.update(passwordBytes);
			message = digest.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < message.length; i++) {
				hexString.append(Integer.toHexString(0xFF & message[i]));
			}
			encrypted = hexString.toString();
		} catch (Exception ex) {
			log.error("ERR: WHILE ENCRYPTING PASS: ", ex);
		}

		return encrypted;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
