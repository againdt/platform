README
------

1) Stop IS_SERVER
2) Add isPrivileged attribute to IS_HOME/repository/conf/scim-schema-extension.config file as follows
{
"attributeURI":"urn:scim:schemas:extension:wso2:1.0:wso2Extension.isPrivileged",
"attributeName":"isPrivileged",
"dataType":"string",
"multiValued":"false",
"multiValuedAttributeChildName":"null",
"description":"Is user privileged",
"schemaURI":"urn:scim:schemas:extension:wso2:1.0",
"readOnly":"false",
"required":"true",
"caseExact":"false",
"subAttributes":"null"
},

Note this should be added before the wso2Extension attribute
3) Define scim claim and wso2 claim for isPrivileged. Please refer to snapshot for the same.
4) Copy the ghix-wso2-password-policy-plugin-MAIN-BUILD-SNAPSHOT.jar to IS_HOME/repository/components/dropins/ and IS_HOME/repository/components/lib directory respectively
5) Add the foll. properties to IS_HOME/repository/conf/security/identity-mgt.properties file in the password policy extension section
	Password.policy.extensions.4=com.getinsured.wso2.identity.mgt.password.policy.GhixPasswordHistory
	Password.policy.extensions.4.history.enable=true
	Password.policy.extensions.4.history.limit=24
	Password.policy.extensions.4.history.errorCode=30000
	Password.policy.extensions.4.history.dbType=oracle (Possible values are mysql|oracle)
6) Create the foll. table in the database. (Assuming that Mysql is being used here)
	 CREATE TABLE `UM_PASSWORD_HISTORY` (
	  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
	  `UM_USER_NAME` varchar(255) NOT NULL,
	  `UM_USER_PASSWORD` varchar(255) NOT NULL,
	  `UM_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`UM_ID`),
	  UNIQUE KEY `UM_ID` (`UM_ID`,`UM_USER_NAME`)
	);
	create index UM_PASSWORD_HISTORY_USR_IDX1 on UM_PASSWORD_HISTORY(UM_USER_NAME);
7) Start IS_SERVER

