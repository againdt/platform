package com.getinsured.upload.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.couchbase.service.CouchDocumentBaseService;
import com.getinsured.upload.model.NoticeTemplateDocument;
import com.getinsured.upload.repository.NoticeTemplateRepository;

@Service
public class NoticeTemplateService extends CouchDocumentBaseService{

	private static final Logger LOG = LogManager.getLogger(NoticeTemplateService.class.getName());

	@Value("${branch}")
	private String branch;

	@Value("${tenantCode}")
	private String tenantCode;

	@Value("${env}")
	private String env;

	@Value("${localFolder}")
	private String localFolder;

	@Autowired
	@Qualifier("noticeTemplateRepository")
	private NoticeTemplateRepository noticeTemplateRepository;

	@SuppressWarnings("unchecked")
	@Override
	public NoticeTemplateRepository getRepository() {
		return noticeTemplateRepository;
	}

	public void test(){
		List<Map<String, String>> listOfMap = new ArrayList<Map<String,String>>();

		Stream.of(listOfMap).forEach((l) -> {
			l.parallelStream().forEach((m) -> {

			});
		});
	}

	public long upload() {
		File file = new File(localFolder);
		File[] files = file.listFiles();

		Stream.of(files).filter(f -> f.isFile()).parallel().forEach((f) -> {
			try {
				create(new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())),"UTF-8"), f.getName());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}});

		return Stream.of(files).filter(f -> f.isFile()).count();
	}

	public void create(String content, String name) {

		NoticeTemplateDocument noticeTemplate =
				noticeTemplateRepository.findByNameAndMetaData_tenantCodeAndMetaData_branch(name, tenantCode, branch);

		String result;
		String id;
		if (noticeTemplate == null){
			result = "Uploading new template - " + name;
			noticeTemplate = new NoticeTemplateDocument();
			noticeTemplate.getMetaData().setBranch(branch);
			noticeTemplate.getMetaData().setTenantCode(tenantCode);
			noticeTemplate.setName(name);
			noticeTemplate.setContent(content);
			id = this.createDocument(noticeTemplate);
		} else {
			result = "Updating template - " + name;
			noticeTemplate.setContent(content);
			noticeTemplate.getMetaData().setModifiedDate(new Date());
			id = this.updateDocument(noticeTemplate);
		}

		LOG.info(result + " - " + id);

	}

	public NoticeTemplateDocument read(String name){
		NoticeTemplateDocument obj = noticeTemplateRepository.findByNameAndMetaData_tenantCodeAndMetaData_branch(name, tenantCode, branch);
		return obj;
	}

}