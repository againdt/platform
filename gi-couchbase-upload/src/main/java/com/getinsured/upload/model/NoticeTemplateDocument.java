package com.getinsured.upload.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.getinsured.couchbase.model.CouchDocument;
import com.getinsured.couchbase.model.CouchMeta;

@Document
public class NoticeTemplateDocument extends CouchDocument {

	private static final String TEMPLATE = "TMPL";
	private static final String PLATFORM = "PT";

	@Field
	@NotNull
	private String name;

	public NoticeTemplateDocument() {
		setContentMetaData();
	}

	private void setContentMetaData(){
		CouchMeta meta = new CouchMeta();
		meta.setCategory(PLATFORM);
		meta.setSubCategory(TEMPLATE);
		//meta.setCustomKey(String.valueOf(new Random(999999).nextLong())); for demo
		meta.setCreationDate(new Date());
		meta.setModifiedDate(new Date());
		meta.setCreatedBy("uploadjob");
		meta.setModifiedBy("uploadjob");
		this.setMetaData(meta);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}