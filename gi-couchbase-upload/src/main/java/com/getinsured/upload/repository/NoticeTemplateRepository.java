package com.getinsured.upload.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.getinsured.couchbase.repository.BaseCouchDocumentRepository;
import com.getinsured.upload.model.NoticeTemplateDocument;

@Repository("noticeTemplateRepository")
public interface NoticeTemplateRepository extends BaseCouchDocumentRepository<NoticeTemplateDocument, String> {


	NoticeTemplateDocument findByNameAndMetaData_tenantCodeAndMetaData_branch(String name, String tenantCode, String branch);
	List<NoticeTemplateDocument> findByMetaData_tenantCodeAndMetaData_branch( String tenantCode, String branch);

}