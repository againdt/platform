package com.getinsured.upload;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.upload.service.NoticeTemplateService;

@SpringBootApplication(
		scanBasePackages ={
				"com.getinsured.couchbase",
				"com.getinsured.upload"
		}
		)
@EnableCouchbaseRepositories({"com.getinsured.upload.repository"})
public class Application {

	private static final Logger LOG = LogManager.getLogger(Application.class.getName());

	@Value("${couchbase.nodes}")
	private List<String> nodes = new ArrayList<String>();

	@Value("${couchbase.binary.bucketname}")
	private String binaryBucketName;

	@Value("${couchbase.nonbinary.bucketname}")
	private String nonbinaryBucketName;

	@Value("${couchbase.password}")
	private String password;

	@Value("${localFolder}")
	private String localFolder;

	@Value("${branch}")
	private String branch;

	@Value("${env}")
	private String env;

	@Value("${tenantCode}")
	private String tenantCode;

	@Value("${spring.datasource.url}")
	private String datasource;

	@Value("${spring.datasource.username}")
	private String dbUsername;

	@Autowired
	private NoticeTemplateService noticeTemplateService;

	@Autowired private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) throws IOException {

		SpringApplication app = new SpringApplication(Application.class);
		app.setWebEnvironment(false);
		ConfigurableApplicationContext ctx = app.run(args);
		Application mainObj = ctx.getBean(Application.class);

		if (args == null || args.length == 0){
			LOG.error("Mode not specified.");
			throw new RuntimeException("Mode not specified.");
		}

		String mode = null;
		if (args.length >= 1){
			mode = args[0];
		}

		mainObj.displayConfiguration(mode);

		if (StringUtils.equalsIgnoreCase(mode, "noticetmpl")){
			mainObj.uploadNoticeTemplate();
		} else {
			LOG.error("Unknown Mode passed!");
			throw new RuntimeException("Unknown Mode passed!");
		}


	}

	private void uploadNoticeTemplate() {

		Instant start = Instant.now();

		int count = jdbcTemplate.update("update notice_types set external_id = null ");
		LOG.info(count);

		long noOfFileUploaded = noticeTemplateService.upload();
		Instant end = Instant.now();
		LOG.info("\nNo of templates uploaded - " + noOfFileUploaded + "\nTime Taken - " + Duration.between(start, end));
	}

	private void displayConfiguration(String mode) {
		LOG.info("=========================================================================");
		LOG.info("---------------------- Notice Upload Job Parameters ---------------------");
		LOG.info("=========================================================================");

		LOG.info("Couchbase Nodes - " + nodes.toString());
		LOG.info("Binary Bucket - " + binaryBucketName);
		LOG.info("Non Binary Bucket - " +nonbinaryBucketName);
		LOG.info("Reading files from local folder - " +localFolder);
		LOG.info("Branch - " + branch);
		LOG.info("Tenant Code - " + tenantCode);
		LOG.info("Env - " + env);
		LOG.info("DB connected - " + datasource);
		LOG.info("DB username - " + dbUsername);

		LOG.info("Mode - " + mode);

		LOG.info("=========================================================================");

	}


}

